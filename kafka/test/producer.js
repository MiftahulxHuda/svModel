const { Kafka } = require('kafkajs')

const kafka = new Kafka({
	clientId: 'myapp',
	brokers: ['mineops.tura.tech:8812'],
	// connectionTimeout: 3000,
	// requestTimeout: 25000,
	// retry: {
 //    	initialRetryTime: 100,
 //    	retries: 8
 //  	},
  	sasl: {
	    mechanism: 'plain',
	    username: 'coba',
	    password: 'cobacoba'
	},
});

const producer = kafka.producer({ groupId: 'fms' })

async function producerRun() {
	await producer.connect()

	for (let ii=0; ii<100000; ii++) {
		await producer.send({
		  	topic: 'gps',
		  	messages: [{
		        key: 'key1',
		        value: 'hello world ' + ii,
		        headers: {
		            'correlation-id': '2bfb68bb-893a-423b-a7fa-7b568cad5b67',
		            'system-id': 'my-system'
		        }
		    }],
		})
	}

	await producer.disconnect()
}

producerRun()
