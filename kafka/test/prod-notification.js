var Kafka = require('node-rdkafka');
var moment = require('moment');

var producer = new Kafka.Producer({
	'client.id': 'client',
	//'debug': 'all',
	'metadata.broker.list': '68.183.186.116:8811,68.183.186.116:8812',
	'group.id': 'fms',
	'enable.auto.commit': false,
	'security.protocol': 'SASL_PLAINTEXT',
	'sasl.mechanisms': 'PLAIN',
	'sasl.username': 'coba',
	'sasl.password': 'cobacoba',
	'dr_cb': true  //delivery report callback
});

var topicName = 'notification';

//logging debug messages, if debug is enabled
producer.on('event.log', function (log) {
	console.log(log);
});

//logging all errors
producer.on('event.error', function (err) {
	console.error('Error from producer');
	console.error(err);
});

//counter to stop this sample after maxMessages are sent
var counter = 0;
var maxMessages = 100000;

producer.on('delivery-report', function (err, report) {
	console.log('delivery-report: ' + JSON.stringify(report));
	counter++;
});

//Wait for the ready event before producing
producer.on('ready', function (arg) {
	console.log('producer ready.' + JSON.stringify(arg));

	//for (var i = 0; i < maxMessages; i++) {
	//var value = Buffer.from('value-' +i);
	var notification1 = {
		date: moment(moment(), "YYYY-MM-DD HH:mm:ss"),
		type: "string",
		destination: ["HD10-001"],
		value: "Pesan dari kafka"
	};

	var value1 = Buffer.from(JSON.stringify(notification1));

	var notification2 = {
		date: moment(moment(), "YYYY-MM-DD HH:mm:ss"),
		type: "sound",
		destination: ["HD10-001"],
		value: "001"
	};

	var value2 = Buffer.from(JSON.stringify(notification2));


	var key = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEiLCJlbWFpbCI6ImFkbWluQG1haWwuY29tIiwibmFtZSI6IkFkbWluIiwiY2Rfcm9sZSI6eyJjZCI6IjAwMzAwMSIsImNkX3NlbGYiOiIwMDMiLCJuYW1lIjoiQWRtaW5pc3RyYXRvciIsImpzb24iOnsiY2QiOiIwMDMwMDEiLCJzZXEiOm51bGwsIm5hbWUiOiJBZG1pbiIsInNsdWciOm51bGwsInRhZ18xIjpudWxsLCJ0YWdfMiI6bnVsbCwidGFnXzMiOm51bGwsImNkX3NlbGYiOiIwMDMiLCJpc19maXhlZCI6MSwiaXNfZGVsZXRlZCI6MH19LCJjZF9kZXBhcnRtZW50Ijp7ImNkIjoiMDA0MDAxIiwiY2Rfc2VsZiI6IjAwNCIsIm5hbWUiOiJTeXN0ZW0iLCJqc29uIjp7ImNkIjoiMDA0MDAxIiwic2VxIjpudWxsLCJuYW1lIjoiU3lzdGVtIiwic2x1ZyI6bnVsbCwidGFnXzEiOm51bGwsInRhZ18yIjpudWxsLCJ0YWdfMyI6bnVsbCwiY2Rfc2VsZiI6IjAwNCIsImlzX2ZpeGVkIjoxLCJpc19kZWxldGVkIjowfX0sInN0YWZmX2lkIjoiMTAwMDAwMDAiLCJmaW5nZXJwcmludF9pZCI6bnVsbCwic29ja2V0X2lkIjoic3RyaW5nIiwiZmlsZV9waWN0dXJlIjoiYXNzZXRzL2ltYWdlcy9hdmF0YXJzL3VzZXIuanBnIiwiZW51bV9hdmFpbGFiaWxpdHkiOjQsImlzX2F1dGhlbnRpY2F0ZWQiOjEsIl9vcmciOnsiaWQiOiIxIiwibmFtZSI6IlBUIFNhdHJpYSBCYWhhbmEgU2FyYW5hIiwiYWJiciI6IlNCUyIsImpzb24iOm51bGx9LCJfcm9sZSI6eyJjZCI6IjAwMzAwMSIsImNkX3NlbGYiOiIwMDMiLCJuYW1lIjoiQWRtaW5pc3RyYXRvciIsImpzb24iOnsiY2QiOiIwMDMwMDEiLCJzZXEiOm51bGwsIm5hbWUiOiJBZG1pbiIsInNsdWciOm51bGwsInRhZ18xIjpudWxsLCJ0YWdfMiI6bnVsbCwidGFnXzMiOm51bGwsImNkX3NlbGYiOiIwMDMiLCJpc19maXhlZCI6MSwiaXNfZGVsZXRlZCI6MH19LCJfZGVwYXJ0bWVudCI6eyJjZCI6IjAwNDAwMSIsImNkX3NlbGYiOiIwMDQiLCJuYW1lIjoiU3lzdGVtIiwianNvbiI6eyJjZCI6IjAwNDAwMSIsInNlcSI6bnVsbCwibmFtZSI6IlN5c3RlbSIsInNsdWciOm51bGwsInRhZ18xIjpudWxsLCJ0YWdfMiI6bnVsbCwidGFnXzMiOm51bGwsImNkX3NlbGYiOiIwMDQiLCJpc19maXhlZCI6MSwiaXNfZGVsZXRlZCI6MH19LCJpYXQiOjE1NjU5MjQwMzUsImV4cCI6MTg4MTI4NDAzNX0.qFIaIAyUmFsV3JglBeUUck1mebJANc5-sVqnDX-KLqg";
	// if partition is set to -1, librdkafka will use the default partitioner
	var partition = -1;
	var headers = [
		{ header: "header value" }
	]

	producer.produce(topicName, partition, value1, key, Date.now(), "", headers);
	producer.produce(topicName, partition, value2, key, Date.now(), "", headers);

	//}

	//need to keep polling for a while to ensure the delivery reports are received
	var pollLoop = setInterval(function () {
		producer.poll();
		if (counter === maxMessages) {
			clearInterval(pollLoop);
			producer.disconnect();
		}
	}, 1000);

});

producer.on('disconnected', function (arg) {
	console.log('producer disconnected. ' + JSON.stringify(arg));
});

//starting the producer
producer.connect();
