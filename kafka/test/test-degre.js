const p1 = {x:1, y:1};
const p2 = {x:2, y:2};
const p3 = {x:0, y:0};
const p4 = {x:1, y:0};

function getRad(x, y) {
    deg = Math.atan(x/y);
    return deg;
}

function getRad2(x, y) {
    deg = Math.atan(x/y) * 180 / Math.PI;
    return deg;
}

const p_12 = {x:p2.x-p1.x, y:p2.y-p1.y};
const p_13 = {x:p3.x-p1.x, y:p3.y-p1.y};
const p_14 = {x:p4.x-p1.x, y:p4.y-p1.y};

console.log(getRad(p_12.x, p_12.y));
console.log(getRad(p_13.x, p_13.y));
console.log(getRad(p_14.x, p_14.y));

console.log(getRad2(p_12.x, p_12.y));
console.log(getRad2(p_13.x, p_13.y));
console.log(getRad2(p_14.x, p_14.y));
