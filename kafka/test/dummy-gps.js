var Kafka = require('node-rdkafka');
var moment = require('moment');

var producer = new Kafka.Producer({
    'client.id': 'client',
    //'debug': 'all',
    'metadata.broker.list': '68.183.186.116:8811,68.183.186.116:8812',
    'group.id': 'fms',
    'enable.auto.commit': false,
    'security.protocol': 'SASL_PLAINTEXT',
    'sasl.mechanisms': 'PLAIN',
    'sasl.username': 'coba',
    'sasl.password': 'cobacoba',
    'dr_cb': true  //delivery report callback
});

var topicName = 'gps';

//logging debug messages, if debug is enabled
producer.on('event.log', function (log) {
    console.log(log);
});

//logging all errors
producer.on('event.error', function (err) {
    console.error('Error from producer');
    console.error(err);
});

//counter to stop this sample after maxMessages are sent
var counter = 0;
var maxMessages = 100000;

producer.on('delivery-report', function (err, report) {
    console.log('delivery-report: ' + JSON.stringify(report));
    counter++;
});

//Wait for the ready event before producing
producer.on('ready', function (arg) {
    console.log('producer ready.' + JSON.stringify(arg));

    function sendData(uid, latitude, longitude, travel) {
        var value = {
            date: moment().unix(),
            unit_id: uid,
            travel: travel, // empty|full
            shift: "007002",
            plan_id: "",
            unit_type: "015001", // 015002, dst
            operator: "",
            loader_id: "", // optional, jika unitnya hauler
            loader_operator: "", // optional, jika unitnya hauler
            location: "",
            destination: "",
            operation: "",
            material: "",
            latitude: latitude,
            longitude: longitude,
            altitude: 100.0,

            ecu: {
	            hour_meter: {
	                unit: "hour",
	                value: 100.0
	            },
	            fuel_rate: {
	                unit: "litre/hour",
	                value: 100.0
	            },
	            distance: {
	                unit: "km",
	                value: 100.0
	            },
	            engine_temperature: {
	                unit: "^C",
	                value: 100.0
	            },
	            payload: {
	                unit: "BCM",
	                value: 100.0
	            },
	            vehicle_speed: {
	                unit: "km/hour",
	                value: 100.0
	            }
            },
            ecus: {
                "date": "2020-03-05 14:00:31",
                "ecuHD785": {
                    "Eng_Speed":655,
                    "TM_Out_Speed":0,
                    "Vehicle_Speed_S":0,
                    "Shift_Indicator":0,
                    "Live_Weight":11.38,
                    "Fuel_Injection":5,
                    "Accel_Pos":0,
                    "Retarder_F_Oit_temp":67,
                    "Retarder_R_Oil_temp":68,
                    "Fuel_Rate_01L":11.9,
                    "Retarder_Pos":80
                }
	            // bbb: {
	            //     unit: "litre/hour",
	            //     value: 100.0
	            // },
	            // ccc: {
	            //     unit: "km",
	            //     value: 100.0
	            // },
            },
            device_status: {
                memory_free: 8247.54176,
                cpu_system_load: 0.89,
                storage_free: 0,
                storage_size: 0,
                cpu_system: 0.17376775271512113,
                cpu_process: 0.00835421888053467,
                memory_size: 16703.717376,
            }
        };
        var key = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEiLCJlbWFpbCI6ImFkbWluQG1haWwuY29tIiwibmFtZSI6IkFkbWluIiwiY2Rfcm9sZSI6eyJjZCI6IjAwMzAwMSIsImNkX3NlbGYiOiIwMDMiLCJuYW1lIjoiQWRtaW5pc3RyYXRvciIsImpzb24iOnsiY2QiOiIwMDMwMDEiLCJzZXEiOm51bGwsIm5hbWUiOiJBZG1pbiIsInNsdWciOm51bGwsInRhZ18xIjpudWxsLCJ0YWdfMiI6bnVsbCwidGFnXzMiOm51bGwsImNkX3NlbGYiOiIwMDMiLCJpc19maXhlZCI6MSwiaXNfZGVsZXRlZCI6MH19LCJjZF9kZXBhcnRtZW50Ijp7ImNkIjoiMDA0MDAxIiwiY2Rfc2VsZiI6IjAwNCIsIm5hbWUiOiJTeXN0ZW0iLCJqc29uIjp7ImNkIjoiMDA0MDAxIiwic2VxIjpudWxsLCJuYW1lIjoiU3lzdGVtIiwic2x1ZyI6bnVsbCwidGFnXzEiOm51bGwsInRhZ18yIjpudWxsLCJ0YWdfMyI6bnVsbCwiY2Rfc2VsZiI6IjAwNCIsImlzX2ZpeGVkIjoxLCJpc19kZWxldGVkIjowfX0sInN0YWZmX2lkIjoiMTAwMDAwMDAiLCJmaW5nZXJwcmludF9pZCI6bnVsbCwic29ja2V0X2lkIjoic3RyaW5nIiwiZmlsZV9waWN0dXJlIjoiYXNzZXRzL2ltYWdlcy9hdmF0YXJzL3VzZXIuanBnIiwiZW51bV9hdmFpbGFiaWxpdHkiOjQsImlzX2F1dGhlbnRpY2F0ZWQiOjEsIl9vcmciOnsiaWQiOiIxIiwibmFtZSI6IlBUIFNhdHJpYSBCYWhhbmEgU2FyYW5hIiwiYWJiciI6IlNCUyIsImpzb24iOm51bGx9LCJfcm9sZSI6eyJjZCI6IjAwMzAwMSIsImNkX3NlbGYiOiIwMDMiLCJuYW1lIjoiQWRtaW5pc3RyYXRvciIsImpzb24iOnsiY2QiOiIwMDMwMDEiLCJzZXEiOm51bGwsIm5hbWUiOiJBZG1pbiIsInNsdWciOm51bGwsInRhZ18xIjpudWxsLCJ0YWdfMiI6bnVsbCwidGFnXzMiOm51bGwsImNkX3NlbGYiOiIwMDMiLCJpc19maXhlZCI6MSwiaXNfZGVsZXRlZCI6MH19LCJfZGVwYXJ0bWVudCI6eyJjZCI6IjAwNDAwMSIsImNkX3NlbGYiOiIwMDQiLCJuYW1lIjoiU3lzdGVtIiwianNvbiI6eyJjZCI6IjAwNDAwMSIsInNlcSI6bnVsbCwibmFtZSI6IlN5c3RlbSIsInNsdWciOm51bGwsInRhZ18xIjpudWxsLCJ0YWdfMiI6bnVsbCwidGFnXzMiOm51bGwsImNkX3NlbGYiOiIwMDQiLCJpc19maXhlZCI6MSwiaXNfZGVsZXRlZCI6MH19LCJpYXQiOjE1NjU5MjQwMzUsImV4cCI6MTg4MTI4NDAzNX0.qFIaIAyUmFsV3JglBeUUck1mebJANc5-sVqnDX-KLqg";
        // if partition is set to -1, librdkafka will use the default partitioner
        var partition = -1;
        var headers = [
            { header: "header value" }
        ];
        var buffer = Buffer.from(JSON.stringify(value));
        producer.produce(topicName, partition, buffer, key, Date.now(), "", headers);

        var ecu = {
	        date: moment(moment(), "YYYY-MM-DD HH:mm:ss"),
	        unit_id: uid,
	        shift: "",
	        plan_id: "",
	        ecu: {
	            hour_meter: {
	                unit: "hour",
	                value: 100.0
	            },
	            fuel_rate: {
	                unit: "litre/hour",
	                value: 100.0
	            },
	            distance: {
	                unit: "km",
	                value: 100.0
	            },
	            engine_temperature: {
	                unit: "^C",
	                value: 100.0
	            },
	            payload: {
	                unit: "BCM",
	                value: 100.0
	            },
	            vehicle_speed: {
	                unit: "km/hour",
	                value: 100.0
	            }
	        }
	    }

		// var bufferEcu = Buffer.from(JSON.stringify(ecu));
        // producer.produce("ecu", partition, bufferEcu, key, Date.now(), "", headers);
    }

    //need to keep polling for a while to ensure the delivery reports are received
    let latitude = -3.7569;
    let longitude = 103.782;
    let travel = 'empty';

    setInterval(function () {

        if (travel === 'empty') {
            latitude += 0.001;
        } else {
            latitude -= 0.001;
        }

        sendData("030af4a0-2e99-40e1-9406-1740243605b7", latitude, longitude, travel);
        // sendData("01a22d2a-7393-47f8-a85d-4430447c8d3b", latitude, longitude+0.01, travel);
        // sendData("094a5c92-f659-4ef0-bf6b-c08c804fcf38", latitude, longitude-0.01, travel);

        if (latitude > -3.7369 ) {
            travel = 'full';
        }
        if (latitude < -3.7569 ) {
            travel = 'empty';
        }

        producer.poll();

    }, 3000);

});

producer.on('disconnected', function (arg) {
    console.log('producer disconnected. ' + JSON.stringify(arg));
});

producer.connect();
