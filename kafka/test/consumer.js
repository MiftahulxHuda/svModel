const { Kafka } = require('kafkajs')

const kafka = new Kafka({
	clientId: 'myapp',
	brokers: ['mineops.tura.tech:8812'],
	connectionTimeout: 3000,
	requestTimeout: 25000,
	retry: {
    	initialRetryTime: 100,
    	retries: 8
  	},
  	sasl: {
	    mechanism: 'plain',
	    username: 'coba',
	    password: 'cobacoba'
	},
});

async function consumerRun() {
	const consumer = kafka.consumer({ groupId: 'fms' })

	await consumer.connect()
	await consumer.subscribe({ topic: 'gps' })

	await consumer.run({
	  eachMessage: async ({ topic, partition, message }) => {
	    console.log({
	      value: message.value.toString(),
	    })
	  },
	})
}

consumerRun()
