const async = require('async');
const moment = require('moment-timezone');
const config = require('./config');
const util = require('./util');

moment.tz.setDefault('Asia/Jakarta');

module.exports.run = (mysql, mongo, clog, callback) => {

    let paramStart = process.argv[2];
    let times = [];

    /**
     * prepare time set
     */
    if (paramStart) {
        const naw = moment().endOf('day').unix();
        let start = moment(paramStart, 'YYYY-MM-DD').startOf('day').unix();
        while (start < naw) {
            times.push({
                start: moment.unix(start).set({ hour: 6, minute: 0, second: 0 }).unix(),
                stop: moment.unix(start).set({ hour: 18, minute: 0, second: 0 }).unix(),
                shift: util.SHIFT_DAY
            });
            times.push({
                start: moment.unix(start).set({ hour: 18, minute: 0, second: 0 }).unix(),
                stop: moment.unix(start).add(1, 'days').set({ hour: 6, minute: 0, second: 0 }).unix(),
                shift: util.SHIFT_NIGHT
            });
            start = moment.unix(start).add(1, 'days').unix();
        }
    } else {
        times.push({
            start: util.util.translateTimestamp('startOfShift'),
            stop: util.util.translateTimestamp('endOfShift'),
            shift: util.util.getCurrentShift()
        });
    }

    /**
     * shift crawler method
     * sudut pandang equipment
     */
    async function shiftCrawler(time, my, mo, shiftCrawlerCallback) {
        const rawData = await util.mysqlQuery(my, "SELECT  \
                m.id, m.time, m.id_actual, m.id_operation_support, m.id_operation_loader, \
                eq.id equipment_id, eq.name equipment_name, eq.cd_equipment equipment_type, \
                us.id user_id, us.name user_name, us.cd_role user_role, us.staff_id user_nik, us.cd_department user_dept, \
                ac.cd activity_cd, ac.name activity_name,  \
                tu.cd tum_cd, tu.name tum_name  \
            FROM equipment_log_start m \
            LEFT JOIN equipment eq ON eq.id=m.id_equipment \
            LEFT JOIN user us ON us.id=m.id_operator \
            LEFT JOIN constant ac ON ac.cd=m.cd_activity \
            LEFT JOIN constant tu ON tu.cd=m.cd_tum \
            WHERE m.time>=" + time.start + " AND m.time<" + time.stop + " \
            ORDER BY m.time ASC");
        const mapped = {};
        for (let iter = 0; iter < rawData.length; iter++) {
            if (mapped[rawData[iter].equipment_id] === undefined) {
                mapped[rawData[iter].equipment_id] = [];
            }
            if (mapped[rawData[iter].equipment_id].length === 0) {
                mapped[rawData[iter].equipment_id].push({
                    start: rawData[iter].time,
                    stop: 0,
                    actual: rawData[iter].id_actual,
                    actual_support: rawData[iter].id_operation_support,
                    actual_loader: rawData[iter].id_operation_loader,
                    operator: {
                        id: rawData[iter].user_id,
                        nik: rawData[iter].user_nik,
                        name: rawData[iter].user_name,
                        role: rawData[iter].user_role,
                        department: rawData[iter].user_dept
                    },
                    equipment: {
                        id: rawData[iter].equipment_id,
                        name: rawData[iter].equipment_name,
                        type: rawData[iter].equipment_type
                    },
                    activity: {
                        cd: rawData[iter].activity_cd,
                        name: rawData[iter].activity_name
                    },
                    tum: {
                        cd: rawData[iter].tum_cd,
                        name: rawData[iter].tum_name
                    },
                    shift: {
                        cd: time.shift,
                        name: time.shift === util.SHIFT_DAY ? 'Day' : 'Night'
                    },
                    duration: 0
                });
            } else {
                const sz = mapped[rawData[iter].equipment_id].length - 1;
                mapped[rawData[iter].equipment_id][sz].stop = rawData[iter].time;
                mapped[rawData[iter].equipment_id][sz].duration = mapped[rawData[iter].equipment_id][sz].stop - mapped[rawData[iter].equipment_id][sz].start;
                mapped[rawData[iter].equipment_id].push({
                    start: rawData[iter].time,
                    stop: 0,
                    operator: {
                        id: rawData[iter].user_id,
                        nik: rawData[iter].user_nik,
                        name: rawData[iter].user_name,
                        role: rawData[iter].user_role,
                        department: rawData[iter].user_dept
                    },
                    equipment: {
                        id: rawData[iter].equipment_id,
                        name: rawData[iter].equipment_name,
                        type: rawData[iter].equipment_type
                    },
                    activity: {
                        cd: rawData[iter].activity_cd,
                        name: rawData[iter].activity_name
                    },
                    tum: {
                        cd: rawData[iter].tum_cd,
                        name: rawData[iter].tum_name
                    },
                    shift: {
                        cd: time.shift,
                        name: time.shift === util.SHIFT_DAY ? 'Day' : 'Night'
                    },
                    duration: 0
                });
            }
        }
        let remap = [];
        for (let items of Object.values(mapped)) {
            remap = remap.concat(items);
        }

        /**
         * hapus data lama
         */
        await util.mongoDelete(mo, {
            collection: config.database.ACTIVITY_RAW,
            option: {
                start: {
                    $gte: time.start,
                    $lt: time.stop
                }
            }
        });

        if (remap.length > 0) {
            await util.mongoInsert(mo, {
                collection: config.database.ACTIVITY_RAW,
                data: remap
            });
        }
        shiftCrawlerCallback();
        /**
         * ingat beberapa data khususnya data di akhir shift akan memiliki
         * duration == 0
         * maka perlu dipasangkan dengan stop nya
         */
    }

    /**
     * refinement
     */
    async function refinementZero(mo, refinementCallback) {
        const zeros = await util.mongoFind(mo, {
            collection: config.database.ACTIVITY_RAW,
            option: {
                stop: 0,
                duration: 0
            }
        });
        for (let dx = 0; dx < zeros.length; dx++) {
            const next = await util.mongoFindOne(mo, {
                collection: config.database.ACTIVITY_RAW,
                option: {
                    start: {
                        $gt: zeros[dx].start
                    },
                    'equipment.id': zeros[dx].equipment.id
                },
                sort: {
                    start: 1
                }
            });
            if (next.length > 0) {
                const stop = next[0].start;
                const duration = stop - zeros[dx].start;
                await util.mongoUpdate(mo, {
                    collection: config.database.ACTIVITY_RAW,
                    option: {
                        _id: zeros[dx]._id
                    },
                    data: {
                        $set: {
                            stop: stop,
                            duration: duration
                        }
                    }
                });
            }
        }
        refinementCallback();
    }

    async.waterfall([

        (cb) => {
            /**
             * do crawl raw data by times
             */
            clog.info('------ crawling by times, ', times.length, ' items');
            async.forEachOf(times, (time, key, _cb) => {
                shiftCrawler(time, mysql, mongo, () => {
                    _cb();
                });
            }, (err) => {
                cb();
            });
        },

        (cb) => {
            /**
             * refinement zero duration
             */
            clog.info('------ refinement zero');
            refinementZero(mongo, () => {
                cb();
            })
        },

    ], (res) => {
        callback();
    });
}