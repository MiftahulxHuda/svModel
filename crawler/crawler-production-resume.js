const async = require('async');
const moment = require('moment-timezone');
const config = require('./config');
const util = require('./util');

moment.tz.setDefault('Asia/Jakarta');

module.exports.run = (mysql, mongo, clog, callback) => {

    /**
     * generate shift range
     */
    async function generateShift(mo, shiftCallback) {
        const min = await util.mongoFindOne(mo, {
            collection: config.database.PRODUCTION_RAW,
            option: {},
            sort: {
                start: 1
            }
        });
        const max = await util.mongoFindOne(mo, {
            collection: config.database.PRODUCTION_RAW,
            option: {},
            sort: {
                start: -1
            }
        });
        const minYear = min.length > 0 ? moment.unix(min[0].start).year() : moment().year();
        const maxYear = max.length > 0 ? moment.unix(max[0].start).year() : moment().year();
        const shifts = [];
        for (let ye = minYear; ye <= maxYear; ye++) {
            for (let mm = 0; mm < 12; mm++) {
                const latestDate = moment().set({ year: ye, month: mm }).endOf('month').date();
                for (let dt = 1; dt <= latestDate; dt++) {
                    shifts.push({
                        year: ye,
                        month: mm,
                        date: dt,
                        shift: util.SHIFT_DAY,
                        start: moment().set({ year: ye, month: mm, date: dt }).set({ hour: 6, minute: 0, second: 0 }).unix(),
                        stop: moment().set({ year: ye, month: mm, date: dt }).set({ hour: 18, minute: 0, second: 0 }).unix(),
                    });
                    shifts.push({
                        year: ye,
                        month: mm,
                        date: dt,
                        shift: util.SHIFT_NIGHT,
                        start: moment().set({ year: ye, month: mm, date: dt }).set({ hour: 18, minute: 0, second: 0 }).unix(),
                        stop: moment().set({ year: ye, month: mm, date: dt }).add(1, 'days').set({ hour: 6, minute: 0, second: 0 }).unix(),
                    });
                }
            }
        }
        shiftCallback(shifts);
    }

    /**
     * get all week
     */
    async function generateWeeks(mo, weekCallback) {
        const min = await util.mongoFindOne(mo, {
            collection: config.database.PRODUCTION_RAW,
            option: {},
            sort: {
                start: 1
            }
        });
        const max = await util.mongoFindOne(mo, {
            collection: config.database.PRODUCTION_RAW,
            option: {},
            sort: {
                start: -1
            }
        });
        const minYear = min.length > 0 ? moment.unix(min[0].start).year() : moment().year();
        const maxYear = max.length > 0 ? moment.unix(max[0].start).year() : moment().year();
        const weeks = [];
        for (let ye = minYear; ye <= maxYear; ye++) {
            const totalWeek = moment().set({ year: ye }).endOf('year').add(-1, 'week').week();
            for (let wee = 1; wee <= totalWeek; wee++) {
                weeks.push({
                    year: ye,
                    week: wee,
                    start: moment().set({ year: ye }).week(wee).startOf('week').unix(),
                    stop: moment().set({ year: ye }).week(wee).endOf('week').unix(),
                });
            }
        }
        weekCallback(weeks);
    }

    /**
     * get all month
     */
    async function generateMonth(mo, monthCallback) {
        const min = await util.mongoFindOne(mo, {
            collection: config.database.PRODUCTION_RAW,
            option: {},
            sort: {
                start: 1
            }
        });
        const max = await util.mongoFindOne(mo, {
            collection: config.database.PRODUCTION_RAW,
            option: {},
            sort: {
                start: -1
            }
        });
        const minYear = min.length > 0 ? moment.unix(min[0].start).year() : moment().year();
        const maxYear = max.length > 0 ? moment.unix(max[0].start).year() : moment().year();
        const months = [];
        for (let ye = minYear; ye <= maxYear; ye++) {
            const totalMonth = 12;
            for (let m = 0; m < totalMonth; m++) {
                months.push({
                    year: ye,
                    month: m,
                    start: moment().set({ year: ye, month: m }).startOf('month').unix(),
                    stop: moment().set({ year: ye, month: m }).endOf('month').unix(),
                });
            }
        }
        monthCallback(months);
    }

    /**
     * generate year
     */
    async function generateYears(mo, yearCallback) {
        const min = await util.mongoFindOne(mo, {
            collection: config.database.PRODUCTION_RAW,
            option: {},
            sort: {
                start: 1
            }
        });
        const max = await util.mongoFindOne(mo, {
            collection: config.database.PRODUCTION_RAW,
            option: {},
            sort: {
                start: -1
            }
        });
        const minYear = min.length > 0 ? moment.unix(min[0].start).year() : moment().year();
        const maxYear = max.length > 0 ? moment.unix(max[0].start).year() : moment().year();
        const years = [];
        for (let ye = minYear; ye <= maxYear; ye++) {
            years.push({
                year: ye,
                start: moment().set({ year: ye }).startOf('year').unix(),
                stop: moment().set({ year: ye }).endOf('year').unix(),
            });
        }
        yearCallback(years);
    }

    /**
     * generate all
     */
    async function generateAll(mo, allCallback) {
        const min = await util.mongoFindOne(mo, {
            collection: config.database.PRODUCTION_RAW,
            option: {},
            sort: {
                start: 1
            }
        });
        const max = await util.mongoFindOne(mo, {
            collection: config.database.PRODUCTION_RAW,
            option: {},
            sort: {
                start: -1
            }
        });
        const mins = min.length > 0 ? min[0].start : moment().unix();
        const maxs = max.length > 0 ? max[0].start : moment().unix();
        const all = [];
        all.push({
            start: mins,
            stop: maxs + 1
        });
        allCallback(all);
    }

    /**
     * get raw data
     */
    async function getRawData(mo, rawDataCallback) {
        const rawData = await util.mongoFind(mo, {
            collection: config.database.PRODUCTION_RAW,
            option: {}
        });
        rawDataCallback(rawData);
    }

    /**
     * resume
     */
    async function resumeByTimes(times, rawData, mo, collection, resumeCallback) {
        const resultData = [];
        const time = Object.assign([], times);

        /**
         * resume duration
         */
        for (let dx = 0; dx < rawData.length; dx++) {
            for (let me = 0; me < time.length; me++) {

                if (time[me]['hauler_material'] === undefined) {
                    time[me]['hauler_material'] = [];
                }
                if (time[me]['hauler_operation'] === undefined) {
                    time[me]['hauler_operation'] = [];
                }
                if (time[me]['hauler_volume'] === undefined) {
                    time[me]['hauler_volume'] = [];
                }
                if (time[me]['loader_material'] === undefined) {
                    time[me]['loader_material'] = [];
                }
                if (time[me]['loader_operation'] === undefined) {
                    time[me]['loader_operation'] = [];
                }
                if (time[me]['loader_volume'] === undefined) {
                    time[me]['loader_volume'] = [];
                }
                if (time[me]['operator_hauler_material'] === undefined) {
                    time[me]['operator_hauler_material'] = [];
                }
                if (time[me]['operator_hauler_operation'] === undefined) {
                    time[me]['operator_hauler_operation'] = [];
                }
                if (time[me]['operator_hauler_volume'] === undefined) {
                    time[me]['operator_hauler_volume'] = [];
                }
                if (time[me]['operator_loader_material'] === undefined) {
                    time[me]['operator_loader_material'] = [];
                }
                if (time[me]['operator_loader_operation'] === undefined) {
                    time[me]['operator_loader_operation'] = [];
                }
                if (time[me]['operator_loader_volume'] === undefined) {
                    time[me]['operator_loader_volume'] = [];
                }
                if (time[me]['location_material'] === undefined) {
                    time[me]['location_material'] = [];
                }
                if (time[me]['location_operation'] === undefined) {
                    time[me]['location_operation'] = [];
                }
                if (time[me]['location_volume'] === undefined) {
                    time[me]['location_volume'] = [];
                }
                if (time[me]['area_material'] === undefined) {
                    time[me]['area_material'] = [];
                }
                if (time[me]['area_operation'] === undefined) {
                    time[me]['area_operation'] = [];
                }
                if (time[me]['area_volume'] === undefined) {
                    time[me]['area_volume'] = [];
                }
                if (time[me]['hauler'] === undefined) {
                    time[me]['hauler'] = [];
                }
                if (time[me]['loader'] === undefined) {
                    time[me]['loader'] = [];
                }
                if (time[me]['operator_hauler'] === undefined) {
                    time[me]['operator_hauler'] = [];
                }
                if (time[me]['operator_loader'] === undefined) {
                    time[me]['operator_loader'] = [];
                }
                if (time[me]['location'] === undefined) {
                    time[me]['location'] = [];
                }
                if (time[me]['area'] === undefined) {
                    time[me]['area'] = [];
                }
                if (time[me]['material'] === undefined) {
                    time[me]['material'] = [];
                }
                if (time[me]['operation'] === undefined) {
                    time[me]['operation'] = [];
                }
                if (time[me]['volume'] === undefined) {
                    time[me]['volume'] = [];
                }

                if (rawData[dx].start >= time[me].start && rawData[dx].start < time[me].stop) {

                    const haulerMaterial = time[me]['hauler_material'].filter(item => {
                        return item.hauler.id === rawData[dx].hauler.id && item.material === rawData[dx].material.type;
                    });
                    const haulerOperation = time[me]['hauler_operation'].filter(item => {
                        return item.hauler.id === rawData[dx].hauler.id && item.operation.cd === rawData[dx].operation.cd;
                    });
                    const haulerVolume = time[me]['hauler_volume'].filter(item => {
                        return item.hauler.id === rawData[dx].hauler.id && item.is_volume === rawData[dx].is_volume;
                    });

                    const loaderMaterial = time[me]['loader_material'].filter(item => {
                        return item.loader.id === rawData[dx].loader.id && item.material === rawData[dx].material.type;
                    });
                    const loaderOperation = time[me]['loader_operation'].filter(item => {
                        return item.loader.id === rawData[dx].loader.id && item.operation.cd === rawData[dx].operation.cd;
                    });
                    const loaderVolume = time[me]['loader_volume'].filter(item => {
                        return item.loader.id === rawData[dx].loader.id && item.is_volume === rawData[dx].is_volume;
                    });

                    const operatorHaulerMaterial = time[me]['operator_hauler_material'].filter(item => {
                        return item.operator_hauler.id === rawData[dx].operator_hauler.id && item.material === rawData[dx].material.type;
                    });
                    const operatorHaulerOperation = time[me]['operator_hauler_operation'].filter(item => {
                        return item.operator_hauler.id === rawData[dx].operator_hauler.id && item.operation.cd === rawData[dx].operation.cd;
                    });
                    const operatorHaulerVolume = time[me]['operator_hauler_volume'].filter(item => {
                        return item.operator_hauler.id === rawData[dx].operator_hauler.id && item.is_volume === rawData[dx].is_volume;
                    });

                    const operatorLoaderMaterial = time[me]['operator_loader_material'].filter(item => {
                        return item.operator_loader.id === rawData[dx].operator_loader.id && item.material === rawData[dx].material.type;
                    });
                    const operatorLoaderOperation = time[me]['operator_loader_operation'].filter(item => {
                        return item.operator_loader.id === rawData[dx].operator_loader.id && item.operation.cd === rawData[dx].operation.cd;
                    });
                    const operatorLoaderVolume = time[me]['operator_loader_volume'].filter(item => {
                        return item.operator_loader.id === rawData[dx].operator_loader.id && item.is_volume === rawData[dx].is_volume;
                    });

                    const locationMaterial = time[me]['location_material'].filter(item => {
                        return item.location.id === rawData[dx].location.id && item.material === rawData[dx].material.type;
                    });
                    const locationOperation = time[me]['location_operation'].filter(item => {
                        return item.location.id === rawData[dx].location.id && item.operation.cd === rawData[dx].operation.cd;
                    });
                    const locationVolume = time[me]['location_volume'].filter(item => {
                        return item.location.id === rawData[dx].location.id && item.is_volume === rawData[dx].is_volume;
                    });

                    const areaMaterial = time[me]['area_material'].filter(item => {
                        return item.area.id === rawData[dx].area.id && item.material === rawData[dx].material.type;
                    });
                    const areaOperation = time[me]['area_operation'].filter(item => {
                        return item.area.id === rawData[dx].area.id && item.operation.cd === rawData[dx].operation.cd;
                    });
                    const areaVolume = time[me]['area_volume'].filter(item => {
                        return item.area.id === rawData[dx].area.id && item.is_volume === rawData[dx].is_volume;
                    });

                    const hauler = time[me]['hauler'].filter(item => {
                        return item.hauler.id === rawData[dx].hauler.id;
                    });

                    const loader = time[me]['loader'].filter(item => {
                        return item.loader.id === rawData[dx].loader.id;
                    });

                    const operatorHauler = time[me]['operator_hauler'].filter(item => {
                        return item.operator_hauler.id === rawData[dx].operator_hauler.id;
                    });

                    const operatorLoader = time[me]['operator_loader'].filter(item => {
                        return item.operator_loader.id === rawData[dx].operator_loader.id;
                    });

                    const location = time[me]['location'].filter(item => {
                        return item.location.id === rawData[dx].location.id;
                    });

                    const area = time[me]['area'].filter(item => {
                        return item.area.id === rawData[dx].area.id;
                    });

                    const material = time[me]['material'].filter(item => {
                        return item.material === rawData[dx].material.type;
                    });

                    const operation = time[me]['operation'].filter(item => {
                        return item.operation.cd === rawData[dx].operation.cd;
                    });

                    const volume = time[me]['volume'].filter(item => {
                        return item.is_volume === rawData[dx].is_volume;
                    });

                    const bcmPayload = rawData[dx].material.type === util.MATERIAL_COAL ?
                        util.util.getBCMFromTonnage(Number(rawData[dx].tonnage_payload), Number(rawData[dx].material.density))
                        : Number(rawData[dx].tonnage_payload);
                    const bcmUjipetik = rawData[dx].material.type === util.MATERIAL_COAL ?
                        util.util.getBCMFromTonnage(Number(rawData[dx].tonnage_ujipetik), Number(rawData[dx].material.density))
                        : Number(rawData[dx].tonnage_ujipetik);
                    const bcmTimbangan = rawData[dx].material.type === util.MATERIAL_COAL ?
                        util.util.getBCMFromTonnage(Number(rawData[dx].tonnage_timbangan), Number(rawData[dx].material.density))
                        : Number(rawData[dx].tonnage_timbangan);
                    const bcmBucket = rawData[dx].material.type === util.MATERIAL_COAL ?
                        util.util.getBCMFromTonnage(Number(rawData[dx].tonnage_bucket), Number(rawData[dx].material.density))
                        : Number(rawData[dx].tonnage_bucket);


                    if (haulerMaterial.length > 0) {
                        haulerMaterial[0].ritase += 1;
                        haulerMaterial[0].bucket_count += Number(rawData[dx].bucket_count);
                        haulerMaterial[0].tonnage_payload += Number(rawData[dx].tonnage_payload);
                        haulerMaterial[0].tonnage_ujipetik += Number(rawData[dx].tonnage_ujipetik);
                        haulerMaterial[0].tonnage_timbangan += Number(rawData[dx].tonnage_timbangan);
                        haulerMaterial[0].tonnage_bucket += Number(rawData[dx].tonnage_bucket);
                        haulerMaterial[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_payload);
                        haulerMaterial[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_ujipetik);
                        haulerMaterial[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_timbangan);
                        haulerMaterial[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_bucket);
                        haulerMaterial[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_payload);
                        haulerMaterial[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_ujipetik);
                        haulerMaterial[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_timbangan);
                        haulerMaterial[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_bucket);
                        haulerMaterial[0].distance_survei_payload = Number(haulerMaterial[0].sp_distance_survei_payload) / Number(haulerMaterial[0].tonnage_payload);
                        haulerMaterial[0].distance_survei_ujipetik = Number(haulerMaterial[0].sp_distance_survei_ujipetik) / Number(haulerMaterial[0].tonnage_ujipetik);
                        haulerMaterial[0].distance_survei_timbangan = Number(haulerMaterial[0].sp_distance_survei_timbangan) / Number(haulerMaterial[0].tonnage_timbangan);
                        haulerMaterial[0].distance_survei_bucket = Number(haulerMaterial[0].sp_distance_survei_bucket) / Number(haulerMaterial[0].tonnage_bucket);
                        haulerMaterial[0].distance_gps_payload = Number(haulerMaterial[0].sp_distance_gps_payload) / Number(haulerMaterial[0].tonnage_payload);
                        haulerMaterial[0].distance_gps_ujipetik = Number(haulerMaterial[0].sp_distance_gps_ujipetik) / Number(haulerMaterial[0].tonnage_ujipetik);
                        haulerMaterial[0].distance_gps_timbangan = Number(haulerMaterial[0].sp_distance_gps_timbangan) / Number(haulerMaterial[0].tonnage_timbangan);
                        haulerMaterial[0].distance_gps_bucket = Number(haulerMaterial[0].sp_distance_gps_bucket) / Number(haulerMaterial[0].tonnage_bucket);
                    } else {
                        time[me]['hauler_material'].push({
                            hauler: rawData[dx].hauler,
                            material: rawData[dx].material.type,
                            ritase: 1,
                            satuan: rawData[dx].material.type === util.MATERIAL_COAL ? 'Ton' : 'BCM',
                            bucket_count: Number(rawData[dx].bucket_count),
                            tonnage_payload: Number(rawData[dx].tonnage_payload),
                            tonnage_ujipetik: Number(rawData[dx].tonnage_ujipetik),
                            tonnage_timbangan: Number(rawData[dx].tonnage_timbangan),
                            tonnage_bucket: Number(rawData[dx].tonnage_bucket),
                            sp_distance_survei_payload: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_payload),
                            sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_ujipetik),
                            sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_timbangan),
                            sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_bucket),
                            sp_distance_gps_payload: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_payload),
                            sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_ujipetik),
                            sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_timbangan),
                            sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_bucket),
                            distance_survei_payload: Number(rawData[dx].distance_survei),
                            distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                            distance_survei_timbangan: Number(rawData[dx].distance_survei),
                            distance_survei_bucket: Number(rawData[dx].distance_survei),
                            distance_gps_payload: Number(rawData[dx].distance_gps),
                            distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                            distance_gps_timbangan: Number(rawData[dx].distance_gps),
                            distance_gps_bucket: Number(rawData[dx].distance_gps),
                        });
                    }

                    if (haulerOperation.length > 0) {
                        haulerOperation[0].ritase += 1;
                        haulerOperation[0].bucket_count += Number(rawData[dx].bucket_count);
                        haulerOperation[0].tonnage_payload += bcmPayload;
                        haulerOperation[0].tonnage_ujipetik += bcmUjipetik;
                        haulerOperation[0].tonnage_timbangan += bcmTimbangan;
                        haulerOperation[0].tonnage_bucket += bcmBucket;
                        haulerOperation[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * bcmPayload;
                        haulerOperation[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * bcmUjipetik;
                        haulerOperation[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * bcmTimbangan;
                        haulerOperation[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * bcmBucket;
                        haulerOperation[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * bcmPayload;
                        haulerOperation[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * bcmUjipetik;
                        haulerOperation[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * bcmTimbangan;
                        haulerOperation[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * bcmBucket;
                        haulerOperation[0].distance_survei_payload = haulerOperation[0].sp_distance_survei_payload / haulerOperation[0].tonnage_payload;
                        haulerOperation[0].distance_survei_ujipetik = haulerOperation[0].sp_distance_survei_ujipetik / haulerOperation[0].tonnage_ujipetik;
                        haulerOperation[0].distance_survei_timbangan = haulerOperation[0].sp_distance_survei_timbangan / haulerOperation[0].tonnage_timbangan;
                        haulerOperation[0].distance_survei_bucket = haulerOperation[0].sp_distance_survei_bucket / haulerOperation[0].tonnage_bucket;
                        haulerOperation[0].distance_gps_payload = haulerOperation[0].sp_distance_gps_payload / haulerOperation[0].tonnage_payload;
                        haulerOperation[0].distance_gps_ujipetik = haulerOperation[0].sp_distance_gps_ujipetik / haulerOperation[0].tonnage_ujipetik;
                        haulerOperation[0].distance_gps_timbangan = haulerOperation[0].sp_distance_gps_timbangan / haulerOperation[0].tonnage_timbangan;
                        haulerOperation[0].distance_gps_bucket = haulerOperation[0].sp_distance_gps_bucket / haulerOperation[0].tonnage_bucket;
                    } else {
                        time[me]['hauler_operation'].push({
                            hauler: rawData[dx].hauler,
                            operation: rawData[dx].operation,
                            ritase: 1,
                            satuan: 'BCM',
                            bucket_count: Number(rawData[dx].bucket_count),
                            tonnage_payload: bcmPayload,
                            tonnage_ujipetik: bcmUjipetik,
                            tonnage_timbangan: bcmTimbangan,
                            tonnage_bucket: bcmBucket,
                            sp_distance_survei_payload: Number(rawData[dx].distance_survei) * bcmPayload,
                            sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * bcmUjipetik,
                            sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * bcmTimbangan,
                            sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * bcmBucket,
                            sp_distance_gps_payload: Number(rawData[dx].distance_gps) * bcmPayload,
                            sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * bcmUjipetik,
                            sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * bcmTimbangan,
                            sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * bcmBucket,
                            distance_survei_payload: Number(rawData[dx].distance_survei),
                            distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                            distance_survei_timbangan: Number(rawData[dx].distance_survei),
                            distance_survei_bucket: Number(rawData[dx].distance_survei),
                            distance_gps_payload: Number(rawData[dx].distance_gps),
                            distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                            distance_gps_timbangan: Number(rawData[dx].distance_gps),
                            distance_gps_bucket: Number(rawData[dx].distance_gps),
                        });
                    }

                    if (rawData[dx].material.type === util.MATERIAL_OB) {
                        if (haulerVolume.length > 0) {
                            haulerVolume[0].ritase += 1;
                            haulerVolume[0].bucket_count += Number(rawData[dx].bucket_count);
                            haulerVolume[0].tonnage_payload += bcmPayload;
                            haulerVolume[0].tonnage_ujipetik += bcmUjipetik;
                            haulerVolume[0].tonnage_timbangan += bcmTimbangan;
                            haulerVolume[0].tonnage_bucket += bcmBucket;
                            haulerVolume[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * bcmPayload;
                            haulerVolume[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * bcmUjipetik;
                            haulerVolume[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * bcmTimbangan;
                            haulerVolume[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * bcmBucket;
                            haulerVolume[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * bcmPayload;
                            haulerVolume[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * bcmUjipetik;
                            haulerVolume[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * bcmTimbangan;
                            haulerVolume[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * bcmBucket;
                            haulerVolume[0].distance_survei_payload = haulerVolume[0].sp_distance_survei_payload / haulerVolume[0].tonnage_payload;
                            haulerVolume[0].distance_survei_ujipetik = haulerVolume[0].sp_distance_survei_ujipetik / haulerVolume[0].tonnage_ujipetik;
                            haulerVolume[0].distance_survei_timbangan = haulerVolume[0].sp_distance_survei_timbangan / haulerVolume[0].tonnage_timbangan;
                            haulerVolume[0].distance_survei_bucket = haulerVolume[0].sp_distance_survei_bucket / haulerVolume[0].tonnage_bucket;
                            haulerVolume[0].distance_gps_payload = haulerVolume[0].sp_distance_gps_payload / haulerVolume[0].tonnage_payload;
                            haulerVolume[0].distance_gps_ujipetik = haulerVolume[0].sp_distance_gps_ujipetik / haulerVolume[0].tonnage_ujipetik;
                            haulerVolume[0].distance_gps_timbangan = haulerVolume[0].sp_distance_gps_timbangan / haulerVolume[0].tonnage_timbangan;
                            haulerVolume[0].distance_gps_bucket = haulerVolume[0].sp_distance_gps_bucket / haulerVolume[0].tonnage_bucket;
                        } else {
                            time[me]['hauler_volume'].push({
                                hauler: rawData[dx].hauler,
                                is_volume: rawData[dx].is_volume,
                                ritase: 1,
                                satuan: 'BCM',
                                bucket_count: Number(rawData[dx].bucket_count),
                                tonnage_payload: bcmPayload,
                                tonnage_ujipetik: bcmUjipetik,
                                tonnage_timbangan: bcmTimbangan,
                                tonnage_bucket: bcmBucket,
                                sp_distance_survei_payload: Number(rawData[dx].distance_survei) * bcmPayload,
                                sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * bcmUjipetik,
                                sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * bcmTimbangan,
                                sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * bcmBucket,
                                sp_distance_gps_payload: Number(rawData[dx].distance_gps) * bcmPayload,
                                sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * bcmUjipetik,
                                sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * bcmTimbangan,
                                sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * bcmBucket,
                                distance_survei_payload: Number(rawData[dx].distance_survei),
                                distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                                distance_survei_timbangan: Number(rawData[dx].distance_survei),
                                distance_survei_bucket: Number(rawData[dx].distance_survei),
                                distance_gps_payload: Number(rawData[dx].distance_gps),
                                distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                                distance_gps_timbangan: Number(rawData[dx].distance_gps),
                                distance_gps_bucket: Number(rawData[dx].distance_gps),
                            });
                        }
                    }

                    if (loaderMaterial.length > 0) {
                        loaderMaterial[0].ritase += 1;
                        loaderMaterial[0].bucket_count += Number(rawData[dx].bucket_count);
                        loaderMaterial[0].tonnage_payload += Number(rawData[dx].tonnage_payload);
                        loaderMaterial[0].tonnage_ujipetik += Number(rawData[dx].tonnage_ujipetik);
                        loaderMaterial[0].tonnage_timbangan += Number(rawData[dx].tonnage_timbangan);
                        loaderMaterial[0].tonnage_bucket += Number(rawData[dx].tonnage_bucket);
                        loaderMaterial[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_payload);
                        loaderMaterial[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_ujipetik);
                        loaderMaterial[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_timbangan);
                        loaderMaterial[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_bucket);
                        loaderMaterial[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_payload);
                        loaderMaterial[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_ujipetik);
                        loaderMaterial[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_timbangan);
                        loaderMaterial[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_bucket);
                        loaderMaterial[0].distance_survei_payload = Number(loaderMaterial[0].sp_distance_survei_payload) / Number(loaderMaterial[0].tonnage_payload);
                        loaderMaterial[0].distance_survei_ujipetik = Number(loaderMaterial[0].sp_distance_survei_ujipetik) / Number(loaderMaterial[0].tonnage_ujipetik);
                        loaderMaterial[0].distance_survei_timbangan = Number(loaderMaterial[0].sp_distance_survei_timbangan) / Number(loaderMaterial[0].tonnage_timbangan);
                        loaderMaterial[0].distance_survei_bucket = Number(loaderMaterial[0].sp_distance_survei_bucket) / Number(loaderMaterial[0].tonnage_bucket);
                        loaderMaterial[0].distance_gps_payload = Number(loaderMaterial[0].sp_distance_gps_payload) / Number(loaderMaterial[0].tonnage_payload);
                        loaderMaterial[0].distance_gps_ujipetik = Number(loaderMaterial[0].sp_distance_gps_ujipetik) / Number(loaderMaterial[0].tonnage_ujipetik);
                        loaderMaterial[0].distance_gps_timbangan = Number(loaderMaterial[0].sp_distance_gps_timbangan) / Number(loaderMaterial[0].tonnage_timbangan);
                        loaderMaterial[0].distance_gps_bucket = Number(loaderMaterial[0].sp_distance_gps_bucket) / Number(loaderMaterial[0].tonnage_bucket);
                    } else {
                        time[me]['loader_material'].push({
                            loader: rawData[dx].loader,
                            material: rawData[dx].material.type,
                            ritase: 1,
                            satuan: rawData[dx].material.type === util.MATERIAL_COAL ? 'Ton' : 'BCM',
                            bucket_count: Number(rawData[dx].bucket_count),
                            tonnage_payload: Number(rawData[dx].tonnage_payload),
                            tonnage_ujipetik: Number(rawData[dx].tonnage_ujipetik),
                            tonnage_timbangan: Number(rawData[dx].tonnage_timbangan),
                            tonnage_bucket: Number(rawData[dx].tonnage_bucket),
                            sp_distance_survei_payload: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_payload),
                            sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_ujipetik),
                            sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_timbangan),
                            sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_bucket),
                            sp_distance_gps_payload: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_payload),
                            sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_ujipetik),
                            sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_timbangan),
                            sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_bucket),
                            distance_survei_payload: Number(rawData[dx].distance_survei),
                            distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                            distance_survei_timbangan: Number(rawData[dx].distance_survei),
                            distance_survei_bucket: Number(rawData[dx].distance_survei),
                            distance_gps_payload: Number(rawData[dx].distance_gps),
                            distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                            distance_gps_timbangan: Number(rawData[dx].distance_gps),
                            distance_gps_bucket: Number(rawData[dx].distance_gps),
                        });
                    }

                    if (loaderOperation.length > 0) {
                        loaderOperation[0].ritase += 1;
                        loaderOperation[0].bucket_count += Number(rawData[dx].bucket_count);
                        loaderOperation[0].tonnage_payload += bcmPayload;
                        loaderOperation[0].tonnage_ujipetik += bcmUjipetik;
                        loaderOperation[0].tonnage_timbangan += bcmTimbangan;
                        loaderOperation[0].tonnage_bucket += bcmBucket;
                        loaderOperation[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * bcmPayload;
                        loaderOperation[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * bcmUjipetik;
                        loaderOperation[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * bcmTimbangan;
                        loaderOperation[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * bcmBucket;
                        loaderOperation[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * bcmPayload;
                        loaderOperation[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * bcmUjipetik;
                        loaderOperation[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * bcmTimbangan;
                        loaderOperation[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * bcmBucket;
                        loaderOperation[0].distance_survei_payload = loaderOperation[0].sp_distance_survei_payload / loaderOperation[0].tonnage_payload;
                        loaderOperation[0].distance_survei_ujipetik = loaderOperation[0].sp_distance_survei_ujipetik / loaderOperation[0].tonnage_ujipetik;
                        loaderOperation[0].distance_survei_timbangan = loaderOperation[0].sp_distance_survei_timbangan / loaderOperation[0].tonnage_timbangan;
                        loaderOperation[0].distance_survei_bucket = loaderOperation[0].sp_distance_survei_bucket / loaderOperation[0].tonnage_bucket;
                        loaderOperation[0].distance_gps_payload = loaderOperation[0].sp_distance_gps_payload / loaderOperation[0].tonnage_payload;
                        loaderOperation[0].distance_gps_ujipetik = loaderOperation[0].sp_distance_gps_ujipetik / loaderOperation[0].tonnage_ujipetik;
                        loaderOperation[0].distance_gps_timbangan = loaderOperation[0].sp_distance_gps_timbangan / loaderOperation[0].tonnage_timbangan;
                        loaderOperation[0].distance_gps_bucket = loaderOperation[0].sp_distance_gps_bucket / loaderOperation[0].tonnage_bucket;
                    } else {
                        time[me]['loader_operation'].push({
                            loader: rawData[dx].loader,
                            operation: rawData[dx].operation,
                            ritase: 1,
                            satuan: 'BCM',
                            bucket_count: Number(rawData[dx].bucket_count),
                            tonnage_payload: bcmPayload,
                            tonnage_ujipetik: bcmUjipetik,
                            tonnage_timbangan: bcmTimbangan,
                            tonnage_bucket: bcmBucket,
                            sp_distance_survei_payload: Number(rawData[dx].distance_survei) * bcmPayload,
                            sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * bcmUjipetik,
                            sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * bcmTimbangan,
                            sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * bcmBucket,
                            sp_distance_gps_payload: Number(rawData[dx].distance_gps) * bcmPayload,
                            sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * bcmUjipetik,
                            sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * bcmTimbangan,
                            sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * bcmBucket,
                            distance_survei_payload: Number(rawData[dx].distance_survei),
                            distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                            distance_survei_timbangan: Number(rawData[dx].distance_survei),
                            distance_survei_bucket: Number(rawData[dx].distance_survei),
                            distance_gps_payload: Number(rawData[dx].distance_gps),
                            distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                            distance_gps_timbangan: Number(rawData[dx].distance_gps),
                            distance_gps_bucket: Number(rawData[dx].distance_gps),
                        });
                    }

                    if (rawData[dx].material.type === util.MATERIAL_OB) {
                        if (loaderVolume.length > 0) {
                            loaderVolume[0].ritase += 1;
                            loaderVolume[0].bucket_count += Number(rawData[dx].bucket_count);
                            loaderVolume[0].tonnage_payload += bcmPayload;
                            loaderVolume[0].tonnage_ujipetik += bcmUjipetik;
                            loaderVolume[0].tonnage_timbangan += bcmTimbangan;
                            loaderVolume[0].tonnage_bucket += bcmBucket;
                            loaderVolume[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * bcmPayload;
                            loaderVolume[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * bcmUjipetik;
                            loaderVolume[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * bcmTimbangan;
                            loaderVolume[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * bcmBucket;
                            loaderVolume[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * bcmPayload;
                            loaderVolume[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * bcmUjipetik;
                            loaderVolume[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * bcmTimbangan;
                            loaderVolume[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * bcmBucket;
                            loaderVolume[0].distance_survei_payload = loaderVolume[0].sp_distance_survei_payload / loaderVolume[0].tonnage_payload;
                            loaderVolume[0].distance_survei_ujipetik = loaderVolume[0].sp_distance_survei_ujipetik / loaderVolume[0].tonnage_ujipetik;
                            loaderVolume[0].distance_survei_timbangan = loaderVolume[0].sp_distance_survei_timbangan / loaderVolume[0].tonnage_timbangan;
                            loaderVolume[0].distance_survei_bucket = loaderVolume[0].sp_distance_survei_bucket / loaderVolume[0].tonnage_bucket;
                            loaderVolume[0].distance_gps_payload = loaderVolume[0].sp_distance_gps_payload / loaderVolume[0].tonnage_payload;
                            loaderVolume[0].distance_gps_ujipetik = loaderVolume[0].sp_distance_gps_ujipetik / loaderVolume[0].tonnage_ujipetik;
                            loaderVolume[0].distance_gps_timbangan = loaderVolume[0].sp_distance_gps_timbangan / loaderVolume[0].tonnage_timbangan;
                            loaderVolume[0].distance_gps_bucket = loaderVolume[0].sp_distance_gps_bucket / loaderVolume[0].tonnage_bucket;
                        } else {
                            time[me]['loader_volume'].push({
                                loader: rawData[dx].loader,
                                is_volume: rawData[dx].is_volume,
                                ritase: 1,
                                satuan: 'BCM',
                                bucket_count: Number(rawData[dx].bucket_count),
                                tonnage_payload: bcmPayload,
                                tonnage_ujipetik: bcmUjipetik,
                                tonnage_timbangan: bcmTimbangan,
                                tonnage_bucket: bcmBucket,
                                sp_distance_survei_payload: Number(rawData[dx].distance_survei) * bcmPayload,
                                sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * bcmUjipetik,
                                sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * bcmTimbangan,
                                sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * bcmBucket,
                                sp_distance_gps_payload: Number(rawData[dx].distance_gps) * bcmPayload,
                                sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * bcmUjipetik,
                                sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * bcmTimbangan,
                                sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * bcmBucket,
                                distance_survei_payload: Number(rawData[dx].distance_survei),
                                distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                                distance_survei_timbangan: Number(rawData[dx].distance_survei),
                                distance_survei_bucket: Number(rawData[dx].distance_survei),
                                distance_gps_payload: Number(rawData[dx].distance_gps),
                                distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                                distance_gps_timbangan: Number(rawData[dx].distance_gps),
                                distance_gps_bucket: Number(rawData[dx].distance_gps),
                            });
                        }
                    }

                    if (operatorHaulerMaterial.length > 0) {
                        operatorHaulerMaterial[0].ritase += 1;
                        operatorHaulerMaterial[0].bucket_count += Number(rawData[dx].bucket_count);
                        operatorHaulerMaterial[0].tonnage_payload += Number(rawData[dx].tonnage_payload);
                        operatorHaulerMaterial[0].tonnage_ujipetik += Number(rawData[dx].tonnage_ujipetik);
                        operatorHaulerMaterial[0].tonnage_timbangan += Number(rawData[dx].tonnage_timbangan);
                        operatorHaulerMaterial[0].tonnage_bucket += Number(rawData[dx].tonnage_bucket);
                        operatorHaulerMaterial[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_payload);
                        operatorHaulerMaterial[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_ujipetik);
                        operatorHaulerMaterial[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_timbangan);
                        operatorHaulerMaterial[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_bucket);
                        operatorHaulerMaterial[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_payload);
                        operatorHaulerMaterial[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_ujipetik);
                        operatorHaulerMaterial[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_timbangan);
                        operatorHaulerMaterial[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_bucket);
                        operatorHaulerMaterial[0].distance_survei_payload = Number(operatorHaulerMaterial[0].sp_distance_survei_payload) / Number(operatorHaulerMaterial[0].tonnage_payload);
                        operatorHaulerMaterial[0].distance_survei_ujipetik = Number(operatorHaulerMaterial[0].sp_distance_survei_ujipetik) / Number(operatorHaulerMaterial[0].tonnage_ujipetik);
                        operatorHaulerMaterial[0].distance_survei_timbangan = Number(operatorHaulerMaterial[0].sp_distance_survei_timbangan) / Number(operatorHaulerMaterial[0].tonnage_timbangan);
                        operatorHaulerMaterial[0].distance_survei_bucket = Number(operatorHaulerMaterial[0].sp_distance_survei_bucket) / Number(operatorHaulerMaterial[0].tonnage_bucket);
                        operatorHaulerMaterial[0].distance_gps_payload = Number(operatorHaulerMaterial[0].sp_distance_gps_payload) / Number(operatorHaulerMaterial[0].tonnage_payload);
                        operatorHaulerMaterial[0].distance_gps_ujipetik = Number(operatorHaulerMaterial[0].sp_distance_gps_ujipetik) / Number(operatorHaulerMaterial[0].tonnage_ujipetik);
                        operatorHaulerMaterial[0].distance_gps_timbangan = Number(operatorHaulerMaterial[0].sp_distance_gps_timbangan) / Number(operatorHaulerMaterial[0].tonnage_timbangan);
                        operatorHaulerMaterial[0].distance_gps_bucket = Number(operatorHaulerMaterial[0].sp_distance_gps_bucket) / Number(operatorHaulerMaterial[0].tonnage_bucket);
                    } else {
                        time[me]['operator_hauler_material'].push({
                            operator_hauler: rawData[dx].operator_hauler,
                            material: rawData[dx].material.type,
                            ritase: 1,
                            satuan: rawData[dx].material.type === util.MATERIAL_COAL ? 'Ton' : 'BCM',
                            bucket_count: Number(rawData[dx].bucket_count),
                            tonnage_payload: Number(rawData[dx].tonnage_payload),
                            tonnage_ujipetik: Number(rawData[dx].tonnage_ujipetik),
                            tonnage_timbangan: Number(rawData[dx].tonnage_timbangan),
                            tonnage_bucket: Number(rawData[dx].tonnage_bucket),
                            sp_distance_survei_payload: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_payload),
                            sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_ujipetik),
                            sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_timbangan),
                            sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_bucket),
                            sp_distance_gps_payload: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_payload),
                            sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_ujipetik),
                            sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_timbangan),
                            sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_bucket),
                            distance_survei_payload: Number(rawData[dx].distance_survei),
                            distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                            distance_survei_timbangan: Number(rawData[dx].distance_survei),
                            distance_survei_bucket: Number(rawData[dx].distance_survei),
                            distance_gps_payload: Number(rawData[dx].distance_gps),
                            distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                            distance_gps_timbangan: Number(rawData[dx].distance_gps),
                            distance_gps_bucket: Number(rawData[dx].distance_gps),
                        });
                    }

                    if (operatorHaulerOperation.length > 0) {
                        operatorHaulerOperation[0].ritase += 1;
                        operatorHaulerOperation[0].bucket_count += Number(rawData[dx].bucket_count);
                        operatorHaulerOperation[0].tonnage_payload += bcmPayload;
                        operatorHaulerOperation[0].tonnage_ujipetik += bcmUjipetik;
                        operatorHaulerOperation[0].tonnage_timbangan += bcmTimbangan;
                        operatorHaulerOperation[0].tonnage_bucket += bcmBucket;
                        operatorHaulerOperation[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * bcmPayload;
                        operatorHaulerOperation[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * bcmUjipetik;
                        operatorHaulerOperation[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * bcmTimbangan;
                        operatorHaulerOperation[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * bcmBucket;
                        operatorHaulerOperation[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * bcmPayload;
                        operatorHaulerOperation[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * bcmUjipetik;
                        operatorHaulerOperation[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * bcmTimbangan;
                        operatorHaulerOperation[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * bcmBucket;
                        operatorHaulerOperation[0].distance_survei_payload = operatorHaulerOperation[0].sp_distance_survei_payload / operatorHaulerOperation[0].tonnage_payload;
                        operatorHaulerOperation[0].distance_survei_ujipetik = operatorHaulerOperation[0].sp_distance_survei_ujipetik / operatorHaulerOperation[0].tonnage_ujipetik;
                        operatorHaulerOperation[0].distance_survei_timbangan = operatorHaulerOperation[0].sp_distance_survei_timbangan / operatorHaulerOperation[0].tonnage_timbangan;
                        operatorHaulerOperation[0].distance_survei_bucket = operatorHaulerOperation[0].sp_distance_survei_bucket / operatorHaulerOperation[0].tonnage_bucket;
                        operatorHaulerOperation[0].distance_gps_payload = operatorHaulerOperation[0].sp_distance_gps_payload / operatorHaulerOperation[0].tonnage_payload;
                        operatorHaulerOperation[0].distance_gps_ujipetik = operatorHaulerOperation[0].sp_distance_gps_ujipetik / operatorHaulerOperation[0].tonnage_ujipetik;
                        operatorHaulerOperation[0].distance_gps_timbangan = operatorHaulerOperation[0].sp_distance_gps_timbangan / operatorHaulerOperation[0].tonnage_timbangan;
                        operatorHaulerOperation[0].distance_gps_bucket = operatorHaulerOperation[0].sp_distance_gps_bucket / operatorHaulerOperation[0].tonnage_bucket;
                    } else {
                        time[me]['operator_hauler_operation'].push({
                            operator_hauler: rawData[dx].operator_hauler,
                            operation: rawData[dx].operation,
                            ritase: 1,
                            satuan: 'BCM',
                            bucket_count: Number(rawData[dx].bucket_count),
                            tonnage_payload: bcmPayload,
                            tonnage_ujipetik: bcmUjipetik,
                            tonnage_timbangan: bcmTimbangan,
                            tonnage_bucket: bcmBucket,
                            sp_distance_survei_payload: Number(rawData[dx].distance_survei) * bcmPayload,
                            sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * bcmUjipetik,
                            sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * bcmTimbangan,
                            sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * bcmBucket,
                            sp_distance_gps_payload: Number(rawData[dx].distance_gps) * bcmPayload,
                            sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * bcmUjipetik,
                            sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * bcmTimbangan,
                            sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * bcmBucket,
                            distance_survei_payload: Number(rawData[dx].distance_survei),
                            distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                            distance_survei_timbangan: Number(rawData[dx].distance_survei),
                            distance_survei_bucket: Number(rawData[dx].distance_survei),
                            distance_gps_payload: Number(rawData[dx].distance_gps),
                            distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                            distance_gps_timbangan: Number(rawData[dx].distance_gps),
                            distance_gps_bucket: Number(rawData[dx].distance_gps),
                        });
                    }

                    if (rawData[dx].material.type === util.MATERIAL_OB) {
                        if (operatorHaulerVolume.length > 0) {
                            operatorHaulerVolume[0].ritase += 1;
                            operatorHaulerVolume[0].bucket_count += Number(rawData[dx].bucket_count);
                            operatorHaulerVolume[0].tonnage_payload += bcmPayload;
                            operatorHaulerVolume[0].tonnage_ujipetik += bcmUjipetik;
                            operatorHaulerVolume[0].tonnage_timbangan += bcmTimbangan;
                            operatorHaulerVolume[0].tonnage_bucket += bcmBucket;
                            operatorHaulerVolume[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * bcmPayload;
                            operatorHaulerVolume[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * bcmUjipetik;
                            operatorHaulerVolume[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * bcmTimbangan;
                            operatorHaulerVolume[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * bcmBucket;
                            operatorHaulerVolume[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * bcmPayload;
                            operatorHaulerVolume[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * bcmUjipetik;
                            operatorHaulerVolume[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * bcmTimbangan;
                            operatorHaulerVolume[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * bcmBucket;
                            operatorHaulerVolume[0].distance_survei_payload = operatorHaulerVolume[0].sp_distance_survei_payload / operatorHaulerVolume[0].tonnage_payload;
                            operatorHaulerVolume[0].distance_survei_ujipetik = operatorHaulerVolume[0].sp_distance_survei_ujipetik / operatorHaulerVolume[0].tonnage_ujipetik;
                            operatorHaulerVolume[0].distance_survei_timbangan = operatorHaulerVolume[0].sp_distance_survei_timbangan / operatorHaulerVolume[0].tonnage_timbangan;
                            operatorHaulerVolume[0].distance_survei_bucket = operatorHaulerVolume[0].sp_distance_survei_bucket / operatorHaulerVolume[0].tonnage_bucket;
                            operatorHaulerVolume[0].distance_gps_payload = operatorHaulerVolume[0].sp_distance_gps_payload / operatorHaulerVolume[0].tonnage_payload;
                            operatorHaulerVolume[0].distance_gps_ujipetik = operatorHaulerVolume[0].sp_distance_gps_ujipetik / operatorHaulerVolume[0].tonnage_ujipetik;
                            operatorHaulerVolume[0].distance_gps_timbangan = operatorHaulerVolume[0].sp_distance_gps_timbangan / operatorHaulerVolume[0].tonnage_timbangan;
                            operatorHaulerVolume[0].distance_gps_bucket = operatorHaulerVolume[0].sp_distance_gps_bucket / operatorHaulerVolume[0].tonnage_bucket;
                        } else {
                            time[me]['operator_hauler_volume'].push({
                                operator_hauler: rawData[dx].operator_hauler,
                                is_volume: rawData[dx].is_volume,
                                ritase: 1,
                                satuan: 'BCM',
                                bucket_count: Number(rawData[dx].bucket_count),
                                tonnage_payload: bcmPayload,
                                tonnage_ujipetik: bcmUjipetik,
                                tonnage_timbangan: bcmTimbangan,
                                tonnage_bucket: bcmBucket,
                                sp_distance_survei_payload: Number(rawData[dx].distance_survei) * bcmPayload,
                                sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * bcmUjipetik,
                                sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * bcmTimbangan,
                                sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * bcmBucket,
                                sp_distance_gps_payload: Number(rawData[dx].distance_gps) * bcmPayload,
                                sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * bcmUjipetik,
                                sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * bcmTimbangan,
                                sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * bcmBucket,
                                distance_survei_payload: Number(rawData[dx].distance_survei),
                                distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                                distance_survei_timbangan: Number(rawData[dx].distance_survei),
                                distance_survei_bucket: Number(rawData[dx].distance_survei),
                                distance_gps_payload: Number(rawData[dx].distance_gps),
                                distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                                distance_gps_timbangan: Number(rawData[dx].distance_gps),
                                distance_gps_bucket: Number(rawData[dx].distance_gps),
                            });
                        }
                    }

                    if (operatorLoaderMaterial.length > 0) {
                        operatorLoaderMaterial[0].ritase += 1;
                        operatorLoaderMaterial[0].bucket_count += Number(rawData[dx].bucket_count);
                        operatorLoaderMaterial[0].tonnage_payload += Number(rawData[dx].tonnage_payload);
                        operatorLoaderMaterial[0].tonnage_ujipetik += Number(rawData[dx].tonnage_ujipetik);
                        operatorLoaderMaterial[0].tonnage_timbangan += Number(rawData[dx].tonnage_timbangan);
                        operatorLoaderMaterial[0].tonnage_bucket += Number(rawData[dx].tonnage_bucket);
                        operatorLoaderMaterial[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_payload);
                        operatorLoaderMaterial[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_ujipetik);
                        operatorLoaderMaterial[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_timbangan);
                        operatorLoaderMaterial[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_bucket);
                        operatorLoaderMaterial[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_payload);
                        operatorLoaderMaterial[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_ujipetik);
                        operatorLoaderMaterial[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_timbangan);
                        operatorLoaderMaterial[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_bucket);
                        operatorLoaderMaterial[0].distance_survei_payload = Number(operatorLoaderMaterial[0].sp_distance_survei_payload) / Number(operatorLoaderMaterial[0].tonnage_payload);
                        operatorLoaderMaterial[0].distance_survei_ujipetik = Number(operatorLoaderMaterial[0].sp_distance_survei_ujipetik) / Number(operatorLoaderMaterial[0].tonnage_ujipetik);
                        operatorLoaderMaterial[0].distance_survei_timbangan = Number(operatorLoaderMaterial[0].sp_distance_survei_timbangan) / Number(operatorLoaderMaterial[0].tonnage_timbangan);
                        operatorLoaderMaterial[0].distance_survei_bucket = Number(operatorLoaderMaterial[0].sp_distance_survei_bucket) / Number(operatorLoaderMaterial[0].tonnage_bucket);
                        operatorLoaderMaterial[0].distance_gps_payload = Number(operatorLoaderMaterial[0].sp_distance_gps_payload) / Number(operatorLoaderMaterial[0].tonnage_payload);
                        operatorLoaderMaterial[0].distance_gps_ujipetik = Number(operatorLoaderMaterial[0].sp_distance_gps_ujipetik) / Number(operatorLoaderMaterial[0].tonnage_ujipetik);
                        operatorLoaderMaterial[0].distance_gps_timbangan = Number(operatorLoaderMaterial[0].sp_distance_gps_timbangan) / Number(operatorLoaderMaterial[0].tonnage_timbangan);
                        operatorLoaderMaterial[0].distance_gps_bucket = Number(operatorLoaderMaterial[0].sp_distance_gps_bucket) / Number(operatorLoaderMaterial[0].tonnage_bucket);
                    } else {
                        time[me]['operator_loader_material'].push({
                            operator_loader: rawData[dx].operator_loader,
                            material: rawData[dx].material.type,
                            ritase: 1,
                            satuan: rawData[dx].material.type === util.MATERIAL_COAL ? 'Ton' : 'BCM',
                            bucket_count: Number(rawData[dx].bucket_count),
                            tonnage_payload: Number(rawData[dx].tonnage_payload),
                            tonnage_ujipetik: Number(rawData[dx].tonnage_ujipetik),
                            tonnage_timbangan: Number(rawData[dx].tonnage_timbangan),
                            tonnage_bucket: Number(rawData[dx].tonnage_bucket),
                            sp_distance_survei_payload: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_payload),
                            sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_ujipetik),
                            sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_timbangan),
                            sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_bucket),
                            sp_distance_gps_payload: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_payload),
                            sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_ujipetik),
                            sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_timbangan),
                            sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_bucket),
                            distance_survei_payload: Number(rawData[dx].distance_survei),
                            distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                            distance_survei_timbangan: Number(rawData[dx].distance_survei),
                            distance_survei_bucket: Number(rawData[dx].distance_survei),
                            distance_gps_payload: Number(rawData[dx].distance_gps),
                            distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                            distance_gps_timbangan: Number(rawData[dx].distance_gps),
                            distance_gps_bucket: Number(rawData[dx].distance_gps),
                        });
                    }

                    if (operatorLoaderOperation.length > 0) {
                        operatorLoaderOperation[0].ritase += 1;
                        operatorLoaderOperation[0].bucket_count += Number(rawData[dx].bucket_count);
                        operatorLoaderOperation[0].tonnage_payload += bcmPayload;
                        operatorLoaderOperation[0].tonnage_ujipetik += bcmUjipetik;
                        operatorLoaderOperation[0].tonnage_timbangan += bcmTimbangan;
                        operatorLoaderOperation[0].tonnage_bucket += bcmBucket;
                        operatorLoaderOperation[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * bcmPayload;
                        operatorLoaderOperation[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * bcmUjipetik;
                        operatorLoaderOperation[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * bcmTimbangan;
                        operatorLoaderOperation[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * bcmBucket;
                        operatorLoaderOperation[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * bcmPayload;
                        operatorLoaderOperation[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * bcmUjipetik;
                        operatorLoaderOperation[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * bcmTimbangan;
                        operatorLoaderOperation[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * bcmBucket;
                        operatorLoaderOperation[0].distance_survei_payload = operatorLoaderOperation[0].sp_distance_survei_payload / operatorLoaderOperation[0].tonnage_payload;
                        operatorLoaderOperation[0].distance_survei_ujipetik = operatorLoaderOperation[0].sp_distance_survei_ujipetik / operatorLoaderOperation[0].tonnage_ujipetik;
                        operatorLoaderOperation[0].distance_survei_timbangan = operatorLoaderOperation[0].sp_distance_survei_timbangan / operatorLoaderOperation[0].tonnage_timbangan;
                        operatorLoaderOperation[0].distance_survei_bucket = operatorLoaderOperation[0].sp_distance_survei_bucket / operatorLoaderOperation[0].tonnage_bucket;
                        operatorLoaderOperation[0].distance_gps_payload = operatorLoaderOperation[0].sp_distance_gps_payload / operatorLoaderOperation[0].tonnage_payload;
                        operatorLoaderOperation[0].distance_gps_ujipetik = operatorLoaderOperation[0].sp_distance_gps_ujipetik / operatorLoaderOperation[0].tonnage_ujipetik;
                        operatorLoaderOperation[0].distance_gps_timbangan = operatorLoaderOperation[0].sp_distance_gps_timbangan / operatorLoaderOperation[0].tonnage_timbangan;
                        operatorLoaderOperation[0].distance_gps_bucket = operatorLoaderOperation[0].sp_distance_gps_bucket / operatorLoaderOperation[0].tonnage_bucket;
                    } else {
                        time[me]['operator_loader_operation'].push({
                            operator_loader: rawData[dx].operator_loader,
                            operation: rawData[dx].operation,
                            ritase: 1,
                            satuan: 'BCM',
                            bucket_count: Number(rawData[dx].bucket_count),
                            tonnage_payload: bcmPayload,
                            tonnage_ujipetik: bcmUjipetik,
                            tonnage_timbangan: bcmTimbangan,
                            tonnage_bucket: bcmBucket,
                            sp_distance_survei_payload: Number(rawData[dx].distance_survei) * bcmPayload,
                            sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * bcmUjipetik,
                            sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * bcmTimbangan,
                            sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * bcmBucket,
                            sp_distance_gps_payload: Number(rawData[dx].distance_gps) * bcmPayload,
                            sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * bcmUjipetik,
                            sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * bcmTimbangan,
                            sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * bcmBucket,
                            distance_survei_payload: Number(rawData[dx].distance_survei),
                            distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                            distance_survei_timbangan: Number(rawData[dx].distance_survei),
                            distance_survei_bucket: Number(rawData[dx].distance_survei),
                            distance_gps_payload: Number(rawData[dx].distance_gps),
                            distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                            distance_gps_timbangan: Number(rawData[dx].distance_gps),
                            distance_gps_bucket: Number(rawData[dx].distance_gps),
                        });
                    }

                    if (rawData[dx].material.type === util.MATERIAL_OB) {
                        if (operatorLoaderVolume.length > 0) {
                            operatorLoaderVolume[0].ritase += 1;
                            operatorLoaderVolume[0].bucket_count += Number(rawData[dx].bucket_count);
                            operatorLoaderVolume[0].tonnage_payload += bcmPayload;
                            operatorLoaderVolume[0].tonnage_ujipetik += bcmUjipetik;
                            operatorLoaderVolume[0].tonnage_timbangan += bcmTimbangan;
                            operatorLoaderVolume[0].tonnage_bucket += bcmBucket;
                            operatorLoaderVolume[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * bcmPayload;
                            operatorLoaderVolume[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * bcmUjipetik;
                            operatorLoaderVolume[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * bcmTimbangan;
                            operatorLoaderVolume[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * bcmBucket;
                            operatorLoaderVolume[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * bcmPayload;
                            operatorLoaderVolume[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * bcmUjipetik;
                            operatorLoaderVolume[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * bcmTimbangan;
                            operatorLoaderVolume[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * bcmBucket;
                            operatorLoaderVolume[0].distance_survei_payload = operatorLoaderVolume[0].sp_distance_survei_payload / operatorLoaderVolume[0].tonnage_payload;
                            operatorLoaderVolume[0].distance_survei_ujipetik = operatorLoaderVolume[0].sp_distance_survei_ujipetik / operatorLoaderVolume[0].tonnage_ujipetik;
                            operatorLoaderVolume[0].distance_survei_timbangan = operatorLoaderVolume[0].sp_distance_survei_timbangan / operatorLoaderVolume[0].tonnage_timbangan;
                            operatorLoaderVolume[0].distance_survei_bucket = operatorLoaderVolume[0].sp_distance_survei_bucket / operatorLoaderVolume[0].tonnage_bucket;
                            operatorLoaderVolume[0].distance_gps_payload = operatorLoaderVolume[0].sp_distance_gps_payload / operatorLoaderVolume[0].tonnage_payload;
                            operatorLoaderVolume[0].distance_gps_ujipetik = operatorLoaderVolume[0].sp_distance_gps_ujipetik / operatorLoaderVolume[0].tonnage_ujipetik;
                            operatorLoaderVolume[0].distance_gps_timbangan = operatorLoaderVolume[0].sp_distance_gps_timbangan / operatorLoaderVolume[0].tonnage_timbangan;
                            operatorLoaderVolume[0].distance_gps_bucket = operatorLoaderVolume[0].sp_distance_gps_bucket / operatorLoaderVolume[0].tonnage_bucket;
                        } else {
                            time[me]['operator_loader_volume'].push({
                                operator_loader: rawData[dx].operator_loader,
                                is_volume: rawData[dx].is_volume,
                                ritase: 1,
                                satuan: 'BCM',
                                bucket_count: Number(rawData[dx].bucket_count),
                                tonnage_payload: bcmPayload,
                                tonnage_ujipetik: bcmUjipetik,
                                tonnage_timbangan: bcmTimbangan,
                                tonnage_bucket: bcmBucket,
                                sp_distance_survei_payload: Number(rawData[dx].distance_survei) * bcmPayload,
                                sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * bcmUjipetik,
                                sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * bcmTimbangan,
                                sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * bcmBucket,
                                sp_distance_gps_payload: Number(rawData[dx].distance_gps) * bcmPayload,
                                sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * bcmUjipetik,
                                sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * bcmTimbangan,
                                sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * bcmBucket,
                                distance_survei_payload: Number(rawData[dx].distance_survei),
                                distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                                distance_survei_timbangan: Number(rawData[dx].distance_survei),
                                distance_survei_bucket: Number(rawData[dx].distance_survei),
                                distance_gps_payload: Number(rawData[dx].distance_gps),
                                distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                                distance_gps_timbangan: Number(rawData[dx].distance_gps),
                                distance_gps_bucket: Number(rawData[dx].distance_gps),
                            });
                        }
                    }

                    if (locationMaterial.length > 0) {
                        locationMaterial[0].ritase += 1;
                        locationMaterial[0].bucket_count += Number(rawData[dx].bucket_count);
                        locationMaterial[0].tonnage_payload += Number(rawData[dx].tonnage_payload);
                        locationMaterial[0].tonnage_ujipetik += Number(rawData[dx].tonnage_ujipetik);
                        locationMaterial[0].tonnage_timbangan += Number(rawData[dx].tonnage_timbangan);
                        locationMaterial[0].tonnage_bucket += Number(rawData[dx].tonnage_bucket);
                        locationMaterial[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_payload);
                        locationMaterial[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_ujipetik);
                        locationMaterial[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_timbangan);
                        locationMaterial[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_bucket);
                        locationMaterial[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_payload);
                        locationMaterial[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_ujipetik);
                        locationMaterial[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_timbangan);
                        locationMaterial[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_bucket);
                        locationMaterial[0].distance_survei_payload = Number(locationMaterial[0].sp_distance_survei_payload) / Number(locationMaterial[0].tonnage_payload);
                        locationMaterial[0].distance_survei_ujipetik = Number(locationMaterial[0].sp_distance_survei_ujipetik) / Number(locationMaterial[0].tonnage_ujipetik);
                        locationMaterial[0].distance_survei_timbangan = Number(locationMaterial[0].sp_distance_survei_timbangan) / Number(locationMaterial[0].tonnage_timbangan);
                        locationMaterial[0].distance_survei_bucket = Number(locationMaterial[0].sp_distance_survei_bucket) / Number(locationMaterial[0].tonnage_bucket);
                        locationMaterial[0].distance_gps_payload = Number(locationMaterial[0].sp_distance_gps_payload) / Number(locationMaterial[0].tonnage_payload);
                        locationMaterial[0].distance_gps_ujipetik = Number(locationMaterial[0].sp_distance_gps_ujipetik) / Number(locationMaterial[0].tonnage_ujipetik);
                        locationMaterial[0].distance_gps_timbangan = Number(locationMaterial[0].sp_distance_gps_timbangan) / Number(locationMaterial[0].tonnage_timbangan);
                        locationMaterial[0].distance_gps_bucket = Number(locationMaterial[0].sp_distance_gps_bucket) / Number(locationMaterial[0].tonnage_bucket);
                    } else {
                        time[me]['location_material'].push({
                            location: rawData[dx].location,
                            material: rawData[dx].material.type,
                            ritase: 1,
                            satuan: rawData[dx].material.type === util.MATERIAL_COAL ? 'Ton' : 'BCM',
                            bucket_count: Number(rawData[dx].bucket_count),
                            tonnage_payload: Number(rawData[dx].tonnage_payload),
                            tonnage_ujipetik: Number(rawData[dx].tonnage_ujipetik),
                            tonnage_timbangan: Number(rawData[dx].tonnage_timbangan),
                            tonnage_bucket: Number(rawData[dx].tonnage_bucket),
                            sp_distance_survei_payload: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_payload),
                            sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_ujipetik),
                            sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_timbangan),
                            sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_bucket),
                            sp_distance_gps_payload: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_payload),
                            sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_ujipetik),
                            sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_timbangan),
                            sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_bucket),
                            distance_survei_payload: Number(rawData[dx].distance_survei),
                            distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                            distance_survei_timbangan: Number(rawData[dx].distance_survei),
                            distance_survei_bucket: Number(rawData[dx].distance_survei),
                            distance_gps_payload: Number(rawData[dx].distance_gps),
                            distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                            distance_gps_timbangan: Number(rawData[dx].distance_gps),
                            distance_gps_bucket: Number(rawData[dx].distance_gps),
                        });
                    }

                    if (locationOperation.length > 0) {
                        locationOperation[0].ritase += 1;
                        locationOperation[0].bucket_count += Number(rawData[dx].bucket_count);
                        locationOperation[0].tonnage_payload += bcmPayload;
                        locationOperation[0].tonnage_ujipetik += bcmUjipetik;
                        locationOperation[0].tonnage_timbangan += bcmTimbangan;
                        locationOperation[0].tonnage_bucket += bcmBucket;
                        locationOperation[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * bcmPayload;
                        locationOperation[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * bcmUjipetik;
                        locationOperation[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * bcmTimbangan;
                        locationOperation[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * bcmBucket;
                        locationOperation[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * bcmPayload;
                        locationOperation[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * bcmUjipetik;
                        locationOperation[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * bcmTimbangan;
                        locationOperation[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * bcmBucket;
                        locationOperation[0].distance_survei_payload = locationOperation[0].sp_distance_survei_payload / locationOperation[0].tonnage_payload;
                        locationOperation[0].distance_survei_ujipetik = locationOperation[0].sp_distance_survei_ujipetik / locationOperation[0].tonnage_ujipetik;
                        locationOperation[0].distance_survei_timbangan = locationOperation[0].sp_distance_survei_timbangan / locationOperation[0].tonnage_timbangan;
                        locationOperation[0].distance_survei_bucket = locationOperation[0].sp_distance_survei_bucket / locationOperation[0].tonnage_bucket;
                        locationOperation[0].distance_gps_payload = locationOperation[0].sp_distance_gps_payload / locationOperation[0].tonnage_payload;
                        locationOperation[0].distance_gps_ujipetik = locationOperation[0].sp_distance_gps_ujipetik / locationOperation[0].tonnage_ujipetik;
                        locationOperation[0].distance_gps_timbangan = locationOperation[0].sp_distance_gps_timbangan / locationOperation[0].tonnage_timbangan;
                        locationOperation[0].distance_gps_bucket = locationOperation[0].sp_distance_gps_bucket / locationOperation[0].tonnage_bucket;
                    } else {
                        time[me]['location_operation'].push({
                            location: rawData[dx].location,
                            operation: rawData[dx].operation,
                            ritase: 1,
                            satuan: 'BCM',
                            bucket_count: Number(rawData[dx].bucket_count),
                            tonnage_payload: bcmPayload,
                            tonnage_ujipetik: bcmUjipetik,
                            tonnage_timbangan: bcmTimbangan,
                            tonnage_bucket: bcmBucket,
                            sp_distance_survei_payload: Number(rawData[dx].distance_survei) * bcmPayload,
                            sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * bcmUjipetik,
                            sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * bcmTimbangan,
                            sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * bcmBucket,
                            sp_distance_gps_payload: Number(rawData[dx].distance_gps) * bcmPayload,
                            sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * bcmUjipetik,
                            sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * bcmTimbangan,
                            sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * bcmBucket,
                            distance_survei_payload: Number(rawData[dx].distance_survei),
                            distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                            distance_survei_timbangan: Number(rawData[dx].distance_survei),
                            distance_survei_bucket: Number(rawData[dx].distance_survei),
                            distance_gps_payload: Number(rawData[dx].distance_gps),
                            distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                            distance_gps_timbangan: Number(rawData[dx].distance_gps),
                            distance_gps_bucket: Number(rawData[dx].distance_gps),
                        });
                    }

                    if (rawData[dx].material.type === util.MATERIAL_OB) {
                        if (locationVolume.length > 0) {
                            locationVolume[0].ritase += 1;
                            locationVolume[0].bucket_count += Number(rawData[dx].bucket_count);
                            locationVolume[0].tonnage_payload += bcmPayload;
                            locationVolume[0].tonnage_ujipetik += bcmUjipetik;
                            locationVolume[0].tonnage_timbangan += bcmTimbangan;
                            locationVolume[0].tonnage_bucket += bcmBucket;
                            locationVolume[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * bcmPayload;
                            locationVolume[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * bcmUjipetik;
                            locationVolume[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * bcmTimbangan;
                            locationVolume[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * bcmBucket;
                            locationVolume[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * bcmPayload;
                            locationVolume[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * bcmUjipetik;
                            locationVolume[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * bcmTimbangan;
                            locationVolume[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * bcmBucket;
                            locationVolume[0].distance_survei_payload = locationVolume[0].sp_distance_survei_payload / locationVolume[0].tonnage_payload;
                            locationVolume[0].distance_survei_ujipetik = locationVolume[0].sp_distance_survei_ujipetik / locationVolume[0].tonnage_ujipetik;
                            locationVolume[0].distance_survei_timbangan = locationVolume[0].sp_distance_survei_timbangan / locationVolume[0].tonnage_timbangan;
                            locationVolume[0].distance_survei_bucket = locationVolume[0].sp_distance_survei_bucket / locationVolume[0].tonnage_bucket;
                            locationVolume[0].distance_gps_payload = locationVolume[0].sp_distance_gps_payload / locationVolume[0].tonnage_payload;
                            locationVolume[0].distance_gps_ujipetik = locationVolume[0].sp_distance_gps_ujipetik / locationVolume[0].tonnage_ujipetik;
                            locationVolume[0].distance_gps_timbangan = locationVolume[0].sp_distance_gps_timbangan / locationVolume[0].tonnage_timbangan;
                            locationVolume[0].distance_gps_bucket = locationVolume[0].sp_distance_gps_bucket / locationVolume[0].tonnage_bucket;
                        } else {
                            time[me]['location_volume'].push({
                                location: rawData[dx].location,
                                is_volume: rawData[dx].is_volume,
                                ritase: 1,
                                satuan: 'BCM',
                                bucket_count: Number(rawData[dx].bucket_count),
                                tonnage_payload: bcmPayload,
                                tonnage_ujipetik: bcmUjipetik,
                                tonnage_timbangan: bcmTimbangan,
                                tonnage_bucket: bcmBucket,
                                sp_distance_survei_payload: Number(rawData[dx].distance_survei) * bcmPayload,
                                sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * bcmUjipetik,
                                sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * bcmTimbangan,
                                sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * bcmBucket,
                                sp_distance_gps_payload: Number(rawData[dx].distance_gps) * bcmPayload,
                                sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * bcmUjipetik,
                                sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * bcmTimbangan,
                                sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * bcmBucket,
                                distance_survei_payload: Number(rawData[dx].distance_survei),
                                distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                                distance_survei_timbangan: Number(rawData[dx].distance_survei),
                                distance_survei_bucket: Number(rawData[dx].distance_survei),
                                distance_gps_payload: Number(rawData[dx].distance_gps),
                                distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                                distance_gps_timbangan: Number(rawData[dx].distance_gps),
                                distance_gps_bucket: Number(rawData[dx].distance_gps),
                            });
                        }
                    }

                    if (areaMaterial.length > 0) {
                        areaMaterial[0].ritase += 1;
                        areaMaterial[0].bucket_count += Number(rawData[dx].bucket_count);
                        areaMaterial[0].tonnage_payload += Number(rawData[dx].tonnage_payload);
                        areaMaterial[0].tonnage_ujipetik += Number(rawData[dx].tonnage_ujipetik);
                        areaMaterial[0].tonnage_timbangan += Number(rawData[dx].tonnage_timbangan);
                        areaMaterial[0].tonnage_bucket += Number(rawData[dx].tonnage_bucket);
                        areaMaterial[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_payload);
                        areaMaterial[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_ujipetik);
                        areaMaterial[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_timbangan);
                        areaMaterial[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_bucket);
                        areaMaterial[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_payload);
                        areaMaterial[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_ujipetik);
                        areaMaterial[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_timbangan);
                        areaMaterial[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_bucket);
                        areaMaterial[0].distance_survei_payload = Number(areaMaterial[0].sp_distance_survei_payload) / Number(areaMaterial[0].tonnage_payload);
                        areaMaterial[0].distance_survei_ujipetik = Number(areaMaterial[0].sp_distance_survei_ujipetik) / Number(areaMaterial[0].tonnage_ujipetik);
                        areaMaterial[0].distance_survei_timbangan = Number(areaMaterial[0].sp_distance_survei_timbangan) / Number(areaMaterial[0].tonnage_timbangan);
                        areaMaterial[0].distance_survei_bucket = Number(areaMaterial[0].sp_distance_survei_bucket) / Number(areaMaterial[0].tonnage_bucket);
                        areaMaterial[0].distance_gps_payload = Number(areaMaterial[0].sp_distance_gps_payload) / Number(areaMaterial[0].tonnage_payload);
                        areaMaterial[0].distance_gps_ujipetik = Number(areaMaterial[0].sp_distance_gps_ujipetik) / Number(areaMaterial[0].tonnage_ujipetik);
                        areaMaterial[0].distance_gps_timbangan = Number(areaMaterial[0].sp_distance_gps_timbangan) / Number(areaMaterial[0].tonnage_timbangan);
                        areaMaterial[0].distance_gps_bucket = Number(areaMaterial[0].sp_distance_gps_bucket) / Number(areaMaterial[0].tonnage_bucket);
                    } else {
                        time[me]['area_material'].push({
                            area: rawData[dx].area,
                            material: rawData[dx].material.type,
                            ritase: 1,
                            satuan: rawData[dx].material.type === util.MATERIAL_COAL ? 'Ton' : 'BCM',
                            bucket_count: Number(rawData[dx].bucket_count),
                            tonnage_payload: Number(rawData[dx].tonnage_payload),
                            tonnage_ujipetik: Number(rawData[dx].tonnage_ujipetik),
                            tonnage_timbangan: Number(rawData[dx].tonnage_timbangan),
                            tonnage_bucket: Number(rawData[dx].tonnage_bucket),
                            sp_distance_survei_payload: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_payload),
                            sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_ujipetik),
                            sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_timbangan),
                            sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_bucket),
                            sp_distance_gps_payload: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_payload),
                            sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_ujipetik),
                            sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_timbangan),
                            sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_bucket),
                            distance_survei_payload: Number(rawData[dx].distance_survei),
                            distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                            distance_survei_timbangan: Number(rawData[dx].distance_survei),
                            distance_survei_bucket: Number(rawData[dx].distance_survei),
                            distance_gps_payload: Number(rawData[dx].distance_gps),
                            distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                            distance_gps_timbangan: Number(rawData[dx].distance_gps),
                            distance_gps_bucket: Number(rawData[dx].distance_gps),
                        });
                    }

                    if (areaOperation.length > 0) {
                        areaOperation[0].ritase += 1;
                        areaOperation[0].bucket_count += Number(rawData[dx].bucket_count);
                        areaOperation[0].tonnage_payload += bcmPayload;
                        areaOperation[0].tonnage_ujipetik += bcmUjipetik;
                        areaOperation[0].tonnage_timbangan += bcmTimbangan;
                        areaOperation[0].tonnage_bucket += bcmBucket;
                        areaOperation[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * bcmPayload;
                        areaOperation[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * bcmUjipetik;
                        areaOperation[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * bcmTimbangan;
                        areaOperation[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * bcmBucket;
                        areaOperation[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * bcmPayload;
                        areaOperation[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * bcmUjipetik;
                        areaOperation[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * bcmTimbangan;
                        areaOperation[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * bcmBucket;
                        areaOperation[0].distance_survei_payload = areaOperation[0].sp_distance_survei_payload / areaOperation[0].tonnage_payload;
                        areaOperation[0].distance_survei_ujipetik = areaOperation[0].sp_distance_survei_ujipetik / areaOperation[0].tonnage_ujipetik;
                        areaOperation[0].distance_survei_timbangan = areaOperation[0].sp_distance_survei_timbangan / areaOperation[0].tonnage_timbangan;
                        areaOperation[0].distance_survei_bucket = areaOperation[0].sp_distance_survei_bucket / areaOperation[0].tonnage_bucket;
                        areaOperation[0].distance_gps_payload = areaOperation[0].sp_distance_gps_payload / areaOperation[0].tonnage_payload;
                        areaOperation[0].distance_gps_ujipetik = areaOperation[0].sp_distance_gps_ujipetik / areaOperation[0].tonnage_ujipetik;
                        areaOperation[0].distance_gps_timbangan = areaOperation[0].sp_distance_gps_timbangan / areaOperation[0].tonnage_timbangan;
                        areaOperation[0].distance_gps_bucket = areaOperation[0].sp_distance_gps_bucket / areaOperation[0].tonnage_bucket;
                    } else {
                        time[me]['area_operation'].push({
                            area: rawData[dx].area,
                            operation: rawData[dx].operation,
                            ritase: 1,
                            satuan: 'BCM',
                            bucket_count: Number(rawData[dx].bucket_count),
                            tonnage_payload: bcmPayload,
                            tonnage_ujipetik: bcmUjipetik,
                            tonnage_timbangan: bcmTimbangan,
                            tonnage_bucket: bcmBucket,
                            sp_distance_survei_payload: Number(rawData[dx].distance_survei) * bcmPayload,
                            sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * bcmUjipetik,
                            sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * bcmTimbangan,
                            sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * bcmBucket,
                            sp_distance_gps_payload: Number(rawData[dx].distance_gps) * bcmPayload,
                            sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * bcmUjipetik,
                            sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * bcmTimbangan,
                            sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * bcmBucket,
                            distance_survei_payload: Number(rawData[dx].distance_survei),
                            distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                            distance_survei_timbangan: Number(rawData[dx].distance_survei),
                            distance_survei_bucket: Number(rawData[dx].distance_survei),
                            distance_gps_payload: Number(rawData[dx].distance_gps),
                            distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                            distance_gps_timbangan: Number(rawData[dx].distance_gps),
                            distance_gps_bucket: Number(rawData[dx].distance_gps),
                        });
                    }

                    if (rawData[dx].material.type === util.MATERIAL_OB) {
                        if (areaVolume.length > 0) {
                            areaVolume[0].ritase += 1;
                            areaVolume[0].bucket_count += Number(rawData[dx].bucket_count);
                            areaVolume[0].tonnage_payload += bcmPayload;
                            areaVolume[0].tonnage_ujipetik += bcmUjipetik;
                            areaVolume[0].tonnage_timbangan += bcmTimbangan;
                            areaVolume[0].tonnage_bucket += bcmBucket;
                            areaVolume[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * bcmPayload;
                            areaVolume[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * bcmUjipetik;
                            areaVolume[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * bcmTimbangan;
                            areaVolume[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * bcmBucket;
                            areaVolume[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * bcmPayload;
                            areaVolume[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * bcmUjipetik;
                            areaVolume[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * bcmTimbangan;
                            areaVolume[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * bcmBucket;
                            areaVolume[0].distance_survei_payload = areaVolume[0].sp_distance_survei_payload / areaVolume[0].tonnage_payload;
                            areaVolume[0].distance_survei_ujipetik = areaVolume[0].sp_distance_survei_ujipetik / areaVolume[0].tonnage_ujipetik;
                            areaVolume[0].distance_survei_timbangan = areaVolume[0].sp_distance_survei_timbangan / areaVolume[0].tonnage_timbangan;
                            areaVolume[0].distance_survei_bucket = areaVolume[0].sp_distance_survei_bucket / areaVolume[0].tonnage_bucket;
                            areaVolume[0].distance_gps_payload = areaVolume[0].sp_distance_gps_payload / areaVolume[0].tonnage_payload;
                            areaVolume[0].distance_gps_ujipetik = areaVolume[0].sp_distance_gps_ujipetik / areaVolume[0].tonnage_ujipetik;
                            areaVolume[0].distance_gps_timbangan = areaVolume[0].sp_distance_gps_timbangan / areaVolume[0].tonnage_timbangan;
                            areaVolume[0].distance_gps_bucket = areaVolume[0].sp_distance_gps_bucket / areaVolume[0].tonnage_bucket;
                        } else {
                            time[me]['area_volume'].push({
                                area: rawData[dx].area,
                                is_volume: rawData[dx].is_volume,
                                ritase: 1,
                                satuan: 'BCM',
                                bucket_count: Number(rawData[dx].bucket_count),
                                tonnage_payload: bcmPayload,
                                tonnage_ujipetik: bcmUjipetik,
                                tonnage_timbangan: bcmTimbangan,
                                tonnage_bucket: bcmBucket,
                                sp_distance_survei_payload: Number(rawData[dx].distance_survei) * bcmPayload,
                                sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * bcmUjipetik,
                                sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * bcmTimbangan,
                                sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * bcmBucket,
                                sp_distance_gps_payload: Number(rawData[dx].distance_gps) * bcmPayload,
                                sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * bcmUjipetik,
                                sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * bcmTimbangan,
                                sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * bcmBucket,
                                distance_survei_payload: Number(rawData[dx].distance_survei),
                                distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                                distance_survei_timbangan: Number(rawData[dx].distance_survei),
                                distance_survei_bucket: Number(rawData[dx].distance_survei),
                                distance_gps_payload: Number(rawData[dx].distance_gps),
                                distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                                distance_gps_timbangan: Number(rawData[dx].distance_gps),
                                distance_gps_bucket: Number(rawData[dx].distance_gps),
                            });
                        }
                    }

                    if (hauler.length > 0) {
                        hauler[0].ritase += 1;
                        hauler[0].bucket_count += Number(rawData[dx].bucket_count);
                        hauler[0].tonnage_payload += bcmPayload;
                        hauler[0].tonnage_ujipetik += bcmUjipetik;
                        hauler[0].tonnage_timbangan += bcmTimbangan;
                        hauler[0].tonnage_bucket += bcmBucket;
                        hauler[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * bcmPayload;
                        hauler[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * bcmUjipetik;
                        hauler[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * bcmTimbangan;
                        hauler[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * bcmBucket;
                        hauler[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * bcmPayload;
                        hauler[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * bcmUjipetik;
                        hauler[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * bcmTimbangan;
                        hauler[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * bcmBucket;
                        hauler[0].distance_survei_payload = hauler[0].sp_distance_survei_payload / hauler[0].tonnage_payload;
                        hauler[0].distance_survei_ujipetik = hauler[0].sp_distance_survei_ujipetik / hauler[0].tonnage_ujipetik;
                        hauler[0].distance_survei_timbangan = hauler[0].sp_distance_survei_timbangan / hauler[0].tonnage_timbangan;
                        hauler[0].distance_survei_bucket = hauler[0].sp_distance_survei_bucket / hauler[0].tonnage_bucket;
                        hauler[0].distance_gps_payload = hauler[0].sp_distance_gps_payload / hauler[0].tonnage_payload;
                        hauler[0].distance_gps_ujipetik = hauler[0].sp_distance_gps_ujipetik / hauler[0].tonnage_ujipetik;
                        hauler[0].distance_gps_timbangan = hauler[0].sp_distance_gps_timbangan / hauler[0].tonnage_timbangan;
                        hauler[0].distance_gps_bucket = hauler[0].sp_distance_gps_bucket / hauler[0].tonnage_bucket;
                    } else {
                        time[me]['hauler'].push({
                            hauler: rawData[dx].hauler,
                            ritase: 1,
                            satuan: 'BCM',
                            bucket_count: Number(rawData[dx].bucket_count),
                            tonnage_payload: bcmPayload,
                            tonnage_ujipetik: bcmUjipetik,
                            tonnage_timbangan: bcmTimbangan,
                            tonnage_bucket: bcmBucket,
                            sp_distance_survei_payload: Number(rawData[dx].distance_survei) * bcmPayload,
                            sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * bcmUjipetik,
                            sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * bcmTimbangan,
                            sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * bcmBucket,
                            sp_distance_gps_payload: Number(rawData[dx].distance_gps) * bcmPayload,
                            sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * bcmUjipetik,
                            sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * bcmTimbangan,
                            sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * bcmBucket,
                            distance_survei_payload: Number(rawData[dx].distance_survei),
                            distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                            distance_survei_timbangan: Number(rawData[dx].distance_survei),
                            distance_survei_bucket: Number(rawData[dx].distance_survei),
                            distance_gps_payload: Number(rawData[dx].distance_gps),
                            distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                            distance_gps_timbangan: Number(rawData[dx].distance_gps),
                            distance_gps_bucket: Number(rawData[dx].distance_gps),
                        });
                    }

                    if (loader.length > 0) {
                        loader[0].ritase += 1;
                        loader[0].bucket_count += Number(rawData[dx].bucket_count);
                        loader[0].tonnage_payload += bcmPayload;
                        loader[0].tonnage_ujipetik += bcmUjipetik;
                        loader[0].tonnage_timbangan += bcmTimbangan;
                        loader[0].tonnage_bucket += bcmBucket;
                        loader[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * bcmPayload;
                        loader[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * bcmUjipetik;
                        loader[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * bcmTimbangan;
                        loader[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * bcmBucket;
                        loader[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * bcmPayload;
                        loader[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * bcmUjipetik;
                        loader[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * bcmTimbangan;
                        loader[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * bcmBucket;
                        loader[0].distance_survei_payload = loader[0].sp_distance_survei_payload / loader[0].tonnage_payload;
                        loader[0].distance_survei_ujipetik = loader[0].sp_distance_survei_ujipetik / loader[0].tonnage_ujipetik;
                        loader[0].distance_survei_timbangan = loader[0].sp_distance_survei_timbangan / loader[0].tonnage_timbangan;
                        loader[0].distance_survei_bucket = loader[0].sp_distance_survei_bucket / loader[0].tonnage_bucket;
                        loader[0].distance_gps_payload = loader[0].sp_distance_gps_payload / loader[0].tonnage_payload;
                        loader[0].distance_gps_ujipetik = loader[0].sp_distance_gps_ujipetik / loader[0].tonnage_ujipetik;
                        loader[0].distance_gps_timbangan = loader[0].sp_distance_gps_timbangan / loader[0].tonnage_timbangan;
                        loader[0].distance_gps_bucket = loader[0].sp_distance_gps_bucket / loader[0].tonnage_bucket;
                    } else {
                        time[me]['loader'].push({
                            loader: rawData[dx].loader,
                            ritase: 1,
                            satuan: 'BCM',
                            bucket_count: Number(rawData[dx].bucket_count),
                            tonnage_payload: bcmPayload,
                            tonnage_ujipetik: bcmUjipetik,
                            tonnage_timbangan: bcmTimbangan,
                            tonnage_bucket: bcmBucket,
                            sp_distance_survei_payload: Number(rawData[dx].distance_survei) * bcmPayload,
                            sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * bcmUjipetik,
                            sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * bcmTimbangan,
                            sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * bcmBucket,
                            sp_distance_gps_payload: Number(rawData[dx].distance_gps) * bcmPayload,
                            sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * bcmUjipetik,
                            sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * bcmTimbangan,
                            sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * bcmBucket,
                            distance_survei_payload: Number(rawData[dx].distance_survei),
                            distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                            distance_survei_timbangan: Number(rawData[dx].distance_survei),
                            distance_survei_bucket: Number(rawData[dx].distance_survei),
                            distance_gps_payload: Number(rawData[dx].distance_gps),
                            distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                            distance_gps_timbangan: Number(rawData[dx].distance_gps),
                            distance_gps_bucket: Number(rawData[dx].distance_gps),
                        });
                    }

                    if (operatorHauler.length > 0) {
                        operatorHauler[0].ritase += 1;
                        operatorHauler[0].bucket_count += Number(rawData[dx].bucket_count);
                        operatorHauler[0].tonnage_payload += bcmPayload;
                        operatorHauler[0].tonnage_ujipetik += bcmUjipetik;
                        operatorHauler[0].tonnage_timbangan += bcmTimbangan;
                        operatorHauler[0].tonnage_bucket += bcmBucket;
                        operatorHauler[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * bcmPayload;
                        operatorHauler[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * bcmUjipetik;
                        operatorHauler[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * bcmTimbangan;
                        operatorHauler[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * bcmBucket;
                        operatorHauler[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * bcmPayload;
                        operatorHauler[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * bcmUjipetik;
                        operatorHauler[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * bcmTimbangan;
                        operatorHauler[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * bcmBucket;
                        operatorHauler[0].distance_survei_payload = operatorHauler[0].sp_distance_survei_payload / operatorHauler[0].tonnage_payload;
                        operatorHauler[0].distance_survei_ujipetik = operatorHauler[0].sp_distance_survei_ujipetik / operatorHauler[0].tonnage_ujipetik;
                        operatorHauler[0].distance_survei_timbangan = operatorHauler[0].sp_distance_survei_timbangan / operatorHauler[0].tonnage_timbangan;
                        operatorHauler[0].distance_survei_bucket = operatorHauler[0].sp_distance_survei_bucket / operatorHauler[0].tonnage_bucket;
                        operatorHauler[0].distance_gps_payload = operatorHauler[0].sp_distance_gps_payload / operatorHauler[0].tonnage_payload;
                        operatorHauler[0].distance_gps_ujipetik = operatorHauler[0].sp_distance_gps_ujipetik / operatorHauler[0].tonnage_ujipetik;
                        operatorHauler[0].distance_gps_timbangan = operatorHauler[0].sp_distance_gps_timbangan / operatorHauler[0].tonnage_timbangan;
                        operatorHauler[0].distance_gps_bucket = operatorHauler[0].sp_distance_gps_bucket / operatorHauler[0].tonnage_bucket;
                    } else {
                        time[me]['operator_hauler'].push({
                            operator_hauler: rawData[dx].operator_hauler,
                            ritase: 1,
                            satuan: 'BCM',
                            bucket_count: Number(rawData[dx].bucket_count),
                            tonnage_payload: bcmPayload,
                            tonnage_ujipetik: bcmUjipetik,
                            tonnage_timbangan: bcmTimbangan,
                            tonnage_bucket: bcmBucket,
                            sp_distance_survei_payload: Number(rawData[dx].distance_survei) * bcmPayload,
                            sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * bcmUjipetik,
                            sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * bcmTimbangan,
                            sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * bcmBucket,
                            sp_distance_gps_payload: Number(rawData[dx].distance_gps) * bcmPayload,
                            sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * bcmUjipetik,
                            sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * bcmTimbangan,
                            sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * bcmBucket,
                            distance_survei_payload: Number(rawData[dx].distance_survei),
                            distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                            distance_survei_timbangan: Number(rawData[dx].distance_survei),
                            distance_survei_bucket: Number(rawData[dx].distance_survei),
                            distance_gps_payload: Number(rawData[dx].distance_gps),
                            distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                            distance_gps_timbangan: Number(rawData[dx].distance_gps),
                            distance_gps_bucket: Number(rawData[dx].distance_gps),
                        });
                    }

                    if (operatorLoader.length > 0) {
                        operatorLoader[0].ritase += 1;
                        operatorLoader[0].bucket_count += Number(rawData[dx].bucket_count);
                        operatorLoader[0].tonnage_payload += bcmPayload;
                        operatorLoader[0].tonnage_ujipetik += bcmUjipetik;
                        operatorLoader[0].tonnage_timbangan += bcmTimbangan;
                        operatorLoader[0].tonnage_bucket += bcmBucket;
                        operatorLoader[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * bcmPayload;
                        operatorLoader[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * bcmUjipetik;
                        operatorLoader[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * bcmTimbangan;
                        operatorLoader[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * bcmBucket;
                        operatorLoader[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * bcmPayload;
                        operatorLoader[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * bcmUjipetik;
                        operatorLoader[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * bcmTimbangan;
                        operatorLoader[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * bcmBucket;
                        operatorLoader[0].distance_survei_payload = operatorLoader[0].sp_distance_survei_payload / operatorLoader[0].tonnage_payload;
                        operatorLoader[0].distance_survei_ujipetik = operatorLoader[0].sp_distance_survei_ujipetik / operatorLoader[0].tonnage_ujipetik;
                        operatorLoader[0].distance_survei_timbangan = operatorLoader[0].sp_distance_survei_timbangan / operatorLoader[0].tonnage_timbangan;
                        operatorLoader[0].distance_survei_bucket = operatorLoader[0].sp_distance_survei_bucket / operatorLoader[0].tonnage_bucket;
                        operatorLoader[0].distance_gps_payload = operatorLoader[0].sp_distance_gps_payload / operatorLoader[0].tonnage_payload;
                        operatorLoader[0].distance_gps_ujipetik = operatorLoader[0].sp_distance_gps_ujipetik / operatorLoader[0].tonnage_ujipetik;
                        operatorLoader[0].distance_gps_timbangan = operatorLoader[0].sp_distance_gps_timbangan / operatorLoader[0].tonnage_timbangan;
                        operatorLoader[0].distance_gps_bucket = operatorLoader[0].sp_distance_gps_bucket / operatorLoader[0].tonnage_bucket;
                    } else {
                        time[me]['operator_loader'].push({
                            operator_loader: rawData[dx].operator_loader,
                            ritase: 1,
                            satuan: 'BCM',
                            bucket_count: Number(rawData[dx].bucket_count),
                            tonnage_payload: bcmPayload,
                            tonnage_ujipetik: bcmUjipetik,
                            tonnage_timbangan: bcmTimbangan,
                            tonnage_bucket: bcmBucket,
                            sp_distance_survei_payload: Number(rawData[dx].distance_survei) * bcmPayload,
                            sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * bcmUjipetik,
                            sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * bcmTimbangan,
                            sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * bcmBucket,
                            sp_distance_gps_payload: Number(rawData[dx].distance_gps) * bcmPayload,
                            sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * bcmUjipetik,
                            sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * bcmTimbangan,
                            sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * bcmBucket,
                            distance_survei_payload: Number(rawData[dx].distance_survei),
                            distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                            distance_survei_timbangan: Number(rawData[dx].distance_survei),
                            distance_survei_bucket: Number(rawData[dx].distance_survei),
                            distance_gps_payload: Number(rawData[dx].distance_gps),
                            distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                            distance_gps_timbangan: Number(rawData[dx].distance_gps),
                            distance_gps_bucket: Number(rawData[dx].distance_gps),
                        });
                    }

                    if (location.length > 0) {
                        location[0].ritase += 1;
                        location[0].bucket_count += Number(rawData[dx].bucket_count);
                        location[0].tonnage_payload += bcmPayload;
                        location[0].tonnage_ujipetik += bcmUjipetik;
                        location[0].tonnage_timbangan += bcmTimbangan;
                        location[0].tonnage_bucket += bcmBucket;
                        location[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * bcmPayload;
                        location[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * bcmUjipetik;
                        location[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * bcmTimbangan;
                        location[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * bcmBucket;
                        location[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * bcmPayload;
                        location[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * bcmUjipetik;
                        location[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * bcmTimbangan;
                        location[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * bcmBucket;
                        location[0].distance_survei_payload = location[0].sp_distance_survei_payload / location[0].tonnage_payload;
                        location[0].distance_survei_ujipetik = location[0].sp_distance_survei_ujipetik / location[0].tonnage_ujipetik;
                        location[0].distance_survei_timbangan = location[0].sp_distance_survei_timbangan / location[0].tonnage_timbangan;
                        location[0].distance_survei_bucket = location[0].sp_distance_survei_bucket / location[0].tonnage_bucket;
                        location[0].distance_gps_payload = location[0].sp_distance_gps_payload / location[0].tonnage_payload;
                        location[0].distance_gps_ujipetik = location[0].sp_distance_gps_ujipetik / location[0].tonnage_ujipetik;
                        location[0].distance_gps_timbangan = location[0].sp_distance_gps_timbangan / location[0].tonnage_timbangan;
                        location[0].distance_gps_bucket = location[0].sp_distance_gps_bucket / location[0].tonnage_bucket;
                    } else {
                        time[me]['location'].push({
                            location: rawData[dx].location,
                            ritase: 1,
                            satuan: 'BCM',
                            bucket_count: Number(rawData[dx].bucket_count),
                            tonnage_payload: bcmPayload,
                            tonnage_ujipetik: bcmUjipetik,
                            tonnage_timbangan: bcmTimbangan,
                            tonnage_bucket: bcmBucket,
                            sp_distance_survei_payload: Number(rawData[dx].distance_survei) * bcmPayload,
                            sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * bcmUjipetik,
                            sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * bcmTimbangan,
                            sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * bcmBucket,
                            sp_distance_gps_payload: Number(rawData[dx].distance_gps) * bcmPayload,
                            sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * bcmUjipetik,
                            sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * bcmTimbangan,
                            sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * bcmBucket,
                            distance_survei_payload: Number(rawData[dx].distance_survei),
                            distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                            distance_survei_timbangan: Number(rawData[dx].distance_survei),
                            distance_survei_bucket: Number(rawData[dx].distance_survei),
                            distance_gps_payload: Number(rawData[dx].distance_gps),
                            distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                            distance_gps_timbangan: Number(rawData[dx].distance_gps),
                            distance_gps_bucket: Number(rawData[dx].distance_gps),
                        });
                    }

                    if (area.length > 0) {
                        area[0].ritase += 1;
                        area[0].bucket_count += Number(rawData[dx].bucket_count);
                        area[0].tonnage_payload += bcmPayload;
                        area[0].tonnage_ujipetik += bcmUjipetik;
                        area[0].tonnage_timbangan += bcmTimbangan;
                        area[0].tonnage_bucket += bcmBucket;
                        area[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * bcmPayload;
                        area[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * bcmUjipetik;
                        area[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * bcmTimbangan;
                        area[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * bcmBucket;
                        area[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * bcmPayload;
                        area[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * bcmUjipetik;
                        area[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * bcmTimbangan;
                        area[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * bcmBucket;
                        area[0].distance_survei_payload = area[0].sp_distance_survei_payload / area[0].tonnage_payload;
                        area[0].distance_survei_ujipetik = area[0].sp_distance_survei_ujipetik / area[0].tonnage_ujipetik;
                        area[0].distance_survei_timbangan = area[0].sp_distance_survei_timbangan / area[0].tonnage_timbangan;
                        area[0].distance_survei_bucket = area[0].sp_distance_survei_bucket / area[0].tonnage_bucket;
                        area[0].distance_gps_payload = area[0].sp_distance_gps_payload / area[0].tonnage_payload;
                        area[0].distance_gps_ujipetik = area[0].sp_distance_gps_ujipetik / area[0].tonnage_ujipetik;
                        area[0].distance_gps_timbangan = area[0].sp_distance_gps_timbangan / area[0].tonnage_timbangan;
                        area[0].distance_gps_bucket = area[0].sp_distance_gps_bucket / area[0].tonnage_bucket;
                    } else {
                        time[me]['area'].push({
                            area: rawData[dx].area,
                            ritase: 1,
                            satuan: 'BCM',
                            bucket_count: Number(rawData[dx].bucket_count),
                            tonnage_payload: bcmPayload,
                            tonnage_ujipetik: bcmUjipetik,
                            tonnage_timbangan: bcmTimbangan,
                            tonnage_bucket: bcmBucket,
                            sp_distance_survei_payload: Number(rawData[dx].distance_survei) * bcmPayload,
                            sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * bcmUjipetik,
                            sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * bcmTimbangan,
                            sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * bcmBucket,
                            sp_distance_gps_payload: Number(rawData[dx].distance_gps) * bcmPayload,
                            sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * bcmUjipetik,
                            sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * bcmTimbangan,
                            sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * bcmBucket,
                            distance_survei_payload: Number(rawData[dx].distance_survei),
                            distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                            distance_survei_timbangan: Number(rawData[dx].distance_survei),
                            distance_survei_bucket: Number(rawData[dx].distance_survei),
                            distance_gps_payload: Number(rawData[dx].distance_gps),
                            distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                            distance_gps_timbangan: Number(rawData[dx].distance_gps),
                            distance_gps_bucket: Number(rawData[dx].distance_gps),
                        });
                    }

                    if (material.length > 0) {
                        material[0].ritase += 1;
                        material[0].bucket_count += Number(rawData[dx].bucket_count);
                        material[0].tonnage_payload += Number(rawData[dx].tonnage_payload);
                        material[0].tonnage_ujipetik += Number(rawData[dx].tonnage_ujipetik);
                        material[0].tonnage_timbangan += Number(rawData[dx].tonnage_timbangan);
                        material[0].tonnage_bucket += Number(rawData[dx].tonnage_bucket);
                        material[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_payload);
                        material[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_ujipetik);
                        material[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_timbangan);
                        material[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_bucket);
                        material[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_payload);
                        material[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_ujipetik);
                        material[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_timbangan);
                        material[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_bucket);
                        material[0].distance_survei_payload = Number(material[0].sp_distance_survei_payload) / Number(material[0].tonnage_payload);
                        material[0].distance_survei_ujipetik = Number(material[0].sp_distance_survei_ujipetik) / Number(material[0].tonnage_ujipetik);
                        material[0].distance_survei_timbangan = Number(material[0].sp_distance_survei_timbangan) / Number(material[0].tonnage_timbangan);
                        material[0].distance_survei_bucket = Number(material[0].sp_distance_survei_bucket) / Number(material[0].tonnage_bucket);
                        material[0].distance_gps_payload = Number(material[0].sp_distance_gps_payload) / Number(material[0].tonnage_payload);
                        material[0].distance_gps_ujipetik = Number(material[0].sp_distance_gps_ujipetik) / Number(material[0].tonnage_ujipetik);
                        material[0].distance_gps_timbangan = Number(material[0].sp_distance_gps_timbangan) / Number(material[0].tonnage_timbangan);
                        material[0].distance_gps_bucket = Number(material[0].sp_distance_gps_bucket) / Number(material[0].tonnage_bucket);
                    } else {
                        time[me]['material'].push({
                            material: rawData[dx].material.type,
                            ritase: 1,
                            satuan: rawData[dx].material.type === util.MATERIAL_COAL ? 'Ton' : 'BCM',
                            bucket_count: Number(rawData[dx].bucket_count),
                            tonnage_payload: Number(rawData[dx].tonnage_payload),
                            tonnage_ujipetik: Number(rawData[dx].tonnage_ujipetik),
                            tonnage_timbangan: Number(rawData[dx].tonnage_timbangan),
                            tonnage_bucket: Number(rawData[dx].tonnage_bucket),
                            sp_distance_survei_payload: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_payload),
                            sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_ujipetik),
                            sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_timbangan),
                            sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * Number(rawData[dx].tonnage_bucket),
                            sp_distance_gps_payload: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_payload),
                            sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_ujipetik),
                            sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_timbangan),
                            sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * Number(rawData[dx].tonnage_bucket),
                            distance_survei_payload: Number(rawData[dx].distance_survei),
                            distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                            distance_survei_timbangan: Number(rawData[dx].distance_survei),
                            distance_survei_bucket: Number(rawData[dx].distance_survei),
                            distance_gps_payload: Number(rawData[dx].distance_gps),
                            distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                            distance_gps_timbangan: Number(rawData[dx].distance_gps),
                            distance_gps_bucket: Number(rawData[dx].distance_gps),
                        });
                    }

                    if (operation.length > 0) {
                        operation[0].ritase += 1;
                        operation[0].bucket_count += Number(rawData[dx].bucket_count);
                        operation[0].tonnage_payload += bcmPayload;
                        operation[0].tonnage_ujipetik += bcmUjipetik;
                        operation[0].tonnage_timbangan += bcmTimbangan;
                        operation[0].tonnage_bucket += bcmBucket;
                        operation[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * bcmPayload;
                        operation[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * bcmUjipetik;
                        operation[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * bcmTimbangan;
                        operation[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * bcmBucket;
                        operation[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * bcmPayload;
                        operation[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * bcmUjipetik;
                        operation[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * bcmTimbangan;
                        operation[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * bcmBucket;
                        operation[0].distance_survei_payload = operation[0].sp_distance_survei_payload / operation[0].tonnage_payload;
                        operation[0].distance_survei_ujipetik = operation[0].sp_distance_survei_ujipetik / operation[0].tonnage_ujipetik;
                        operation[0].distance_survei_timbangan = operation[0].sp_distance_survei_timbangan / operation[0].tonnage_timbangan;
                        operation[0].distance_survei_bucket = operation[0].sp_distance_survei_bucket / operation[0].tonnage_bucket;
                        operation[0].distance_gps_payload = operation[0].sp_distance_gps_payload / operation[0].tonnage_payload;
                        operation[0].distance_gps_ujipetik = operation[0].sp_distance_gps_ujipetik / operation[0].tonnage_ujipetik;
                        operation[0].distance_gps_timbangan = operation[0].sp_distance_gps_timbangan / operation[0].tonnage_timbangan;
                        operation[0].distance_gps_bucket = operation[0].sp_distance_gps_bucket / operation[0].tonnage_bucket;
                    } else {
                        time[me]['operation'].push({
                            operation: rawData[dx].operation,
                            ritase: 1,
                            satuan: 'BCM',
                            bucket_count: Number(rawData[dx].bucket_count),
                            tonnage_payload: bcmPayload,
                            tonnage_ujipetik: bcmUjipetik,
                            tonnage_timbangan: bcmTimbangan,
                            tonnage_bucket: bcmBucket,
                            sp_distance_survei_payload: Number(rawData[dx].distance_survei) * bcmPayload,
                            sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * bcmUjipetik,
                            sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * bcmTimbangan,
                            sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * bcmBucket,
                            sp_distance_gps_payload: Number(rawData[dx].distance_gps) * bcmPayload,
                            sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * bcmUjipetik,
                            sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * bcmTimbangan,
                            sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * bcmBucket,
                            distance_survei_payload: Number(rawData[dx].distance_survei),
                            distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                            distance_survei_timbangan: Number(rawData[dx].distance_survei),
                            distance_survei_bucket: Number(rawData[dx].distance_survei),
                            distance_gps_payload: Number(rawData[dx].distance_gps),
                            distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                            distance_gps_timbangan: Number(rawData[dx].distance_gps),
                            distance_gps_bucket: Number(rawData[dx].distance_gps),
                        });
                    }

                    if (rawData[dx].material.type === util.MATERIAL_OB) {
                        if (volume.length > 0) {
                            volume[0].ritase += 1;
                            volume[0].bucket_count += Number(rawData[dx].bucket_count);
                            volume[0].tonnage_payload += bcmPayload;
                            volume[0].tonnage_ujipetik += bcmUjipetik;
                            volume[0].tonnage_timbangan += bcmTimbangan;
                            volume[0].tonnage_bucket += bcmBucket;
                            volume[0].sp_distance_survei_payload += Number(rawData[dx].distance_survei) * bcmPayload;
                            volume[0].sp_distance_survei_ujipetik += Number(rawData[dx].distance_survei) * bcmUjipetik;
                            volume[0].sp_distance_survei_timbangan += Number(rawData[dx].distance_survei) * bcmTimbangan;
                            volume[0].sp_distance_survei_bucket += Number(rawData[dx].distance_survei) * bcmBucket;
                            volume[0].sp_distance_gps_payload += Number(rawData[dx].distance_gps) * bcmPayload;
                            volume[0].sp_distance_gps_ujipetik += Number(rawData[dx].distance_gps) * bcmUjipetik;
                            volume[0].sp_distance_gps_timbangan += Number(rawData[dx].distance_gps) * bcmTimbangan;
                            volume[0].sp_distance_gps_bucket += Number(rawData[dx].distance_gps) * bcmBucket;
                            volume[0].distance_survei_payload = volume[0].sp_distance_survei_payload / volume[0].tonnage_payload;
                            volume[0].distance_survei_ujipetik = volume[0].sp_distance_survei_ujipetik / volume[0].tonnage_ujipetik;
                            volume[0].distance_survei_timbangan = volume[0].sp_distance_survei_timbangan / volume[0].tonnage_timbangan;
                            volume[0].distance_survei_bucket = volume[0].sp_distance_survei_bucket / volume[0].tonnage_bucket;
                            volume[0].distance_gps_payload = volume[0].sp_distance_gps_payload / volume[0].tonnage_payload;
                            volume[0].distance_gps_ujipetik = volume[0].sp_distance_gps_ujipetik / volume[0].tonnage_ujipetik;
                            volume[0].distance_gps_timbangan = volume[0].sp_distance_gps_timbangan / volume[0].tonnage_timbangan;
                            volume[0].distance_gps_bucket = volume[0].sp_distance_gps_bucket / volume[0].tonnage_bucket;
                        } else {
                            time[me]['volume'].push({
                                is_volume: rawData[dx].is_volume,
                                ritase: 1,
                                satuan: 'BCM',
                                bucket_count: Number(rawData[dx].bucket_count),
                                tonnage_payload: bcmPayload,
                                tonnage_ujipetik: bcmUjipetik,
                                tonnage_timbangan: bcmTimbangan,
                                tonnage_bucket: bcmBucket,
                                sp_distance_survei_payload: Number(rawData[dx].distance_survei) * bcmPayload,
                                sp_distance_survei_ujipetik: Number(rawData[dx].distance_survei) * bcmUjipetik,
                                sp_distance_survei_timbangan: Number(rawData[dx].distance_survei) * bcmTimbangan,
                                sp_distance_survei_bucket: Number(rawData[dx].distance_survei) * bcmBucket,
                                sp_distance_gps_payload: Number(rawData[dx].distance_gps) * bcmPayload,
                                sp_distance_gps_ujipetik: Number(rawData[dx].distance_gps) * bcmUjipetik,
                                sp_distance_gps_timbangan: Number(rawData[dx].distance_gps) * bcmTimbangan,
                                sp_distance_gps_bucket: Number(rawData[dx].distance_gps) * bcmBucket,
                                distance_survei_payload: Number(rawData[dx].distance_survei),
                                distance_survei_ujipetik: Number(rawData[dx].distance_survei),
                                distance_survei_timbangan: Number(rawData[dx].distance_survei),
                                distance_survei_bucket: Number(rawData[dx].distance_survei),
                                distance_gps_payload: Number(rawData[dx].distance_gps),
                                distance_gps_ujipetik: Number(rawData[dx].distance_gps),
                                distance_gps_timbangan: Number(rawData[dx].distance_gps),
                                distance_gps_bucket: Number(rawData[dx].distance_gps),
                            });
                        }
                    }

                }
            }
        }

        /**
         * generate raw data
         */
        time.map(_raw => {
            const baseObj = {};
            for (let ok of Object.keys(_raw)) {
                if (ok !== 'hauler_material' && ok !== 'hauler_operation' && ok !== 'hauler_volume'
                    && ok !== 'loader_material' && ok !== 'loader_operation' && ok !== 'loader_volume'
                    && ok !== 'operator_hauler_material' && ok !== 'operator_hauler_operation' && ok !== 'operator_hauler_volume'
                    && ok !== 'operator_loader_material' && ok !== 'operator_loader_operation' && ok !== 'operator_loader_volume'
                    && ok !== 'location_material' && ok !== 'location_operation' && ok !== 'location_volume'
                    && ok !== 'area_material' && ok !== 'area_operation' && ok !== 'area_volume'
                    && ok !== 'hauler'
                    && ok !== 'loader'
                    && ok !== 'operator_hauler'
                    && ok !== 'operator_loader'
                    && ok !== 'location'
                    && ok !== 'area'
                    && ok !== 'material'
                    && ok !== 'operation'
                    && ok !== 'volume') {
                    baseObj[ok] = _raw[ok];
                }
            }

            for (let ook of Object.keys(_raw)) {
                if (ook !== 'hauler_material' && ook !== 'hauler_operation' && ook !== 'hauler_volume'
                    && ook !== 'loader_material' && ook !== 'loader_operation' && ook !== 'loader_volume'
                    && ook !== 'operator_hauler_material' && ook !== 'operator_hauler_operation' && ook !== 'operator_hauler_volume'
                    && ook !== 'operator_loader_material' && ook !== 'operator_loader_operation' && ook !== 'operator_loader_volume'
                    && ook !== 'location_material' && ook !== 'location_operation' && ook !== 'location_volume'
                    && ook !== 'area_material' && ook !== 'area_operation' && ook !== 'area_volume'
                    && ook !== 'hauler'
                    && ook !== 'loader'
                    && ook !== 'operator_hauler'
                    && ook !== 'operator_loader'
                    && ook !== 'location'
                    && ook !== 'area'
                    && ook !== 'material'
                    && ook !== 'operation'
                    && ook !== 'volume') {
                    // do nothing
                } else {
                    _raw[ook].map(_itemRaw => {
                        _itemRaw['type'] = ook;
                        for (let _ky of Object.keys(_itemRaw)) {
                            if (Number.isNaN(_itemRaw[_ky])) {
                                _itemRaw[_ky] = 0;
                            }
                        }
                        const _rawObj = {
                            ...baseObj, ..._itemRaw
                        };

                        resultData.push(_rawObj);
                    });
                }
            }
        });

        /**
         * hapus data lama
         */
        await util.mongoDelete(mo, {
            collection: collection,
            option: {}
        });

        await util.mongoInsert(mo, {
            collection: collection,
            data: resultData
        });

        resumeCallback();
    }

    async.waterfall([

        (cb) => {
            /**
             * raw data
             */
            clog.info('------ get raw data');
            getRawData(mongo, (result) => {
                cb(null, result);
            });
        },

        (rawData, cb) => {
            /**
             * generate time range week
             */
            clog.info('------ generate shift range');
            generateShift(mongo, (result) => {
                cb(null, rawData, result);
            });
        },

        (rawData, shifts, cb) => {
            /**
             * resume
             */
            clog.info('------ resume shift');
            resumeByTimes(shifts, rawData, mongo, config.database.PRODUCTION_SHIFT, () => {
                cb(null, rawData);
            })
        },

        (rawData, cb) => {
            /**
             * generate time range week
             */
            clog.info('------ generate week range');
            generateWeeks(mongo, (result) => {
                cb(null, rawData, result);
            });
        },

        (rawData, weeks, cb) => {
            /**
             * resume
             */
            clog.info('------ resume week');
            resumeByTimes(weeks, rawData, mongo, config.database.PRODUCTION_WEEK, () => {
                cb(null, rawData);
            })
        },

        (rawData, cb) => {
            /**
             * generate time month
             */
            clog.info('------ generate month range');
            generateMonth(mongo, (result) => {
                cb(null, rawData, result);
            });
        },

        (rawData, months, cb) => {
            /**
             * resume
             */
            clog.info('------ resume month');
            resumeByTimes(months, rawData, mongo, config.database.PRODUCTION_MONTH, () => {
                cb(null, rawData);
            })
        },

        (rawData, cb) => {
            /**
             * generate time year
             */
            clog.info('------ generate year range');
            generateYears(mongo, (result) => {
                cb(null, rawData, result);
            });
        },

        (rawData, years, cb) => {
            /**
             * resume
             */
            clog.info('------ resume year');
            resumeByTimes(years, rawData, mongo, config.database.PRODUCTION_YEAR, () => {
                cb(null, rawData);
            })
        },

        (rawData, cb) => {
            /**
             * generate time year
             */
            clog.info('------ generate all range');
            generateAll(mongo, (result) => {
                cb(null, rawData, result);
            });
        },

        (rawData, alls, cb) => {
            /**
             * resume
             */
            clog.info('------ resume all');
            resumeByTimes(alls, rawData, mongo, config.database.PRODUCTION_ALL, () => {
                cb(null, rawData);
            })
        },

    ], (res) => {
        callback();
    });
}