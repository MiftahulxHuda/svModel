// const config = require('./config');
const mysql = require('mysql');
const MongoClient = require('mongodb').MongoClient;
const runner = require('./crawler-run');
const async = require('async');
const winston = require('winston');
const moment = require('moment');
const fs = require('fs');
require('winston-daily-rotate-file');

const cfg = fs.readFileSync(__dirname + '/config.json').toString();
const config = JSON.parse(cfg);

let start = moment().unix();
let stop;

const transport = new (winston.transports.DailyRotateFile)({
    filename: config.global.logdir + 'crawler-%DATE%.log',
    datePattern: 'YYYY-MM-DD',
    zippedArchive: true,
    maxSize: '20m',
    maxFiles: '14d',
    format: winston.format.combine(
        // winston.format.timestamp(),
        winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
        winston.format.json()
    )
});
const clog = winston.createLogger({
    transports: [
        transport
    ]
});

let connMysql = null;
let connMongo = null;

async.waterfall([

    (cb) => {
        /**
         * connect to MySQL
         */
        clog.info('-- connect to mysql');
        connMysql = mysql.createConnection(config.mysqlConfig);
        connMysql.connect(function (err) {
            if (err) {
                clog.error('error connecting: ' + err.stack);
                return;
            }
            cb();
        });
    },

    (cb) => {
        /**
         * connect to MongoDB
         */
        clog.info('-- connect to mongodb');
        MongoClient.connect(config.mongodbConfig.url, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        }, function (err, db) {
            if (err) throw err;
            connMongo = db;
            cb();
        });
    },

    (cb) => {
        /**
         * run crawler
         */
        clog.info('-- run crawler');
        runner.run(connMysql, connMongo, clog, (res) => {
            cb();
        });
    },

    (cb) => {
        /**
         * close MySQL connection
         */
        clog.info('-- close mysql');
        connMysql.end();
        cb();
    },

    (cb) => {
        /**
         * close MongoDB connection
         */
        clog.info('-- close mongodb');
        connMongo.close();
        cb();
    }

], (res) => {

    stop = moment().unix();
    const duration = stop - start;
    clog.info('-- done, duration : ' + duration + ' second');
    console.log('duration : ', duration, ' second');

});