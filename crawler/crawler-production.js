const async = require('async');
const moment = require('moment-timezone');
const config = require('./config');
const util = require('./util');

moment.tz.setDefault('Asia/Jakarta');

module.exports.run = (mysql, mongo, clog, callback) => {

    let paramStart = process.argv[2];
    let times = [];

    /**
     * prepare time set
     */
    if (paramStart) {
        const naw = moment().endOf('day').unix();
        let start = moment(paramStart, 'YYYY-MM-DD').startOf('day').unix();
        while (start < naw) {
            times.push({
                start: moment.unix(start).set({ hour: 6, minute: 0, second: 0 }).unix(),
                stop: moment.unix(start).set({ hour: 18, minute: 0, second: 0 }).unix(),
                shift: util.SHIFT_DAY
            });
            times.push({
                start: moment.unix(start).set({ hour: 18, minute: 0, second: 0 }).unix(),
                stop: moment.unix(start).add(1, 'days').set({ hour: 6, minute: 0, second: 0 }).unix(),
                shift: util.SHIFT_NIGHT
            });
            start = moment.unix(start).add(1, 'days').unix();
        }
    } else {
        times.push({
            start: util.util.translateTimestamp('startOfShift'),
            stop: util.util.translateTimestamp('endOfShift'),
            shift: util.util.getCurrentShift()
        });
    }

    /**
     * shift crawler method
     * sudut pandang equipment
     */
    async function shiftCrawler(time, my, mo, shiftCrawlerCallback) {
        const rawData = await util.mysqlQuery(my, "SELECT  \
                m.id, m.date, m.id_actual, actual.distance distance_survei, m.distance distance_gps, m.is_volume is_volume, \
                m.tonnage tonnage_payload, m.tonnage_ujipetik tonnage_ujipetik, m.tonnage_timbangan tonnage_timbangan, m.tonnage_bucket tonnage_bucket, \
                m.bucket_count bucket_count, \
                shift.cd shift_cd, shift.name shift_name,  \
                loader.id loader_id, loader.name loader_name,  \
                hauler.id hauler_id, hauler.name hauler_name,  \
                operator_loader.id operator_loader_id, operator_loader.name operator_loader_name, \
                operator_loader.cd_role operator_loader_role, operator_loader.staff_id operator_loader_nik, operator_loader.cd_department operator_loader_dept, \
                operator_hauler.id operator_hauler_id, operator_hauler.name operator_hauler_name, \
                operator_hauler.cd_role operator_hauler_role, operator_hauler.staff_id operator_hauler_nik, operator_hauler.cd_department operator_hauler_dept, \
                mat.cd material_cd, mat.name material_name, mat.type material_type, mat.density material_density, \
                operation.cd operation_cd, operation.name operation_name,  \
                location.id location_id, location.name location_name,  \
                area.id area_id, area.name area_name  \
            FROM operation_actual_raw m \
            LEFT JOIN operation_actual actual ON actual.id=m.id_actual \
            LEFT JOIN constant shift ON shift.cd=actual.cd_shift \
            LEFT JOIN equipment loader ON loader.id=actual.id_loader \
            LEFT JOIN equipment hauler ON hauler.id=actual.id_hauler \
            LEFT JOIN user operator_loader ON operator_loader.id=actual.id_loader_operator \
            LEFT JOIN user operator_hauler ON operator_hauler.id=actual.id_hauler_operator \
            LEFT JOIN material mat ON mat.cd=actual.cd_material \
            LEFT JOIN constant operation ON operation.cd=actual.cd_operation \
            LEFT JOIN location location ON location.id=actual.id_location \
            LEFT JOIN area area ON area.id=location.id_area \
            WHERE m.date>=" + time.start + " AND m.date<" + time.stop + " \
            ORDER BY m.date ASC");

        const mapped = {};
        let startTime = time.start;

        for (let iter = 0; iter < rawData.length; iter++) {
            if (mapped[rawData[iter].hauler_id] === undefined) {
                mapped[rawData[iter].hauler_id] = [];

                /**
                 * find timestart from previous data, cari produksi sebelumya yang masih pada shift yg sama
                 */
                const prevData = await util.mysqlQuery(my, "SELECT id,date FROM operation_actual_raw WHERE id_actual='"
                    + rawData[iter].id_actual + "' AND id_hauler='"
                    + rawData[iter].hauler_id + "' AND date<" + rawData[iter].date + " ORDER BY date DESC LIMIT 1");
                if (prevData.length > 0) {
                    // id_actual sama dapat dipastikan shift sama
                    startTime = prevData[0].date;
                }
            }

            mapped[rawData[iter].hauler_id].push({
                /**
                 * start stop disini menggambarkan waktu yang diperlukan untuk
                 * melakukan satu ritase, dimulai dari time.start
                 * data awal kemungkinan akan kurang sesuai
                 * karena memakai time.start bukan date ritase sebelumnya
                 */
                start: startTime,
                stop: rawData[iter].date,
                actual: rawData[iter].id_actual,
                hauler: {
                    id: rawData[iter].hauler_id,
                    name: rawData[iter].hauler_name
                },
                loader: {
                    id: rawData[iter].loader_id,
                    name: rawData[iter].loader_name
                },
                operator_hauler: {
                    id: rawData[iter].operator_hauler_id,
                    nik: rawData[iter].operator_hauler_nik,
                    name: rawData[iter].operator_hauler_name,
                    role: rawData[iter].operator_hauler_role,
                    department: rawData[iter].operator_hauler_dept
                },
                operator_loader: {
                    id: rawData[iter].operator_loader_id,
                    nik: rawData[iter].operator_loader_nik,
                    name: rawData[iter].operator_loader_name,
                    role: rawData[iter].operator_loader_role,
                    department: rawData[iter].operator_loader_dept
                },
                location: {
                    id: rawData[iter].location_id,
                    name: rawData[iter].location_name
                },
                area: {
                    id: rawData[iter].area_id,
                    name: rawData[iter].area_name
                },

                material: {
                    cd: rawData[iter].material_cd,
                    name: rawData[iter].material_name,
                    type: rawData[iter].material_type,
                    density: rawData[iter].material_density
                },
                operation: {
                    cd: rawData[iter].operation_cd,
                    name: rawData[iter].operation_name
                },

                shift: {
                    cd: rawData[iter].shift_cd,
                    name: rawData[iter].shift_cd === util.SHIFT_DAY ? 'Day' : 'Night'
                },

                is_volume: rawData[iter].is_volume,

                duration: rawData[iter].date - startTime,
                distance_survei: rawData[iter].distance_survei,
                distance_gps: rawData[iter].distance_gps,
                bucket_count: rawData[iter].bucket_count,
                tonnage_payload: rawData[iter].tonnage_payload,
                tonnage_ujipetik: rawData[iter].tonnage_ujipetik,
                tonnage_timbangan: rawData[iter].tonnage_timbangan,
                tonnage_bucket: rawData[iter].tonnage_bucket
            });

            startTime = rawData[iter].date;
        }

        let remap = [];
        for (let items of Object.values(mapped)) {
            remap = remap.concat(items);
        }

        /**
         * hapus data lama
         */
        await util.mongoDelete(mo, {
            collection: config.database.PRODUCTION_RAW,
            option: {
                start: {
                    $gte: time.start,
                    $lt: time.stop
                },
                /**
                 * shift diserahkan pada data saja
                 */
                // 'shift.cd': time.shift
            }
        });

        if (remap.length > 0) {
            await util.mongoInsert(mo, {
                collection: config.database.PRODUCTION_RAW,
                data: remap
            });
        }

        shiftCrawlerCallback();
    }

    async.waterfall([

        (cb) => {
            /**
             * do crawl raw data by times
             */
            clog.info('------ crawling by times, ', times.length, ' items');
            async.forEachOf(times, (time, key, _cb) => {
                shiftCrawler(time, mysql, mongo, () => {
                    _cb();
                });
            }, (err) => {
                cb();
            });
        }

    ], (res) => {
        callback();
    });
}