const async = require('async');
const util = require('./util');

const cActivity = require('./crawler-activity');
const cActivityResume = require('./crawler-activity-resume');
const cCycleActivity = require('./crawler-cycle-activity');
const cCycleActivityResume = require('./crawler-cycle-activity-resume');
const cProduction = require('./crawler-production');
const cProductionResume = require('./crawler-production-resume');
const cMaintenance = require('./crawler-maintenance');
const cMaintenanceResume = require('./crawler-maintenance-resume');
const cActivityHmFuelRate = require('./crawler-activity-hm-fuelrate');
const cActivityHmFuelRateResume = require('./crawler-activity-hm-fuelrate-resume');
const refinerProductionData = require('./refiner-production-data');

module.exports.run = (mysql, mongo, clog, callback) => {

    let paramStart = process.argv[2]

    async.waterfall([

        (cb) => {
            /**
             * run crawler activity
             */
            if (paramStart === util.CMD_RESUME) {
                console.log('---- crawler resume activity');
                cActivityResume.run(mysql, mongo, clog, () => {
                    cb();
                });
            } else {
                console.log('---- crawler activity');
                cActivity.run(mysql, mongo, clog, () => {
                    cb();
                });
            }
        },

        (cb) => {
            /**
             * run crawler cycle activity
             */
            if (paramStart === util.CMD_RESUME) {
                console.log('---- crawler resume cycle activity');
                cCycleActivityResume.run(mysql, mongo, clog, () => {
                    cb();
                });
            } else {
                console.log('---- crawler cycle activity');
                cCycleActivity.run(mysql, mongo, clog, () => {
                    cb();
                });
            }
        },

        (cb) => {
            /**
             * run crawler production
             */
            if (paramStart === util.CMD_RESUME) {
                console.log('---- crawler resume production');
                cProductionResume.run(mysql, mongo, clog, () => {
                    cb();
                });
            } else {
                console.log('---- crawler production');
                cProduction.run(mysql, mongo, clog, () => {
                    cb();
                });
            }
        },

        (cb) => {
            /**
             * run crawler activity hm fuel rate
             */
            if (paramStart === util.CMD_RESUME) {
                console.log('---- crawler resume activity hm fuel rate');
                cActivityHmFuelRateResume.run(mysql, mongo, clog, () => {
                    cb();
                });
            } else {
                console.log('---- crawler activity hm fuel rate');
                cActivityHmFuelRate.run(mysql, mongo, clog, () => {
                    cb();
                });
            }
        },

        (cb) => {
            /**
             * run crawler maintenance
             */
            if (paramStart === util.CMD_RESUME) {
                console.log('---- crawler resume maintenance');
                cMaintenanceResume.run(mysql, mongo, clog, () => {
                    cb();
                });
            } else {
                console.log('---- crawler maintenance');
                cMaintenance.run(mysql, mongo, clog, () => {
                    cb();
                });
            }
        },

        (cb) => {
            /**
             * run refiner
             */
            console.log('---- refiner run');
            refinerProductionData.run(mysql, mongo, clog, () => {
                cb();
            });
        }

    ], (res) => {
        callback();
    });
}