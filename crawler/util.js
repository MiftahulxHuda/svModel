const moment = require('moment-timezone');
const config = require('./config');

moment.tz.setDefault('Asia/Jakarta');

const TUM_WORKING_TIME = 'TUM1';
const TUM_DELAY_TIME = 'TUM2';
const TUM_IDLE_TIME = 'TUM3';
const TUM_DOWN_TIME = 'TUM4';
const TUM_RFU_TIME = 'TUM5';
const TUM_STANDBY_TIME = 'TUM6';
const TUM_ASSIGNED_TIME = 'TUM7';
const TUM_MAINTENANCE_TIME = 'TUM8';
module.exports.TUM_WORKING_TIME = TUM_WORKING_TIME;
module.exports.TUM_DELAY_TIME = TUM_DELAY_TIME;
module.exports.TUM_IDLE_TIME = TUM_IDLE_TIME;
module.exports.TUM_DOWN_TIME = TUM_DOWN_TIME;
module.exports.TUM_RFU_TIME = TUM_RFU_TIME;
module.exports.TUM_STANDBY_TIME = TUM_STANDBY_TIME;
module.exports.TUM_ASSIGNED_TIME = TUM_ASSIGNED_TIME;
module.exports.TUM_MAINTENANCE_TIME = TUM_MAINTENANCE_TIME;

const SHIFT_DAY = '007001';
const SHIFT_NIGHT = '007002';
module.exports.SHIFT_DAY = SHIFT_DAY;
module.exports.SHIFT_NIGHT = SHIFT_NIGHT;

const ROSTER_DAY = 'R1';
const ROSTER_NIGHT = 'R2';
const ROSTER_CUTI = 'R3';
module.exports.ROSTER_DAY = ROSTER_DAY;
module.exports.ROSTER_NIGHT = ROSTER_NIGHT;
module.exports.ROSTER_CUTI = ROSTER_CUTI;

const CYCLE_QUEUING_LOADING = 'CYCLEACT1';
const CYCLE_SPOTTING_LOADING = 'CYCLEACT2';
const CYCLE_LOADING = 'CYCLEACT3';
const CYCLE_FULL_TRAVEL = 'CYCLEACT4';
const CYCLE_QUEUING_DUMPING = 'CYCLEACT5';
const CYCLE_SPOTTING_DUMPING = 'CYCLEACT6';
const CYCLE_DUMPING = 'CYCLEACT7';
const CYCLE_EMPTY_TRAVEL = 'CYCLEACT8';
const CYCLE_HANGING = 'CYCLEACT9';
module.exports.CYCLE_QUEUING_LOADING = CYCLE_QUEUING_LOADING;
module.exports.CYCLE_SPOTTING_LOADING = CYCLE_SPOTTING_LOADING;
module.exports.CYCLE_LOADING = CYCLE_LOADING;
module.exports.CYCLE_FULL_TRAVEL = CYCLE_FULL_TRAVEL;
module.exports.CYCLE_QUEUING_DUMPING = CYCLE_QUEUING_DUMPING;
module.exports.CYCLE_SPOTTING_DUMPING = CYCLE_SPOTTING_DUMPING;
module.exports.CYCLE_DUMPING = CYCLE_DUMPING;
module.exports.CYCLE_EMPTY_TRAVEL = CYCLE_EMPTY_TRAVEL;
module.exports.CYCLE_HANGING = CYCLE_HANGING;

const TIME_CYCLE_ARRIVE_LOADING = "021001";
const TIME_CYCLE_FIRST_BUCKET = "021002";
const TIME_CYCLE_FULL_BUCKET = "021003";
const TIME_CYCLE_ARRIVE_DUMPING = "021004";
const TIME_CYCLE_START_DUMP = "021005";
const TIME_CYCLE_FINISH_DUMP = "021006";
module.exports.TIME_CYCLE_ARRIVE_LOADING = TIME_CYCLE_ARRIVE_LOADING;
module.exports.TIME_CYCLE_FIRST_BUCKET = TIME_CYCLE_FIRST_BUCKET;
module.exports.TIME_CYCLE_FULL_BUCKET = TIME_CYCLE_FULL_BUCKET;
module.exports.TIME_CYCLE_ARRIVE_DUMPING = TIME_CYCLE_ARRIVE_DUMPING;
module.exports.TIME_CYCLE_START_DUMP = TIME_CYCLE_START_DUMP;
module.exports.TIME_CYCLE_FINISH_DUMP = TIME_CYCLE_FINISH_DUMP;

const MATERIAL_OB = 'Overburden';
const MATERIAL_COAL = 'Coal';
module.exports.MATERIAL_OB = MATERIAL_OB;
module.exports.MATERIAL_COAL = MATERIAL_COAL;

const CMD_RESUME = 'resume';
module.exports.CMD_RESUME = CMD_RESUME;

module.exports.util = {

    /**
     * Berkaitan dengan tabel equipment_log_start
     * untuk menentukan cd_tum
     * @param cdSelf equipment activity (CD_SELF)
     * @param cd equipment activity (CD)
     */
    getCdTUMFromCdSelf(cdSelf, cd) {
        if (cdSelf.startsWith('ACTIVITY') || cdSelf.startsWith('GENERAL')) {
            return TUM_WORKING_TIME;
        } else if (cdSelf.startsWith('DELAY')) {
            return TUM_DELAY_TIME;
        } else if (cdSelf.startsWith('IDLE')) {
            return TUM_IDLE_TIME;
        } else if (cdSelf.startsWith('STANDBY')) {
            return TUM_STANDBY_TIME;
        } else if (cdSelf.startsWith('M') || cd === 'B3') { // wait mechanic
            return TUM_DOWN_TIME;
        } else if (cdSelf.startsWith('B')) {
            return TUM_MAINTENANCE_TIME;
        } else {
            return null;
        }
    },

    generateWONumber() {
        return 'WO-' + moment().unix();
    },

    getYesterdayTime() {
        const start = moment().subtract(1, 'days').startOf('day').unix();
        const end = moment().subtract(1, 'days').endOf('day').unix();
        return {
            start: start,
            end: end
        };
    },

    isBefore6() {
        const naw = moment().unix();
        const tm6 = moment().set({ hour: 6, minute: 0 }).unix();
        if (naw < tm6) {
            return true;
        } else {
            return false;
        }
    },

    getCurrentShift() {
        const naw = moment().unix();
        const tm0 = moment().set({ hour: 0, minute: 0 }).unix();
        const tm6 = moment().set({ hour: 6, minute: 0 }).unix();
        const tm18 = moment().set({ hour: 18, minute: 0 }).unix();

        if (naw > tm0 && naw < tm6) {
            return SHIFT_NIGHT;
        }
        if (naw > tm6 && naw < tm18) {
            return SHIFT_DAY;
        }
        if (naw > tm18) {
            return SHIFT_NIGHT;
        }
        return null;
    },

    getCurrentRoster() {
        const shift = this.getCurrentShift();
        if (shift === SHIFT_NIGHT) {
            return ROSTER_NIGHT;
        } else if (shift === SHIFT_DAY) {
            return ROSTER_DAY;
        } else {
            return '';
        }
    },

    getShiftByTime(naw) {
        const tm0 = moment.unix(naw).set({ hour: 0, minute: 0 }).unix();
        const tm6 = moment.unix(naw).set({ hour: 6, minute: 0 }).unix();
        const tm18 = moment.unix(naw).set({ hour: 18, minute: 0 }).unix();

        if (naw > tm0 && naw < tm6) {
            return SHIFT_NIGHT;
        }
        if (naw > tm6 && naw < tm18) {
            return SHIFT_DAY;
        }
        if (naw > tm18) {
            return SHIFT_NIGHT;
        }
        return null;
    },

    isToday(tm) {
        const tm0 = moment().set({ hour: 0, minute: 0 }).unix();
        const tm23 = moment().set({ hour: 23, minute: 59, second: 59 }).unix();
        if (tm >= tm0 && tm < tm23) {
            return true;
        } else {
            return false;
        }
    },

    getStartEndTime() {
        const start = moment().startOf('day').unix();
        const end = moment().endOf('day').unix();
        return {
            start: start,
            end: end
        };
    },

    getShiftDayStartTime() {
        let tm;
        if (this.isBefore6()) {
            tm = moment().subtract(1, 'days').startOf('day').set({ hour: 6 }).unix();
        } else {
            tm = moment().startOf('day').set({ hour: 6 }).unix();
        }
        return tm;
    },

    getShiftNightStartTime() {
        let tm;
        if (this.isBefore6()) {
            tm = moment().subtract(1, 'days').startOf('day').set({ hour: 18 }).unix();
        } else {
            tm = moment().startOf('day').set({ hour: 18 }).unix();
        }
        return tm;
    },

    getShiftNightEndTime() {
        let tm;
        if (this.isBefore6()) {
            tm = moment().startOf('day').set({ hour: 6 }).unix();
        } else {
            tm = moment().add(1, 'days').startOf('day').set({ hour: 6 }).unix();
        }
        return tm;
    },

    getDateOfShift() {
        let tm;
        if (this.isBefore6()) {
            tm = moment().subtract(1, 'days').startOf('day').unix();
        } else {
            tm = moment().startOf('day').unix();
        }
        return tm;
    },

    getRad(x, y) {
        const deg = Math.atan(x / y);
        return deg;
    },

    translateTimestamp(tmt) {
        if (tmt === 'current') {
            /** mengambil waktu saat ini */
            return moment().unix();

        } else if (tmt === 'startShiftDay') {
            /** start shift day pada saat ini */
            return this.getShiftDayStartTime();

        } else if (tmt === 'endShiftDay') {
            /** end shift day pada saat ini */
            return this.getShiftNightStartTime();

        } else if (tmt === 'startShiftNight') {
            /** start shift night pada saat ini, call sebelum jam 24 */
            return this.getShiftNightStartTime();

        } else if (tmt === 'endShiftNight') {
            /** end shift night kemarin, call antara jam 0 sampe 6 */
            return this.getShiftNightEndTime();

        } else if (tmt === 'startDay') {
            return moment().startOf('day').unix();

        } else if (tmt === 'endDay') {
            return moment().endOf('day').unix();

        } else if (tmt === 'startOfShift') {
            /** get start of shift (day/night), call bebas */
            const cur = this.getCurrentShift();
            if (cur === SHIFT_DAY) {
                return this.getShiftDayStartTime();
            } else {
                return this.getShiftNightStartTime();
            }

        } else if (tmt === 'endOfShift') {
            /** get end of shift (day/night), call bebas */
            const cur = this.getCurrentShift();
            if (cur === SHIFT_DAY) {
                return this.getShiftNightStartTime();
            } else {
                return this.getShiftNightEndTime();
            }

        } else if (tmt === 'dateOfShift') {
            /** mengambil tanggal dari shift saat ini */
            return this.getDateOfShift();

        } else {
            return null;

        }
    },

    getBCMFromTonnage(tonnage, density) {
        return tonnage / density;
    },

    getTonnageFromBCM(bcm, density) {
        return bcm * density;
    },

    /**
     * @param payload payload from loadcell device
     * @param capacityReal depend on type of material
     */
    getTonnage(payload, capacityReal) {
        return payload - capacityReal;
    },

    /**
     * Distance from lat lon in meter
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     */
    getDistance(lat1, lon1, lat2, lon2) {
        const R = 6371; // Radius of the earth in km
        const dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
        const dLon = this.deg2rad(lon2 - lon1);
        const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        const d = R * c; // Distance in km
        const dM = d * 1000; // Distance in meter
        return dM;
    },

    deg2rad(deg) {
        return deg * (Math.PI / 180)
    }

};

module.exports.mysqlQuery = (my, sql) => {
    return new Promise((resolve, reject) => {
        my.query(sql, function (err, result, fields) {
            if (err) reject(err);
            resolve(result);
        });
    });
};

module.exports.mongoFind = (mo, opt) => {
    return new Promise((resolve, reject) => {
        const mongo = mo.db(config.mongodbConfig.database).collection(opt.collection).find(opt.option)

        if (opt.sort) {
            mongo.sort(opt.sort)
        }

        mongo.toArray(function (err, result) {
            if (err) reject(err);
            resolve(result);
        });
    });
};

module.exports.mongoFindOne = (mo, opt) => {
    return new Promise((resolve, reject) => {
        mo.db(config.mongodbConfig.database).collection(opt.collection).find(opt.option).sort(opt.sort).limit(1).toArray(function (err, result) {
            if (err) reject(err);
            resolve(result);
        });
    });
};

module.exports.mongoInsert = (mo, opt) => {
    return new Promise((resolve, reject) => {
        mo.db(config.mongodbConfig.database).collection(opt.collection).insertMany(opt.data, function (err, result) {
            if (err) reject(err);
            resolve(result);
        });
    });
};

module.exports.mongoDelete = (mo, opt) => {
    return new Promise((resolve, reject) => {
        mo.db(config.mongodbConfig.database).collection(opt.collection).deleteMany(opt.option, function (err, result) {
            if (err) reject(err);
            resolve(result);
        });
    });
};

module.exports.mongoUpdate = (mo, opt) => {
    return new Promise((resolve, reject) => {
        mo.db(config.mongodbConfig.database).collection(opt.collection).updateMany(opt.option, opt.data, function (err, result) {
            if (err) reject(err);
            resolve(result);
        });
    });
};

module.exports.mongoAggregate = (mo, opt) => {
    return new Promise((resolve, reject) => {
        const mongo = mo.db(config.mongodbConfig.database).collection(opt.collection).aggregate(opt.option)

        mongo.toArray(function (err, result) {
            if (err) reject(err);
            resolve(result);
        });
    });
};