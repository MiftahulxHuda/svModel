const async = require('async');
const moment = require('moment-timezone');
const config = require('./config');
const util = require('./util');
const request = require('request');
const https = require('https');

moment.tz.setDefault('Asia/Jakarta');

module.exports.run = (mysql, mongo, clog, callback) => {

    async function generateTimeMinAndMax(hourCallback) {
        request({
            url: `${config.APIs.baseUSUrl}/gps/time-min-max`,
            json: true
        }, function (error, response, body) {
            // console.error('error:', error); // Print the error if one occurred
            // console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
            // console.log('body:', body['output']); // Print the HTML for the Google homepage.

            if(error) {
                clog.error({
                    message: "error generateTimeMinAndMax in crawler-hm-fuel-rate-resume",
                    data: error
                });
            }

            hourCallback(body['output']);
        });
    }

    async function generateTimeEachHour(times, shiftCallback) {
        // const min = await util.mongoFindOne(mo, {
        //     collection: config.database.GPS,
        //     option: {},
        //     sort: {
        //         date: 1
        //     }
        // });
        // const max = await util.mongoFindOne(mo, {
        //     collection: config.database.GPS,
        //     option: {},
        //     sort: {
        //         date: -1
        //     }
        // });

        // const minYear = min.length > 0 ? moment.unix(min[0].date).year() : moment().year();
        // const maxYear = max.length > 0 ? moment.unix(max[0].date).year() : moment().year();

        const minYear = times.min ? moment.unix(times.min).year() : moment().year();
        const maxYear = times.max ? moment.unix(times.max).year() : moment().year();

        const hoursOfDay = [];
        for(var hour = 0; hour < 24; hour++) {
            hoursOfDay.push(hour) 
        }

        const hours = [];
        for (let ye = minYear; ye <= maxYear; ye++) {
            for (let mm = 0; mm < 12; mm++) {
                const latestDate = moment().set({ year: ye, month: mm }).endOf('month').date();
                for (let dt = 1; dt <= latestDate; dt++) {
                    for (let hour = 0; hour < hoursOfDay.length; hour++) {
                        const before = hoursOfDay[hour - 1];
                        const current = hoursOfDay[hour];

                        if(before) {
                            hours.push({
                                year: ye,
                                month: mm,
                                date: dt,
                                start: moment().set({ year: ye, month: mm, date: dt }).set({ hour: before, minute: 0, second: 0 }).unix(),
                                stop: moment().set({ year: ye, month: mm, date: dt }).set({ hour: current, minute: 0, second: 0 }).unix(),
                            });
                        }
                    }
                }
            }
        }

        shiftCallback(hours);
    }


    /**
     * generate shift range
     */
    async function generateShift(times, shiftCallback) {
        // const min = await util.mongoFindOne(mo, {
        //     collection: config.database.GPS,
        //     option: {},
        //     sort: {
        //         start: 1
        //     }
        // });
        // const max = await util.mongoFindOne(mo, {
        //     collection: config.database.GPS,
        //     option: {},
        //     sort: {
        //         start: -1
        //     }
        // });
        // const minYear = min.length > 0 ? moment.unix(min[0].date).year() : moment().year();
        // const maxYear = max.length > 0 ? moment.unix(max[0].date).year() : moment().year();
        
        const minYear = times.min ? moment.unix(times.min).year() : moment().year();
        const maxYear = times.max ? moment.unix(times.max).year() : moment().year();

        const shifts = [];
        for (let ye = minYear; ye <= maxYear; ye++) {
            for (let mm = 0; mm < 12; mm++) {
                const latestDate = moment().set({ year: ye, month: mm }).endOf('month').date();
                for (let dt = 1; dt <= latestDate; dt++) {
                    shifts.push({
                        year: ye,
                        month: mm,
                        date: dt,
                        shift: util.SHIFT_DAY,
                        start: moment().set({ year: ye, month: mm, date: dt }).set({ hour: 6, minute: 0, second: 0 }).unix(),
                        stop: moment().set({ year: ye, month: mm, date: dt }).set({ hour: 18, minute: 0, second: 0 }).unix(),
                    });
                    shifts.push({
                        year: ye,
                        month: mm,
                        date: dt,
                        shift: util.SHIFT_NIGHT,
                        start: moment().set({ year: ye, month: mm, date: dt }).set({ hour: 18, minute: 0, second: 0 }).unix(),
                        stop: moment().set({ year: ye, month: mm, date: dt }).add(1, 'days').set({ hour: 6, minute: 0, second: 0 }).unix(),
                    });
                }
            }
        }
        shiftCallback(shifts);
    }

    /**
     * get all week
     */
    async function generateWeeks(times, weekCallback) {
        // const min = await util.mongoFindOne(mo, {
        //     collection: config.database.GPS,
        //     option: {},
        //     sort: {
        //         start: 1
        //     }
        // });
        // const max = await util.mongoFindOne(mo, {
        //     collection: config.database.GPS,
        //     option: {},
        //     sort: {
        //         start: -1
        //     }
        // });
        // const minYear = min.length > 0 ? moment.unix(min[0].date).year() : moment().year();
        // const maxYear = max.length > 0 ? moment.unix(max[0].date).year() : moment().year();

        const minYear = times.min ? moment.unix(times.min).year() : moment().year();
        const maxYear = times.max ? moment.unix(times.max).year() : moment().year();
        
        const weeks = [];
        for (let ye = minYear; ye <= maxYear; ye++) {
            const totalWeek = moment().set({ year: ye }).endOf('year').add(-1, 'week').week();
            for (let wee = 1; wee <= totalWeek; wee++) {
                weeks.push({
                    year: ye,
                    week: wee,
                    start: moment().set({ year: ye }).week(wee).startOf('week').unix(),
                    stop: moment().set({ year: ye }).week(wee).endOf('week').unix(),
                });
            }
        }
        weekCallback(weeks);
    }

    /**
     * get all month
     */
    async function generateMonth(times, monthCallback) {
        // const min = await util.mongoFindOne(mo, {
        //     collection: config.database.GPS,
        //     option: {},
        //     sort: {
        //         start: 1
        //     }
        // });
        // const max = await util.mongoFindOne(mo, {
        //     collection: config.database.GPS,
        //     option: {},
        //     sort: {
        //         start: -1
        //     }
        // });
        // const minYear = min.length > 0 ? moment.unix(min[0].date).year() : moment().year();
        // const maxYear = max.length > 0 ? moment.unix(max[0].date).year() : moment().year();

        const minYear = times.min ? moment.unix(times.min).year() : moment().year();
        const maxYear = times.max ? moment.unix(times.max).year() : moment().year();
        
        const months = [];
        for (let ye = minYear; ye <= maxYear; ye++) {
            const totalMonth = 12;
            for (let m = 0; m < totalMonth; m++) {
                months.push({
                    year: ye,
                    month: m,
                    start: moment().set({ year: ye, month: m }).startOf('month').unix(),
                    stop: moment().set({ year: ye, month: m }).endOf('month').unix(),
                });
            }
        }
        monthCallback(months);
    }

    /**
     * generate year
     */
    async function generateYears(times, yearCallback) {
        // const min = await util.mongoFindOne(mo, {
        //     collection: config.database.GPS,
        //     option: {},
        //     sort: {
        //         start: 1
        //     }
        // });
        // const max = await util.mongoFindOne(mo, {
        //     collection: config.database.GPS,
        //     option: {},
        //     sort: {
        //         start: -1
        //     }
        // });
        // const minYear = min.length > 0 ? moment.unix(min[0].date).year() : moment().year();
        // const maxYear = max.length > 0 ? moment.unix(max[0].date).year() : moment().year();

        const minYear = times.min ? moment.unix(times.min).year() : moment().year();
        const maxYear = times.max ? moment.unix(times.max).year() : moment().year();

        const years = [];
        for (let ye = minYear; ye <= maxYear; ye++) {
            years.push({
                year: ye,
                start: moment().set({ year: ye }).startOf('year').unix(),
                stop: moment().set({ year: ye }).endOf('year').unix(),
            });
        }
        yearCallback(years);
    }

    /**
     * generate all
     */
    async function generateAll(times, allCallback) {
        // const min = await util.mongoFindOne(mo, {
        //     collection: config.database.GPS,
        //     option: {},
        //     sort: {
        //         start: 1
        //     }
        // });
        // const max = await util.mongoFindOne(mo, {
        //     collection: config.database.GPS,
        //     option: {},
        //     sort: {
        //         start: -1
        //     }
        // });
        // const mins = min.length > 0 ? min[0].date : moment().unix();
        // const maxs = max.length > 0 ? max[0].date : moment().unix();

        const mins = times.min ? times.min : moment().unix();
        const maxs = times.max ? times.max : moment().unix();

        const all = [];
        all.push({
            start: mins,
            stop: maxs + 1
        });
        allCallback(all);
    }

    /**
     * get raw data
     */
    async function getRawData(rawDataCallback) {
        // const rawData = await util.mongoFind(mo, {
        //     collection: config.database.GPS,
        //     option: {},
        //     sort: {
        //         date: 1
        //     }
        // });
        // rawDataCallback(rawData);

        // console.log("call api " + `${config.APIs.baseUSUrl}/gps`)
        request({
            url: `${config.APIs.baseUSUrl}/gps`,
            json: true
        }, function (error, response, body) {
            // console.error('error:', error); // Print the error if one occurred
            // console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
            // console.log('body:', body['output'].length); // Print the HTML for the Google homepage.
            if(error) {
                clog.error({
                    message: "error getRawData gps in crawler-hm-fuel-rate-resume",
                    data: error
                });
            }

            rawDataCallback(body['output']);
        });
    }

    /**
     * resume
     */
    async function resumeByTimes(times, rawData, mo, my, collection, resumeCallback) {
        const resultData = [];
        const TIME = Object.assign([], times);
        const LAST_DATE_MAP = [];

        for (let me = 0; me < TIME.length; me++) {
            if(TIME[me]['units'] == undefined) {
                TIME[me]['units'] = [];
            }
            
            let filterDateRawData = rawData.filter(el => {
                return el.date >= TIME[me].start && el.date <= TIME[me].stop;
            });

            if(filterDateRawData.length > 0) {
                for (let indexFilterDateRawData = 0; indexFilterDateRawData < filterDateRawData.length; indexFilterDateRawData++) {
                    const elRaw = filterDateRawData[indexFilterDateRawData];
                    const filterUnitId = TIME[me]['units'].findIndex(unit => {
                        return unit.unit_id === elRaw.unit_id;
                    })


                    if(filterUnitId > -1) {
                        TIME[me]['units'][filterUnitId]['fuel_rate'] += (elRaw.ecu.fuel_rate ? elRaw.ecu.fuel_rate.value : 0);
                        TIME[me]['units'][filterUnitId]['count_fuel_rate'] += 1;

                        const duration_hour_meter = filterDateRawData[indexFilterDateRawData].date - LAST_DATE_MAP[filterUnitId].last_date;
                        TIME[me]['units'][filterUnitId]['duration_hour_meter'] += duration_hour_meter;
                        LAST_DATE_MAP[filterUnitId]['last_date'] = filterDateRawData[indexFilterDateRawData].date;
                    } else {
                        TIME[me]['units'].push({
                            unit_id: elRaw.unit_id,
                            unit_type: elRaw.unit_type,
                            fuel_rate: (elRaw.ecu.fuel_rate ? elRaw.ecu.fuel_rate.value : 0),
                            count_fuel_rate: 0,
                            duration_hour_meter: 0,
                        });
                        LAST_DATE_MAP.push({
                            unit_id: elRaw.unit_id,
                            last_date: elRaw.date
                        });
                    }
                }
            }
            
            
        }

        const HM_FUELRATE = [];
        for (let iTime = 0; iTime < TIME.length; iTime++) {
            const eTime = TIME[iTime];
            if(eTime.units.length > 0) {
                // console.log(eTime.units.length, eTime.start, eTime.stop)
                for (let iUnit = 0; iUnit < eTime.units.length; iUnit++) {
                    const eUnit = eTime.units[iUnit];
                    let unitName = "";
                    if(eUnit.unit_id != undefined) {
                        unitName = await util.mysqlQuery(
                            my, 
                            `SELECT name FROM equipment WHERE id = "${eUnit.unit_id}" LIMIT 1`
                        );
                    }
                    let fuel_rate = 0;
                    if(eUnit.count_fuel_rate) { 
                        fuel_rate = eUnit.fuel_rate / eUnit.count_fuel_rate;
                    }

                    const FUEL_CONSUMPTION = eUnit.duration_hour_meter * fuel_rate;
                    HM_FUELRATE.push({
                        "year": eTime.year,
                        "month": eTime.month,
                        "date": eTime.date,
                        "start": eTime.start,
                        "stop": eTime.stop,
                        "unit": {
                            unit_id: eUnit.unit_id,
                            unit_name: unitName.length > 0 ? unitName.name : "",
                            unit_type: eUnit.unit_type,
                        },
                        "fuel_rate": fuel_rate,
                        "fuel_consumption": FUEL_CONSUMPTION,
                        // "raw_fuel_rate": eUnit.fuel_rate,
                        // "raw_count_fuel_rate": eUnit.count_fuel_rate,
                        "duration_hour_meter": eUnit.duration_hour_meter
                    })
                }
            }
        }

        /**
         * hapus data lama
         */
        await util.mongoDelete(mo, {
            collection: collection,
            option: {}
        });

        await util.mongoInsert(mo, {
            collection: collection,
            data: HM_FUELRATE
        });

        resumeCallback();
    }

    async.waterfall([

        (cb) => {
            /**
             * raw data
             */
            clog.info('------ get raw data');
            getRawData((result) => {
                cb(null, result);
            });
        },

        (rawData, cb) => {
            /**
             * raw data
             */
            clog.info('------ get raw data');
            generateTimeMinAndMax((result) => {
                cb(null, rawData, result);
            });
        },

        (rawData, times, cb) => {
            /**
             * generate time range week
             */
            clog.info('------ generate shift range');
            generateTimeEachHour(times, (result) => {
                cb(null, rawData, times, result);
            });
        },

        (rawData, times, hours, cb) => {
            /**
             * resume
             */
            clog.info('------ resume shift');
            resumeByTimes(hours, rawData, mongo, mysql, config.database.HM_FUELRATE_HOURS, () => {
                cb(null, rawData, times);
            })
        },

        (rawData, times, cb) => {
            /**
             * generate time range week
             */
            clog.info('------ generate shift range');
            generateShift(times, (result) => {
                cb(null, rawData, times, result);
            });
        },

        (rawData, times, shifts, cb) => {
            /**
             * resume
             */
            clog.info('------ resume shift');
            resumeByTimes(shifts, rawData, mongo, mysql, config.database.HM_FUELRATE_SHIFT, () => {
                cb(null, rawData, times);
            })
        },

        (rawData, times, cb) => {
            /**
             * generate time range week
             */
            clog.info('------ generate week range');
            generateWeeks(times, (result) => {
                cb(null, rawData, times, result);
            });
        },

        (rawData, times, weeks, cb) => {
            /**
             * resume
             */
            clog.info('------ resume week');
            resumeByTimes(weeks, rawData, mongo, mysql, config.database.HM_FUELRATE_WEEK, () => {
                cb(null, rawData, times);
            })
        },

        (rawData, times, cb) => {
            /**
             * generate time month
             */
            clog.info('------ generate month range');
            generateMonth(times, (result) => {
                cb(null, rawData, times, result);
            });
        },

        (rawData, times, months, cb) => {
            /**
             * resume
             */
            clog.info('------ resume month');
            resumeByTimes(months, rawData, mongo, mysql, config.database.HM_FUELRATE_MONTH, () => {
                cb(null, rawData, times);
            })
        },

        (rawData, times, cb) => {
            /**
             * generate time year
             */
            clog.info('------ generate year range');
            generateYears(times, (result) => {
                cb(null, rawData, times, result);
            });
        },

        (rawData, times, years, cb) => {
            /**
             * resume
             */
            clog.info('------ resume year');
            resumeByTimes(years, rawData, mongo, mysql, config.database.HM_FUELRATE_YEAR, () => {
                cb(null, rawData, times);
            })
        },

        (rawData, times, cb) => {
            /**
             * generate time year
             */
            clog.info('------ generate all range');
            generateAll(times, (result) => {
                cb(null, rawData, times, result);
            });
        },

        (rawData, times, alls, cb) => {
            /**
             * resume
             */
            // clog.info('------ resume all');
            resumeByTimes(alls, rawData, mongo, mysql, config.database.HM_FUELRATE_ALL, () => {
                cb(null, rawData);
            })
        },

    ], (res) => {
        callback();
    });
}