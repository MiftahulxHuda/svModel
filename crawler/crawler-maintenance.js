const async = require('async');
const moment = require('moment-timezone');
const config = require('./config');
const util = require('./util');

moment.tz.setDefault('Asia/Jakarta');

module.exports.run = (mysql, mongo, clog, callback) => {

    let paramStart = process.argv[2];
    let times = [];

    /**
     * prepare time set
     */
    if (paramStart) {
        const naw = moment().endOf('day').unix();
        let start = moment(paramStart, 'YYYY-MM-DD').startOf('day').unix();
        while (start < naw) {
            times.push({
                start: moment.unix(start).set({ hour: 6, minute: 0, second: 0 }).unix(),
                stop: moment.unix(start).set({ hour: 18, minute: 0, second: 0 }).unix(),
                shift: util.SHIFT_DAY
            });
            times.push({
                start: moment.unix(start).set({ hour: 18, minute: 0, second: 0 }).unix(),
                stop: moment.unix(start).add(1, 'days').set({ hour: 6, minute: 0, second: 0 }).unix(),
                shift: util.SHIFT_NIGHT
            });
            start = moment.unix(start).add(1, 'days').unix();
        }
    } else {
        times.push({
            start: util.util.translateTimestamp('startOfShift'),
            stop: util.util.translateTimestamp('endOfShift'),
            shift: util.util.getCurrentShift()
        });
    }

    /**
     * shift crawler method
     * sudut pandang equipment
     */
    async function shiftCrawler(time, my, mo, shiftCrawlerCallback) {
        const rawData = await util.mysqlQuery(my, "SELECT  \
                m.id, m.created_at time, m.type, m.completed_at completed, \
                eq.id equipment_id, eq.name equipment_name, eq.cd_equipment equipment_type, \
                problem.cd problem_cd, problem.name problem_name  \
            FROM maintenance m \
            LEFT JOIN equipment eq ON eq.id=m.equipment_id \
            LEFT JOIN constant problem ON problem.cd=m.problem_id \
            WHERE m.created_at>=" + time.start + " AND m.created_at<" + time.stop + " \
            ORDER BY m.created_at ASC");
        const mapped = {};
        for (let iter = 0; iter < rawData.length; iter++) {
            if (mapped[rawData[iter].equipment_id] === undefined) {
                mapped[rawData[iter].equipment_id] = [];
            }
            let duration = rawData[iter].completed;
            if (!Number.isNaN(duration) && duration > 0) {
                duration -= rawData[iter].time;
            }
            mapped[rawData[iter].equipment_id].push({
                start: rawData[iter].time,
                stop: rawData[iter].completed,
                equipment: {
                    id: rawData[iter].equipment_id,
                    name: rawData[iter].equipment_name,
                    type: rawData[iter].equipment_type
                },
                problem: {
                    cd: rawData[iter].activity_cd,
                    name: rawData[iter].activity_name
                },
                duration: duration
            });

        }
        let remap = [];
        for (let items of Object.values(mapped)) {
            remap = remap.concat(items);
        }
        if (remap.length > 0) {
            /**
             * hapus data lama
             */
            await util.mongoDelete(mo, {
                collection: config.database.MAINTENANCE_RAW,
                option: {
                    start: {
                        $gte: time.start,
                        $lt: time.stop
                    },
                    'shift.cd': time.shift
                }
            });

            await util.mongoInsert(mo, {
                collection: config.database.MAINTENANCE_RAW,
                data: remap
            });
        }
        shiftCrawlerCallback();
        /**
         * ingat beberapa data khususnya data di akhir shift akan memiliki
         * duration == 0
         * maka perlu dipasangkan dengan stop nya
         */
    }

    async.waterfall([

        (cb) => {
            /**
             * do crawl raw data by times
             */
            clog.info('------ crawling by times, ', times.length, ' items');
            async.forEachOf(times, (time, key, _cb) => {
                shiftCrawler(time, mysql, mongo, () => {
                    _cb();
                });
            }, (err) => {
                cb();
            });
        },

    ], (res) => {
        callback();
    });
}