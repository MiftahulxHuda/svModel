module.exports.mysqlConfig = {
    host: '0.0.0.0',
    database: 'checker_sbs',
    user: 'root',
    password: 'root',
    port: '3306'
};

module.exports.APIs = {
    baseUSUrl: "http://localhost:8888/v-us"
};

module.exports.mongodbConfig = {
    url: 'mongodb://localhost:27017/',
    database: 'fms'
};

module.exports.global = {
    logdir: '/home/huda/TMP/logs/',
};

module.exports.database = {
    ACTIVITY_RAW: 'activity_raw',
    ACTIVITY_SHIFT: 'activity_shift',
    ACTIVITY_DAY: 'activity_day',
    ACTIVITY_WEEK: 'activity_week',
    ACTIVITY_MONTH: 'activity_month',
    ACTIVITY_YEAR: 'activity_year',
    ACTIVITY_ALL: 'activity_all',

    CYCLE_ACTIVITY_RAW: 'cycle_activity_raw',
    CYCLE_ACTIVITY_SHIFT: 'cycle_activity_shift',
    CYCLE_ACTIVITY_DAY: 'cycle_activity_day',
    CYCLE_ACTIVITY_WEEK: 'cycle_activity_week',
    CYCLE_ACTIVITY_MONTH: 'cycle_activity_month',
    CYCLE_ACTIVITY_YEAR: 'cycle_activity_year',
    CYCLE_ACTIVITY_ALL: 'cycle_activity_all',

    PRODUCTION_RAW: 'production_raw',
    PRODUCTION_SHIFT: 'production_shift',
    PRODUCTION_DAY: 'production_day',
    PRODUCTION_WEEK: 'production_week',
    PRODUCTION_MONTH: 'production_month',
    PRODUCTION_YEAR: 'production_year',
    PRODUCTION_ALL: 'production_all',

    MAINTENANCE_RAW: 'maintenance_raw',
    MAINTENANCE_SHIFT: 'maintenance_shift',
    MAINTENANCE_DAY: 'maintenance_day',
    MAINTENANCE_WEEK: 'maintenance_week',
    MAINTENANCE_MONTH: 'maintenance_month',
    MAINTENANCE_YEAR: 'maintenance_year',
    MAINTENANCE_ALL: 'maintenance_all',

    GPS: "gps",
    ACTIVITY_HM_FUELRATE: "activity_hm_fuelrate_raw",
    ACTIVITY_HM_FUELRATE_SHIFT: "activity_hm_fuelrate_shift",
    ACTIVITY_HM_FUELRATE_DAY: "activity_hm_fuelrate_day",
    ACTIVITY_HM_FUELRATE_WEEK: "activity_hm_fuelrate_week",
    ACTIVITY_HM_FUELRATE_MONTH: "activity_hm_fuelrate_month",
    ACTIVITY_HM_FUELRATE_YEAR: "activity_hm_fuelrate_year",
    ACTIVITY_HM_FUELRATE_ALL: "activity_hm_fuelrate_all"
};