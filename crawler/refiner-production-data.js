const async = require('async');
const moment = require('moment-timezone');
const config = require('./config');
const util = require('./util');

moment.tz.setDefault('Asia/Jakarta');

const CD_ARRIVE_LOADING = '021001';

const CD_FIRST_BUCKET = '021002';

const CD_FULL_BUCKET = '021003';

const CD_ARRIVE_DUMPING = '021004';

const CD_START_DUMP = '021005';

const CD_FINISH_DUMP = '021006';


/**
 * refinement data operation_log_detail (cycle activity), untuk first bucket dan full bucket
 * refinement data operation_actual_raw (ritase), untuk bucket count dan tonnage bucket
 */
module.exports.run = (mysql, mongo, clog, callback) => {

    /**
     * get hauler list by time and loader
     */
    async function getHaulerList(tm, loader) {
        const actuals = await util.mysqlQuery(my, "SELECT * FROM operation_actual \
            WHERE ");
    }

    /**
     * get heular with actual OR location NULL
     */
    async function getNullActual(my, cb) {
        const nullData = await util.mysqlQuery(my, "SELECT * FROM operation_log_detail \
            WHERE (id_actual IS NULL OR id_location IS NULL) AND id_equipment IS NOT NULL ORDER BY date ASC");
        cb(nullData);
    }


    /**
     * process cycle activity
     */
    async function getUncompleteCycleActivity(my, cb) {
        const cycle = await util.mysqlQuery(my, "SELECT * FROM operation_log_detail \
            WHERE id_actual IS NULL AND (cd_time='" + CD_FIRST_BUCKET + "' OR cd_time='" + CD_FULL_BUCKET + "') ORDER BY date ASC");
        cb(cycle);
    }

    /**
     * solve process
     */
    async function solveUncompleteCycleActivity(my, data, cb) {
        for (item of data) {
            const tm = item.date;
            const loader = item.id_loader;
            let tmStart = moment.unix(tm).startOf('day').unix();
            let tmEnd = moment.unix(tm).endOf('day').unix();
            const shift = util.util.getShiftByTime(tm);
            if (shift === util.SHIFT_NIGHT) {
                const tm6 = moment.unix(tm).set({ hour: 6, minute: 0 }).unix();
                if (tm < tm6) {
                    tmStart = moment.unix(tm).subtract(1, 'days').startOf('day').unix();
                }
            }
            const actuals = await util.mysqlQuery(my, "SELECT m.*,e.name hauler_name FROM operation_actual m LEFT JOIN equipment e ON e.id=m.id_hauler \
                WHERE m.id_loader='"+ loader + "' AND m.cd_shift='" + shift + "' AND m.date>=" + tmStart + " AND m.date<" + tmEnd);
            const hauler = actuals.map(item => {
                return {
                    id_actual: item.id,
                    id_hauler: item.id_hauler
                }
            });

            if (item.cd_time === CD_FIRST_BUCKET) {
                /**
                 * first bucket solver
                 */


                /**
                 * get queue order
                 *
                 * order date ASC, priority for the old event
                 */
                const strHauler = hauler.map(im => "'" + im.id_hauler + "'").join();
                const queuing = await util.mysqlQuery(my, "SELECT * FROM operation_log_detail WHERE cd_time='" + CD_ARRIVE_LOADING + "' \
                    AND date<"+ tm + " AND date>" + tmStart + " AND id_equipment IN (" + strHauler + ") ORDER BY date ASC LIMIT 5");

                /**
                 * jika :
                 * - tidak ada arrive loading dari hauler, maka data dianggap tidak valid, perlu dicek oleh dispatcher/operation
                 * - ditemukan 1, dapat dipastikan first bucket untuk hauler tersebut
                 * - ditemukan > 1, perlu proses lanjutan
                 */
                if (queuing.length === 0) {
                    clog.info('no solver found');

                } else if (queuing.length === 1) {
                    clog.info('solver is found');

                    // update cycle event
                    await util.mysqlQuery(my, "UPDATE operation_log_detail SET id_actual='" + queuing[0].id_actual
                        + "', id_equipment='" + queuing[0].id_equipment
                        + "', id_location='" + queuing[0].id_location + "' WHERE id='" + item.id + "'");

                } else if (queuing.length > 1) {
                    clog.info('found some', queuing.length);

                    // check last activity for each equipment
                    let found = false;

                    for (let ii = 0; ii < queuing.length; ii++) {

                        // pass if solver found
                        if (found) {
                            continue;
                        }

                        // get latest activity for current equipment
                        const latestEvent = await util.mysqlQuery(my, "SELECT * FROM operation_log_detail WHERE date<" + item.date + " AND id_equipment='" + queuing[ii].id_equipment + "' ORDER BY date DESC LIMIT 1");

                        if (latestEvent.length > 0) {

                            // choose the old arrive loading
                            if (latestEvent[0].cd_time === CD_ARRIVE_LOADING) {
                                found = true;

                                // update cycle event
                                await util.mysqlQuery(my, "UPDATE operation_log_detail SET id_actual='" + queuing[ii].id_actual
                                    + "', id_equipment='" + queuing[ii].id_equipment
                                    + "', id_location='" + queuing[ii].id_location + "' WHERE id='" + item.id + "'");
                            }

                        }

                    }
                }

            } else if (item.cd_time === CD_FULL_BUCKET) {


                /**
                 * get queue order
                 *
                 * order date ASC, priority for the old event
                 */
                const strHauler = hauler.map(im => "'" + im.id_hauler + "'").join();
                const queuing = await util.mysqlQuery(my, "SELECT * FROM operation_log_detail WHERE cd_time='" + CD_FIRST_BUCKET + "' \
                    AND date<"+ tm + " AND date>" + tmStart + " AND id_equipment IN (" + strHauler + ") ORDER BY date ASC LIMIT 5");

                /**
                 * jika :
                 * - tidak ada first bucket dari hauler, maka data dianggap tidak valid, perlu dicek oleh dispatcher/operation
                 * - ditemukan 1, dapat dipastikan full bucket untuk hauler tersebut
                 * - ditemukan > 1, perlu proses lanjutan
                 *
                 * logic mirip dengan penanganan FIRST BUCKET
                 *
                 */
                if (queuing.length === 0) {
                    clog.info('no solver found');

                } else if (queuing.length === 1) {
                    clog.info('solver is found');

                    // update cycle event
                    await util.mysqlQuery(my, "UPDATE operation_log_detail SET id_actual='" + queuing[0].id_actual
                        + "', id_equipment='" + queuing[0].id_equipment
                        + "', id_location='" + queuing[0].id_location + "' WHERE id='" + item.id + "'");

                } else if (queuing.length > 1) {
                    clog.info('found some', queuing.length);

                    // check last activity for each equipment
                    let found = false;

                    for (let ii = 0; ii < queuing.length; ii++) {

                        // pass if solver found
                        if (found) {
                            continue;
                        }

                        // get latest activity for current equipment
                        const latestEvent = await util.mysqlQuery(my, "SELECT * FROM operation_log_detail WHERE date<" + item.date + " AND id_equipment='" + queuing[ii].id_equipment + "' ORDER BY date DESC LIMIT 1");

                        if (latestEvent.length > 0) {

                            // choose the old arrive loading
                            if (latestEvent[0].cd_time === CD_FIRST_BUCKET) {
                                found = true;

                                // update cycle event
                                await util.mysqlQuery(my, "UPDATE operation_log_detail SET id_actual='" + queuing[ii].id_actual
                                    + "', id_equipment='" + queuing[ii].id_equipment
                                    + "', id_location='" + queuing[ii].id_location + "' WHERE id='" + item.id + "'");
                            }

                        }

                    }
                }
            }

        }

        cb();
    }

    async.waterfall([

        (cb) => {
            /**
             * refine id_actual & id_location NULL
             */
            getNullActual(mysql, (items) => {

                // console.log('found NULL actual', items.length);

                /**
                 * search actual for each log detail
                 */
                async.forEachOf(items, (item, key, _cb) => {

                    // prepare filter
                    const tm = item.date;
                    let tmStart = moment.unix(tm).startOf('day').unix();
                    let tmEnd = tm; //moment.unix(tm).endOf('day').unix();
                    const shift = util.util.getShiftByTime(tm);
                    if (shift === util.SHIFT_NIGHT) {
                        const tm6 = moment.unix(tm).set({ hour: 6, minute: 0 }).unix();
                        if (tm < tm6) {
                            tmStart = moment.unix(tm).subtract(1, 'days').startOf('day').unix();
                        }
                    }

                    // query get actual
                    util.mysqlQuery(mysql, "SELECT m.*,e.name hauler_name FROM operation_actual m LEFT JOIN equipment e ON e.id=m.id_hauler \
                        WHERE m.id_hauler='"+ item.id_equipment + "' AND m.cd_shift='" + shift + "' AND m.date>=" + tmStart + " AND m.date<" + tmEnd
                        + " ORDER BY m.date DESC ").then(actual => {

                            if (actual.length > 0) {

                                // update log detail
                                util.mysqlQuery(mysql, "UPDATE operation_log_detail SET id_actual='" + actual[0].id + "', id_location='"
                                    + actual[0].id_location + "' WHERE id='" + item.id + "'").then(updated => {
                                        _cb();
                                    });

                            } else {

                                _cb();

                            }
                        });

                }, (err) => {

                    clog.info('refine actual done');
                    cb();

                });

            });
        },

        (cb) => {
            /**
             * get data
             */
            getUncompleteCycleActivity(mysql, (res) => {
                cb(null, res);
            });
        },

        (data, cb) => {
            /**
             * solve data
             */
            solveUncompleteCycleActivity(mysql, data, (res) => {
                cb(null, res);
            });
        },

        (data, cb) => {
            /**
             * get data ritase NULL
             */

            cb();
        },

    ], (res) => {

        clog.info('finish refiner');

        callback();

    });

}