const constant = require('./constant');

module.exports = (dataSource, clog) => {
    const module = {};

    /**
     * Perhitungan AHP faktor
     * @param {*} dataSource
     * @param {*} gps
     * @param {*} counter
     */
    module.GPSHandler = (message, counter) => {

        clog.info('data', message.unit_id);

        /**
         * remove this dummy
         */
        const dummyJson = {
            "date": 1594283549,
            "area_name": "",
            "altitude": "0",
            "user_status": "",
            "device_status": {
                "memory_free": 6024.282112,
                "cpu_system_load": 3.08,
                "storage_free": 0,
                "storage_size": 0,
                "cpu_system": 0.38018628281117695,
                "cpu_process": 0.04572396274343776,
                "memory_size": 16703.717376
            },
            "latitude": "0.0",
            "actual_id": "",
            "unit_type": "015001",
            "operation_loader_id": "",
            "material_type": "",
            "actual_support_id": "",
            "ecus": {
                "date": "2020-07-09 15:32:29",
                "ecuHD785": {
                    "Eng_Speed": 0,
                    "TM_Out_Speed": 0,
                    "Vehicle_Speed_S": 0,
                    "Shift_Indicator": 0,
                    "Live_Weight": 0,
                    "Fuel_Injection": 0,
                    "Accel_Pos": 0,
                    "Retarder_F_Oit_temp": 0,
                    "Retarder_R_Oil_temp": 0,
                    "Fuel_Rate_01L": 0,
                    "Retarder_Pos": 0
                }
            },
            "user_id": "",
            "engine": "off",
            "ecu": {
                "hour_meter": {
                    "unit": "seconds",
                    "value": 0
                },
                "distance": {
                    "unit": "m",
                    "value": 0
                },
                "payload": {
                    "unit": "ton",
                    "value": 0
                },
                "engine_temperature": {
                    "unit": "C",
                    "value": 0
                },
                "fuel_rate": {
                    "unit": "liter",
                    "value": 0
                },
                "vehicle_speed": {
                    "unit": "km/hour",
                    "value": 0
                }
            },
            "user_login": false,
            "user_tum": "",
            "unit_id": "01a22d2a-7393-47f8-a85d-4430447c8d3b",
            "longitude": "0.0",
            "status": ""
        };

        /**
         * menghitung semua parameter optimasi
         */
        dataSource.computeDistance(dummyJson);
    };


    /**
     * Handler cycle data
     * @param {*} cycle
     */
    module.CycleHandler = (cycle) => {
        clog.info('handler', 'CycleHandler');

        const obj = JSON.parse(cycle);

        if (obj.message === constant.TIME_CYCLE_FULL_BUCKET) {
            // the message is from loader
            if (dataSource.ANTRIAN[obj.sender] !== undefined) {
                if (dataSource.ANTRIAN[obj.sender].indexOf(obj.destination) >= 0) {
                    dataSource.ANTRIAN[obj.sender].splice(obj.destination, 1);
                }
            }

        } else if (obj.message === constant.TIME_CYCLE_ARRIVE_LOADING) {
            // the message is from hauler
            if (dataSource.ANTRIAN[obj.destination] === undefined) {
                dataSource.ANTRIAN[obj.destination] = [];
            }

            if (dataSource.ANTRIAN[obj.destination].indexOf(obj.sender) < 0) {
                dataSource.ANTRIAN[obj.destination].push(obj.sender);
            }
        }

    };


    /**
     * Main handler
     * @param {*} factor
     * @param {*} dataSource
     * @param {*} topic
     * @param {*} message
     * @param {*} counter
     */
    module.prepareData = (factor, topic, message, counter) => {
        clog.info('handler', 'compute');

        for (let fac of factor) {
            if (fac.code === constant.FACTOR_DISTANCE) {
                if (topic === 'gps') {
                    module.GPSHandler(message, counter);
                }
            }
            if (fac.code === constant.FACTOR_PRIORITAS) {
                if (counter === constant.STEP_DURATION1) {
                    dataSource.getPrioritas();
                }
            }
            if (fac.code === constant.FACTOR_PRODUKTIVITAS) {
                if (counter === constant.STEP_DURATION2) {
                    dataSource.computeProductivity();
                }
            }
            if (fac.code === constant.FACTOR_HAULER) {
                if (counter === constant.STEP_DURATION3) {
                    dataSource.getHaulerNeed();
                }
            }
            if (fac.code === constant.FACTOR_ANTRIAN) {
                if (topic === 'cycle') {
                    module.CycleHandler(message);
                }
            }
        }

    }

    /**
     * compute AHP
     */
    module.compute = () => {
        console.log('loder position', dataSource.LOADER_POSITION);
        console.log('hauler position', dataSource.LOADER_HAULER);
        console.log('distance', dataSource.DISTANCE);
        console.log('prioritas', dataSource.PRIORITAS);
        console.log('produktivitas', dataSource.PRODUKTIVITAS);
        console.log('hauler', dataSource.HAULER);
        console.log('antrian', dataSource.ANTRIAN);
    }

    return module;
};