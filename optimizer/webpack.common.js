const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const JavaScriptObfuscator = require('webpack-obfuscator');

module.exports = {
    entry: {
        app: './optimizer.js',
    },
    plugins: [
        // new CleanWebpackPlugin(['dist/*']) for < v2 versions of CleanWebpackPlugin
        new CleanWebpackPlugin(),
        new JavaScriptObfuscator({
            rotateStringArray: true
        })
    ],
    output: {
        filename: 'fms.optimizer.js',
        path: path.resolve(__dirname, 'dist'),
    },
};