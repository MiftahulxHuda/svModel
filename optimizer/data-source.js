/**
 * DISTANCE
 * PRIORITAS
 * PRODUKTIVITAS
 * HAULER
 * ANTRIAN
 */

const constant = require('./constant');
const util = require('./util');
const moment = require('moment-timezone');

moment.tz.setDefault('Asia/Jakarta');

module.exports = function (mysql, mongo, config, clog) {
    const module = {};

    /**
     * gps position
     */
    module.LOADER_POSITION = {};
    module.HAULER_POSITION = {};

    /**
     * listen dari GPS
     * dengan format DISTANCE[hauler][loader]
     * re-compute setiap ada message gps
     */
    module.DISTANCE = {};

    /**
     * read langsung dari equipment
     * format data PRIORITAS[loader]
     */
    module.PRIORITAS = {};

    /**
     * read dari collection mongodb
     * re-compute setiap ada gps
     */
    module.PRODUKTIVITAS = {};

    /**
     * read dari hauler_need
     * format data HAULER[loader]
     */
    module.HAULER = {};

    /**
     * jumlah hauler yang antri pada loader bersangkutan
     * format data ANTRIAN[loader]
     */
    module.ANTRIAN = {};

    /**
     * recompute data
     * @param {*} gps
     */
    module.compute = function (gps) {
        this.computeDistance(gps);
    }

    /**
     * compute distance
     * run setiap ada socket gps
     */
    module.computeDistance = function (gp) {
        clog.info('data-source', 'computeDistance');

        // distance
        if (gp.unit_type === constant.UNIT_LOADER) {
            this.LOADER_POSITION[gp.unit_id] = {
                latitude: gp.latitude,
                longitude: gp.longitude
            };
        }
        if (gp.unit_type === constant.UNIT_HAULER) {
            this.HAULER_POSITION[gp.unit_id] = {
                latitude: gp.latitude,
                longitude: gp.longitude
            };
        }
        // compute distance
        for (let hauler of Object.keys(this.HAULER_POSITION)) {
            for (let loader of Object.keys(this.LOADER_POSITION)) {
                if (this.DISTANCE[hauler] === undefined) {
                    this.DISTANCE[hauler] = {}
                }
                this.DISTANCE[hauler][loader] = util.getDistance(
                    this.HAULER_POSITION[hauler].latitude,
                    this.HAULER_POSITION[hauler].longitude,
                    this.LOADER_POSITION[loader].latitude,
                    this.LOADER_POSITION[loader].longitude
                );
            }
        }
    }

    /**
     * data prioritas loader yang diset user
     * run dalam jangka waktu tertentu
     */
    module.getPrioritas = async function () {
        clog.info('data-source', 'getPriority');

        const rawData = await util.mysqlQuery(mysql, "SELECT id, priority FROM equipment WHERE cd_equipment='" + constant.UNIT_LOADER + "' ORDER BY id DESC");
        for (let eq of rawData) {
            this.PRIORITAS[eq.id] = eq.priority ? eq.priority : 0;
        }
    }

    /**
     * menghitung produktivitas
     * run dalam jangka waktu tertentu
     */
    module.computeProductivity = async function () {
        clog.info('data-source', 'computeProductivity');

        /**
         * productivity = produksi/working time
         *
         * step
         * - get total produksi
         * - gel total hm
         */

        // const shift = util.getCurrentShift();
        // let start;
        // if (shift === constant.SHIFT_DAY) {
        //     start = util.getShiftDayStartTime();
        // } else {
        //     start = util.getShiftNightStartTime();
        // }

        const tm = util.getYesterdayTime();
        const start = tm.start;

        const loaderProduction = await util.mongoAggregate(mongo, {
            database: config.mongodbConfig.database,
            collection: constant.PRODUCTION_RAW,
            option: [
                {
                    $match: {
                        start: {
                            $gte: start
                        }
                    }
                },
                {
                    $group: {
                        _id: {
                            loader: '$loader',
                            material: '$material'
                        },
                        duration: { $sum: '$duration' },
                        distance_survei: { $avg: '$distance_survei' },
                        distance_gps: { $avg: '$distance_gps' },
                        bucket_count: { $sum: '$bucket_count' },
                        tonnage_payload: { $sum: '$tonnage_payload' },
                        tonnage_ujipetik: { $sum: '$tonnage_ujipetik' },
                        tonnage_timbangan: { $sum: '$tonnage_timbangan' },
                        tonnage_bucket: { $sum: '$tonnage_bucket' }
                    }
                }
            ],
        });

        const loaderWorkingTime = await util.mongoAggregate(mongo, {
            database: config.mongodbConfig.database,
            collection: constant.ACTIVITY_RAW,
            option: [
                {
                    $match: {
                        start: {
                            $gte: start
                        },
                        'equipment.type': constant.UNIT_LOADER
                    }
                },
                {
                    $group: {
                        _id: {
                            loader: '$equipment'
                        },
                        duration: { $sum: '$duration' }
                    }
                }
            ],
        });

        /**
         * compute productivity
         */
        for (let loader of Object.keys(this.LOADER_POSITION)) {
            const rawProduction = loaderProduction.filter(item => item._id.loader.id === loader);
            const rawWorking = loaderWorkingTime.filter(item => item._id.loader.id === loader);

            if (rawProduction.length > 0 && rawWorking.length > 0) {
                const production = rawProduction.reduce((a, b) => {
                    /**
                     * belum ada konversi dari ton ke bcm atau sebaliknya
                     */
                    return {
                        tonnage_payload: (a.tonnage_payload + b.tonnage_payload),
                        tonnage_ujipetik: (a.tonnage_ujipetik + b.tonnage_ujipetik),
                        tonnage_timbangan: (a.tonnage_timbangan + b.tonnage_timbangan),
                        tonnage_bucket: (a.tonnage_bucket + b.tonnage_bucket)
                    }
                });
                const workingTime = rawWorking.reduce((c, d) => {
                    return {
                        duration: (c.duration + d.duration)
                    }
                });
                // konversi ke jam, 3600 second
                const workingTimeHour = workingTime.duration / 3600;

                this.PRODUKTIVITAS[loader] = {
                    payload: production.tonnage_payload / workingTimeHour,
                    ujipetik: production.tonnage_ujipetik / workingTimeHour,
                    timbangan: production.tonnage_timbangan / workingTimeHour,
                    bucket: production.tonnage_bucket / workingTimeHour
                };
            }
        }

        // console.log(this.PRODUKTIVITAS);
    }

    /**
     * jumlah hauler yang dibutuhkan loader
     * run dalam jangka waktu tertentu
     */
    module.getHaulerNeed = async function () {
        clog.info('data-source', 'getHaulerNeed');

        const maxdt = await util.mysqlQuery(mysql, 'SELECT MAX(date) dt FROM loader_need');
        let mdt = moment().unix();

        if (maxdt.length > 0) {
            mdt = moment.unix(maxdt[0].dt).startOf('day').unix();
        }

        const rawData = await util.mysqlQuery(mysql, 'SELECT * FROM loader_need WHERE date>=' + mdt + ' ORDER BY loader_id DESC');
        for (let eq of rawData) {
            this.HAULER[eq.loader_id] = eq.need_hauler;
        }
    }

    return module;
};