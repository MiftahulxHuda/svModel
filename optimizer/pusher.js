module.exports = function (producer, key) {
    const module = {};

    const partition = null;
    const headers = [
        { header: "header value" }
    ];

    module.send = function (topic, message) {
        const buffer = Buffer.from(JSON.stringify(message));
        producer.produce(topic, partition, buffer, key, Date.now(), "", headers);
    };

    return module;
};