/**
 * #FMS Optimizer
 *
 * optimizer bekerja dengan trigger dari kafka, setiap ada data masuk maka akan
 * dilakukan komputasi untuk mencari solusi terbaik, menggunakan metode AHP.
 *
 * hasil perhitungan akan diinfokan melalui kafka, beserta detail perhitungan.
 *
 * #Tipe Optimizer
 * - mencari jumlah optimal hauler yang dibutuhkan setiap loader, agar produksinya
 * optimum
 * - mencari loader terbaik untuk hauler yang akan beroperasi
 *
 * #Kebutuhan data
 * - jarak terdekat, jarak hauler ke setiap loader yang ada, dapat dihitung dari
 * titik gps antar unit
 * - prioritas loader, loader yang diprioritaskan untuk dipilih, ditentukan user
 * - produktivitas loader, hasil perhitungan dari produksi
 * - kebutuhan hauler, jumlah hauler yang dibutuhkan loader
 * - antrian hauler pada loader bersangkutan, jumlah hauler yang arrive loading dan
 * belum full bucket
 *
 */

const kafka = require('node-rdkafka');
const mysql = require('mysql');
const MongoClient = require('mongodb').MongoClient;
const async = require('async');
const winston = require('winston');
const moment = require('moment-timezone');
const learning = require('./learning')();
const fs = require('fs');
const constant = require('./constant');
require('winston-daily-rotate-file');

moment.tz.setDefault('Asia/Jakarta');

let consumer = null;
let producer = null;
let isProducerReady = false;
let connMysql = null;
let connMongo = null;
let pusher = null;
let dataSource = null;
let AHP = null;
let HANDLER = null;
let counter = 0;

const cfg = fs.readFileSync(__dirname + '/config.json').toString();
const config = JSON.parse(cfg);

const transport = new (winston.transports.DailyRotateFile)({
    filename: config.global.logdir + 'optimizer-%DATE%.log',
    datePattern: 'YYYY-MM-DD',
    zippedArchive: true,
    maxSize: '20m',
    maxFiles: '14d',
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.json()
    )
});
const clog = winston.createLogger({
    transports: [
        transport
    ]
});

function initConsumer(cb) {
    if (consumer !== null) {
        return consumer;

    } else {
        consumer = new kafka.KafkaConsumer({
            'client.id': 'optimizer-consumer',
            'debug': 'all',
            'metadata.broker.list': '68.183.186.116:8811,68.183.186.116:8812',
            'group.id': 'optimizer-consumer',
            'enable.auto.commit': true,
            'rebalance_cb': true,
            'security.protocol': 'SASL_PLAINTEXT',
            'sasl.mechanisms': 'PLAIN',
            'sasl.username': 'coba',
            'sasl.password': 'cobacoba',
        });

        consumer.on('event.log', function (log) {
            // console.log('event-log', log);
        });

        consumer.on('event.error', function (err) {
            console.error('Error from consumer', err);
        });

        consumer.on('ready', function (arg) {
            console.log('consumer ready');
            consumer.subscribe(config.topic);
            consumer.consume();
            cb();
        });

        consumer.on('data', function (m) {
            const factor = learning.getFactor();

            HANDLER.prepareData(factor, m.topic, JSON.parse(m.value.toString()), counter);

            HANDLER.compute();

            consumer.commit();

            counter++;

            if (counter > constant.TOTAL_DURATION) {
                counter = 0;
            }
        });

        consumer.on('disconnected', function (arg) {
            console.log('consumer disconnected. ');
        });

        consumer.connect();
    }
}

function initProducer(cb) {
    if (producer !== null) {
        return producer;

    } else {
        producer = new kafka.Producer({
            'client.id': 'optimizer-producer',
            'metadata.broker.list': '68.183.186.116:8811,68.183.186.116:8812',

            'compression.codec': 'lz4',

            'group.id': 'optimizer-producer',
            'enable.auto.commit': false,
            'security.protocol': 'SASL_PLAINTEXT',
            'sasl.mechanisms': 'PLAIN',
            'sasl.username': 'coba',
            'sasl.password': 'cobacoba',
            'dr_cb': true
        });

        producer.on('event.log', function (log) {
            // console.log('event-log', log);
        });

        producer.on('event.error', function (err) {
            console.error('Error from producer');
            console.error(err);
        });

        producer.on('ready', function (arg) {
            console.log('producer ready');
            isProducerReady = true;
            cb();
        });

        producer.on('disconnected', function (arg) {
            console.log('producer disconnected. ' + JSON.stringify(arg));
        });

        producer.connect();
    }
}

async.waterfall([

    (cb) => {
        /**
         * connect to MySQL
         */
        clog.info('-- connect to mysql');
        connMysql = mysql.createConnection(config.mysqlConfig);
        connMysql.connect(function (err) {
            if (err) {
                console.error('error connecting: ' + err.stack);
                throw err;
            }
            cb();
        });
    },

    (cb) => {
        /**
         * connect to MongoDB
         */
        clog.info('-- connect to mongodb');
        MongoClient.connect(config.mongodbConfig.url, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        }, function (err, db) {
            if (err) {
                console.error(err);
                throw err;
            }
            connMongo = db;
            cb();
        });
    },

    (cb) => {
        /**
         * learning process
         */
        AHP = learning.learning();
        dataSource = require('./data-source')(connMysql, connMongo, config, clog);
        HANDLER = require('./handler')(dataSource, clog);
        cb();
    },

    (cb) => {
        /**
         * init consumer
         */
        initProducer(() => {
            pusher = require('./pusher')(producer, config.key);
            cb();
        });
    },

    (cb) => {
        /**
         * init consumer
         */
        initConsumer(() => {
            cb();
        });
    },


], (res) => {

    console.log(AHP);

    console.log('Optimizer active');

    // pusher.send('gps', { data: 'coba coba' });

});