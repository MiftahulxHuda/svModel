const config = require('./config');
const request = require('request');
var tcpp = require('tcp-ping');

const TelegramBot = require('node-telegram-bot-api');

// replace the value below with the Telegram token you receive from @BotFather
const token = config.serviceConfig.telegram_token;

// Create a bot that uses 'polling' to fetch new updates
const bot = new TelegramBot(token, {polling: true});

setInterval(() => {
    for (let iService = 0; iService < config.serviceConfig.service.length; iService++) {
        const elService = config.serviceConfig.service[iService];
    
        let date = new Date();
        if(typeof(elService.port) == 'object') {
            for (let iPort = 0; iPort < elService.port.length; iPort++) {
                const elPort = elService.port[iPort];
    
                tcpp.probe(elService.ip_address, elPort, function(err, available) {
                    if(available == false) {
                        bot.sendMessage(config.serviceConfig.channel_id, `Service: ${elService.name}\n` +
                        `IP Address: ${elService.ip_address}\n` + `Port: ${elPort}\n` +
                        `Date: ${date.getDate()}-${(date.getMonth() + 1)}-${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}\n`
                        + `Status: Offline`);
                    }
                });
            }
        } else {
            tcpp.probe(elService.ip_address, elService.port, function(err, available) {
                if(available == false) {
                    bot.sendMessage(config.serviceConfig.channel_id, `Service: ${elService.name}\n` +
                    `IP Address: ${elService.ip_address}\n` + `Port: ${elService.port}\n` +
                    `Date: ${date.getDate()}-${(date.getMonth() + 1)}-${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}\n`
                    + `Status: Offline`);
                }
            });
        }
    }
}, config.serviceConfig.interval);