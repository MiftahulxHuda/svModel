var mqtt = require('mqtt');
var client = mqtt.connect("mqtt://192.168.100.177");
var jwt = require('jsonwebtoken');

client.on("connect", function() {
    console.log("connected");
    var topic_list = ["auth"];
    client.subscribe(topic_list);
});

client.on("error", function(error) {
    console.log("Can't connect " + error);
});

client.on('message', function (topic, message) {
    switch(topic) {
        case 'auth':
            jwt.verify(JSON.parse(message).token, 'turatech', (err, decoded) => {
                if (err) {
                    console.log(err);
                    client.end();
                } else {
                    console.log(decoded);
                }
            });
    }
});