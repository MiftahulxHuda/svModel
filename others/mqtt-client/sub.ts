var mqtt = require('mqtt');
var client = mqtt.connect("mqtt://192.168.100.177");
var jwt = require('jsonwebtoken');

client.on("connect", function() {
    console.log("connected");
    var topic_list = ["myTopic"];
    client.subscribe(topic_list);
});

client.on("error", function(error) {
    console.log("Can't connect " + error);
});

client.on('message', function (topic, message) {
    switch(topic) {
        case 'myTopic':
            console.log(topic, message.toString());
    }
});