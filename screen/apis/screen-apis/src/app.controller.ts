import { Controller, Get, UseGuards, Req } from '@nestjs/common';
import { AppService } from './app.service';
import { ConfigService } from 'nestjs-config';
import { ApiOperation } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './modules/auth/auth.service';

@Controller()
export class AppController {
    constructor(private readonly appService: AppService, private readonly config: ConfigService, private readonly auth: AuthService) { }

    @Get()
    getHello(): string {
        return this.appService.getHello();
    }

    @Get('secure')
    @ApiOperation({ title: "Test secure api" })
    @UseGuards(AuthGuard('jwt'))
    async getSecure(@Req() req) {
        const user = req.user;
        return user;
    }
}
