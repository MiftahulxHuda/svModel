import { Controller, Get, Res, HttpStatus } from '@nestjs/common';
import { ApiUseTags } from '@nestjs/swagger';
import { KafkaService } from './kafka.service';

@Controller('kafka')
@ApiUseTags('kafka')
export class KafkaController {
    constructor(private kafkaService: KafkaService) { }

    @Get('status')
    async getStatus(@Res() res) {
        this.kafkaService.check(function (status) {
            return res.status(HttpStatus.OK).json(status);
        });
    }

}