import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Operator } from './operator.interface';
import { CreateOperatorDTO } from './operator.dto';

@Injectable()
export class OperatorService {
    constructor(@InjectModel('Operator') private readonly operatorModel: Model<Operator>) { }

    async findAll(): Promise<Operator[]> {
        return await this.operatorModel.find().exec();
    }

    async find(operatorID): Promise<CreateOperatorDTO> {
        const operator = await this.operatorModel.findById(operatorID).exec();
        return operator;
    }

    async create(createOperatorDTO: CreateOperatorDTO): Promise<Operator> {
        const created = new this.operatorModel(createOperatorDTO);
        return await created.save();
    }

    async createIfNotExists(createOperatorDTO: CreateOperatorDTO): Promise<Operator> {
        const find = await this.operatorModel.findById(createOperatorDTO._id);
        if (find == null) {
            const created = new this.operatorModel(createOperatorDTO);
            return await created.save();
        } else {
            return Promise.resolve(find);
        }
    }

    async update(operatorID, createOperatorDTO: CreateOperatorDTO): Promise<Operator> {
        const updated = await this.operatorModel.findOneAndUpdate(operatorID, createOperatorDTO, { new: true });
        return updated;
    }

    async delete(operatorID): Promise<Operator> {
        const deleted = await this.operatorModel.findOneAndRemove(operatorID);
        return deleted;
    }

}
