import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { OperatorSchema } from './operator.schema';
import { OperatorService } from './operator.service';
import { OperatorController } from './operator.controller';

@Module({
  imports: [ MongooseModule.forFeature([{ name: 'Operator', schema: OperatorSchema }]) ],
  controllers: [ OperatorController ],
  providers: [ OperatorService ],
})
export class OperatorModule {}