import * as mongoose from 'mongoose';

export const OperatorSchema = new mongoose.Schema({
  _id: String,
  nik: String,
  name: String
});