import { Controller, Get, Res, HttpStatus, Post, Body, Put, Query, NotFoundException, Delete, Param } from '@nestjs/common';
import { CreateOperatorDTO } from './operator.dto';
import { OperatorService } from './operator.service';
import { ApiUseTags, ApiImplicitParam, ApiImplicitBody } from '@nestjs/swagger';

@Controller('operators')
@ApiUseTags('operators')
export class OperatorController {
    constructor(private operatorService: OperatorService) { }

    @Get()
    async getAllOperator(@Res() res) {
        const operators = await this.operatorService.findAll();
        return res.status(HttpStatus.OK).json(operators);
    }

    @Get(':operatorID')
    @ApiImplicitParam({ name: 'operatorID' })
    async getOperator(@Res() res, @Param('operatorID') operatorID) {
        const operator = await this.operatorService.find(operatorID);
        if (!operator) throw new NotFoundException('Operator does not exist!');
        return res.status(HttpStatus.OK).json(operator);
    }

    @Post()
    async addOperator(@Res() res, @Body() createOperatorDTO: CreateOperatorDTO) {
        const operator = await this.operatorService.create(createOperatorDTO);
        return res.status(HttpStatus.OK).json({
            message: "Operator has been created successfully",
            operator
        })
    }

    @Put()
    @ApiImplicitParam({ name: 'operatorID' })
    async updateOperator(@Res() res, @Param('operatorID') operatorID, @Body() createOperatorDTO: CreateOperatorDTO) {
        const operator = await this.operatorService.update(operatorID, createOperatorDTO);
        if (!operator) throw new NotFoundException('Operator does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Operator has been successfully updated',
            operator
        })
    }

    @Delete()
    @ApiImplicitParam({ name: 'operatorID' })
    async deleteOperator(@Res() res, @Param('operatorID') operatorID) {
        const operator = await this.operatorService.delete(operatorID);
        if (!operator) throw new NotFoundException('Operator does not exist');
        return res.status(HttpStatus.OK).json({
            message: 'Operator has been deleted',
            operator
        })
    }    

}