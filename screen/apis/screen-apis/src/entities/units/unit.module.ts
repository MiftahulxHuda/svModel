import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UnitSchema } from './unit.schema';
import { UnitService } from './unit.service';
import { UnitController } from './unit.controller';

@Module({
  imports: [ MongooseModule.forFeature([{ name: 'Unit', schema: UnitSchema }]) ],
  controllers: [ UnitController ],
  providers: [ UnitService ],
})
export class UnitsModule {}