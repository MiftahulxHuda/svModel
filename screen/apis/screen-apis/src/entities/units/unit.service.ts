import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Unit } from './unit.interface';
import { CreateUnitDTO } from './unit.dto';

@Injectable()
export class UnitService {
    constructor(@InjectModel('Unit') private readonly unitModel: Model<Unit>) { }

    async findAll(): Promise<Unit[]> {
        return await this.unitModel.find().exec();
    }

    async find(unitID): Promise<CreateUnitDTO> {
        const found = await this.unitModel.findById(unitID).exec();
        return found;
    }

    async create(createUnitDTO: CreateUnitDTO): Promise<Unit> {
        const created = new this.unitModel(createUnitDTO);
        return await created.save();
    }

    async createIfNotExists(createUnitDTO: CreateUnitDTO): Promise<Unit> {
        const find = await this.unitModel.findById(createUnitDTO._id);
        if (find == null) {
            const created = new this.unitModel(createUnitDTO);
            return await created.save();
        } else {
            return Promise.resolve(find);
        }
    }

    async update(unitID, createUnitDTO: CreateUnitDTO): Promise<Unit> {
        const updated = await this.unitModel.findOneAndUpdate(unitID, createUnitDTO, { new: true });
        return updated;
    }

    async delete(unitID): Promise<Unit> {
        const deleted = await this.unitModel.findOneAndRemove(unitID);
        return deleted;
    }

}
