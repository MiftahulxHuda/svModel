import { Controller, Get, Res, HttpStatus, Post, Body, Put, Query, NotFoundException, Delete, Param } from '@nestjs/common';
import { ApiUseTags, ApiImplicitParam, ApiImplicitBody } from '@nestjs/swagger';
import { CreateUnitDTO } from './unit.dto';
import { UnitService } from './unit.service';

@Controller('units')
@ApiUseTags('units')
export class UnitController {
    constructor(private unitService: UnitService) { }

    @Get()
    async getAllUnit(@Res() res) {
        const units = await this.unitService.findAll();
        return res.status(HttpStatus.OK).json(units);
    }

    @Get(':unitID')
    @ApiImplicitParam({ name: 'unitID' })
    async getUnit(@Res() res, @Param('unitID') unitID) {
        const unit = await this.unitService.find(unitID);
        if (!unit) throw new NotFoundException('Unit does not exist!');
        return res.status(HttpStatus.OK).json(unit);
    }

    @Post()
    async addUnit(@Res() res, @Body() createUnitDTO: CreateUnitDTO) {
        const unit = await this.unitService.create(createUnitDTO);
        return res.status(HttpStatus.OK).json({
            message: "Unit has been created successfully",
            unit
        })
    }

    @Put()
    @ApiImplicitParam({ name: 'unitID' })
    async updateUnit(@Res() res, @Param('unitID') unitID, @Body() createUnitDTO: CreateUnitDTO) {
        const unit = await this.unitService.update(unitID, createUnitDTO);
        if (!unit) throw new NotFoundException('Unit does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Unit has been successfully updated',
            unit
        })
    }

    @Delete()
    @ApiImplicitParam({ name: 'unitID' })
    async deleteUnit(@Res() res, @Param('unitID') unitID) {
        const unit = await this.unitService.delete(unitID);
        if (!unit) throw new NotFoundException('Unit does not exist');
        return res.status(HttpStatus.OK).json({
            message: 'Unit has been deleted',
            unit
        })
    }    

}