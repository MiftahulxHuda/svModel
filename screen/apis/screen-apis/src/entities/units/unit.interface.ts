import { Document } from 'mongoose';

export interface Unit extends Document {
    readonly _id: String;
    readonly name: String;
}