import { ApiModelProperty } from '@nestjs/swagger';

export class CreateUnitDTO {
    @ApiModelProperty()
    readonly _id: String;

    @ApiModelProperty()
    readonly name: String;
}