import * as mongoose from 'mongoose';

export const UnitSchema = new mongoose.Schema({
  _id: String,
  name: String
});