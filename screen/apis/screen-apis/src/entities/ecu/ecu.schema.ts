import * as mongoose from 'mongoose';

export const ECUSchema = new mongoose.Schema({
  date: Date,
  unit_id: String,
  shift: String,
  plan_id: String,
  ecu: Object
});