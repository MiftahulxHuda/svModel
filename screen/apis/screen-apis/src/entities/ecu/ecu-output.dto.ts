import { CreateECUDTO } from "./ecu.dto";

export class ECUOutput {
  output: CreateECUDTO[];
  message: string;
  response: string;
}

export class ECUOneOutput {
    output: CreateECUDTO;
    message: string;
    response: string;
}