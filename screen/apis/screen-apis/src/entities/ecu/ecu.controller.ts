import { Controller, Get, Res, HttpStatus, Post, Body, Put, Query, NotFoundException, Delete, Param } from '@nestjs/common';
import { CreateECUDTO } from './ecu.dto';
import { ECUService } from './ecu.service';
import { ApiUseTags, ApiImplicitParam, ApiImplicitBody, ApiOperation } from '@nestjs/swagger';

@Controller('ecu')
@ApiUseTags('ecu')
export class ECUController {
    constructor(private ecuService: ECUService) { }

    @Get()
    @ApiOperation({ title: "Select bulk data" })
    async getAllECU(@Res() res) {
        const ecu = await this.ecuService.findAll();
        return res.status(HttpStatus.OK).send(ecu);
    }

    @Get(':id')
    @ApiOperation({ title: "Select by id" })
    @ApiImplicitParam({ name: 'id' })
    async getECU(@Res() res, @Param('id') id) {
        const ecu = await this.ecuService.find(id);
        return res.status(HttpStatus.OK).send(ecu);
    }

    @Post()
    @ApiOperation({ title: "Insert single data" })
    async addECU(@Res() res, @Body() createECUDTO: CreateECUDTO) {
        const ecu = await this.ecuService.create(createECUDTO);
        return res.status(HttpStatus.OK).send(ecu)
    }

    @Put(':id')
    @ApiOperation({ title: "Update single data" })
    @ApiImplicitParam({ name: 'id' })
    async updateECU(@Res() res, @Param('id') id, @Body() updateECUDTO: CreateECUDTO) {
        const ecu = await this.ecuService.update(id, updateECUDTO);
        return res.status(HttpStatus.OK).send(ecu)
    }

    @Delete(':id')
    @ApiOperation({ title: "Delete by id" })
    @ApiImplicitParam({ name: 'id' })
    async deleteECU(@Res() res, @Param('id') id) {
        const ecu = await this.ecuService.delete(id);
        return res.status(HttpStatus.OK).send(ecu)
    }    

}