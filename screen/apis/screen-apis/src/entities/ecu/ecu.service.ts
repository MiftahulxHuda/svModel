import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ECU } from './ecu.interface';
import { CreateECUDTO } from './ecu.dto';
import { ECUOutput, ECUOneOutput } from './ecu-output.dto';

@Injectable()
export class ECUService {
  constructor(@InjectModel('ECU') private readonly ecuModel: Model<ECU>) {}

  async findAll(): Promise<ECUOutput> {
    let output = new ECUOutput();

    try {
        const data = await this.ecuModel.find().exec();
        output.response = "success";
        output.message = "Data Recevied";
        output.output = data;
        return output;
    } catch(error) {
        output.response = "error";
        output.message = error;
        output.output = null;
        return output;
    }
  }
  
  async find(ecuID): Promise<ECUOneOutput> {
    let output = new ECUOneOutput();

    try {
        const data = await this.ecuModel.findById(ecuID).exec();
        output.response = "success";
        output.message = "Data Recevied";
        output.output = data;
        return output;
    } catch(error) {
        output.response = "error";
        output.message = error;
        output.output = null;
        return output;
    }
  }

  async create(createECUDTO: CreateECUDTO): Promise<ECUOneOutput> {
    let output = new ECUOneOutput();

    try {
        const created = await new this.ecuModel(createECUDTO).save();
        output.response = "success";
        output.message = "Data Saved";
        output.output = created;
        return output;
    } catch(error) {
        output.response = "error";
        output.message = error;
        output.output = null;
        return output;
    }
  }

  async update(ecuID, updateECUDTO: CreateECUDTO): Promise<ECUOneOutput> {
    let output = new ECUOneOutput();

    try {
        const findById = await this.find(ecuID);
        if(findById['output']) {
            const updated = await this.ecuModel.updateOne({_id: ecuID}, updateECUDTO);
            output.response = "success";
            output.message = "Data Updated";
            output.output = updateECUDTO;
            return output;
        } else {
            return findById;
        }
    } catch(error) {
        output.response = "error";
        output.message = error;
        output.output = null;
        return output;
    }
  }

  async delete(ecuID): Promise<ECUOneOutput> {
    let output = new ECUOneOutput();

    try {
        const findById = await this.find(ecuID);
        if(findById['output']) {
            const deleted = await this.ecuModel.deleteOne({_id: ecuID});
            output.response = "success";
            output.message = "Data Deleted";
            output.output = deleted;
            return output;
        } else {
            return findById;
        }
    } catch(error) {
        output.response = "error";
        output.message = error;
        output.output = null;
        return output;
    }
  }

}
