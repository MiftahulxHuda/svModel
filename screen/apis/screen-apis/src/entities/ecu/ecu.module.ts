import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ECUSchema } from './ecu.schema';
import { ECUService } from './ecu.service';
import { ECUController } from './ecu.controller';

@Module({
  imports: [ MongooseModule.forFeature([{ name: 'ECU', schema: ECUSchema }]) ],
  controllers: [ ECUController ],
  providers: [ ECUService ],
})
export class ECUModule {}