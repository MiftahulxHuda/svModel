import { ApiModelProperty } from '@nestjs/swagger';

export class detailECUDTO {

    @ApiModelProperty()
    readonly hour_meter: Object;    

    @ApiModelProperty()
    readonly fuel_rate: Object;

    @ApiModelProperty()
    readonly distance: Object;    

    @ApiModelProperty()
    readonly engine_temperature: Object;    

    @ApiModelProperty()
    readonly payload: Object;    

    @ApiModelProperty()
    readonly vehicle_speed: Object;    
}

export class CreateECUDTO {

    @ApiModelProperty()
    readonly date: Date;

    @ApiModelProperty()
    readonly unit_id: String;

    @ApiModelProperty()
    readonly shift: String;

    @ApiModelProperty()
    readonly plan_id: String;

    @ApiModelProperty()
    readonly ecu: detailECUDTO;
}