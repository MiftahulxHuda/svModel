import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Put, Query, Req, Res, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiImplicitQuery, ApiOperation, ApiUseTags } from '@nestjs/swagger';
import { ServiceStatusService } from './service-status.service';


@ApiBearerAuth()
@ApiUseTags('service-status')
@Controller('service-status')
// @UseGuards(AuthGuard('jwt'))
export class ServiceStatusController {
    constructor(
        private readonly serviceStatusService: ServiceStatusService
    ) {

    }

    @ApiOperation({ title: 'Select data' })
    @Get()
    async getPM2Process(
        @Res() res, @Req() req) {
        let output = await this.serviceStatusService.getPM2Process();
        return res.status(HttpStatus.OK).send(output);
    }
}