import { Injectable } from "@nestjs/common";
let pm2 = require('pm2');

@Injectable()
export class ServiceStatusService {

    constructor(
    ) { }

    async getPM2Process() {
        const processes = await this.getProcesses();
        console.log({
            message: "processes",
            data: processes
        })
        return processes;
    }

    getProcesses() {
        return new Promise(resolve => {
            pm2.connect(function(err) {
                if (err) {
                    console.error(err);
                    process.exit(2);
                }
                
                pm2.list((err, list) => {
                    console.log(err, list)
                    resolve(list)
                })
            })
        })
    }
}