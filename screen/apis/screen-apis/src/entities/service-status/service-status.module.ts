import { forwardRef, Module } from '@nestjs/common';
import { ServiceStatusController } from './servuce-status.controller';
import { ServiceStatusService } from './service-status.service';

@Module({
    imports: [],
    exports: [],
    controllers: [ServiceStatusController],
    providers: [ServiceStatusService],
})
export class ServiceStatusModule {

}
