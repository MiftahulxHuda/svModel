import { Controller, Get, HttpStatus, Query, Req, Res, Param } from '@nestjs/common';
import { ApiImplicitQuery, ApiUseTags, ApiImplicitParam, ApiOperation } from '@nestjs/swagger';
import { UnitService } from './unit.service';

@Controller('unit')
@ApiUseTags('unit')
export class UnitController {

    constructor(private unit: UnitService) { }

    @Get()
    @ApiOperation({ title: "Get data unit or operation time of unit" })
    @ApiImplicitQuery({ name: 'unit', required: false, description: 'ID of unit' })
    async getUnit(@Res() res, @Req() req, @Query('unit') unitID) {
        /**
         * if unit_id empty
         *      return list available data unit
         * else
         *      return data operation unit each shift
         */
        if (unitID === null || unitID === undefined) {
            const unitList = await this.unit.findAll();
            return res.status(HttpStatus.OK).json(unitList);
        } else {
            const oprList = await this.unit.getOperationDate(unitID);
            return res.status(HttpStatus.OK).json(oprList);
        }
    }

    @Get('gps/:unit')
    @ApiOperation({ title: "Get list data GPS unit at date" })
    @ApiImplicitParam({ name: 'unit', required: true })
    @ApiImplicitQuery({ name: 'date', required: false })
    @ApiImplicitQuery({ name: 'dateend', required: false })
    async getGPS(@Res() res, @Req() req, @Param('unit') unitID, @Query('date') operationDate, @Query('dateend') operationDateEnd) {
        /**
         * if date empty
         *      return list data gps last shift
         * if dateend empty
         *      return list data gps at date
         * if dateend not empty
         *      return list data gps at range date to dateend
         */
        return res.status(HttpStatus.OK).json([]);
    }

    @Get('ecu/:unit')
    @ApiOperation({ title: "Get list data ECU unit at date" })
    @ApiImplicitParam({ name: 'unit', required: true })
    @ApiImplicitQuery({ name: 'date', required: false })
    @ApiImplicitQuery({ name: 'dateend', required: false })
    async getECU(@Res() res, @Req() req, @Param('unit') unitID, @Query('date') operationDate, @Query('dateend') operationDateEnd) {
        /**
         * if date empty
         *      return data ecu last shift
         * if dateend empty
         *      return data ecu at date
         * if dateend not empty
         *      return data ecu at range date to dateend
         */
        return res.status(HttpStatus.OK).json([]);
    }

}