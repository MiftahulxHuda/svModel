import { ApiModelProperty } from '@nestjs/swagger';

export class CreateUnitDTO {
    @ApiModelProperty()
    readonly name: String;

    @ApiModelProperty()
    readonly age: Number;
}