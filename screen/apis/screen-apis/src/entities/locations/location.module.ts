import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { LocationSchema } from './location.schema';
import { LocationService } from './location.service';
import { LocationController } from './location.controller';

@Module({
  imports: [ MongooseModule.forFeature([{ name: 'Location', schema: LocationSchema }]) ],
  controllers: [ LocationController ],
  providers: [ LocationService ],
})
export class LocationModule {}