import { Document } from 'mongoose';

export interface Location extends Document {
    readonly _id: String;
    readonly area: String;
    readonly fleet: String;
}