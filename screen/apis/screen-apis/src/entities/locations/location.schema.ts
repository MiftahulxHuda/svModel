import * as mongoose from 'mongoose';

export const LocationSchema = new mongoose.Schema({
  _id: String,
  area: String,
  fleet: String
});