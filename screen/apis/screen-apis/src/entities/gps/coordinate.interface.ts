import { Document } from 'mongoose';

export interface Coordinate extends Document {
    readonly lat: Number;
    readonly lon: Number;
}