import { ApiModelProperty } from '@nestjs/swagger';

export class CoordinateDTO {

    @ApiModelProperty()
    readonly lat: Number;

    @ApiModelProperty()
    readonly lon: Number;
}