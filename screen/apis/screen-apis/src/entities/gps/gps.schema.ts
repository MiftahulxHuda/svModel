import * as mongoose from 'mongoose';

export const GPSSchema = new mongoose.Schema({
    date: Number,
    actual_id: String,
    actual_support_id: String,
    operation_loader_id: String,
    unit_id: String,
    unit_type: String,
    user_id: String,
    latitude: Number,
    longitude: Number,
    altitude: Number,
    user_login: Boolean,
    user_status: String,
    user_tum: String,
    engine: String, // on/off
    status: String, // queuing /spoting /loading /fulltravel /spoting /dumping /emptytravel
    ecu: Object
});