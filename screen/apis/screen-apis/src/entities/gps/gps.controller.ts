import { Controller, Get, Res, HttpStatus, Post, Body, Put, Query, NotFoundException, Delete, Param } from '@nestjs/common';
import { CreateGPSDTO } from './gps.dto';
import { GPSService } from './gps.service';
import { ApiUseTags, ApiImplicitParam, ApiImplicitBody, ApiOperation, ApiImplicitQuery } from '@nestjs/swagger';

@Controller('gps')
@ApiUseTags('gps')
export class GPSController {
    constructor(private gpsService: GPSService) { }

    @Get()
    @ApiOperation({ title: "Select bulk data" })
    @ApiImplicitQuery({ name: 'size', required: false })
    async getAllGPS(@Res() res, @Query('size') size: number) {
        const gps = await this.gpsService.findAll(size);
        return res.status(HttpStatus.OK).send(gps);
    }

    @ApiOperation({ title: "Select bulk data" })
    @Get("paging")
    @ApiImplicitQuery({ name: 'time_start', type: Number })
    @ApiImplicitQuery({ name: 'time_end', type: Number })
    @ApiImplicitQuery({ name: 'limit', type: Number })
    @ApiImplicitQuery({ name: 'page', type: Number })
    async getGPSWithPagging(
        @Res() res,
        @Query('time_start') time_start: number,
        @Query('time_end') time_end: number,
        @Query('limit') limit: number,
        @Query('page') page: number,
    ) {
        const gps = await this.gpsService.findGPSWithPagging({
            time_start: time_start,
            time_end: time_end,
            take: Number(limit),
            skip: Number(limit * Math.abs(page - 1)),
            page: Number(page),
        });
        return res.status(HttpStatus.OK).send(gps);
    }

    @ApiOperation({ title: "Select bulk data" })
    @Get("time-min-max")
    async getTimeMinAndMax(@Res() res) {
        const gps = await this.gpsService.findTimeMinAndMax();
        return res.status(HttpStatus.OK).send(gps);
    }

    @Get('actual-id/:id')
    @ApiOperation({ title: "Select bulk data" })
    @ApiImplicitParam({ name: 'id' })
    @ApiImplicitQuery({ name: 'size', required: false })
    async getByActualId(@Res() res, @Param('id') id, @Query('size') size: number) {
        const gps = await this.gpsService.findByActualId(id, size);
        return res.status(HttpStatus.OK).send(gps);
    }

    @Get('actual-support-id/:id')
    @ApiOperation({ title: "Select bulk data" })
    @ApiImplicitParam({ name: 'id' })
    @ApiImplicitQuery({ name: 'size', required: false })
    async getBySupportId(@Res() res, @Param('id') id, @Query('size') size: number) {
        const gps = await this.gpsService.findBySupportId(id, size);
        return res.status(HttpStatus.OK).send(gps);
    }

    @Get('operation-loader-id/:id')
    @ApiOperation({ title: "Select bulk data" })
    @ApiImplicitParam({ name: 'id' })
    @ApiImplicitQuery({ name: 'size', required: false })
    async getByOLoaderId(@Res() res, @Param('id') id, @Query('size') size: number) {
        const gps = await this.gpsService.findByOLoaderId(id, size);
        return res.status(HttpStatus.OK).send(gps);
    }

    @Get('unit-id/:id')
    @ApiOperation({ title: "Select bulk data" })
    @ApiImplicitParam({ name: 'id' })
    @ApiImplicitQuery({ name: 'size', required: false })
    async getByUnitId(@Res() res, @Param('id') id, @Query('size') size: number) {
        const gps = await this.gpsService.findByUnitId(id, size);
        return res.status(HttpStatus.OK).send(gps);
    }

    @Get(':id')
    @ApiOperation({ title: "Select by id" })
    @ApiImplicitParam({ name: 'id' })
    async getGPS(@Res() res, @Param('id') id) {
        const gps = await this.gpsService.find(id);
        return res.status(HttpStatus.OK).send(gps);
    }

    @Post()
    @ApiOperation({ title: "Insert single data" })
    async addGPS(@Res() res, @Body() data: any) {
        if (data.json) {
            const parsed = JSON.parse(data.json);
            const gps = await this.gpsService.create(parsed);
            return res.status(HttpStatus.OK).send(gps);
        } else {
            return res.status(HttpStatus.OK).send({ response: 'error' });
        }
    }

    /**
     * Not Used
     */

    // @Put(':id')
    // @ApiOperation({ title: "Update single data" })
    // @ApiImplicitParam({ name: 'id' })
    // async updateGPS(@Res() res, @Param('id') id, @Body() updateGPSDTO: CreateGPSDTO) {
    //     const gps = await this.gpsService.update(id, updateGPSDTO);
    //     return res.status(HttpStatus.OK).send(gps);
    // }

    // @Delete(':id')
    // @ApiOperation({ title: "Delete by id" })
    // @ApiImplicitParam({ name: 'id' })
    // async deleteGPS(@Res() res, @Param('id') id) {
    //     const gps = await this.gpsService.delete(id);
    //     return res.status(HttpStatus.OK).send(gps);
    // }

}