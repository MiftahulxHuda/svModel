import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { GPSController } from './gps.controller';
import { GPSSchema } from './gps.schema';
import { GPSService } from './gps.service';

@Module({
    imports: [MongooseModule.forFeature([{ name: 'GPS', schema: GPSSchema }])],
    controllers: [GPSController],
    providers: [GPSService]
})
export class GPSModule { }