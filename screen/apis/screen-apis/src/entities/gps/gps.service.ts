import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateGPSDTO } from './gps.dto';
import { GPS } from './gps.interface';
import { GPSOutput, GPSOneOutput } from './gps-output.dto';

@Injectable()
export class GPSService {
    constructor(@InjectModel('GPS') private readonly gpsModel: Model<GPS>) { }

    async findAll(sz): Promise<GPSOutput> {
        let output = new GPSOutput();

        try {
            const data = await this.gpsModel.find(null, null, { sort: { date: -1 }, limit: parseInt(sz) }).exec();
            output.response = "success";
            output.message = "Data Recevied";
            output.output = data;
            return output;
        } catch (error) {
            output.response = "error";
            output.message = error;
            output.output = null;
            return output;
        }
    }

    async findGPSWithPagging(option) {
        console.log({
            message: "findGPSWithPagging",
            data: option
        })
        let output: any = {};

        try {
            // let data = this.gpsModel.find(null, null, { sort: { date: -1 }, skip: option.skip, limit: parseInt(option.take) }).exec();
            let params: any = {};
            if(option.time_start && option.time_end) {
                params = {
                    start: {
                        $gte: option.time_start,
                        $lt: option.time_end
                    },
                }
            }
            
            let data = this.gpsModel.find(params).sort({ date: -1});
            if(option.skip) {
                data = data.skip(option.skip)
            }

            if(option.take) {
                data = data.limit(parseInt(option.take))
            }

            data = await data.exec();
            let totalItems = await this.gpsModel.aggregate( [
                { $count: "total_items" }
            ]).exec();

            if(totalItems.length > 0) {
                totalItems = totalItems[0]["total_items"];
            } else {
                totalItems = 0;
            }

            output.output = data;
            output.response = 'success';
            output.message = 'Data Retrieved';
            output.pages = Math.round(data.length / option.take);
            output.limit = option.limit;
            output.totalItems = totalItems;
            output.currentPage = option.page
            return output;
        } catch (error) {
            output.response = "error";
            output.message = error;
            output.output = null;
            return output;
        }
    }

    async findTimeMinAndMax() {
        let output: any = {};

        try {
            const min = await this.gpsModel.findOne(null, null, { sort: { date: 1 }}).exec();
            const max = await this.gpsModel.findOne(null, null, { sort: { date: -1 }}).exec();

            // let totalItems = await this.gpsModel.aggregate( [
            //     { $count: "total_items" }
            // ]).exec();

            // if(totalItems.length > 0) {
            //     totalItems = totalItems[0]["total_items"];
            // } else {
            //     totalItems = 0;
            // }

            output.output = {
                min: min ? min['date'] : '',
                max: max ? max['date'] : ''
            };
            output.response = 'success';
            output.message = 'Data Retrieved';
            // output.pages = Math.round(data.length / option.take);
            // output.limit = option.limit;
            // output.totalItems = totalItems;
            // output.currentPage = option.page
            return output;
        } catch (error) {
            output.response = "error";
            output.message = error;
            output.output = null;
            return output;
        }
    }

    async findByActualId(id: string, sz): Promise<GPSOutput> {
        let output = new GPSOutput();

        try {
            const data = await this.gpsModel.find({ actual_id: id }, null, { sort: { date: -1 }, limit: parseInt(sz) }).exec();
            output.response = "success";
            output.message = "Data Recevied";
            output.output = data;
            return output;
        } catch (error) {
            output.response = "error";
            output.message = error;
            output.output = null;
            return output;
        }
    }

    async findBySupportId(id: string, sz): Promise<GPSOutput> {
        let output = new GPSOutput();

        try {
            const data = await this.gpsModel.find({ actual_support_id: id }, null, { sort: { date: -1 }, limit: parseInt(sz) }).exec();
            output.response = "success";
            output.message = "Data Recevied";
            output.output = data;
            return output;
        } catch (error) {
            output.response = "error";
            output.message = error;
            output.output = null;
            return output;
        }
    }

    async findByOLoaderId(id: string, sz): Promise<GPSOutput> {
        let output = new GPSOutput();

        try {
            const data = await this.gpsModel.find({ operation_loader_id: id }, null, { sort: { date: -1 }, limit: parseInt(sz) }).exec();
            output.response = "success";
            output.message = "Data Recevied";
            output.output = data;
            return output;
        } catch (error) {
            output.response = "error";
            output.message = error;
            output.output = null;
            return output;
        }
    }

    async findByUnitId(id: string, sz): Promise<GPSOutput> {
        let output = new GPSOutput();

        try {
            const data = await this.gpsModel.find({ unit_id: id }, null, { sort: { date: -1 }, limit: parseInt(sz) }).exec();
            output.response = "success";
            output.message = "Data Recevied";
            output.output = data;
            return output;
        } catch (error) {
            output.response = "error";
            output.message = error;
            output.output = null;
            return output;
        }
    }

    async find(gpsID): Promise<GPSOneOutput> {
        let output = new GPSOneOutput();

        try {
            const data = await this.gpsModel.findById(gpsID).exec();
            output.response = "success";
            output.message = "Data Recevied";
            output.output = data;
            return output;
        } catch (error) {
            output.response = "error";
            output.message = error;
            output.output = null;
            return output;
        }
    }

    async create(createGPSDTO: CreateGPSDTO): Promise<GPSOneOutput> {
        let output = new GPSOneOutput();

        try {
            const created = await new this.gpsModel(createGPSDTO).save();
            output.response = "success";
            output.message = "Data Saved";
            output.output = created;
            return output;
        } catch (error) {
            output.response = "error";
            output.message = error;
            output.output = null;
            return output;
        }
    }

    async update(gpsID, updateGPSDTO: CreateGPSDTO): Promise<GPSOneOutput> {
        let output = new GPSOneOutput();

        try {
            const findById = await this.find(gpsID);
            if (findById['output']) {
                const updated = await this.gpsModel.updateOne({ _id: gpsID }, updateGPSDTO);
                output.response = "success";
                output.message = "Data Updated";
                output.output = updateGPSDTO;
                return output;
            } else {
                return findById;
            }
        } catch (error) {
            output.response = "error";
            output.message = error;
            output.output = null;
            return output;
        }
    }

    async delete(gpsID): Promise<GPSOneOutput> {
        let output = new GPSOneOutput();

        try {
            const findById = await this.find(gpsID);
            if (findById['output']) {
                const deleted = await this.gpsModel.deleteOne({ _id: gpsID });
                output.response = "success";
                output.message = "Data Deleted";
                output.output = deleted;
                return output;
            } else {
                return findById;
            }
        } catch (error) {
            output.response = "error";
            output.message = error;
            output.output = null;
            return output;
        }
    }

}
