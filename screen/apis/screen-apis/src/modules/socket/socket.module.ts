import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ECUSchema } from '../../entities/ecu/ecu.schema';
import { ECUService } from '../../entities/ecu/ecu.service';
import { GPSSchema } from '../../entities/gps/gps.schema';
import { GPSService } from '../../entities/gps/gps.service';
import { LocationSchema } from '../../entities/locations/location.schema';
import { LocationService } from '../../entities/locations/location.service';
import { OperatorSchema } from '../../entities/operator/operator.schema';
import { OperatorService } from '../../entities/operator/operator.service';
import { UnitSchema } from '../../entities/units/unit.schema';
import { UnitService } from '../../entities/units/unit.service';
import { SocketGateway } from './socket.gateway';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: 'Unit', schema: UnitSchema }]),
        MongooseModule.forFeature([{ name: 'Location', schema: LocationSchema }]),
        MongooseModule.forFeature([{ name: 'Operator', schema: OperatorSchema }]),
        MongooseModule.forFeature([{ name: 'GPS', schema: GPSSchema }]),
        MongooseModule.forFeature([{ name: 'ECU', schema: ECUSchema }])
    ],
    providers: [
        UnitService,
        LocationService,
        OperatorService,
        GPSService,
        ECUService,
        SocketGateway
    ]
})
export class SocketModule { }
