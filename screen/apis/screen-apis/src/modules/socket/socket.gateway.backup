import { WebSocketGateway, WebSocketServer, SubscribeMessage, OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit } from '@nestjs/websockets';
import { Socket, Client } from 'socket.io';
import * as jwt from 'jsonwebtoken';
import { ConfigService } from 'nestjs-config';
import { UnitService } from '../../entities/units/unit.service';
import { NotFoundException } from '@nestjs/common';
import { LocationService } from '../../entities/locations/location.service';
import { OperatorService } from '../../entities/operator/operator.service';
import { GPSService } from '../../entities/gps/gps.service';
import { ECUService } from '../../entities/ecu/ecu.service';

@WebSocketGateway()
export class SocketGateway implements OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit {

    @WebSocketServer() server;
    clients: any = [];

    constructor(
        private readonly config: ConfigService,
        private readonly unitService: UnitService,
        private readonly locationService: LocationService,
        private readonly operatorService: OperatorService,
        private readonly gpsService: GPSService,
        private readonly ecuService: ECUService
    ) { }

    afterInit(socket: Socket) {
    }

    setJWT(socket: any) {
        return new Promise((resolve, reject) => {
            const token = socket.handshake.query.token;
            jwt.verify(token, this.config.get('global.jwtKey'), (err, decoded) => {
                if (err) {
                    reject(err);
                } else {
                    socket.user = decoded;
                    resolve(true);
                }
            });
        });
    }

    async init(socket: any) {
        try {
            await this.setJWT(socket);

            const unit = await this.unitService.find(socket.user.id);
            if (!unit) {
                this.server.emit('unit');
            } else {
                socket.user.unit = unit;
            }

            const location = await this.locationService.find(socket.user.id);
            if (!location) {
                this.server.emit('location');
            } else {
                socket.user.location = location;
            }

            const operator = await this.operatorService.find(socket.user.id);
            if (!operator) {
                const saveOperator = {
                    _id: socket.user.id,
                    nik: socket.user.staff_id,
                    name: socket.user.name
                }
                const operator = await this.operatorService.create(saveOperator);
                if (!operator) {
                    throw new NotFoundException('operator does not saved!');
                }

                socket.user.operator = saveOperator;
            } else {
                socket.user.operator = operator;
            }

            this.clients.push(socket);

        } catch (error) {
            console.error(error);
        }
    }

    handleConnection(socket: any) {
        this.init(socket);
    }

    findSocketId(id) {
        return new Promise((resolve, reject) => {
            this.clients.filter(function (data, index) {
                if (data.id == id) {
                    resolve(index);
                }
            });

            reject(false);
        });
    }

    async handleDisconnect(socket: Socket) {
        try {
            let id: any = await this.findSocketId(socket.id);
            id++;
            const clients = this.clients.slice(id, 1);
            this.clients = clients;
            console.log('disconnected');
        } catch (error) {
            console.error(error);
        }
    }

    @SubscribeMessage('unit')
    async handleUnit(client: any, data: any) {
        const unit = await this.unitService.create(data);
        if (!unit) {
            throw new NotFoundException('Unit does not saved!');
        }

        client.user.unit = data;
    }

    @SubscribeMessage('location')
    async handleLocation(client: any, data: any) {
        const location = await this.locationService.create(data);
        if (!location) {
            throw new NotFoundException('Location does not saved!');
        }

        client.user.location = data;
    }


}
