import { Injectable, OnModuleInit } from "@nestjs/common";
const ip = require('ip');
const { Kafka, logLevel } = require('kafkajs')

@Injectable()
export class KafkaService implements OnModuleInit {
    constructor() {
    }

    onModuleInit() {
        const host = process.env.HOST_IP || ip.address()
        const topic = 'test-topic'

        const kafka = new Kafka({
            logLevel: logLevel.INFO,
            clientId: 'my-app',
            brokers: [`${host}:9092`]
        })

        // CONSUMER
        const consumer = kafka.consumer({ groupId: 'test-group' })

        const run = async () => {
            await consumer.connect()
            await consumer.subscribe({ topic: 'test-topic' })
            await consumer.run({
                eachMessage: async ({ topic, partition, message }) => {
                    console.log({
                        value: message.value.toString(),
                    })
                },
            })
        }

        run().catch(e => console.error(`[example/consumer] ${e.message}`, e))

        // PRODUCER
        // const producer = kafka.producer()

        // const sendMessage = () => {
        //     return producer.send({
        //         topic: 'test-topic',
        //         messages: [
        //             { value: 'Hello KafkaJS user!' },
        //         ],
        //     })
        //     .then(console.log)
        //     .catch(e => console.error(`[example/producer] ${e.message}`, e))
        // }

        // const run = async () => {
        //     await producer.connect()
        //     setInterval(sendMessage, 3000)
        // }

        // run().catch(e => console.error(`[example/producer] ${e.message}`, e))
    }
}