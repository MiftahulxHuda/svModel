export interface JwtPayload {
    id: string;
    email: string;
    name: string;
    cd_role: string;
    cd_department: string;
    staff_id: string;
    fingerprint_id: number;
    socket_id: string;
    file_picture: string;
    enum_availability: number;
    is_authenticated: number;
    _org: object;
    _role: object;
    _department: object;
}