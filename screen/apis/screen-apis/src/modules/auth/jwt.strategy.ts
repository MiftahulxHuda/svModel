import { Injectable, UnauthorizedException, ExecutionContext } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { JwtPayload } from './jwt.interface';
import { ConfigService } from 'nestjs-config';
import { AuthService } from './auth.service';


@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(private readonly config: ConfigService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: config.get('global.jwtKey')
        });
    }

    async validate(payload: JwtPayload, done: Function) {
        if (!payload.is_authenticated) {
            return await done(new UnauthorizedException(), false);
        }
        return await done(null, payload);
    }
}