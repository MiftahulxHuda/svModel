import { Module, Global } from '@nestjs/common';

import { AuthService } from './auth.service';
import { JwtStrategy } from './jwt.strategy';

@Global()
@Module({
    exports: [AuthService, JwtStrategy],
    providers: [AuthService, JwtStrategy]
})
export class AuthModule {

}