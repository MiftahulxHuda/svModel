import { Injectable, ExecutionContext } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';

import { JwtPayload } from './jwt.interface';
import { ConfigService } from 'nestjs-config';

@Injectable()
export class AuthService {
    constructor(private readonly config: ConfigService) { }

    async createToken(data) {
        const user: JwtPayload = {
            id: data.id,
            email: data.email,
            name: data.name,
            cd_role: data.__cd_role__,
            cd_department: data.__cd_department__,
            staff_id: data.staff_id,
            fingerprint_id: data.fingerprint_id,
            socket_id: data.socket_id,
            file_picture: data.file_picture,
            enum_availability: data.enum_availability,
            is_authenticated: data.is_authenticated,
            _org: data.__id_org__,
            _role: data.__cd_role__,
            _department: data.__cd_department__
        }

        return await jwt.sign(user, this.config.get('global.jwtKey'), { algorithm: 'HS256', expiresIn: '3650 day' });
    }

    async extractToken(req) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            const token = req.headers.authorization.split(' ')[1];
            return await jwt.verify(token, this.config.get('global.jwtKey'), (err, payload) => {
                if (!err) {
                    // confirm identity and check user permissions
                    // req.user = payload;
                    return payload;

                } else {
                    return err;
                }
            });
        } else {
            return null
        }
    }
}