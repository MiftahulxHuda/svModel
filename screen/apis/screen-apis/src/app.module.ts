import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule, ConfigService } from 'nestjs-config';
import * as path from 'path';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ECUModule } from './entities/ecu/ecu.module';
import { GPSModule } from './entities/gps/gps.module';
import { KafkaModule } from './entities/kafka/kafka.module';
import { UnitModule } from './entities/unit/unit.module';
import { AuthModule } from './modules/auth/auth.module';
import { SocketModule } from './modules/socket/socket.module';
import { ServiceStatusModule } from './entities/service-status/service-status.module';

const entitiesModule = [
    // UnitModule,
    GPSModule,
    ServiceStatusModule,
    // ECUModule
];

@Module({
    imports: [
        ConfigModule.load(path.resolve(__dirname, 'config', '**/!(*.d).{ts,js}')),
        MongooseModule.forRootAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => ({
                uri: configService.get('global.environment') === 'production' ? configService.get('production.mongodbUri') : configService.get('development.mongodbUri'),
            }),
            inject: [ConfigService]
        }),
        AuthModule,
        SocketModule,
        KafkaModule,
        ...entitiesModule
    ],
    controllers: [AppController],
    providers: [
        AppService
    ],
})
export class AppModule { }
