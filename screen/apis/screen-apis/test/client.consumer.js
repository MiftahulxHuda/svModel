const express = require('express');
const app = express();
const port = process.argv[2];

console.log('port ', port);

const io = require('socket.io-client');

const socket = io('ws://localhost:8080', {
    query: {
        token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjExMDMiLCJlbWFpbCI6ImFkbWluM0BtYWlsLmNvbSIsIm5hbWUiOiJBZG1pbiAzIiwiY2Rfcm9sZSI6eyJjZCI6IjAwMzAwMSIsImNkX3NlbGYiOiIwMDMiLCJuYW1lIjoiQWRtaW5pc3RyYXRvciIsImpzb24iOnsiY2QiOiIwMDMwMDEiLCJzZXEiOm51bGwsIm5hbWUiOiJBZG1pbiIsInNsdWciOm51bGwsInRhZ18xIjpudWxsLCJ0YWdfMiI6bnVsbCwidGFnXzMiOm51bGwsImNkX3NlbGYiOiIwMDMiLCJpc19maXhlZCI6MSwiaXNfZGVsZXRlZCI6MH19LCJjZF9kZXBhcnRtZW50Ijp7ImNkIjoiMDA0MDAxIiwiY2Rfc2VsZiI6IjAwNCIsIm5hbWUiOiJTeXN0ZW0iLCJqc29uIjp7ImNkIjoiMDA0MDAxIiwic2VxIjpudWxsLCJuYW1lIjoiU3lzdGVtIiwic2x1ZyI6bnVsbCwidGFnXzEiOm51bGwsInRhZ18yIjpudWxsLCJ0YWdfMyI6bnVsbCwiY2Rfc2VsZiI6IjAwNCIsImlzX2ZpeGVkIjoxLCJpc19kZWxldGVkIjowfX0sInN0YWZmX2lkIjoiMTAwMDAwMDMiLCJmaW5nZXJwcmludF9pZCI6bnVsbCwic29ja2V0X2lkIjpudWxsLCJmaWxlX3BpY3R1cmUiOiJhc3NldHMvaW1hZ2VzL2F2YXRhcnMvdXNlci5qcGciLCJlbnVtX2F2YWlsYWJpbGl0eSI6NCwiaXNfYXV0aGVudGljYXRlZCI6MSwiX29yZyI6eyJpZCI6IjEiLCJuYW1lIjoiUFQgU2F0cmlhIEJhaGFuYSBTYXJhbmEiLCJhYmJyIjoiU0JTIiwianNvbiI6bnVsbH0sIl9yb2xlIjp7ImNkIjoiMDAzMDAxIiwiY2Rfc2VsZiI6IjAwMyIsIm5hbWUiOiJBZG1pbmlzdHJhdG9yIiwianNvbiI6eyJjZCI6IjAwMzAwMSIsInNlcSI6bnVsbCwibmFtZSI6IkFkbWluIiwic2x1ZyI6bnVsbCwidGFnXzEiOm51bGwsInRhZ18yIjpudWxsLCJ0YWdfMyI6bnVsbCwiY2Rfc2VsZiI6IjAwMyIsImlzX2ZpeGVkIjoxLCJpc19kZWxldGVkIjowfX0sIl9kZXBhcnRtZW50Ijp7ImNkIjoiMDA0MDAxIiwiY2Rfc2VsZiI6IjAwNCIsIm5hbWUiOiJTeXN0ZW0iLCJqc29uIjp7ImNkIjoiMDA0MDAxIiwic2VxIjpudWxsLCJuYW1lIjoiU3lzdGVtIiwic2x1ZyI6bnVsbCwidGFnXzEiOm51bGwsInRhZ18yIjpudWxsLCJ0YWdfMyI6bnVsbCwiY2Rfc2VsZiI6IjAwNCIsImlzX2ZpeGVkIjoxLCJpc19kZWxldGVkIjowfX0sImlhdCI6MTU2MTk1MTcwNywiZXhwIjoxODc3MzExNzA3fQ.ufg_Dawo3AuJR7Ys7ZQAhuNzJP1N3fh3LzEpZ6wc2NY'
    }
});

socket.on('connect', function () {
    console.log('connected');
});

socket.on('unit', function (data) {
    console.log('unit ', data);
});
socket.on('location', function (data) {
    console.log('location ', data);
});

socket.on('map-info', function (data) {
    console.log('map info ', data);
});

socket.on('ecu-info', function (data) {
    console.log('ecu info ', data);
});

socket.on('disconnect', function (c) {
    console.log('disconnect', c);
});

app.get('/', (req, res) => {
    res.send([]);
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));