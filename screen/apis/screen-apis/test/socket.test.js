const express = require('express');
const app = express();
const port = process.argv[2];
const usersel = process.argv[3];
const unit = process.argv[4];
const location = process.argv[5];

console.log('port ', port);

const io = require('socket.io-client');
// const emitter = require('socket.io-emitter');
const gps = require('./gps-from-unit.data.json');
const moment = require('moment');

const tokens = [
    // 10000000
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEiLCJlbWFpbCI6ImFkbWluQG1haWwuY29tIiwibmFtZSI6IkFkbWluIiwiY2Rfcm9sZSI6eyJjZCI6IjAwMzAwMSIsImNkX3NlbGYiOiIwMDMiLCJuYW1lIjoiQWRtaW5pc3RyYXRvciIsImpzb24iOnsiY2QiOiIwMDMwMDEiLCJzZXEiOm51bGwsIm5hbWUiOiJBZG1pbiIsInNsdWciOm51bGwsInRhZ18xIjpudWxsLCJ0YWdfMiI6bnVsbCwidGFnXzMiOm51bGwsImNkX3NlbGYiOiIwMDMiLCJpc19maXhlZCI6MSwiaXNfZGVsZXRlZCI6MH19LCJjZF9kZXBhcnRtZW50Ijp7ImNkIjoiMDA0MDAxIiwiY2Rfc2VsZiI6IjAwNCIsIm5hbWUiOiJTeXN0ZW0iLCJqc29uIjp7ImNkIjoiMDA0MDAxIiwic2VxIjpudWxsLCJuYW1lIjoiU3lzdGVtIiwic2x1ZyI6bnVsbCwidGFnXzEiOm51bGwsInRhZ18yIjpudWxsLCJ0YWdfMyI6bnVsbCwiY2Rfc2VsZiI6IjAwNCIsImlzX2ZpeGVkIjoxLCJpc19kZWxldGVkIjowfX0sInN0YWZmX2lkIjoiMTAwMDAwMDAiLCJmaW5nZXJwcmludF9pZCI6bnVsbCwic29ja2V0X2lkIjpudWxsLCJmaWxlX3BpY3R1cmUiOiJhc3NldHMvaW1hZ2VzL2F2YXRhcnMvdXNlci5qcGciLCJlbnVtX2F2YWlsYWJpbGl0eSI6NCwiaXNfYXV0aGVudGljYXRlZCI6MSwiX29yZyI6eyJpZCI6IjEiLCJuYW1lIjoiUFQgU2F0cmlhIEJhaGFuYSBTYXJhbmEiLCJhYmJyIjoiU0JTIiwianNvbiI6bnVsbH0sIl9yb2xlIjp7ImNkIjoiMDAzMDAxIiwiY2Rfc2VsZiI6IjAwMyIsIm5hbWUiOiJBZG1pbmlzdHJhdG9yIiwianNvbiI6eyJjZCI6IjAwMzAwMSIsInNlcSI6bnVsbCwibmFtZSI6IkFkbWluIiwic2x1ZyI6bnVsbCwidGFnXzEiOm51bGwsInRhZ18yIjpudWxsLCJ0YWdfMyI6bnVsbCwiY2Rfc2VsZiI6IjAwMyIsImlzX2ZpeGVkIjoxLCJpc19kZWxldGVkIjowfX0sIl9kZXBhcnRtZW50Ijp7ImNkIjoiMDA0MDAxIiwiY2Rfc2VsZiI6IjAwNCIsIm5hbWUiOiJTeXN0ZW0iLCJqc29uIjp7ImNkIjoiMDA0MDAxIiwic2VxIjpudWxsLCJuYW1lIjoiU3lzdGVtIiwic2x1ZyI6bnVsbCwidGFnXzEiOm51bGwsInRhZ18yIjpudWxsLCJ0YWdfMyI6bnVsbCwiY2Rfc2VsZiI6IjAwNCIsImlzX2ZpeGVkIjoxLCJpc19kZWxldGVkIjowfX0sImlhdCI6MTU2MTQ0ODkxOSwiZXhwIjoxODc2ODA4OTE5fQ.5B0WacpmWzhSyTiSbQiLGPbfwhHo_NsWRTO-jKoHJno',
    // 10000001
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjExMDEiLCJlbWFpbCI6ImFkbWluMUBtYWlsLmNvbSIsIm5hbWUiOiJBZG1pbiAxIiwiY2Rfcm9sZSI6eyJjZCI6IjAwMzAwMSIsImNkX3NlbGYiOiIwMDMiLCJuYW1lIjoiQWRtaW5pc3RyYXRvciIsImpzb24iOnsiY2QiOiIwMDMwMDEiLCJzZXEiOm51bGwsIm5hbWUiOiJBZG1pbiIsInNsdWciOm51bGwsInRhZ18xIjpudWxsLCJ0YWdfMiI6bnVsbCwidGFnXzMiOm51bGwsImNkX3NlbGYiOiIwMDMiLCJpc19maXhlZCI6MSwiaXNfZGVsZXRlZCI6MH19LCJjZF9kZXBhcnRtZW50Ijp7ImNkIjoiMDA0MDAxIiwiY2Rfc2VsZiI6IjAwNCIsIm5hbWUiOiJTeXN0ZW0iLCJqc29uIjp7ImNkIjoiMDA0MDAxIiwic2VxIjpudWxsLCJuYW1lIjoiU3lzdGVtIiwic2x1ZyI6bnVsbCwidGFnXzEiOm51bGwsInRhZ18yIjpudWxsLCJ0YWdfMyI6bnVsbCwiY2Rfc2VsZiI6IjAwNCIsImlzX2ZpeGVkIjoxLCJpc19kZWxldGVkIjowfX0sInN0YWZmX2lkIjoiMTAwMDAwMDEiLCJmaW5nZXJwcmludF9pZCI6bnVsbCwic29ja2V0X2lkIjpudWxsLCJmaWxlX3BpY3R1cmUiOiJhc3NldHMvaW1hZ2VzL2F2YXRhcnMvdXNlci5qcGciLCJlbnVtX2F2YWlsYWJpbGl0eSI6NCwiaXNfYXV0aGVudGljYXRlZCI6MSwiX29yZyI6eyJpZCI6IjEiLCJuYW1lIjoiUFQgU2F0cmlhIEJhaGFuYSBTYXJhbmEiLCJhYmJyIjoiU0JTIiwianNvbiI6bnVsbH0sIl9yb2xlIjp7ImNkIjoiMDAzMDAxIiwiY2Rfc2VsZiI6IjAwMyIsIm5hbWUiOiJBZG1pbmlzdHJhdG9yIiwianNvbiI6eyJjZCI6IjAwMzAwMSIsInNlcSI6bnVsbCwibmFtZSI6IkFkbWluIiwic2x1ZyI6bnVsbCwidGFnXzEiOm51bGwsInRhZ18yIjpudWxsLCJ0YWdfMyI6bnVsbCwiY2Rfc2VsZiI6IjAwMyIsImlzX2ZpeGVkIjoxLCJpc19kZWxldGVkIjowfX0sIl9kZXBhcnRtZW50Ijp7ImNkIjoiMDA0MDAxIiwiY2Rfc2VsZiI6IjAwNCIsIm5hbWUiOiJTeXN0ZW0iLCJqc29uIjp7ImNkIjoiMDA0MDAxIiwic2VxIjpudWxsLCJuYW1lIjoiU3lzdGVtIiwic2x1ZyI6bnVsbCwidGFnXzEiOm51bGwsInRhZ18yIjpudWxsLCJ0YWdfMyI6bnVsbCwiY2Rfc2VsZiI6IjAwNCIsImlzX2ZpeGVkIjoxLCJpc19kZWxldGVkIjowfX0sImlhdCI6MTU2MTk1MTYxOCwiZXhwIjoxODc3MzExNjE4fQ.qS34NAtpRHTkb6L5zqTpzktSesyM-zjaMCqtAYop2gw',
    // 10000002
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjExMDIiLCJlbWFpbCI6ImFkbWluMjJAbWFpbC5jb20iLCJuYW1lIjoiQWRtaW4gMiIsImNkX3JvbGUiOnsiY2QiOiIwMDMwMDEiLCJjZF9zZWxmIjoiMDAzIiwibmFtZSI6IkFkbWluaXN0cmF0b3IiLCJqc29uIjp7ImNkIjoiMDAzMDAxIiwic2VxIjpudWxsLCJuYW1lIjoiQWRtaW4iLCJzbHVnIjpudWxsLCJ0YWdfMSI6bnVsbCwidGFnXzIiOm51bGwsInRhZ18zIjpudWxsLCJjZF9zZWxmIjoiMDAzIiwiaXNfZml4ZWQiOjEsImlzX2RlbGV0ZWQiOjB9fSwiY2RfZGVwYXJ0bWVudCI6eyJjZCI6IjAwNDAwMSIsImNkX3NlbGYiOiIwMDQiLCJuYW1lIjoiU3lzdGVtIiwianNvbiI6eyJjZCI6IjAwNDAwMSIsInNlcSI6bnVsbCwibmFtZSI6IlN5c3RlbSIsInNsdWciOm51bGwsInRhZ18xIjpudWxsLCJ0YWdfMiI6bnVsbCwidGFnXzMiOm51bGwsImNkX3NlbGYiOiIwMDQiLCJpc19maXhlZCI6MSwiaXNfZGVsZXRlZCI6MH19LCJzdGFmZl9pZCI6IjEwMDAwMDAyIiwiZmluZ2VycHJpbnRfaWQiOm51bGwsInNvY2tldF9pZCI6bnVsbCwiZmlsZV9waWN0dXJlIjoiYXNzZXRzL2ltYWdlcy9hdmF0YXJzL3VzZXIuanBnIiwiZW51bV9hdmFpbGFiaWxpdHkiOjQsImlzX2F1dGhlbnRpY2F0ZWQiOjEsIl9vcmciOnsiaWQiOiIxIiwibmFtZSI6IlBUIFNhdHJpYSBCYWhhbmEgU2FyYW5hIiwiYWJiciI6IlNCUyIsImpzb24iOm51bGx9LCJfcm9sZSI6eyJjZCI6IjAwMzAwMSIsImNkX3NlbGYiOiIwMDMiLCJuYW1lIjoiQWRtaW5pc3RyYXRvciIsImpzb24iOnsiY2QiOiIwMDMwMDEiLCJzZXEiOm51bGwsIm5hbWUiOiJBZG1pbiIsInNsdWciOm51bGwsInRhZ18xIjpudWxsLCJ0YWdfMiI6bnVsbCwidGFnXzMiOm51bGwsImNkX3NlbGYiOiIwMDMiLCJpc19maXhlZCI6MSwiaXNfZGVsZXRlZCI6MH19LCJfZGVwYXJ0bWVudCI6eyJjZCI6IjAwNDAwMSIsImNkX3NlbGYiOiIwMDQiLCJuYW1lIjoiU3lzdGVtIiwianNvbiI6eyJjZCI6IjAwNDAwMSIsInNlcSI6bnVsbCwibmFtZSI6IlN5c3RlbSIsInNsdWciOm51bGwsInRhZ18xIjpudWxsLCJ0YWdfMiI6bnVsbCwidGFnXzMiOm51bGwsImNkX3NlbGYiOiIwMDQiLCJpc19maXhlZCI6MSwiaXNfZGVsZXRlZCI6MH19LCJpYXQiOjE1NjE5NTE2NjUsImV4cCI6MTg3NzMxMTY2NX0.kEalo4u3iV2-lWoo5Nv83EABFk16puAPwNQHqDDtZoA',
    // 10000003
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjExMDMiLCJlbWFpbCI6ImFkbWluM0BtYWlsLmNvbSIsIm5hbWUiOiJBZG1pbiAzIiwiY2Rfcm9sZSI6eyJjZCI6IjAwMzAwMSIsImNkX3NlbGYiOiIwMDMiLCJuYW1lIjoiQWRtaW5pc3RyYXRvciIsImpzb24iOnsiY2QiOiIwMDMwMDEiLCJzZXEiOm51bGwsIm5hbWUiOiJBZG1pbiIsInNsdWciOm51bGwsInRhZ18xIjpudWxsLCJ0YWdfMiI6bnVsbCwidGFnXzMiOm51bGwsImNkX3NlbGYiOiIwMDMiLCJpc19maXhlZCI6MSwiaXNfZGVsZXRlZCI6MH19LCJjZF9kZXBhcnRtZW50Ijp7ImNkIjoiMDA0MDAxIiwiY2Rfc2VsZiI6IjAwNCIsIm5hbWUiOiJTeXN0ZW0iLCJqc29uIjp7ImNkIjoiMDA0MDAxIiwic2VxIjpudWxsLCJuYW1lIjoiU3lzdGVtIiwic2x1ZyI6bnVsbCwidGFnXzEiOm51bGwsInRhZ18yIjpudWxsLCJ0YWdfMyI6bnVsbCwiY2Rfc2VsZiI6IjAwNCIsImlzX2ZpeGVkIjoxLCJpc19kZWxldGVkIjowfX0sInN0YWZmX2lkIjoiMTAwMDAwMDMiLCJmaW5nZXJwcmludF9pZCI6bnVsbCwic29ja2V0X2lkIjpudWxsLCJmaWxlX3BpY3R1cmUiOiJhc3NldHMvaW1hZ2VzL2F2YXRhcnMvdXNlci5qcGciLCJlbnVtX2F2YWlsYWJpbGl0eSI6NCwiaXNfYXV0aGVudGljYXRlZCI6MSwiX29yZyI6eyJpZCI6IjEiLCJuYW1lIjoiUFQgU2F0cmlhIEJhaGFuYSBTYXJhbmEiLCJhYmJyIjoiU0JTIiwianNvbiI6bnVsbH0sIl9yb2xlIjp7ImNkIjoiMDAzMDAxIiwiY2Rfc2VsZiI6IjAwMyIsIm5hbWUiOiJBZG1pbmlzdHJhdG9yIiwianNvbiI6eyJjZCI6IjAwMzAwMSIsInNlcSI6bnVsbCwibmFtZSI6IkFkbWluIiwic2x1ZyI6bnVsbCwidGFnXzEiOm51bGwsInRhZ18yIjpudWxsLCJ0YWdfMyI6bnVsbCwiY2Rfc2VsZiI6IjAwMyIsImlzX2ZpeGVkIjoxLCJpc19kZWxldGVkIjowfX0sIl9kZXBhcnRtZW50Ijp7ImNkIjoiMDA0MDAxIiwiY2Rfc2VsZiI6IjAwNCIsIm5hbWUiOiJTeXN0ZW0iLCJqc29uIjp7ImNkIjoiMDA0MDAxIiwic2VxIjpudWxsLCJuYW1lIjoiU3lzdGVtIiwic2x1ZyI6bnVsbCwidGFnXzEiOm51bGwsInRhZ18yIjpudWxsLCJ0YWdfMyI6bnVsbCwiY2Rfc2VsZiI6IjAwNCIsImlzX2ZpeGVkIjoxLCJpc19kZWxldGVkIjowfX0sImlhdCI6MTU2MTk1MTcwNywiZXhwIjoxODc3MzExNzA3fQ.ufg_Dawo3AuJR7Ys7ZQAhuNzJP1N3fh3LzEpZ6wc2NY'
];

const socket = io('ws://159.65.0.218:8888', {
    query: {
        token: tokens[usersel]
    }
});

const dataUnit = [
    { id: '001', name: 'TRUCK 1' },
    { id: '002', name: 'TRUCK 2' },
    { id: '003', name: 'TRUCK 3' },
];
const dataLocation = [
    { id: '001', name: 'LOCATION 1' },
    { id: '002', name: 'LOCATION 2' },
    { id: '003', name: 'LOCATION 3' },
];

socket.on('connect', function () {
    console.log('connected');
    socket.emit('gps', 'test');
});
socket.on('unit', function (data) {
    console.log('server request data unit');
    socket.emit('unit', dataUnit[unit]);
});
socket.on('location', function (data) {
    console.log('server request data location');
    socket.emit('location', dataLocation[location]);
});

socket.on('gps', function (data) {
    for (const gg of gps) {
        gg.date = moment().format('DD/MM/YYYY HH:mm:ss:SS');
        gg.operator = '00' + usersel;
        gg.unit = dataUnit[unit].id;
        gg.location = dataLocation[location].id;
        socket.emit('gps', gg);
    }
    // socket.emit('gps', gps[0]);
});

socket.on('map-info', function (data) {
    console.log('map info ', data);
});

socket.on('ecu-info', function (data) {
    console.log('ecu info ', data);
});

socket.on('disconnect', function (c) {
    console.log('disconnect', c);
});

// app.get('/gps', (req, res) => {
//     res.send([]);
// });

// app.listen(port, () => console.log(`Example app listening on port ${port}!`));
