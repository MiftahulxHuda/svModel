# Screen App

## Token list
Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEiLCJlbWFpbCI6ImFkbWluQG1haWwuY29tIiwibmFtZSI6IkFkbWluIiwiY2Rfcm9sZSI6eyJjZCI6IjAwMzAwMSIsImNkX3NlbGYiOiIwMDMiLCJuYW1lIjoiQWRtaW5pc3RyYXRvciIsImpzb24iOnsiY2QiOiIwMDMwMDEiLCJzZXEiOm51bGwsIm5hbWUiOiJBZG1pbiIsInNsdWciOm51bGwsInRhZ18xIjpudWxsLCJ0YWdfMiI6bnVsbCwidGFnXzMiOm51bGwsImNkX3NlbGYiOiIwMDMiLCJpc19maXhlZCI6MSwiaXNfZGVsZXRlZCI6MH19LCJjZF9kZXBhcnRtZW50Ijp7ImNkIjoiMDA0MDAxIiwiY2Rfc2VsZiI6IjAwNCIsIm5hbWUiOiJTeXN0ZW0iLCJqc29uIjp7ImNkIjoiMDA0MDAxIiwic2VxIjpudWxsLCJuYW1lIjoiU3lzdGVtIiwic2x1ZyI6bnVsbCwidGFnXzEiOm51bGwsInRhZ18yIjpudWxsLCJ0YWdfMyI6bnVsbCwiY2Rfc2VsZiI6IjAwNCIsImlzX2ZpeGVkIjoxLCJpc19kZWxldGVkIjowfX0sInN0YWZmX2lkIjoiMTAwMDAwMDAiLCJmaW5nZXJwcmludF9pZCI6bnVsbCwic29ja2V0X2lkIjoic3RyaW5nIiwiZmlsZV9waWN0dXJlIjoiYXNzZXRzL2ltYWdlcy9hdmF0YXJzL3VzZXIuanBnIiwiZW51bV9hdmFpbGFiaWxpdHkiOjQsImlzX2F1dGhlbnRpY2F0ZWQiOjEsIl9vcmciOnsiaWQiOiIxIiwibmFtZSI6IlBUIFNhdHJpYSBCYWhhbmEgU2FyYW5hIiwiYWJiciI6IlNCUyIsImpzb24iOm51bGx9LCJfcm9sZSI6eyJjZCI6IjAwMzAwMSIsImNkX3NlbGYiOiIwMDMiLCJuYW1lIjoiQWRtaW5pc3RyYXRvciIsImpzb24iOnsiY2QiOiIwMDMwMDEiLCJzZXEiOm51bGwsIm5hbWUiOiJBZG1pbiIsInNsdWciOm51bGwsInRhZ18xIjpudWxsLCJ0YWdfMiI6bnVsbCwidGFnXzMiOm51bGwsImNkX3NlbGYiOiIwMDMiLCJpc19maXhlZCI6MSwiaXNfZGVsZXRlZCI6MH19LCJfZGVwYXJ0bWVudCI6eyJjZCI6IjAwNDAwMSIsImNkX3NlbGYiOiIwMDQiLCJuYW1lIjoiU3lzdGVtIiwianNvbiI6eyJjZCI6IjAwNDAwMSIsInNlcSI6bnVsbCwibmFtZSI6IlN5c3RlbSIsInNsdWciOm51bGwsInRhZ18xIjpudWxsLCJ0YWdfMiI6bnVsbCwidGFnXzMiOm51bGwsImNkX3NlbGYiOiIwMDQiLCJpc19maXhlZCI6MSwiaXNfZGVsZXRlZCI6MH19LCJpYXQiOjE1NjU5MjQwMzUsImV4cCI6MTg4MTI4NDAzNX0.qFIaIAyUmFsV3JglBeUUck1mebJANc5-sVqnDX-KLqg

## MongoDB
`docker run -d -p 27017:27017 -v /root/data/mongo:/data/db mongo:jessie`

## Class Operation
- `public void insertIgnore(Object eqa)`
digunakan ketika saving ke server gagal dan ketika offline mode

- `public Object getFromServer(Integer page, Integer size)`
get data dari server
cek network status

- `public List<Object> getFlaggedData()`
untuk mengambil data offline yang belum terkirim ke server

- `public void postBulkData(List<Object> eq)`
untuk mengirim data offline yang bersumber dari fungsi `getFlaggedData()` kemudian set flagging/dihapus
ketika gagal data tidak diflagging/tidak dihapus
berpengaruh terhadap network status

- `public Integer getRemoteCount()`
mengambil total count dari server
berpengaruh terhadap network status

## Mekanik

10150162

## Supervisor

+----------+
| staff_id |
+----------+
| 10170147 |
| 20060001 |
| 10170294 |
| 10160136 |
| 10150002 |
+----------+

## Group Leader

+----------+
| staff_id |
+----------+
| 10180144 |
| 10150235 |
| 10170159 |
| 10160071 |
| 20130002 |
+----------+


## Dummy Actual minefleet.tura.tech
+----------+----------+------------+----------+----------+----------------+---------+---------+-------------+---------+---------+
| cd_shift | loader   | hauler     | oloader  | ohauler  | location       | lat     | lon     | destination | lat     | lon     |
+----------+----------+------------+----------+----------+----------------+---------+---------+-------------+---------+---------+
| 007001   | EX08-004 | AUDT03-021 | 10180077 | 10180004 | TEM1F150LS     | -3.7259 | 103.779 | INPTMNOB02  | -3.7864 | 103.841 |
| 007001   | EX08-004 | DT02-005   | 10180077 | 10180130 | TEM1F150LS     | -3.7259 | 103.779 | INPTMNOB02  | -3.7864 | 103.841 |
| 007001   | EX08-004 | DT02-007   | 10180077 | 10170167 | TEM1F150LS     | -3.7259 | 103.779 | INPTMNOB02  | -3.7864 | 103.841 |
| 007001   | EX08-004 | DT02-025   | 10180077 | 10180153 | TEM1F150LS     | -3.7259 | 103.779 | INPTMNOB02  | -3.7864 | 103.841 |
| 007001   | EX08-004 | HD10-001   | 10180077 | 11160218 | TEM1F150LS     | -3.7259 | 103.779 | INPTMNOB02  | -3.7864 | 103.841 |
| 007001   | EX08-004 | HD10-013   | 10180077 | 10170122 | TEM1F150LS     | -3.7259 | 103.779 | INPTMNOB02  | -3.7864 | 103.841 |
| 007001   | EX08-004 | HD10-033   | 10180077 | 10180133 | TEM1F150LS     | -3.7259 | 103.779 | INPTMNOB02  | -3.7864 | 103.841 |
| 007001   | EX08-004 | MTHD10-015 | 10180077 | 10170288 | TEM1F150LS     | -3.7259 | 103.779 | INPTMNOB02  | -3.7864 | 103.841 |
| 007001   | EX08-004 | SMDT03-010 | 10180077 | 12101028 | TEM1F150LS     | -3.7259 | 103.779 | INPTMNOB02  | -3.7864 | 103.841 |
| 007001   | EX08-004 | UNDT02-006 | 10180077 | 10160176 | TEM1F150LS     | -3.7259 | 103.779 | INPTMNOB02  | -3.7864 | 103.841 |
| 007001   | EX10-003 | AT04-006   | 10180111 | 10180056 | PIT1BOB1-PIT 1 |       0 |       0 | BWEU1       | -3.7322 | 103.834 |
| 007001   | EX10-003 | DT02-013   | 10180111 | 12101025 | PIT1BOB1-PIT 1 |       0 |       0 | BWEU1       | -3.7322 | 103.834 |
| 007001   | EX10-003 | DT02-031   | 10180111 | 10150093 | PIT1BOB1-PIT 1 |       0 |       0 | BWEU1       | -3.7322 | 103.834 |
| 007001   | EX10-003 | DT02-053   | 10180111 | 10150130 | PIT1BOB1-PIT 1 |       0 |       0 | BWEU1       | -3.7322 | 103.834 |
| 007001   | EX10-003 | HD10-009   | 10180111 | 10160070 | PIT1BOB1-PIT 1 |       0 |       0 | BWEU1       | -3.7322 | 103.834 |
| 007001   | EX10-003 | HD10-018   | 10180111 | 10150101 | PIT1BOB1-PIT 1 |       0 |       0 | BWEU1       | -3.7322 | 103.834 |
| 007001   | EX10-003 | HD10-021   | 10180111 | 10170097 | PIT1BOB1-PIT 1 |       0 |       0 | BWEU1       | -3.7322 | 103.834 |
| 007001   | EX10-003 | HD10-026   | 10180111 | 10180090 | PIT1BOB1-PIT 1 |       0 |       0 | BWEU1       | -3.7322 | 103.834 |
| 007001   | EX10-003 | HD10-027   | 10180111 | 12101025 | PIT1BOB1-PIT 1 |       0 |       0 | BWEU1       | -3.7322 | 103.834 |
| 007001   | EX10-003 | UNDT02-008 | 10180111 | 10170196 | PIT1BOB1-PIT 1 |       0 |       0 | BWEU1       | -3.7322 | 103.834 |
| 007001   | EX10-006 | AT04-008   | 10160154 | 10150113 | INPTMNOB02     | -3.7864 | 103.841 | TEMMERPATI  | -3.7246 | 103.787 |
| 007001   | EX10-006 | AT04-012   | 10160154 | 10160010 | INPTMNOB02     | -3.7864 | 103.841 | TEMMERPATI  | -3.7246 | 103.787 |
| 007001   | EX10-006 | DT02-022   | 10160154 | 10180040 | INPTMNOB02     | -3.7864 | 103.841 | TEMMERPATI  | -3.7246 | 103.787 |
| 007001   | EX10-006 | HD06-001   | 10160154 | 10170127 | INPTMNOB02     | -3.7864 | 103.841 | TEMMERPATI  | -3.7246 | 103.787 |
| 007001   | EX10-006 | HD06-004   | 10160154 | 10160198 | INPTMNOB02     | -3.7864 | 103.841 | TEMMERPATI  | -3.7246 | 103.787 |
| 007001   | EX10-006 | HD06-009   | 10160154 | 11150033 | INPTMNOB02     | -3.7864 | 103.841 | TEMMERPATI  | -3.7246 | 103.787 |
| 007001   | EX10-006 | HD10-035   | 10160154 | 10170111 | INPTMNOB02     | -3.7864 | 103.841 | TEMMERPATI  | -3.7246 | 103.787 |
| 007001   | EX10-006 | HD10-052   | 10160154 | 10160321 | INPTMNOB02     | -3.7864 | 103.841 | TEMMERPATI  | -3.7246 | 103.787 |
| 007001   | EX10-006 | HD10-062   | 10160154 | 10150124 | INPTMNOB02     | -3.7864 | 103.841 | TEMMERPATI  | -3.7246 | 103.787 |
| 007001   | EX10-006 | UNDT02-007 | 10160154 | 10160194 | INPTMNOB02     | -3.7864 | 103.841 | TEMMERPATI  | -3.7246 | 103.787 |
| 007001   | EX10-009 | AT04-011   | 10180110 | 10180185 | BWEU1          | -3.7322 | 103.834 | TEM1F150LS  | -3.7259 | 103.779 |
| 007001   | EX10-009 | AUDT03-029 | 10180110 | 10180121 | BWEU1          | -3.7322 | 103.834 | TEM1F150LS  | -3.7259 | 103.779 |
| 007001   | EX10-009 | DT02-006   | 10180110 | 10160075 | BWEU1          | -3.7322 | 103.834 | TEM1F150LS  | -3.7259 | 103.779 |
| 007001   | EX10-009 | DT02-015   | 10180110 | 10160121 | BWEU1          | -3.7322 | 103.834 | TEM1F150LS  | -3.7259 | 103.779 |
| 007001   | EX10-009 | DT03-005   | 10180110 | 10160156 | BWEU1          | -3.7322 | 103.834 | TEM1F150LS  | -3.7259 | 103.779 |
| 007001   | EX10-009 | HD06-003   | 10180110 | 10180152 | BWEU1          | -3.7322 | 103.834 | TEM1F150LS  | -3.7259 | 103.779 |
| 007001   | EX10-009 | HD10-037   | 10180110 | 10180170 | BWEU1          | -3.7322 | 103.834 | TEM1F150LS  | -3.7259 | 103.779 |
| 007001   | EX10-009 | MTHD10-006 | 10180110 | 10180106 | BWEU1          | -3.7322 | 103.834 | TEM1F150LS  | -3.7259 | 103.779 |
| 007001   | EX10-009 | OHT10-045  | 10180110 | 10180104 | BWEU1          | -3.7322 | 103.834 | TEM1F150LS  | -3.7259 | 103.779 |
| 007001   | EX10-009 | SMDT03-019 | 10180110 | 10150232 | BWEU1          | -3.7322 | 103.834 | TEM1F150LS  | -3.7259 | 103.779 |
| 007002   | EX08-004 | AUDT03-013 | 10180077 | 12101030 | TEM1F150LS     | -3.7259 | 103.779 | INPTMNOB02  | -3.7864 | 103.841 |
| 007002   | EX08-004 | AUDT03-022 | 10180077 | 10170044 | TEM1F150LS     | -3.7259 | 103.779 | INPTMNOB02  | -3.7864 | 103.841 |
| 007002   | EX08-004 | AUDT03-030 | 10180077 | 10150121 | TEM1F150LS     | -3.7259 | 103.779 | INPTMNOB02  | -3.7864 | 103.841 |
| 007002   | EX08-004 | DT02-034   | 10180077 | 10180008 | TEM1F150LS     | -3.7259 | 103.779 | INPTMNOB02  | -3.7864 | 103.841 |
| 007002   | EX08-004 | DT02-060   | 10180077 | 10160199 | TEM1F150LS     | -3.7259 | 103.779 | INPTMNOB02  | -3.7864 | 103.841 |
| 007002   | EX08-004 | HD06-010   | 10180077 | 10160153 | TEM1F150LS     | -3.7259 | 103.779 | INPTMNOB02  | -3.7864 | 103.841 |
| 007002   | EX08-004 | HD10-025   | 10180077 | 10150127 | TEM1F150LS     | -3.7259 | 103.779 | INPTMNOB02  | -3.7864 | 103.841 |
| 007002   | EX08-004 | HD10-029   | 10180077 | 10150142 | TEM1F150LS     | -3.7259 | 103.779 | INPTMNOB02  | -3.7864 | 103.841 |
| 007002   | EX08-004 | SMDT03-001 | 10180077 | 10170300 | TEM1F150LS     | -3.7259 | 103.779 | INPTMNOB02  | -3.7864 | 103.841 |
| 007002   | EX08-004 | SMDT03-017 | 10180077 | 10180099 | TEM1F150LS     | -3.7259 | 103.779 | INPTMNOB02  | -3.7864 | 103.841 |
| 007002   | EX10-003 | DT02-001   | 10180111 | 10170073 | PIT1BOB1-PIT 1 |       0 |       0 | BWEU1       | -3.7322 | 103.834 |
| 007002   | EX10-003 | DT02-004   | 10180111 | 10180063 | PIT1BOB1-PIT 1 |       0 |       0 | BWEU1       | -3.7322 | 103.834 |
| 007002   | EX10-003 | DT02-032   | 10180111 | 10170126 | PIT1BOB1-PIT 1 |       0 |       0 | BWEU1       | -3.7322 | 103.834 |
| 007002   | EX10-003 | DT02-038   | 10180111 | 10150106 | PIT1BOB1-PIT 1 |       0 |       0 | BWEU1       | -3.7322 | 103.834 |
| 007002   | EX10-003 | HD10-005   | 10180111 | 10170159 | PIT1BOB1-PIT 1 |       0 |       0 | BWEU1       | -3.7322 | 103.834 |
| 007002   | EX10-003 | HD10-045   | 10180111 | 10160044 | PIT1BOB1-PIT 1 |       0 |       0 | BWEU1       | -3.7322 | 103.834 |
| 007002   | EX10-003 | HD10-050   | 10180111 | 12101025 | PIT1BOB1-PIT 1 |       0 |       0 | BWEU1       | -3.7322 | 103.834 |
| 007002   | EX10-003 | HD10-061   | 10180111 | 10160130 | PIT1BOB1-PIT 1 |       0 |       0 | BWEU1       | -3.7322 | 103.834 |
| 007002   | EX10-003 | MTHD10-004 | 10180111 | 10150144 | PIT1BOB1-PIT 1 |       0 |       0 | BWEU1       | -3.7322 | 103.834 |
| 007002   | EX10-003 | SMDT03-020 | 10180111 | 10180131 | PIT1BOB1-PIT 1 |       0 |       0 | BWEU1       | -3.7322 | 103.834 |
| 007002   | EX10-006 | AUDT03-009 | 10160154 | 10160243 | INPTMNOB02     | -3.7864 | 103.841 | TEMMERPATI  | -3.7246 | 103.787 |
| 007002   | EX10-006 | AUDT03-027 | 10160154 | 10180057 | INPTMNOB02     | -3.7864 | 103.841 | TEMMERPATI  | -3.7246 | 103.787 |
| 007002   | EX10-006 | DT02-039   | 10160154 | 10180234 | INPTMNOB02     | -3.7864 | 103.841 | TEMMERPATI  | -3.7246 | 103.787 |
| 007002   | EX10-006 | DT03-004   | 10160154 | 10180142 | INPTMNOB02     | -3.7864 | 103.841 | TEMMERPATI  | -3.7246 | 103.787 |
| 007002   | EX10-006 | HD06-012   | 10160154 | 10170281 | INPTMNOB02     | -3.7864 | 103.841 | TEMMERPATI  | -3.7246 | 103.787 |
| 007002   | EX10-006 | HD10-015   | 10160154 | 10180001 | INPTMNOB02     | -3.7864 | 103.841 | TEMMERPATI  | -3.7246 | 103.787 |
| 007002   | EX10-006 | HD10-031   | 10160154 | 11150049 | INPTMNOB02     | -3.7864 | 103.841 | TEMMERPATI  | -3.7246 | 103.787 |
| 007002   | EX10-006 | HD10-044   | 10160154 | 10150013 | INPTMNOB02     | -3.7864 | 103.841 | TEMMERPATI  | -3.7246 | 103.787 |
| 007002   | EX10-006 | SMDT03-002 | 10160154 | 10170257 | INPTMNOB02     | -3.7864 | 103.841 | TEMMERPATI  | -3.7246 | 103.787 |
| 007002   | EX10-006 | UNDT02-005 | 10160154 | 10160033 | INPTMNOB02     | -3.7864 | 103.841 | TEMMERPATI  | -3.7246 | 103.787 |
| 007002   | EX10-009 | AT04-010   | 10180110 | 10150058 | BWEU1          | -3.7322 | 103.834 | TEM1F150LS  | -3.7259 | 103.779 |
| 007002   | EX10-009 | AUDT03-005 | 10180110 | 10160202 | BWEU1          | -3.7322 | 103.834 | TEM1F150LS  | -3.7259 | 103.779 |
| 007002   | EX10-009 | DT03-006   | 10180110 | 10180162 | BWEU1          | -3.7322 | 103.834 | TEM1F150LS  | -3.7259 | 103.779 |
| 007002   | EX10-009 | HD10-008   | 10180110 | 11150008 | BWEU1          | -3.7322 | 103.834 | TEM1F150LS  | -3.7259 | 103.779 |
| 007002   | EX10-009 | HD10-038   | 10180110 | 12101025 | BWEU1          | -3.7322 | 103.834 | TEM1F150LS  | -3.7259 | 103.779 |
| 007002   | EX10-009 | HD10-040   | 10180110 | 10180149 | BWEU1          | -3.7322 | 103.834 | TEM1F150LS  | -3.7259 | 103.779 |
| 007002   | EX10-009 | HD10-056   | 10180110 | 12101025 | BWEU1          | -3.7322 | 103.834 | TEM1F150LS  | -3.7259 | 103.779 |
| 007002   | EX10-009 | MTHD10-001 | 10180110 | 10170302 | BWEU1          | -3.7322 | 103.834 | TEM1F150LS  | -3.7259 | 103.779 |
| 007002   | EX10-009 | MTHD10-014 | 10180110 | 10170263 | BWEU1          | -3.7322 | 103.834 | TEM1F150LS  | -3.7259 | 103.779 |
| 007002   | EX10-009 | SMDT03-013 | 10180110 | 10180207 | BWEU1          | -3.7322 | 103.834 | TEM1F150LS  | -3.7259 | 103.779 |
+----------+----------+------------+----------+----------+----------------+---------+---------+-------------+---------+---------+

## Offline Mode
- Offline data user (download)
- Offline data actual (download-upload)
- Offline prestart (download)
- Offline equipment (download)
- Offline location (download)
- Offline constant (download)
- Offline material (download)
- Offline GPS (upload)
- Offline ECU (upload)
- Offline device status (upload)
- Offline activity; equipment-log-start (upload)
- Offline safety status (upload)
- Offline operation cycle; operation-actual-raw (upload)
- Offline prestart log (upload)
- Offline fatigue log (upload)

## JavaFX Command

mvn clean compile assembly:single

## ToDo
- disable logalat ecu
- fix on screen keyboard

## Snippet
```
-fx-border-style: solid inside;
-fx-border-width: 1;
-fx-border-insets: 1;
-fx-border-color: RED;

 style="-fx-border-style: solid inside;-fx-border-width: 1;-fx-border-insets: 1;-fx-border-color: RED;"

 &#10;

```

## Flow Process
- pemindahan loader, mengharuskan semua hauler dibawahnya stop
- pemintahan loader, secara default hauler sama dg hauler sebelumnya, tapi dapat juga berubah, tergantung disptcher
- perjalanan ke tempat pengisian fuel masih dianggap operation travel
- dispatcher dapat melogout semua screen device secara remote
- dispatcher dapat mengetahui status semua unit
- geofencing berdasarkan radius yang diset dispatcher


## Version

