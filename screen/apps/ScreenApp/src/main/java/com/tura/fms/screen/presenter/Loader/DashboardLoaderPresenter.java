package com.tura.fms.screen.presenter.Loader;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.database.Dashboard_DB;
import com.tura.fms.screen.helpers.SessionFMS;
import com.tura.fms.screen.models.dashboard_model.DashboardDetail;
import com.tura.fms.screen.models.dashboard_model.DashboardModel;
import com.tura.fms.screen.network.NetworkConnection;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DashboardLoaderPresenter {

    private ObservableList<DashboardModel> recordsData = FXCollections.observableArrayList();
    String topic_subcribe = "/fms/dashboardhauler";
    DashboardDetail dashboardDetail = new DashboardDetail();
    private static final Logger log = LoggerFactory.getLogger(DashboardLoaderPresenter.class);
    // MqttLib mqttLib ;
    // SessionFMS sessionFMS;

    public static final NetworkConnection networkConnection = new NetworkConnection();

    Dashboard_DB dashboard_db;

    public DashboardLoaderPresenter() {
        // mqttLib = new MqttLib();
        dashboard_db = new Dashboard_DB();
        // sessionFMS = new SessionFMS();
    }

    public void clearModel() {
        recordsData.clear();
        // gridata.getChildren().clear();
    }

    public void connect_close() {

        /*
         * try { mqttLib.clientMqtt().disconnect(); mqttLib.clientMqtt().close(); }
         * catch (MqttException e) { e.printStackTrace(); }
         */

    }

    // isi data model
    public void isiModel(DashboardDetail dashboardDetailPram) {

        String status = "Not Working";
        try {
            if (dashboardDetailPram.getStatus() != null) {
                status = dashboardDetailPram.getStatus();
            } else {
                status = "Not Working";
            }

        } catch (Exception e) {
        }

        String statusText = "Activity";
        if (dashboardDetailPram.getStatusText() != null) {
            statusText = dashboardDetailPram.getStatusText();
        }

        String from = "-";
        try {

            if (dashboardDetailPram.getLoadingPoint() != null) {
                from = dashboardDetailPram.getLoadingPoint();
            } else {
                from = "-";
            }

        } catch (Exception e) {
        }

        String to = "-";
        try {
            if (dashboardDetailPram.getDumpingPoint() != null) {
                to = dashboardDetailPram.getDumpingPoint();
            } else {
                to = "-";
            }

        } catch (Exception e) {
        }

        String cycle = "0";
        try {
            if (dashboardDetailPram.getCycle() != null) {
                cycle = String.valueOf(dashboardDetailPram.getCycle().intValue());
            } else {
                cycle = "0";
            }

        } catch (Exception e) {
        }

        String distance = "0 M";
        try {
            if (dashboardDetailPram.getDistance() != null) {
                distance = dashboardDetailPram.getDistance().toString();
            } else {
                distance = "0 M";
            }

        } catch (Exception e) {
        }

        String tonase = "0%";
        try {
            if (dashboardDetailPram.getPayload() != null) {
                tonase = dashboardDetailPram.getPayload().toString();
            } else {
                tonase = "0%";
            }

        } catch (Exception e) {
        }

        String speed = "0 Km/H";
        try {
            if (dashboardDetailPram.getSpeed() != null) {
                speed = dashboardDetailPram.getSpeed().toString();
            } else {
                speed = "0 Km/H";
            }

        } catch (Exception e) {
        }

        String fuel = "0  Ltr/H";
        try {
            if (dashboardDetailPram.getFuel() != null) {
                fuel = dashboardDetailPram.getFuel().toString() + "  Ltr/H";
            } else {
                fuel = "0  Ltr/H";
            }

        } catch (Exception e) {
        }

        String temp = "0 C";
        try {
            if (dashboardDetailPram.getTemperatur() != null) {
                temp = dashboardDetailPram.getTemperatur().toString() + " C";
            } else {
                temp = "0 C";
            }

        } catch (Exception e) {
        }

        String hour_meter = "0 H";
        try {
            if (dashboardDetailPram.getHourMeter() != null) {
                hour_meter = dashboardDetailPram.getHourMeter().toString() + " H";
            } else {
                hour_meter = "0 H";
            }

        } catch (Exception e) {
        }

        // recordsData.add(new DashboardModel("1", statusText, status));
        recordsData.add(new DashboardModel("1", "Location", from));
        recordsData.add(new DashboardModel("2", "Hour Meter", hour_meter));
        recordsData.add(new DashboardModel("3", "Tonase/BCM", tonase));
        recordsData.add(new DashboardModel("4", "Cycle", cycle));
        recordsData.add(new DashboardModel("5", "Fuel Rate", fuel));
        recordsData.add(new DashboardModel("6", "Engine Temperature", temp));

    }

    public ObservableList<DashboardModel> runConsumer() {
        if (MainApp.latestActivity != null) {
            MainApp.dashboardInfo.setStatus(MainApp.latestActivity.getCdActivity().getName());
        }
        if (MainApp.actualInfo != null) {
            MainApp.dashboardInfo.setLoadingPoint(MainApp.actualInfo.getId_location().getName());
            MainApp.dashboardInfo.setDumpingPoint(MainApp.actualInfo.getId_destination().getName());
        }

        isiModel(MainApp.dashboardInfo);

        return recordsData;
    }

}
