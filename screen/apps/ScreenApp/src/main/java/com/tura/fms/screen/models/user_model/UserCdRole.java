package com.tura.fms.screen.models.user_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserCdRole {

    @SerializedName("cd")
    @Expose
    private String cd;

    @SerializedName("cd_self")
    @Expose
    private String cd_self;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("json")
    @Expose
    private JsonCdRole json = null;

    public JsonCdRole getJson() {
        return json;
    }

    public void setJson(JsonCdRole json) {
        this.json = json;
    }

    public String getCd() {
        return cd;
    }

    public void setCd(String cd) {
        this.cd = cd;
    }

    public String getCd_self() {
        return cd_self;
    }

    public void setCd_self(String cd_self) {
        this.cd_self = cd_self;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
