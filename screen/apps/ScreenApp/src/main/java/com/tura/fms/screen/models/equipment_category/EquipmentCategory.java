package com.tura.fms.screen.models.equipment_category;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.tura.fms.screen.models.constant.Constant;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "cd_manufactur", "cd_product", "cd_type", "cd_brand", "cd_model", "cd_class",
        "bucket_capacity", "fuel_ratio", "kapasitas_tangki", "productivity", "pa" })
public class EquipmentCategory {

    @JsonProperty("id")
    private String id;
    @JsonProperty("cd_manufactur")
    private Constant cd_manufactur;
    @JsonProperty("cd_product")
    private Constant cd_product;
    @JsonProperty("cd_type")
    private Constant cd_type;
    @JsonProperty("cd_brand")
    private Constant cd_brand;
    @JsonProperty("cd_model")
    private Constant cd_model;
    @JsonProperty("cd_class")
    private Constant cd_class;
    @JsonProperty("bucket_capacity")
    private Double bucket_capacity;

    @JsonProperty("fuel_ratio")
    private Double fuel_ratio;
    @JsonProperty("kapasitas_tangki")
    private Double kapasitas_tangki;
    @JsonProperty("productivity")
    private Double productivity;
    @JsonProperty("pa")
    private Double pa;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("cd_manufactur")
    public Constant getCdManufactur() {
        return cd_manufactur;
    }

    @JsonProperty("cd_manufactur")
    public void setCdManufactur(Constant manufactur) {
        this.cd_manufactur = manufactur;
    }

    @JsonProperty("cd_product")
    public Constant getCdProduct() {
        return cd_product;
    }

    @JsonProperty("cd_product")
    public void setCdProduct(Constant product) {
        this.cd_product = product;
    }

    @JsonProperty("cd_type")
    public Constant getCdType() {
        return cd_type;
    }

    @JsonProperty("cd_type")
    public void setCdType(Constant tipe) {
        this.cd_type = tipe;
    }

    @JsonProperty("cd_brand")
    public Constant getCdBrand() {
        return cd_brand;
    }

    @JsonProperty("cd_brand")
    public void setCdBrand(Constant brand) {
        this.cd_brand = brand;
    }

    @JsonProperty("cd_model")
    public Constant getCdModel() {
        return cd_model;
    }

    @JsonProperty("cd_model")
    public void setCdModel(Constant model) {
        this.cd_model = model;
    }

    @JsonProperty("cd_class")
    public Constant getCdClass() {
        return cd_class;
    }

    @JsonProperty("cd_class")
    public void setCdClass(Constant cdclass) {
        this.cd_class = cdclass;
    }

    @JsonProperty("bucket_capacity")
    public Double getBucketCapacity() {
        return bucket_capacity;
    }

    @JsonProperty("bucket_capacity")
    public void setBucketCapacity(Double bucket_capacity) {
        this.bucket_capacity = bucket_capacity;
    }

    @JsonProperty("fuel_ratio")
    public Double getFuelRatio() {
        return fuel_ratio;
    }

    @JsonProperty("fuel_ratio")
    public void setFuelRatio(Double fuel_ratio) {
        this.fuel_ratio = fuel_ratio;
    }

    @JsonProperty("kapasitas_tangki")
    public Double getKapasitasTangki() {
        return kapasitas_tangki;
    }

    @JsonProperty("kapasitas_tangki")
    public void setKapasitasTangki(Double kapasitas_tangki) {
        this.kapasitas_tangki = kapasitas_tangki;
    }

    @JsonProperty("productivity")
    public Double getProductivity() {
        return productivity;
    }

    @JsonProperty("productivity")
    public void setProductivity(Double productivity) {
        this.productivity = productivity;
    }

    @JsonProperty("pa")
    public Double getPA() {
        return pa;
    }

    @JsonProperty("pa")
    public void setPA(Double pa) {
        this.pa = pa;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
