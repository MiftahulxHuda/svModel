package com.tura.fms.screen.models.prestart_model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PreStartValModel {

    private final StringProperty id;
    private final StringProperty name;
    private final BooleanProperty nilai;

    public PreStartValModel(String id, String name,Boolean nilai) {
        this.id = new SimpleStringProperty(id);
        this.name = new SimpleStringProperty(name);
        this.nilai = new SimpleBooleanProperty(nilai);
    }

    public String getId() {
        return id.get();
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public StringProperty idProperty() {
        return id;
    }

    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public StringProperty nameProperty() {
        return name;
    }

    public Boolean getNilai() {
        return nilai.get();
    }

    public void setNilai(Boolean nilai) {
        this.nilai.set(nilai);
    }

    public BooleanProperty nilaiProperty() {
        return nilai;
    }

}
