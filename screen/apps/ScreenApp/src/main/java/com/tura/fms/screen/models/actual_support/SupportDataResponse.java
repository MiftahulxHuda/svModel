package com.tura.fms.screen.models.actual_support;

import com.google.gson.annotations.SerializedName;

public class SupportDataResponse {

    @SerializedName("output")
    private OperationSupport output;

    @SerializedName("response")
    private String response;

    @SerializedName("message")
    private String message;

    public OperationSupport getOutput() {
        return output;
    }

    public void setOutput(OperationSupport output) {
        this.output = output;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String res) {
        this.response = res;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String mes) {
        this.message = mes;
    }

}
