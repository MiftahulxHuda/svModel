package com.tura.fms.screen.models.maintenance_model.task_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.constant.Constant;
import com.tura.fms.screen.models.user_model.UserActual;

public class MaintenanceDetails {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("maintenance_id")
    @Expose
    private Object maintenance_id;

    @SerializedName("date")
    @Expose
    private Long date;

    @SerializedName("problem_id")
    @Expose
    private Constant problem_id;

    @SerializedName("status_id")
    @Expose
    private Constant status_id;

    @SerializedName("start_time")
    @Expose
    private String start_time;

    @SerializedName("end_time")
    @Expose
    private String end_time;

    @SerializedName("mechanic_id")
    @Expose
    private UserActual mechanic_id;

    @SerializedName("cd_shift")
    @Expose
    private Constant cd_shift;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Object getMaintenanceId() {
        return maintenance_id;
    }

    public void setMaintenanceId(Object mainte) {
        this.maintenance_id = mainte;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long dt) {
        this.date = dt;
    }

    public Constant getProblemId() {
        return problem_id;
    }

    public void setProblemId(Constant problem) {
        this.problem_id = problem;
    }

    public Constant getStatusId() {
        return status_id;
    }

    public void setStatusId(Constant stat) {
        this.status_id = stat;
    }

    public String getStartTime() {
        return start_time;
    }

    public void setStartTime(String start) {
        this.start_time = start;
    }

    public String getEndTime() {
        return end_time;
    }

    public void setEndTime(String end) {
        this.end_time = end;
    }

    public UserActual getMechanicId() {
        return mechanic_id;
    }

    public void setMechanicId(UserActual mec) {
        this.mechanic_id = mec;
    }

    public Constant getCdShift() {
        return cd_shift;
    }

    public void setCdShift(Constant cd) {
        this.cd_shift = cd;
    }

    public String getDateLabel() {
        return Utilities.timeStampToDate(this.date, "tanggal");
    }

    public String getShiftLabel() {
        return this.cd_shift.getName();
    }

    public String getProblemLabel() {
        return this.problem_id.getName();
    }

    public String getStatusLabel() {
        return this.status_id.getName();
    }

    public String getMechanicLabel() {
        return this.mechanic_id.getName();
    }

    public String getStartLabel() {
        return this.start_time;
    }

    public String getStartHour() {
        if (start_time == null) {
            return "";
        }
        String[] st = this.start_time.split(":");
        if (st != null) {
            return st[0];
        } else {
            return "";
        }
    }

    public String getStartMinute() {
        if (start_time == null) {
            return "";
        }
        String[] ed = this.start_time.split(":");
        if (ed != null) {
            return ed[1];
        } else {
            return "";
        }
    }

    public String getEndLabel() {
        return this.end_time;
    }

    public String getEndHour() {
        if (end_time == null) {
            return "";
        }
        String[] st = this.end_time.split(":");
        if (st != null) {
            return st[0];
        } else {
            return "";
        }
    }

    public String getEndMinute() {
        if (end_time == null) {
            return "";
        }
        String[] ed = this.end_time.split(":");
        if (ed != null) {
            return ed[1];
        } else {
            return "";
        }
    }

}