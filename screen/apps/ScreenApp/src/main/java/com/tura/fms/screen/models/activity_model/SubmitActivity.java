package com.tura.fms.screen.models.activity_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubmitActivity {

    @SerializedName("id_equipment")
    @Expose
    private String id_equipment;

    @SerializedName("id_actual")
    @Expose
    private String id_actual;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("cd_operation")
    @Expose
    private String cd_operation;

    @SerializedName("cd_type_proccess")
    @Expose
    private String cd_type_proccess;

    @SerializedName("id_user")
    @Expose
    private String id_user;

    @SerializedName("activity_before")
    @Expose
    private String activity_before;

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getCd_operation() {
        return cd_operation;
    }

    public void setCd_operation(String cd_operation) {
        this.cd_operation = cd_operation;
    }

    public String getCd_type_proccess() {
        return cd_type_proccess;
    }

    public void setCd_type_proccess(String cd_type_proccess) {
        this.cd_type_proccess = cd_type_proccess;
    }

    public String getId_equipment() {
        return id_equipment;
    }

    public void setId_equipment(String id_equipment) {
        this.id_equipment = id_equipment;
    }

    public String getId_actual() {
        return id_actual;
    }

    public void setId_actual(String id_actual) {
        this.id_actual = id_actual;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getActivityBefore() {
        return activity_before;
    }

    public void setActivityBefore(String activity_before) {
        this.activity_before = activity_before;
    }
}
