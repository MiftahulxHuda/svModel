package com.tura.fms.screen.models.equipment;

import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.database.Operation;
import com.tura.fms.screen.database.Synchronizer;
import com.tura.fms.screen.models.ResponseModel;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;

import retrofit2.Call;
import retrofit2.Response;

public class EquipmentDB extends Operation {
    public static final String TABLE_NAME = "Equipment_Record";

    private static final String id = "id";
    private static final String id_org = "id_org";
    private static final String id_operator = "id_operator";
    private static final String cd_operation = "cd_operation";
    private static final String cd_equipment = "cd_equipment";
    private static final String name = "name";
    private static final String capacity_real = "capacity_real";
    private static final String capacity_labs = "capacity_labs";
    private static final String enum_availability = "enum_availability";
    private static final String is_deleted = "is_deleted";
    private static final String date_submitted = "date_submitted";
    private static final String date_modified = "date_modified";
    private static final String json = "json";
    private static final String empty_weight = "empty_weight";
    private static final String load_weight = "load_weight";
    private static final String capacity_labs_2 = "capacity_labs_2";
    private static final String capacity_labs_3 = "capacity_labs_3";
    private static final String hour_meter = "hour_meter";
    private static final String id_location = "id_location";
    private static final String code = "code";
    private static final String hm_start = "hm_start";
    private static final String hm_now = "hm_now";
    private static final String hm_date = "hm_date";
    private static final String id_equipment_category = "id_equipment_category";
    private static final String sync_status = "sync_status";

    public EquipmentDB() {
        super(EquipmentDB.TABLE_NAME, ConstantValues.DATA_EQUIPMENT);
    }

    public void createTableEquipment() {
        try {
            DatabaseMetaData dbm = conn().getMetaData();
            ResultSet rs = dbm.getTables(null, null, TABLE_NAME, null);
            Statement stmt = null;
            if (rs.next()) {
                System.out.println("Table equipment exists");
            } else {
                stmt = conn().createStatement();
                stmt.setQueryTimeout(10);
                stmt.executeUpdate(createTable());
                stmt.close();
                System.out.println("Table equipment created");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String createTable() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + id + " TEXT PRIMARY KEY," + id_org + " TEXT NULL,"
                + id_operator + " TEXT NULL," + cd_operation + " TEXT NULL," + cd_equipment + " TEXT NULL," + name
                + " TEXT NULL," + capacity_labs + " TEXT NULL," + capacity_labs_2 + " TEXT NULL," + capacity_labs_3
                + " TEXT NULL," + capacity_real + " TEXT NULL," + is_deleted + " TEXT NULL," + date_modified
                + " TEXT NULL," + date_submitted + " TEXT NULL," + json + " TEXT NULL," + empty_weight + " TEXT NULL,"
                + load_weight + " TEXT NULL," + hour_meter + " TEXT NULL," + id_location + " TEXT NULL," + code
                + " TEXT NULL," + hm_date + " TEXT NULL," + hm_now + " TEXT NULL," + hm_start + " TEXT NULL,"
                + id_equipment_category + " TEXT NULL," + sync_status + " NUMBER NULL" + ")";
    }

    @Override
    public Object insertIgnore(Object eqa) {
        Equipment eq = Synchronizer.mapper.convertValue(eqa, Equipment.class);
        PreparedStatement st = null;
        Boolean rs = null;
        try {
            String sql = "INSERT INTO " + TABLE_NAME + " " + "(" + id + "," + id_org + "," + id_operator + ","
                    + cd_operation + "," + cd_equipment + "," + name + "," + capacity_labs + "," + capacity_labs_2 + ","
                    + capacity_labs_3 + "," + capacity_real + "," + is_deleted + "," + date_modified + ","
                    + date_submitted + "," + json + "," + empty_weight + "," + load_weight + "," + hour_meter + ","
                    + id_location + "," + code + "," + hm_start + "," + hm_date + "," + hm_now + ","
                    + id_equipment_category + "," + sync_status + ")"
                    + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,1) ON CONFLICT(id) DO UPDATE SET " + id
                    + "=?," + id_org + "=?," + id_operator + "=?," + cd_operation + "=?," + cd_equipment + "=?," + name
                    + "=?," + capacity_labs + "=?," + capacity_labs_2 + "=?," + capacity_labs_3 + "=?," + capacity_real
                    + "=?," + id_equipment_category + "=?;";

            st = conn().prepareStatement(sql);
            st.setString(1, eq.getId());
            st.setString(24, eq.getId());
            if (eq.getIdOrg() != null) {
                st.setString(2, eq.getIdOrg().getId());
                st.setString(25, eq.getIdOrg().getId());
            } else {
                st.setString(2, "");
                st.setString(25, "");
            }
            if (eq.getIdOperator() != null) {
                st.setString(3, eq.getIdOperator().getId());
                st.setString(26, eq.getIdOperator().getId());
            } else {
                st.setString(3, "");
                st.setString(26, "");
            }
            if (eq.getCdOperation() != null) {
                st.setString(4, eq.getCdOperation().getCd());
                st.setString(27, eq.getCdOperation().getCd());
            } else {
                st.setString(4, "");
                st.setString(27, "");
            }
            if (eq.getCdEquipment() != null) {
                st.setString(5, eq.getCdEquipment().getCd());
                st.setString(28, eq.getCdEquipment().getCd());
            } else {
                st.setString(5, "");
                st.setString(28, "");
            }
            if (eq.getName() != null) {
                st.setString(6, eq.getName());
                st.setString(29, eq.getName());
            } else {
                st.setString(6, "");
                st.setString(29, "");
            }
            if (eq.getCapacityLabs() != null) {
                st.setDouble(7, eq.getCapacityLabs());
                st.setDouble(30, eq.getCapacityLabs());
            } else {
                st.setDouble(7, 0);
                st.setDouble(30, 0);
            }
            if (eq.getCapacityLabs2() != null) {
                st.setDouble(8, eq.getCapacityLabs2());
                st.setDouble(31, eq.getCapacityLabs2());
            } else {
                st.setDouble(8, 0);
                st.setDouble(31, 0);
            }
            if (eq.getCapacityLabs3() != null) {
                st.setDouble(9, eq.getCapacityLabs3());
                st.setDouble(32, eq.getCapacityLabs3());
            } else {
                st.setDouble(9, 0);
                st.setDouble(32, 0);
            }
            if (eq.getCapacityReal() != null) {
                st.setInt(10, eq.getCapacityReal());
                st.setInt(33, eq.getCapacityReal());
            } else {
                st.setInt(10, 0);
                st.setInt(33, 0);
            }
            if (eq.getIsDeleted() != null) {
                st.setInt(11, eq.getIsDeleted());
            } else {
                st.setInt(11, 0);
            }
            if (eq.getDateModified() != null) {
                st.setInt(12, eq.getDateModified());
            } else {
                st.setInt(12, 0);
            }
            if (eq.getDateSubmitted() != null) {
                st.setInt(13, eq.getDateSubmitted());
            } else {
                st.setInt(13, 0);
            }
            if (eq.getJson() != null) {
                st.setString(14, eq.getJson());
            } else {
                st.setString(14, eq.getJson());
            }
            if (eq.getEmptyWeight() != null) {
                st.setInt(15, eq.getEmptyWeight());
            } else {
                st.setInt(15, 0);
            }
            if (eq.getLoadWeight() != null) {
                st.setInt(16, eq.getLoadWeight());
            } else {
                st.setInt(16, 0);
            }
            if (eq.getHourMeter() != null) {
                st.setInt(17, eq.getHourMeter());
            } else {
                st.setInt(17, 0);
            }
            if (eq.getIdLocation() != null) {
                st.setString(18, eq.getIdLocation().getId());
            } else {
                st.setString(18, "");
            }
            st.setString(19, eq.getCode());
            if (eq.getHmStart() != null) {
                st.setInt(20, eq.getHmStart());
            } else {
                st.setInt(20, -1);
            }
            if (eq.getHmDate() != null) {
                st.setInt(21, eq.getHmDate());
            } else {
                st.setInt(21, -1);
            }
            if (eq.getHmNow() != null) {
                st.setInt(22, eq.getHmNow());
            } else {
                st.setInt(22, -1);
            }
            if (eq.getEquipmentCategory() != null) {
                st.setString(23, eq.getEquipmentCategory().getId());
                st.setString(34, eq.getEquipmentCategory().getId());
            } else {
                st.setString(23, null);
                st.setString(34, null);
            }
            rs = st.execute();
            // System.out.println("----------------");
            // System.out.println(rs);
            st.close();

            return eq;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs && st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
        return null;
    }

    @Override
    public void deleteData(String ids) {
        String sql = "delete from " + TABLE_NAME + " where " + id + "='" + ids + "'";
        try {
            Statement stmt = conn().createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object getFromServer(Integer page, Integer size) {
        if (!NetworkConnection.getServiceStatus()) {
            return null;
        }
        Call<Object> call = APIClient.getInstance().getEquipment(MainApp.requestHeader, "", "", page, size);
        try {
            Response<Object> response = call.execute();
            return response.body();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Object getFromServerById(String id) {
        if (!NetworkConnection.getServiceStatus()) {
            return null;
        }
        Call<Object> call = APIClient.getInstance().getEquipmentById(MainApp.requestHeader, id);
        try {
            Response<Object> response = call.execute();
            return response.body();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Integer getRemoteCount() {
        if (!NetworkConnection.getServiceStatus()) {
            return 0;
        }
        Call<Object> call = APIClient.getInstance().getEquipmentCount(MainApp.requestHeader);
        try {
            Response<Object> response = call.execute();
            Map<String, Number> mapped = APIClient.mapper.convertValue(response.body(), Map.class);
            if (mapped.get("output") != null) {
                return mapped.get("output").intValue();
            } else {
                return 0;
            }
        } catch (IOException ex) {
            // ex.printStackTrace();
            System.out.println("[Equipment getRemoteCount] " + ex.getMessage());
        }
        return 0;
    }

    @Override
    public List<Object> getFlaggedData() {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE sync_status=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, "0");
            ResultSet cursor = preparedStatement.executeQuery();
            List<Object> result = new ArrayList<Object>();
            while (cursor.next()) {
                EquipmentLocal eq = new EquipmentLocal();
                eq.setId(cursor.getString("id"));
                eq.setId_org(cursor.getString("id_org"));
                if (!cursor.getString("id_operator").isEmpty()) {
                    eq.setId_operator(cursor.getString("id_operator"));
                } else {
                    eq.setId_operator(null);
                }
                eq.setCd_equipment(cursor.getString("cd_equipment"));
                eq.setCd_operation(cursor.getString("cd_operation"));
                eq.setName(cursor.getString("name"));
                eq.setCapacity_labs(cursor.getInt("capacity_labs"));
                eq.setCapacity_labs_2(cursor.getInt("capacity_labs_2"));
                eq.setCapacity_labs_3(cursor.getInt("capacity_labs_3"));
                eq.setCapacity_real(cursor.getInt("capacity_real"));
                // eq.setIsDeleted(cursor.getInt("is_deleted"));
                eq.setIs_deleted(0);
                eq.setDate_modified(cursor.getInt("date_modified"));
                eq.setDate_submitted(cursor.getInt("date_submitted"));
                // eq.setJson(cursor.getString("cd_operation"));
                eq.setEmpty_weight(cursor.getInt("empty_weight"));
                eq.setLoad_weight(cursor.getInt("load_weight"));
                eq.setHour_meter(cursor.getInt("hour_meter"));
                eq.setId_location(cursor.getString("id_location"));
                eq.setCode(cursor.getString("code"));
                eq.setHm_date(cursor.getInt("hm_date"));
                eq.setHm_now(cursor.getInt("hm_now"));
                eq.setHm_start(cursor.getInt("hm_start"));
                eq.setEquipmentCategory(cursor.getString("id_equipment_category"));
                result.add(eq);
            }
            cursor.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void postBulkData(List<Object> eq) {
        if (!NetworkConnection.getServiceStatus()) {
            return;
        }
        System.out.println("--- post synchronization ---");
        Call<ResponseModel> call = APIClient.getInstance().postEquipment(MainApp.requestHeader, eq);
        try {
            Response<ResponseModel> response = call.execute();
            System.out.println(response);
            System.out.println(response.body().getResponse());
            System.out.println(response.body().getMessage());
            if (response.body().getResponse().equals("success")) {
                System.out.println("post success");
                /**
                 * change flag (sync_status to 1)
                 */
                for (Object obj : eq) {
                    EquipmentLocal oqe = (EquipmentLocal) obj;
                    setSynchronized(oqe.getId());
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void setSynchronized(String ids) {
        String sql = "update " + TABLE_NAME + " set sync_status=1 where " + id + "='" + ids + "'";
        try {
            Statement stmt = conn().createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object getLocalOneById(String fieldId, String searchId) {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + fieldId + "=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, searchId);
            ResultSet cursor = preparedStatement.executeQuery();
            if (cursor.next()) {
                EquipmentLocal eq = new EquipmentLocal();
                eq.setId(cursor.getString("id"));
                eq.setId_org(cursor.getString("id_org"));
                if (!cursor.getString("id_operator").isEmpty()) {
                    eq.setId_operator(cursor.getString("id_operator"));
                } else {
                    eq.setId_operator(null);
                }
                eq.setCd_equipment(cursor.getString("cd_equipment"));
                eq.setCd_operation(cursor.getString("cd_operation"));
                eq.setName(cursor.getString("name"));
                eq.setCapacity_labs(cursor.getInt("capacity_labs"));
                eq.setCapacity_labs_2(cursor.getInt("capacity_labs_2"));
                eq.setCapacity_labs_3(cursor.getInt("capacity_labs_3"));
                eq.setCapacity_real(cursor.getInt("capacity_real"));
                // eq.setIsDeleted(cursor.getInt("is_deleted"));
                eq.setIs_deleted(0);
                eq.setDate_modified(cursor.getInt("date_modified"));
                eq.setDate_submitted(cursor.getInt("date_submitted"));
                // eq.setJson(cursor.getString("cd_operation"));
                eq.setEmpty_weight(cursor.getInt("empty_weight"));
                eq.setLoad_weight(cursor.getInt("load_weight"));
                eq.setHour_meter(cursor.getInt("hour_meter"));
                eq.setId_location(cursor.getString("id_location"));
                eq.setCode(cursor.getString("code"));
                eq.setHm_date(cursor.getInt("hm_date"));
                eq.setHm_now(cursor.getInt("hm_now"));
                eq.setHm_start(cursor.getInt("hm_start"));
                eq.setEquipmentCategory(cursor.getString("id_equipment_category"));

                cursor.close();
                return (Object) eq;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
