package com.tura.fms.screen.models.equipment;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.tura.fms.screen.models.constant.Constant;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "id_actual", "id_operation_support", "id_operation_loader", "time", "id_equipment",
        "id_operator", "cd_activity", "cd_tum", "cd_type", "latitude", "longitude", "created_by" })
public class EquipmentLogStartLocal {

    @JsonProperty("id")
    private String id;
    @JsonProperty("id_actual")
    private String id_actual;
    @JsonProperty("id_operation_support")
    private String id_operation_support;
    @JsonProperty("id_operation_loader")
    private String id_operation_loader;
    @JsonProperty("time")
    private Integer time;
    @JsonProperty("id_equipment")
    private Object id_equipment;
    @JsonProperty("id_operator")
    private Object id_operator;
    @JsonProperty("cd_activity")
    private Constant cd_activity;
    @JsonProperty("cd_tum")
    private Constant cd_tum;
    @JsonProperty("cd_type")
    private String cd_type;
    @JsonProperty("id_equipment_user")
    private String id_equipment_user;
    @JsonProperty("created_by")
    private String created_by;

    @JsonProperty("latitude")
    private Double latitude;

    @JsonProperty("longitude")
    private Double longitude;

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double id) {
        this.latitude = id;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double id) {
        this.longitude = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdEquipmentUser() {
        return id_equipment_user;
    }

    public void setIdEquipmentUser(String id) {
        this.id_equipment_user = id;
    }

    public String getIdActual() {
        return id_actual;
    }

    public void setIdActual(String id_actual) {
        this.id_actual = id_actual;
    }

    public String getIdOperationSupport() {
        return id_operation_support;
    }

    public void setIdOperationSupport(String id_operation_support) {
        this.id_operation_support = id_operation_support;
    }

    public String getIdOperationLoader() {
        return id_operation_loader;
    }

    public void setIdOperationLoader(String id_operation_loader) {
        this.id_operation_loader = id_operation_loader;
    }

    public Object getIdEquipment() {
        return id_equipment;
    }

    public void setIdEquipment(Object id_equipment) {
        this.id_equipment = id_equipment;
    }

    public Object getIdOperator() {
        return id_operator;
    }

    public void setIdOperator(Object id_operator) {
        this.id_operator = id_operator;
    }

    public Constant getCdActivity() {
        return cd_activity;
    }

    public void setCdActivity(Constant cd_activity) {
        this.cd_activity = cd_activity;
    }

    public Constant getCdTum() {
        return cd_tum;
    }

    public void setCdTum(Constant cd_tum) {
        this.cd_tum = cd_tum;
    }

    public String getCdType() {
        return cd_type;
    }

    public void setCdType(String cdtype) {
        this.cd_type = cdtype;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getCreatedBy() {
        return created_by;
    }

    public void setCreatedBy(String createdBy) {
        this.created_by = createdBy;
    }

}
