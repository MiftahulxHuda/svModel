package com.tura.fms.screen.models.maintenance_model.task_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProblemItem{

	@SerializedName("name")
	@Expose
	private String name;

	@SerializedName("cd")
	@Expose
	private String cd;

	public String getCd() {
		return cd;
	}

	public void setCd(String cd) {
		this.cd = cd;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}


}
