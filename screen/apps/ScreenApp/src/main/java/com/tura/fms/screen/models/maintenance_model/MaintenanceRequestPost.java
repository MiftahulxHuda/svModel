package com.tura.fms.screen.models.maintenance_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MaintenanceRequestPost {

    @SerializedName("response")
    @Expose
    String response;

    @SerializedName("message")
    @Expose
    String message;

    @SerializedName("output")
    @Expose
    MaintenanceRequestDetail output;

    public MaintenanceRequestDetail getOutput() {
        return output;
    }

    public void setOutput(MaintenanceRequestDetail output) {
        this.output = output;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
