package com.tura.fms.screen.models;

import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.database.Operation;
import com.tura.fms.screen.network.NetworkConnection;

public class ScreenConfigDB extends Operation {

    public static final String TABLE_NAME = "ScreenConfig_Record";

    private static final String name = "name";
    private static final String value = "value";

    public ScreenConfigDB() {
        super(ScreenConfigDB.TABLE_NAME, ConstantValues.DATA_SCREEN_CONFIG);
    }

    public void createTableScreenConfig() {
        try {
            DatabaseMetaData dbm = conn().getMetaData();
            ResultSet rs = dbm.getTables(null, null, TABLE_NAME, null);
            Statement stmt = null;
            if (rs.next()) {
                System.out.println("Table screenConfig exists");
            } else {
                stmt = conn().createStatement();
                stmt.setQueryTimeout(10);
                stmt.executeUpdate(createTable());
                stmt.close();
                System.out.println("Table screenConfig created");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String createTable() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + name + " TEXT PRIMARY KEY," + value + " TEXT NULL"
                + ")";
    }

    @Override
    public Object insertIgnore(Object eqa) {
        ScreenConfigLocal eq = (ScreenConfigLocal) eqa;
        PreparedStatement st = null;
        Boolean rs = null;
        try {
            String sql = "INSERT INTO " + TABLE_NAME + " " + "(" + name + "," + value + ")"
                    + " VALUES(?,?) ON CONFLICT(name) DO UPDATE SET " + name + "=?," + value + "=?";

            st = conn().prepareStatement(sql);

            st.setString(1, eq.getName());
            st.setString(3, eq.getName());
            st.setString(2, eq.getValue() == null ? null : eq.getValue().toString());
            st.setString(4, eq.getValue() == null ? null : eq.getValue().toString());

            rs = st.execute();
            st.close();

            return eq;

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (rs && st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
        return null;
    }

    @Override
    public void deleteData(String ids) {
        String sql = "delete from " + TABLE_NAME + " where " + name + "=?";
        try {
            PreparedStatement ps = conn().prepareStatement(sql);
            ps.setString(1, ids);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object getFromServer(Integer page, Integer size) {
        if (!NetworkConnection.getServiceStatus()) {
            return null;
        }
        // no action
        return null;
    }

    @Override
    public Integer getRemoteCount() {
        if (!NetworkConnection.getServiceStatus()) {
            return 0;
        }
        // no action
        return 0;
    }

    @Override
    public List<Object> getFlaggedData() {
        return null;
    }

    public List<ScreenConfigLocal> getAllLocal() {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME;
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            ResultSet cursor = preparedStatement.executeQuery();
            List<ScreenConfigLocal> result = new ArrayList<ScreenConfigLocal>();

            while (cursor.next()) {
                ScreenConfigLocal eq = new ScreenConfigLocal();

                eq.setName(cursor.getString(name));
                eq.setValue(cursor.getString(value));

                result.add(eq);
            }

            cursor.close();
            return result;

        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    @Override
    public List<Object> getLocalById(String fieldId, String searchId) {
        return null;
    }

    @Override
    public Object getLocalOneById(String fieldId, String searchId) {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + fieldId + "=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, searchId);
            ResultSet cursor = preparedStatement.executeQuery();

            if (cursor.next()) {
                ScreenConfigLocal eq = new ScreenConfigLocal();

                eq.setName(cursor.getString(name));
                eq.setValue(cursor.getString(value));

                cursor.close();

                return eq;
            }

        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    @Override
    public void postBulkData(List<Object> eq) {
        if (!NetworkConnection.getKafkaStatus()) {
            return;
        }
        System.out.println("--- post synchronization ---");
        System.out.println("NOT DONE YET !!!");
        // for (Object obj : eq) {
        // HMLocal oqe = (HMLocal) obj;
        // RecordMetadata rm = MainApp.producer.push(IKafkaConstant.TOPIC_PRODUCE_ECU,
        // obj);
        // if (rm.hasOffset()) {
        // setSynchronized(oqe.getDate());
        // }
        // }
    }

    @Override
    public void setSynchronized(String ids) {
        String sql = "update " + TABLE_NAME + " set sync_status=1 where " + name + "=?";
        // deleteData(id);
        try {
            PreparedStatement ps = conn().prepareStatement(sql);
            ps.setString(1, ids);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
