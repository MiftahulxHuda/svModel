package com.tura.fms.screen.presenter;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.helpers.GPS;
import com.tura.fms.screen.helpers.NavigationUI;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.equipment.EquipmentLogStartRequest;
import com.tura.fms.screen.models.maintenance_model.LostStartPost;
import com.tura.fms.screen.models.maintenance_model.MainConstModel;
import com.tura.fms.screen.models.maintenance_model.MaintenanceMasterPost;
import com.tura.fms.screen.models.maintenance_model.MaintenancePost;
import com.tura.fms.screen.models.maintenance_model.MaintenanceRequestDetail;
import com.tura.fms.screen.models.maintenance_model.MaintenanceRequestPost;
import com.tura.fms.screen.models.operation_status.OperationStatusLocal;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BreakdownPresenter {

    private ObservableList<MainConstModel> recordsData = FXCollections.observableArrayList();
    private ObservableList<MainConstModel> recordsDataUnschedule = FXCollections.observableArrayList();
    public static final NetworkConnection networkConnection = new NetworkConnection();

    // private NavigationUI navigationUI = new NavigationUI();

    private static final Logger log = LoggerFactory.getLogger(BreakdownPresenter.class);
    // SessionFMS sessionFMS;

    public BreakdownPresenter() {
        // sessionFMS = new SessionFMS();
        // sessionFMS.touchFms();
    }

    public ObservableList<MainConstModel> getConstant() {

        recordsData.add(new MainConstModel("1", "Engine", "okdoki"));
        recordsData.add(new MainConstModel("2", "Lubication System", "okdoki"));
        recordsData.add(new MainConstModel("3", "Fuel System", "okdoki"));
        recordsData.add(new MainConstModel("4", "Cooling System", "okdoki"));
        recordsData.add(new MainConstModel("5", "Air Inlet System", "okdoki"));
        recordsData.add(new MainConstModel("6", "Exhaust System", "okdoki"));
        recordsData.add(new MainConstModel("7", "Electrical Engine", "okdoki"));
        recordsData.add(new MainConstModel("8", "Transmision", "okdoki"));
        recordsData.add(new MainConstModel("9", "Tonque Converter", "okdoki"));
        recordsData.add(new MainConstModel("10", "Transmision Cloud GP", "okdoki"));

        return recordsData;

    }

    public ObservableList<MainConstModel> getConstantUnshecule() {

        recordsDataUnschedule.add(new MainConstModel("1", "Transmision Cluth GP", "okdoki"));
        recordsDataUnschedule.add(new MainConstModel("2", "Differensial", "okdoki"));
        recordsDataUnschedule.add(new MainConstModel("3", "Power Train Related", "okdoki"));
        recordsDataUnschedule.add(new MainConstModel("4", "Final Drive/Hub Reduction", "okdoki"));
        recordsDataUnschedule.add(new MainConstModel("5", "Break System", "okdoki"));
        recordsDataUnschedule.add(new MainConstModel("6", "Steering System", "okdoki"));
        recordsDataUnschedule.add(new MainConstModel("7", "Hydraulic", "okdoki"));
        recordsDataUnschedule.add(new MainConstModel("8", "Lines Hydraulic", "okdoki"));

        return recordsDataUnschedule;
    }

    public void stopRestApi(String token, LostStartPost lostStartPost, StackPane stackPane,
            MaintenanceRequestDetail maintenanceRequest) {

        Map<String, String> aturheader = new HashMap<>();
        aturheader.put("Authorization", "Bearer " + token);

        if (NetworkConnection.getServiceStatus()) {

            Call<LostStartPost> call = APIClient.getInstance().postStopMaintenance(aturheader, lostStartPost);
            call.enqueue(new Callback<LostStartPost>() {

                @Override
                public void onResponse(Call<LostStartPost> call, Response<LostStartPost> response) {

                    if (response.isSuccessful()) {

                    }
                    System.out.println(response.body());

                }

                @Override
                public void onFailure(Call<LostStartPost> call, Throwable t) {

                    t.printStackTrace();
                    pesanOk(t.getMessage(), stackPane);
                }
            });

        } else {
            pesanOk(" Koneksi Sedang Bermasalah ", stackPane);
        }

    }

    public void postRestApi(MaintenancePost cd, StackPane stackPane, String label, String id, CustomRunnable callback) {

        // Map<String, String> aturheader = new HashMap<>();
        // aturheader.put("Authorization", "Bearer " + token);

        if (MainApp.isCanPost()) {

            Call<MaintenanceRequestPost> call = APIClient.getInstance().PostMaintenance(MainApp.requestHeader, cd);

            call.enqueue(new Callback<MaintenanceRequestPost>() {

                @Override
                public void onResponse(Call<MaintenanceRequestPost> call, Response<MaintenanceRequestPost> response) {

                    if (response.isSuccessful()) {

                        if (response.body().getResponse().equals("success")) {

                            MaintenanceRequestDetail maintenanceRequestDetail = response.body().getOutput();

                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    pesanAlert(id, label, stackPane, maintenanceRequestDetail, cd, callback);
                                    NavigationUI.addLogActivity("", "Maintenance request, problem " + label);
                                }
                            });

                        } else {

                            if (callback != null) {
                                callback.run();
                            }

                        }

                    } else {
                        if (callback != null) {
                            callback.run();
                        }
                    }

                }

                @Override
                public void onFailure(Call<MaintenanceRequestPost> call, Throwable t) {
                    if (callback != null) {
                        callback.run();
                    }
                    t.printStackTrace();
                    pesanOk(t.getMessage(), stackPane);
                }
            });

        } else {

            if (callback != null) {
                callback.run();
            }
            pesanOk("Koneksi sedang bermasalah", stackPane);
        }

    }

    public void postRestApiRunning(MaintenancePost cd, StackPane stackPane, String label, String id) {

        // Map<String, String> aturheader = new HashMap<>();
        // aturheader.put("Authorization", "Bearer " + token);

        if (NetworkConnection.getServiceStatus()) {
            Call<MaintenanceRequestPost> call = APIClient.getInstance().PostMaintenance(MainApp.requestHeader, cd);

            call.enqueue(new Callback<MaintenanceRequestPost>() {

                @Override
                public void onResponse(Call<MaintenanceRequestPost> call, Response<MaintenanceRequestPost> response) {

                    if (response.isSuccessful()) {

                        if (response.body().getResponse().equals("success")) {

                            MaintenanceRequestDetail maintenanceRequestDetail = response.body().getOutput();

                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {

                                }
                            });

                        } else {

                        }

                    }

                }

                @Override
                public void onFailure(Call<MaintenanceRequestPost> call, Throwable t) {
                    t.printStackTrace();
                    pesanOk(t.getMessage(), stackPane);
                }
            });

        } else {
            pesanOk("koneksi sedang bermasalah", stackPane);

        }

    }

    public void pesanAlert(String id, String label, StackPane stackPane,
            MaintenanceRequestDetail maintenanceRequestDetail, MaintenancePost cd, CustomRunnable callback) {

        JFXDialogLayout content = new JFXDialogLayout();
        Text txt = new Text("KONFIRMASI - " + label);
        txt.setStyle("-fx-font-size:20px;");
        content.setHeading(txt);
        content.setBody(new Text("Silakan untuk memilih salah satu proses"));
        JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER);

        JFXButton confirm = new JFXButton("Stop");
        confirm.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
        confirm.setPrefHeight(30.0);
        confirm.setPrefWidth(100.0);
        confirm.getStyleClass().add("button-primary");

        confirm.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                /**
                 * ketika memilih untuk berhenti, maka screen mengirim activity breakdown
                 * status, dengan activity waiting mechanic
                 */
                GPS gps = Utilities.getCurrentGPS();

                if (MainApp.isCanPost()) {

                    EquipmentLogStartRequest activity = new EquipmentLogStartRequest();
                    if (Utilities.isLoaderHauler()) {
                        activity.setIdActual(MainApp.actualInfo.getId());
                    } else {
                        activity.setIdOperationSupport(MainApp.actualSupport.getId());
                    }
                    activity.setIdEquipment(MainApp.equipmentLocal.getId());
                    activity.setIdOperator(MainApp.user.getId());
                    activity.setTime("current");
                    activity.setCdActivity(ConstantValues.BREAKDOWN_WAITING_MECHANIC);

                    Utilities.setActivity(activity, new Runnable() {

                        @Override
                        public void run() {

                            /** todo: post maintenance */
                            MaintenanceMasterPost data = new MaintenanceMasterPost();
                            data.setType(ConstantValues.MAINTENANCE_UNSCHEDULE);
                            data.setEquipment_id(MainApp.equipmentLocal.getId());
                            data.setProblem_id(maintenanceRequestDetail.getProblem_id());
                            data.setMaintenance_request_id(maintenanceRequestDetail.getId());
                            JSONObject hm = MainApp.device.getHourMeter();
                            try {
                                Double hmm = hm.getJSONObject("hour_meter").getDouble("value");
                                data.setHM(hmm.toString());
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                            data.setStatus(ConstantValues.MAINTENANCE_STATUS_NOT);
                            data.setIs_deleted(0);
                            if (Utilities.isLoaderHauler()) {
                                data.setForeman_id(MainApp.actualInfo.getId_supervisor().getId());
                                data.setLocation_id(MainApp.actualInfo.getId_location().getId());
                            } else {
                                data.setForeman_id(MainApp.actualSupport.getSupervisorId().getId());
                                data.setLocation_id(MainApp.actualSupport.getLocationId().getId());
                            }
                            Call<MaintenanceRequestPost> call = APIClient.getInstance()
                                    .postMaintenanceMaster(MainApp.requestHeader, data);
                            call.enqueue(new Callback<MaintenanceRequestPost>() {
                                @Override
                                public void onResponse(Call<MaintenanceRequestPost> call,
                                        Response<MaintenanceRequestPost> response) {
                                    Platform.runLater(() -> {
                                        dialog.close();
                                        // Utilities.showMessageDialog("Unit Breakdown", "", "Anda akan logout dari
                                        // aplikasi");
                                        if (callback != null) {
                                            callback.setData("logout");
                                            callback.run();
                                        }
                                    });
                                }

                                @Override
                                public void onFailure(Call<MaintenanceRequestPost> call, Throwable t) {
                                    t.printStackTrace();
                                }
                            });

                        }
                    });

                } else {
                    /**
                     * offline mode
                     */

                    OperationStatusLocal act = new OperationStatusLocal();
                    act.setIdActual(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId());
                    act.setIdOperationSupport(MainApp.actualSupport == null ? null : MainApp.actualSupport.getId());
                    act.setIdOperationLoader(MainApp.actualLoader == null ? null : MainApp.actualLoader.getId());
                    act.setIdEquipment(MainApp.equipmentLocal.getId());
                    act.setIdOperator(MainApp.user.getId());
                    act.setCdActivity(ConstantValues.BREAKDOWN_WAITING_MECHANIC);
                    act.setCdTum(ConstantValues.TUM_DOWN);
                    act.setCdType(MainApp.config("config.unit_type"));
                    act.setTime(Utilities.getCurrentUnix());
                    act.setLatitude(gps.getLat());
                    act.setLongitude(gps.getLon());
                    OperationStatusLocal saved = (OperationStatusLocal) MainApp.operationStatusDB.insertIgnore(act);

                    Utilities.playSound("");

                    Utilities.setLatestActivity(saved);

                    if (MainApp.latestActivity != null) {

                        if (MainApp.latestActivity.getCdActivity() != null) {
                            MainApp.dashboardInfo.setStatus(MainApp.latestActivity.getCdActivity().getName());
                        }
                        if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_WORKING)) {
                            MainApp.dashboardInfo.setStatusText("Activity");
                        }
                        if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_DELAY)) {
                            MainApp.dashboardInfo.setStatusText("Delay");
                        }
                        if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_IDLE)) {
                            MainApp.dashboardInfo.setStatusText("Idle");
                        }
                        if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_DOWN)) {
                            MainApp.dashboardInfo.setStatusText("Down");
                        }
                        if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_STANDBY)) {
                            MainApp.dashboardInfo.setStatusText("Standby");
                        }
                        if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_ASSIGNED)) {
                            MainApp.dashboardInfo.setStatusText("Assigned");
                        }
                        if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_MAINTENANCE)) {
                            MainApp.dashboardInfo.setStatusText("Maintenance");
                        }

                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                String actName = MainApp.latestActivity.getCdActivity() == null ? ""
                                        : ", " + MainApp.latestActivity.getCdActivity().getName();
                                NavigationUI.addLogActivity(MainApp.latestActivity.getCdTum().getCd(),
                                        MainApp.latestActivity.getCdTum().getName() + actName);
                            }
                        });
                    }
                }

                dialog.close();

                pesanOk(label + " Berhasil di Stop ", stackPane);
            }
        });

        JFXButton running = new JFXButton("Running");
        running.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
        running.setPrefHeight(30.0);
        running.setPrefWidth(100.0);
        running.getStyleClass().add("button-primary");

        running.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                MaintenancePost maintenancePost = new MaintenancePost();
                maintenancePost.setEquipment_id(cd.getEquipment_id());
                maintenancePost.setIs_deleted(0);
                maintenancePost.setLocation_id(cd.getLocation_id());
                maintenancePost.setProblem_id(cd.getProblem_id());

                try {
                    maintenancePost.setLatitude(cd.getLatitude());
                    maintenancePost.setLatitude(cd.getLongitude());
                } catch (Exception e) {
                    // e.printStackTrace();
                }

                maintenancePost.setOperation_actual_id(cd.getOperation_actual_id());
                maintenancePost.setOperator_id(cd.getOperator_id());
                maintenancePost.setResponse("unschedule");
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                maintenancePost.setResponse_date(timestamp.getTime() / 1000);

                postRestApiRunning(maintenancePost, stackPane, label, id);

                dialog.close();

                pesanOk(label + " Berhasil di running ", stackPane);

            }
        });

        content.setActions(confirm, running);
        // try {
        // if (!cd.getResponse().equals("uncschedule") || cd.getResponse()==null) {

        dialog.show();

        // }
        // }catch (Exception e){}

    }

    public void pesanOk(String label, StackPane stackPane) {

        JFXDialogLayout content = new JFXDialogLayout();
        Text txt = new Text("Informasi - " + label);
        txt.setStyle("-fx-font-size:20px;");
        content.setHeading(txt);
        content.setBody(new Text(label));
        JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER);

        JFXButton confirm = new JFXButton("Close");
        confirm.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
        confirm.setPrefHeight(30.0);
        confirm.setPrefWidth(100.0);
        confirm.getStyleClass().add("button-primary");

        confirm.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                dialog.close();
            }
        });

        content.setActions(confirm);

        dialog.show();

    }

}
