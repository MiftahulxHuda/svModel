package com.tura.fms.screen.kafka;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.util.Properties;

import com.tura.fms.screen.helpers.Utilities;

import static com.tura.fms.screen.constanst.IKafkaConstant.*;

public class ConsumerCreator {

    public static Consumer<String, String> createConsumer() {
        String jaasTemplate = "org.apache.kafka.common.security.scram.ScramLoginModule required username=\"%s\" password=\"%s\";";
        String jaasCfg = String.format(jaasTemplate, username_kafka, password_kafka);

        String serializer = "org.apache.kafka.common.serialization.StringSerializer";
        String deserializer = "org.apache.kafka.common.serialization.StringDeserializer";

        final Properties props = new Properties();
        props.put("bootstrap.servers", KAFKA_BROKERS);
        props.put("group.id", GROUP_ID_CONFIG);
        props.put("enable.auto.commit", true);
        props.put("auto.commit.interval.ms", AUTO_COMMIT_INTERVAL);
        props.put("auto.offset.reset", OFFSET_RESET_LATEST);
        props.put("session.timeout.ms", SESSION_TIMEOUT);
        props.put("key.deserializer", deserializer);
        props.put("value.deserializer", deserializer);
        // props.put("key.serializer", serializer);
        // props.put("value.serializer", serializer);
        props.put("max.poll.interval.ms", MAX_POLL_INTERVAL_MS);
        props.put("session.timeout.ms", SESSION_TIMEOUT_MS);
        props.put("max.poll.records", MAX_POLL_RECORDS);
        props.put("security.protocol", SECURITY_PROTOCOL);
        props.put("sasl.mechanism", mechanism_kafka);
        props.put("sasl.jaas.config", jaasCfg);

        final Consumer<String, String> consumer = new KafkaConsumer<>(props);

        if (Utilities.isHauler()) {
            consumer.subscribe(TOPIC_SUBSCRIBE_HAULER);
        } else if (Utilities.isLoader()) {
            consumer.subscribe(TOPIC_SUBSCRIBE_LOADER);
        } else {
            consumer.subscribe(TOPIC_SUBSCRIBE_SUPPORT);
        }

        return consumer;
    }

}
