package com.tura.fms.screen.models.operation_log_detail;

import java.io.IOException;
import java.net.ConnectException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.database.Operation;
import com.tura.fms.screen.database.SQLiteJDBC;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.ResponseModel;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OperationLogDetailDB extends Operation {

    public static final String TABLE_NAME = "OperationLogDetail_Record";

    private static final String id = "id";
    private static final String date = "date";
    private static final String id_actual = "id_actual";
    private static final String cd_time = "cd_time";
    private static final String id_equipment = "id_equipment";
    private static final String id_operator = "id_operator";
    private static final String cd_equipment_type = "cd_equipment_type";
    private static final String id_location = "id_location";
    private static final String id_loader = "id_loader";
    private static final String latitude = "latitude";
    private static final String longitude = "longitude";
    private static final String bucket_count = "bucket_count";
    private static final String sync_status = "sync_status";

    public OperationLogDetailDB() {
        super(OperationLogDetailDB.TABLE_NAME, ConstantValues.DATA_OPERATION_LOG_DETAIL);
    }

    public void createTableOperationLogDetail() {
        try {
            DatabaseMetaData dbm = conn().getMetaData();
            ResultSet rs = dbm.getTables(null, null, TABLE_NAME, null);
            Statement stmt = null;
            if (rs.next()) {
                System.out.println("Table operation log detail exists");
            } else {
                stmt = conn().createStatement();
                stmt.setQueryTimeout(10);
                stmt.executeUpdate(createTable());
                stmt.close();
                System.out.println("Table operation log detail created");
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String createTable() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + id + " TEXT PRIMARY KEY," + date + " NUMBER NULL,"
                + id_actual + " TEXT NULL," + cd_time + " TEXT NULL," + id_equipment + " TEXT NULL," + id_operator
                + " TEXT NULL," + cd_equipment_type + " TEXT NULL," + id_location + " TEXT NULL," + id_loader
                + " TEXT NULL," + latitude + " NUMBER NULL," + longitude + " NUMBER NULL," + bucket_count
                + " NUMBER NULL," + sync_status + " NUMBER NULL" + ")";
    }

    @Override
    public Object insertIgnore(Object eqa) {
        OperationLogDetailLocal eq = (OperationLogDetailLocal) eqa;
        PreparedStatement st = null;
        Boolean rs = null;
        try {
            String sql = "INSERT INTO " + TABLE_NAME + " " + "(" + id + "," + date + "," + id_actual + "," + cd_time
                    + "," + id_equipment + "," + id_operator + "," + cd_equipment_type + "," + id_location + ","
                    + id_loader + "," + latitude + "," + longitude + "," + bucket_count + "," + sync_status + ")"
                    + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,0) ON CONFLICT(" + id + ") DO UPDATE SET " + id + "=?," + date
                    + "=?," + id_actual + "=?," + cd_time + "=?," + id_equipment + "=?," + id_operator + "=?,"
                    + cd_equipment_type + "=?," + id_location + "=?," + id_loader + "=?," + latitude + "=?," + longitude
                    + "=?," + bucket_count + "=?";

            st = conn().prepareStatement(sql);
            final String uuid = UUID.randomUUID().toString().replace("-", "");
            // st.setString(1, eq.getId());
            st.setString(1, uuid);
            st.setString(13, uuid);

            st.setString(2, eq.getDate());
            st.setString(14, eq.getDate());

            st.setString(3, eq.getIdActual());
            st.setString(15, eq.getIdActual());

            st.setString(4, eq.getCdTime());
            st.setString(16, eq.getCdTime());

            st.setString(5, eq.getIdEquipment());
            st.setString(17, eq.getIdEquipment());

            st.setString(6, eq.getIdOperator());
            st.setString(18, eq.getIdOperator());

            st.setString(7, eq.getCdEquipmentType());
            st.setString(19, eq.getCdEquipmentType());

            st.setString(8, eq.getIdLocation());
            st.setString(20, eq.getIdLocation());

            st.setString(9, eq.getIdLoader());
            st.setString(21, eq.getIdLoader());

            st.setDouble(10, eq.getLat() == null ? 0.0 : eq.getLat());
            st.setDouble(22, eq.getLat() == null ? 0.0 : eq.getLat());

            st.setDouble(11, eq.getLon() == null ? 0.0 : eq.getLon());
            st.setDouble(23, eq.getLon() == null ? 0.0 : eq.getLon());

            st.setInt(12, eq.getBucketCount() == null ? 0 : eq.getBucketCount());
            st.setInt(24, eq.getBucketCount() == null ? 0 : eq.getBucketCount());

            rs = st.execute();
            st.close();

            return this.getLocalOneById(id, uuid);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Object getLocalOneById(String fieldId, String searchId) {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + fieldId + "=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, searchId);
            ResultSet cursor = preparedStatement.executeQuery();

            if (cursor.next()) {
                OperationLogDetailLocal eq = new OperationLogDetailLocal();
                eq.setId(cursor.getString(id));
                eq.setDate(cursor.getString(date));
                eq.setIdActual(cursor.getString(id_actual));
                eq.setCdTime(cursor.getString(cd_time));
                eq.setIdEquipment(cursor.getString(id_equipment));
                eq.setIdOperator(cursor.getString(id_operator));
                eq.setCdEquipmentType(cursor.getString(cd_equipment_type));
                eq.setIdLocation(cursor.getString(id_location));
                eq.setIdLoader(cursor.getString(id_loader));
                eq.setLat(cursor.getDouble(latitude));
                eq.setLon(cursor.getDouble(longitude));
                eq.setBucketCount(cursor.getInt(bucket_count));
                cursor.close();
                return eq;
            }

        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    @Override
    public void deleteData(String id) {
        String sql = "delete from " + TABLE_NAME + " where id=?";
        try {
            PreparedStatement ps = conn().prepareStatement(sql);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object getFromServer(Integer page, Integer size) {
        if (!NetworkConnection.getServiceStatus()) {
            return null;
        }
        // no action
        return null;
    }

    @Override
    public Integer getRemoteCount() {
        if (!NetworkConnection.getServiceStatus()) {
            return 0;
        }
        // no action
        return 0;
    }

    @Override
    public List<Object> getFlaggedData() {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE sync_status=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, "0");
            ResultSet cursor = preparedStatement.executeQuery();
            List<Object> result = new ArrayList<Object>();
            while (cursor.next()) {
                OperationLogDetailLocal eq = new OperationLogDetailLocal();
                eq.setId(cursor.getString(id));
                eq.setDate(cursor.getString(date));
                eq.setIdActual(cursor.getString(id_actual));
                eq.setCdTime(cursor.getString(cd_time));
                eq.setIdEquipment(cursor.getString(id_equipment));
                eq.setIdOperator(cursor.getString(id_operator));
                eq.setCdEquipmentType(cursor.getString(cd_equipment_type));
                eq.setIdLocation(cursor.getString(id_location));
                eq.setIdLoader(cursor.getString(id_loader));
                eq.setLat(cursor.getDouble(latitude));
                eq.setLon(cursor.getDouble(longitude));
                eq.setBucketCount(cursor.getInt(bucket_count));
                result.add(eq);
            }
            cursor.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Object getFlaggedDataOne() {
        try {
            String type = MainApp.config("config.unit_type");
            String unitId = MainApp.equipmentLocal != null ? MainApp.equipmentLocal.getId() : "";

            String sql = "";
            if (type.equals(ConstantValues.EQUIPMENT_HAULER)) {
                sql = "SELECT * FROM " + TABLE_NAME
                        + " WHERE sync_status=? AND id_equipment=? ORDER BY id_equipment DESC LIMIT 1";
            } else if (type.equals(ConstantValues.EQUIPMENT_LOADER)) {
                sql = "SELECT * FROM " + TABLE_NAME
                        + " WHERE sync_status=? AND id_loader=? ORDER BY id_loader DESC LIMIT 1";
            }

            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, "0");
            preparedStatement.setString(2, unitId);
            ResultSet cursor = preparedStatement.executeQuery();

            if (cursor.next()) {
                OperationLogDetailLocal eq = new OperationLogDetailLocal();
                eq.setId(cursor.getString(id));
                eq.setDate(cursor.getString(date));
                eq.setIdActual(cursor.getString(id_actual));
                eq.setCdTime(cursor.getString(cd_time));
                eq.setIdEquipment(cursor.getString(id_equipment));
                eq.setIdOperator(cursor.getString(id_operator));
                eq.setCdEquipmentType(cursor.getString(cd_equipment_type));
                eq.setIdLocation(cursor.getString(id_location));
                eq.setIdLoader(cursor.getString(id_loader));
                eq.setLat(cursor.getDouble(latitude));
                eq.setLon(cursor.getDouble(longitude));
                eq.setBucketCount(cursor.getInt(bucket_count));
                cursor.close();
                return eq;

            } else {

                cursor.close();
                return null;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    @Override
    public void postBulkData(List<Object> eq, Runnable cbSuccess, Runnable cbFail) {
        if (!NetworkConnection.getServiceStatus()) {
            return;
        }
        System.out.println("--- post synchronization ---");
        Call<ResponseModel> call = APIClient.getInstance().postOperationLogDetailBulk(MainApp.requestHeader, eq);
        call.enqueue(new Callback<ResponseModel>() {

            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                System.out.println(response.body());
                if (response.isSuccessful()) {
                    System.out.println("post success");
                    /**
                     * change flag (sync_status to 1)
                     */
                    for (Object obj : eq) {
                        OperationLogDetailLocal oqe = (OperationLogDetailLocal) obj;
                        setSynchronized(oqe.getId());
                    }
                    if (cbSuccess != null) {
                        cbSuccess.run();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                /**
                 * offline mode
                 */
                OperationLogDetailLocal logDetailLocal = (OperationLogDetailLocal) eq.get(0);

                if (logDetailLocal != null) {
                    logDetailLocal.setDate(Utilities.getCurrentUnix().toString());
                    MainApp.operationLogDetailDB.insertIgnore(logDetailLocal);

                }

                if (t instanceof ConnectException) {
                    Utilities.messageSocketException(t, "postBulkData OperationLogDetail");
                } else if (t instanceof TimeoutException) {
                    Utilities.messageTimeoutException(t, "postBulkData OperationLogDetail");
                } else {
                    Utilities.messageOtherException(t, "postBulkData OperationLogDetail");
                }

                if (cbFail != null) {
                    cbFail.run();
                }
                // t.printStackTrace();
            }
        });
    }

    @Override
    public void postBulkData(List<Object> eq) {
        postBulkData(eq, null, null);
    }

    @Override
    public void postData(Object eq, Runnable cb) {
        if (!NetworkConnection.getServiceStatus()) {
            return;
        }

        Call<ResponseModel> call = APIClient.getInstance().postOfflineOperationLogDetail(MainApp.requestHeader, eq);
        try {
            Response<ResponseModel> response = call.execute();
            if (response.body() != null) {
                if (response.body().getResponse() != null) {
                    if (response.body().getResponse().equals("success")) {
                        System.out.println("post success");

                        if (cb != null) {
                            cb.run();
                        }
                    }
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void setSynchronized(String id) {
        String sql = "update " + TABLE_NAME + " set sync_status=1 where id=?";
        // deleteData(id);
        try {
            PreparedStatement ps = conn().prepareStatement(sql);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
