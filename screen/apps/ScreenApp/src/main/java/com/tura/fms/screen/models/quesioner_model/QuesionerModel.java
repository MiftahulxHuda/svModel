package com.tura.fms.screen.models.quesioner_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tura.fms.screen.models.activity_model.ActivityDetail;

import java.util.List;

public class QuesionerModel {

    @SerializedName("output")
    @Expose
    private List<QuesionerDetail> output = null;

    public List<QuesionerDetail> getOutput() {
        return output;
    }

    public void setOutput(List<QuesionerDetail> output) {
        this.output = output;
    }
}
