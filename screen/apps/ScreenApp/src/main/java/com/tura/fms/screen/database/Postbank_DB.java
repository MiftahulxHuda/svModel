package com.tura.fms.screen.database;

import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.bank_model.BankDetail;
import com.tura.fms.screen.models.dashboard_model.DashboardDetail;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Postbank_DB extends Operation {
    // Table Name
    private static final String TABLE_INFO = "Postbank_Record";
    // Table Columns
    private static final String id = "id";
    private static final String cd = "cd";
    private static final String name = "name";
    private static final String name_alias = "name_alias";
    private static final String code_3 = "code_3";
    private static final String code_7 = "code_7";
    private static final String code_se = "code_se";
    private static final String file_logo = "file_logo";
    private static final String url_website = "url_website";
    private static final String enum_bank = "enum_bank";
    private static final String is_deleted = "is_deleted";
    private static final String json = "json";

    public Postbank_DB() {
        super(Postbank_DB.TABLE_INFO, ConstantValues.DATA_BANK);
    }

    public void insertData(BankDetail bankDetail) {
        PreparedStatement st = null;
        Boolean rs = null;
        String proses;
        try {
            String sql;
            if (getExisting() == 0) {

                sql = "INSERT INTO " + TABLE_INFO + " " + "(" + cd + "," + name + "," + name_alias + "," + code_3 + ","
                        + code_7 + "," + code_se + "," + file_logo + "," + url_website + "," + enum_bank + ","
                        + is_deleted + "," + json + "" + ")" + "VALUES(?,?,?,?,?,?,?,?,?,?,?);";

                proses = "proses";

            } else {

                sql = "UPDATE  " + TABLE_INFO + " SET " + "" + cd + "=?," + name + "=?," + code_3 + "=?," + code_7
                        + "=?," + code_se + "=?," + file_logo + "=?," + url_website + "=?," + enum_bank + "=?,"
                        + is_deleted + "=?," + json + "=?" + " WHERE " + cd + "=?;";

                proses = "update";

            }

            st = conn().prepareStatement(sql);

            st.setString(1, bankDetail.getCd());
            st.setString(2, bankDetail.getName());
            st.setString(3, bankDetail.getCode3());
            st.setString(4, bankDetail.getCode7());
            st.setString(5, bankDetail.getCodeSe());
            st.setString(6, bankDetail.getFileLogo());
            st.setString(7, bankDetail.getUrlWebsite());
            st.setInt(8, bankDetail.getEnumBank());
            st.setInt(9, bankDetail.getIs_deleted());
            st.setString(10, bankDetail.getJson());
            if (proses.equals("update")) {
                st.setString(11, bankDetail.getCd());
            }
            rs = st.execute();
            // conn().close();
            // st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {

            if (rs) {
                try {
                    st.close();
                    // conn().close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public int getExisting() {
        try {
            String sql = "SELECT count(*) as total FROM " + TABLE_INFO + "";

            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            ResultSet cursor = preparedStatement.executeQuery();

            int jumlah;

            if (cursor.next()) {
                jumlah = cursor.getInt("total");
            } else {
                jumlah = 0;
            }
            cursor.close();
            // conn().close();
            return jumlah;
        } catch (Exception e) {

            return 0;
        }

    }

    public BankDetail getData() {
        try {
            ResultSet cursor = null;

            String sql = "SELECT * FROM " + TABLE_INFO;
            PreparedStatement preparedStatement = conn().prepareStatement(sql);

            cursor = preparedStatement.executeQuery();

            BankDetail bankDetail = new BankDetail();

            while (cursor.next()) {

                bankDetail.setCd(cursor.getString(2));
                bankDetail.setName(cursor.getString(3));
                bankDetail.setCode3(cursor.getString(4));
                bankDetail.setCode7(cursor.getString(5));
                bankDetail.setCodeSe(cursor.getString(6));
                bankDetail.setFileLogo(cursor.getString(7));
                bankDetail.setUrlWebsite(cursor.getString(8));
                bankDetail.setEnumBank(cursor.getInt(9));
                bankDetail.setIs_deleted(cursor.getInt(10));
                bankDetail.setJson(cursor.getString(10));

            }

            // close the Database
            // conn().close();
            return bankDetail;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

}
