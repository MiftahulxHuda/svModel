package com.tura.fms.screen.presenter;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXSpinner;
import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.database.User_Info_DB;
import com.tura.fms.screen.helpers.NavigationUI;
import com.tura.fms.screen.helpers.SessionFMS;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.user_model.UserAPILocal;
import com.tura.fms.screen.models.user_model.UserData;
import com.tura.fms.screen.models.user_model.UserDetails;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.javalite.app_config.AppConfig.p;

public class UserMekanikPresenter {

    private NavigationUI navigationUI = new NavigationUI();

    String respon, message;
    User_Info_DB user_info_db;

    private static final Logger log = LoggerFactory.getLogger(UserPresenter.class);

    public static final NetworkConnection networkConnection = new NetworkConnection();

    UserAPILocal userDetails = null;


    //untuk login
    public void login_async(String username,
                            String password,
                            String sesi_level,
                            Boolean setSession,
                            JFXSpinner loading,
                            Event event,
                            StackPane stackPane){

        if (NetworkConnection.getServiceStatus()) {

            Call<UserData> call = APIClient.getInstance().ProsesLoginMekanik(username, Utilities.getMd5(password),
                    "login | logout");
            call.enqueue(new Callback<UserData>() {
                @Override
                public void onResponse(Call<UserData> call, Response<UserData> response) {
                    UserData userData2 = response.body();

                    if (response.isSuccessful()) {
                        //jika username dan password salah
                        if (userData2.getRespon().equals("error")) {

                            jika_gagal(loading,
                                    "Username atau Password Salah",
                                    stackPane
                                    );


                            //jika berhasil
                        } else {

                            set_success_login(userData2.getKeluaran(),
                                    setSession);


                            jika_berhasil(
                                     loading,
                                     event

                            );


                        }

                        //jika server error
                    } else {

                        jika_gagal(loading,
                                userData2.getMessage(),
                                stackPane
                                );

                    }

                }

                //jika server error
                @Override
                public void onFailure(Call<UserData> call, Throwable throwable) {


                    jika_gagal(loading,
                            throwable.getMessage(),
                            stackPane
                            );


                }


            });

            //jika konseksi error
        }else{

            user_info_db = new User_Info_DB();

            userDetails = user_info_db.userLoginLocal(username, Utilities.getMd5(password));
            int ceklogin = user_info_db.getExistingLogin(username, Utilities.getMd5(password));

            if (ceklogin > 0) {
                // ngambil di lokal jika koneksi mati
                message = "Koneksi bermasalah data masuk ke mode offline";
                respon = "offline";

            //    MainApp.loginCallback(true, userDetails);

                UserDetails userDetails2 = new UserDetails();
                userDetails2.setId(userDetails.getId());
                userDetails2.setName(userDetails.getName());
                userDetails2.setToken(userDetails.getToken());




            } else {

                message = "Username dan Password Error";
                respon = "error";

                jika_gagal(
                        loading,
                        message,
                        stackPane);


            }



        }
    }

    //jika berhasil
    private void jika_berhasil(
                               JFXSpinner loading,
                               Event event

                               ){


        Platform.runLater(new Runnable() {
            @Override
            public void run() {


                ((Node) (event.getSource())).getScene().getWindow().hide();
                try {
                    navigationUI.windowsNew(p("app.linkMechanicTask"), p("app.titleMechanicTask"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                loading.setVisible(false);

            }
        });

    }

    private void jika_gagal(
                            JFXSpinner loading,
                            String pesan,
                            StackPane stackPane){

        Platform.runLater(new Runnable() {
            @Override
            public void run() {

                pesanAlert(pesan,  stackPane);
                loading.setVisible(false);

            }
        });

    }

    //set session ketika berhasil
    private void set_success_login(UserDetails keluaran,
                                   Boolean setSession){
        UserDetails userDet = keluaran;

        userDetails = new UserAPILocal();
        userDetails.setId(userDet.getId());

        if (userDet.getId_crew() != null) {
            userDetails.setIdCrew(userDet.getId_crew());
        } else {
            userDetails.setIdCrew("");
        }
        if (userDet.getOrg() != null) {
            userDetails.setIdOrg(userDet.getOrg().getId());
        } else {
            userDetails.setIdOrg("");
        }
        if (userDet.getCd_role() != null) {
            userDetails.setCdRole(userDet.getCd_role().getCd());
        } else {
            userDetails.setCdRole("");
        }
        if (userDet.getCd_department() != null) {
            userDetails.setCdDepartment(userDet.getCd_department().getCd());
        } else {
            userDetails.setCdDepartment("");
        }
        userDetails.setName(userDet.getName());
        userDetails.setPwd(userDet.getPwd());
        userDetails.setStaffId(userDet.getStaff_id());
        userDetails.setFingerprintId(userDet.getFingerprint_id());
        userDetails.setSocketId(userDet.getSocket_id());
        userDetails.setFilePicture(userDet.getFile_picture());
        userDetails.setIpAddress(userDet.getIp_address());
        userDetails.setToken(userDet.getToken());
        userDetails.setEnumAvailability(userDet.getEnum_availability());
        userDetails.setIsAuthenticated(userDet.getIs_authenticated());
        userDetails.setJson(userDet.getJson());

      //  MainApp.loginCallback(true, userDetails);

        if (setSession) {
            setSessionUser(userDetails);
        }
    }


    public void setSessionUser(UserAPILocal userDetails) {

        try {
            SessionFMS sessionFMS = new SessionFMS();
            // sessionFMS.logoutFms();
            sessionFMS.proseLogin("root", "secret");
            sessionFMS.setSessionFms("sesi_level", MainApp.config("config.unit_type").trim());
            sessionFMS.setSessionFms("unit_id", MainApp.config("config.unit_id").trim());

            sessionFMS.setSessionFms("id", userDetails.getId());
            sessionFMS.setSessionFms("sesi_name_operator", userDetails.getName());
            sessionFMS.setSessionFms("token", userDetails.getToken());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pesanAlert(String pesan, StackPane stackPane){

        JFXDialogLayout content = new JFXDialogLayout();
        content.setHeading(new Text("INFORMASI"));
        content.setBody(new Text(pesan));
        JFXDialog dialog = new JFXDialog(stackPane,content,JFXDialog.DialogTransition.CENTER);

        JFXButton confirm = new JFXButton("Keluar");
        confirm.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
        confirm.setPrefHeight(30.0);
        confirm.setPrefWidth(100.0);
        confirm.getStyleClass().add("button-primary");

        confirm.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialog.close();
            }
        });

        content.setActions(confirm);
        dialog.show();

    }

}
