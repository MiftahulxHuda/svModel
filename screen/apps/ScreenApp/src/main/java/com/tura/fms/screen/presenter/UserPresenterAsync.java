package com.tura.fms.screen.presenter;

import static com.tura.fms.screen.helpers.Utilities.timeStampToDate;
import static org.javalite.app_config.AppConfig.p;

import java.net.ConnectException;
import java.util.concurrent.TimeoutException;

import com.jfoenix.controls.JFXSpinner;
import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.database.Actual_DB;
import com.tura.fms.screen.database.User_Info_DB;
import com.tura.fms.screen.helpers.GPS;
import com.tura.fms.screen.helpers.NavigationUI;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.DefaultResponse;
import com.tura.fms.screen.models.actual_model.ActualData;
import com.tura.fms.screen.models.actual_model.CdMaterial;
import com.tura.fms.screen.models.actual_support.CdActivitySupport;
import com.tura.fms.screen.models.actual_support.EquipmentSupport;
import com.tura.fms.screen.models.actual_support.LocationSupport;
import com.tura.fms.screen.models.actual_support.OperationSupport;
import com.tura.fms.screen.models.actual_support.SupportData;
import com.tura.fms.screen.models.actual_support.UserSupport;
import com.tura.fms.screen.models.maintenance_model.MechanicArrivePost;
import com.tura.fms.screen.models.user_model.UserAPILocal;
import com.tura.fms.screen.models.user_model.UserData;
import com.tura.fms.screen.models.user_model.UserDetails;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.application.Platform;
import javafx.event.Event;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserPresenterAsync {

    // private NavigationUI navigationUI = new NavigationUI();
    String respon, message;
    User_Info_DB user_info_db;
    ActualData actualData;
    Actual_DB actual_db;

    private static final Logger log = LoggerFactory.getLogger(UserPresenter.class);

    public static final NetworkConnection networkConnection = new NetworkConnection();

    UserAPILocal userDetails = null;

    // untuk login
    public void login_async(String username, String password, String sesi_level, Boolean setSession, Label erroLabel,
            JFXSpinner loading, HBox kotakError, Event event, Runnable callback, Runnable callbackWhenServiceProblem) {

        if (NetworkConnection.getServiceStatus()) {

            GPS gps = Utilities.getCurrentGPS();

            String equipId = MainApp.equipmentLocal == null ? "" : MainApp.equipmentLocal.getId();

            Call<UserData> call = APIClient.getInstance().ProsesLogin(username, Utilities.getMd5(password),
                    username + "/" + MainApp.config("config.unit_id"), ConstantValues.AUTH_STATE_LOGIN, equipId,
                    MainApp.config("config.unit_type"), gps.getLat(), gps.getLon());

            MainApp.username = username;
            MainApp.password = Utilities.getMd5(password);

            call.enqueue(new Callback<UserData>() {
                @Override
                public void onResponse(Call<UserData> call, Response<UserData> response) {
                    MainApp.offlineMode = false;

                    UserData userData2 = response.body();

                    if (response.isSuccessful()) {
                        // jika username dan password salah
                        if (userData2.getRespon().equals("error")) {
                            if (callback != null) {
                                callback.run();
                            }
                            jika_gagal(erroLabel, loading, kotakError, event, "Username atau Password Salah");

                            // jika berhasil
                        } else {

                            set_success_login(userData2.getKeluaran(), setSession);

                            user_info_db = new User_Info_DB();

                            userDetails = user_info_db.userLoginLocal(username, Utilities.getMd5(password));

                            MainApp.loginCallback(true, userData2.getKeluaran());

                            MainApp.isMechanic = false;
                            MainApp.isOperator = false;
                            MainApp.isSupervisor = false;
                            MainApp.isGroupLeader = false;

                            if (userData2.getKeluaran().getCd_role().getCd().equals(ConstantValues.ROLE_MECHANIC)) {
                                MainApp.isMechanic = true;
                            }
                            if (userData2.getKeluaran().getCd_role().getCd().equals(ConstantValues.ROLE_OPERATOR)) {
                                MainApp.isOperator = true;
                            }
                            if (userData2.getKeluaran().getCd_role().getCd().equals(ConstantValues.ROLE_SUPERVISOR)) {
                                MainApp.isSupervisor = true;
                            }
                            if (userData2.getKeluaran().getCd_role().getCd().equals(ConstantValues.ROLE_GROUP_LEADER)) {
                                MainApp.isGroupLeader = true;
                            }

                            // System.out.println(userData2.getKeluaran().getCd_role().getCd());
                            // System.out.println(MainApp.isMechanic);
                            // System.out.println(MainApp.isOperator);
                            // System.out.println(MainApp.isSupervisor);
                            // System.out.println(MainApp.isGroupLeader);

                            Utilities.checkLatestActivity2(new Runnable() {

                                @Override
                                public void run() {

                                    System.out.println("------------------- latest activity");
                                    System.out.println(MainApp.latestActivity);

                                    boolean fine = true;

                                    if (MainApp.latestActivity != null) {

                                        if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_DOWN)
                                                || MainApp.latestActivity.getCdTum().getCd()
                                                        .equals(ConstantValues.TUM_MAINTENANCE)) {
                                            fine = false;

                                            /** trap mechanic here */
                                            if (userData2.getKeluaran().getCd_role().getCd()
                                                    .equals(ConstantValues.ROLE_MECHANIC)) {
                                                jika_berhasil_mekanik(erroLabel, loading, kotakError, event,
                                                        userData2.getKeluaran(), userDetails, setSession, "online");
                                            } else {
                                                jika_gagal(erroLabel, loading, kotakError, event,
                                                        "Unit breakdown/maintenance, tidak dapat beroperasi");
                                            }

                                            if (callback != null) {
                                                callback.run();
                                            }

                                            return;
                                        }

                                    }

                                    /** trap jenis user */
                                    if (userData2.getKeluaran().getCd_role().getCd()
                                            .equals(ConstantValues.ROLE_MECHANIC)) {
                                        fine = false;

                                        jika_berhasil_mekanik(erroLabel, loading, kotakError, event,
                                                userData2.getKeluaran(), userDetails, setSession, "online");

                                        if (callback != null) {
                                            callback.run();
                                        }

                                        return;

                                    } else if (userData2.getKeluaran().getCd_role().getCd()
                                            .equals(ConstantValues.ROLE_SUPERVISOR)) {

                                        fine = false;

                                        Platform.runLater(new Runnable() {

                                            @Override
                                            public void run() {
                                                try {
                                                    NavigationUI.windowsNewScene(p("app.linkPrestart"),
                                                            p("app.titlePrestart"), event, true);
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }

                                                if (callback != null) {
                                                    callback.run();
                                                }
                                            }
                                        });

                                        return;

                                    } else if (userData2.getKeluaran().getCd_role().getCd()
                                            .equals(ConstantValues.ROLE_GROUP_LEADER)) {

                                        fine = false;

                                        Platform.runLater(new Runnable() {

                                            @Override
                                            public void run() {
                                                try {
                                                    NavigationUI.windowsNewScene(p("app.linkPrestart"),
                                                            p("app.titlePrestart"), event, true);
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }

                                                if (callback != null) {
                                                    callback.run();
                                                }
                                            }
                                        });

                                        return;
                                    }

                                    if (fine) {

                                        // untuk hauler, loader, dan support

                                        if (MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_LOADER)
                                                || MainApp.config("config.unit_type")
                                                        .equals(ConstantValues.EQUIPMENT_HAULER)) {
                                            /** actual loader hauler */
                                            saveActualAsync(erroLabel, loading, kotakError, event, setSession,
                                                    userData2.getKeluaran(), userDetails, callback);
                                        } else {
                                            /** get actual support */
                                            saveActualSupportAsync(erroLabel, loading, kotakError, event, setSession,
                                                    userData2.getKeluaran(), userDetails, callback);
                                        }

                                        // Utilities.postHMStart();
                                    }
                                }
                            });

                            if (callback != null) {
                                callback.run();
                            }
                        }

                        // jika server error
                    } else {

                        if (callback != null) {
                            callback.run();
                        }

                        if (MainApp.hostUpButServiceDown == false) {

                            String message = userData2 != null ? userData2.getMessage()
                                    : "Response server tidak valid, silakan hubungi administrator";

                            jika_gagal(erroLabel, loading, kotakError, event, message);

                            MainApp.hostUpButServiceDown = true;

                        } else {

                            /**
                             * masuk ke offline mode
                             */

                            if (callbackWhenServiceProblem != null) {
                                MainApp.hostUpButServiceDown = false;
                                callbackWhenServiceProblem.run();
                            }

                        }
                    }

                }

                /**
                 * kondisi failure disini kemungkinan besar karena service problem maka
                 * pilihannya adalah aktifkan mode offline
                 *
                 * walaupun secara jaringan dan host semuanya up, tapi service nya down
                 *
                 */
                @Override
                public void onFailure(Call<UserData> call, Throwable t) {

                    if (callbackWhenServiceProblem != null) {
                        callbackWhenServiceProblem.run();
                    }

                    if (t instanceof ConnectException) {
                        Utilities.messageSocketException(t, "Login");
                    } else if (t instanceof TimeoutException) {
                        Utilities.messageTimeoutException(t, "Login");
                    } else {
                        Utilities.messageOtherException(t, "Login");
                    }

                    if (callback != null) {
                        callback.run();
                    }

                    jika_gagal(erroLabel, loading, kotakError, event, t.getMessage());

                    MainApp.log.error("user presenter async", t);
                }

            });

            // jika konseksi error
        } else {

            /** offline mode */

            user_info_db = new User_Info_DB();

            userDetails = user_info_db.userLoginLocal(username, Utilities.getMd5(password));
            int ceklogin = user_info_db.getExistingLogin(username, Utilities.getMd5(password));

            if (ceklogin > 0) {
                // ngambil di lokal jika koneksi mati
                message = "Koneksi bermasalah data masuk ke mode offline";
                respon = "offline";

                // MainApp.loginCallback(true, userDetails);

                UserDetails userDetails2 = new UserDetails();
                userDetails2.setId(userDetails.getId());
                userDetails2.setName(userDetails.getName());
                userDetails2.setToken(userDetails.getToken());

                if (MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_LOADER)
                        || MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_HAULER)) {
                    /** actual loader hauler */
                    saveActualAsync(erroLabel, loading, kotakError, event, setSession, userDetails2, userDetails,
                            callback);

                } else {
                    /** get actual support */
                    saveActualSupportAsync(erroLabel, loading, kotakError, event, setSession, userDetails2, userDetails,
                            callback);
                }

            } else {

                message = "Username dan Password Error";
                respon = "error";
                callback.run();
                jika_gagal(erroLabel, loading, kotakError, event, message);

            }

        }
    }

    // jika berhasil
    public static void jika_berhasil_mekanik(Label erroLabel, JFXSpinner loading, HBox kotakError, Event event,
            UserDetails userDetails_param, UserAPILocal userDetailslocal_param, Boolean setSession_param,
            String respon) {

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                boolean postMechanicArrive = false;

                /** check unit status, its realdy breakdown or maintenance */
                if (MainApp.latestActivity != null) {
                    if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_DOWN)
                            || MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_MAINTENANCE)) {
                        postMechanicArrive = true;
                    }
                }

                if (postMechanicArrive) {

                    System.out.println("----------------- post mechanic arrive");
                    /** inform mechanic arrive */
                    MechanicArrivePost post = new MechanicArrivePost();
                    post.setId_equipment(MainApp.equipmentLocal.getId());
                    post.setId_mechanic(userDetails_param.getId());
                    Call<DefaultResponse> call = APIClient.getInstance().postMechanicArrive(MainApp.requestHeader,
                            post);

                    call.enqueue(new Callback<DefaultResponse>() {
                        @Override
                        public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                            // ((Node) (event.getSource())).getScene().getWindow().hide();

                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {

                                    MainApp.pauseEmit = false;

                                    try {
                                        NavigationUI.windowsNewScene(p("app.linkMechanicTask"),
                                                p("app.titleMechanicTash"), event, true);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                            // kotakError.setVisible(false);
                            // loading.setVisible(false);
                            // erroLabel.setText("");
                        }

                        @Override
                        public void onFailure(Call<DefaultResponse> call, Throwable t) {
                            t.printStackTrace();
                        }
                    });

                } else {

                    System.out.println("----------------- NOT post mechanic arrive");

                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {

                            MainApp.pauseEmit = false;

                            try {
                                NavigationUI.windowsNewScene(p("app.linkMechanicTask"), p("app.titleMechanicTash"),
                                        event);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }

                Utilities.playSound("");
            }
        });

    }

    // jika berhasil
    public static void jika_berhasil(Label erroLabel, JFXSpinner loading, HBox kotakError, Event event,
            UserDetails userDetails_param, UserAPILocal userDetailslocal_param, Boolean setSession_param,
            String respon) {

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                // ((Node) (event.getSource())).getScene().getWindow().hide();

                /** aktifkan bagian ini jika p2h hanya sekali/shift nya */
                // Utilities.checkLatestActivity(new Runnable() {

                // @Override
                // public void run() {
                // Platform.runLater(() -> {
                // try {
                // NavigationUI.windowsNewScene(p("app.linkPrestart"), p("app.titlePrestart"),
                // event);
                // } catch (Exception e) {
                // e.printStackTrace();
                // }
                // });
                // }
                // });

                MainApp.pauseEmit = false;

                try {
                    NavigationUI.windowsNewScene(p("app.linkPrestart"), p("app.titlePrestart"), event, true);

                    // NavigationUI.windowsNewScene(p("app.linkDashboard"), p("app.titleDashboard"),
                    // event, true);
                    // NavigationUI.windowsNewScene(p("app.linkDashboardLoader"),
                    // p("app.titleDashboardLoader"), event,
                    // true);
                    // NavigationUI.windowsNewScene(p("app.linkDashboardSupport"),
                    // p("app.titleDashboardSupport"), event,
                    // true);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                // kotakError.setVisible(false);
                // loading.setVisible(false);
                // erroLabel.setText("");
            }
        });

    }

    private void jika_gagal(Label erroLabel, JFXSpinner loading, HBox kotakError, Event event, String pesan) {

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                MainApp.pauseEmit = false;

                erroLabel.setText(pesan);
                loading.setVisible(false);
                kotakError.setVisible(true);

            }
        });

    }

    // set session ketika berhasil
    private void set_success_login(UserDetails keluaran, Boolean setSession) {
        UserDetails userDet = keluaran;

        userDetails = new UserAPILocal();
        userDetails.setId(userDet.getId());

        if (userDet.getId_crew() != null) {
            userDetails.setIdCrew(userDet.getId_crew());
        } else {
            userDetails.setIdCrew("");
        }
        if (userDet.getOrg() != null) {
            userDetails.setIdOrg(userDet.getOrg().getId());
        } else {
            userDetails.setIdOrg("");
        }
        if (userDet.getCd_role() != null) {
            userDetails.setCdRole(userDet.getCd_role().getCd());
        } else {
            userDetails.setCdRole("");
        }
        if (userDet.getCd_department() != null) {
            userDetails.setCdDepartment(userDet.getCd_department().getCd());
        } else {
            userDetails.setCdDepartment("");
        }
        userDetails.setName(userDet.getName());
        userDetails.setPwd(userDet.getPwd());
        userDetails.setStaffId(userDet.getStaff_id());
        userDetails.setFingerprintId(userDet.getFingerprint_id());
        userDetails.setSocketId(userDet.getSocket_id());
        userDetails.setFilePicture(userDet.getFile_picture());
        userDetails.setIpAddress(userDet.getIp_address());
        userDetails.setToken(userDet.getToken());
        userDetails.setEnumAvailability(userDet.getEnum_availability());
        userDetails.setIsAuthenticated(userDet.getIs_authenticated());
        userDetails.setJson(userDet.getJson());

        // MainApp.loginCallback(true, userDetails);

        // if (setSession) {
        // setSessionUser(userDetails);
        // }
    }

    // untuk save actual
    private void saveActualAsync(Label erroLabel, JFXSpinner loading, HBox kotakError, Event event,
            Boolean setSession_param, UserDetails userDetails_param, UserAPILocal userAPILocal, Runnable callback) {

        // jika dia mekanik
        if (userDetails_param.getCd_role().getCd().equals(ConstantValues.ROLE_MECHANIC)) {

            callback.run();
            jika_berhasil_mekanik(erroLabel, loading, kotakError, event, userDetails_param, userAPILocal,
                    setSession_param, "online");

        } else {

            Platform.runLater(new Runnable() {
                @Override
                public void run() {

                    /** test sound */
                    Utilities.playSound("");

                    erroLabel.setTextFill(Color.AZURE);
                    erroLabel.setText("Sedang Mengecek Data Actual");
                }
            });

            if (NetworkConnection.getServiceStatus()) {
                // String tanggal = Utilities.getCurrentDateOnly();

                // String tanggal = "2019-08-11";
                // String tanggal = "2019-10-27";

                String tanggal = Utilities.getCurrentUnix().toString();
                // String tanggal = "2019-08-11";

                System.out.println(tanggal);

                Call<ActualData> call = APIClient.getInstance().getActual(MainApp.requestHeader, tanggal,
                        MainApp.config("config.unit_id").trim(), MainApp.config("config.unit_type").trim(), "");

                call.enqueue(new Callback<ActualData>() {
                    @Override
                    public void onResponse(Call<ActualData> call, Response<ActualData> response) {
                        ActualData actual = response.body();

                        if (response.isSuccessful()) {

                            Utilities.getOperationLoader();

                            if (actual.getOutput() == null) {
                                erroLabel.setTextFill(Color.RED);

                                jika_gagal(erroLabel, loading, kotakError, event, actual.getMessage());

                                // jika berhasil
                            } else {

                                MainApp.actualInfo = actual.getOutput();

                                if (MainApp.actualInfo != null) {

                                    System.out.println("actual id ada");

                                    // SessionFMS sessionFMS = new SessionFMS();
                                    // sessionFMS.setSessionFms("sesi_from_location",
                                    // MainApp.actualInfo.getId_location().getName());
                                    // sessionFMS.setSessionFms("sesi_destination_location",
                                    // MainApp.actualInfo.getId_destination().getName());
                                    // sessionFMS.setSessionFms("sesi_exavator",
                                    // MainApp.actualInfo.getId_loader().getName());
                                    // sessionFMS.setSessionFms("sesi_id_plan", MainApp.actualInfo.getId_plan());
                                    // sessionFMS.setSessionFms("sesi_shift",
                                    // MainApp.actualInfo.getCd_shift().getName());
                                    // sessionFMS.setSessionFms("sesi_tanggal_operation",
                                    // timeStampToDate(MainApp.actualInfo.getDate(), "tanggal"));

                                    // save data ke local
                                    try {

                                        if (MainApp.config("config.unit_type")
                                                .equals(ConstantValues.EQUIPMENT_LOADER)) {

                                            Utilities.getHaulerListAsync(new Runnable() {

                                                @Override
                                                public void run() {
                                                    jika_berhasil(erroLabel, loading, kotakError, event,
                                                            userDetails_param, userAPILocal, setSession_param,
                                                            "online");
                                                }
                                            }, new CustomRunnable() {

                                                @Override
                                                public void run() {
                                                    String msg = this.getData() != null ? this.getData().toString()
                                                            : "";
                                                    jika_gagal(erroLabel, loading, kotakError, event, msg);
                                                }
                                            });

                                        } else {

                                            jika_berhasil(erroLabel, loading, kotakError, event, userDetails_param,
                                                    userAPILocal, setSession_param, "online");

                                        }

                                        // actual_db.insertData(MainApp.actualInfo);
                                        System.out.println("==================== get area");
                                        Utilities.getUnitArea();

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    if (MainApp.latestActivity == null) {
                                        Utilities.setActivityChangeShift(new Runnable() {

                                            @Override
                                            public void run() {
                                                Utilities.setActivityPrestart(null);
                                            }
                                        });
                                    } else {
                                        Utilities.setActivityPrestart(null);
                                    }

                                } else {
                                    jika_gagal(erroLabel, loading, kotakError, event, actual.getMessage());
                                }

                            }

                        } else {
                            erroLabel.setTextFill(Color.RED);

                            jika_gagal(erroLabel, loading, kotakError, event, "Data Actual Tidak Ditemukan");

                            MainApp.resetUser();
                        }

                        callback.run();
                    }

                    // jika server error
                    @Override
                    public void onFailure(Call<ActualData> call, Throwable throwable) {

                        erroLabel.setTextFill(Color.RED);
                        callback.run();
                        jika_gagal(erroLabel, loading, kotakError, event, throwable.getMessage());

                        MainApp.resetUser();

                    }

                });

            } else {

                callback.run();
                erroLabel.setTextFill(Color.RED);
                jika_gagal(erroLabel, loading, kotakError, event, "Data Actual Tidak Ditemukan");

                MainApp.resetUser();

            }

        }

    }

    private void saveActualSupportAsync(Label erroLabel, JFXSpinner loading, HBox kotakError, Event event,
            Boolean setSession_param, UserDetails userDetails_param, UserAPILocal userAPILocal, Runnable callback) {

        // jika dia mekanik
        if (userDetails_param.getCd_role().getCd().equals(ConstantValues.ROLE_MECHANIC)) {

            callback.run();
            jika_berhasil_mekanik(erroLabel, loading, kotakError, event, userDetails_param, userAPILocal,
                    setSession_param, "online");

        } else {

            Platform.runLater(new Runnable() {
                @Override
                public void run() {

                    /** test sound */
                    Utilities.playSound("");

                    erroLabel.setTextFill(Color.AZURE);
                    erroLabel.setText("Sedang Mengecek Data Actual");
                }
            });

            if (NetworkConnection.getServiceStatus()) {
                String tanggal = Utilities.getCurrentUnix().toString();
                System.out.println(tanggal);

                Call<SupportData> call = APIClient.getInstance().getOperationSupport(MainApp.requestHeader, tanggal,
                        MainApp.config("config.unit_id").trim(), MainApp.config("config.unit_type").trim(), "");

                call.enqueue(new Callback<SupportData>() {
                    @Override
                    public void onResponse(Call<SupportData> call, Response<SupportData> response) {

                        SupportData actual = response.body();

                        if (response.isSuccessful()) {

                            if (actual.getOutput() == null) {
                                erroLabel.setTextFill(Color.RED);

                                jika_gagal(erroLabel, loading, kotakError, event, actual.getMessage());

                                // jika berhasil
                            } else {

                                MainApp.actualSupport = new OperationSupport();

                                if (actual.getOutput() != null) {
                                    MainApp.actualSupport.setId(actual.getOutput().getId());
                                    MainApp.actualSupport.setDate(actual.getOutput().getDate());
                                    MainApp.actualSupport.setHmStart(actual.getOutput().getHm_start());
                                    MainApp.actualSupport.setHmStop(actual.getOutput().getHm_stop());
                                    MainApp.actualSupport.setCreatedAt(actual.getOutput().getCreated_at());
                                    MainApp.actualSupport.setIsDeleted(actual.getOutput().getIs_deleted());
                                    if (actual.getOutput().getEquipment_id() != null) {
                                        EquipmentSupport es = new EquipmentSupport();
                                        es.setId(actual.getOutput().getEquipment_id().getId());
                                        es.setName(actual.getOutput().getEquipment_id().getName());
                                        es.setBrand(actual.getOutput().getEquipment_id().getBrand());
                                        es.setClass_(actual.getOutput().getEquipment_id().getClass_());
                                        MainApp.actualSupport.setEquipmentId(es);
                                    }
                                    if (actual.getOutput().getLocation_id() != null) {
                                        LocationSupport ls = new LocationSupport();
                                        ls.setId(actual.getOutput().getLocation_id().getId());
                                        ls.setName(actual.getOutput().getLocation_id().getName());
                                        ls.setCdLocation(actual.getOutput().getLocation_id().getCd_location());
                                        ls.setLatitude(actual.getOutput().getLocation_id().getLatitude());
                                        ls.setLongitude(actual.getOutput().getLocation_id().getLongitude());
                                        ls.setAltitude(actual.getOutput().getLocation_id().getAltitude());
                                        MainApp.actualSupport.setLocationId(ls);
                                    }
                                    if (actual.getOutput().getDestination_id() != null) {
                                        LocationSupport ls = new LocationSupport();
                                        ls.setId(actual.getOutput().getDestination_id().getId());
                                        ls.setName(actual.getOutput().getDestination_id().getName());
                                        ls.setCdLocation(actual.getOutput().getDestination_id().getCd_location());
                                        ls.setLatitude(actual.getOutput().getDestination_id().getLatitude());
                                        ls.setLongitude(actual.getOutput().getDestination_id().getLongitude());
                                        ls.setAltitude(actual.getOutput().getDestination_id().getAltitude());
                                        MainApp.actualSupport.setDestinationId(ls);
                                    }
                                    if (actual.getOutput().getCd_activity() != null) {
                                        CdActivitySupport as = new CdActivitySupport();
                                        as.setCd(actual.getOutput().getCd_activity().getCd());
                                        as.setCdSelf(actual.getOutput().getCd_activity().getCd_self());
                                        as.setName(actual.getOutput().getCd_activity().getName());
                                        MainApp.actualSupport.setCdActivity(as);
                                    }
                                    if (actual.getOutput().getCd_shift() != null) {
                                        CdActivitySupport cs = new CdActivitySupport();
                                        cs.setCd(actual.getOutput().getCd_shift().getCd());
                                        cs.setCdSelf(actual.getOutput().getCd_shift().getCd_self());
                                        cs.setName(actual.getOutput().getCd_shift().getName());
                                        MainApp.actualSupport.setCdShift(cs);
                                    }
                                    if (actual.getOutput().getCd_material() != null) {
                                        MainApp.actualSupport.setCdMaterial(actual.getOutput().getCd_material());
                                    }
                                    if (actual.getOutput().getOperator_id() != null) {
                                        UserSupport us = new UserSupport();
                                        us.setId(actual.getOutput().getOperator_id().getId());
                                        us.setName(actual.getOutput().getOperator_id().getName());
                                        us.setEmail(actual.getOutput().getOperator_id().getEmail());
                                        us.setStaffId(actual.getOutput().getOperator_id().getStaff_id());
                                        us.setCdRole(actual.getOutput().getOperator_id().getCd_role());
                                        us.setCdDepartment(actual.getOutput().getOperator_id().getCd_department());
                                        us.setIdOrg(actual.getOutput().getOperator_id().getId_org());
                                        MainApp.actualSupport.setOperatorId(us);
                                    }
                                    if (actual.getOutput().getSupervisor_id() != null) {
                                        UserSupport us = new UserSupport();
                                        us.setId(actual.getOutput().getSupervisor_id().getId());
                                        us.setName(actual.getOutput().getSupervisor_id().getName());
                                        us.setEmail(actual.getOutput().getSupervisor_id().getEmail());
                                        us.setStaffId(actual.getOutput().getSupervisor_id().getStaff_id());
                                        us.setCdRole(actual.getOutput().getSupervisor_id().getCd_role());
                                        us.setCdDepartment(actual.getOutput().getSupervisor_id().getCd_department());
                                        us.setIdOrg(actual.getOutput().getSupervisor_id().getId_org());
                                        MainApp.actualSupport.setSupervisorId(us);
                                    }
                                    if (actual.getOutput().getGroup_leader_id() != null) {
                                        UserSupport us = new UserSupport();
                                        us.setId(actual.getOutput().getGroup_leader_id().getId());
                                        us.setName(actual.getOutput().getGroup_leader_id().getName());
                                        us.setEmail(actual.getOutput().getGroup_leader_id().getEmail());
                                        us.setStaffId(actual.getOutput().getGroup_leader_id().getStaff_id());
                                        us.setCdRole(actual.getOutput().getGroup_leader_id().getCd_role());
                                        us.setCdDepartment(actual.getOutput().getGroup_leader_id().getCd_department());
                                        us.setIdOrg(actual.getOutput().getGroup_leader_id().getId_org());
                                        MainApp.actualSupport.setGroupLeaderId(us);
                                    }
                                }

                                Utilities.mapObject(MainApp.actualSupport);

                                if (MainApp.actualSupport != null) {

                                    System.out.println("actual id ada");

                                    // SessionFMS sessionFMS = new SessionFMS();
                                    // if (MainApp.actualSupport.getLocationId() != null) {
                                    // sessionFMS.setSessionFms("sesi_from_location",
                                    // MainApp.actualSupport.getLocationId().getName());
                                    // }
                                    // if (MainApp.actualSupport.getDestinationId() != null) {
                                    // sessionFMS.setSessionFms("sesi_destination_location",
                                    // MainApp.actualSupport.getDestinationId().getName());
                                    // }
                                    // if (MainApp.actualSupport.getEquipmentId() != null) {
                                    // sessionFMS.setSessionFms("sesi_exavator",
                                    // MainApp.actualSupport.getEquipmentId().getName());
                                    // }
                                    // sessionFMS.setSessionFms("sesi_id_plan", MainApp.actualSupport.getId());
                                    // if (MainApp.actualSupport.getCdShift() != null) {
                                    // sessionFMS.setSessionFms("sesi_shift",
                                    // MainApp.actualSupport.getCdShift().getName());
                                    // }
                                    // sessionFMS.setSessionFms("sesi_tanggal_operation", timeStampToDate(
                                    // Integer.toUnsignedLong(MainApp.actualSupport.getDate()), "tanggal"));

                                    // save data ke local
                                    // try {
                                    // actual_db.insertData(MainApp.actualSupport);
                                    // } catch (Exception e) {
                                    // }

                                    jika_berhasil(erroLabel, loading, kotakError, event, userDetails_param,
                                            userAPILocal, setSession_param, "online");

                                } else {
                                    jika_gagal(erroLabel, loading, kotakError, event, actual.getMessage());
                                }

                                if (MainApp.latestActivity == null) {
                                    Utilities.setActivityChangeShift(new Runnable() {

                                        @Override
                                        public void run() {
                                            Utilities.setActivityPrestart(null);
                                        }
                                    });
                                } else {
                                    Utilities.setActivityPrestart(null);
                                }

                            }

                        } else {
                            erroLabel.setTextFill(Color.RED);

                            jika_gagal(erroLabel, loading, kotakError, event, "Data Actual Tidak Ditemukan");

                            MainApp.resetUser();
                        }

                        callback.run();
                    }

                    // jika server error
                    @Override
                    public void onFailure(Call<SupportData> call, Throwable throwable) {
                        erroLabel.setTextFill(Color.RED);
                        callback.run();
                        jika_gagal(erroLabel, loading, kotakError, event, throwable.getMessage());

                        MainApp.resetUser();
                    }

                });

            } else {

                erroLabel.setTextFill(Color.RED);
                callback.run();
                jika_gagal(erroLabel, loading, kotakError, event, "Data Actual Tidak Ditemukan");

                MainApp.resetUser();
            }

        }

    }

}
