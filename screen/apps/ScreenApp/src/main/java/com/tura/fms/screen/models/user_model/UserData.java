package com.tura.fms.screen.models.user_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserData {

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("response")
    @Expose
    private String response;

    @SerializedName("output")
    @Expose
    private UserDetails output = null;

    private UserAPILocal result = null;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRespon() {
        return response;
    }

    public void setRespon(String response) {
        this.response = response;
    }

    public UserDetails getKeluaran() {
        return output;
    }

    public void setKeluaran(UserDetails output) {
        this.output = output;
    }

    public UserAPILocal getResult() {
        return result;
    }

    public void setResult(UserAPILocal result) {
        this.result = result;
    }


}
