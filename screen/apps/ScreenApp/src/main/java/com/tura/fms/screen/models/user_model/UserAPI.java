package com.tura.fms.screen.models.user_model;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.tura.fms.screen.models.constant.Constant;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "__id_crew__", "__has_id_crew__", "__id_org__", "__has_id_org__", "__cd_role__",
        "__has_cd_role__", "__cd_department__", "__has_cd_department__", "name", "email", "pwd", "staff_id",
        "fingerprint_id", "socket_id", "file_picture", "ip_address", "token", "enum_availability", "is_authenticated",
        "json", "skill", "equipments" })
public class UserAPI {

    @JsonProperty("id")
    private String id;
    @JsonProperty("__id_crew__")
    private Object idCrew;
    @JsonProperty("__has_id_crew__")
    private Boolean hasIdCrew;
    @JsonProperty("__id_org__")
    private UserOrg idOrg;
    @JsonProperty("__has_id_org__")
    private Boolean hasIdOrg;
    @JsonProperty("__cd_role__")
    private Constant cdRole;
    @JsonProperty("__has_cd_role__")
    private Boolean hasCdRole;
    @JsonProperty("__cd_department__")
    private Constant cdDepartment;
    @JsonProperty("__has_cd_department__")
    private Boolean hasCdDepartment;
    @JsonProperty("name")
    private String name;
    @JsonProperty("email")
    private String email;
    @JsonProperty("pwd")
    private String pwd;
    @JsonProperty("staff_id")
    private String staffId;
    @JsonProperty("fingerprint_id")
    private String fingerprintId;
    @JsonProperty("socket_id")
    private String socketId;
    @JsonProperty("file_picture")
    private String filePicture;
    @JsonProperty("ip_address")
    private String ipAddress;
    @JsonProperty(value = "token", required = false)
    private String token;
    @JsonProperty("enum_availability")
    private Integer enumAvailability;
    @JsonProperty("is_authenticated")
    private Integer isAuthenticated;
    @JsonProperty("json")
    private Object json;
    @JsonProperty("skill")
    private Object skill;
    @JsonProperty("equipments")
    private List<Object> equipments = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("__id_crew__")
    public Object getIdCrew() {
        return idCrew;
    }

    @JsonProperty("__id_crew__")
    public void setIdCrew(Object idCrew) {
        this.idCrew = idCrew;
    }

    @JsonProperty("__has_id_crew__")
    public Boolean getHasIdCrew() {
        return hasIdCrew;
    }

    @JsonProperty("__has_id_crew__")
    public void setHasIdCrew(Boolean hasIdCrew) {
        this.hasIdCrew = hasIdCrew;
    }

    @JsonProperty("__id_org__")
    public UserOrg getIdOrg() {
        return idOrg;
    }

    @JsonProperty("__id_org__")
    public void setIdOrg(UserOrg idOrg) {
        this.idOrg = idOrg;
    }

    @JsonProperty("__has_id_org__")
    public Boolean getHasIdOrg() {
        return hasIdOrg;
    }

    @JsonProperty("__has_id_org__")
    public void setHasIdOrg(Boolean hasIdOrg) {
        this.hasIdOrg = hasIdOrg;
    }

    @JsonProperty("__cd_role__")
    public Constant getCdRole() {
        return cdRole;
    }

    @JsonProperty("__cd_role__")
    public void setCdRole(Constant cdRole) {
        this.cdRole = cdRole;
    }

    @JsonProperty("__has_cd_role__")
    public Boolean getHasCdRole() {
        return hasCdRole;
    }

    @JsonProperty("__has_cd_role__")
    public void setHasCdRole(Boolean hasCdRole) {
        this.hasCdRole = hasCdRole;
    }

    @JsonProperty("__cd_department__")
    public Constant getCdDepartment() {
        return cdDepartment;
    }

    @JsonProperty("__cd_department__")
    public void setCdDepartment(Constant cdDepartment) {
        this.cdDepartment = cdDepartment;
    }

    @JsonProperty("__has_cd_department__")
    public Boolean getHasCdDepartment() {
        return hasCdDepartment;
    }

    @JsonProperty("__has_cd_department__")
    public void setHasCdDepartment(Boolean hasCdDepartment) {
        this.hasCdDepartment = hasCdDepartment;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("pwd")
    public String getPwd() {
        return pwd;
    }

    @JsonProperty("pwd")
    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    @JsonProperty("staff_id")
    public String getStaffId() {
        return staffId;
    }

    @JsonProperty("staff_id")
    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    @JsonProperty("fingerprint_id")
    public String getFingerprintId() {
        return fingerprintId;
    }

    @JsonProperty("fingerprint_id")
    public void setFingerprintId(String fingerprintId) {
        this.fingerprintId = fingerprintId;
    }

    @JsonProperty("socket_id")
    public String getSocketId() {
        return socketId;
    }

    @JsonProperty("socket_id")
    public void setSocketId(String socketId) {
        this.socketId = socketId;
    }

    @JsonProperty("file_picture")
    public String getFilePicture() {
        return filePicture;
    }

    @JsonProperty("file_picture")
    public void setFilePicture(String filePicture) {
        this.filePicture = filePicture;
    }

    @JsonProperty("ip_address")
    public String getIpAddress() {
        return ipAddress;
    }

    @JsonProperty("ip_address")
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    @JsonProperty("token")
    public String getToken() {
        return token;
    }

    @JsonProperty("token")
    public void setToken(String token) {
        this.token = token;
    }

    @JsonProperty("enum_availability")
    public Integer getEnumAvailability() {
        return enumAvailability;
    }

    @JsonProperty("enum_availability")
    public void setEnumAvailability(Integer enumAvailability) {
        this.enumAvailability = enumAvailability;
    }

    @JsonProperty("is_authenticated")
    public Integer getIsAuthenticated() {
        return isAuthenticated;
    }

    @JsonProperty("is_authenticated")
    public void setIsAuthenticated(Integer isAuthenticated) {
        this.isAuthenticated = isAuthenticated;
    }

    @JsonProperty("json")
    public Object getJson() {
        return json;
    }

    @JsonProperty("json")
    public void setJson(Object json) {
        this.json = json;
    }

    @JsonProperty("skill")
    public Object getSkill() {
        return skill;
    }

    @JsonProperty("skill")
    public void setSkill(Object skill) {
        this.skill = skill;
    }

    @JsonProperty("equipments")
    public List<Object> getEquipments() {
        return equipments;
    }

    @JsonProperty("equipments")
    public void setEquipments(List<Object> equipments) {
        this.equipments = equipments;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
