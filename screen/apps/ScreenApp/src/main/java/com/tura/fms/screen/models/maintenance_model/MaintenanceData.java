package com.tura.fms.screen.models.maintenance_model;


import java.util.List;

public class MaintenanceData {

    private String  id;
    private String  name;
    private List<MaintenanceDetail> output = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MaintenanceDetail> getOutput() {
        return output;
    }

    public void setOutput(List<MaintenanceDetail> output) {
        this.output = output;
    }
}
