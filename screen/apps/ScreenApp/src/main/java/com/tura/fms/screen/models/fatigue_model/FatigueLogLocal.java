package com.tura.fms.screen.models.fatigue_model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "id_equipment", "id_user", "level", "id_location", "date_submitted", "id_actual",
        "id_actual_support", "id_actual_loader" })
public class FatigueLogLocal {
    @JsonProperty("id")
    private String id;
    @JsonProperty("id_equipment")
    private String id_equipment;
    @JsonProperty("id_user")
    private String id_user;
    @JsonProperty("level")
    private Integer level;
    @JsonProperty("id_location")
    private String id_location;
    @JsonProperty("date_submitted")
    private String date_submitted;
    @JsonProperty("id_actual")
    private String id_actual;
    @JsonProperty("id_actual_support")
    private String id_actual_support;
    @JsonProperty("id_actual_loader")
    private String id_actual_loader;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdEquipment() {
        return id_equipment;
    }

    public void setIdEquipment(String id_equipment) {
        this.id_equipment = id_equipment;
    }

    public String getIdUser() {
        return id_user;
    }

    public void setIdUser(String id_user) {
        this.id_user = id_user;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getIdLocation() {
        return id_location;
    }

    public void setIdLocation(String id_location) {
        this.id_location = id_location;
    }

    public String getDate() {
        return date_submitted;
    }

    public void setDate(String date) {
        this.date_submitted = date;
    }

    public String getIdActual() {
        return id_actual;
    }

    public void setIdActual(String id_actual) {
        this.id_actual = id_actual;
    }

    public String getIdActualSupport() {
        return id_actual_support;
    }

    public void setIdActualSupport(String id_actual_support) {
        this.id_actual_support = id_actual_support;
    }

    public String getIdActualLoader() {
        return id_actual_loader;
    }

    public void setIdActualLoader(String id_actual_loader) {
        this.id_actual_loader = id_actual_loader;
    }

}
