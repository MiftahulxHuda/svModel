package com.tura.fms.screen.models.user_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class UserLocal {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName ("id_crew")
    @Expose
    private String id_crew;

    @SerializedName ("has_id_crew")
    @Expose
    private Boolean has_id_crew;

    //user org
    @SerializedName ("id_org")
    @Expose
    private String id_org;

    @SerializedName ("name_org")
    @Expose
    private String name_org;

    @SerializedName ("abbr_org")
    @Expose
    private String abbr_org;

    @SerializedName ("has_id_org")
    @Expose
    private Boolean has_id_org;

    //cd org
    @SerializedName ("cd_role")
    @Expose
    private String cd_role;

    @SerializedName ("cd_self_role")
    @Expose
    private String cd_self_role;

    @SerializedName ("name_role")
    @Expose
    private String name_role;

    //json role belum
    @SerializedName ("cd_role_json")
    @Expose
    private String cd_role_json;

    @SerializedName ("seq_role_json")
    @Expose
    private String seq_role_json;

    @SerializedName ("name_role_json")
    @Expose
    private String name_role_json;

    @SerializedName ("slug_role_json")
    @Expose
    private String slug_role_json;

    @SerializedName ("tag_1_role_json")
    @Expose
    private String tag_1_role_json;

    @SerializedName ("tag_2_role_json")
    @Expose
    private String tag_2_role_json;

    @SerializedName ("tag_3_role_json")
    @Expose
    private String tag_3_role_json;

    @SerializedName ("cd_self_role_json")
    @Expose
    private String cd_self_role_json;

    @SerializedName ("is_fixed_role_json")
    @Expose
    private int is_fixed_role_json;

    @SerializedName ("is_deleted_role_json")
    @Expose
    private int is_deleted_role_json;

    @SerializedName ("has_cd_role")
    @Expose
    private Boolean has_cd_role;

    //cd departement
    @SerializedName ("cd_departement")
    @Expose
    private String cd_departement;

    @SerializedName ("cd_self_departement")
    @Expose
    private String cd_self_departement;

    @SerializedName ("cd_name_departement")
    @Expose
    private String cd_name_departement;
    //json departement blum
    @SerializedName ("cd_departement_json")
    @Expose
    private String cd_departement_json;

    @SerializedName ("seq_departement_json")
    @Expose
    private String seq_departement_json;

    @SerializedName ("name_departement_json")
    @Expose
    private String name_departement_json;

    @SerializedName ("slug_departement_json")
    @Expose
    private String slug_departement_json;

    @SerializedName ("tag_1_departement_json")
    @Expose
    private String tag_1_departement_json;

    @SerializedName ("tag_2_departement_json")
    @Expose
    private String tag_2_departement_json;

    @SerializedName ("tag_3_departement_json")
    @Expose
    private String tag_3_departement_json;

    @SerializedName ("cd_self_departement_json")
    @Expose
    private String cd_self_departement_json;

    @SerializedName ("is_fixed_departement_json")
    @Expose
    private int is_fixed_departement_json;

    @SerializedName ("is_deleted_departement_json")
    @Expose
    private int is_deleted_departement_json;

    @SerializedName ("has_cd_department")
    @Expose
    private Boolean has_cd_department;

    @SerializedName ("name")
    @Expose
    private String name;

    @SerializedName ("email")
    @Expose
    private String email;

    @SerializedName ("pwd")
    @Expose
    private String pwd;

    @SerializedName ("staff_id")
    @Expose
    private String staff_id;

    @SerializedName ("fingerprint_id")
    @Expose
    private String fingerprint_id;

    @SerializedName ("socket_id")
    @Expose
    private String socket_id;

    @SerializedName ("file_picture")
    @Expose
    private String file_picture;

    @SerializedName ("ip_address")
    @Expose
    private String ip_address;

    @SerializedName ("token")
    @Expose
    private String token;

    @SerializedName ("enum_availability")
    @Expose
    private Integer enum_availability;

    @SerializedName ("is_authenticated")
    @Expose
    private Integer is_authenticated;

    @SerializedName ("json")
    @Expose
    private String json;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_crew() {
        return id_crew;
    }

    public void setId_crew(String id_crew) {
        this.id_crew = id_crew;
    }

    public Boolean getHas_id_crew() {
        return has_id_crew;
    }

    public void setHas_id_crew(Boolean has_id_crew) {
        this.has_id_crew = has_id_crew;
    }

    public String getId_org() {
        return id_org;
    }

    public void setId_org(String id_org) {
        this.id_org = id_org;
    }

    public String getName_org() {
        return name_org;
    }

    public void setName_org(String name_org) {
        this.name_org = name_org;
    }

    public String getAbbr_org() {
        return abbr_org;
    }

    public void setAbbr_org(String abbr_org) {
        this.abbr_org = abbr_org;
    }

    public Boolean getHas_id_org() {
        return has_id_org;
    }

    public void setHas_id_org(Boolean has_id_org) {
        this.has_id_org = has_id_org;
    }

    public String getCd_role() {
        return cd_role;
    }

    public void setCd_role(String cd_role) {
        this.cd_role = cd_role;
    }

    public String getCd_self_role() {
        return cd_self_role;
    }

    public void setCd_self_role(String cd_self_role) {
        this.cd_self_role = cd_self_role;
    }

    public String getName_role() {
        return name_role;
    }

    public void setName_role(String name_role) {
        this.name_role = name_role;
    }

    public String getCd_role_json() {
        return cd_role_json;
    }

    public void setCd_role_json(String cd_role_json) {
        this.cd_role_json = cd_role_json;
    }

    public String getSeq_role_json() {
        return seq_role_json;
    }

    public void setSeq_role_json(String seq_role_json) {
        this.seq_role_json = seq_role_json;
    }

    public String getName_role_json() {
        return name_role_json;
    }

    public void setName_role_json(String name_role_json) {
        this.name_role_json = name_role_json;
    }

    public String getSlug_role_json() {
        return slug_role_json;
    }

    public void setSlug_role_json(String slug_role_json) {
        this.slug_role_json = slug_role_json;
    }

    public String getTag_1_role_json() {
        return tag_1_role_json;
    }

    public void setTag_1_role_json(String tag_1_role_json) {
        this.tag_1_role_json = tag_1_role_json;
    }

    public String getTag_2_role_json() {
        return tag_2_role_json;
    }

    public void setTag_2_role_json(String tag_2_role_json) {
        this.tag_2_role_json = tag_2_role_json;
    }

    public String getTag_3_role_json() {
        return tag_3_role_json;
    }

    public void setTag_3_role_json(String tag_3_role_json) {
        this.tag_3_role_json = tag_3_role_json;
    }

    public String getCd_self_role_json() {
        return cd_self_role_json;
    }

    public void setCd_self_role_json(String cd_self_role_json) {
        this.cd_self_role_json = cd_self_role_json;
    }

    public int getIs_fixed_role_json() {
        return is_fixed_role_json;
    }

    public void setIs_fixed_role_json(int is_fixed_role_json) {
        this.is_fixed_role_json = is_fixed_role_json;
    }

    public int getIs_deleted_role_json() {
        return is_deleted_role_json;
    }

    public void setIs_deleted_role_json(int is_deleted_role_json) {
        this.is_deleted_role_json = is_deleted_role_json;
    }

    public Boolean getHas_cd_role() {
        return has_cd_role;
    }

    public void setHas_cd_role(Boolean has_cd_role) {
        this.has_cd_role = has_cd_role;
    }

    public String getCd_departement() {
        return cd_departement;
    }

    public void setCd_departement(String cd_departement) {
        this.cd_departement = cd_departement;
    }

    public String getCd_self_departement() {
        return cd_self_departement;
    }

    public void setCd_self_departement(String cd_self_departement) {
        this.cd_self_departement = cd_self_departement;
    }

    public String getCd_name_departement() {
        return cd_name_departement;
    }

    public void setCd_name_departement(String cd_name_departement) {
        this.cd_name_departement = cd_name_departement;
    }

    public String getCd_departement_json() {
        return cd_departement_json;
    }

    public void setCd_departement_json(String cd_departement_json) {
        this.cd_departement_json = cd_departement_json;
    }

    public String getSeq_departement_json() {
        return seq_departement_json;
    }

    public void setSeq_departement_json(String seq_departement_json) {
        this.seq_departement_json = seq_departement_json;
    }

    public String getName_departement_json() {
        return name_departement_json;
    }

    public void setName_departement_json(String name_departement_json) {
        this.name_departement_json = name_departement_json;
    }

    public String getSlug_departement_json() {
        return slug_departement_json;
    }

    public void setSlug_departement_json(String slug_departement_json) {
        this.slug_departement_json = slug_departement_json;
    }

    public String getTag_1_departement_json() {
        return tag_1_departement_json;
    }

    public void setTag_1_departement_json(String tag_1_departement_json) {
        this.tag_1_departement_json = tag_1_departement_json;
    }

    public String getTag_2_departement_json() {
        return tag_2_departement_json;
    }

    public void setTag_2_departement_json(String tag_2_departement_json) {
        this.tag_2_departement_json = tag_2_departement_json;
    }

    public String getTag_3_departement_json() {
        return tag_3_departement_json;
    }

    public void setTag_3_departement_json(String tag_3_departement_json) {
        this.tag_3_departement_json = tag_3_departement_json;
    }

    public String getCd_self_departement_json() {
        return cd_self_departement_json;
    }

    public void setCd_self_departement_json(String cd_self_departement_json) {
        this.cd_self_departement_json = cd_self_departement_json;
    }

    public int getIs_fixed_departement_json() {
        return is_fixed_departement_json;
    }

    public void setIs_fixed_departement_json(int is_fixed_departement_json) {
        this.is_fixed_departement_json = is_fixed_departement_json;
    }

    public int getIs_deleted_departement_json() {
        return is_deleted_departement_json;
    }

    public void setIs_deleted_departement_json(int is_deleted_departement_json) {
        this.is_deleted_departement_json = is_deleted_departement_json;
    }

    public Boolean getHas_cd_department() {
        return has_cd_department;
    }

    public void setHas_cd_department(Boolean has_cd_department) {
        this.has_cd_department = has_cd_department;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getStaff_id() {
        return staff_id;
    }

    public void setStaff_id(String staff_id) {
        this.staff_id = staff_id;
    }

    public String getFingerprint_id() {
        return fingerprint_id;
    }

    public void setFingerprint_id(String fingerprint_id) {
        this.fingerprint_id = fingerprint_id;
    }

    public String getSocket_id() {
        return socket_id;
    }

    public void setSocket_id(String socket_id) {
        this.socket_id = socket_id;
    }

    public String getFile_picture() {
        return file_picture;
    }

    public void setFile_picture(String file_picture) {
        this.file_picture = file_picture;
    }

    public String getIp_address() {
        return ip_address;
    }

    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getEnum_availability() {
        return enum_availability;
    }

    public void setEnum_availability(Integer enum_availability) {
        this.enum_availability = enum_availability;
    }

    public int getIs_authenticated() {
        return is_authenticated;
    }

    public void setIs_authenticated(Integer is_authenticated) {
        this.is_authenticated = is_authenticated;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }
}

