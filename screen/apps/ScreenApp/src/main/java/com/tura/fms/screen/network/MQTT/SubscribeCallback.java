package com.tura.fms.screen.network.MQTT;

import org.eclipse.paho.client.mqttv3.*;

public class SubscribeCallback implements MqttCallback {

    @Override
    public void connectionLost(Throwable cause) {
        //This is called when the connection is lost. We could reconnect here.
    }

    @Override
    public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
        System.out.println("Message arrived. Topic: " +s + "  Message: " + mqttMessage.toString());

        if ("homeautomation/LWT".equals(s)) {
            System.err.println("Publisher gone!");
        }
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }


}
