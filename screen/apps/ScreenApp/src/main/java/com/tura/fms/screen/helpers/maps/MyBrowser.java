package com.tura.fms.screen.helpers.maps;
import java.net.URL;

import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

public class MyBrowser extends  Region{
    HBox toolbar;

    WebView webView = new WebView();
    WebEngine webEngine = webView.getEngine();

    public MyBrowser(){

        final URL urlGoogleMaps = getClass().getResource("/map/map.html");
        webEngine.load(urlGoogleMaps.toExternalForm());
        /*String content =
                "Hello World!";

        webEngine.getLoadWorker().stateProperty().addListener(
                new ChangeListener() {
                    @Override
                    public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                        System.out.println("oldValue: " + oldValue);
                        System.out.println("newValue: " + newValue);

                        if (newValue != Worker.State.SUCCEEDED) {
                            return;
                        }
                        System.out.println("Succeeded!");
                        String hello = (String) webEngine.executeScript("alert('ok')");
                        System.out.println("hello: " + hello);
                    }
                }
        );*/


       // webEngine.loadContent(content, "text/html");

        getChildren().add(webView);

    }
}
