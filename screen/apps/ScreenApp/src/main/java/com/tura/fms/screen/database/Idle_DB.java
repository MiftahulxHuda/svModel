package com.tura.fms.screen.database;

import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.models.activity_model.ActivityDetail;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Idle_DB extends Operation {
    // Table Name
    private static final String TABLE_INFO = "Activity_Record";

    // Table Columns
    private static final String cd = "cd";
    private static final String name = "name";
    private static final String cd_self = "cd_self";

    public Idle_DB() {
        super(Idle_DB.TABLE_INFO, ConstantValues.DATA_OPERATION_STATUS);
    }

    public static String createTable() {

        return "CREATE TABLE " + TABLE_INFO + "(" + cd + " TEXT," + name + " TEXT," + cd_self + " TEXT" +

                ")";

    }

    public void createTableActivity() {
        try {
            DatabaseMetaData dbm = conn().getMetaData();
            ResultSet rs = dbm.getTables(null, null, TABLE_INFO, null);
            Statement stmt = null;

            if (rs.next()) {
                System.out.println("Table exists Activity");
                // stmt.close();
                // conn().close();
            } else {
                stmt = conn().createStatement();
                stmt.executeUpdate(createTable());
                stmt.close();
                // conn().close();
                System.out.println("Table created Activity");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void insertData(ActivityDetail activityDetail) {

        PreparedStatement st = null;
        Boolean rs = null;
        String proses;

        System.out.println("coba " + activityDetail.getCd());

        try {
            String sql;

            if (getExisting(activityDetail.getCd()) == 0) {

                sql = "INSERT INTO " + TABLE_INFO + " " + "(" + cd + "," + name + "," + cd_self + "" + ")"
                        + "VALUES(?,?,?);";

                proses = "proses";

            } else {

                sql = "UPDATE  " + TABLE_INFO + " SET " + "" + cd + "=?," + name + "=?," + cd_self + "=?" + " WHERE "
                        + cd + "=?;";

                proses = "update";

            }

            st = conn().prepareStatement(sql);

            st.setString(1, activityDetail.getCd());
            st.setString(2, activityDetail.getName());
            st.setString(3, activityDetail.getCd_self());

            if (proses.equals("update")) {
                st.setString(4, activityDetail.getCd());
            }
            rs = st.execute();

            // conn().close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {

            if (rs) {
                try {
                    st.close();
                    // conn().close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public int getExisting(String cd_param) {
        try {
            String sql = "SELECT count(*) as total FROM " + TABLE_INFO + " where " + cd + "=?";

            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, cd_param);

            ResultSet cursor = preparedStatement.executeQuery();

            int jumlah;

            if (cursor.next()) {
                jumlah = cursor.getInt("total");
            } else {
                jumlah = 0;
            }
            cursor.close();
            // conn().close();
            return jumlah;
        } catch (Exception e) {

            return 0;
        }

    }

    public int getExisting2() {
        try {
            String sql = "SELECT count(*) as total FROM " + TABLE_INFO + "";

            PreparedStatement preparedStatement = conn().prepareStatement(sql);

            ResultSet cursor = preparedStatement.executeQuery();

            int jumlah;

            if (cursor.next()) {
                jumlah = cursor.getInt("total");
            } else {
                jumlah = 0;
            }
            cursor.close();
            // conn().close();
            return jumlah;
        } catch (Exception e) {

            return 0;
        }

    }

    public List<ActivityDetail> getData() {
        try {
            ResultSet cursor = null;

            String sql = "SELECT * FROM " + TABLE_INFO;
            PreparedStatement preparedStatement = conn().prepareStatement(sql);

            cursor = preparedStatement.executeQuery();

            List<ActivityDetail> activityDetail = new ArrayList();

            while (cursor.next()) {

                ActivityDetail activityDetail1 = new ActivityDetail();

                System.out.println(cursor.getString(1));

                activityDetail1.setCd(cursor.getString(1));
                activityDetail1.setName(cursor.getString(2));
                activityDetail1.setCd_self(cursor.getString(3));

                activityDetail.add(activityDetail1);

            }

            // close the Database
            // conn().close();

            return activityDetail;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

}
