package com.tura.fms.screen.models.prestart_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonPrestart {


    @SerializedName("cd")
    @Expose
    String cd;

    @SerializedName("seq")
    @Expose
    String seq;

    @SerializedName("name")
    @Expose
    String name;

    @SerializedName("slug")
    @Expose
    String slug;

    @SerializedName("tag_1")
    @Expose
    String tag_1;

    @SerializedName("tag_2")
    @Expose
    String tag_2;

    @SerializedName("tag_3")
    @Expose
    String tag_3;

    @SerializedName("cd_self")
    @Expose
    String cd_self;

    @SerializedName("is_fixed")
    @Expose
    int is_fixed;

    @SerializedName("is_deleted")
    @Expose
    int is_deleted;

    public String getCd() {
        return cd;
    }

    public void setCd(String cd) {
        this.cd = cd;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getTag_1() {
        return tag_1;
    }

    public void setTag_1(String tag_1) {
        this.tag_1 = tag_1;
    }

    public String getTag_2() {
        return tag_2;
    }

    public void setTag_2(String tag_2) {
        this.tag_2 = tag_2;
    }

    public String getTag_3() {
        return tag_3;
    }

    public void setTag_3(String tag_3) {
        this.tag_3 = tag_3;
    }

    public String getCd_self() {
        return cd_self;
    }

    public void setCd_self(String cd_self) {
        this.cd_self = cd_self;
    }

    public int getIs_fixed() {
        return is_fixed;
    }

    public void setIs_fixed(int is_fixed) {
        this.is_fixed = is_fixed;
    }

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }



}
