package com.tura.fms.screen.models.config;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class ScreenConfigResponse {

    @SerializedName("response")
    @Expose
    String response;

    @SerializedName("output")
    @Expose
    List<ScreenConfig> output;

    public String getResponse() {
        return response;
    }

    public List<ScreenConfig> getOutput() {
        return output;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public void setOutput(List<ScreenConfig> output) {
        this.output = output;
    }
}
