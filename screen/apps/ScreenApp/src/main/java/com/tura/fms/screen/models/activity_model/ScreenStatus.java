package com.tura.fms.screen.models.activity_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tura.fms.screen.models.equipment.EquipmentLogStartRequest;

public class ScreenStatus {

    @SerializedName("ref")
    @Expose
    private String ref;

    @SerializedName("data")
    @Expose
    private EquipmentLogStartRequest data;

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public EquipmentLogStartRequest getData() {
        return data;
    }

    public void setData(EquipmentLogStartRequest data) {
        this.data = data;
    }

}
