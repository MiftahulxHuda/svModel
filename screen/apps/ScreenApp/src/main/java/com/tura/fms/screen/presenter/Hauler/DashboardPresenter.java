package com.tura.fms.screen.presenter.Hauler;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.constanst.IKafkaConstant;
import com.tura.fms.screen.database.Synchronizer;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.dashboard_model.DashboardDetail;
import com.tura.fms.screen.models.dashboard_model.DashboardModel;
import com.tura.fms.screen.network.NetworkConnection;

import org.apache.kafka.clients.producer.Producer;
import org.json.JSONException;
import org.json.JSONObject;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class DashboardPresenter {

    private ObservableList<DashboardModel> recordsData = FXCollections.observableArrayList();

    public void clearModel() {
        recordsData.clear();
    }

    public void isiModel(DashboardDetail dashboardDetailPram) {

        String status = "Not Working";
        try {
            if (dashboardDetailPram.getStatus() != null) {
                status = dashboardDetailPram.getStatus();
            } else {
                status = "Not Working";
            }

        } catch (Exception e) {
        }

        String statusText = "Activity";
        if (dashboardDetailPram.getStatusText() != null) {
            statusText = dashboardDetailPram.getStatusText();
        }

        String from = "-";
        try {

            if (dashboardDetailPram.getLoadingPoint() != null) {
                from = dashboardDetailPram.getLoadingPoint();
            } else {
                from = "-";
            }

        } catch (Exception e) {
        }

        String to = "-";
        try {
            if (dashboardDetailPram.getDumpingPoint() != null) {
                to = dashboardDetailPram.getDumpingPoint();
            } else {
                to = "-";
            }

        } catch (Exception e) {
        }

        String cycle = "0";
        try {
            if (dashboardDetailPram.getCycle() != null) {
                cycle = dashboardDetailPram.getCycle().toString();
            } else {
                cycle = "0";
            }

        } catch (Exception e) {
        }

        String distance = "0 M";
        try {
            if (dashboardDetailPram.getDistance() != null) {
                distance = dashboardDetailPram.getDistance().toString() + " M";
            } else {
                distance = "0 M";
            }

        } catch (Exception e) {
        }

        String tonase = "0%";
        try {
            if (dashboardDetailPram.getPayload() != null) {
                if (MainApp.actualInfo.getCd_material().getType().equals(ConstantValues.MATERIAL_OB)) {
                    tonase = dashboardDetailPram.getPayload().toString() + " BCM";
                }
                if (MainApp.actualInfo.getCd_material().getType().equals(ConstantValues.MATERIAL_COAL)) {
                    tonase = dashboardDetailPram.getPayload().toString() + " Ton";
                }
                if (MainApp.actualInfo.getCd_material().getType().equals(ConstantValues.MATERIAL_MUD)) {
                    tonase = dashboardDetailPram.getPayload().toString() + " BCM";
                }
            } else {
                tonase = "0%";
            }

        } catch (Exception e) {
        }

        String speed = "0 Km/H";
        try {
            if (dashboardDetailPram.getSpeed() != null) {
                speed = dashboardDetailPram.getSpeed().toString() + " Km/H";
            } else {
                speed = "0 Km/H";
            }

        } catch (Exception e) {
        }

        String fuel = "0  Ltr/H";
        try {
            if (dashboardDetailPram.getFuel() != null) {
                fuel = dashboardDetailPram.getFuel().toString() + " Ltr/H";
            } else {
                fuel = "0  Ltr/H";
            }

        } catch (Exception e) {
        }

        String temp = "0 C";
        try {
            if (dashboardDetailPram.getTemperatur() != null) {
                temp = dashboardDetailPram.getTemperatur().toString() + " C";
            } else {
                temp = "0 C";
            }

        } catch (Exception e) {
        }

        String hour_meter = "0 H";
        try {
            if (dashboardDetailPram.getHourMeter() != null) {
                hour_meter = dashboardDetailPram.getHourMeter().toString() + " H";
            } else {
                hour_meter = "0 H";
            }

        } catch (Exception e) {
        }

        // recordsData.add(new DashboardModel("1", statusText, status));
        recordsData.add(new DashboardModel("1", "From", from));
        recordsData.add(new DashboardModel("2", "To", to));
        recordsData.add(new DashboardModel("3", "Cycle", cycle));
        recordsData.add(new DashboardModel("4", "Distance", distance));
        recordsData.add(new DashboardModel("5", "Tonase/BCM", tonase));
        recordsData.add(new DashboardModel("6", "Speed", speed));
        recordsData.add(new DashboardModel("7", "Fuel Rate", fuel));
        recordsData.add(new DashboardModel("8", "Engine Temperature", temp));
        recordsData.add(new DashboardModel("9", "Hour Meter", hour_meter));
    }

    public ObservableList<DashboardModel> runConsumer() {
        if (MainApp.latestActivity != null) {
            MainApp.dashboardInfo.setStatus(MainApp.latestActivity.getCdActivity().getName());
        }
        if (MainApp.actualInfo != null) {
            MainApp.dashboardInfo.setLoadingPoint(MainApp.actualInfo.getId_location().getName());
            MainApp.dashboardInfo.setDumpingPoint(MainApp.actualInfo.getId_destination().getName());
        }

        isiModel(MainApp.dashboardInfo);

        return recordsData;
    }

}
