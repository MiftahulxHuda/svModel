package com.tura.fms.screen;

import static org.javalite.app_config.AppConfig.p;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fazecast.jSerialComm.SerialPort;
import com.tura.fms.screen.constanst.ConfigApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.database.Activity_DB;
import com.tura.fms.screen.database.ActualSupport_DB;
import com.tura.fms.screen.database.Actual_DB;
import com.tura.fms.screen.database.Dashboard_DB;
import com.tura.fms.screen.database.SQLiteJDBC;
import com.tura.fms.screen.database.Synchronizer;
import com.tura.fms.screen.database.User_Info_DB;
import com.tura.fms.screen.helpers.NavigationUI;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.ScreenConfigDB;
import com.tura.fms.screen.models.actual_model.ActualDetail;
import com.tura.fms.screen.models.actual_model.ActualLoader;
import com.tura.fms.screen.models.actual_support.OperationSupport;
import com.tura.fms.screen.models.constant.ConstantDB;
import com.tura.fms.screen.models.dashboard_model.DashboardDetail;
import com.tura.fms.screen.models.device_status.DeviceStatusDB;
import com.tura.fms.screen.models.ecu.HMDB;
import com.tura.fms.screen.models.equipment.EquipmentAPI;
import com.tura.fms.screen.models.equipment.EquipmentDB;
import com.tura.fms.screen.models.equipment.EquipmentLogStartLocal;
import com.tura.fms.screen.models.equipment_category.EquipmentCategoryDB;
import com.tura.fms.screen.models.fatigue_model.FatigueLogDB;
import com.tura.fms.screen.models.gps.GPSDB;
import com.tura.fms.screen.models.location.Area;
import com.tura.fms.screen.models.location.LocationDB;
import com.tura.fms.screen.models.log_history.LogHistoryDB;
import com.tura.fms.screen.models.maintenance_model.MaintenanceScheduleDB;
import com.tura.fms.screen.models.maintenance_model.MaintenanceUnscheduleDB;
import com.tura.fms.screen.models.material.MaterialDB;
import com.tura.fms.screen.models.operation.OperationDB;
import com.tura.fms.screen.models.operation_log_detail.OperationLogDetailDB;
import com.tura.fms.screen.models.operation_status.OperationStatusDB;
import com.tura.fms.screen.models.prestart_model.PrestartLogDB;
import com.tura.fms.screen.models.prestart_model.Prestart_DB;
import com.tura.fms.screen.models.safety_status.SafetyStatusDB;
import com.tura.fms.screen.models.user_model.UserDetails;
import com.tura.fms.screen.network.NetworkConnection;
import com.tura.fms.screen.presenter.KafkaConsumerPresenter;
import com.tura.fms.screen.presenter.KafkaProducerPresenter;

import org.apache.log4j.xml.DOMConfigurator;
import org.comtel2000.keyboard.control.KeyBoardPopup;
import org.comtel2000.keyboard.control.KeyBoardPopupBuilder;
import org.comtel2000.keyboard.control.KeyboardType;
import org.fxmisc.richtext.InlineCssTextArea;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import id.bobilab.fms.Device;
import id.bobilab.fms.event.DeviceEvent;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class MainApp extends Application {

    public static final Logger log = LoggerFactory.getLogger(MainApp.class);
    private int posX, posY;

    public static String kafkaStatus = ConstantValues.DONE;
    public static String apiStatus = ConstantValues.DONE;

    private double xOffset = 0;
    private double yOffset = 0;

    public static String username;
    public static String password;

    public static Boolean offlineMode = false;

    public static final LogHistoryDB logHistoryDB = new LogHistoryDB();

    public static final ScreenConfigDB screenConfigDB = new ScreenConfigDB();
    // local storage all user
    public static final User_Info_DB userDB = new User_Info_DB();
    public static final Dashboard_DB dashboardDB = new Dashboard_DB();
    // local storage daftar prestart
    public static final Prestart_DB prestartDB = new Prestart_DB();
    public static final Activity_DB activityDB = new Activity_DB();
    // local storage setingan actual
    public static final Actual_DB actualDB = new Actual_DB();
    // local storage setingan actual support
    public static final ActualSupport_DB actualSupportDB = new ActualSupport_DB();
    // local storage equipment category
    public static final EquipmentCategoryDB equipmentCategorytDB = new EquipmentCategoryDB();
    // local storage semua unit
    public static final EquipmentDB equipmentDB = new EquipmentDB();
    // local storage data location
    public static final LocationDB locationDB = new LocationDB();
    // local storage semua data constant
    public static final ConstantDB constantDB = new ConstantDB();
    // local storage data material
    public static final MaterialDB materialDB = new MaterialDB();
    // local storage gps
    public static final GPSDB gpsDB = new GPSDB();
    // local storage ecu
    public static final HMDB hmDB = new HMDB();
    // local storage cpu, ram, disk, db status
    public static final DeviceStatusDB deviceStatusDB = new DeviceStatusDB();
    // local storage operation status
    public static final OperationStatusDB operationStatusDB = new OperationStatusDB();
    // local storage safety status
    public static final SafetyStatusDB safetyStatusDB = new SafetyStatusDB();
    // local storage data ritase
    public static final OperationDB operationDB = new OperationDB();
    // local storage prestart report
    public static final PrestartLogDB prestartLogDB = new PrestartLogDB();
    // local storage fatigue report
    public static final FatigueLogDB fatigueLogDB = new FatigueLogDB();
    // local storage maintenance schedule
    public static final MaintenanceScheduleDB maintenanceScheduleDB = new MaintenanceScheduleDB();
    // local storage maintenance unschedule
    public static final MaintenanceUnscheduleDB maintenanceUnscheduleDB = new MaintenanceUnscheduleDB();
    // local storage operation log detail
    public static final OperationLogDetailDB operationLogDetailDB = new OperationLogDetailDB();

    // public static final NetworkConnection networkConnection = new
    // NetworkConnection();

    public static Logger getLog() {
        return log;
    }

    public static final ConfigApp configApp = new ConfigApp();

    public static KafkaConsumerPresenter consumer;

    public static KafkaProducerPresenter producer;

    public static Thread kafkaTask;

    public static Thread networkCheckTask;

    public static Timer timer;

    public static TimerTask networkTask;

    public static Map<String, String> remoteConfig;

    public static boolean loggedIn = false;

    public static UserDetails user = null;

    public static String userToken = null;

    public static Map<String, String> requestHeader = null;

    public static ActualLoader actualLoader = null;

    // hauler dan loader
    public static ActualDetail actualInfo = null;

    public static OperationSupport actualSupport = null;

    // daftar current hauler
    public static List<ActualDetail> haulerList = null;

    /**
     * khusus hanya untuk loader menunjukan hauler yang sedang diisi oleh loader
     * mereferensi ke array haulerList
     */
    public static Integer activeHauler = null;

    // info equipment
    public static EquipmentAPI equipmentLocal = null;

    public static TimeZone timezone;

    public static Device device = new Device(MainApp.config("config.unit_id"));

    public static Double loadWeight = 0.0;

    public static Double emptyWeight = 0.0;

    // berdasarkan payload
    public static Double tonnage = 0.0;

    public static Double tonnageUjiPetik = 0.0;

    public static Double tonnageTimbangan = 0.0;

    public static Double tonnageBucket = 0.0;

    // berat dari checker lewat socket
    public static Double beratTimbangan = 0.0;

    // loading point ke dumping point
    public static Double distance = 0.0;

    // latest activity
    public static EquipmentLogStartLocal latestActivity = null;

    // ukuran screen
    public static Rectangle2D screenBounds;

    // loader bucket count
    public static int bucketCount = 0;

    // latest window event
    public static Event latestRefEvent;

    // for dashboard
    public static DashboardDetail dashboardInfo;

    // empty or full
    public static String travel;

    // dashboard thread
    public static Timeline clockDashboard = null;

    // fatigue level
    public static Integer fatigueLevel;

    // onscreen keyboard
    public static KeyBoardPopup popup;

    // interval dashboard update, dari config.properties
    public static Integer dashboardInterval;

    // NOT USED, push ecu or gps
    public static byte ecuGPS = 0;

    // pause emit ecu dan gps
    public static boolean pauseEmit = false;

    // log activity
    public static InlineCssTextArea textLog;

    // latest operation_actual_raw
    public static Object latestCycle;

    // key untuk sinkronisasi sebelum login
    public static String keyDefault = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImNiOWU5YzA4LTQyYmYtMTFlYS04MDZhLTAyNDJhYzEyMDAwMiIsImVtYWlsIjoiYWRtaW5pc3RyYXRvckBtYWlsLmNvbSIsIm5hbWUiOiJBZG1pbmlzdHJhdG9yIiwiY2Rfcm9sZSI6eyJjZCI6IjAwMzAwMCIsImNkX3NlbGYiOiIwMDMiLCJuYW1lIjoiQWRtaW5pc3RyYXRvciIsImpzb24iOnsiY2QiOiIwMDMwMDAiLCJzZXEiOm51bGwsIm5hbWUiOiJOT05FIiwic2x1ZyI6bnVsbCwidGFnXzEiOm51bGwsInRhZ18yIjpudWxsLCJ0YWdfMyI6bnVsbCwiY2Rfc2VsZiI6IjAwMyIsImlzX2ZpeGVkIjoxLCJpc19kZWxldGVkIjowfX0sImNkX2RlcGFydG1lbnQiOnsiY2QiOiIwMDQwMDEiLCJjZF9zZWxmIjoiMDA0IiwibmFtZSI6IlN5c3RlbSIsImpzb24iOnsiY2QiOiIwMDQwMDEiLCJzZXEiOm51bGwsIm5hbWUiOiJTeXN0ZW0iLCJzbHVnIjpudWxsLCJ0YWdfMSI6bnVsbCwidGFnXzIiOm51bGwsInRhZ18zIjpudWxsLCJjZF9zZWxmIjoiMDA0IiwiaXNfZml4ZWQiOjEsImlzX2RlbGV0ZWQiOjB9fSwic3RhZmZfaWQiOiIwMDAwMDAiLCJmaW5nZXJwcmludF9pZCI6MCwic29ja2V0X2lkIjpudWxsLCJmaWxlX3BpY3R1cmUiOm51bGwsImVudW1fYXZhaWxhYmlsaXR5Ijo0LCJpc19hdXRoZW50aWNhdGVkIjoxLCJfb3JnIjp7ImlkIjoiMSIsIm5hbWUiOiJQVCBTYXRyaWEgQmFoYW5hIFNhcmFuYSIsImFiYnIiOiJTQlMiLCJqc29uIjpudWxsfSwiX3JvbGUiOnsiY2QiOiIwMDMwMDAiLCJjZF9zZWxmIjoiMDAzIiwibmFtZSI6IkFkbWluaXN0cmF0b3IiLCJqc29uIjp7ImNkIjoiMDAzMDAwIiwic2VxIjpudWxsLCJuYW1lIjoiTk9ORSIsInNsdWciOm51bGwsInRhZ18xIjpudWxsLCJ0YWdfMiI6bnVsbCwidGFnXzMiOm51bGwsImNkX3NlbGYiOiIwMDMiLCJpc19maXhlZCI6MSwiaXNfZGVsZXRlZCI6MH19LCJfZGVwYXJ0bWVudCI6eyJjZCI6IjAwNDAwMSIsImNkX3NlbGYiOiIwMDQiLCJuYW1lIjoiU3lzdGVtIiwianNvbiI6eyJjZCI6IjAwNDAwMSIsInNlcSI6bnVsbCwibmFtZSI6IlN5c3RlbSIsInNsdWciOm51bGwsInRhZ18xIjpudWxsLCJ0YWdfMiI6bnVsbCwidGFnXzMiOm51bGwsImNkX3NlbGYiOiIwMDQiLCJpc19maXhlZCI6MSwiaXNfZGVsZXRlZCI6MH19LCJpYXQiOjE1ODk4NDc2NjUsImV4cCI6MTkwNTIwNzY2NX0.g3RTSvgqyE1RYBkQv7elsh1cabE6zNnTIPUXey4XiLw";

    // satuan berdaraskan material
    public static String satuan;

    // hour meter terakhir
    public static Double latestHourMeter;

    // dalam meter, KHUSUS untuk HAULER
    public static Double distanceToLoadingPoint;
    public static Double distanceToDumpingPoint;

    // khusus untuk loader
    public static Double activeHaulerPayload = 0.0;

    // gps speed
    public static Double gpsSpeed = 0.0;

    // latest lat
    public static Double latestLat = 0.0;

    // latest lon
    public static Double latestLon = 0.0;

    // latest gps time
    public static Long latestGpsTime = 0L;

    public static boolean dialogHourMeterShow = false;
    public static boolean dialogHourMeterLogoutClick = false;
    public static boolean dialogCustomDialogShow = false;

    // khusus mechanic
    public static String currentMaintenanceId;

    public static boolean isOperator = false;
    public static boolean isMechanic = false;
    public static boolean isSupervisor = false;
    public static boolean isGroupLeader = false;

    public static String haulerState = "";
    public static boolean needGetHaulerActual = false;

    public static Double hmDeviceCounter = 0.0;

    public static Integer counter = 0;

    public static Integer syncingCounter = 0;

    public static Area area = null;

    public static boolean initProcess = false;

    public static Long latestRequest = 0L;

    public static boolean hostUpButServiceDown = false;

    public static void main(String[] args) throws Exception {
        launch(args);
    }

    public static final String config(String param) {
        return configApp.get(param);
    }

    public void initialize_database() {
        SQLiteJDBC.openDB();
        MainApp.logHistoryDB.createTableLogHistory();
        MainApp.screenConfigDB.createTableScreenConfig();
        MainApp.userDB.createTableUser();
        MainApp.dashboardDB.createTableDashboard();
        MainApp.prestartDB.createTablePrestart();
        MainApp.activityDB.createTableActivity();
        MainApp.actualDB.createTableActual();
        MainApp.actualSupportDB.createTableActual();
        MainApp.equipmentCategorytDB.createTableEquipmentCategory();
        MainApp.equipmentDB.createTableEquipment();
        MainApp.locationDB.createTableLocation();
        MainApp.constantDB.createTableConstant();
        MainApp.materialDB.createTableMaterial();
        MainApp.gpsDB.createTableGPS();
        MainApp.hmDB.createTableHM();
        MainApp.deviceStatusDB.createTableDeviceStatus();
        MainApp.operationStatusDB.createTableOperationStatus();
        MainApp.safetyStatusDB.createTableSafetyStatus();
        MainApp.operationDB.createTableOperation();
        MainApp.prestartLogDB.createTablePrestartLog();
        MainApp.fatigueLogDB.createTableFatigueLog();
        MainApp.maintenanceScheduleDB.createTableMaintenanceSchedule();
        MainApp.maintenanceUnscheduleDB.createTableMaintenanceUnschedule();
        MainApp.operationLogDetailDB.createTableOperationLogDetail();
    }

    public static String[] getSerialPortNames() {
        SerialPort[] commPorts = SerialPort.getCommPorts();

        String[] names = new String[commPorts.length];
        for (int i = 0; i < commPorts.length; i++) {
            names[i] = commPorts[i].getSystemPortName();
        }
        return names;
    }

    public static void resetSettings() {
        MainApp.timezone = TimeZone.getTimeZone(MainApp.config("config.timezone"));

        Integer ecuReader = Integer.parseInt(MainApp.config("config.activate_ecu_reader"));
        Integer synchronizer = Integer.parseInt(MainApp.config("config.activate_synchronizer"));
        MainApp.dashboardInterval = Integer.parseInt(MainApp.config("config.dashboard_interval"));
        MainApp.screenBounds = Screen.getPrimary().getVisualBounds();
        MainApp.fatigueLevel = 0;

        MainApp.device = new Device(MainApp.config("config.unit_id"));

        Utilities.getEquipmentByName(MainApp.config("config.unit_id"));

        if (ecuReader == 1) {
            MainApp.engine_bobilab();
        }
    }

    public static void engine_bobilab() {
        MainApp.counter = 1;
        // System.out.println("Untuk Check Port : " +
        // Arrays.toString(getSerialPortNames()));

        // MainApp.device = new Device(MainApp.config("config.unit_id"));
        // MainApp.device.setGPSPort(MainApp.config("config.com_gps"));
        MainApp.device.setSensorPort(MainApp.config("config.com_gps"));
        MainApp.device.setParameterFuel(MainApp.config("config.setfuelparameter"));

        int interval = Integer.parseInt(MainApp.config("config.send_gps_ecu_interval"));
        String ecuIpAddress = MainApp.config("config.ecu_param_ip_address");
        String ecuType = MainApp.config("config.ecu_param_type");

        MainApp.device.setIntervalECU(interval);
        MainApp.device.openSensor();
        // MainApp.device.openGPS();
        try {
            MainApp.device.getECU(ecuIpAddress, ecuType);
        } catch (Exception ex) {
            log.error("MainApp.engine_bobilab", ex);
            Utilities.showMessageDialog("Failed", "", "Gagal set ECU IP Address, periksa kembali setting ScreenApp");
        }

        // PrintWriter writer = null;
        // try {
        // writer = new PrintWriter("log_alat_ecu.txt");
        // writer.print("");
        // writer.close();
        // } catch (FileNotFoundException e) {
        // e.printStackTrace();
        // }

        MainApp.device.addECUListener(new DeviceEvent() {
            @Override
            public void onData(JSONObject json) {
                // System.out.println("------------------- listener " +
                // Utilities.getDateTime().toString());

                // System.out.println(json.toString());

                JSONObject jSon = new JSONObject();
                try {
                    JSONObject dataHourMeter = MainApp.device.getHourMeter();

                    Double currentHM = dataHourMeter.getJSONObject("hour_meter").getDouble("value");
                    Boolean engineStatus = false;
                    if (currentHM > MainApp.hmDeviceCounter) {
                        engineStatus = true;
                        MainApp.hmDeviceCounter = currentHM;
                    } else {
                        engineStatus = false;
                    }

                    Double vva = MainApp.latestHourMeter;
                    if (currentHM != null && vva != null) {
                        vva += currentHM / 3600;
                    } else {
                        vva = 0.0;
                    }
                    if (MainApp.dashboardInfo != null) {
                        MainApp.dashboardInfo.setHourMeter(vva);
                    }

                    JSONObject jsonHourMeter = new JSONObject();
                    jsonHourMeter.put("unit", json.getJSONObject("hour_meter").getString("unit"));
                    jsonHourMeter.put("value", vva);
                    jSon.put("hour_meter", jsonHourMeter);

                    JSONObject jsonFuelRate = new JSONObject();
                    jsonFuelRate.put("unit", json.getJSONObject("fuel_rate").getString("unit"));
                    jsonFuelRate.put("value", json.getJSONObject("fuel_rate").getDouble("value"));
                    jSon.put("fuel_rate", jsonFuelRate);

                    JSONObject jsonDistance = new JSONObject();
                    jsonDistance.put("unit", json.getJSONObject("distance").getString("unit"));
                    jsonDistance.put("value", json.getJSONObject("distance").getDouble("value"));
                    jSon.put("distance", jsonDistance);

                    JSONObject jsonEngine = new JSONObject();
                    jsonEngine.put("unit", json.getJSONObject("temperature").getString("unit"));
                    jsonEngine.put("value", json.getJSONObject("temperature").getDouble("value"));
                    jSon.put("engine_temperature", jsonEngine);

                    JSONObject jsonPayload = new JSONObject();
                    jsonPayload.put("unit", json.getJSONObject("payload").getString("unit"));
                    jsonPayload.put("value", json.getJSONObject("payload").getDouble("value"));
                    jSon.put("payload", jsonPayload);

                    JSONObject jsonSpeed = new JSONObject();
                    jsonSpeed.put("unit", json.getJSONObject("speed").getString("unit"));
                    jsonSpeed.put("value", json.getJSONObject("speed").getDouble("value"));
                    jSon.put("vehicle_speed", jsonSpeed);

                    // GPS gps = Utilities.getCurrentGPS();

                    Double longitude = json.getJSONObject("gps").getDouble("lon");
                    Double latitude = json.getJSONObject("gps").getDouble("lat");

                    JSONObject ecus = null;
                    if (ecuType == null) {
                    } else if (ecuType.trim().equals("HD785")) {
                        ecus = MainApp.device.getECUHD785();
                    } else if (ecuType.trim().equals("PC1250")) {
                        ecus = MainApp.device.getECUPC1250();
                    } else if (ecuType.trim().equals("PC2000")) {
                        ecus = MainApp.device.getECUPC2000();
                    }

                    // System.out.println("ada -> ");
                    // System.out.println(MainApp.device.getECUHD785().toString());
                    // System.out.println(MainApp.device.getECUPC1250().toString());
                    // System.out.println(MainApp.device.getECUPC2000().toString());

                    // Double longitude = 103.782;
                    // Double latitude = -3.7569;
                    // latitude += 0.0001 * MainApp.counter;
                    // MainApp.counter++;

                    // Utilities.pushGPS(String.valueOf(gps.getLon()), String.valueOf(gps.getLat()),
                    // "0",
                    // json.getString("date"), jSon, engineStatus, ecus);

                    Utilities.pushGPS(String.valueOf(longitude), String.valueOf(latitude), "0", json.getString("date"),
                            jSon, engineStatus, ecus);

                    MainApp.latestLat = latitude;
                    MainApp.latestLon = longitude;

                } catch (Exception e) {
                    log.error("MainApp ecu listener gps", e);
                }

            }
        });
    }

    void setInitKey() {
        MainApp.requestHeader = new HashMap<>();
        MainApp.requestHeader.put("Authorization", keyDefault);
    }

    public void start(final Stage stage) {
        try {
            log.info("It works");

            NetworkConnection.getAPIUpStatus();
            NetworkConnection.getKafkaUpStatus();

            MainApp.timezone = TimeZone.getTimeZone(MainApp.config("config.timezone"));

            Integer ecuReader = Integer.parseInt(MainApp.config("config.activate_ecu_reader"));
            Integer synchronizer = Integer.parseInt(MainApp.config("config.activate_synchronizer"));
            MainApp.dashboardInterval = Integer.parseInt(MainApp.config("config.dashboard_interval"));
            MainApp.screenBounds = Screen.getPrimary().getVisualBounds();
            MainApp.fatigueLevel = 0;

            initialize_database();

            if (ecuReader == 1) {
                MainApp.engine_bobilab();
            }

            setInitKey();

            Font.loadFont(MainApp.class.getResource("/fonts/arialbd.ttf").toExternalForm(), 10);

            MainApp.popup = KeyBoardPopupBuilder.create().initLocale(Locale.ENGLISH).build();
            MainApp.popup.getKeyBoard().setKeyboardType(KeyboardType.NUMERIC);
            MainApp.popup.getKeyBoard().setScale(2);
            MainApp.popup.getKeyBoard().setMaxScale(2);
            MainApp.popup.getKeyBoard().setMinScale(2);
            MainApp.popup.setX(0);
            MainApp.popup.addGlobalFocusListener();
            MainApp.popup.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent event) {
                    if (event.getCode().equals(KeyCode.ENTER)) {
                        try {
                            MainApp.popup.hide();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            });

            NavigationUI.root = FXMLLoader.load(getClass().getResource(p("app.linkLogin")));

            textLog = new InlineCssTextArea();
            textLog.setId("textLog");
            textLog.getStyleClass().add("bg-dark");
            textLog.setPrefHeight(200);
            textLog.setWrapText(true);
            // textLog.setDisable(true);

            /*
             * root.setOnMousePressed((MouseEvent event) -> { xOffset = event.getSceneX();
             * yOffset = event.getSceneY(); }); root.setOnMouseDragged((MouseEvent event) ->
             * { stage.setX(event.getScreenX() - xOffset); stage.setY(event.getScreenY() -
             * yOffset); });
             */

            Scene scene = new Scene(NavigationUI.root);// , screenBounds.getWidth(), screenBounds.getHeight());
            scene.setFill(null);
            stage.setTitle(p("app.name"));
            stage.getIcons().add(new Image("/images/logo.png"));
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setScene(scene);
            stage.setMaximized(true);
            // stage.setResizable(false);
            // MainApp.popup.registerScene(scene);
            stage.setFullScreenExitHint("");
            stage.setFullScreen(true);
            stage.show();

            /**
             * activate kafka
             */
            Synchronizer.mapper = new ObjectMapper();
            MainApp.dashboardInfo = new DashboardDetail();
            // MainApp.kafkaTask = new Thread(new Task<Void>() {
            // @Override
            // public Void call() {

            MainApp.consumer = new KafkaConsumerPresenter();
            MainApp.producer = new KafkaProducerPresenter();

            // return null;
            // }
            // });
            // MainApp.kafkaTask.start();

            /**
             * get screen config
             */
            Utilities.getScreenConfig(null);

            /**
             * get equipment
             */
            Utilities.getEquipmentByName(MainApp.config("config.unit_id"));

            if (NetworkConnection.getServiceStatus()) {

                Thread syncTask = new Thread(new Task<Void>() {
                    @Override
                    public Void call() {
                        Synchronizer.doSync();
                        return null;
                    }
                });
                syncTask.start();

            }

            // if (synchronizer == 1) {
            startNetworkTasks();
            // }

        } catch (Exception ex) {

            log.error("MainApp.start", ex);

        }
    }

    private void startNetworkTasks() {
        /**
         * khusus untuk pengecekan kondisi service utama dan kafka
         */
        MainApp.networkCheckTask = new Thread(new Task<Void>() {

            @Override
            public Void call() {
                /**
                 * activate synchronizer
                 */
                // Synchronizer.createSynchronizer();

                /**
                 * network & service monitor
                 */
                MainApp.networkTask = new TimerTask() {
                    @Override
                    public void run() {
                        NetworkConnection.getAPIUpStatus();
                        NetworkConnection.getKafkaUpStatus();
                        // System.out.print("Service status" + " ---- ");
                        // System.out.print(NetworkConnection.getServiceStatus());
                        // System.out.println(NetworkConnection.getKafkaStatus());

                        MainApp.syncingCounter++;

                        Utilities.initOnline();

                        if (MainApp.syncingCounter > 60) {
                            MainApp.syncingCounter = 0;
                        }
                    }
                };

                MainApp.timer = new Timer();
                Long interval = Long.parseLong(MainApp.config("config.service_check_interval"));
                MainApp.timer.schedule(MainApp.networkTask, 0, interval);

                return null;
            }

        });

        MainApp.networkCheckTask.start();
    }

    /**
     * dipanggil ketika proses login sukses
     */
    public static void loginCallback(boolean status, UserDetails user) {
        System.out.println("============== login callback ==============");

        MainApp.loggedIn = status;
        MainApp.user = user;

        if (MainApp.offlineMode == false) {
            if (MainApp.requestHeader != null) {
                MainApp.requestHeader.clear();
            } else {
                MainApp.requestHeader = new HashMap<>();
            }
            MainApp.requestHeader.put("Authorization", "Bearer " + MainApp.user.getToken());
            if (MainApp.user.getToken() != null) {
                MainApp.userToken = MainApp.user.getToken();
            } else {
                MainApp.userToken = null;
            }

        } else {

            MainApp.requestHeader = null;

        }

        Utilities.getEquipmentByName(MainApp.config("config.unit_id"));
    }

    public static void resetUser() {
        MainApp.loggedIn = false;
        MainApp.isOperator = false;
        MainApp.isGroupLeader = false;
        MainApp.isSupervisor = false;
        MainApp.isMechanic = false;
        MainApp.actualInfo = null;
        MainApp.actualSupport = null;
        MainApp.actualLoader = null;
        MainApp.activeHauler = 0;

        if (MainApp.haulerList != null) {
            MainApp.haulerList.clear();
        }

        MainApp.requestHeader = null;
    }

    @Override
    public void stop() {
        System.exit(0);
    }

    /**
     * method penentu post data ke cloud or lokal
     *
     * MainApp.offlineMode, penanda offline mode diawal
     *
     * MainApp.isCanPost(), cek apakah bisa post ke API or tidak
     */
    public static boolean isCanPost() {
        // System.out.println("--- check rest api ---");
        if (!NetworkConnection.getServiceStatus()) {
            return false;
        }
        // System.out.println("--- check kafka ---");
        if (!NetworkConnection.getKafkaStatus()) {
            return false;
        }
        // System.out.println("--- check actual ---");
        if (MainApp.actualInfo == null && MainApp.actualSupport == null && MainApp.actualLoader == null) {
            return false;
        }
        // System.out.println("--- pass all ---");
        return true;
    }

}
