package com.tura.fms.screen.models.constant;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ConstantData {

    @SerializedName("output")
    @Expose
    private List<Constant> output = null;

    public List<Constant> getOutput() {
        return output;
    }

    public void setOutput(List<Constant> cd) {
        this.output = cd;
    }
}
