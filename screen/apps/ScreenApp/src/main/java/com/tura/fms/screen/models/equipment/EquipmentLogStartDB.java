package com.tura.fms.screen.models.equipment;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.database.Operation;
import com.tura.fms.screen.database.SQLiteJDBC;
import com.tura.fms.screen.models.DefaultResponse;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;

import retrofit2.Call;
import retrofit2.Response;

public class EquipmentLogStartDB extends Operation {
    public static final String TABLE_NAME = "EquipmentLogStart_Record";

    private static final String id = "id";
    private static final String id_actual = "id_actual";
    private static final String id_operation_support = "id_operation_support";
    private static final String time = "time";
    private static final String id_equipment = "id_equipment";
    private static final String id_operator = "id_operator";
    private static final String cd_activity = "cd_activity";
    private static final String cd_tum = "cd_tum";

    private static final String sync_status = "sync_status";

    public EquipmentLogStartDB() {
        super(EquipmentLogStartDB.TABLE_NAME, ConstantValues.DATA_EQUIPMENT_LOG_START);
    }

    public void createTableEquipmentLogStart() {
        try {
            conn().setAutoCommit(true);
            DatabaseMetaData dbm = conn().getMetaData();
            ResultSet rs = dbm.getTables(null, null, TABLE_NAME, null);
            Statement stmt = null;
            if (rs.next()) {
                System.out.println("Table equipment-log-start exists");
            } else {
                stmt = conn().createStatement();
                stmt.setQueryTimeout(10);
                stmt.executeUpdate(createTable());
                stmt.close();
                System.out.println("Table equipment-log-start created");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String createTable() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + id + " TEXT PRIMARY KEY," + id_actual + " TEXT NULL,"
                + id_operation_support + " TEXT NULL," + time + " NUMBER NULL," + id_equipment + " TEXT NULL,"
                + id_operator + " TEXT NULL," + cd_activity + " TEXT NULL," + cd_tum + " TEXT NULL," + sync_status
                + " NUMBER NULL" + ")";
    }

    @Override
    public Object insertIgnore(Object eqa) {
        EquipmentLogStartLocal eq = (EquipmentLogStartLocal) eqa;
        PreparedStatement st = null;
        Boolean rs = null;
        try {
            String sql = "INSERT INTO " + TABLE_NAME + " " + "(" + id + "," + id_actual + "," + id_operation_support
                    + "," + time + "," + id_equipment + "," + id_operator + "," + cd_activity + "," + cd_tum + ","
                    + sync_status + ")"
                    + " VALUES(?,?,?,?,?,?,?,?,0) ON CONFLICT(date) DO UPDATE SET sync_status=sync_status;";
            st = conn().prepareStatement(sql);
            st.setString(1, eq.getId());
            st.setString(2, eq.getIdActual());
            st.setString(3, eq.getIdOperationSupport());
            st.setInt(4, eq.getTime());
            st.setString(5, eq.getIdEquipment().toString());
            st.setString(6, eq.getIdOperator().toString());
            st.setString(7, eq.getCdActivity().getCd());
            st.setString(8, eq.getCdTum().getCd());
            rs = st.execute();
            st.close();

            return eq;

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (rs && st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
        return null;
    }

    @Override
    public void deleteData(String id) {
        String sql = "delete from " + TABLE_NAME + " where id=?";
        try {
            PreparedStatement ps = conn().prepareStatement(sql);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object getFromServer(Integer page, Integer size) {
        if (!NetworkConnection.getServiceStatus()) {
            return null;
        }
        // no action
        return null;
    }

    @Override
    public Integer getRemoteCount() {
        if (!NetworkConnection.getServiceStatus()) {
            return 0;
        }
        // no action
        return 0;
    }

    @Override
    public List<Object> getFlaggedData() {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE sync_status=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, "0");
            ResultSet cursor = preparedStatement.executeQuery();
            List<Object> result = new ArrayList<Object>();
            while (cursor.next()) {
                EquipmentLogStartLocal eq = new EquipmentLogStartLocal();
                eq.setId(cursor.getString(id));
                eq.setIdActual(cursor.getString(id_actual));
                eq.setIdOperationSupport(cursor.getString(id_operation_support));
                eq.setTime(cursor.getInt(time));
                eq.setIdEquipment(cursor.getString(id_equipment));
                eq.setIdOperator(cursor.getString(id_operator));
                // eq.setCdActivity(cursor.getString(cd_activity));
                // eq.setCdTum(cursor.getString(cd_tum));
                result.add(eq);
            }
            cursor.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void postBulkData(List<Object> eq) {
        if (!NetworkConnection.getKafkaStatus()) {
            return;
        }
        System.out.println("--- post synchronization ---");
        Call<DefaultResponse> call = APIClient.getInstance().postEquipmentLogStart(MainApp.requestHeader, eq);
        try {
            Response<DefaultResponse> response = call.execute();
            System.out.println(response);
            System.out.println(response.body().getRespon());
            System.out.println(response.body().getMessage());
            if (response.body().getRespon().equals("success")) {
                System.out.println("post success");
                /**
                 * change flag (sync_status to 1)
                 */
                for (Object obj : eq) {
                    EquipmentLogStart oqe = (EquipmentLogStart) obj;
                    setSynchronized(oqe.getId());
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void setSynchronized(String id) {
        String sql = "update " + TABLE_NAME + " set sync_status=1 where id=?";
        // deleteData(id);
        try {
            PreparedStatement ps = conn().prepareStatement(sql);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
