package com.tura.fms.screen.models.prestart_model.submit;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PrestartResponseBulk {

    @SerializedName("message")
    @Expose
    String message;

    @SerializedName("respon")
    @Expose
    String respon;

    @SerializedName("output")
    @Expose
    List<PrestartResponseDetail> prestartResponseDetail;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRespon() {
        return respon;
    }

    public void setRespon(String respon) {
        this.respon = respon;
    }

    public List<PrestartResponseDetail> getPrestartResponseDetail() {
        return prestartResponseDetail;
    }

    public void setPrestartResponseDetail(List<PrestartResponseDetail> prestartResponseDetail) {
        this.prestartResponseDetail = prestartResponseDetail;
    }

}
