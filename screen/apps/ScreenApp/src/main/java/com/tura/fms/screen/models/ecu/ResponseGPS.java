package com.tura.fms.screen.models.ecu;

import java.time.LocalDateTime;

public class ResponseGPS {

    public LocalDateTime date;

    public Double latitude;

    public Double longitude;

    public ResponseGPS(LocalDateTime date, Double latitude, Double longitude) {
        this.date = date;
        this.latitude = latitude;
        this.longitude = longitude;
    }

}
