package com.tura.fms.screen.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseModelCustom<T> {

    @SerializedName("response")
    @Expose
    String response;

    @SerializedName("message")
    @Expose
    String message;

    @SerializedName("output")
    @Expose
    T output;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getOutput() {
        return output;
    }

    public void setOutput(T output) {
        this.output = output;
    }
}
