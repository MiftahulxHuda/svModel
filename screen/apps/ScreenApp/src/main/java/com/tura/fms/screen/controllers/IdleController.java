package com.tura.fms.screen.controllers;

import static org.javalite.app_config.AppConfig.p;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXSnackbar;
import com.jfoenix.controls.JFXSnackbarLayout;
import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.database.Activity_DB;
import com.tura.fms.screen.database.Actual_DB;
import com.tura.fms.screen.helpers.GPS;
import com.tura.fms.screen.helpers.NavigationUI;
import com.tura.fms.screen.helpers.SessionFMS;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.activity_model.ActivityData;
import com.tura.fms.screen.models.activity_model.ActivityDetail;
import com.tura.fms.screen.models.activity_model.ScreenStatus;
import com.tura.fms.screen.models.actual_model.ActualDetail;
import com.tura.fms.screen.models.equipment.EquipmentLogStartRequest;
import com.tura.fms.screen.models.idle_model.IdleModel;
import com.tura.fms.screen.models.operation_status.OperationStatusLocal;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;
import com.tura.fms.screen.presenter.ActivityPresenter;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IdleController implements Initializable {

    private NavigationUI navigationUI = new NavigationUI();

    @FXML
    private Button TitleBar;

    @FXML
    private GridPane gridata;

    private ObservableList<IdleModel> recordsData = FXCollections.observableArrayList();

    // SessionFMS sessionFMS;

    ActivityData activityData;

    ActivityPresenter activityPresenter = new ActivityPresenter();

    // String token;

    Activity_DB activity_db;

    Actual_DB actual_db;

    ActualDetail actualDetail;

    @FXML
    VBox rootpane;

    JFXSnackbar snackbar;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        MainApp.clockDashboard.stop();

        TitleBar.setText(p("app.textLoading"));

        // sessionFMS = new SessionFMS();
        // token = sessionFMS.getSessionFms("token");

        // sessionFMS.touchFms();
        snackbar = new JFXSnackbar(rootpane);
        snackbar.setPrefWidth(300);
        snackbar.getStyleClass().add("-fx-background-color: #ccc;");

        activity_db = new Activity_DB();
        actual_db = new Actual_DB();

        drawerAction();

        getData();
    }

    private void getData() {
        List<Object> localDataStandby = MainApp.constantDB.getLocalById("cd_self", "STANDBY");
        List<Object> localDataIdle = MainApp.constantDB.getLocalById("cd_self", "IDLE");

        if (localDataStandby != null && localDataIdle != null) {

            TitleBar.setText(p("app.titleIdle"));
            ActivityData act = new ActivityData();
            act.setOutputObject(localDataStandby);
            ActivityData actIdle = new ActivityData();
            actIdle.setOutputObject(localDataIdle);
            tampilkanData(act, actIdle);

        } else {

            if (NetworkConnection.getServiceStatus()) {

                Call<ActivityData> call = APIClient.getInstance().GetActivity(MainApp.requestHeader, "IDLE");
                call.enqueue(new Callback<ActivityData>() {

                    @Override
                    public void onResponse(Call<ActivityData> call, Response<ActivityData> response) {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                TitleBar.setText(p("app.titleIdle"));
                                tampilkanData(response.body(), new ActivityData());
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<ActivityData> call, Throwable t) {
                        t.printStackTrace();
                    }
                });

            } else {
                System.out.println("Service not available and local data is empty");
            }
        }
    }

    private void tampilkanData(ActivityData activityData, ActivityData dataIdle) {

        int Col = 0;
        int Row = 0;

        try {
            // activityData = activityPresenter.getDataRestApi(token, "019");

            if (activityData.getOutput() != null) {

                GPS gps = Utilities.getCurrentGPS();

                for (int i = 0; i < activityData.getOutput().size(); i++) {

                    JFXButton rowbutton = new JFXButton(activityData.getOutput().get(i).getName());
                    VBox vboxForButtons = new VBox();

                    // ActivityDetail activityDetail = new ActivityDetail();
                    // activityDetail.setCd(activityData.getOutput().get(i).getCd());
                    // activityDetail.setName(activityData.getOutput().get(i).getName());
                    // activityDetail.setCd_self(activityData.getOutput().get(i).getCd_self());

                    // activity_db.insertData(activityDetail);

                    rowbutton.setButtonType(JFXButton.ButtonType.RAISED);
                    rowbutton.getStyleClass().add("button-activity");
                    rowbutton.setFont(Font.font("Arial", FontWeight.BOLD, 90));

                    rowbutton.setPrefHeight(80);
                    rowbutton.setPrefWidth(250);

                    rowbutton.setId(String.valueOf(i));

                    vboxForButtons.setPadding(new Insets(10, 5, 10, 5));

                    vboxForButtons.getChildren().add(rowbutton);

                    gridata.add(vboxForButtons, Col, Row);

                    gridata.setHalignment(rowbutton, HPos.CENTER);

                    gridata.setValignment(rowbutton, VPos.CENTER);

                    rowbutton.setOnAction((ActionEvent) -> {
                        rowbutton.setDisable(true);

                        int index = Integer.valueOf(rowbutton.getId());

                        if (MainApp.equipmentLocal == null) {

                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    Utilities.showMessageDialog("Equipment not registered", "",
                                            MainApp.config("config.unit_id") + " tidak belum terdaftar dalam database");

                                    rowbutton.setDisable(false);
                                }
                            });

                        } else if (MainApp.isCanPost()) {

                            EquipmentLogStartRequest activity = new EquipmentLogStartRequest();
                            if (Utilities.isLoaderHauler()) {
                                if (MainApp.isOperator) {
                                    activity.setIdActual(MainApp.actualInfo.getId());
                                }
                            } else {
                                if (MainApp.isOperator) {
                                    activity.setIdOperationSupport(MainApp.actualSupport.getId());
                                }
                            }
                            activity.setIdEquipment(MainApp.equipmentLocal.getId());
                            activity.setIdOperator(MainApp.user.getId());
                            activity.setTime("current");
                            activity.setCdActivity(activityData.getOutput().get(index).getCd());
                            activity.setCdTum(ConstantValues.TUM_IDLE);
                            Utilities.setActivity(activity, new Runnable() {

                                @Override
                                public void run() {
                                    rowbutton.setDisable(false);
                                }
                            });

                            /** push to kafka */
                            Utilities.producerActivity(activityData.getOutput().get(index).getCd(),
                                    activityData.getOutput().get(index).getName());

                        } else {

                            /**
                             * offline mode
                             */

                            OperationStatusLocal act = new OperationStatusLocal();
                            act.setIdActual(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId());
                            act.setIdOperationSupport(
                                    MainApp.actualSupport == null ? null : MainApp.actualSupport.getId());
                            act.setIdOperationLoader(
                                    MainApp.actualLoader == null ? null : MainApp.actualLoader.getId());
                            act.setIdEquipment(MainApp.equipmentLocal.getId());
                            act.setIdOperator(MainApp.user.getId());
                            act.setCdActivity(activityData.getOutput().get(index).getCd());
                            act.setCdTum(ConstantValues.TUM_IDLE);
                            act.setCdType(MainApp.config("config.unit_type"));
                            act.setTime(Utilities.getCurrentUnix());
                            act.setLatitude(gps.getLat());
                            act.setLongitude(gps.getLon());
                            OperationStatusLocal saved = (OperationStatusLocal) MainApp.operationStatusDB
                                    .insertIgnore(act);

                            Utilities.playSound("");

                            Utilities.setLatestActivity(saved);

                            if (MainApp.latestActivity != null) {

                                if (MainApp.latestActivity.getCdActivity() != null) {
                                    MainApp.dashboardInfo.setStatus(MainApp.latestActivity.getCdActivity().getName());
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_WORKING)) {
                                    MainApp.dashboardInfo.setStatusText("Activity");
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_DELAY)) {
                                    MainApp.dashboardInfo.setStatusText("Delay");
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_IDLE)) {
                                    MainApp.dashboardInfo.setStatusText("Idle");
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_DOWN)) {
                                    MainApp.dashboardInfo.setStatusText("Down");
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_STANDBY)) {
                                    MainApp.dashboardInfo.setStatusText("Standby");
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_ASSIGNED)) {
                                    MainApp.dashboardInfo.setStatusText("Assigned");
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_MAINTENANCE)) {
                                    MainApp.dashboardInfo.setStatusText("Maintenance");
                                }

                                Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        String actName = MainApp.latestActivity.getCdActivity() == null ? ""
                                                : ", " + MainApp.latestActivity.getCdActivity().getName();
                                        NavigationUI.addLogActivity(MainApp.latestActivity.getCdTum().getCd(),
                                                MainApp.latestActivity.getCdTum().getName() + actName);
                                    }
                                });
                            }

                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    rowbutton.setDisable(false);
                                }
                            });

                        }

                        snackbar.fireEvent(new JFXSnackbar.SnackbarEvent(new JFXSnackbarLayout("Success")));

                        TitleBar.fire();
                    });

                    Col++;

                    if (Col > 3) {
                        // Reset Column
                        Col = 0;
                        // Next Row
                        Row++;
                    }
                }

                Col = 0;
                Row++;
                Pane spring = new Pane();
                spring.setPrefHeight(60);
                gridata.add(spring, Col, Row);
                Row++;

                for (int i = 0; i < dataIdle.getOutput().size(); i++) {

                    JFXButton rowbutton = new JFXButton(dataIdle.getOutput().get(i).getName());
                    VBox vboxForButtons = new VBox();

                    // ActivityDetail activityDetail = new ActivityDetail();
                    // activityDetail.setCd(activityData.getOutput().get(i).getCd());
                    // activityDetail.setName(activityData.getOutput().get(i).getName());
                    // activityDetail.setCd_self(activityData.getOutput().get(i).getCd_self());

                    // activity_db.insertData(activityDetail);

                    rowbutton.setButtonType(JFXButton.ButtonType.RAISED);
                    rowbutton.getStyleClass().add("button-activity");
                    rowbutton.setFont(Font.font("Arial", FontWeight.BOLD, 90));

                    rowbutton.setPrefHeight(80);
                    rowbutton.setPrefWidth(250);

                    rowbutton.setId(String.valueOf(i));

                    vboxForButtons.setPadding(new Insets(10, 5, 10, 5));

                    vboxForButtons.getChildren().add(rowbutton);

                    gridata.add(vboxForButtons, Col, Row);

                    gridata.setHalignment(rowbutton, HPos.CENTER);

                    gridata.setValignment(rowbutton, VPos.CENTER);

                    rowbutton.setOnAction((ActionEvent) -> {
                        rowbutton.setDisable(true);

                        int index = Integer.valueOf(rowbutton.getId());

                        if (MainApp.isCanPost()) {

                            EquipmentLogStartRequest activity = new EquipmentLogStartRequest();
                            if (Utilities.isLoaderHauler()) {
                                activity.setIdActual(MainApp.actualInfo.getId());
                            } else {
                                activity.setIdOperationSupport(MainApp.actualSupport.getId());
                            }
                            activity.setIdEquipment(MainApp.equipmentLocal.getId());
                            activity.setIdOperator(MainApp.user.getId());
                            activity.setTime("current");
                            activity.setCdActivity(dataIdle.getOutput().get(index).getCd());
                            Utilities.setActivity(activity, new Runnable() {

                                @Override
                                public void run() {
                                    rowbutton.setDisable(false);
                                }
                            });

                            /** push to kafka */
                            Utilities.producerActivity(dataIdle.getOutput().get(index).getCd(),
                                    dataIdle.getOutput().get(index).getName());

                        } else {
                            /**
                             * offline mode
                             */

                            OperationStatusLocal act = new OperationStatusLocal();
                            act.setIdActual(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId());
                            act.setIdOperationSupport(
                                    MainApp.actualSupport == null ? null : MainApp.actualSupport.getId());
                            act.setIdOperationLoader(
                                    MainApp.actualLoader == null ? null : MainApp.actualLoader.getId());
                            act.setIdEquipment(MainApp.equipmentLocal.getId());
                            act.setIdOperator(MainApp.user.getId());
                            act.setCdActivity(dataIdle.getOutput().get(index).getCd());
                            act.setCdTum(ConstantValues.TUM_IDLE);
                            act.setCdType(MainApp.config("config.unit_type"));
                            act.setTime(Utilities.getCurrentUnix());
                            act.setLatitude(gps.getLat());
                            act.setLongitude(gps.getLon());
                            OperationStatusLocal saved = (OperationStatusLocal) MainApp.operationStatusDB
                                    .insertIgnore(act);

                            Utilities.playSound("");

                            Utilities.setLatestActivity(saved);

                            if (MainApp.latestActivity != null) {

                                if (MainApp.latestActivity.getCdActivity() != null) {
                                    MainApp.dashboardInfo.setStatus(MainApp.latestActivity.getCdActivity().getName());
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_WORKING)) {
                                    MainApp.dashboardInfo.setStatusText("Activity");
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_DELAY)) {
                                    MainApp.dashboardInfo.setStatusText("Delay");
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_IDLE)) {
                                    MainApp.dashboardInfo.setStatusText("Idle");
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_DOWN)) {
                                    MainApp.dashboardInfo.setStatusText("Down");
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_STANDBY)) {
                                    MainApp.dashboardInfo.setStatusText("Standby");
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_ASSIGNED)) {
                                    MainApp.dashboardInfo.setStatusText("Assigned");
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_MAINTENANCE)) {
                                    MainApp.dashboardInfo.setStatusText("Maintenance");
                                }

                                Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        String actName = MainApp.latestActivity.getCdActivity() == null ? ""
                                                : ", " + MainApp.latestActivity.getCdActivity().getName();
                                        NavigationUI.addLogActivity(MainApp.latestActivity.getCdTum().getCd(),
                                                MainApp.latestActivity.getCdTum().getName() + actName);
                                    }
                                });
                            }

                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    rowbutton.setDisable(false);
                                }
                            });

                        }

                        snackbar.fireEvent(new JFXSnackbar.SnackbarEvent(new JFXSnackbarLayout("Success")));

                        TitleBar.fire();
                    });

                    Col++;

                    if (Col > 3) {
                        // Reset Column
                        Col = 0;
                        // Next Row
                        Row++;
                    }

                }

            }

        } catch (Exception e) {

        }

    }

    // isi data model
    public void isiModel() {
        recordsData.add(new IdleModel("1", "P2H - P2H"));
        recordsData.add(new IdleModel("2", "TRVL - TRAVEL"));
        recordsData.add(new IdleModel("3", "PRFRN - PERBAIKAN FRONT"));
    }

    private void drawerAction() {
        TitleBar.setOnAction((ActionEvent evt) -> {
            TitleBar.setText(p("app.textLoading"));

            TitleBar.setDisable(true);

            navigationUI.back_dashboard(evt, new Runnable() {

                @Override
                public void run() {
                    TitleBar.setDisable(false);
                }
            });

            MainApp.clockDashboard.play();
        });
    }

}
