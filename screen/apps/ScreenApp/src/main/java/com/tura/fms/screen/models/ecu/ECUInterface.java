package com.tura.fms.screen.models.ecu;

import java.util.Map;

public interface ECUInterface {

    public ResponseData getHourMeter();

    public ResponseData getFuelRate();

    public ResponseData getDistance();

    public ResponseData getEngineTemperature();

    public ResponseData getPayload();

    public ResponseData getVehicleSpeed();

    public Map<String, ResponseData> getAllECU();

    public ResponseGPS getGPS();

    public void setTimezone(String tz);

    public void setECUPrefetchRate(Integer rate);

    public void setGPSPrefetchRate(Integer rate);

    public void setSoundVolume(Integer volume);

    public void setUnitID(String id);

    public String getTimezone();

    public Integer getECUPrefetchRate();

    public Integer getGPSPrefetchRate();

    public Integer getSoundVolume();

    public String getUnitID();

}
