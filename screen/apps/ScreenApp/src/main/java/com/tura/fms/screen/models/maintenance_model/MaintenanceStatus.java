package com.tura.fms.screen.models.maintenance_model;

public class MaintenanceStatus {

    private  String id;
    private  String name;

    public MaintenanceStatus(String id, String name) {
        this.id = id;
        this.name = name;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id=id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name= name;
    }

    @Override
    public String toString()
    {
        return "MaintenanceStatus [id=" + id + ", name=" + name + "]";
    }

}
