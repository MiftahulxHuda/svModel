package com.tura.fms.screen.network;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.util.Random;

import static com.tura.fms.screen.constanst.ConstantValues.broker_server;
import static com.tura.fms.screen.constanst.ConstantValues.qos_client;

public class MqttLib{

    private int qos;
    private String broker;
    private String client_id;
    private MemoryPersistence persistence;
    private MqttClient connectClient;
    private MqttConnectOptions connOpts;

    public MqttLib() {

        this.qos             = qos_client;
        this.broker       = broker_server;

        persistence = new MemoryPersistence();

        Random rand = new Random();
        int rand_int1 = rand.nextInt(1000);
        this.client_id     = "fms"+rand_int1;

        try {
            connectClient = new MqttClient(broker, client_id, persistence);
            connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            System.out.println("Connecting to broker: "+broker);
            connectClient.connect(connOpts);
            System.out.println("Connected");
            connOpts.setKeepAliveInterval(30);
            connOpts.setConnectionTimeout(30);


        } catch(MqttException me) {
            System.out.println("reason "+me.getReasonCode());
            System.out.println("msg "+me.getMessage());
            System.out.println("loc "+me.getLocalizedMessage());
            System.out.println("cause "+me.getCause());
            System.out.println("excep "+me);

           // me.printStackTrace();
        }

    }

    public MqttConnectOptions conOpts(){
        return connOpts;
    }

    public MqttClient clientMqtt(){

        return connectClient;
    }


   /* public void publish_message(String content,String Topic){

        try {

        System.out.println("Publishing message: "+content);
        MqttMessage message = new MqttMessage(content.getBytes());
        message.setQos(this.qos);
        clientMqtt().publish(Topic, message);
        System.out.println("Message published");
        clientMqtt().disconnect();
        System.out.println("Disconnected");
       // System.exit(0);

        } catch(MqttException me) {

            System.out.println("msg "+me.getMessage());
            System.out.println("loc "+me.getLocalizedMessage());
            System.out.println("cause "+me.getCause());
            System.out.println("excep "+me);

        }

    }

    public String subcribe(String topic_subcribe){



        try {

            connOpts.setCleanSession(true);
            connectClient.setCallback(new MqttCallback() {

                public void connectionLost(Throwable cause) {}

                public void messageArrived(String topic, MqttMessage message) throws Exception {
                    setMessage(message.toString());

                }

                public void deliveryComplete(IMqttDeliveryToken token) {


                }
            });

            connectClient.subscribe(topic_subcribe);
          //  connectClient.connect(connOpts);



        } catch (MqttException e) {
            e.printStackTrace();

        }

        return  getMessage();


    }*/




}
