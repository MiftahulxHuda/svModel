package com.tura.fms.screen.constanst;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class ConfigApp {

    private String path_url = System.getProperty("user.dir") + "/config.properties";

    public String get(String key_param) {
        String readconfig = "";
        try {

            Properties prop = new Properties();

            // load a properties file from class path, inside static method
            prop.load(new FileInputStream(path_url));

            // get the property value and print it out

            readconfig = prop.getProperty(key_param);

        } catch (IOException ex) {
            ex.printStackTrace();

        }
        return readconfig;
    }

    public void settingConfig(String unit_id, String unit_type, String comGps, String comSensor, String timezone,
            String main_service_address, String domain_name, String unit_service_address, String unit_service_domain,
            String kafka_address, String kafka_port, String request_timeout, String service_check_interval,
            String send_gps_ecu_interval, String synchronizer_interval, String synchronizer_delay, String sqlite_uri,
            String sqlite_auth, String sqlite_clean_interval, String main_service_address_backup,
            String kafka_addresss_backup, String sync_page_size, String activate_ecu_reader,
            String activate_synchronizer, String dashboard_interval, String setfuelparameter, String self_signed,
            String ecu_param_ip_address, String ecu_param_type, String kafka_poll_interval, String write_log) {

        try {

            OutputStream output = new FileOutputStream(path_url);
            Properties prop = new Properties();

            // set the properties value

            try {
                if (unit_id != null) {
                    prop.setProperty("config.unit_id", unit_id);
                } else {
                    prop.setProperty("config.unit_id", "HD10-001");
                }
            } catch (Exception e) {
            }

            try {
                if (unit_type != null) {
                    prop.setProperty("config.unit_type", unit_type);
                } else {
                    prop.setProperty("config.unit_type", "015001");
                }
            } catch (Exception e) {
            }

            // System.out.println(prop1.getProperty("config.main_service_address"));
            prop.setProperty("config.timezone", timezone);

            prop.setProperty("config.main_service_address", main_service_address);
            prop.setProperty("config.domain_name", domain_name);
            prop.setProperty("config.unit_service_address", unit_service_address);
            prop.setProperty("config.unit_service_domain", unit_service_domain);
            prop.setProperty("config.kafka_address", kafka_address);
            prop.setProperty("config.kafka_port", kafka_port);
            prop.setProperty("config.request_timeout", request_timeout);
            prop.setProperty("config.service_check_interval", service_check_interval);
            prop.setProperty("config.send_gps_ecu_interval", send_gps_ecu_interval);
            prop.setProperty("config.synchronizer_interval", synchronizer_interval);
            prop.setProperty("config.synchronizer_delay", synchronizer_delay);
            prop.setProperty("config.sqlite_uri", sqlite_uri);
            prop.setProperty("config.sqlite_auth", sqlite_auth);
            prop.setProperty("config.sqlite_clean_interval", sqlite_clean_interval);
            prop.setProperty("config.main_service_address_backup", main_service_address_backup);
            prop.setProperty("config.kafka_addresss_backup", kafka_addresss_backup);
            prop.setProperty("config.com_gps", comGps);
            prop.setProperty("config.com_sensor", comSensor);
            prop.setProperty("config.sync_page_size", sync_page_size);
            prop.setProperty("config.activate_ecu_reader", activate_ecu_reader);
            prop.setProperty("config.activate_synchronizer", activate_synchronizer);
            prop.setProperty("config.dashboard_interval", dashboard_interval);
            prop.setProperty("config.setfuelparameter", setfuelparameter);
            prop.setProperty("config.self_signed", self_signed);
            prop.setProperty("config.ecu_param_ip_address", ecu_param_ip_address);
            prop.setProperty("config.ecu_param_type", ecu_param_type);
            prop.setProperty("config.kafka_poll_interval", kafka_poll_interval);
            prop.setProperty("config.write_log", write_log);

            // save properties to project root folder
            prop.store(output, null);

            // System.out.println(prop1.getProperty("config.clean_period"));

        } catch (IOException io) {
            io.printStackTrace();
        }

    }

}
