package com.tura.fms.screen.models.actual_support;

import java.util.HashMap;
import java.util.Map;

public class LocationSupportLocal {

    private String id;
    private String cdLocation;
    private String name;
    private Integer longitude;
    private Integer latitude;
    private Integer altitude;
    private Integer dateSubmitted;
    private Integer dateModified;
    private Object json;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCd_location() {
        return cdLocation;
    }

    public void setCd_location(String cdLocation) {
        this.cdLocation = cdLocation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLongitude() {
        return longitude;
    }

    public void setLongitude(Integer longitude) {
        this.longitude = longitude;
    }

    public Integer getLatitude() {
        return latitude;
    }

    public void setLatitude(Integer latitude) {
        this.latitude = latitude;
    }

    public Integer getAltitude() {
        return altitude;
    }

    public void setAltitude(Integer altitude) {
        this.altitude = altitude;
    }

    public Integer getDate_submitted() {
        return dateSubmitted;
    }

    public void setDate_submitted(Integer dateSubmitted) {
        this.dateSubmitted = dateSubmitted;
    }

    public Integer getDate_modified() {
        return dateModified;
    }

    public void setDate_modified(Integer dateModified) {
        this.dateModified = dateModified;
    }

    public Object getJson() {
        return json;
    }

    public void setJson(Object json) {
        this.json = json;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}