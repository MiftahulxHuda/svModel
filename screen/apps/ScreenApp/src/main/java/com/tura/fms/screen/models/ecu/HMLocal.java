package com.tura.fms.screen.models.ecu;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "id_actual", "id_operation_support", "id_operation_loader", "time", "id_equipment",
        "id_operator", "type", "hm", "info" })
public class HMLocal {

    @JsonProperty("id")
    private String id;
    @JsonProperty("id_actual")
    private Object idActual;
    @JsonProperty("id_operation_support")
    private Object idOperationSupport;
    @JsonProperty("id_operation_loader")
    private Object idOperationLoader;
    @JsonProperty("time")
    private Long time;
    @JsonProperty("id_equipment")
    private String idEquipment;
    @JsonProperty("id_operator")
    private String idOperator;
    @JsonProperty("type")
    private String type;
    @JsonProperty("hm")
    private Double hm;
    @JsonProperty("info")
    private Object info;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("id_actual")
    public Object getIdActual() {
        return idActual;
    }

    @JsonProperty("id_actual")
    public void setIdActual(Object idActual) {
        this.idActual = idActual;
    }

    @JsonProperty("id_operation_support")
    public Object getIdOperationSupport() {
        return idOperationSupport;
    }

    @JsonProperty("id_operation_support")
    public void setIdOperationSupport(Object idOperationSupport) {
        this.idOperationSupport = idOperationSupport;
    }

    @JsonProperty("id_operation_loader")
    public Object getIdOperationLoader() {
        return idOperationLoader;
    }

    @JsonProperty("id_operation_loader")
    public void setIdOperationLoader(Object idOperationLoader) {
        this.idOperationLoader = idOperationLoader;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("time")
    public Long getTime() {
        return time;
    }

    @JsonProperty("time")
    public void setTime(Long time) {
        this.time = time;
    }

    @JsonProperty("id_equipment")
    public String getIdEquipment() {
        return idEquipment;
    }

    @JsonProperty("id_equipment")
    public void setIdEquipment(String idEquipment) {
        this.idEquipment = idEquipment;
    }

    @JsonProperty("id_operator")
    public String getIdOperator() {
        return idOperator;
    }

    @JsonProperty("id_operator")
    public void setIdOperator(String idOperator) {
        this.idOperator = idOperator;
    }

    @JsonProperty("hm")
    public Double getHm() {
        return hm;
    }

    @JsonProperty("hm")
    public void setHm(Double hm) {
        this.hm = hm;
    }

    @JsonProperty("info")
    public Object getInfo() {
        return info;
    }

    @JsonProperty("info")
    public void setInfo(Object info) {
        this.info = info;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}