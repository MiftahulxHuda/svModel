package com.tura.fms.screen.presenter.Support;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.models.dashboard_model.DashboardDetail;
import com.tura.fms.screen.models.dashboard_model.DashboardModel;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class DashboardSupportPresenter {

    private ObservableList<DashboardModel> recordsData = FXCollections.observableArrayList();

    public void isiModel(DashboardDetail dashboardDetailPram) {

        String status = "Not Working";
        try {
            if (dashboardDetailPram.getStatus() != null) {
                status = dashboardDetailPram.getStatus();
            } else {
                status = "Not Working";
            }

        } catch (Exception e) {
        }

        String statusText = "Activity";
        if (dashboardDetailPram.getStatusText() != null) {
            statusText = dashboardDetailPram.getStatusText();
        }

        String from = "-";
        try {
            if (dashboardDetailPram.getLoadingPoint() != null) {
                from = dashboardDetailPram.getLoadingPoint();
            } else {
                from = "-";
            }
        } catch (Exception e) {
        }

        String to = "-";
        try {
            if (dashboardDetailPram.getDumpingPoint() != null) {
                to = dashboardDetailPram.getDumpingPoint();
            } else {
                to = "-";
            }

        } catch (Exception e) {
        }

        String cycle = "0";
        try {
            if (dashboardDetailPram.getCycle() != null) {
                cycle = dashboardDetailPram.getCycle().toString();
            } else {
                cycle = "0";
            }

        } catch (Exception e) {
        }

        String distance = "0 M";
        try {
            if (dashboardDetailPram.getDistance() != null) {
                distance = dashboardDetailPram.getDistance().toString();
            } else {
                distance = "0 M";
            }

        } catch (Exception e) {
        }

        String tonase = "0%";
        try {
            if (dashboardDetailPram.getPayload() != null) {
                tonase = dashboardDetailPram.getPayload().toString();
            } else {
                tonase = "0%";
            }

        } catch (Exception e) {
        }

        String speed = "0 Km/H";
        try {
            if (dashboardDetailPram.getSpeed() != null) {
                speed = dashboardDetailPram.getSpeed().toString() + "  Km/H";
            } else {
                speed = "0 Km/H";
            }

        } catch (Exception e) {
        }

        String fuel = "0%";
        try {
            if (dashboardDetailPram.getFuel() != null) {
                fuel = dashboardDetailPram.getFuel().toString();
            } else {
                fuel = "0%";
            }

        } catch (Exception e) {
        }

        String temp = "0 C";
        try {
            if (dashboardDetailPram.getTemperatur() != null) {
                temp = dashboardDetailPram.getTemperatur().toString();
            } else {
                temp = "0 C";
            }

        } catch (Exception e) {
        }

        String hour_meter = "0 H";
        try {
            if (dashboardDetailPram.getHourMeter() != null) {
                hour_meter = dashboardDetailPram.getHourMeter().toString() + " H";
            } else {
                hour_meter = "0 H";
            }

        } catch (Exception e) {
        }

        // recordsData.add(new DashboardModel("1", statusText, status));
        recordsData.add(new DashboardModel("1", "Location", from));
        recordsData.add(new DashboardModel("2", "Hour Meter Awal", hour_meter));
        recordsData.add(new DashboardModel("3", "Speed", speed));
    }

    public ObservableList<DashboardModel> runConsumer() {
        if (MainApp.latestActivity != null) {
            MainApp.dashboardInfo.setStatus(MainApp.latestActivity.getCdActivity().getName());
        }
        if (MainApp.actualSupport != null) {
            if (MainApp.actualSupport.getLocationId() != null) {
                MainApp.dashboardInfo.setLoadingPoint(MainApp.actualSupport.getLocationId().getName());
            }
            if (MainApp.actualSupport.getDestinationId() != null) {
                MainApp.dashboardInfo.setDumpingPoint(MainApp.actualSupport.getDestinationId().getName());
            }
        }

        MainApp.dashboardInfo.setHourMeter(MainApp.latestHourMeter);

        isiModel(MainApp.dashboardInfo);

        return recordsData;
    }

}
