package com.tura.fms.screen.models.user_model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "__id_org__", "__has_id_org__", "__cd_role__", "__has_cd_role__", "__cd_department__",
        "__has_cd_department__", "name", "email", "pwd", "staff_id", "fingerprint_id", "socket_id", "file_picture",
        "ip_address", "token", "enum_availability", "is_authenticated", "json", "skill" })
public class UserActual {

    @JsonProperty("id")
    private String id;
    @JsonProperty("__id_org__")
    private String idOrg;
    @JsonProperty("__has_id_org__")
    private Boolean hasIdOrg;
    @JsonProperty("__cd_role__")
    private String cdRole;
    @JsonProperty("__has_cd_role__")
    private Boolean hasCdRole;
    @JsonProperty("__cd_department__")
    private String cdDepartment;
    @JsonProperty("__has_cd_department__")
    private Boolean hasCdDepartment;
    @JsonProperty("name")
    private String name;
    @JsonProperty("email")
    private String email;
    @JsonProperty("pwd")
    private String pwd;
    @JsonProperty("staff_id")
    private String staffId;
    @JsonProperty("fingerprint_id")
    private Integer fingerprintId;
    @JsonProperty("socket_id")
    private Object socketId;
    @JsonProperty("file_picture")
    private Object filePicture;
    @JsonProperty("ip_address")
    private Object ipAddress;
    @JsonProperty("token")
    private Object token;
    @JsonProperty("enum_availability")
    private Integer enumAvailability;
    @JsonProperty("is_authenticated")
    private Integer isAuthenticated;
    @JsonProperty("json")
    private Object json;
    @JsonProperty("skill")
    private Object skill;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("__id_org__")
    public String getIdOrg() {
        return idOrg;
    }

    @JsonProperty("__id_org__")
    public void setIdOrg(String idOrg) {
        this.idOrg = idOrg;
    }

    @JsonProperty("__has_id_org__")
    public Boolean getHasIdOrg() {
        return hasIdOrg;
    }

    @JsonProperty("__has_id_org__")
    public void setHasIdOrg(Boolean hasIdOrg) {
        this.hasIdOrg = hasIdOrg;
    }

    @JsonProperty("__cd_role__")
    public String getCdRole() {
        return cdRole;
    }

    @JsonProperty("__cd_role__")
    public void setCdRole(String cdRole) {
        this.cdRole = cdRole;
    }

    @JsonProperty("__has_cd_role__")
    public Boolean getHasCdRole() {
        return hasCdRole;
    }

    @JsonProperty("__has_cd_role__")
    public void setHasCdRole(Boolean hasCdRole) {
        this.hasCdRole = hasCdRole;
    }

    @JsonProperty("__cd_department__")
    public String getCdDepartment() {
        return cdDepartment;
    }

    @JsonProperty("__cd_department__")
    public void setCdDepartment(String cdDepartment) {
        this.cdDepartment = cdDepartment;
    }

    @JsonProperty("__has_cd_department__")
    public Boolean getHasCdDepartment() {
        return hasCdDepartment;
    }

    @JsonProperty("__has_cd_department__")
    public void setHasCdDepartment(Boolean hasCdDepartment) {
        this.hasCdDepartment = hasCdDepartment;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("pwd")
    public String getPwd() {
        return pwd;
    }

    @JsonProperty("pwd")
    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    @JsonProperty("staff_id")
    public String getStaffId() {
        return staffId;
    }

    @JsonProperty("staff_id")
    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    @JsonProperty("fingerprint_id")
    public Integer getFingerprintId() {
        return fingerprintId;
    }

    @JsonProperty("fingerprint_id")
    public void setFingerprintId(Integer fingerprintId) {
        this.fingerprintId = fingerprintId;
    }

    @JsonProperty("socket_id")
    public Object getSocketId() {
        return socketId;
    }

    @JsonProperty("socket_id")
    public void setSocketId(Object socketId) {
        this.socketId = socketId;
    }

    @JsonProperty("file_picture")
    public Object getFilePicture() {
        return filePicture;
    }

    @JsonProperty("file_picture")
    public void setFilePicture(Object filePicture) {
        this.filePicture = filePicture;
    }

    @JsonProperty("ip_address")
    public Object getIpAddress() {
        return ipAddress;
    }

    @JsonProperty("ip_address")
    public void setIpAddress(Object ipAddress) {
        this.ipAddress = ipAddress;
    }

    @JsonProperty("token")
    public Object getToken() {
        return token;
    }

    @JsonProperty("token")
    public void setToken(Object token) {
        this.token = token;
    }

    @JsonProperty("enum_availability")
    public Integer getEnumAvailability() {
        return enumAvailability;
    }

    @JsonProperty("enum_availability")
    public void setEnumAvailability(Integer enumAvailability) {
        this.enumAvailability = enumAvailability;
    }

    @JsonProperty("is_authenticated")
    public Integer getIsAuthenticated() {
        return isAuthenticated;
    }

    @JsonProperty("is_authenticated")
    public void setIsAuthenticated(Integer isAuthenticated) {
        this.isAuthenticated = isAuthenticated;
    }

    @JsonProperty("json")
    public Object getJson() {
        return json;
    }

    @JsonProperty("json")
    public void setJson(Object json) {
        this.json = json;
    }

    @JsonProperty("skill")
    public Object getSkill() {
        return skill;
    }

    @JsonProperty("skill")
    public void setSkill(Object skill) {
        this.skill = skill;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}