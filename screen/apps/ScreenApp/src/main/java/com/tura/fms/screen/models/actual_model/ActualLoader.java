package com.tura.fms.screen.models.actual_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tura.fms.screen.models.constant.Constant;

public class ActualLoader {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("id_supervisor")
    @Expose
    private Supervisor id_supervisor;

    @SerializedName("id_group_leader")
    @Expose
    private GroupLeader id_group_leader;

    @SerializedName("id_checker")
    @Expose
    private String id_checker;

    @SerializedName("id_loader_operator")
    @Expose
    private String id_loader_operator;

    @SerializedName("id_loader")
    @Expose
    private Loader id_loader = null;

    @SerializedName("id_location")
    @Expose
    private Location id_location = null;

    @SerializedName("id_destination")
    @Expose
    private Location id_destination;

    @SerializedName("cd_operation")
    @Expose
    private CdOperation cd_operation;

    @SerializedName("cd_shift")
    @Expose
    private Constant cd_shift;

    @SerializedName("cd_material")
    @Expose
    private CdMaterial cd_material;

    @SerializedName("date")
    @Expose
    private Long date;

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Supervisor getId_supervisor() {
        return id_supervisor;
    }

    public void setId_supervisor(Supervisor id_supervisor) {
        this.id_supervisor = id_supervisor;
    }

    public GroupLeader getId_group_leader() {
        return id_group_leader;
    }

    public void setId_group_leader(GroupLeader id_group_leader) {
        this.id_group_leader = id_group_leader;
    }

    public String getId_checker() {
        return id_checker;
    }

    public void setId_checker(String id_checker) {
        this.id_checker = id_checker;
    }

    public String getId_loader_operator() {
        return id_loader_operator;
    }

    public void setId_loader_operator(String id_loader_operator) {
        this.id_loader_operator = id_loader_operator;
    }

    public Loader getId_loader() {
        return id_loader;
    }

    public void setId_loader(Loader id_loader) {
        this.id_loader = id_loader;
    }

    public Location getId_location() {
        return id_location;
    }

    public void setId_location(Location id_location) {
        this.id_location = id_location;
    }

    public Location getId_destination() {
        return id_destination;
    }

    public void setId_destination(Location id_destination) {
        this.id_destination = id_destination;
    }

    public CdOperation getCd_operation() {
        return cd_operation;
    }

    public void setCd_operation(CdOperation cd_operation) {
        this.cd_operation = cd_operation;
    }

    public Constant getCd_shift() {
        return cd_shift;
    }

    public void setCd_shift(Constant cd_shift) {
        this.cd_shift = cd_shift;
    }

    public CdMaterial getCd_material() {
        return cd_material;
    }

    public void setCd_material(CdMaterial cd_material) {
        this.cd_material = cd_material;
    }

}
