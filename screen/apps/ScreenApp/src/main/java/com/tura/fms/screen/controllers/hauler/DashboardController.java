package com.tura.fms.screen.controllers.hauler;

import static javafx.geometry.NodeOrientation.LEFT_TO_RIGHT;
import static javafx.scene.shape.StrokeType.OUTSIDE;
import static org.javalite.app_config.AppConfig.p;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ScheduledExecutorService;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.constanst.IKafkaConstant;
import com.tura.fms.screen.database.Synchronizer;
import com.tura.fms.screen.helpers.FatigueUI;
import com.tura.fms.screen.helpers.GPS;
import com.tura.fms.screen.helpers.NavigationUI;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.dashboard_model.DashboardModel;
import com.tura.fms.screen.models.fatigue_model.FatigueModel;
import com.tura.fms.screen.models.operation.OperationLocal;
import com.tura.fms.screen.models.operation_log_detail.OperationLogDetailLocal;
import com.tura.fms.screen.presenter.Hauler.DashboardPresenter;

import org.json.JSONObject;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Duration;
import com.tura.fms.screen.network.NetworkConnection;

public class DashboardController implements Initializable {

    private NavigationUI navigationUI = new NavigationUI();

    @FXML
    private Text TitleBar;

    @FXML
    private Text textActivity;

    @FXML
    private Text textHour;

    @FXML
    private VBox paneLog;

    @FXML
    private GridPane gridata;

    private ObservableList<DashboardModel> recordsData = FXCollections.observableArrayList();

    // SessionFMS sessionFMS;

    @FXML
    private VBox vboxRoot;

    @FXML
    private Label labelMaterial;

    @FXML
    private Label location_label;

    @FXML
    private Label destination_label;

    @FXML
    private Label exavator_label;

    @FXML
    private Button buttonArriveFront;

    @FXML
    private Button buttonArriveDump;

    @FXML
    private Button buttonStartDump;

    @FXML
    private Button buttonFinishDump;

    DashboardPresenter dashboardPresenter;

    // @FXMLs
    // private Text lblinfo;

    @FXML
    private Text opname;

    @FXML
    private Label mode_connect;
    int no = 0;
    int no_offline = 0;

    @FXML
    private StackPane stackPane;

    FatigueUI fatigueUI;
    String time_fatigue = "";
    private ObservableList<FatigueModel> fatigueData = FXCollections.observableArrayList();

    Date date = new Date();

    Integer fatigueTimeoutConfig;
    Integer fatigueTimeout = 0;
    Boolean showFatigue = false;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        NavigationUI.publicStackPane = stackPane;

        System.out.println("---- dashboard controller ----- ");
        // TODO
        TitleBar.setText(MainApp.config("config.unit_id"));

        // sessionFMS = new SessionFMS();
        dashboardPresenter = new DashboardPresenter();
        fatigueUI = new FatigueUI();
        // sessionFMS.touchFms();

        opname.setText(MainApp.user.getName());

        Utilities.setSatuan();

        // set titme fatigue
        Utilities.setupFatigueTime();

        paneLog.getChildren().add(0, MainApp.textLog);

        gridata.setGridLinesVisible(true);

        MainApp.tonnageUjiPetik = Utilities.getUjiPetikHauler();

        String fTC = MainApp.remoteConfig.get(ConstantValues.SCREEN_CONFIG_FATIGUE_TIMEOUT);
        if (fTC != null) {
            fatigueTimeoutConfig = Integer.parseInt(fTC);
        } else {
            fatigueTimeoutConfig = ConstantValues.SCREEN_CONFIG_FATIGUE_DEFAULT_TIMEOUT;
        }

        MainApp.clockDashboard = new Timeline(new KeyFrame(Duration.ZERO, e -> {

            clearModel();

            recordsData = dashboardPresenter.runConsumer();
            tampilkanData(recordsData);

            fatigueUI.showFatigePopup(stackPane, Utilities.fatigueData, new Runnable() {

                @Override
                public void run() {
                    if (!showFatigue) {
                        showFatigue = true;
                        fatigueTimeout = 0;
                    }
                }
            }, new Runnable() {

                @Override
                public void run() {
                    if (showFatigue) {
                        fatigueUI.closeAlertDialog();
                    }
                    showFatigue = false;
                    fatigueTimeout = 0;
                    Utilities.postFatigueStatus();
                }
            });

            fatigueTimeout += MainApp.dashboardInterval;
            if (fatigueTimeout > fatigueTimeoutConfig && showFatigue) {
                MainApp.fatigueLevel++;
                fatigueTimeout = 0;
                fatigueUI.closeAlertDialog();
                fatigueUI.showMessage("Fatigue test timeout, LEVEL " + MainApp.fatigueLevel.toString(), stackPane);
            }
            if (MainApp.fatigueLevel >= ConstantValues.SCREEN_CONFIG_FATIGUE_MAX_LEVEL) {
                showFatigue = false;
                fatigueTimeout = 0;
                fatigueUI.closeAlertDialog();
                Utilities.postFatigueStatus();
            }

            /**
             * check activity tum
             */
            if (MainApp.latestActivity != null) {
                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_DOWN)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            Utilities.showMessageDialog("Unit Breakdown", "",
                                    "Unit breakdown, anda akan logout dari aplikasi");
                            Utilities.doLogOut(null, (Stage) buttonArriveFront.getScene().getWindow());
                        }
                    });
                }
            }

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    String tm = Utilities.getCurrentTimeString();
                    textHour.setText(tm);

                    String netS = NetworkConnection.getStringStatus();
                    mode_connect.setText(netS);
                    mode_connect.getStyleClass().clear();
                    if (netS.equals("Online")) {
                        mode_connect.getStyleClass().add("font-online");
                    } else {
                        mode_connect.getStyleClass().add("font-offline");
                    }

                    if (MainApp.isCanPost()) {
                        /**
                         * online mode
                         */

                        if (MainApp.isOperator) {
                            if (MainApp.actualInfo.getCd_material().getType().toUpperCase().trim()
                                    .equals("OVERBURDEN")) {
                                labelMaterial.setText("OB");
                            } else {
                                labelMaterial
                                        .setText(MainApp.actualInfo.getCd_material().getType().toUpperCase().trim());
                            }
                            String date = Utilities.timeStampToDate(MainApp.actualInfo.getDate(), "tanggal");
                            location_label.setText("DATE : " + date);
                            destination_label.setText("SHIFT : " + MainApp.actualInfo.getCd_shift().getName());
                            exavator_label.setText("EXCAVATOR : " + MainApp.actualInfo.getId_loader().getName());
                        } else {

                        }

                    } else {
                        /**
                         * offline mode
                         */
                        labelMaterial.setText("?");
                        String date = Utilities.timeStampToDate(Utilities.getCurrentUnix(), "tanggal");
                        location_label.setText("DATE : " + date);
                        destination_label.setText("SHIFT : "
                                + (Utilities.getCurrentShift().equals(ConstantValues.SHIFT_DAY) ? "Day" : "Night"));
                        exavator_label.setText("EXCAVATOR : ?");
                    }
                }
            });

        }), new KeyFrame(Duration.seconds(MainApp.dashboardInterval)));

        MainApp.clockDashboard.setCycleCount(Animation.INDEFINITE);
        MainApp.clockDashboard.play();
    }

    public void clearModel() {
        recordsData.clear();
        gridata.getChildren().clear();
    }

    private void tampilkanData(ObservableList<DashboardModel> recordsDataParam) {

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                if (MainApp.latestActivity != null) {

                    textActivity.setText(MainApp.latestActivity.getCdTum().getName().replaceAll(" Time", "") + ": "
                            + MainApp.latestActivity.getCdActivity().getName());

                    if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_WORKING)) {
                        textActivity.setFill(Color.web(ConstantValues.COLOR_WORKING, 1));
                    } else if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_DELAY)) {
                        textActivity.setFill(Color.web(ConstantValues.COLOR_DELAY, 1));
                    } else if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_IDLE)) {
                        textActivity.setFill(Color.web(ConstantValues.COLOR_IDLE, 1));
                    } else if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_DOWN)) {
                        textActivity.setFill(Color.web(ConstantValues.COLOR_DOWN, 1));
                    } else if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_RFU)) {
                        textActivity.setFill(Color.web(ConstantValues.COLOR_RFU, 1));
                    } else if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_STANDBY)) {
                        textActivity.setFill(Color.web(ConstantValues.COLOR_STANDBY, 1));
                    } else if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_ASSIGNED)) {
                        textActivity.setFill(Color.web(ConstantValues.COLOR_ASSIGNED, 1));
                    } else if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_MAINTENANCE)) {
                        textActivity.setFill(Color.web(ConstantValues.COLOR_MAINTENANCE, 1));
                    }

                }
            }
        });

        int Col = 0;
        int Row = 0;

        for (int i = 0; i < recordsDataParam.size(); i++) {

            VBox vboxForButtons = new VBox();
            vboxForButtons.setPrefWidth(247);
            vboxForButtons.setPrefHeight(90);

            HBox hboxForButtons = new HBox();
            // ImageView gambar = new ImageView(new Image("/images/icondash.png", 120, 100,
            // false, true));
            Text textdashboard = new Text();

            vboxForButtons.setAlignment(Pos.TOP_CENTER);
            vboxForButtons.setNodeOrientation(LEFT_TO_RIGHT);
            // vboxForButtons.setPrefHeight(99.0);
            // vboxForButtons.setPrefWidth(50.0);
            vboxForButtons.getStyleClass().add("button-dashboard");

            // set untuk button

            // hboxForButtons.setPrefHeight(10.0); // 30
            // hboxForButtons.setPrefWidth(30.0);10170127 // 60
            // hboxForButtons.setMaxHeight(20.0);
            hboxForButtons.setPadding(new Insets(5, 10, 10, 10));

            Text texthbox = new Text();
            texthbox.setStrokeType(OUTSIDE);
            texthbox.setText(recordsDataParam.get(i).getName());
            texthbox.setStrokeWidth(0.0);
            texthbox.setWrappingWidth(180);
            texthbox.setFont(Font.font("Verdana", 17));
            // texthbox.getStyleClass().add("text-dashboard");
            // texthbox.getStyleClass().add("font-white");
            texthbox.setFill(Color.WHITE);

            // gambar.setFitWidth(33.0);
            // gambar.setPickOnBounds(true);
            // gambar.setPreserveRatio(true);
            // gambar.setFitHeight(40.0);
            // gambar.setFitWidth(33.0);
            // gambar.setPickOnBounds(true);
            // gambar.setPreserveRatio(true);
            // gambar.setImage(new Image("@../images/icondash.png",160,160,false,true));

            hboxForButtons.getChildren().add(texthbox);

            // set text dashboard
            textdashboard.setText(recordsDataParam.get(i).getDescription());
            textdashboard.getStyleClass().add("text-dashboard");
            textdashboard.getStyleClass().add("font-white");
            // extdashboard.setFont(Font.font("Verdana", 20));
            textdashboard.setFill(Color.GREENYELLOW);

            VBox.setMargin(textdashboard, new Insets(0, 0, 10, 0));

            if (recordsDataParam.get(i).getId().equals("5")) {

                // perlakuan khusus untuk tonnage/payload

                Text textUjiPetik = new Text();
                textUjiPetik.setStrokeType(OUTSIDE);
                textUjiPetik.setTextAlignment(TextAlignment.RIGHT);
                // Double cap = Utilities.getUjiPetikHauler();
                Double cap = MainApp.tonnageUjiPetik;

                textUjiPetik.setText(cap.toString() + MainApp.satuan);
                textUjiPetik.setStrokeWidth(0.0);
                textUjiPetik.setWrappingWidth(80);
                textUjiPetik.setFont(Font.font("Verdana", 16));
                hboxForButtons.getChildren().add(textUjiPetik);

                vboxForButtons.getChildren().add(hboxForButtons);

                HBox hboxTonnage = new HBox();
                hboxTonnage.setPadding(new Insets(5, 5, 5, 5));
                Text tonPayload = new Text();
                tonPayload.setStrokeType(OUTSIDE);
                tonPayload.setTextAlignment(TextAlignment.CENTER);
                tonPayload.setText(MainApp.dashboardInfo.getTonnagePayload().toString() + MainApp.satuan);
                tonPayload.setStrokeWidth(0.0);
                tonPayload.setWrappingWidth(130);
                tonPayload.setFont(Font.font("Verdana", 18));
                tonPayload.setFill(Color.GREENYELLOW);
                Text tonBucket = new Text();
                tonBucket.setStrokeType(OUTSIDE);
                tonBucket.setTextAlignment(TextAlignment.CENTER);
                tonBucket.setText(MainApp.dashboardInfo.getTonnageBucket().toString() + MainApp.satuan);
                tonBucket.setStrokeWidth(0.0);
                tonBucket.setWrappingWidth(130);
                tonBucket.setFont(Font.font("Verdana", 18));
                tonBucket.setFill(Color.BLUE);
                hboxTonnage.getChildren().add(tonPayload);
                hboxTonnage.getChildren().add(tonBucket);

                vboxForButtons.getChildren().add(hboxTonnage);

                if (MainApp.dashboardInfo.getTonnageBucket() < cap) {
                    textUjiPetik.setFill(Color.YELLOW);
                    vboxForButtons.setStyle("-fx-background-color: RED;");
                } else {
                    textUjiPetik.setFill(Color.RED);
                    vboxForButtons.setStyle("-fx-background-color: GREEN;");
                }

                Double dbl = cap + (cap / 10);

                if (MainApp.dashboardInfo.getTonnageBucket() > dbl) {
                    vboxForButtons.setStyle("-fx-background-color: GRAY;");
                }

            } else {

                vboxForButtons.getChildren().add(hboxForButtons);
                vboxForButtons.getChildren().add(textdashboard);
            }

            gridata.add(vboxForButtons, Col, Row);

            Col++;

            if (Col > 2) {
                // Reset Column
                Col = 0;
                // Next Row
                Row++;
            }

        }

    }

    // public void scrollingText() {
    // TranslateTransition tt = new TranslateTransition(Duration.millis(10000),
    // lblinfo);
    // tt.setFromX(0 - lblinfo.getWrappingWidth() - 10); // setFromX sets the
    // starting position, coming from the left
    // // and going to the right.
    // int boundWidth = 760;
    // tt.setToX(lblinfo.getWrappingWidth() + boundWidth); // setToX sets to target
    // position, go beyond the right side
    // // of the screen.
    // tt.setCycleCount(Timeline.INDEFINITE); // repeats for ever
    // tt.setAutoReverse(true); // Always start over
    // tt.play();
    // }

    @FXML
    public void breakdownAction(ActionEvent event) throws Exception {
        MainApp.clockDashboard.stop();
        // scheduledExecutorService.shutdownNow();
        navigationUI.windowsTab(p("app.linkBreakdown"), p("app.titleBreakdown"), event);
    }

    @FXML
    public void logoutAction(ActionEvent event) throws Exception {
        Utilities.showHourMeterDialog(event);
    }

    @FXML
    public void lostimeAction(ActionEvent event) throws Exception {
        MainApp.clockDashboard.stop();
        // scheduledExecutorService.shutdownNow();
        navigationUI.windowsTab(p("app.linkLosttime"), p("app.titleLosttime"), event);
    }

    @FXML
    public void idleAction(ActionEvent event) throws Exception {
        MainApp.clockDashboard.stop();
        // scheduledExecutorService.shutdownNow();
        navigationUI.windowsTab(p("app.linkIdle"), p("app.titleIdle"), event);
    }

    @FXML
    public void safetyAction(ActionEvent event) throws Exception {
        MainApp.clockDashboard.stop();
        // scheduledExecutorService.shutdownNow();
        navigationUI.windowsTab(p("app.linkSafetyIssue"), p("app.titleSafety"), event);
    }

    @FXML
    public void onDutyAction(ActionEvent event) throws Exception {
        MainApp.clockDashboard.stop();
        navigationUI.windowsTab(p("app.linkOnDuty"), p("app.titleOnDuty"), event);
    }

    @FXML
    public void activityAction(ActionEvent event) throws Exception {
        MainApp.clockDashboard.stop();
        // scheduledExecutorService.shutdownNow();
        navigationUI.windowsTab(p("app.linkActivity"), p("app.titleActivity"), event);
    }

    @FXML
    public void arriveDumpAction(ActionEvent event) throws Exception {
        MainApp.haulerState = ConstantValues.HAULER_STATE_SPOTING_AT_DUMP;

        buttonArriveDump.setDisable(true);

        // todo: post status history
        OperationLogDetailLocal data = new OperationLogDetailLocal();
        data.setDate("current");
        data.setIdActual(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId());
        data.setCdTime(ConstantValues.TIME_CYCLE_ARRIVE_DUMPING);
        if (MainApp.equipmentLocal != null) {
            data.setIdEquipment(MainApp.equipmentLocal.getId());
        }
        data.setIdOperator(MainApp.user.getId());
        data.setCdEquipmentType(MainApp.config("config.unit_type"));
        data.setIdLocation(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId_location().getId());

        GPS gps = Utilities.getCurrentGPS();
        data.setLat(gps.getLat());
        data.setLon(gps.getLon());

        // data.setIdHauler();
        List<Object> eqs = new ArrayList<Object>();
        eqs.add(data);

        if (MainApp.isCanPost()) {
            MainApp.operationLogDetailDB.postBulkData(eqs, new Runnable() {

                @Override
                public void run() {
                    buttonArriveDump.setDisable(true);

                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            NavigationUI.addLogActivity("", "Arrive at dump");
                        }
                    });
                }
            }, new Runnable() {

                @Override
                public void run() {
                    buttonArriveDump.setDisable(true);

                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            NavigationUI.addLogActivity("", "Arrive at dump");
                        }
                    });
                }
            });

            // todo: send socket cycle
            SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
            Date date = new Date();
            String strDate = formatter.format(date);
            JSONObject json = new JSONObject();
            json.put("date", strDate);
            json.put("type",
                    MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_HAULER) ? "hauler" : "loader");
            json.put("sender", MainApp.equipmentLocal.getId());
            json.put("sender_name", MainApp.config("config.unit_id"));
            json.put("destination", MainApp.actualInfo.getId_loader().getId());
            json.put("message", ConstantValues.CYCLE_ARRIVE_DUMPING);
            MainApp.producer.push(IKafkaConstant.TOPIC_PRODUCE_CYCLE, json.toString());

        } else {
            /**
             * offline mode
             */
            data.setDate(Utilities.getCurrentUnix().toString());
            MainApp.operationLogDetailDB.insertIgnore(data);

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    NavigationUI.addLogActivity("", "Arrive at dump");
                }
            });

        }

        // set full weight
        JSONObject fullPayload = MainApp.device.getPayload();
        System.out.println(fullPayload.toString());
        MainApp.loadWeight = fullPayload.getJSONObject("payload").getDouble("value");

        /** berat loadcell yang dipakai itu di dumping point */
        if (MainApp.loadWeight > 0) {
            MainApp.tonnage = MainApp.loadWeight;
        }

        // dashboard hauler button status
        buttonArriveFront.setDisable(true);
        buttonArriveDump.setDisable(true);
        buttonStartDump.setDisable(false);
        buttonFinishDump.setDisable(true);

    }

    @FXML
    public void arriveFrontAction(ActionEvent event) throws Exception {
        MainApp.haulerState = ConstantValues.HAULER_STATE_QUEUE_AT_LOADING;

        buttonArriveFront.setDisable(true);

        // todo: post status history
        OperationLogDetailLocal data = new OperationLogDetailLocal();
        data.setDate("current");
        data.setIdActual(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId());
        data.setCdTime(ConstantValues.TIME_CYCLE_ARRIVE_LOADING);
        if (MainApp.equipmentLocal != null) {
            data.setIdEquipment(MainApp.equipmentLocal.getId());
        }
        data.setIdOperator(MainApp.user.getId());
        data.setCdEquipmentType(MainApp.config("config.unit_type"));
        data.setIdLocation(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId_location().getId());

        GPS gps = Utilities.getCurrentGPS();
        data.setLat(gps.getLat());
        data.setLon(gps.getLon());

        // data.setIdHauler();
        List<Object> eqs = new ArrayList<Object>();
        eqs.add(data);

        if (MainApp.isCanPost()) {

            MainApp.operationLogDetailDB.postBulkData(eqs, new Runnable() {

                @Override
                public void run() {
                    buttonArriveFront.setDisable(false);
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            NavigationUI.addLogActivity("", "Arrive at front");
                        }
                    });
                }
            }, new Runnable() {

                @Override
                public void run() {
                    buttonArriveFront.setDisable(false);
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            NavigationUI.addLogActivity("", "Arrive at front");
                        }
                    });
                }
            });

            // todo: send socket cycle
            SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
            Date date = new Date();
            String strDate = formatter.format(date);
            JSONObject json = new JSONObject();
            json.put("date", strDate);
            json.put("type",
                    MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_HAULER) ? "hauler" : "loader");
            json.put("sender", MainApp.equipmentLocal.getId());
            json.put("sender_name", MainApp.config("config.unit_id"));
            json.put("destination", MainApp.actualInfo.getId_loader().getId());
            json.put("message", ConstantValues.CYCLE_ARRIVE_LOADING);
            MainApp.producer.push(IKafkaConstant.TOPIC_PRODUCE_CYCLE, json.toString());

            // get cycle
            Utilities.getActualCycle(MainApp.actualInfo.getId(), null);

        } else {

            /**
             * offline mode
             */
            data.setDate(Utilities.getCurrentUnix().toString());
            MainApp.operationLogDetailDB.insertIgnore(data);

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    NavigationUI.addLogActivity("", "Arrive at front");
                }
            });

        }

        // set empty weight
        JSONObject emptyPayload = MainApp.device.getPayload();
        System.out.println(emptyPayload.toString());
        MainApp.emptyWeight = emptyPayload.getJSONObject("payload").getDouble("value");

        // dashboard hauler button status
        buttonArriveFront.setDisable(true);
        buttonArriveDump.setDisable(false);
        buttonStartDump.setDisable(true);
        buttonFinishDump.setDisable(true);

    }

    @FXML
    public void startDumpAction(ActionEvent event) throws Exception {
        MainApp.haulerState = ConstantValues.HAULER_STATE_START_DUMP;

        buttonStartDump.setDisable(true);

        // post status
        OperationLogDetailLocal data = new OperationLogDetailLocal();
        data.setDate("current");
        data.setIdActual(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId());
        data.setCdTime(ConstantValues.TIME_CYCLE_START_DUMP);
        if (MainApp.equipmentLocal != null) {
            data.setIdEquipment(MainApp.equipmentLocal.getId());
        }
        data.setIdOperator(MainApp.user.getId());
        data.setCdEquipmentType(MainApp.config("config.unit_type"));
        data.setIdLocation(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId_location().getId());

        GPS gps = Utilities.getCurrentGPS();
        data.setLat(gps.getLat());
        data.setLon(gps.getLon());

        // data.setIdHauler();
        List<Object> eqs = new ArrayList<Object>();
        eqs.add(data);

        if (MainApp.isCanPost()) {
            /**
             * online mode
             */

            MainApp.operationLogDetailDB.postBulkData(eqs, new Runnable() {

                @Override
                public void run() {
                    buttonStartDump.setDisable(true);

                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            NavigationUI.addLogActivity("", "Start dumping");
                        }
                    });
                }
            }, new Runnable() {

                @Override
                public void run() {
                    buttonStartDump.setDisable(true);

                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            NavigationUI.addLogActivity("", "Start dumping");
                        }
                    });
                }
            });

            // todo: send socket cycle
            SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
            Date date = new Date();
            String strDate = formatter.format(date);
            JSONObject json = new JSONObject();
            json.put("date", strDate);
            json.put("type",
                    MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_HAULER) ? "hauler" : "loader");
            json.put("sender", MainApp.equipmentLocal.getId());
            json.put("sender_name", MainApp.config("config.unit_id"));
            json.put("destination", MainApp.actualInfo.getId_loader().getId());
            json.put("message", ConstantValues.CYCLE_START_DUMP);
            MainApp.producer.push(IKafkaConstant.TOPIC_PRODUCE_CYCLE, json.toString());

        } else {
            /**
             * offline mode
             */
            data.setDate(Utilities.getCurrentUnix().toString());
            MainApp.operationLogDetailDB.insertIgnore(data);

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    NavigationUI.addLogActivity("", "Start dumping");
                }
            });
        }

        // set end distance
        JSONObject distance = MainApp.device.getDistance();
        MainApp.distance = distance.getJSONObject("distance").getDouble("value") - MainApp.distance;
        MainApp.dashboardInfo.setDistance(MainApp.distance);

        // dashboard hauler button status
        buttonArriveFront.setDisable(true);
        buttonArriveDump.setDisable(true);
        buttonStartDump.setDisable(true);
        buttonFinishDump.setDisable(false);
    }

    @FXML
    public void finishDumpAction(ActionEvent event) throws Exception {
        MainApp.haulerState = ConstantValues.HAULER_STATE_EMPTY_TRAVEL;

        buttonFinishDump.setDisable(true);

        // post status
        OperationLogDetailLocal data = new OperationLogDetailLocal();
        data.setDate("current");
        data.setIdActual(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId());
        data.setCdTime(ConstantValues.TIME_CYCLE_FINISH_DUMP);
        if (MainApp.equipmentLocal != null) {
            data.setIdEquipment(MainApp.equipmentLocal.getId());
        }
        data.setIdOperator(MainApp.user.getId());
        data.setCdEquipmentType(MainApp.config("config.unit_type"));
        data.setIdLocation(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId_location().getId());

        GPS gps = Utilities.getCurrentGPS();
        data.setLat(gps.getLat());
        data.setLon(gps.getLon());

        // data.setIdHauler();
        List<Object> eqs = new ArrayList<Object>();
        eqs.add(data);

        // post ritase
        OperationLocal ritase = new OperationLocal();
        // System.out.println("----------------- loader operator ");
        // System.out.println(MainApp.actualInfo.getId_loader_operator());
        // ritase.setId("");
        ritase.setDate("current");
        ritase.setDateOperated("current");
        ritase.setIdEquipment(MainApp.equipmentLocal == null ? null : MainApp.equipmentLocal.getId());
        ritase.setIdOperator(MainApp.user == null ? null : MainApp.user.getId());
        ritase.setIdActual(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId());
        ritase.setMaterialType(MainApp.actualInfo == null ? null : MainApp.actualInfo.getCd_material().getType());
        ritase.setShift(
                MainApp.actualInfo == null ? Utilities.getCurrentShift() : MainApp.actualInfo.getCd_shift().getCd());
        // ritase.setDate();
        ritase.setIdHaulerOperator(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId_hauler_operator());
        ritase.setIdMaterial(MainApp.actualInfo == null ? null : MainApp.actualInfo.getCd_material().getCd());
        ritase.setIdHauler(MainApp.equipmentLocal == null ? null : MainApp.equipmentLocal.getId());
        ritase.setIdLoaderOperator(MainApp.user == null ? null : MainApp.user.getId());
        ritase.setIdLoader(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId_loader().getId());
        ritase.setIdLocation(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId_location().getId());
        ritase.setIdDestination(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId_destination().getId());
        // ritase.setDateOperated();
        // todo : compute load weight, empty weight, tonnage, distance
        ritase.setLoadWeight(MainApp.loadWeight);
        ritase.setEmptyWeight(MainApp.emptyWeight);
        ritase.setTonnage(MainApp.tonnage);
        ritase.setTonnageTimbangan(MainApp.tonnageTimbangan);
        ritase.setTonnageUjipetik(MainApp.tonnageUjiPetik);
        ritase.setTonnageBucket(MainApp.tonnageBucket);
        ritase.setDistance(MainApp.distance);
        ritase.setBucketCount(MainApp.bucketCount);
        ritase.setIsSupervised(1);
        /**
         * is volume ditentukan server side
         */
        ritase.setIsVolume(0);
        ritase.setLat(gps.getLat());
        ritase.setLon(gps.getLon());

        if (MainApp.isCanPost()) {
            MainApp.operationLogDetailDB.postBulkData(eqs, new Runnable() {

                @Override
                public void run() {
                    buttonFinishDump.setDisable(true);

                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            NavigationUI.addLogActivity("", "Finish dumping");
                        }
                    });
                }
            }, new Runnable() {

                @Override
                public void run() {
                    buttonFinishDump.setDisable(true);

                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            NavigationUI.addLogActivity("", "Finish dumping");
                        }
                    });
                }
            });

            // todo: send socket cycle
            SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
            Date date = new Date();
            String strDate = formatter.format(date);
            JSONObject json = new JSONObject();
            json.put("date", strDate);
            json.put("type",
                    MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_HAULER) ? "hauler" : "loader");
            json.put("sender", MainApp.equipmentLocal.getId());
            json.put("sender_name", MainApp.config("config.unit_id"));
            json.put("destination", MainApp.actualInfo.getId_loader().getId());
            json.put("message", ConstantValues.CYCLE_FINISH_DUMP);
            JSONObject detail = new JSONObject();
            detail.put("loadWeight", MainApp.loadWeight);
            detail.put("emptyWeight", MainApp.emptyWeight);
            detail.put("tonnage", MainApp.tonnage);
            detail.put("distance", MainApp.distance);
            detail.put("bucketCount", MainApp.bucketCount);

            detail.put("latitude", gps.getLat());
            detail.put("longitude", gps.getLon());

            json.put("detail", detail);
            MainApp.producer.push(IKafkaConstant.TOPIC_PRODUCE_CYCLE, json.toString());

            MainApp.operationDB.postSingleData(ritase, new Runnable() {

                @Override
                public void run() {
                    buttonFinishDump.setDisable(true);
                }
            }, new Runnable() {

                @Override
                public void run() {
                    buttonFinishDump.setDisable(true);
                }
            });

            // get cycle
            Utilities.getActualCycle(MainApp.actualInfo.getId(), null);

            /**
             * ketika terjadi perubahan setingan actual oleh dispatcher maka lakukan update
             * data actual
             */
            if (MainApp.needGetHaulerActual) {
                Utilities.getOperationActual();
                NavigationUI.addLogActivity("", "Perubahan actual sudah dilakukan");

                MainApp.needGetHaulerActual = false;
            }

        } else {
            /**
             * offline mode
             */
            data.setDate(Utilities.getCurrentUnix().toString());
            MainApp.operationLogDetailDB.insertIgnore(data);

            ritase.setDate(Utilities.getCurrentUnix().toString());
            ritase.setDateOperated(Utilities.getCurrentUnix().toString());
            MainApp.operationDB.insertIgnore(ritase);

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    NavigationUI.addLogActivity("", "Finish dumping");
                }
            });

            Double cyc = MainApp.dashboardInfo.getCycle();
            if (cyc != null) {
                cyc += 1.0;
                MainApp.dashboardInfo.setCycle(cyc);
            }

        }

        MainApp.travel = ConstantValues.TRAVEL_EMPTY;

        // dashboard hauler button status
        buttonArriveFront.setDisable(false);
        buttonArriveDump.setDisable(true);
        buttonStartDump.setDisable(true);
        buttonFinishDump.setDisable(true);
    }

}
