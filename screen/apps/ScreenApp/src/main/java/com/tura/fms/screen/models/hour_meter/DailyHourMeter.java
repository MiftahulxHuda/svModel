package com.tura.fms.screen.models.hour_meter;

public class DailyHourMeter {

    private String actual_id;
    private String actual_support_id;
    private Double hm;
    private String equipment_id;
    private String type;
    private String info;
    private Integer date;
    private Integer is_deleted;

    public String getType() {
        return type;
    }

    public void setType(String ty) {
        this.type = ty;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getActualId() {
        return actual_id;
    }

    public void setActualId(String actualId) {
        this.actual_id = actualId;
    }

    public String getActualSupportId() {
        return actual_support_id;
    }

    public void setActualSupportId(String actualSupportId) {
        this.actual_support_id = actualSupportId;
    }

    public Double getHm() {
        return hm;
    }

    public void setHm(Double hm) {
        this.hm = hm;
    }

    public String getEquipmentId() {
        return equipment_id;
    }

    public void setEquipmentId(String equipmentId) {
        this.equipment_id = equipmentId;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public Integer getIsDeleted() {
        return is_deleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.is_deleted = isDeleted;
    }

}
