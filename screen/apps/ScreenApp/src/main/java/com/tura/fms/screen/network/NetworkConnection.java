package com.tura.fms.screen.network;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.models.ResponseModel;
import javafx.scene.control.Alert;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.URI;
import java.util.Collections;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NetworkConnection {

    private static Boolean status = true;

    private static Boolean kafkaStatus = true;

    public Boolean checkStatus() {
        Call<ResponseModel> call = APIClient.getInstance().getHealthCheck();
        try {
            Response<ResponseModel> response = call.execute();
            if (response.body().getMessage().equals("ok")) {
                NetworkConnection.status = true;
                // System.out.println("server up");
            } else {
                NetworkConnection.status = false;
                // System.out.println("server down");
            }
        } catch (IOException ex) {
            // ex.printStackTrace();
            NetworkConnection.status = false;
            // System.out.println("server down");
        }
        return NetworkConnection.status;
    }

    public static Boolean getServiceStatus() {
        // return true;
        return NetworkConnection.status;
    }

    public static Boolean getKafkaStatus() {
        // return true;
        return NetworkConnection.kafkaStatus;
    }

    public Boolean kafkaStatus() {
        Call<ResponseModel> call = APIClient.getUnitServiceInstance().getKafkaStatus();
        try {
            Response<ResponseModel> response = call.execute();
            if (response.body().getResponse().equals("success")) {
                NetworkConnection.kafkaStatus = true;
                // System.out.println("kafka up");
            } else {
                NetworkConnection.kafkaStatus = false;
                // System.out.println("kafka down");
            }
        } catch (IOException ex) {
            // ex.printStackTrace();
            NetworkConnection.kafkaStatus = false;
            // System.out.println("kafka down");
        }
        return NetworkConnection.kafkaStatus;
    }

    public String cek_indicator() {
        Socket sock = new Socket();
        InetSocketAddress addr = new InetSocketAddress(MainApp.config("config.domain_name"), 80);

        try {
            sock.connect(addr, 300);
            return "Online";
        } catch (Exception e) {
            return "Offline";
        } finally {
            try {
                sock.close();
            } catch (Exception e) {
            }
        }
    }

    public Boolean check_connection_bersih() {
        Socket sock = new Socket();
        InetSocketAddress addr = new InetSocketAddress(MainApp.config("config.domain_name"), 80);
        try {
            sock.connect(addr, 300);
            // alert.setTitle("Information");
            // alert.setHeaderText("Look, an Information Dialog");
            // alert.setContentText("Internet Successfully!");
            // System.out.println("check_connection_bersih => connected");
            return true;
            // alert.showAndWait();
        } catch (Exception e) {
            // System.out.println("check_connection_bersih => disconnected");
            return false;
        } finally {
            try {
                sock.close();
            } catch (Exception e) {
            }
        }
    }

    public Boolean check_connection() {
        Socket sock = new Socket();
        InetSocketAddress addr = new InetSocketAddress(MainApp.config("config.domain_name"), 80);
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        try {
            sock.connect(addr, 3000);
            // alert.setTitle("Information");
            // alert.setHeaderText("Look, an Information Dialog");
            // alert.setContentText("Internet Successfully!");
            return true;
            // alert.showAndWait();
        } catch (Exception e) {
            alert.setTitle("Information");
            alert.setHeaderText("Internet Lost Connection!");
            alert.setContentText("You can still run applications with the offline version");
            alert.showAndWait();
            return false;
        } finally {
            try {
                sock.close();
            } catch (Exception e) {
            }
        }
    }

    public static final class OsUtils {
        private static String OS = null;

        public static String getOsName() {
            if (OS == null) {
                OS = System.getProperty("os.name");
            }
            return OS;
        }

        public static boolean isWindows() {
            return getOsName().startsWith("Windows");
        }

        // public static boolean isUnix() // and so on
    }

    public static void networkInterfaces() {
        try {
            Enumeration<NetworkInterface> nets = NetworkInterface.getNetworkInterfaces();
            for (NetworkInterface nif : Collections.list(nets)) {
                String name = nif.getName();
                Boolean up = nif.isUp();
                System.out.print(name);
                System.out.println(up);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static Boolean checkHost(String ip, Integer port) {
        try {
            Socket sock = new Socket();
            InetSocketAddress addr = new InetSocketAddress(ip, port);
            sock.connect(addr, 5000);
            sock.close();
            return true;
        } catch (Exception ex) {
            // ex.printStackTrace();
            return false;
        }
    }

    public static void getAPIUpStatus() {
        String url = MainApp.config("config.main_service_address");
        try {
            URI uri = new URI(url);
            String domain = uri.getHost();
            NetworkConnection.status = NetworkConnection.checkHost(domain, 80);
            // System.out.println(uri + " ---- " + domain + " ---- " +
            // NetworkConnection.status.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void getKafkaUpStatus() {
        // Pattern IPA =
        // Pattern.compile("((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(25[0-5]|2[0-4]"
        // + "[0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]"
        // + "[0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}" +
        // "|[1-9][0-9]|[0-9]))");
        Pattern IPA = Pattern.compile("([0-9.]*)(\\/|:)([0-9]*)");
        String ka = MainApp.config("config.kafka_address");
        Matcher matcher = IPA.matcher(ka);
        if (matcher.find()) {
            String host = matcher.group(1);
            String port = matcher.group(3);
            try {
                NetworkConnection.kafkaStatus = NetworkConnection.checkHost(host, Integer.parseInt(port));
                // System.out.println(
                // ka + " ---- " + host + " ---- " + port + " ---- " +
                // NetworkConnection.kafkaStatus.toString());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public static String getStringStatus() {
        if (MainApp.initProcess) {
            return "Syncing";
        }
        if (status && kafkaStatus) {
            return "Online";
        } else {
            return "Offline";
        }
    }

}
