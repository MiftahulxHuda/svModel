package com.tura.fms.screen.models.user_model;

import java.util.HashMap;
import java.util.Map;

public class UserAPILocal {

    private String id;
    private String id_crew;
    private String id_org;
    private String cd_role;
    private String cd_department;
    private String name;
    private String email;
    private String pwd;
    private String staff_id;
    private String fingerprint_id;
    private String socket_id;
    private String file_picture;
    private String ip_address;
    private String token;
    private Integer enum_availability;
    private Integer is_authenticated;
    private String json;
    private String skill;
    private String equipments;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdCrew() {
        return id_crew;
    }

    public void setIdCrew(String idCrew) {
        this.id_crew = idCrew;
    }

    public String getIdOrg() {
        return id_org;
    }

    public void setIdOrg(String idOrg) {
        this.id_org = idOrg;
    }

    public String getCdRole() {
        return cd_role;
    }

    public void setCdRole(String cdRole) {
        this.cd_role = cdRole;
    }

    public String getCdDepartment() {
        return cd_department;
    }

    public void setCdDepartment(String cdDepartment) {
        this.cd_department = cdDepartment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getStaffId() {
        return staff_id;
    }

    public void setStaffId(String staffId) {
        this.staff_id = staffId;
    }

    public String getFingerprintId() {
        return fingerprint_id;
    }

    public void setFingerprintId(String fingerprintId) {
        this.fingerprint_id = fingerprintId;
    }

    public String getSocketId() {
        return socket_id;
    }

    public void setSocketId(String socketId) {
        this.socket_id = socketId;
    }

    public String getFilePicture() {
        return file_picture;
    }

    public void setFilePicture(String filePicture) {
        this.file_picture = filePicture;
    }

    public String getIpAddress() {
        return ip_address;
    }

    public void setIpAddress(String ipAddress) {
        this.ip_address = ipAddress;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getEnumAvailability() {
        return enum_availability;
    }

    public void setEnumAvailability(Integer enumAvailability) {
        this.enum_availability = enumAvailability;
    }

    public Integer getIsAuthenticated() {
        return is_authenticated;
    }

    public void setIsAuthenticated(Integer isAuthenticated) {
        this.is_authenticated = isAuthenticated;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public String getEquipments() {
        return equipments;
    }

    public void setEquipments(String equipments) {
        this.equipments = equipments;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
