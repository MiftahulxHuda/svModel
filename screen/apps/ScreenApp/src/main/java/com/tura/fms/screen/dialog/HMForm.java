package com.tura.fms.screen.dialog;

import javafx.beans.property.SimpleIntegerProperty;

public class HMForm {

    private SimpleIntegerProperty hm;

    public HMForm(int hm) {
        this.hm = new SimpleIntegerProperty(hm);
    }

    public int getHm() {
        return hm.get();
    }

    public void setHm(int h) {
        this.hm.set(h);
    }

    @Override
    public String toString() {
        return "hm: " + hm.get();
    }

}