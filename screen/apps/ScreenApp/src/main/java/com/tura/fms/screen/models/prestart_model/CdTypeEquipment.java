package com.tura.fms.screen.models.prestart_model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "cd", "cd_self", "name", "json" })
public class CdTypeEquipment {

    @JsonProperty("cd")
    private String cd;
    @JsonProperty("cd_self")
    private String cdSelf;
    @JsonProperty("name")
    private String name;
    @JsonProperty("json")
    private Object json;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("cd")
    public String getCd() {
        return cd;
    }

    @JsonProperty("cd")
    public void setCd(String cd) {
        this.cd = cd;
    }

    @JsonProperty("cd_self")
    public String getCdSelf() {
        return cdSelf;
    }

    @JsonProperty("cd_self")
    public void setCdSelf(String cdSelf) {
        this.cdSelf = cdSelf;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("json")
    public Object getJson() {
        return json;
    }

    @JsonProperty("json")
    public void setJson(Object json) {
        this.json = json;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}