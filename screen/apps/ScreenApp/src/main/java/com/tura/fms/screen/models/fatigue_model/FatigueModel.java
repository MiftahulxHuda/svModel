package com.tura.fms.screen.models.fatigue_model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class FatigueModel {

    private final StringProperty time;
    private final StringProperty date;
    private final IntegerProperty level;
    private final StringProperty status;
    private final IntegerProperty count;
    private final StringProperty id_user;

    public FatigueModel(String time,
                        String date,
                        int level,
                        String status,
                        int count,
                        String id_user
                        ) {

        this.time = new SimpleStringProperty(time);
        this.date = new SimpleStringProperty(date);
        this.level = new SimpleIntegerProperty(level);
        this.status = new SimpleStringProperty(status);
        this.count = new SimpleIntegerProperty(count);
        this.id_user = new SimpleStringProperty(id_user);


    }

    public String getTime() {
        return time.get();
    }

    public StringProperty timeProperty() {
        return time;
    }

    public void setTime(String time) {
        this.time.set(time);
    }

    public String getDate() {
        return date.get();
    }

    public StringProperty dateProperty() {
        return date;
    }

    public void setDate(String date) {
        this.date.set(date);
    }

    public int getLevel() {
        return level.get();
    }

    public IntegerProperty levelProperty() {
        return level;
    }

    public void setLevel(int level) {
        this.level.set(level);
    }

    public String getStatus() {
        return status.get();
    }

    public StringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    public int getCount() {
        return count.get();
    }

    public IntegerProperty countProperty() {
        return count;
    }

    public void setCount(int count) {
        this.count.set(count);
    }

    public String getId_user() {
        return id_user.get();
    }

    public StringProperty id_userProperty() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user.set(id_user);
    }




}
