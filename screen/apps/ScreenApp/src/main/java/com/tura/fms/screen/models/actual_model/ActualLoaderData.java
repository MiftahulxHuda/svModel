package com.tura.fms.screen.models.actual_model;

import com.google.gson.annotations.SerializedName;

public class ActualLoaderData {

    @SerializedName("output")
    private ActualLoader output;

    @SerializedName("response")
    private String response;

    @SerializedName("message")
    private String message;

    public ActualLoader getOutput() {
        return output;
    }

    public void setOutput(ActualLoader output) {
        this.output = output;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String res) {
        this.response = res;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String mes) {
        this.message = mes;
    }

}
