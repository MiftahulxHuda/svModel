package com.tura.fms.screen.presenter;

import java.util.concurrent.ExecutionException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.kafka.ProducerCreator;
import com.tura.fms.screen.network.APIClient;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

public class KafkaProducerPresenter {

    public static boolean exceptionHappened = false;
    public static Producer<String, String> producer = null;

    public RecordMetadata push(String topic, Object data) {
        if (KafkaProducerPresenter.producer == null) {
            KafkaProducerPresenter.producer = ProducerCreator.createProducer();
        }
        try {
            String jsonStr = APIClient.mapper.writeValueAsString(data);
            ProducerRecord<String, String> record = new ProducerRecord<String, String>(topic, MainApp.userToken,
                    jsonStr);

            producer.send(record, (metadata, exception) -> {
                System.out.println("------------------- kafka callback");
                if (metadata != null) {
                    System.out.println(
                            "Record sent to partition " + metadata.partition() + " with offset " + metadata.offset());
                    KafkaProducerPresenter.exceptionHappened = false;
                } else {
                    exception.printStackTrace();
                    if (KafkaProducerPresenter.exceptionHappened == false) {
                        Utilities.showMessageDialog("Broker Problem", "Terjadi kesalahan", exception.getMessage());
                    }
                    KafkaProducerPresenter.exceptionHappened = true;

                    // todo : data yang gagal dikirim, save di lokal

                }
                // producer.flush();
                // producer.close();
            });
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
            if (KafkaProducerPresenter.exceptionHappened == false) {
                Utilities.showMessageDialog("Exception", "Terjadi kesalahan", e.getMessage());
            }
            KafkaProducerPresenter.exceptionHappened = true;
        } finally {
            // producer.flush();
            // producer.close();
        }
        return null;
    }

    public RecordMetadata push(String topic, String jsonStr) {
        if (MainApp.kafkaStatus == ConstantValues.PROCESSING) {
            /**
             * TODO: save to lokal ketika kafka masih memproses request sebelumnya maka save
             * di lokal
             */

            /**
             * sepertinya tidak perlu di block ketika masih processing, karena di kafka
             * berlaku queuing untuk message yang nasuk
             */

            // return null;
        }

        if (KafkaProducerPresenter.producer == null) {
            KafkaProducerPresenter.producer = ProducerCreator.createProducer();
        }
        try {
            MainApp.kafkaStatus = ConstantValues.PROCESSING;

            String token = MainApp.userToken;

            if (token == null) {
                token = MainApp.keyDefault.replace("Bearer ", "");
            }

            ProducerRecord<String, String> record = new ProducerRecord<String, String>(topic, token, jsonStr);
            producer.send(record, (metadata, exception) -> {

                MainApp.kafkaStatus = ConstantValues.DONE;
                System.out.println("------------------- kafka sent");

                if (metadata != null) {
                    System.out.println(
                            "Record sent to partition " + metadata.partition() + " with offset " + metadata.offset());
                    KafkaProducerPresenter.exceptionHappened = false;
                } else {
                    exception.printStackTrace();
                    if (KafkaProducerPresenter.exceptionHappened == false) {
                        Utilities.showMessageDialog("Exception", "Terjadi kesalahan", exception.getMessage());
                    }
                    KafkaProducerPresenter.exceptionHappened = true;

                    // todo : data yang gagal dikirim, save di lokal
                }
                // producer.flush();
                // producer.close();
            });
        } catch (Exception e) {
            e.printStackTrace();
            if (KafkaProducerPresenter.exceptionHappened == false) {
                Utilities.showMessageDialog("Exception", "Terjadi kesalahan", e.getMessage());
            }
            KafkaProducerPresenter.exceptionHappened = true;
        } finally {
            // producer.flush();
            // producer.close();
        }
        return null;
    }

    public static void close() {
        if (KafkaProducerPresenter.producer == null) {
            KafkaProducerPresenter.producer.flush();
            KafkaProducerPresenter.producer.close();
        }
    }

}
