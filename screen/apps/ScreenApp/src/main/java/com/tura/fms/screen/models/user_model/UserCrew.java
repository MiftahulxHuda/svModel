package com.tura.fms.screen.models.user_model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.tura.fms.screen.models.constant.Constant;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "cd_roster", "cd_operation", "name", "json", "_roster", "_operation" })
public class UserCrew {

    @JsonProperty("id")
    private String id;
    @JsonProperty("cd_roster")
    private String cdRoster;
    @JsonProperty("cd_operation")
    private String cdOperation;
    @JsonProperty("name")
    private String name;
    @JsonProperty("json")
    private Object json;
    @JsonProperty("_roster")
    private Constant roster;
    @JsonProperty("_operation")
    private Constant operation;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("cd_roster")
    public String getCdRoster() {
        return cdRoster;
    }

    @JsonProperty("cd_roster")
    public void setCdRoster(String cdRoster) {
        this.cdRoster = cdRoster;
    }

    @JsonProperty("cd_operation")
    public String getCdOperation() {
        return cdOperation;
    }

    @JsonProperty("cd_operation")
    public void setCdOperation(String cdOperation) {
        this.cdOperation = cdOperation;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("json")
    public Object getJson() {
        return json;
    }

    @JsonProperty("json")
    public void setJson(Object json) {
        this.json = json;
    }

    @JsonProperty("_roster")
    public Constant getRoster() {
        return roster;
    }

    @JsonProperty("_roster")
    public void setRoster(Constant roster) {
        this.roster = roster;
    }

    @JsonProperty("_operation")
    public Constant getOperation() {
        return operation;
    }

    @JsonProperty("_operation")
    public void setOperation(Constant operation) {
        this.operation = operation;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
