package com.tura.fms.screen.database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.DefaultResponse;
import com.tura.fms.screen.models.PagingResponse;
import com.tura.fms.screen.models.ResponseModel;
import com.tura.fms.screen.models.actual_support.CdActivitySupport;
import com.tura.fms.screen.models.actual_support.EquipmentSupport;
import com.tura.fms.screen.models.actual_support.LocationSupport;
import com.tura.fms.screen.models.actual_support.OperationSupport;
import com.tura.fms.screen.models.actual_support.UserSupport;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;

import retrofit2.Call;
import retrofit2.Response;

public class ActualSupport_DB extends Operation {
    // Table Name
    private static final String TABLE_INFO = "ActualSupport_Record";

    // Table Columns
    private static final String id = "id";
    private static final String date = "date";
    private static final String id_supervisor = "id_supervisor";
    private static final String id_group_leader = "id_group_leader";
    private static final String id_operator = "id_operator";
    private static final String id_equipment = "id_equipment";
    private static final String id_location = "id_location";
    private static final String id_destination = "id_destination";
    private static final String cd_activity = "cd_activity";
    private static final String cd_shift = "cd_shift";
    private static final String hm_start = "hm_start";
    private static final String hm_stop = "hm_stop";
    private static final String sync_status = "sync_status";

    public static String createTable() {

        return "CREATE TABLE " + TABLE_INFO + "(" + id + " TEXT PRIMARY KEY," + date + " NUMBER  NULL," + id_supervisor
                + " TEXT  NULL," + id_group_leader + " TEXT NULL," + id_operator + " TEXT NULL," + id_equipment
                + " TEXT NULL," + id_location + " TEXT NULL," + id_destination + " TEXT NULL," + cd_activity
                + " TEXT NULL," + cd_shift + " TEXT NULL," + hm_start + " NUMBER NULL," + hm_stop + " NUMBER NULL,"
                + sync_status + " NUMBER NULL" + ")";
    }

    public ActualSupport_DB() {
        super(ActualSupport_DB.TABLE_INFO, ConstantValues.DATA_ACTUAL_SUPPORT);
    }

    public void createTableActual() {
        try {
            DatabaseMetaData dbm = conn().getMetaData();
            ResultSet rs = dbm.getTables(null, null, TABLE_INFO, null);
            Statement stmt = null;

            if (rs.next()) {
                System.out.println("Table exists Actual support");
                // stmt.close();
                // conn().close();
            } else {
                stmt = conn().createStatement();
                stmt.executeUpdate(createTable());
                stmt.close();
                // conn().close();
                System.out.println("Table created Actual support");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int getExisting(String cd_param) {
        try {
            String sql = "SELECT count(*) as total FROM " + TABLE_INFO + " where " + id + "=?";

            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, cd_param);

            ResultSet cursor = preparedStatement.executeQuery();

            int jumlah;

            if (cursor.next()) {
                jumlah = cursor.getInt("total");
            } else {
                jumlah = 0;
            }
            cursor.close();
            // conn().close();
            return jumlah;
        } catch (Exception e) {
            return 0;
        }
    }

    public OperationSupport getData() {
        try {
            ResultSet cursor = null;

            String column = id + "," + date + "," + id_supervisor + "," + id_group_leader + "," + id_operator + ","
                    + id_equipment + "," + id_location + "," + id_destination + "," + cd_activity + "," + cd_shift + ","
                    + hm_start + "," + hm_stop;

            String sql = "SELECT " + column + " FROM " + TABLE_INFO + " ORDER BY " + id + " desc LIMIT 1";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);

            cursor = preparedStatement.executeQuery();

            OperationSupport actualDetail = new OperationSupport();

            while (cursor.next()) {

                actualDetail.setId(cursor.getString(1));
                actualDetail.setDate(cursor.getInt(2));

                UserSupport supervisor = new UserSupport();
                supervisor.setId(cursor.getString(3));
                actualDetail.setSupervisorId(supervisor);
                UserSupport groupLeader = new UserSupport();
                groupLeader.setId(cursor.getString(4));
                actualDetail.setGroupLeaderId(groupLeader);

                UserSupport operator = new UserSupport();
                operator.setId(cursor.getString(5));
                actualDetail.setOperatorId(operator);

                EquipmentSupport equipememt = new EquipmentSupport();
                equipememt.setId(cursor.getString(6));
                actualDetail.setEquipmentId(equipememt);

                LocationSupport location = new LocationSupport();
                location.setId(cursor.getString(7));
                actualDetail.setLocationId(location);

                LocationSupport destination = new LocationSupport();
                destination.setId(cursor.getString(8));
                actualDetail.setDestinationId(destination);

                CdActivitySupport operation = new CdActivitySupport();
                operation.setCd(cursor.getString(9));
                actualDetail.setCdActivity(operation);

                CdActivitySupport shift = new CdActivitySupport();
                shift.setCd(cursor.getString(10));
                actualDetail.setCdShift(shift);

                actualDetail.setHmStart(cursor.getDouble(11));
                actualDetail.setHmStop(cursor.getDouble(12));
            }

            // conn().close();
            cursor.close();
            return actualDetail;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void insertData(OperationSupport actualDetail) {
        PreparedStatement st = null;
        Boolean rs = null;
        String proses;
        try {
            String sql;

            if (getExisting(actualDetail.getId()) == 0) {

                sql = "INSERT INTO " + TABLE_INFO + " " + "(" + id + "," + date + "," + id_supervisor + ","
                        + id_group_leader + "," + id_operator + "," + id_equipment + "," + id_location + ","
                        + id_destination + "," + cd_activity + "," + cd_shift + "," + hm_start + "," + hm_stop + ","
                        + sync_status + ")" + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,0);";

                proses = "proses";

            } else {

                sql = "UPDATE  " + TABLE_INFO + " SET " + "" + id + "=?," + date + "=?" + id_supervisor + "=?,"
                        + id_group_leader + "=?," + id_operator + "=?," + id_equipment + "=?," + id_location + "=?,"
                        + id_destination + "=?," + cd_activity + "=?," + cd_shift + "=?" + hm_start + "=?" + hm_start
                        + "=?" + " WHERE " + id + "=?;";

                proses = "update";

            }

            st = conn().prepareStatement(sql);

            st.setString(1, actualDetail.getId());
            st.setLong(2, actualDetail.getDate());
            st.setString(3, actualDetail.getSupervisorId().getId());
            st.setString(4, actualDetail.getGroupLeaderId().getId());
            st.setString(5, actualDetail.getOperatorId().getId());
            st.setString(6, actualDetail.getEquipmentId().getId());
            st.setString(7, actualDetail.getLocationId().getId());
            st.setString(8, actualDetail.getDestinationId().getId());
            st.setString(9, actualDetail.getCdActivity().getCd());
            st.setString(10, actualDetail.getCdShift().getCd());
            st.setDouble(11, actualDetail.getHmStart() == null ? 0 : actualDetail.getHmStart());
            st.setDouble(12, actualDetail.getHmStop() == null ? 0 : actualDetail.getHmStop());

            if (proses.equals("update")) {
                st.setString(13, actualDetail.getId());
            }
            rs = st.execute();
            // conn().close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {

            if (rs) {
                try {
                    st.close();
                    // conn().close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public int localCount() {
        try {
            Statement s = conn().createStatement();
            ResultSet r = s.executeQuery("SELECT COUNT(*) AS rowcount FROM " + TABLE_INFO + " WHERE date>="
                    + Utilities.getCurrentTimestamp().getTime());
            r.next();
            int count = r.getInt("rowcount");
            r.close();
            s.close();
            return count;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    @Override
    public Object insertIgnore(Object eqa) {
        OperationSupport eq = Synchronizer.mapper.convertValue(eqa, OperationSupport.class);
        PreparedStatement st = null;
        Boolean rs = null;
        try {
            String sql = "INSERT INTO " + TABLE_INFO + " " + "(" + id + "," + date + "," + id_supervisor + ","
                    + id_group_leader + "," + id_operator + "," + id_equipment + "," + id_location + ","
                    + id_destination + "," + cd_activity + "," + cd_shift + "," + hm_start + "," + hm_stop + ","
                    + sync_status + ")"
                    + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,1) ON CONFLICT(id) DO UPDATE SET sync_status=sync_status;";

            st = conn().prepareStatement(sql);
            st.setString(1, eq.getId());
            st.setLong(2, eq.getDate());
            st.setString(3, eq.getSupervisorId().getId());
            st.setString(4, eq.getGroupLeaderId().getId());
            st.setString(5, eq.getOperatorId().getId());
            st.setString(6, eq.getEquipmentId().getId());
            st.setString(7, eq.getLocationId().getId());
            st.setString(8, eq.getDestinationId().getId());
            st.setString(9, eq.getCdActivity().getCd());
            st.setString(10, eq.getCdShift().getCd());
            st.setDouble(11, eq.getHmStart() == null ? 0 : eq.getHmStart());
            st.setDouble(12, eq.getHmStop() == null ? 0 : eq.getHmStop());
            rs = st.execute();
            // System.out.println("---------------- insert ignore ");
            // System.out.println(rs);
            st.close();
            return eq;

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (rs && st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
        return null;
    }

    @Override
    public void deleteData(String id) {
        String sql = "delete from " + TABLE_INFO + " where id=" + id;
        try {
            Statement stmt = conn().createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object getFromServer(Integer page, Integer size) {
        if (!NetworkConnection.getServiceStatus()) {
            return null;
        }
        String equipment_id = MainApp.equipmentLocal.getId();
        Call<PagingResponse> call = APIClient.getInstance().getActualSupport(MainApp.requestHeader, page, size,
                equipment_id, "startDay");
        try {
            Response<PagingResponse> response = call.execute();
            return response.body();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Integer getRemoteCount() {
        if (!NetworkConnection.getServiceStatus()) {
            return 0;
        }
        Call<DefaultResponse> call = APIClient.getInstance().getCountActualSupportToday(MainApp.requestHeader,
                MainApp.equipmentLocal.getId(), MainApp.config("config.unit_type"));
        try {
            Response<DefaultResponse> response = call.execute();
            if (response.body().getOutput() != null) {
                return Double.valueOf(response.body().getOutput().toString()).intValue();
            } else {
                return 0;
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    @Override
    public List<Object> getFlaggedData() {
        try {
            String column = id + "," + date + "," + id_supervisor + "," + id_group_leader + "," + id_operator + ","
                    + id_equipment + "," + id_location + "," + id_destination + "," + cd_activity + "," + cd_shift + ","
                    + hm_start + "," + hm_stop;

            String sql = "SELECT " + column + " FROM " + TABLE_INFO + " WHERE sync_status=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, "0");
            ResultSet cursor = preparedStatement.executeQuery();
            List<Object> result = new ArrayList<Object>();

            while (cursor.next()) {
                OperationSupport actualDetail = new OperationSupport();

                actualDetail.setId(cursor.getString(1));
                actualDetail.setDate(cursor.getInt(2));

                UserSupport supervisor = new UserSupport();
                supervisor.setId(cursor.getString(3));
                actualDetail.setSupervisorId(supervisor);
                UserSupport groupLeader = new UserSupport();
                groupLeader.setId(cursor.getString(4));
                actualDetail.setGroupLeaderId(groupLeader);

                UserSupport operator = new UserSupport();
                operator.setId(cursor.getString(5));
                actualDetail.setOperatorId(operator);

                EquipmentSupport equipememt = new EquipmentSupport();
                equipememt.setId(cursor.getString(6));
                actualDetail.setEquipmentId(equipememt);

                LocationSupport location = new LocationSupport();
                location.setId(cursor.getString(7));
                actualDetail.setLocationId(location);

                LocationSupport destination = new LocationSupport();
                destination.setId(cursor.getString(8));
                actualDetail.setDestinationId(destination);

                CdActivitySupport operation = new CdActivitySupport();
                operation.setCd(cursor.getString(9));
                actualDetail.setCdActivity(operation);

                CdActivitySupport shift = new CdActivitySupport();
                shift.setCd(cursor.getString(10));
                actualDetail.setCdShift(shift);

                actualDetail.setHmStart(cursor.getDouble(11));
                actualDetail.setHmStop(cursor.getDouble(12));

                result.add(actualDetail);
            }
            cursor.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void postBulkData(List<Object> eq) {
        if (!NetworkConnection.getServiceStatus()) {
            return;
        }
        System.out.println("--- post synchronization ---");
        Call<ResponseModel> call = APIClient.getInstance().postOperationActualSupport(MainApp.requestHeader, eq);
        try {
            Response<ResponseModel> response = call.execute();
            System.out.println(response);
            System.out.println(response.body().getResponse());
            System.out.println(response.body().getMessage());
            if (response.body().getResponse().equals("success")) {
                System.out.println("post success");
                /**
                 * change flag (sync_status to 1)
                 */
                for (Object obj : eq) {
                    OperationSupport oqe = (OperationSupport) obj;
                    setSynchronized(oqe.getId());
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void setSynchronized(String id) {
        String sql = "update " + TABLE_INFO + " set sync_status=1 where id='" + id + "'";
        try {
            Statement stmt = conn().createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object getLocalOneById(String fieldId, String searchId) {
        try {
            String column = id + "," + date + "," + id_supervisor + "," + id_group_leader + "," + id_operator + ","
                    + id_equipment + "," + id_location + "," + id_destination + "," + cd_activity + "," + cd_shift + ","
                    + hm_start + "," + hm_stop;

            String sql = "SELECT " + column + " FROM " + TABLE_INFO + " WHERE " + fieldId + "=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, searchId);
            ResultSet cursor = preparedStatement.executeQuery();

            if (cursor.next()) {
                OperationSupport actualDetail = new OperationSupport();

                actualDetail.setId(cursor.getString(1));
                actualDetail.setDate(cursor.getInt(2));

                UserSupport supervisor = new UserSupport();
                supervisor.setId(cursor.getString(3));
                actualDetail.setSupervisorId(supervisor);
                UserSupport groupLeader = new UserSupport();
                groupLeader.setId(cursor.getString(4));
                actualDetail.setGroupLeaderId(groupLeader);

                UserSupport operator = new UserSupport();
                operator.setId(cursor.getString(5));
                actualDetail.setOperatorId(operator);

                EquipmentSupport equipememt = new EquipmentSupport();
                equipememt.setId(cursor.getString(6));
                actualDetail.setEquipmentId(equipememt);

                LocationSupport location = new LocationSupport();
                location.setId(cursor.getString(7));
                actualDetail.setLocationId(location);

                LocationSupport destination = new LocationSupport();
                destination.setId(cursor.getString(8));
                actualDetail.setDestinationId(destination);

                CdActivitySupport operation = new CdActivitySupport();
                operation.setCd(cursor.getString(9));
                actualDetail.setCdActivity(operation);

                CdActivitySupport shift = new CdActivitySupport();
                shift.setCd(cursor.getString(10));
                actualDetail.setCdShift(shift);

                actualDetail.setHmStart(cursor.getDouble(11));
                actualDetail.setHmStop(cursor.getDouble(12));

                cursor.close();
                return (Object) actualDetail;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
