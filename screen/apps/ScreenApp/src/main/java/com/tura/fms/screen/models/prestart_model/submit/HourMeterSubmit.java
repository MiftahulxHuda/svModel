package com.tura.fms.screen.models.prestart_model.submit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HourMeterSubmit {

    @SerializedName("hm")
    @Expose
    String hm;

    @SerializedName("equipment_id")
    @Expose
    String equipment_id;

    @SerializedName("date")
    @Expose
    Long date;

    @SerializedName("is_deleted")
    @Expose
    int is_deleted;

    public String getHm() {
        return hm;
    }

    public void setHm(String hm) {
        this.hm = hm;
    }

    public String getEquipment_id() {
        return equipment_id;
    }

    public void setEquipment_id(String equipment_id) {
        this.equipment_id = equipment_id;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }
}
