package com.tura.fms.screen.controllers;

import com.jfoenix.controls.JFXButton;
import com.tura.fms.screen.helpers.NavigationUI;
import com.tura.fms.screen.helpers.SessionFMS;
import com.tura.fms.screen.models.maintenance_model.MainConstModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.ResourceBundle;

import static org.javalite.app_config.AppConfig.p;

public class MechanicUnschedule implements Initializable {

    private NavigationUI navigationUI = new NavigationUI();

    @FXML
    private Button TitleBar;

    // SessionFMS sessionFMS;

    @FXML
    private JFXButton maintenance;

    @FXML
    private JFXButton task;

    @FXML
    private JFXButton maintenance_unschedule;

    @FXML
    private GridPane gridata;

    private ObservableList<MainConstModel> recordsData = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        TitleBar.setText(p("app.titleMechanicUnscheduleMaintenance"));

        // sessionFMS = new SessionFMS();
        // sessionFMS.touchFms();

        isi_model();
        tampilan();
        drawerAction();
    }

    public void tampilan() {

        int Col = 0;
        int Row = 0;

        for (int i = 0; i < recordsData.size(); i++) {

            HBox hbo1 = new HBox();
            hbo1.setAlignment(Pos.CENTER);
            hbo1.setPadding(new Insets(15, 10, 10, 15));
            hbo1.getStyleClass().add("box-shadow");
            hbo1.setPrefHeight(104);
            hbo1.setPrefWidth(561);

            Label label1 = new Label("HD-001");
            label1.setAlignment(Pos.TOP_LEFT);
            label1.setPrefHeight(15);
            label1.setPrefWidth(126);

            hbo1.getChildren().add(label1);

            VBox vbox1 = new VBox();
            vbox1.setAlignment(Pos.CENTER_LEFT);
            vbox1.setPrefHeight(91);
            vbox1.setPrefWidth(248);

            Label labelv1 = new Label("Maintenance List :");
            labelv1.setPrefHeight(24);
            labelv1.setPrefWidth(104);

            Label labellist1 = new Label("1. Problem A");
            Label labellist2 = new Label("1. Problem B");

            vbox1.getChildren().add(labelv1);
            vbox1.getChildren().add(labellist1);
            vbox1.getChildren().add(labellist2);

            hbo1.getChildren().add(vbox1);

            JFXButton buttonaksi = new JFXButton("Start Maintenance");
            buttonaksi.getStyleClass().add("button-primary-sm");
            buttonaksi.setButtonType(JFXButton.ButtonType.RAISED);
            buttonaksi.setPrefHeight(27);
            buttonaksi.setPrefWidth(151);

            hbo1.getChildren().add(buttonaksi);
            hbo1.setMargin(buttonaksi, new Insets(15, 10, 10, 15));

            gridata.add(hbo1, Col, Row);

            Col++;

            if (Col > 0) {
                // Reset Column
                Col = 0;
                // Next Row
                Row++;
            }
        }

    }

    public void isi_model() {

        recordsData.add(new MainConstModel("1", "Steering", ""));
        recordsData.add(new MainConstModel("1", "Steering", ""));
        recordsData.add(new MainConstModel("1", "Steering", ""));
        recordsData.add(new MainConstModel("1", "Steering", ""));
        recordsData.add(new MainConstModel("1", "Steering", ""));
        recordsData.add(new MainConstModel("1", "Steering", ""));
        recordsData.add(new MainConstModel("1", "Steering", ""));
        recordsData.add(new MainConstModel("1", "Steering", ""));

    }

    private void drawerAction() {

        TitleBar.setOnAction((ActionEvent evt) -> {
            // navigationUI.back_dashboard(sessionFMS.getSessionFms("sesi_level"),evt);

        });

        task.setOnAction((ActionEvent evt) -> {
            try {
                navigationUI.windowsTab(p("app.linkMechanicTask"), p("app.titleMechanicTash"), evt);
            } catch (Exception e) {

            }
        });

        maintenance.setOnAction((ActionEvent evt) -> {
            try {
                navigationUI.windowsTab(p("app.linkMechanicMaintenance"), p("app.titleMechanicMaintenance"), evt);
            } catch (Exception e) {
            }

        });

        maintenance_unschedule.setOnAction((ActionEvent evt) -> {
            try {
                navigationUI.windowsTab(p("app.linkMechanicUnscheduleMaintenance"),
                        p("app.titleMechanicUnscheduleMaintenance"), evt);
            } catch (Exception e) {

            }
        });

    }

}
