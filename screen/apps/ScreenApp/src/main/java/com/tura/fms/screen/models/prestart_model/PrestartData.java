package com.tura.fms.screen.models.prestart_model;

import java.util.HashMap;
import java.util.Map;

public class PrestartData {

    private String id;
    private String id_equipment;
    private String id_user;
    private Integer date;
    private String id_prestart;
    private String id_actual;
    private String id_actual_support;
    private String value;
    private Integer qty;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdEquipment() {
        return id_equipment;
    }

    public void setIdEquipment(String id_equipment) {
        this.id_equipment = id_equipment;
    }

    public String getIdUser() {
        return id_user;
    }

    public void setIdUser(String id_user) {
        this.id_user = id_user;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public String getIdPrestart() {
        return id_prestart;
    }

    public void setIdPrestart(String id_prestart) {
        this.id_prestart = id_prestart;
    }

    public String getIdActual() {
        return id_actual;
    }

    public void setIdActual(String id_actual) {
        this.id_actual = id_actual;
    }

    public String getIdActualSupport() {
        return id_actual_support;
    }

    public void setIdActualSupport(String id_actual_support) {
        this.id_actual_support = id_actual_support;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

}
