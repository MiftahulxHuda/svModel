package com.tura.fms.screen.database;

import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.dashboard_model.DashboardDetail;

import java.sql.*;

public class Dashboard_DB extends Operation {
    // Table Name
    private static final String TABLE_INFO = "Dashboard_Record";
    // Table Columns
    private static final String id = "id";
    private static final String status = "status";
    private static final String tanggal = "tanggal";
    private static final String shift = "shift";
    private static final String cycle = "cycle";
    private static final String distance = "distance";
    private static final String operation = "operation";
    private static final String enginehealth = "enginehealth";
    private static final String fuel = "fuel";
    private static final String enginetemp = "enginetemp";
    private static final String tanggal_update = "tanggal_update";
    private static final String loading_point = "loading_point";
    private static final String dumping_point = "dumping_point";
    private static final String unit_id = "unit_id";
    private static final String unit_type = "unit_type";
    private static final String hour_meter = "hour_meter";
    private static final String speed = "speed";
    private static final String payload = "payload";

    public Dashboard_DB() {
        super(Dashboard_DB.TABLE_INFO, ConstantValues.DATA_DASHBOARD);
    }

    // *********** Returns the Query to Create TABLE_USER_INFO ********//
    public void createTableDashboard() {
        try {
            DatabaseMetaData dbm = conn().getMetaData();
            ResultSet rs = dbm.getTables(null, null, TABLE_INFO, null);
            Statement stmt = null;

            if (rs.next()) {
                System.out.println("Table Dashboard exists");
                // conn().close();
                // stmt.close();
            } else {
                stmt = conn().createStatement();
                stmt.executeUpdate(createTable());
                stmt.close();
                // conn().close();
                System.out.println("Table created");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static String createTable() {

        return "CREATE TABLE " + TABLE_INFO + "(" + id + " INTEGER PRIMARY KEY AUTOINCREMENT," + status + " TEXT,"
                + tanggal + " TEXT," + shift + " TEXT," + cycle + " TEXT," + distance + " TEXT," + operation + " TEXT,"
                + enginehealth + " TEXT," + fuel + " TEXT," + enginetemp + " TEXT," + tanggal_update + " TEXT,"
                + loading_point + " TEXT," + dumping_point + " TEXT," + unit_id + " TEXT," + unit_type + " TEXT,"
                + hour_meter + " TEXT," + payload + " TEXT," + speed + " TEXT" + ");";

    }

    // *********** Insert New User Data ********//

    public void insertData(DashboardDetail dashboardDetail) {

        PreparedStatement st = null;
        Boolean rs = null;

        try {
            String sql;
            if (getExisting(dashboardDetail.getUnitType(), dashboardDetail.getUnitId()) == 0) {

                sql = "INSERT INTO " + TABLE_INFO + " " + "(" + status + "," + tanggal + "," + shift + "," + cycle + ","
                        + distance + "," + operation + "," + enginehealth + "," + fuel + "," + enginetemp + ","
                        + tanggal_update + "," + loading_point + "," + dumping_point + "," + unit_id + "," + unit_type
                        + "," + hour_meter + "," + payload + "," + speed + "" + ")" + "VALUES(" + "?," + "?," + "?,"
                        + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?," + "?"
                        + ");";

            } else {

                sql = "UPDATE  " + TABLE_INFO + " SET " + "" + status + "=?," + tanggal + "=?," + shift + "=?," + cycle
                        + "=?," + distance + "=?," + operation + "=?," + enginehealth + "=?," + fuel + "=?,"
                        + enginetemp + "=?," + tanggal_update + "=?," + loading_point + "=?," + dumping_point + "=?,"
                        + unit_id + "=?," + unit_type + "=?," + hour_meter + "=?," + payload + "=?," + speed + "=?"
                        + " WHERE " + unit_id + "=? and " + unit_type + "=? ";

            }

            String tanggal_update = Utilities.getDateTime();

            st = conn().prepareStatement(sql);

            st.setString(1, dashboardDetail.getStatus());
            st.setString(2, dashboardDetail.getDate());
            st.setString(3, dashboardDetail.getShift());
            st.setDouble(4, dashboardDetail.getCycle());
            st.setDouble(5, dashboardDetail.getDistance());
            st.setString(6, dashboardDetail.getOperation());
            st.setDouble(7, dashboardDetail.getEngine());
            st.setDouble(8, dashboardDetail.getFuel());
            st.setDouble(9, dashboardDetail.getTemperatur());
            st.setString(10, tanggal_update);
            st.setString(11, dashboardDetail.getLoadingPoint());
            st.setString(12, dashboardDetail.getDumpingPoint());
            st.setString(13, dashboardDetail.getUnitId());
            st.setString(14, dashboardDetail.getUnitType());
            st.setDouble(15, dashboardDetail.getHourMeter());
            st.setDouble(16, dashboardDetail.getPayload());
            st.setDouble(17, dashboardDetail.getSpeed());

            if (getExisting(dashboardDetail.getUnitType(), dashboardDetail.getUnitId()) > 0) {
                st.setString(18, dashboardDetail.getUnitId());
                st.setString(19, dashboardDetail.getUnitType());
            }

            rs = st.execute();
            // conn().close();
            // st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {

            if (rs) {
                try {
                    st.close();
                    // conn().close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public int getExisting(String unit_type_param, String unit_id_param) {
        try {
            String sql = "SELECT count(*) as total FROM " + TABLE_INFO + " WHERE " + unit_type + "=? and " + unit_id
                    + "=?";

            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, unit_type_param);
            preparedStatement.setString(2, unit_id_param);
            ResultSet cursor = preparedStatement.executeQuery();

            int jumlah;

            if (cursor.next()) {
                jumlah = cursor.getInt("total");
            } else {
                jumlah = 0;
            }
            cursor.close();
            // conn().close();
            return jumlah;
        } catch (Exception e) {

            return 0;
        }

    }

    public DashboardDetail getData(String unit_id_param, String unit_type_param) {
        try {
            ResultSet cursor = null;

            String sql = "SELECT * FROM " + TABLE_INFO + " WHERE " + unit_id + "=? and " + unit_type + "=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, unit_id_param);
            preparedStatement.setString(2, unit_type_param);

            cursor = preparedStatement.executeQuery();

            DashboardDetail dashboardDetail = new DashboardDetail();

            while (cursor.next()) {

                dashboardDetail.setStatus(cursor.getString(2));

                dashboardDetail.setDate(cursor.getString(3));

                dashboardDetail.setShift(cursor.getString(4));

                dashboardDetail.setCycle(cursor.getDouble(5));
                dashboardDetail.setDistance(cursor.getDouble(6));

                dashboardDetail.setOperation(cursor.getString(7));

                dashboardDetail.setEngine(cursor.getDouble(8));

                dashboardDetail.setFuel(cursor.getDouble(9));
                dashboardDetail.setTemperatur(cursor.getDouble(10));

                dashboardDetail.setLoadingPoint(cursor.getString(12));
                dashboardDetail.setDumpingPoint(cursor.getString(13));
                dashboardDetail.setUnitId(cursor.getString(14));
                dashboardDetail.setUnitType(cursor.getString(15));
                dashboardDetail.setHourMeter(cursor.getDouble(16));
                dashboardDetail.setPayload(cursor.getDouble(17));

                dashboardDetail.setSpeed(cursor.getDouble(18));

            }

            // close the Database
            // conn().close();
            return dashboardDetail;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

}
