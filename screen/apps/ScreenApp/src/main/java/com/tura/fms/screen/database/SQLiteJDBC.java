package com.tura.fms.screen.database;

import com.tura.fms.screen.MainApp;
import org.sqlite.SQLiteConfig;

import java.sql.*;

public class SQLiteJDBC {

    public static Connection connection = null;

    public static Connection openDB() {
        if (SQLiteJDBC.connection == null) {
            try {
                Class.forName("org.sqlite.JDBC");
                String database = System.getProperty("user.dir") + System.getProperty("file.separator")
                        + MainApp.config("config.sqlite_uri");
                SQLiteConfig config = new SQLiteConfig();
                System.out.println("using database " + database);
                SQLiteJDBC.connection = DriverManager.getConnection("jdbc:sqlite:" + database, config.toProperties());
                SQLiteJDBC.connection.setAutoCommit(true);
            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
                System.exit(0);
            }
            return SQLiteJDBC.connection;
        } else {
            return SQLiteJDBC.connection;
        }
    }

}
