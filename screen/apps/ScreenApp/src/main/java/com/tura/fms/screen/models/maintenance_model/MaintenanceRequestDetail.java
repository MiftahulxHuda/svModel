package com.tura.fms.screen.models.maintenance_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MaintenanceRequestDetail {

    @SerializedName("id")
    @Expose
    String id;

    @SerializedName("operation_actual_id")
    @Expose
    String operation_actual_id;

    @SerializedName("actual_support_id")
    @Expose
    String actual_support_id;

    @SerializedName("problem_id")
    @Expose
    String problem_id;
    @SerializedName("equipment_id")
    @Expose
    String equipment_id;

    @SerializedName("operator_id")
    @Expose
    String operator_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOperation_actual_id() {
        return operation_actual_id;
    }

    public void setOperation_actual_id(String operation_actual_id) {
        this.operation_actual_id = operation_actual_id;
    }

    public String getActual_support_id() {
        return actual_support_id;
    }

    public void setActual_support_id(String actual_support_id) {
        this.actual_support_id = actual_support_id;
    }

    public String getProblem_id() {
        return problem_id;
    }

    public void setProblem_id(String problem_id) {
        this.problem_id = problem_id;
    }

    public String getEquipment_id() {
        return equipment_id;
    }

    public void setEquipment_id(String equipment_id) {
        this.equipment_id = equipment_id;
    }

    public String getOperator_id() {
        return operator_id;
    }

    public void setOperator_id(String operator_id) {
        this.operator_id = operator_id;
    }
}
