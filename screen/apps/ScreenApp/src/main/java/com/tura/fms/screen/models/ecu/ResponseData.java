package com.tura.fms.screen.models.ecu;


import java.time.LocalDateTime;

public class ResponseData {

    public LocalDateTime date;

    public String unit;

    public Double numValue;

    public String strValue;

    public ResponseData(LocalDateTime date, String unit, Double value) {
        this.date = date;
        this.unit = unit;
        this.numValue = value;
    }

    public ResponseData(LocalDateTime date, String unit, String value) {
        this.date = date;
        this.unit = unit;
        this.strValue = value;
    }
}
