package com.tura.fms.screen.helpers;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
// import org.apache.shiro.config.IniSecurityManagerFactory;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.Factory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SessionFMS {

    Factory<SecurityManager> factory;
    SecurityManager securityManager;
    public static Subject currentUser;
    Session session;

    private static final transient Logger log = LoggerFactory.getLogger(SessionFMS.class);

    public SessionFMS() {
        // factory = new IniSecurityManagerFactory("classpath:shiro.ini");
        securityManager = factory.getInstance();
        SecurityUtils.setSecurityManager(securityManager);
        currentUser = SecurityUtils.getSubject();
        session = currentUser.getSession();
    }

    public void setSessionFms(String kunci, String nilai) {
        session.setAttribute(kunci, nilai);
    }

    public void touchFmsX() {
        try {
            if (currentUser.isAuthenticated()) {
                session.touch();
            }
        } catch (Exception e) {

        }
    }

    public String getSessionFmsX(String kunci) {
        try {
            if (currentUser.isAuthenticated()) {
                return (String) session.getAttribute(kunci);
            } else {
                return "";
            }
        } catch (Exception e) {
            return "";
        }

    }

    public void logoutFms() {
        try {
            if (currentUser.isAuthenticated()) {
                currentUser.logout();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void logout() {
        try {
            if (currentUser.isAuthenticated()) {
                currentUser.logout();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Boolean cekSession() {
        if (currentUser.isAuthenticated()) {
            return true;
        } else {
            return false;
        }
    }

    public void proseLogin(String username, String password) {
        if (!currentUser.isAuthenticated()) {
            UsernamePasswordToken token = new UsernamePasswordToken(username, password);
            token.setRememberMe(true);

            try {
                logoutFms();
                currentUser.login(token);
                // session.setTimeout(ConstantValues.SESSIONTIMEOUT);
            } catch (UnknownAccountException uae) {
                log.info("There is no user with username of " + token.getPrincipal());
            } catch (IncorrectCredentialsException ice) {
                log.info("Password for account " + token.getPrincipal() + " was incorrect!");
            } catch (LockedAccountException lae) {
                log.info("The account for username " + token.getPrincipal() + " is locked.  "
                        + "Please contact your administrator to unlock it.");
            }
            // ... catch more exceptions here (maybe custom ones specific to your
            // application?
            catch (AuthenticationException ae) {
                log.info("The account for username " + ae.getMessage());
            }
        }
    }

}
