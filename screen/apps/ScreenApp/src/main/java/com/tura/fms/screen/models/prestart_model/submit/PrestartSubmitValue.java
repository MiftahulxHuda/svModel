package com.tura.fms.screen.models.prestart_model.submit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PrestartSubmitValue {

    @SerializedName("id")
    @Expose
    String id;

    @SerializedName("name")
    @Expose
    String name;

    @SerializedName("alias")
    @Expose
    String alias;

    @SerializedName("cd_type_equipment")
    @Expose
    String cd_type_equipment;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getCd_type_equipment() {
        return cd_type_equipment;
    }

    public void setCd_type_equipment(String cd_type_equipment) {
        this.cd_type_equipment = cd_type_equipment;
    }
}
