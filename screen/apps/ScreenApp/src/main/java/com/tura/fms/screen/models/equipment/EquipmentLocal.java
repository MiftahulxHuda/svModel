package com.tura.fms.screen.models.equipment;

import java.util.HashMap;
import java.util.Map;

public class EquipmentLocal {
    private String id;
    private String id_org;
    private String id_operator;
    private String cd_operation;
    private String cd_equipment;
    private String name;
    private Integer capacity_real;
    private Integer capacity_labs;
    private Integer enum_availability;
    private Integer is_deleted;
    private Integer date_submitted;
    private Integer date_modified;
    private Integer load_weight;
    private Integer empty_weight;
    private Integer capacity_labs_2;
    private Integer capacity_labs_3;
    private Integer hour_meter;
    private String _class;
    private String code;
    private Json json;
    private String id_location;
    private Integer hm_start;
    private Integer hm_now;
    private Integer hm_date;
    private String id_equipment_category;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_org() {
        return id_org;
    }

    public void setId_org(String id) {
        this.id_org = id;
    }

    public String getId_operator() {
        return id_operator;
    }

    public void setId_operator(String id_operator) {
        this.id_operator = id_operator;
    }

    public String getCd_operation() {
        return cd_operation;
    }

    public void setCd_operation(String cd_operation) {
        this.cd_operation = cd_operation;
    }

    public String getCd_equipment() {
        return cd_equipment;
    }

    public void setCd_equipment(String cd_equipment) {
        this.cd_equipment = cd_equipment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCapacity_real() {
        return capacity_real;
    }

    public void setCapacity_real(Integer capacity_real) {
        this.capacity_real = capacity_real;
    }

    public Integer getCapacity_labs() {
        return capacity_labs;
    }

    public void setCapacity_labs(Integer capacity_labs) {
        this.capacity_labs = capacity_labs;
    }

    public Integer getEnum_availability() {
        return enum_availability;
    }

    public void setEnum_availability(Integer enum_availability) {
        this.enum_availability = enum_availability;
    }

    public Integer getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(Integer is_deleted) {
        this.is_deleted = is_deleted;
    }

    public Integer getDate_submitted() {
        return date_submitted;
    }

    public void setDate_submitted(Integer date_submitted) {
        this.date_submitted = date_submitted;
    }

    public Integer getDate_modified() {
        return date_modified;
    }

    public void setDate_modified(Integer date_modified) {
        this.date_modified = date_modified;
    }

    public Integer getLoad_weight() {
        return load_weight;
    }

    public void setLoad_weight(Integer load_weight) {
        this.load_weight = load_weight;
    }

    public Integer getEmpty_weight() {
        return empty_weight;
    }

    public void setEmpty_weight(Integer empty_weight) {
        this.empty_weight = empty_weight;
    }

    public Integer getCapacity_labs_2() {
        return capacity_labs_2;
    }

    public void setCapacity_labs_2(Integer capacity_labs_2) {
        this.capacity_labs_2 = capacity_labs_2;
    }

    public Integer getCapacity_labs_3() {
        return capacity_labs_3;
    }

    public void setCapacity_labs_3(Integer capacity_labs_3) {
        this.capacity_labs_3 = capacity_labs_3;
    }

    public Integer getHour_meter() {
        return hour_meter;
    }

    public void setHour_meter(Integer hour_meter) {
        this.hour_meter = hour_meter;
    }

    public String getClass_() {
        return _class;
    }

    public void setClass_(String _class) {
        this._class = _class;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Json getJson() {
        return json;
    }

    public void setJson(Json json) {
        this.json = json;
    }

    public String getId_location() {
        return id_location;
    }

    public void setId_location(String id_location) {
        this.id_location = id_location;
    }

    public Integer getHm_start() {
        return hm_start;
    }

    public void setHm_start(Integer hm_start) {
        this.hm_start = hm_start;
    }

    public Integer getHm_now() {
        return hm_now;
    }

    public void setHm_now(Integer hm_now) {
        this.hm_now = hm_now;
    }

    public Integer getHm_date() {
        return hm_date;
    }

    public void setHm_date(Integer hm_date) {
        this.hm_date = hm_date;
    }

    public String getEquipmentCategory() {
        return id_equipment_category;
    }

    public void setEquipmentCategory(String cat) {
        this.id_equipment_category = cat;
    }

}

class Json {

    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
