package com.tura.fms.screen.presenter;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.constanst.IKafkaConstant;
import com.tura.fms.screen.controllers.loader.DashboardLoaderController;
import com.tura.fms.screen.database.Synchronizer;
import com.tura.fms.screen.helpers.NavigationUI;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.kafka.ConsumerCreator;
import com.tura.fms.screen.models.actual_model.ActualDetail;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.json.JSONException;
import org.json.JSONObject;

import javafx.application.Platform;
import javafx.scene.control.Button;

import static org.javalite.app_config.AppConfig.p;

@SuppressWarnings("unchecked")
public class KafkaConsumerPresenter {

    private Consumer<String, String> consumer = null;
    // private boolean run = true;
    private TimerTask task;
    private Timer timer;

    public KafkaConsumerPresenter() {
        consumer = ConsumerCreator.createConsumer();
        prepareConsumer();
    }

    public void prepareConsumer() {
        task = new TimerTask() {
            @Override
            public void run() {

                if (NetworkConnection.getKafkaStatus()) {

                    readKafka();

                }

                // System.out.println("----------------------- kafka consumer heartbeat
                // --------------------");
            }
        };
        timer = new Timer();
        timer.schedule(task, 0, Integer.parseInt(MainApp.config("config.kafka_poll_interval")));
    }

    public void readKafka() {
        ConsumerRecords<String, String> records = consumer.poll(5);
        for (ConsumerRecord<String, String> record : records) {
            if (Utilities.authenticateToken(record.key())) {

                try {
                    System.out.println("********************************************************");
                    System.out.println("****************** Socket Accepted *********************");
                    System.out.println("********************************************************");
                    System.out.println(">>>>>>>>>>>> " + record.topic());
                    System.out.println(">>>>>>>>>>>> " + record.value());

                    if (record.topic().equals(IKafkaConstant.TOPIC_SUBSCRIBE_CYCLE)) {
                        handleCycle(Utilities.decodedJWT, parseJSONMap(record.value()));
                    }
                    if (record.topic().equals(IKafkaConstant.TOPIC_SUBSCRIBE_DATA)) {
                        handleData(Utilities.decodedJWT, parseJSONMap(record.value()));
                    }
                    if (record.topic().equals(IKafkaConstant.TOPIC_SUBSCRIBE_NOTIFICATION)) {
                        handleNotification(Utilities.decodedJWT, parseJSONMap(record.value()));
                    }
                    if (record.topic().equals(IKafkaConstant.TOPIC_SUBSCRIBE_COMMAND)) {
                        handleCommand(Utilities.decodedJWT, parseJSONMap(record.value()));
                    }
                    if (record.topic().equals(IKafkaConstant.TOPIC_SUBSCRIBE_DISTANCE)) {
                        handleDistance(Utilities.decodedJWT, parseJSONMap(record.value()));
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            } else {

                System.out.println("???????????????????? key failed ?????????????????????");
                System.out.println(record.topic());
                System.out.println(record.value());

            }
        }
        // consumer.commitAsync();
    }

    public void doRun() {
        prepareConsumer();
    }

    public void stop() {
        timer.cancel();
    }

    public Map<String, Object> parseJSONMap(String jsonString) {
        try {
            Map<String, Object> map = APIClient.mapper.readValue(jsonString, Map.class);
            return map;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Object> parseJSONList(String jsonString) {
        try {
            List<Object> map = APIClient.mapper.readValue(jsonString, List.class);
            return map;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<String> parseListString(String strString) {
        String[] tmp = strString.replace("[", "").replace("]", "").replace(" ", "").split(",");
        List<String> result = Arrays.asList(tmp);
        return result;
    }

    public void handleCycle(DecodedJWT dj, Map<String, Object> value) {
        final String date = value.get("date").toString();
        final String type = value.get("type").toString();
        final String sender = value.get("sender").toString();
        final String sender_name = (String) value.get("sender_name");
        final String destination = value.get("destination").toString();
        final String message = value.get("message").toString();
        final Integer bucket_count = value.get("bucket_count") != null
                ? Integer.parseInt(value.get("bucket_count").toString())
                : 0;

        final String currentUnitType = MainApp.config("config.unit_type");
        String currentUnitId = null;

        if (MainApp.equipmentLocal != null) {
            currentUnitId = MainApp.equipmentLocal.getId();
        }

        // cek apakah mesej untuk screen ini
        if (destination.equals(currentUnitId) || destination.equals(ConstantValues.KAFKA_DESTINATION_ALL)) {

            // cek apakah screen hauler atau loader
            if (currentUnitType.equals(ConstantValues.EQUIPMENT_HAULER)) {

                if (message.equals(ConstantValues.CYCLE_FIRST_BUCKET)) {

                    MainApp.haulerState = ConstantValues.HAULER_STATE_LOADING;

                    // send api actual detail
                    NavigationUI.setLabelText("First bucket");

                    Utilities.handleFirstBucketOnHauler();

                } else if (message.equals(ConstantValues.CYCLE_FULL_BUCKET)) {

                    MainApp.haulerState = ConstantValues.HAULER_STATE_FULL_TRAVEL;

                    // send api actual detail
                    NavigationUI.setLabelText("Full bucket");

                    MainApp.travel = ConstantValues.TRAVEL_FULL;

                    MainApp.bucketCount = bucket_count;

                    try {
                        // set start distance
                        JSONObject distance = MainApp.device.getDistance();
                        MainApp.distance = distance.getJSONObject("distance").getDouble("value");

                        // set tonase
                        JSONObject payload = MainApp.device.getPayload();
                        Double tonase = payload.getJSONObject("payload").getDouble("value");
                        // Double density =
                        // Double.parseDouble(MainApp.actualInfo.getCd_material().getDensity());
                        MainApp.tonnage = tonase;

                        Utilities.handleFullBucketOnHauler();

                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }

                }

            } else if (currentUnitType.equals(ConstantValues.EQUIPMENT_LOADER)) {

                if (message.equals(ConstantValues.CYCLE_ARRIVE_LOADING)) {

                    // send api actual detail
                    Utilities.setNotificationText(sender_name + " arrive at loading");

                    for (Button btn : DashboardLoaderController.buttonHauler) {
                        if (btn.getId().equals(sender)) {
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    btn.setStyle("-fx-background-color: #966e35;");
                                }
                            });
                        }
                    }

                }

                if (message.equals(ConstantValues.CYCLE_FINISH_DUMP)) {

                    System.out.println("----- get loader cycle");
                    Utilities.getActualCycleLoader(MainApp.equipmentLocal.getId(), null);

                }
            }

        } else {

            System.out.println(">>>>>>>>>>>>> socket not accepted, different destination");
            System.out.println(destination);
            System.out.println(currentUnitId);

        }
    }

    public void handleData(DecodedJWT dj, Map<String, Object> value) {
        final String date = value.get("date").toString();
        final String state = value.get("state").toString();
        final String sender = value.get("sender").toString();
        final Object sender_name = value.get("sender_name").toString();
        final String destination = value.get("destination").toString();
        final Object type = value.get("type").toString();
        final Object data = value.get("data");
        String currentUnitId = null;

        if (MainApp.equipmentLocal != null) {
            currentUnitId = MainApp.equipmentLocal.getId();
        }

        // cek apakah socket memang untuk unit ini
        if (destination.equals(currentUnitId) || destination.equals(ConstantValues.KAFKA_DESTINATION_ALL)) {

            System.out.println(">>>>>>>>>>>>>>> message accepted");
            if (state.equals(ConstantValues.KAFKA_DATA_STATE_REQUEST)) {

                if (type.equals(ConstantValues.KAFKA_DATA_TYPE_PAYLOAD)) {

                    JSONObject dataPayload = MainApp.device.getPayload();
                    try {
                        Double payload = dataPayload.getJSONObject("payload").getDouble("value");
                        Utilities.pushResponseData(ConstantValues.KAFKA_DATA_TYPE_PAYLOAD, sender, payload.toString());
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }

                if (type.equals(ConstantValues.KAFKA_DATA_TYPE_SYNC)) {
                    if (data != null) {
                        Synchronizer.resetLocalTableForSync(data.toString());
                    }
                }

            } else if (state.equals(ConstantValues.KAFKA_DATA_STATE_RESPONSE)) {

                if (type.equals(ConstantValues.KAFKA_DATA_TYPE_PAYLOAD)) {
                    if (data != null) {
                        if (Utilities.isLoader()) {
                            Integer capacityReal = 0;
                            if (MainApp.activeHauler != null) {
                                capacityReal = MainApp.haulerList.get(MainApp.activeHauler).getId_hauler()
                                        .getCapacity_real();
                            }

                            MainApp.activeHaulerPayload = Double.parseDouble(data.toString());
                            MainApp.tonnage = MainApp.activeHaulerPayload - capacityReal;
                            MainApp.dashboardInfo.setTonnagePayload(MainApp.tonnage);

                            Utilities.setNotificationText("Accept payload from " + sender_name + " : "
                                    + MainApp.activeHaulerPayload + " " + MainApp.satuan);
                            Utilities.setNotificationText(
                                    "Payload " + sender_name + " : " + MainApp.tonnage + " " + MainApp.satuan);
                        }
                    }
                }

                if (type.equals(ConstantValues.KAFKA_DATA_TYPE_HM)) {
                    if (data != null) {
                        Double hm = Double.parseDouble(data.toString());
                    }
                }

                if (type.equals(ConstantValues.KAFKA_DATA_TYPE_TIMBANGAN)) {
                    if (data != null) {
                        MainApp.beratTimbangan = Double.parseDouble(data.toString());

                        Utilities.handleTimbanganOnHauler();

                        Platform.runLater(() -> {
                            Utilities.showMessageDialog("Information", "Berat timbangan",
                                    MainApp.beratTimbangan.toString() + " Ton");
                        });
                    }
                }

            }

        } else {

            System.out.println(">>>>>>>>>>>>> socket not accepted, different destination");
            System.out.println(destination);
            System.out.println(currentUnitId);

        }
    }

    public void handleNotification(DecodedJWT dj, Map<String, Object> value) {
        final String date = value.get("date").toString();
        final String type = value.get("type").toString();
        final Map<String, Object> sender = Utilities.mapObject(value.get("sender"));
        final Map<String, Object> destination = Utilities.mapObject(value.get("destination"));
        final String va = value.get("value").toString();
        String currentUnitId = null;

        if (MainApp.equipmentLocal != null) {
            currentUnitId = MainApp.equipmentLocal.getId();
        }

        final String to = destination.get("to").toString();
        final List<String> equipment = Utilities.mapListString(destination.get("equipment"));

        System.out.println(equipment);
        System.out.println(currentUnitId);

        // cek apakah socket memang untuk unit ini
        if (to.equals("screen")
                && (equipment.contains(ConstantValues.KAFKA_DESTINATION_ALL) || equipment.contains(currentUnitId))) {

            System.out.println(">>>>>>>>>>>>>>> message accepted");
            System.out.println(va);
            if (type.equals(ConstantValues.NOTIF_TYPE_STRING)) {
                // NavigationUI.setLabelText(va);
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        NavigationUI.addLogActivity("", "Message [from " + sender.get("name").toString() + "] : " + va);
                    }
                });

            } else {

                Utilities.playSound(va);

            }

        } else {

            System.out.println(">>>>>>>>>>>>> socket not accepted, different destination");
            System.out.println(sender);
            System.out.println(to);
            System.out.println(destination);
            System.out.println(currentUnitId);

        }
    }

    public void handleCommand(DecodedJWT dj, Map<String, Object> value) {
        final String date = value.get("date").toString();
        final String command = value.get("command").toString();
        final String type = value.get("type").toString();
        final String reference = value.get("reference").toString();
        final List<String> destination = this.parseListString(value.get("destination").toString());

        String currentUnitId = null;

        if (MainApp.equipmentLocal != null) {
            currentUnitId = MainApp.equipmentLocal.getId();
        }

        // cek apakah socket memang untuk unit ini
        if (destination != null) {

            System.out.println(">>>>>>>>>>>>>>> message accepted");
            System.out.println(destination);
            System.out.println(currentUnitId);

            if (destination.contains(currentUnitId) || destination.contains(ConstantValues.KAFKA_DESTINATION_ALL)) {

                System.out.println("---- destination match ----");

                if (command.equals(ConstantValues.KAFKA_CMD_DATA_CHANGE)) {

                    ActualDetail newActual = null;

                    if (reference != null) {
                        newActual = Utilities.getHaulerActualById(reference);
                    }

                    if (Utilities.isHauler()) {
                        if (type.equals(ConstantValues.KAFKA_CMD_DATA_CHANGE_HAULER_LOADER)) {
                            String loader = newActual != null ? newActual.getId_loader().getName() : "-";

                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    NavigationUI.addLogActivity("", "Dispatcer memindahkan unit ini ke " + loader
                                            + ", silakan ikuti arahan dispatcher");
                                }
                            });

                            if (MainApp.haulerState.equals(ConstantValues.HAULER_STATE_EMPTY_TRAVEL)) {
                                Utilities.getOperationActual();

                                Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        NavigationUI.addLogActivity("", "Perubahan actual sudah dilakukan");
                                    }
                                });

                            } else {
                                /** actual terbaru akan diambil pada event finish dump */
                                MainApp.needGetHaulerActual = true;
                            }
                        }
                        if (type.equals(ConstantValues.KAFKA_CMD_DATA_CHANGE_HAULER_LOCATION)) {
                            String location = newActual != null ? newActual.getId_location().getName() : "-";

                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    NavigationUI.addLogActivity("", "Dispatcer mengubah lokasi loading unit ini ke "
                                            + location + ", silakan ikuti arahan dispatcher");
                                }
                            });

                            if (MainApp.haulerState.equals(ConstantValues.HAULER_STATE_EMPTY_TRAVEL)) {
                                Utilities.getOperationActual();

                                Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        NavigationUI.addLogActivity("", "Perubahan actual sudah dilakukan");
                                    }
                                });

                            } else {
                                /** actual terbaru akan diambil pada event finish dump */
                                MainApp.needGetHaulerActual = true;
                            }
                        }
                        if (type.equals(ConstantValues.KAFKA_CMD_DATA_CHANGE_HAULER_DESTINATION)) {
                            String dest = newActual != null ? newActual.getId_destination().getName() : "-";

                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    NavigationUI.addLogActivity("", "Dispatcer mengubah lokasi dumping unit ini ke "
                                            + dest + ", silakan ikuti arahan dispatcher");
                                }
                            });

                            if (MainApp.haulerState.equals(ConstantValues.HAULER_STATE_EMPTY_TRAVEL)) {
                                Utilities.getOperationActual();

                                Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        NavigationUI.addLogActivity("", "Perubahan actual sudah dilakukan");
                                    }
                                });

                            } else {
                                /** actual terbaru akan diambil pada event finish dump */
                                MainApp.needGetHaulerActual = true;
                            }
                        }
                        if (type.equals(ConstantValues.KAFKA_CMD_DATA_CHANGE_HAULER_GROUPLEADER)) {
                            String gl = newActual != null ? newActual.getId_group_leader().getName() : "-";

                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    NavigationUI.addLogActivity("", "Dispatcer mengganti group leader menjadi " + gl);
                                }
                            });

                            Utilities.getOperationActual();

                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    NavigationUI.addLogActivity("", "Perubahan actual sudah dilakukan");
                                }
                            });

                        }
                        if (type.equals(ConstantValues.KAFKA_CMD_DATA_CHANGE_HAULER_SUPERVISOR)) {
                            String sv = newActual != null ? newActual.getId_supervisor().getName() : "-";

                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    NavigationUI.addLogActivity("", "Dispatcer mengganti supervisor menjadi " + sv);
                                }
                            });

                            Utilities.getOperationActual();

                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    NavigationUI.addLogActivity("", "Perubahan actual sudah dilakukan");
                                }
                            });

                        }
                        if (type.equals(ConstantValues.KAFKA_CMD_DATA_CHANGE_HAULER_MATERIAL)) {
                            String material = newActual != null ? newActual.getCd_material().getName() : "-";

                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    NavigationUI.addLogActivity("", "Dispatcer mengubah material unit ini menjadi "
                                            + material + ", silakan ikuti arahan dispatcher");
                                }
                            });

                            if (MainApp.haulerState.equals(ConstantValues.HAULER_STATE_EMPTY_TRAVEL)) {
                                Utilities.getOperationActual();

                                Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        NavigationUI.addLogActivity("", "Perubahan actual sudah dilakukan");
                                    }
                                });

                            } else {
                                /** actual terbaru akan diambil pada event finish dump */
                                MainApp.needGetHaulerActual = true;
                            }
                        }
                        if (type.equals(ConstantValues.KAFKA_CMD_DATA_CHANGE_HAULER_ACTIVITY)) {
                            String operation = newActual != null ? newActual.getCd_operation().getName() : "-";

                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    NavigationUI.addLogActivity("", "Dispatcer mengubah operasi unit ini ke menjadi "
                                            + operation + ", silakan ikuti arahan dispatcher");
                                }
                            });

                            if (MainApp.haulerState.equals(ConstantValues.HAULER_STATE_EMPTY_TRAVEL)) {
                                Utilities.getOperationActual();

                                Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        NavigationUI.addLogActivity("", "Perubahan actual sudah dilakukan");
                                    }
                                });

                            } else {
                                /** actual terbaru akan diambil pada event finish dump */
                                MainApp.needGetHaulerActual = true;
                            }
                        }
                        if (type.equals(ConstantValues.KAFKA_CMD_DATA_CHANGE_HAULER_OPERATOR)) {

                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    NavigationUI.addLogActivity("",
                                            "Dispatcer mengubah operator unit ini, silakan ikuti arahan dispatcher");
                                }
                            });

                        }
                    }

                    if (Utilities.isLoader()) {
                        if (type.equals(ConstantValues.KAFKA_CMD_DATA_CHANGE_LOADER_LOCATION)) {
                            NavigationUI.addLogActivity("",
                                    "Dispatcer mengubah lokasi loading ke ?, silakan ikuti arahan dispatcher");
                        }
                        if (type.equals(ConstantValues.KAFKA_CMD_DATA_CHANGE_LOADER_DESTINATION)) {
                            NavigationUI.addLogActivity("",
                                    "Dispatcer mengubah lokasi dumping ke ?, silakan ikuti arahan dispatcher");
                        }
                        if (type.equals(ConstantValues.KAFKA_CMD_DATA_CHANGE_LOADER_GROUPLEADER)) {
                            NavigationUI.addLogActivity("", "Dispatcer mengganti group leader menjadi ?");
                        }
                        if (type.equals(ConstantValues.KAFKA_CMD_DATA_CHANGE_LOADER_SUPERVISOR)) {
                            NavigationUI.addLogActivity("", "Dispatcer mengganti supervisor menjadi ?");
                        }
                        if (type.equals(ConstantValues.KAFKA_CMD_DATA_CHANGE_LOADER_MATERIAL)) {
                            NavigationUI.addLogActivity("",
                                    "Dispatcer mengubah material menjadi ?, silakan ikuti arahan dispatcher");
                        }
                        if (type.equals(ConstantValues.KAFKA_CMD_DATA_CHANGE_LOADER_ACTIVITY)) {
                            NavigationUI.addLogActivity("",
                                    "Dispatcer mengubah operasi unit ini ke menjadi ?, silakan ikuti arahan dispatcher");
                        }
                        if (type.equals(ConstantValues.KAFKA_CMD_DATA_CHANGE_LOADER_OPERATOR)) {
                            NavigationUI.addLogActivity("",
                                    "Dispatcer mengubah operator unit ini, silakan ikuti arahan dispatcher");
                        }
                    }

                    if (!Utilities.isLoaderHauler()) {
                        if (type.equals(ConstantValues.KAFKA_CMD_DATA_CHANGE_SUPPORT_LOCATION)) {
                            NavigationUI.addLogActivity("",
                                    "Dispatcer mengubah lokasi unit ini ke ?, silakan ikuti arahan dispatcher");
                        }
                        if (type.equals(ConstantValues.KAFKA_CMD_DATA_CHANGE_SUPPORT_DESTINATION)) {
                            NavigationUI.addLogActivity("",
                                    "Dispatcer mengubah destinasi unit ini ke ?, silakan ikuti arahan dispatcher");
                        }
                        if (type.equals(ConstantValues.KAFKA_CMD_DATA_CHANGE_SUPPORT_GROUPLEADER)) {
                            NavigationUI.addLogActivity("", "Dispatcer mengganti group leader menjadi ?");
                        }
                        if (type.equals(ConstantValues.KAFKA_CMD_DATA_CHANGE_SUPPORT_SUPERVISOR)) {
                            NavigationUI.addLogActivity("", "Dispatcer mengganti supervisor menjadi ?");
                        }
                        if (type.equals(ConstantValues.KAFKA_CMD_DATA_CHANGE_SUPPORT_MATERIAL)) {
                            NavigationUI.addLogActivity("",
                                    "Dispatcer mengubah material menjadi ?, silakan ikuti arahan dispatcher");
                        }
                        if (type.equals(ConstantValues.KAFKA_CMD_DATA_CHANGE_SUPPORT_ACTIVITY)) {
                            NavigationUI.addLogActivity("",
                                    "Dispatcer mengubah operasi unit ini ke menjadi ?, silakan ikuti arahan dispatcher");
                        }
                        if (type.equals(ConstantValues.KAFKA_CMD_DATA_CHANGE_SUPPORT_OPERATOR)) {
                            NavigationUI.addLogActivity("",
                                    "Dispatcer mengubah operator unit ini, silakan ikuti arahan dispatcher");
                        }
                    }

                    if (type.equals(ConstantValues.KAFKA_TYPE_OPERATION_ACTUAL)) {

                        /** sudah langsung set activity */
                        if (Utilities.isLoaderHauler()) {
                            Utilities.getOperationActual();
                        } else {
                            Utilities.getOperationSupportActual();
                        }

                        if (MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_LOADER)) {

                            Utilities.getHaulerListAsync(new Runnable() {

                                @Override
                                public void run() {
                                    NavigationUI.windowsRefreshDashboard(p("app.linkDashboardLoader"),
                                            p("app.titleDashboardLoader"), new Runnable() {

                                                @Override
                                                public void run() {
                                                    Utilities.setNotificationText("Perubahan konfigurasi aktual fleet");
                                                }
                                            });
                                }
                            }, null);

                        } else {

                            Utilities.setNotificationText("Perubahan konfigurasi aktual fleet");

                        }

                    }

                    if (type.equals(ConstantValues.KAFKA_TYPE_SCREEN_CONFIG)) {
                        Utilities.getScreenConfig(new Runnable() {

                            @Override
                            public void run() {
                                Utilities.setupFatigueTime();
                            }
                        });
                    }

                } else if (command.equals(ConstantValues.KAFKA_CMD_LOGOUT)) {

                    System.out.println("---- command accepted, do logout action ----");

                    Utilities.doLogOut(null, NavigationUI.stage);

                } else if (command.equals(ConstantValues.KAFKA_CMD_SYNC)) {

                    Synchronizer.syncStatus = true;
                    Synchronizer.syncState = ConstantValues.SYNC_STATE_DOWNLOAD;

                    if (type.equals(ConstantValues.TABLE_USER)) {
                        Synchronizer.syncData = MainApp.userDB.getId();
                        Synchronizer.syncObject = MainApp.userDB;

                    } else if (type.equals(ConstantValues.TABLE_EQUIPMENT_CATEGORY)) {
                        Synchronizer.syncData = MainApp.equipmentCategorytDB.getId();
                        Synchronizer.syncObject = MainApp.equipmentCategorytDB;

                    } else if (type.equals(ConstantValues.TABLE_EQUIPMENT)) {
                        Synchronizer.syncData = MainApp.equipmentDB.getId();
                        Synchronizer.syncObject = MainApp.equipmentDB;

                    } else if (type.equals(ConstantValues.TABLE_LOCATION)) {
                        Synchronizer.syncData = MainApp.locationDB.getId();
                        Synchronizer.syncObject = MainApp.locationDB;

                    } else if (type.equals(ConstantValues.TABLE_CONSTANT)) {
                        Synchronizer.syncData = MainApp.constantDB.getId();
                        Synchronizer.syncObject = MainApp.constantDB;

                    } else if (type.equals(ConstantValues.TABLE_MATERIAL)) {
                        Synchronizer.syncData = MainApp.materialDB.getId();
                        Synchronizer.syncObject = MainApp.materialDB;

                    } else if (type.equals(ConstantValues.TABLE_PRESTART)) {
                        Synchronizer.syncData = MainApp.prestartDB.getId();
                        Synchronizer.syncObject = MainApp.prestartDB;

                    } else if (type.equals(ConstantValues.TABLE_GPS)) {
                        Synchronizer.syncData = MainApp.gpsDB.getId();
                        Synchronizer.syncObject = MainApp.gpsDB;

                    } else if (type.equals(ConstantValues.TABLE_HM)) {
                        Synchronizer.syncData = MainApp.hmDB.getId();
                        Synchronizer.syncObject = MainApp.hmDB;

                    } else if (type.equals(ConstantValues.TABLE_DEVICE_STATUS)) {
                        Synchronizer.syncData = MainApp.deviceStatusDB.getId();
                        Synchronizer.syncObject = MainApp.deviceStatusDB;

                    } else if (type.equals(ConstantValues.TABLE_OPERATION_STATUS)) {
                        Synchronizer.syncData = MainApp.operationStatusDB.getId();
                        Synchronizer.syncObject = MainApp.operationStatusDB;

                    } else if (type.equals(ConstantValues.TABLE_SAFETY_STATUS)) {
                        Synchronizer.syncData = MainApp.safetyStatusDB.getId();
                        Synchronizer.syncObject = MainApp.safetyStatusDB;

                    } else if (type.equals(ConstantValues.TABLE_OPERATION)) {
                        Synchronizer.syncData = MainApp.operationDB.getId();
                        Synchronizer.syncObject = MainApp.operationDB;

                    } else if (type.equals(ConstantValues.TABLE_PRESTART_LOG)) {
                        Synchronizer.syncData = MainApp.prestartLogDB.getId();
                        Synchronizer.syncObject = MainApp.prestartLogDB;

                    } else if (type.equals(ConstantValues.TABLE_FATIGUE_LOG)) {
                        Synchronizer.syncData = MainApp.fatigueLogDB.getId();
                        Synchronizer.syncObject = MainApp.fatigueLogDB;

                    } else if (type.equals(ConstantValues.TABLE_MAINTENANCE_SCHEDULE)) {
                        Synchronizer.syncData = MainApp.maintenanceScheduleDB.getId();
                        Synchronizer.syncObject = MainApp.maintenanceScheduleDB;

                    } else if (type.equals(ConstantValues.TABLE_MAINTENANCE_UNSCHEDULE)) {
                        Synchronizer.syncData = MainApp.maintenanceUnscheduleDB.getId();
                        Synchronizer.syncObject = MainApp.maintenanceUnscheduleDB;

                    } else if (type.equals(ConstantValues.TABLE_OPERATION_LOG_DETAIL)) {
                        Synchronizer.syncData = MainApp.operationLogDetailDB.getId();
                        Synchronizer.syncObject = MainApp.operationLogDetailDB;

                    } else if (type.equals(ConstantValues.TABLE_SCREEN_CONFIG)) {
                        Synchronizer.syncData = MainApp.screenConfigDB.getId();
                        Synchronizer.syncObject = MainApp.screenConfigDB;
                    }

                    Object obj = Synchronizer.syncObject.getFromServerById(reference);
                    Map<Object, Object> fs = Synchronizer.mapper.convertValue(obj, Map.class);
                    if (fs != null) {
                        Object output = fs.get("output");
                        Synchronizer.syncObject.insertIgnore(output);
                    }

                    Synchronizer.syncStatus = false;
                    Synchronizer.syncData = null;
                    Synchronizer.syncState = null;
                    Synchronizer.syncObject = null;

                } else {

                    System.out.println("---- command accepted, no action ----");

                }

            }

        } else {

            System.out.println(">>>>>>>>>>>>> socket not accepted, different destination");
            System.out.println(destination);
            System.out.println(currentUnitId);

        }
    }

    public void handleDistance(DecodedJWT dj, Map<String, Object> value) {
        final String date = value.get("date").toString();
        final String hauler = value.get("hauler").toString();
        final String hauler_name = value.get("hauler_name").toString();
        final String loader = value.get("loader").toString();
        final Object distance_to_loading = value.get("distance_to_loading");
        final Object distance_to_dumping = value.get("distance_to_dumping");

        Double distanceLoading = distance_to_loading == null ? 0.0 : Double.parseDouble(distance_to_loading.toString());
        Double distanceDumping = distance_to_dumping == null ? 0.0 : Double.parseDouble(distance_to_dumping.toString());

        String currentUnitId = MainApp.equipmentLocal.getId();

        Double radius = MainApp.remoteConfig.get(ConstantValues.SCREEN_CONFIG_GEOFENCING_RADIUS) == null ? 0.0
                : Double.parseDouble(
                        MainApp.remoteConfig.get(ConstantValues.SCREEN_CONFIG_GEOFENCING_RADIUS).toString());

        // cek apakah socket memang untuk unit ini
        if (loader.equals(currentUnitId)) {

            System.out.println(">>>>>>>>>>>>>>> message accepted");
            System.out.println(distance_to_loading);
            System.out.println(distance_to_dumping);

            if (radius == 0.0) {
                return; // invalid setup
            }

            if (distanceLoading == 0.0 && distanceDumping == 0.0) {
                return; // something wrong
            }

            if (distanceLoading <= radius) {

                for (Button btn : DashboardLoaderController.buttonHauler) {
                    if (btn.getId().equals(hauler)) {
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                btn.setStyle("-fx-background-color: #966e35;");
                            }
                        });
                    }
                }

                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        NavigationUI.addLogActivity("", hauler_name + " arrive at loading (geofencing)");
                    }
                });

                // Utilities.playSound("");

            } else if (distanceDumping <= radius) {

                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        NavigationUI.addLogActivity("", hauler_name + " arrive at dumping (geofencing)");
                    }
                });

                // Utilities.playSound("");

            }

        } else {

            System.out.println(">>>>>>>>>>>>> socket not accepted, different loader");
            System.out.println(loader);
            System.out.println(currentUnitId);

        }
    }

}
