package com.tura.fms.screen.models.actual_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CdMaterial {
    @SerializedName("cd")
    @Expose
    String cd;

    @SerializedName("name")
    @Expose
    String name;

    @SerializedName("type")
    @Expose
    String type;

    @SerializedName("density")
    @Expose
    String density;

    @SerializedName("price")
    @Expose
    String price;

    @SerializedName("json")
    @Expose
    Object json;

    public String getCd() {
        return cd;
    }

    public void setCd(String cd) {
        this.cd = cd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDensity() {
        return density;
    }

    public void setDensity(String density) {
        this.density = density;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Object getJson() {
        return json;
    }

    public void setJson(Object json) {
        this.json = json;
    }
}
