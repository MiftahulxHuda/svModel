package com.tura.fms.screen.models.prestart_model.submit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PrestartResponse {

    @SerializedName("message")
    @Expose
    String message;

    @SerializedName("respon")
    @Expose
    String respon;

    @SerializedName("output")
    @Expose
    PrestartResponseDetail prestartResponseDetail;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRespon() {
        return respon;
    }

    public void setRespon(String respon) {
        this.respon = respon;
    }

    public PrestartResponseDetail getPrestartResponseDetail() {
        return prestartResponseDetail;
    }

    public void setPrestartResponseDetail(PrestartResponseDetail prestartResponseDetail) {
        this.prestartResponseDetail = prestartResponseDetail;
    }

}
