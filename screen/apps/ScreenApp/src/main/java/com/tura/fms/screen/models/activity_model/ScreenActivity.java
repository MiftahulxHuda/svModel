package com.tura.fms.screen.models.activity_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ScreenActivity {

    @SerializedName("id_equipment")
    @Expose
    private String id_equipment;

    @SerializedName("id_actual")
    @Expose
    private String id_actual;

    @SerializedName("cd_activity")
    @Expose
    private String cd_activity;

    @SerializedName("id_user")
    @Expose
    private String id_user;

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getCd_activity() {
        return cd_activity;
    }

    public void setCd_activity(String cd_activity) {
        this.cd_activity = cd_activity;
    }

    public String getId_equipment() {
        return id_equipment;
    }

    public void setId_equipment(String id_equipment) {
        this.id_equipment = id_equipment;
    }

    public String getId_actual() {
        return id_actual;
    }

    public void setId_actual(String id_actual) {
        this.id_actual = id_actual;
    }

}
