package com.tura.fms.screen.models.maintenance_model.task_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseMaintenanceDetailsOutput {

    @SerializedName("response")
    @Expose
    String response;

    @SerializedName("message")
    @Expose
    String message;

    @SerializedName("pages")
    @Expose
    int pages;

    @SerializedName("output")
    @Expose
    List<MaintenanceDetails> output;

    @SerializedName("totalItems")
    @Expose
    int totalItems;

    @SerializedName("currentPage")
    @Expose
    int currentPage;

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public List<MaintenanceDetails> getOutput() {
        return output;
    }

    public void setOutput(List<MaintenanceDetails> output) {
        this.output = output;
    }

    public int getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
