package com.tura.fms.screen.models.prestart_model;

import com.tura.fms.screen.database.Operation;
import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.database.SQLiteJDBC;
import com.tura.fms.screen.database.Synchronizer;
import com.tura.fms.screen.models.ResponseModel;
import com.tura.fms.screen.models.activity_model.ActivityDetail;
import com.tura.fms.screen.models.prestart_model.PrestartDataDetail;
import com.tura.fms.screen.models.prestart_model.PrestartEquipment;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;

import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Prestart_DB extends Operation {
    // Table Name
    private static final String TABLE_NAME = "Prestart_Record";

    // Table Columns
    private static final String id = "id";
    private static final String name = "name";
    private static final String alias = "alias";
    private static final String type = "type";
    private static final String cd_type_equipment = "cd_type_equipment";
    private static final String cd_self_type_equipment = "cd_self_type_equipment";
    private static final String name_type_equipment = "name_type_equipment";
    private static final String sync_status = "sync_status";

    public Prestart_DB() {
        super(Prestart_DB.TABLE_NAME, ConstantValues.DATA_PRESTART);
    }

    public static String createTable() {
        return "CREATE TABLE " + TABLE_NAME + "(" + id + " TEXT PRIMARY KEY," + name + " TEXT," + alias + " TEXT,"
                + type + " TEXT," + cd_type_equipment + " TEXT," + cd_self_type_equipment + " TEXT,"
                + name_type_equipment + " TEXT, " + sync_status + " NUMBER NULL" + ")";

    }

    public void createTablePrestart() {
        try {

            DatabaseMetaData dbm = conn().getMetaData();
            ResultSet rs = dbm.getTables(null, null, TABLE_NAME, null);
            Statement stmt = null;

            if (rs.next()) {
                System.out.println("Table exists prestart");
                // stmt.close();
                // conn().close();
            } else {
                stmt = conn().createStatement();
                stmt.executeUpdate(createTable());
                stmt.close();
                // conn().close();
                System.out.println("Table created prestart");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public List<PrestartDataDetail> getData(String param_cd, String ty) {

        List<PrestartDataDetail> detail = new ArrayList<>();

        try {
            ResultSet cursor = null;

            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + cd_type_equipment + "=?";
            if (type != null) {
                sql += " AND " + type + "=?";
            }
            // System.out.println("<<<<<<<<<<<<<<<<<<<< sql");
            // System.out.println(sql);

            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, param_cd);
            if (type != null) {
                preparedStatement.setString(2, ty);
            }

            cursor = preparedStatement.executeQuery();

            while (cursor.next()) {

                PrestartDataDetail setdetail = new PrestartDataDetail();

                setdetail.setId(cursor.getString(1));
                setdetail.setName(cursor.getString(2));
                setdetail.setAlias(cursor.getString(3));
                setdetail.setType(cursor.getString(4));

                PrestartEquipment prestartEquipment = new PrestartEquipment();
                prestartEquipment.setCd(cursor.getString(5));
                prestartEquipment.setCd_self(cursor.getString(6));
                prestartEquipment.setName(cursor.getString(7));

                setdetail.setPrestartEquipment(prestartEquipment);

                detail.add(setdetail);

            }

            // conn().close();

            return detail;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;

    }

    public void insertData(List<PrestartDataDetail> detail) {

        PreparedStatement st = null;
        Boolean rs = null;
        String proses;

        for (int i = 0; i < detail.size(); i++) {
            // System.out.println("coba " + detail.get(i).getId());

            try {
                String sql;

                if (getExisting(detail.get(i).getId()) == 0) {

                    sql = "INSERT INTO " + TABLE_NAME + " " + "(" + id + "," + name + "," + alias + "," + type + ","
                            + cd_type_equipment + "," + cd_self_type_equipment + "," + name_type_equipment + "" + ")"
                            + "VALUES(?,?,?,?,?,?,?);";

                    proses = "proses";

                } else {

                    sql = "UPDATE  " + TABLE_NAME + " SET " + "" + id + "=?," + name + "=?," + alias + "=?," + type
                            + "=?," + cd_type_equipment + "=?," + cd_self_type_equipment + "=?," + name_type_equipment
                            + "=?" + " WHERE " + id + " =? ;";

                    proses = "update";

                }

                st = conn().prepareStatement(sql);

                st.setString(1, detail.get(i).getId());
                st.setString(2, detail.get(i).getName());
                st.setString(3, detail.get(i).getAlias());
                st.setString(4, detail.get(i).getType());
                st.setString(5, detail.get(i).getPrestartEquipment().getCd());
                st.setString(6, detail.get(i).getPrestartEquipment().getCd_self());
                st.setString(7, detail.get(i).getPrestartEquipment().getName());

                if (proses.equals("update")) {
                    st.setString(8, detail.get(i).getId());
                }
                rs = st.execute();

                // conn().close();

            } catch (SQLException e) {
                e.printStackTrace();
            } finally {

                if (rs) {
                    try {
                        st.close();
                        // conn().close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        }

    }

    public int getExisting(String cd_param) {
        try {
            String sql = "SELECT count(*) as total FROM " + TABLE_NAME + " where " + id + "=?";

            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, cd_param);

            ResultSet cursor = preparedStatement.executeQuery();

            int jumlah;

            if (cursor.next()) {
                jumlah = cursor.getInt("total");
            } else {
                jumlah = 0;
            }
            cursor.close();
            // conn().close();
            return jumlah;
        } catch (Exception e) {

            return 0;
        }
    }

    @Override
    public Object insertIgnore(Object eqa) {
        PrestartAPI eq = Synchronizer.mapper.convertValue(eqa, PrestartAPI.class);
        PreparedStatement st = null;
        Boolean rs = null;
        try {
            String sql = "INSERT INTO " + TABLE_NAME + " " + "(" + id + "," + name + "," + alias + "," + type + ","
                    + cd_type_equipment + "," + cd_self_type_equipment + "," + name_type_equipment + ", sync_status"
                    + ")" + "VALUES(?,?,?,?,?,?,?,1) ON CONFLICT(" + id + ") DO UPDATE SET " + id + "=?," + name + "=?,"
                    + alias + "=?," + type + "=?," + cd_type_equipment + "=?," + cd_self_type_equipment + "=?,"
                    + name_type_equipment + "=?;";

            st = conn().prepareStatement(sql);
            st.setString(1, eq.getId());
            st.setString(2, eq.getName());
            st.setString(3, eq.getAlias());
            st.setString(4, eq.getType());
            st.setString(8, eq.getId());
            st.setString(9, eq.getName());
            st.setString(10, eq.getAlias());
            st.setString(11, eq.getType());
            if (eq.getCdTypeEquipment() != null) {
                st.setString(5, eq.getCdTypeEquipment().getCd());
                st.setString(6, eq.getCdTypeEquipment().getCdSelf());
                st.setString(7, eq.getCdTypeEquipment().getName());
                st.setString(12, eq.getCdTypeEquipment().getCd());
                st.setString(13, eq.getCdTypeEquipment().getCdSelf());
                st.setString(14, eq.getCdTypeEquipment().getName());
            } else {
                st.setString(5, "");
                st.setString(6, "");
                st.setString(7, "");
                st.setString(12, "");
                st.setString(13, "");
                st.setString(14, "");
            }
            rs = st.execute();
            st.close();

            return eq;

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (rs && st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
        return null;
    }

    @Override
    public void deleteData(String id) {
        String sql = "delete from " + TABLE_NAME + " where id=" + id;
        try {
            Statement stmt = conn().createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object getFromServer(Integer page, Integer size) {
        if (!NetworkConnection.getServiceStatus()) {
            return null;
        }
        Call<Object> call = APIClient.getInstance().getPrestart(MainApp.requestHeader, page, size);
        try {
            Response<Object> response = call.execute();
            return response.body();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Object getFromServerById(String id) {
        if (!NetworkConnection.getServiceStatus()) {
            return null;
        }
        Call<Object> call = APIClient.getInstance().getPrestartById(MainApp.requestHeader, id);
        try {
            Response<Object> response = call.execute();
            return response.body();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Integer getRemoteCount() {
        if (!NetworkConnection.getServiceStatus()) {
            return 0;
        }
        Call<Object> call = APIClient.getInstance().getPrestartCount(MainApp.requestHeader);
        try {
            Response<Object> response = call.execute();
            Map<String, Number> mapped = APIClient.mapper.convertValue(response.body(), Map.class);
            if (mapped.get("output") != null) {
                return mapped.get("output").intValue();
            } else {
                return 0;
            }
        } catch (IOException ex) {
            // ex.printStackTrace();
            System.out.println("[Prestart getRemoteCount] " + ex.getMessage());
        }
        return 0;
    }

    @Override
    public List<Object> getFlaggedData() {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE sync_status=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, "0");
            ResultSet cursor = preparedStatement.executeQuery();
            List<Object> result = new ArrayList<Object>();
            while (cursor.next()) {
                PrestartModel eq = new PrestartModel(cursor.getString("id"), cursor.getString("name"),
                        cursor.getString("alias"));
                result.add(eq);
            }
            cursor.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void postBulkData(List<Object> eq) {
        if (!NetworkConnection.getServiceStatus()) {
            return;
        }
        // do noting
    }

    @Override
    public void setSynchronized(String id) {
        String sql = "update " + TABLE_NAME + " set sync_status=1 where id='" + id + "'";
        try {
            Statement stmt = conn().createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
