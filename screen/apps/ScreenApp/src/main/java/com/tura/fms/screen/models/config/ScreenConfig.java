package com.tura.fms.screen.models.config;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ScreenConfig {

    @SerializedName("id")
    @Expose
    String id;

    @SerializedName("name")
    @Expose
    String name;

    @SerializedName("value")
    @Expose
    String value;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
