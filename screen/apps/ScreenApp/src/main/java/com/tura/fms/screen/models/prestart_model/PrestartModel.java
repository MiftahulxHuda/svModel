package com.tura.fms.screen.models.prestart_model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PrestartModel {

    private final StringProperty id;
    private final StringProperty name;
    private final StringProperty alias;

    public PrestartModel(String id, String name) {
        this.id = new SimpleStringProperty(id);
        this.name = new SimpleStringProperty(name);
        this.alias = new SimpleStringProperty("");
    }

    public PrestartModel(String id, String name, String alias) {
        this.id = new SimpleStringProperty(id);
        this.name = new SimpleStringProperty(name);
        this.alias = new SimpleStringProperty(alias);
    }

    public String getId() {
        return id.get();
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public StringProperty idProperty() {
        return id;
    }

    public StringProperty nameProperty() {
        return name;
    }

    public String getAlias() {
        return alias.get();
    }

    public void setAlias(String alias) {
        this.alias.set(alias);
    }

}
