package com.tura.fms.screen.models.maintenance_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MechanicArrivePost {

    @SerializedName("id_equipment")
    @Expose
    String id_equipment;;

    @SerializedName("id_mechanic")
    @Expose
    String id_mechanic;

    public String getId_equipment() {
        return id_equipment;
    }

    public void setId_equipment(String id_equipment) {
        this.id_equipment = id_equipment;
    }

    public String getId_mechanic() {
        return id_mechanic;
    }

    public void setId_mechanic(String id_mechanic) {
        this.id_mechanic = id_mechanic;
    }

}
