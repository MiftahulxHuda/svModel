package com.tura.fms.screen.models.activity_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tura.fms.screen.models.constant.ConstantLocal;

import java.util.ArrayList;
import java.util.List;

public class ActivityData {

    @SerializedName("output")
    @Expose
    private List<ActivityDetail> output = null;

    public List<ActivityDetail> getOutput() {
        return output;
    }

    public void setOutput(List<ActivityDetail> output) {
        this.output = output;
    }

    public void setOutputObject(List<Object> output) {
        this.output = new ArrayList<ActivityDetail>();
        for (Object obj : output) {
            ConstantLocal eq = (ConstantLocal) obj;
            ActivityDetail act = new ActivityDetail();
            act.setCd(eq.getCd());
            act.setCd_self(eq.getCdSelf());
            act.setName(eq.getName());
            this.output.add(act);
        }
    }
}
