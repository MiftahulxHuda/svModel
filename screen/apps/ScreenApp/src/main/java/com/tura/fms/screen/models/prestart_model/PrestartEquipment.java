package com.tura.fms.screen.models.prestart_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PrestartEquipment {

    @SerializedName("cd")
    @Expose
    String cd;

    @SerializedName("cd_self")
    @Expose
    String cd_self;

    @SerializedName("name")
    @Expose
    String name;

    @SerializedName("json")
    @Expose
    JsonPrestart json;

    public String getCd() {
        return cd;
    }

    public void setCd(String cd) {
        this.cd = cd;
    }

    public String getCd_self() {
        return cd_self;
    }

    public void setCd_self(String cd_self) {
        this.cd_self = cd_self;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public JsonPrestart getJson() {
        return json;
    }

    public void setJson(JsonPrestart json) {
        this.json = json;
    }
}
