package com.tura.fms.screen.models.actual_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tura.fms.screen.models.constant.Constant;

public class ActualDetail {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("id_plan")
    @Expose
    private String id_plan;

    @SerializedName("__id_supervisor__")
    @Expose
    private Supervisor id_supervisor;

    @SerializedName("__id_group_leader__")
    @Expose
    private GroupLeader id_group_leader;

    @SerializedName("__id_checker__")
    @Expose
    private String id_checker;

    @SerializedName("__id_loader_operator__")
    @Expose
    private String id_loader_operator;

    @SerializedName("__id_loader__")
    @Expose
    private Loader id_loader = null;

    @SerializedName("__id_hauler_operator__")
    @Expose
    private String id_hauler_operator;

    @SerializedName("__id_hauler__")
    @Expose
    private Hauler id_hauler;

    @SerializedName("__id_location__")
    @Expose
    private Location id_location = null;

    @SerializedName("__id_destination__")
    @Expose
    private Location id_destination;

    @SerializedName("__cd_operation__")
    @Expose
    private CdOperation cd_operation;

    @SerializedName("__cd_shift__")
    @Expose
    private Constant cd_shift;

    @SerializedName("__cd_material__")
    @Expose
    private CdMaterial cd_material;

    @SerializedName("date")
    @Expose
    private Long date;

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_plan() {
        return id_plan;
    }

    public void setId_plan(String id_plan) {
        this.id_plan = id_plan;
    }

    public Supervisor getId_supervisor() {
        return id_supervisor;
    }

    public void setId_supervisor(Supervisor id_supervisor) {
        this.id_supervisor = id_supervisor;
    }

    public GroupLeader getId_group_leader() {
        return id_group_leader;
    }

    public void setId_group_leader(GroupLeader id_group_leader) {
        this.id_group_leader = id_group_leader;
    }

    public String getId_checker() {
        return id_checker;
    }

    public void setId_checker(String id_checker) {
        this.id_checker = id_checker;
    }

    public String getId_loader_operator() {
        return id_loader_operator;
    }

    public void setId_loader_operator(String id_loader_operator) {
        this.id_loader_operator = id_loader_operator;
    }

    public Loader getId_loader() {
        return id_loader;
    }

    public void setId_loader(Loader id_loader) {
        this.id_loader = id_loader;
    }

    public String getId_hauler_operator() {
        return id_hauler_operator;
    }

    public void setId_hauler_operator(String id_hauler_operator) {
        this.id_hauler_operator = id_hauler_operator;
    }

    public Hauler getId_hauler() {
        return id_hauler;
    }

    public void setId_hauler(Hauler id_hauler) {
        this.id_hauler = id_hauler;
    }

    public Location getId_location() {
        return id_location;
    }

    public void setId_location(Location id_location) {
        this.id_location = id_location;
    }

    public Location getId_destination() {
        return id_destination;
    }

    public void setId_destination(Location id_destination) {
        this.id_destination = id_destination;
    }

    public CdOperation getCd_operation() {
        return cd_operation;
    }

    public void setCd_operation(CdOperation cd_operation) {
        this.cd_operation = cd_operation;
    }

    public Constant getCd_shift() {
        return cd_shift;
    }

    public void setCd_shift(Constant cd_shift) {
        this.cd_shift = cd_shift;
    }

    public CdMaterial getCd_material() {
        return cd_material;
    }

    public void setCd_material(CdMaterial cd_material) {
        this.cd_material = cd_material;
    }

}
