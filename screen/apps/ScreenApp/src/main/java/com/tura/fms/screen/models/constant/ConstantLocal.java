package com.tura.fms.screen.models.constant;

import java.util.HashMap;
import java.util.Map;

public class ConstantLocal {

    private String cd;
    private String cd_self;
    private String name;
    private Object json;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getCd() {
        return cd;
    }

    public void setCd(String cd) {
        this.cd = cd;
    }

    public String getCdSelf() {
        return cd_self;
    }

    public void setCdSelf(String cdSelf) {
        this.cd_self = cdSelf;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getJson() {
        return json;
    }

    public void setJson(Object json) {
        this.json = json;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}