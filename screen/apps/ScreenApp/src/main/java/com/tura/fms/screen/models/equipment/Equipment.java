package com.tura.fms.screen.models.equipment;

import com.fasterxml.jackson.annotation.*;
import com.tura.fms.screen.models.location.Location;
import com.tura.fms.screen.models.constant.Constant;
import com.tura.fms.screen.models.equipment_category.EquipmentCategory;
import com.tura.fms.screen.models.user_model.UserDetails;
import com.tura.fms.screen.models.user_model.UserOrg;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "__id_org__", "__has_id_org__", "__id_operator__", "__has_id_operator__", "__cd_operation__",
        "__has_cd_operation__", "__cd_equipment__", "__has_cd_equipment__", "name", "capacity_real", "capacity_labs",
        "enum_availability", "is_deleted", "date_submitted", "date_modified", "load_weight", "empty_weight",
        "capacity_labs_2", "capacity_labs_3", "hour_meter", "class", "code", "json", "__id_location__",
        "__has_id_location__", "hm_start", "hm_now", "hm_date", "id_equipment_category" })
public class Equipment {

    @JsonProperty("id")
    private String id;
    @JsonProperty("__id_org__")
    private UserOrg idOrg;
    @JsonProperty("__has_id_org__")
    private Boolean hasIdOrg;
    @JsonProperty("__id_operator__")
    private UserDetails idOperator;
    @JsonProperty("__has_id_operator__")
    private Boolean hasIdOperator;
    @JsonProperty("__cd_operation__")
    private Constant cdOperation;
    @JsonProperty("__has_cd_operation__")
    private Boolean hasCdOperation;
    @JsonProperty("__cd_equipment__")
    private Constant cdEquipment;
    @JsonProperty("__has_cd_equipment__")
    private Boolean hasCdEquipment;
    @JsonProperty("name")
    private String name;
    @JsonProperty("capacity_real")
    private Integer capacityReal;
    @JsonProperty("capacity_labs")
    private Double capacityLabs;
    @JsonProperty("enum_availability")
    private Integer enumAvailability;
    @JsonProperty("is_deleted")
    private Integer isDeleted;
    @JsonProperty("date_submitted")
    private Integer dateSubmitted;
    @JsonProperty("date_modified")
    private Integer dateModified;
    @JsonProperty("load_weight")
    private Integer loadWeight;
    @JsonProperty("empty_weight")
    private Integer emptyWeight;
    @JsonProperty("capacity_labs_2")
    private Double capacityLabs2;
    @JsonProperty("capacity_labs_3")
    private Double capacityLabs3;
    @JsonProperty("hour_meter")
    private Integer hourMeter;
    @JsonProperty("class")
    private String _class;
    @JsonProperty("code")
    private String code;
    @JsonProperty("json")
    private String json;
    @JsonProperty("__id_location__")
    private Location idLocation;
    @JsonProperty("__has_id_location__")
    private Boolean hasIdLocation;
    @JsonProperty("hm_start")
    private Integer hmStart;
    @JsonProperty("hm_now")
    private Integer hmNow;
    @JsonProperty("hm_date")
    private Integer hmDate;
    @JsonProperty("id_equipment_category")
    private EquipmentCategory idEquipmentCategory;
    @JsonProperty("sync_status")
    private Integer syncStatus;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("__id_org__")
    public UserOrg getIdOrg() {
        return idOrg;
    }

    @JsonProperty("__id_org__")
    public void setIdOrg(UserOrg idOrg) {
        this.idOrg = idOrg;
    }

    @JsonProperty("__has_id_org__")
    public Boolean getHasIdOrg() {
        return hasIdOrg;
    }

    @JsonProperty("__has_id_org__")
    public void setHasIdOrg(Boolean hasIdOrg) {
        this.hasIdOrg = hasIdOrg;
    }

    @JsonProperty("__id_operator__")
    public UserDetails getIdOperator() {
        return idOperator;
    }

    @JsonProperty("__id_operator__")
    public void setIdOperator(UserDetails idOperator) {
        this.idOperator = idOperator;
    }

    @JsonProperty("__has_id_operator__")
    public Boolean getHasIdOperator() {
        return hasIdOperator;
    }

    @JsonProperty("__has_id_operator__")
    public void setHasIdOperator(Boolean hasIdOperator) {
        this.hasIdOperator = hasIdOperator;
    }

    @JsonProperty("__cd_operation__")
    public Constant getCdOperation() {
        return cdOperation;
    }

    @JsonProperty("__cd_operation__")
    public void setCdOperation(Constant cdOperation) {
        this.cdOperation = cdOperation;
    }

    @JsonProperty("__has_cd_operation__")
    public Boolean getHasCdOperation() {
        return hasCdOperation;
    }

    @JsonProperty("__has_cd_operation__")
    public void setHasCdOperation(Boolean hasCdOperation) {
        this.hasCdOperation = hasCdOperation;
    }

    @JsonProperty("__cd_equipment__")
    public Constant getCdEquipment() {
        return cdEquipment;
    }

    @JsonProperty("__cd_equipment__")
    public void setCdEquipment(Constant cdEquipment) {
        this.cdEquipment = cdEquipment;
    }

    @JsonProperty("__has_cd_equipment__")
    public Boolean getHasCdEquipment() {
        return hasCdEquipment;
    }

    @JsonProperty("__has_cd_equipment__")
    public void setHasCdEquipment(Boolean hasCdEquipment) {
        this.hasCdEquipment = hasCdEquipment;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("capacity_real")
    public Integer getCapacityReal() {
        return capacityReal;
    }

    @JsonProperty("capacity_real")
    public void setCapacityReal(Integer capacityReal) {
        this.capacityReal = capacityReal;
    }

    @JsonProperty("capacity_labs")
    public Double getCapacityLabs() {
        return capacityLabs;
    }

    @JsonProperty("capacity_labs")
    public void setCapacityLabs(Double capacityLabs) {
        this.capacityLabs = capacityLabs;
    }

    @JsonProperty("enum_availability")
    public Integer getEnumAvailability() {
        return enumAvailability;
    }

    @JsonProperty("enum_availability")
    public void setEnumAvailability(Integer enumAvailability) {
        this.enumAvailability = enumAvailability;
    }

    @JsonProperty("is_deleted")
    public Integer getIsDeleted() {
        return isDeleted;
    }

    @JsonProperty("is_deleted")
    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    @JsonProperty("date_submitted")
    public Integer getDateSubmitted() {
        return dateSubmitted;
    }

    @JsonProperty("date_submitted")
    public void setDateSubmitted(Integer dateSubmitted) {
        this.dateSubmitted = dateSubmitted;
    }

    @JsonProperty("date_modified")
    public Integer getDateModified() {
        return dateModified;
    }

    @JsonProperty("date_modified")
    public void setDateModified(Integer dateModified) {
        this.dateModified = dateModified;
    }

    @JsonProperty("load_weight")
    public Integer getLoadWeight() {
        return loadWeight;
    }

    @JsonProperty("load_weight")
    public void setLoadWeight(Integer loadWeight) {
        this.loadWeight = loadWeight;
    }

    @JsonProperty("empty_weight")
    public Integer getEmptyWeight() {
        return emptyWeight;
    }

    @JsonProperty("empty_weight")
    public void setEmptyWeight(Integer emptyWeight) {
        this.emptyWeight = emptyWeight;
    }

    @JsonProperty("capacity_labs_2")
    public Double getCapacityLabs2() {
        return capacityLabs2;
    }

    @JsonProperty("capacity_labs_2")
    public void setCapacityLabs2(Double capacityLabs2) {
        this.capacityLabs2 = capacityLabs2;
    }

    @JsonProperty("capacity_labs_3")
    public Double getCapacityLabs3() {
        return capacityLabs3;
    }

    @JsonProperty("capacity_labs_3")
    public void setCapacityLabs3(Double capacityLabs3) {
        this.capacityLabs3 = capacityLabs3;
    }

    @JsonProperty("hour_meter")
    public Integer getHourMeter() {
        return hourMeter;
    }

    @JsonProperty("hour_meter")
    public void setHourMeter(Integer hourMeter) {
        this.hourMeter = hourMeter;
    }

    @JsonProperty("class")
    public String getClass_() {
        return _class;
    }

    @JsonProperty("class")
    public void setClass_(String _class) {
        this._class = _class;
    }

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty("json")
    public String getJson() {
        return json;
    }

    @JsonProperty("json")
    public void setJson(String json) {
        this.json = json;
    }

    @JsonProperty("__id_location__")
    public Location getIdLocation() {
        return idLocation;
    }

    @JsonProperty("__id_location__")
    public void setIdLocation(Location idLocation) {
        this.idLocation = idLocation;
    }

    @JsonProperty("__has_id_location__")
    public Boolean getHasIdLocation() {
        return hasIdLocation;
    }

    @JsonProperty("__has_id_location__")
    public void setHasIdLocation(Boolean hasIdLocation) {
        this.hasIdLocation = hasIdLocation;
    }

    @JsonProperty("hm_start")
    public Integer getHmStart() {
        return hmStart;
    }

    @JsonProperty("hm_start")
    public void setHmStart(Integer hmStart) {
        this.hmStart = hmStart;
    }

    @JsonProperty("hm_now")
    public Integer getHmNow() {
        return hmNow;
    }

    @JsonProperty("hm_now")
    public void setHmNow(Integer hmNow) {
        this.hmNow = hmNow;
    }

    @JsonProperty("hm_date")
    public Integer getHmDate() {
        return hmDate;
    }

    @JsonProperty("hm_date")
    public void setHmDate(Integer hmDate) {
        this.hmDate = hmDate;
    }

    @JsonProperty("id_equipment_category")
    public EquipmentCategory getEquipmentCategory() {
        return idEquipmentCategory;
    }

    @JsonProperty("id_equipment_category")
    public void setEquipmentCategory(EquipmentCategory eq) {
        this.idEquipmentCategory = eq;
    }

    @JsonProperty("sync_status")
    public Integer getSyncStatus() {
        return syncStatus;
    }

    @JsonProperty("sync_status")
    public void setSyncStatus(Integer syncStatus) {
        this.syncStatus = syncStatus;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
