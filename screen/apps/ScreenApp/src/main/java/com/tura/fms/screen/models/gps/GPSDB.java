package com.tura.fms.screen.models.gps;

import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.database.Operation;
import com.tura.fms.screen.models.ResponseModel;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;

import retrofit2.Call;
import retrofit2.Response;

public class GPSDB extends Operation {
    public static final String TABLE_NAME = "GPS_Record";

    private static final String id = "id";
    private static final String id_actual = "id_actual";
    private static final String id_operation_support = "id_operation_support";
    private static final String id_operation_loader = "id_operation_loader";
    private static final String time = "time";
    private static final String id_equipment = "id_equipment";
    private static final String id_operator = "id_operator";
    private static final String json = "json";
    private static final String sync_status = "sync_status";

    public GPSDB() {
        super(GPSDB.TABLE_NAME, ConstantValues.DATA_GPS);
    }

    public void createTableGPS() {
        try {
            DatabaseMetaData dbm = conn().getMetaData();
            ResultSet rs = dbm.getTables(null, null, TABLE_NAME, null);
            Statement stmt = null;
            if (rs.next()) {
                System.out.println("Table gps exists");
            } else {
                stmt = conn().createStatement();
                stmt.setQueryTimeout(10);
                stmt.executeUpdate(createTable());
                stmt.close();
                System.out.println("Table gps created");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String createTable() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + id + " TEXT PRIMARY KEY," + id_actual + " TEXT NULL,"
                + id_operation_support + " TEXT NULL," + id_operation_loader + " TEXT NULL," + time + " NUMBER NULL,"
                + id_equipment + " TEXT NULL," + id_operator + " TEXT NULL," + json + " TEXT NULL," + sync_status
                + " NUMBER NULL" + ")";
    }

    @Override
    public Object insertIgnore(Object eqa) {
        GPSLocal eq = (GPSLocal) eqa;
        PreparedStatement st = null;
        Boolean rs = null;
        try {
            String sql = "INSERT INTO " + TABLE_NAME + " " + "(" + id + "," + id_actual + "," + id_operation_support
                    + "," + id_operation_loader + "," + time + "," + id_equipment + "," + id_operator + "," + json + ","
                    + sync_status + ")" + " VALUES(?,?,?,?,?,?,?,?,0) ON CONFLICT(id) DO UPDATE SET " + id + "=?,"
                    + id_actual + "=?," + id_operation_support + "=?," + id_operation_loader + "=?," + time + "=?,"
                    + id_equipment + "=?," + id_operator + "=?," + json + "=?";

            st = conn().prepareStatement(sql);

            final String uuid = UUID.randomUUID().toString().replace("-", "");

            st.setString(1, uuid);
            st.setString(9, uuid);
            st.setString(2, eq.getIdActual() == null ? null : eq.getIdActual().toString());
            st.setString(10, eq.getIdActual() == null ? null : eq.getIdActual().toString());
            st.setString(3, eq.getIdOperationSupport() == null ? null : eq.getIdOperationSupport().toString());
            st.setString(11, eq.getIdOperationSupport() == null ? null : eq.getIdOperationSupport().toString());
            st.setString(4, eq.getIdOperationLoader() == null ? null : eq.getIdOperationLoader().toString());
            st.setString(12, eq.getIdOperationLoader() == null ? null : eq.getIdOperationLoader().toString());
            st.setLong(5, eq.getTime());
            st.setLong(13, eq.getTime());
            st.setString(6, eq.getIdEquipment() == null ? null : eq.getIdEquipment().toString());
            st.setString(14, eq.getIdEquipment() == null ? null : eq.getIdEquipment().toString());
            st.setString(7, eq.getIdOperator() == null ? null : eq.getIdOperator().toString());
            st.setString(15, eq.getIdOperator() == null ? null : eq.getIdOperator().toString());
            st.setString(8, eq.getJson() == null ? null : eq.getJson().toString());
            st.setString(16, eq.getJson() == null ? null : eq.getJson().toString());
            rs = st.execute();
            st.close();

            return this.getLocalById(id, uuid);

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (rs && st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }

    @Override
    public void deleteData(String ids) {
        String sql = "delete from " + TABLE_NAME + " where " + id + "=?";
        try {
            PreparedStatement ps = conn().prepareStatement(sql);
            ps.setString(1, ids);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object getFromServer(Integer page, Integer size) {
        if (!NetworkConnection.getServiceStatus()) {
            return null;
        }
        // no action
        return null;
    }

    @Override
    public Integer getRemoteCount() {
        if (!NetworkConnection.getServiceStatus()) {
            return 0;
        }
        // no action
        return 0;
    }

    @Override
    public List<Object> getFlaggedData() {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE sync_status=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, "0");
            ResultSet cursor = preparedStatement.executeQuery();
            List<Object> result = new ArrayList<Object>();
            while (cursor.next()) {
                GPSLocal eq = new GPSLocal();
                eq.setId(cursor.getString(id));
                eq.setIdActual(cursor.getString(id_actual));
                eq.setIdOperationSupport(cursor.getString(id_operation_support));
                eq.setIdOperationLoader(cursor.getString(id_operation_loader));
                eq.setTime(cursor.getLong(time));
                eq.setIdEquipment(cursor.getString(id_equipment));
                eq.setIdOperator(cursor.getString(id_operator));
                eq.setJson(cursor.getString(json));
                result.add(eq);
            }
            cursor.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Object getFlaggedDataOne() {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE sync_status=? AND " + id_equipment + "=? LIMIT 1";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, "0");
            String eqId = MainApp.equipmentLocal != null ? MainApp.equipmentLocal.getId() : "";
            preparedStatement.setString(2, eqId);
            ResultSet cursor = preparedStatement.executeQuery();

            if (cursor.next()) {
                GPSLocal eq = new GPSLocal();
                eq.setId(cursor.getString(id));
                eq.setIdActual(cursor.getString(id_actual));
                eq.setIdOperationSupport(cursor.getString(id_operation_support));
                eq.setIdOperationLoader(cursor.getString(id_operation_loader));
                eq.setTime(cursor.getLong(time));
                eq.setIdEquipment(cursor.getString(id_equipment));
                eq.setIdOperator(cursor.getString(id_operator));
                eq.setJson(cursor.getString(json));

                cursor.close();
                return eq;

            } else {

                cursor.close();
                return null;
            }

        } catch (Exception e) {

            System.out.println(e.getMessage());
            return null;

        }
    }

    @Override
    public void postBulkData(List<Object> eq) {
        if (!NetworkConnection.getKafkaStatus()) {
            return;
        }
        System.out.println("--- post synchronization ---");
        System.out.println("NOT DONE YET !!!");
        // for (Object obj : eq) {
        // GPSLocal oqe = (GPSLocal) obj;
        // RecordMetadata rm = MainApp.producer.push(IKafkaConstant.TOPIC_PRODUCE_GPS,
        // obj);
        // if (rm.hasOffset()) {
        // setSynchronized(oqe.getId());
        // }
        // }
    }

    @Override
    public void postData(Object eq, Runnable cb) {
        if (!NetworkConnection.getServiceStatus()) {
            return;
        }

        String url = MainApp.config("config.unit_service_address") + "v-us/gps";
        Call<ResponseModel> call = APIClient.getInstance().postOfflineGPS(url, MainApp.requestHeader, eq);
        try {
            Response<ResponseModel> response = call.execute();
            if (response.body() != null) {
                if (response.body().getResponse() != null) {
                    if (response.body().getResponse().equals("success")) {
                        System.out.println("post success");

                        if (cb != null) {
                            cb.run();
                        }
                    }
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void setSynchronized(String ids) {
        String sql = "update " + TABLE_NAME + " set sync_status=1 where " + id + "=?";
        // deleteData(id);
        try {
            PreparedStatement ps = conn().prepareStatement(sql);
            ps.setString(1, ids);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
