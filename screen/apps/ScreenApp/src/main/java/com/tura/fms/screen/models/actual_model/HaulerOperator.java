package com.tura.fms.screen.models.actual_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HaulerOperator {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("id_crew")
    @Expose
    private String id_crew;

    @SerializedName("__has_id_crew__")
    @Expose
    private Boolean has_id_crew;

    @SerializedName("__id_org__")
    @Expose
    private String id_org;

    @SerializedName("__has_id_org__")
    @Expose
    private Boolean has_id_org;

    @SerializedName("__cd_role__")
    @Expose
    private String cd_role;

    @SerializedName("__has_cd_role__")
    @Expose
    private Boolean has_cd_role;

    @SerializedName("__cd_department__")
    @Expose
    private String cd_department;

    @SerializedName("__has_cd_department__")
    @Expose
    private Boolean has_cd_department;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("pwd")
    @Expose
    private String pwd;

    @SerializedName("staff_id")
    @Expose
    private String staff_id;

    @SerializedName("fingerprint_id")
    @Expose
    private int fingerprint_id;

    @SerializedName("socket_id")
    @Expose
    private String socket_id;

    @SerializedName("file_picture")
    @Expose
    private String file_picture;

    @SerializedName("ip_address")
    @Expose
    private String ip_address;

    @SerializedName("token")
    @Expose
    private String token;

    @SerializedName("enum_availability")
    @Expose
    private int enum_availability;

    @SerializedName("is_authenticated")
    @Expose
    private int is_authenticated;




    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_crew() {
        return id_crew;
    }

    public void setId_crew(String id_crew) {
        this.id_crew = id_crew;
    }

    public Boolean getHas_id_crew() {
        return has_id_crew;
    }

    public void setHas_id_crew(Boolean has_id_crew) {
        this.has_id_crew = has_id_crew;
    }

    public String getId_org() {
        return id_org;
    }

    public void setId_org(String id_org) {
        this.id_org = id_org;
    }

    public Boolean getHas_id_org() {
        return has_id_org;
    }

    public void setHas_id_org(Boolean has_id_org) {
        this.has_id_org = has_id_org;
    }

    public String getCd_role() {
        return cd_role;
    }

    public void setCd_role(String cd_role) {
        this.cd_role = cd_role;
    }

    public Boolean getHas_cd_role() {
        return has_cd_role;
    }

    public void setHas_cd_role(Boolean has_cd_role) {
        this.has_cd_role = has_cd_role;
    }

    public String getCd_department() {
        return cd_department;
    }

    public void setCd_department(String cd_department) {
        this.cd_department = cd_department;
    }

    public Boolean getHas_cd_department() {
        return has_cd_department;
    }

    public void setHas_cd_department(Boolean has_cd_department) {
        this.has_cd_department = has_cd_department;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getStaff_id() {
        return staff_id;
    }

    public void setStaff_id(String staff_id) {
        this.staff_id = staff_id;
    }

    public int getFingerprint_id() {
        return fingerprint_id;
    }

    public void setFingerprint_id(int fingerprint_id) {
        this.fingerprint_id = fingerprint_id;
    }

    public String getSocket_id() {
        return socket_id;
    }

    public void setSocket_id(String socket_id) {
        this.socket_id = socket_id;
    }

    public String getFile_picture() {
        return file_picture;
    }

    public void setFile_picture(String file_picture) {
        this.file_picture = file_picture;
    }

    public String getIp_address() {
        return ip_address;
    }

    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getEnum_availability() {
        return enum_availability;
    }

    public void setEnum_availability(int enum_availability) {
        this.enum_availability = enum_availability;
    }

    public int getIs_authenticated() {
        return is_authenticated;
    }

    public void setIs_authenticated(int is_authenticated) {
        this.is_authenticated = is_authenticated;
    }


}
