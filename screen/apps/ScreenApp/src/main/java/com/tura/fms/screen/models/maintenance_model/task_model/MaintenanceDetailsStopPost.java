package com.tura.fms.screen.models.maintenance_model.task_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MaintenanceDetailsStopPost {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("end_time")
    @Expose
    private String end_time;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEndTime() {
        return end_time;
    }

    public void setEndTime(String end_time) {
        this.end_time = end_time;
    }

}