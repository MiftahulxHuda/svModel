package com.tura.fms.screen.models.ecu;

public class ECUData {

    public static final String HOUR_METER = "hour_meter";

    public static final String FUEL_RATE = "fuel_rate";

    public static final String DISTANCE = "distance";

    public static final String ENGINE_TEMPERATURE = "engine_temperature";

    public static final String PAYLOAD = "payload";

    public static final String VEHICLE_SPEED = "vehicle_speed";

    public static final String GPS = "gps";
}
