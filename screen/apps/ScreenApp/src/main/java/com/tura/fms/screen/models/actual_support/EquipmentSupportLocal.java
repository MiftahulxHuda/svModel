package com.tura.fms.screen.models.actual_support;

import java.util.HashMap;
import java.util.Map;

public class EquipmentSupportLocal {

    private String id;
    private String idOrg;
    private Boolean hasId_org;
    private Object idOperator;
    private Boolean hasId_operator;
    private String cdOperation;
    private Boolean hasCd_operation;
    private String cdEquipment;
    private Boolean hasCd_equipment;
    private String name;
    private Integer capacityReal;
    private Integer capacityLabs;
    private Integer enumAvailability;
    private Integer isDeleted;
    private Integer dateSubmitted;
    private Integer dateModified;
    private Integer loadWeight;
    private Integer emptyWeight;
    private Integer capacityLabs2;
    private Integer capacityLabs3;
    private Integer hourMeter;
    private Object brand;
    private String _class;
    private Object code;
    private Object json;
    private Object idLocation;
    private Boolean hasId_location;
    private Object hmStart;
    private Object hmNow;
    private Object hmDate;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_org() {
        return idOrg;
    }

    public void setId_org(String idOrg) {
        this.idOrg = idOrg;
    }

    public Boolean getHasId_org() {
        return hasId_org;
    }

    public void setHasId_org(Boolean hasId_org) {
        this.hasId_org = hasId_org;
    }

    public Object getId_operator() {
        return idOperator;
    }

    public void setId_operator(Object idOperator) {
        this.idOperator = idOperator;
    }

    public Boolean getHasId_operator() {
        return hasId_operator;
    }

    public void setHasId_operator(Boolean hasId_operator) {
        this.hasId_operator = hasId_operator;
    }

    public String getCd_operation() {
        return cdOperation;
    }

    public void setCd_operation(String cdOperation) {
        this.cdOperation = cdOperation;
    }

    public Boolean getHasCd_operation() {
        return hasCd_operation;
    }

    public void setHasCd_operation(Boolean hasCd_operation) {
        this.hasCd_operation = hasCd_operation;
    }

    public String getCd_equipment() {
        return cdEquipment;
    }

    public void setCd_equipment(String cdEquipment) {
        this.cdEquipment = cdEquipment;
    }

    public Boolean getHasCd_equipment() {
        return hasCd_equipment;
    }

    public void setHasCd_equipment(Boolean hasCd_equipment) {
        this.hasCd_equipment = hasCd_equipment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCapacity_real() {
        return capacityReal;
    }

    public void setCapacity_real(Integer capacityReal) {
        this.capacityReal = capacityReal;
    }

    public Integer getCapacity_labs() {
        return capacityLabs;
    }

    public void setCapacity_labs(Integer capacityLabs) {
        this.capacityLabs = capacityLabs;
    }

    public Integer getEnum_availability() {
        return enumAvailability;
    }

    public void setEnum_availability(Integer enumAvailability) {
        this.enumAvailability = enumAvailability;
    }

    public Integer getIs_deleted() {
        return isDeleted;
    }

    public void setIs_deleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Integer getDate_submitted() {
        return dateSubmitted;
    }

    public void setDate_submitted(Integer dateSubmitted) {
        this.dateSubmitted = dateSubmitted;
    }

    public Integer getDate_modified() {
        return dateModified;
    }

    public void setDate_modified(Integer dateModified) {
        this.dateModified = dateModified;
    }

    public Integer getLoad_weight() {
        return loadWeight;
    }

    public void setLoad_weight(Integer loadWeight) {
        this.loadWeight = loadWeight;
    }

    public Integer getEmpty_weight() {
        return emptyWeight;
    }

    public void setEmpty_weight(Integer emptyWeight) {
        this.emptyWeight = emptyWeight;
    }

    public Integer getCapacity_labs2() {
        return capacityLabs2;
    }

    public void setCapacity_labs2(Integer capacityLabs2) {
        this.capacityLabs2 = capacityLabs2;
    }

    public Integer getCapacity_labs3() {
        return capacityLabs3;
    }

    public void setCapacity_labs3(Integer capacityLabs3) {
        this.capacityLabs3 = capacityLabs3;
    }

    public Integer getHour_meter() {
        return hourMeter;
    }

    public void setHour_meter(Integer hourMeter) {
        this.hourMeter = hourMeter;
    }

    public Object getBrand() {
        return brand;
    }

    public void setBrand(Object brand) {
        this.brand = brand;
    }

    public String getClass_() {
        return _class;
    }

    public void setClass_(String _class) {
        this._class = _class;
    }

    public Object getCode() {
        return code;
    }

    public void setCode(Object code) {
        this.code = code;
    }

    public Object getJson() {
        return json;
    }

    public void setJson(Object json) {
        this.json = json;
    }

    public Object getId_location() {
        return idLocation;
    }

    public void setId_location(Object idLocation) {
        this.idLocation = idLocation;
    }

    public Boolean getHasId_location() {
        return hasId_location;
    }

    public void setHasId_location(Boolean hasId_location) {
        this.hasId_location = hasId_location;
    }

    public Object getHm_start() {
        return hmStart;
    }

    public void setHm_start(Object hmStart) {
        this.hmStart = hmStart;
    }

    public Object getHm_now() {
        return hmNow;
    }

    public void setHm_now(Object hmNow) {
        this.hmNow = hmNow;
    }

    public Object getHm_date() {
        return hmDate;
    }

    public void setHm_date(Object hmDate) {
        this.hmDate = hmDate;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}