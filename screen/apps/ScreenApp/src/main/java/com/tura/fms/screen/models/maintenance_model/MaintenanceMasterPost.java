package com.tura.fms.screen.models.maintenance_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MaintenanceMasterPost {

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("maintenance_group_id")
    @Expose
    private String maintenance_group_id;

    @SerializedName("problem_id")
    @Expose
    private String problem_id;

    @SerializedName("equipment_id")
    @Expose
    private String equipment_id;

    @SerializedName("maintenance_request_id")
    @Expose
    private String maintenance_request_id;

    @SerializedName("hm")
    @Expose
    private String hm;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("foreman_id")
    @Expose
    private String foreman_id;

    @SerializedName("is_deleted")
    @Expose
    private int is_deleted;

    @SerializedName("completed_at")
    @Expose
    private String completed_at;

    @SerializedName("location_id")
    @Expose
    private String location_id;

    public String getCompleted_at() {
        return completed_at;
    }

    public void setCompleted_at(String completed_at) {
        this.completed_at = completed_at;
    }

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMaintenance_group_id() {
        return maintenance_group_id;
    }

    public void setMaintenance_group_id(String maintenance_group_id) {
        this.maintenance_group_id = maintenance_group_id;
    }

    public String getProblem_id() {
        return problem_id;
    }

    public void setProblem_id(String problem_id) {
        this.problem_id = problem_id;
    }

    public String getEquipment_id() {
        return equipment_id;
    }

    public void setEquipment_id(String equipment_id) {
        this.equipment_id = equipment_id;
    }

    public String getMaintenance_request_id() {
        return maintenance_request_id;
    }

    public void setMaintenance_request_id(String maintenance_request_id) {
        this.maintenance_request_id = maintenance_request_id;
    }

    public String getHM() {
        return hm;
    }

    public void setHM(String hm) {
        this.hm = hm;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getForeman_id() {
        return foreman_id;
    }

    public void setForeman_id(String foreman_id) {
        this.foreman_id = foreman_id;
    }

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }

}
