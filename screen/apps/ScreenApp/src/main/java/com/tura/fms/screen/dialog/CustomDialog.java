package com.tura.fms.screen.dialog;

import java.net.URL;
import java.util.ResourceBundle;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.ResponseModel;
import com.tura.fms.screen.models.hour_meter.DailyHourMeter;
import com.tura.fms.screen.models.hour_meter.LogOutHourMeter;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import javafx.scene.control.Button;

public class CustomDialog implements Initializable {

    @FXML
    private TextField tfHm;

    @FXML
    private Button buttonLogout;

    @FXML
    private Button buttonCancel;

    private Boolean clicked = false;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        clicked = false;

        tfHm.setText("0");
        tfHm.setPromptText("0-9");
        tfHm.getProperties().put("vkType", "numeric");
        tfHm.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d*")) {
                    tfHm.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
        // tfHm.setOnKeyPressed((KeyEvent ke) -> {
        // if (ke.getCode().equals(KeyCode.ENTER)) {
        // try {
        // // buttonLogout.fire();
        // MainApp.popup.hide();
        // } catch (Exception ex) {
        // ex.printStackTrace();
        // }
        // }
        // });
    }

    @FXML
    void actionLogOut(ActionEvent event) {
        if (clicked) {
            return;
        }

        clicked = true;

        Double ihm = Double.parseDouble(tfHm.getText().trim());

        if (ihm <= MainApp.latestHourMeter) {
            hideStage(event);
            Utilities.showMessageDialog("Hour Meter Tidak Valid", "",
                    "HM lebih kecil/sama dengan HM terakhir (" + MainApp.latestHourMeter.toString() + ")",
                    new Runnable() {

                        @Override
                        public void run() {
                            showStage(event);
                        }
                    });
            clicked = false;
            return;
        }

        DailyHourMeter dhm = new DailyHourMeter();
        if (Utilities.isLoaderHauler()) {
            dhm.setActualId(MainApp.actualInfo.getId());
        } else {
            dhm.setActualSupportId(MainApp.actualSupport.getId());
        }
        dhm.setHm(ihm);
        dhm.setEquipmentId(MainApp.equipmentLocal.getId());
        dhm.setIsDeleted(0);

        if (NetworkConnection.getServiceStatus()) {
            Call<ResponseModel> call = APIClient.getInstance().postDailyStop(MainApp.requestHeader, dhm);
            call.enqueue(new Callback<ResponseModel>() {

                @Override
                public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                    MainApp.latestHourMeter = ihm;

                    clicked = false;

                    Utilities.doLogOut(event, null);

                    closeStage(event);
                }

                @Override
                public void onFailure(Call<ResponseModel> call, Throwable t) {
                    clicked = false;
                    t.printStackTrace();
                    closeStage(event);
                }
            });
        } else {
            // save local hm
            Utilities.doLogOut(event, null);
            closeStage(event);
        }
    }

    @FXML
    void actionCancel(ActionEvent event) {
        closeStage(event);
    }

    private void closeStage(ActionEvent event) {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                Node source = (Node) event.getSource();
                Stage stage = (Stage) source.getScene().getWindow();
                if (stage != null) {
                    stage.close();
                }
            }
        });
    }

    private void showStage(ActionEvent event) {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                Node source = (Node) event.getSource();
                Stage stage = (Stage) source.getScene().getWindow();
                if (stage != null) {
                    stage.show();
                }
            }
        });
    }

    private void hideStage(ActionEvent event) {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                Node source = (Node) event.getSource();
                Stage stage = (Stage) source.getScene().getWindow();
                if (stage != null) {
                    stage.hide();
                }
            }
        });
    }

}