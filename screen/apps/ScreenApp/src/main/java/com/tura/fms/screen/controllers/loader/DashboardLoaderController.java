package com.tura.fms.screen.controllers.loader;

import static javafx.geometry.NodeOrientation.LEFT_TO_RIGHT;
import static javafx.scene.shape.StrokeType.OUTSIDE;
import static org.javalite.app_config.AppConfig.p;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.constanst.IKafkaConstant;
import com.tura.fms.screen.helpers.FatigueUI;
import com.tura.fms.screen.helpers.GPS;
import com.tura.fms.screen.helpers.NavigationUI;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.actual_model.ActualDetail;
import com.tura.fms.screen.models.dashboard_model.DashboardModel;
import com.tura.fms.screen.models.fatigue_model.FatigueModel;
import com.tura.fms.screen.models.operation_log_detail.OperationLogDetailLocal;
import com.tura.fms.screen.network.NetworkConnection;
import com.tura.fms.screen.presenter.Loader.DashboardLoaderPresenter;

import org.json.JSONObject;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Duration;

import javafx.scene.control.ScrollPane;

public class DashboardLoaderController implements Initializable {

    private NavigationUI navigationUI = new NavigationUI();

    @FXML
    private Text TitleBar;

    @FXML
    private Text textActivity;

    @FXML
    private GridPane gridata;

    private ObservableList<DashboardModel> recordsData = FXCollections.observableArrayList();

    // SessionFMS sessionFMS;

    @FXML
    private VBox vboxRoot;

    @FXML
    private Label labelMaterial;

    @FXML
    private Text textHour;

    Timeline clock;

    @FXML
    private Label mode_connect;

    DashboardLoaderPresenter dashboardPresenter;

    // @FXML
    // private Text lblinfo;

    @FXML
    private Text opname;

    FatigueUI fatigueUI;
    String time_fatigue = "";
    private ObservableList<FatigueModel> fatigueData = FXCollections.observableArrayList();

    Date date = new Date();

    @FXML
    private StackPane stackPane;

    @FXML
    private Label location_label;

    @FXML
    private Label destination_label;

    @FXML
    private Label exavator_label;

    @FXML
    private HBox hBoxCycle1;

    @FXML
    private GridPane gridHauler;

    @FXML
    private VBox paneLog;

    @FXML
    private ScrollPane scrollPane1;

    @FXML
    private ScrollPane scrollPane2;

    public static List<Button> buttonHauler = new ArrayList<>();

    public static List<Button> buttonCycle1 = new ArrayList<>();

    // public static List<Button> buttonCycle2 = new ArrayList<>();

    Integer fatigueTimeoutConfig;

    Integer fatigueTimeout = 0;

    Boolean showFatigue = false;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        NavigationUI.publicStackPane = stackPane;

        System.out.println("------------------------- dashboard loader");

        TitleBar.setText(MainApp.config("config.unit_id"));

        fatigueUI = new FatigueUI();

        opname.setText(MainApp.user.getName());

        dashboardPresenter = new DashboardLoaderPresenter();

        Utilities.setSatuan();

        // // set indikator connection
        // mode_connect.setText(NetworkConnection.getStringStatus());

        // set titme fatigue
        Utilities.setupFatigueTime();

        gridata.setGridLinesVisible(true);

        paneLog.getChildren().add(0, MainApp.textLog);

        String fTC = MainApp.remoteConfig.get(ConstantValues.SCREEN_CONFIG_FATIGUE_TIMEOUT);
        if (fTC != null) {
            fatigueTimeoutConfig = Integer.parseInt(fTC);
        } else {
            fatigueTimeoutConfig = ConstantValues.SCREEN_CONFIG_FATIGUE_DEFAULT_TIMEOUT;
        }

        MainApp.clockDashboard = new Timeline(new KeyFrame(Duration.ZERO, e -> {
            clearModel();

            recordsData = dashboardPresenter.runConsumer();

            tampilkanData(recordsData);

            String netS = NetworkConnection.getStringStatus();
            mode_connect.setText(netS);
            mode_connect.getStyleClass().clear();
            if (netS.equals("Online")) {
                mode_connect.getStyleClass().add("font-online");
            } else {
                mode_connect.getStyleClass().add("font-offline");
            }

            fatigueUI.showFatigePopup(stackPane, Utilities.fatigueData, new Runnable() {

                @Override
                public void run() {
                    if (!showFatigue) {
                        showFatigue = true;
                        fatigueTimeout = 0;
                    }
                }

            }, new Runnable() {

                @Override
                public void run() {
                    if (showFatigue) {
                        fatigueUI.closeAlertDialog();
                    }
                    showFatigue = false;
                    fatigueTimeout = 0;
                    Utilities.postFatigueStatus();
                }
            });

            fatigueTimeout += MainApp.dashboardInterval;
            if (fatigueTimeout > fatigueTimeoutConfig && showFatigue) {
                MainApp.fatigueLevel++;
                fatigueTimeout = 0;
                fatigueUI.closeAlertDialog();
                fatigueUI.showMessage("Fatigue test timeout, LEVEL " + MainApp.fatigueLevel.toString(), stackPane);
            }
            if (MainApp.fatigueLevel >= ConstantValues.SCREEN_CONFIG_FATIGUE_MAX_LEVEL) {
                showFatigue = false;
                fatigueTimeout = 0;
                fatigueUI.closeAlertDialog();
                Utilities.postFatigueStatus();
            }

            /**
             * check activity tum
             */
            if (MainApp.latestActivity != null) {
                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_DOWN)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            Utilities.showMessageDialog("Unit Breakdown", "",
                                    "Unit breakdown, anda akan logout dari aplikasi");
                            Utilities.doLogOut(null, (Stage) location_label.getScene().getWindow());
                        }
                    });
                }
            }

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    String tm = Utilities.getCurrentTimeString();
                    textHour.setText(tm);

                    if (MainApp.actualInfo != null) {
                        if (MainApp.actualInfo.getCd_material().getType().toUpperCase().trim().equals("OVERBURDEN")) {
                            labelMaterial.setText("OB");
                        } else {
                            labelMaterial.setText(MainApp.actualInfo.getCd_material().getType().toUpperCase().trim());
                        }

                    } else {
                        labelMaterial.setText("?");

                    }

                    if (MainApp.actualLoader != null) {
                        String date = Utilities.timeStampToDate(MainApp.actualLoader.getDate(), "tanggal");
                        location_label.setText("DATE : " + date);
                        destination_label.setText("SHIFT : " + MainApp.actualLoader.getCd_shift().getName());
                        exavator_label.setText("DISPOSAL : " + MainApp.actualLoader.getId_destination().getName());

                    } else {
                        String date = Utilities.timeStampToDate(Utilities.getCurrentUnix(), "tanggal");
                        location_label.setText("DATE : " + date);
                        destination_label.setText("SHIFT : "
                                + (Utilities.getCurrentShift().equals(ConstantValues.SHIFT_DAY) ? "Day" : "Night"));
                        exavator_label.setText("DISPOSAL : ?");

                    }
                }
            });

        }), new KeyFrame(Duration.seconds(MainApp.dashboardInterval)));

        MainApp.clockDashboard.setCycleCount(Animation.INDEFINITE);
        MainApp.clockDashboard.play();

        DashboardLoaderController.buttonHauler = new ArrayList<>();

        // create button hauler
        if (MainApp.haulerList != null && MainApp.isCanPost()) {

            Double width = 200.0 * (MainApp.haulerList.size() / 2);
            gridHauler.setPrefWidth(width);

            for (ActualDetail item : MainApp.haulerList) {

                Button btn = new Button(item.getId_hauler().getName());
                btn.setId(item.getId_hauler().getId());
                btn.setPadding(new Insets(5));
                btn.setPrefHeight(20);
                btn.setPrefWidth(200);
                btn.getStyleClass().add("button-raised");
                btn.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        MainApp.activeHauler = MainApp.haulerList.indexOf(item);
                        MainApp.bucketCount = 0;
                        DashboardLoaderController.buttonCycle1.get(0).setDisable(false);

                        for (Button btn : DashboardLoaderController.buttonHauler) {
                            btn.setStyle("-fx-background-color: #520a12;");
                        }
                        btn.setStyle("-fx-background-color: BLUE;");

                        MainApp.tonnage = 0.0;
                        MainApp.tonnageUjiPetik = 0.0;
                        MainApp.tonnageBucket = 0.0;
                        MainApp.tonnageTimbangan = 0.0;
                        MainApp.dashboardInfo.setTonnagePayload(0.0);
                        MainApp.dashboardInfo.setTonnageUjiPetik(0.0);
                        MainApp.dashboardInfo.setTonnageBucket(0.0);
                        MainApp.dashboardInfo.setTonnageTimbangan(0.0);

                        Utilities.setNotificationText(item.getId_hauler().getName() + " active, ready for loading");
                    }
                });
                DashboardLoaderController.buttonHauler.add(btn);
            }

        } else {
            /**
             * offline mode atau tidak ada hauler
             */

            Button btn = new Button("Prepare Loading");
            btn.setId("1");
            btn.setPadding(new Insets(5));
            btn.setPrefHeight(20);
            btn.setPrefWidth(200);
            btn.getStyleClass().add("button-raised");
            btn.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    MainApp.activeHauler = 0;
                    MainApp.bucketCount = 0;
                    DashboardLoaderController.buttonCycle1.get(0).setDisable(false);

                    btn.setStyle("-fx-background-color: BLUE;");

                    MainApp.tonnage = 0.0;
                    MainApp.tonnageUjiPetik = 0.0;
                    MainApp.tonnageBucket = 0.0;
                    MainApp.tonnageTimbangan = 0.0;
                    MainApp.dashboardInfo.setTonnagePayload(0.0);
                    MainApp.dashboardInfo.setTonnageUjiPetik(0.0);
                    MainApp.dashboardInfo.setTonnageBucket(0.0);
                    MainApp.dashboardInfo.setTonnageTimbangan(0.0);

                    Utilities.setNotificationText("Ready for loading");
                }
            });
            DashboardLoaderController.buttonHauler.add(btn);
        }

        DashboardLoaderController.buttonCycle1 = new ArrayList<>();
        Button btnFirstBucket = new Button("First Bucket");
        btnFirstBucket.setPadding(new Insets(10));
        btnFirstBucket.setId("0");
        btnFirstBucket.setPrefHeight(20);
        btnFirstBucket.getStyleClass().add("button-raised");
        btnFirstBucket.setDisable(true);
        btnFirstBucket.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                MainApp.bucketCount = 1;
                for (Button btn : DashboardLoaderController.buttonCycle1) {
                    btn.setDisable(false);
                }
                DashboardLoaderController.buttonCycle1.get(0).setDisable(true);
                try {
                    firstBucketAction(event);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    MainApp.log.error("dashboard loader", ex);
                }

            }
        });

        DashboardLoaderController.buttonCycle1.add(btnFirstBucket);

        for (int i = 3; i <= 15; i++) {
            Button btn = new Button(String.valueOf(i));
            btn.setId(String.valueOf(i));
            btn.setPadding(new Insets(10, 15, 10, 15));
            btn.setDisable(true);
            btn.setPrefWidth(73);
            btn.setPrefHeight(55);
            btn.getStyleClass().add("button-cancel");
            btn.setStyle("-fx-background-color: BLUE;");
            btn.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {
                    String id = btn.getId();
                    MainApp.bucketCount = Integer.parseInt(id);
                    for (Button btns : DashboardLoaderController.buttonCycle1) {
                        String lId = btns.getId();
                        if (lId != null && id != null) {
                            if (Integer.parseInt(lId) <= Integer.parseInt(id)) {
                                btns.setDisable(true);
                            }
                        }
                    }
                    DashboardLoaderController.buttonHauler.get(MainApp.activeHauler)
                            .setStyle("-fx-background-color: #520a12;");
                    try {
                        fullBucketAction(event);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        MainApp.log.error("dashboard loader", ex);
                    }
                }
            });
            DashboardLoaderController.buttonCycle1.add(btn);
        }

        hBoxCycle1.getChildren().addAll(DashboardLoaderController.buttonCycle1);

        int cIndex = 0;
        for (int rr = 0; rr < 2; rr++) {
            for (int cc = 0; cc < 6; cc++) {
                if (cIndex < buttonHauler.size()) {
                    gridHauler.addRow(rr, DashboardLoaderController.buttonHauler.get(cIndex));
                }
                cIndex++;
            }
        }
    }

    public void clearModel() {
        recordsData.clear();
        gridata.getChildren().clear();

    }

    private void tampilkanData(ObservableList<DashboardModel> recordsDataParam) {

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                textActivity.setText(MainApp.latestActivity.getCdTum().getName().replaceAll(" Time", "") + ": "
                        + MainApp.latestActivity.getCdActivity().getName());
                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_WORKING)) {
                    textActivity.setFill(Color.web(ConstantValues.COLOR_WORKING, 1));
                } else if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_DELAY)) {
                    textActivity.setFill(Color.web(ConstantValues.COLOR_DELAY, 1));
                } else if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_IDLE)) {
                    textActivity.setFill(Color.web(ConstantValues.COLOR_IDLE, 1));
                } else if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_DOWN)) {
                    textActivity.setFill(Color.web(ConstantValues.COLOR_DOWN, 1));
                } else if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_RFU)) {
                    textActivity.setFill(Color.web(ConstantValues.COLOR_RFU, 1));
                } else if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_STANDBY)) {
                    textActivity.setFill(Color.web(ConstantValues.COLOR_STANDBY, 1));
                } else if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_ASSIGNED)) {
                    textActivity.setFill(Color.web(ConstantValues.COLOR_ASSIGNED, 1));
                } else if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_MAINTENANCE)) {
                    textActivity.setFill(Color.web(ConstantValues.COLOR_MAINTENANCE, 1));
                }
            }
        });

        int Col = 0;
        int Row = 0;

        for (int i = 0; i < recordsDataParam.size(); i++) {

            Text texthbox = new Text();
            VBox vboxForButtons = new VBox();
            vboxForButtons.setPrefWidth(256);
            vboxForButtons.setPrefHeight(100);

            HBox hboxForButtons = new HBox();
            // ImageView gambar = new ImageView(new Image("/images/icondash.png", 120, 100,
            // false, true));
            Text textdashboard = new Text();

            vboxForButtons.setAlignment(Pos.TOP_CENTER);
            vboxForButtons.setNodeOrientation(LEFT_TO_RIGHT);
            // vboxForButtons.setPrefHeight(99.0);
            // vboxForButtons.setPrefWidth(50.0);
            vboxForButtons.getStyleClass().add("button-dashboard");

            // set untuk button

            // hboxForButtons.setPrefHeight(50.0);
            // hboxForButtons.setPrefWidth(180.0);
            hboxForButtons.setPadding(new Insets(5, 10, 10, 10));

            texthbox.setStrokeType(OUTSIDE);
            texthbox.setText(recordsDataParam.get(i).getName());
            texthbox.setStrokeWidth(0.0);
            texthbox.setWrappingWidth(170.0);
            texthbox.setFont(Font.font("Verdana", 18));

            if (i == 5) {
                texthbox.setFont(Font.font("Verdana", 16));
            }
            // texthbox.getStyleClass().add("text-dashboard");
            // texthbox.getStyleClass().add("font-white");
            texthbox.setFill(Color.WHITE);

            // gambar.setFitHeight(40.0);
            // gambar.setFitWidth(33.0);
            // gambar.setPickOnBounds(true);
            // gambar.setPreserveRatio(true);
            // gambar.setImage(new Image("@../images/icondash.png",160,160,false,true));

            hboxForButtons.getChildren().add(texthbox);
            // hboxForButtons.getChildren().add(gambar);

            // set text dashboard
            // textdashboard.setFill(Color.web("0x15b967",1.0));
            textdashboard.setText(recordsDataParam.get(i).getDescription());
            textdashboard.getStyleClass().add("text-dashboard");
            textdashboard.getStyleClass().add("font-white");
            // extdashboard.setFont(Font.font("Verdana", 20));
            textdashboard.setFill(Color.GREENYELLOW);

            VBox.setMargin(textdashboard, new Insets(0, 0, 10, 0));

            if (recordsDataParam.get(i).getId().equals("3")) {

                // perlakuan khusus untuk tonnage/payload

                Text textUjiPetik = new Text();
                textUjiPetik.setStrokeType(OUTSIDE);
                textUjiPetik.setTextAlignment(TextAlignment.RIGHT);
                Double cap = Utilities.getUjiPetikHauler();

                textUjiPetik.setText(cap.toString() + MainApp.satuan);
                textUjiPetik.setStrokeWidth(0.0);
                textUjiPetik.setWrappingWidth(80);
                textUjiPetik.setFont(Font.font("Verdana", 16));
                hboxForButtons.getChildren().add(textUjiPetik);

                vboxForButtons.getChildren().add(hboxForButtons);

                HBox hboxTonnage = new HBox();
                hboxTonnage.setPadding(new Insets(5, 5, 5, 5));
                Text tonPayload = new Text();
                tonPayload.setStrokeType(OUTSIDE);
                tonPayload.setTextAlignment(TextAlignment.CENTER);
                tonPayload.setText(MainApp.dashboardInfo.getTonnagePayload().toString() + MainApp.satuan);
                tonPayload.setStrokeWidth(0.0);
                tonPayload.setWrappingWidth(130);
                tonPayload.setFont(Font.font("Verdana", 18));
                tonPayload.setFill(Color.GREENYELLOW);
                Text tonBucket = new Text();
                tonBucket.setStrokeType(OUTSIDE);
                tonBucket.setTextAlignment(TextAlignment.CENTER);
                tonBucket.setText(MainApp.dashboardInfo.getTonnageBucket().toString() + MainApp.satuan);
                tonBucket.setStrokeWidth(0.0);
                tonBucket.setWrappingWidth(130);
                tonBucket.setFont(Font.font("Verdana", 18));
                tonBucket.setFill(Color.BLUE);
                hboxTonnage.getChildren().add(tonPayload);
                hboxTonnage.getChildren().add(tonBucket);

                vboxForButtons.getChildren().add(hboxTonnage);

                if (MainApp.dashboardInfo.getTonnageBucket() < cap) {
                    textUjiPetik.setFill(Color.YELLOW);
                    vboxForButtons.setStyle("-fx-background-color: RED;");
                } else {
                    textUjiPetik.setFill(Color.RED);
                    vboxForButtons.setStyle("-fx-background-color: GREEN;");
                }

                Double dbl = cap + (cap / 10);

                if (MainApp.dashboardInfo.getTonnageBucket() > dbl) {
                    vboxForButtons.setStyle("-fx-background-color: GRAY;");
                }

            } else {

                vboxForButtons.getChildren().add(hboxForButtons);
                vboxForButtons.getChildren().add(textdashboard);
            }

            // vboxForButtons.getChildren().add(hboxForButtons);
            // vboxForButtons.getChildren().add(textdashboard);

            gridata.add(vboxForButtons, Col, Row);

            Col++;

            if (Col > 2) {
                // Reset Column
                Col = 0;
                // Next Row
                Row++;
            }
        }
    }

    @FXML
    public void logoutAction(ActionEvent event) throws Exception {
        Utilities.showHourMeterDialog(event);
    }

    @FXML
    public void lostimeAction(ActionEvent event) throws Exception {
        // dashboardPresenter.connect_close();
        MainApp.clockDashboard.stop();
        navigationUI.windowsTab(p("app.linkLosttime"), p("app.titleLosttime"), event);
    }

    @FXML
    public void breakdownAction(ActionEvent event) throws Exception {
        MainApp.clockDashboard.stop();
        navigationUI.windowsTab(p("app.linkBreakdown"), p("app.titleBreakdown"), event);
    }

    @FXML
    public void idleAction(ActionEvent event) throws Exception {
        // dashboardPresenter.connect_close();
        MainApp.clockDashboard.stop();
        navigationUI.windowsTab(p("app.linkIdle"), p("app.titleIdle"), event);
    }

    // public void scrollingText() {
    // TranslateTransition tt = new TranslateTransition(Duration.millis(10000),
    // lblinfo);
    // tt.setFromX(0 - lblinfo.getWrappingWidth() - 10); // setFromX sets the
    // starting position, coming from the left
    // // and going to the right.
    // int boundWidth = 760;
    // tt.setToX(lblinfo.getWrappingWidth() + boundWidth); // setToX sets to target
    // position, go beyond the right side
    // // of the screen.
    // tt.setCycleCount(Timeline.INDEFINITE); // repeats for ever
    // tt.setAutoReverse(true); // Always start over
    // tt.play();
    // }

    @FXML
    public void safetyAction(ActionEvent event) throws Exception {
        MainApp.clockDashboard.stop();
        // scheduledExecutorService.shutdownNow();
        navigationUI.windowsTab(p("app.linkSafetyIssue"), p("app.titleSafety"), event);
    }

    @FXML
    public void onDutyAction(ActionEvent event) throws Exception {
        MainApp.clockDashboard.stop();
        navigationUI.windowsTab(p("app.linkOnDuty"), p("app.titleOnDuty"), event);
    }

    @FXML
    public void activityAction(ActionEvent event) throws Exception {
        // dashboardPresenter.connect_close();
        MainApp.clockDashboard.stop();
        navigationUI.windowsTab(p("app.linkActivity"), p("app.titleActivity"), event);
    }

    public void firstBucketAction(ActionEvent event) throws Exception {

        if (MainApp.isCanPost() && MainApp.haulerList != null) {

            if (MainApp.activeHauler != null && MainApp.activeHauler < MainApp.haulerList.size()) {

                OperationLogDetailLocal data = new OperationLogDetailLocal();
                data.setDate("current");
                data.setIdActual(MainApp.haulerList.get(MainApp.activeHauler).getId());
                data.setCdTime(ConstantValues.TIME_CYCLE_FIRST_BUCKET);
                // if (MainApp.equipmentLocal != null) {
                data.setIdLoader(MainApp.equipmentLocal.getId());
                // }
                data.setIdOperator(MainApp.user.getId());
                data.setCdEquipmentType(MainApp.config("config.unit_type"));
                data.setIdLocation(MainApp.haulerList.get(MainApp.activeHauler).getId_location().getId());
                data.setIdEquipment(MainApp.haulerList.get(MainApp.activeHauler).getId_hauler().getId());
                data.setBucketCount(1);

                GPS gps = Utilities.getCurrentGPS();
                data.setLat(gps.getLat());
                data.setLon(gps.getLon());

                List<Object> eqs = new ArrayList<Object>();
                eqs.add(data);
                MainApp.operationLogDetailDB.postBulkData(eqs);

                // todo: send socket cycle
                SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
                Date date = new Date();
                String strDate = formatter.format(date);
                JSONObject json = new JSONObject();
                json.put("date", strDate);
                json.put("type", "loader");
                json.put("sender", MainApp.equipmentLocal.getId());
                json.put("sender_name", MainApp.config("config.unit_id"));
                json.put("destination", MainApp.haulerList.get(MainApp.activeHauler).getId_hauler().getId());
                json.put("message", ConstantValues.CYCLE_FIRST_BUCKET);

                MainApp.producer.push(IKafkaConstant.TOPIC_PRODUCE_CYCLE, json.toString());

                // request payload to hauler
                Utilities.pushRequestData(ConstantValues.KAFKA_DATA_TYPE_PAYLOAD,
                        MainApp.haulerList.get(MainApp.activeHauler).getId_hauler().getId());

                Utilities.setNotificationText(
                        MainApp.haulerList.get(MainApp.activeHauler).getId_hauler().getName() + ", first load");

                Utilities.handleFirstBucketOnLoader();

            } else {

                System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                System.out.println("No active hauler");
                Utilities.setNotificationText("No active hauler");

            }

        } else {
            /**
             * offline mode
             */

            OperationLogDetailLocal data = new OperationLogDetailLocal();
            // data.setIdActual(MainApp.haulerList.get(MainApp.activeHauler).getId());
            data.setCdTime(ConstantValues.TIME_CYCLE_FIRST_BUCKET);
            data.setIdLoader(MainApp.equipmentLocal.getId());
            data.setIdOperator(MainApp.user.getId());
            data.setCdEquipmentType(MainApp.config("config.unit_type"));
            data.setDate(Utilities.getCurrentUnix().toString());
            data.setBucketCount(1);
            // data.setIdLocation(MainApp.haulerList.get(MainApp.activeHauler).getId_location().getId());
            // data.setIdEquipment(MainApp.haulerList.get(MainApp.activeHauler).getId_hauler().getId());

            GPS gps = Utilities.getCurrentGPS();
            data.setLat(gps.getLat());
            data.setLon(gps.getLon());

            MainApp.operationLogDetailDB.insertIgnore(data);

            Utilities.setNotificationText("First load");
        }
    }

    public void fullBucketAction(ActionEvent event) throws Exception {
        if (MainApp.isCanPost() && MainApp.haulerList != null) {

            if (MainApp.activeHauler != null && MainApp.activeHauler < MainApp.haulerList.size()) {
                OperationLogDetailLocal data = new OperationLogDetailLocal();
                data.setDate("current");
                data.setIdActual(MainApp.haulerList.get(MainApp.activeHauler).getId());
                data.setCdTime(ConstantValues.TIME_CYCLE_FULL_BUCKET);
                // if (MainApp.equipmentLocal != null) {
                data.setIdLoader(MainApp.equipmentLocal.getId());
                // }
                data.setIdOperator(MainApp.user.getId());
                data.setCdEquipmentType(MainApp.config("config.unit_type"));
                data.setIdLocation(MainApp.haulerList.get(MainApp.activeHauler).getId_location().getId());
                data.setIdEquipment(MainApp.haulerList.get(MainApp.activeHauler).getId_hauler().getId());
                data.setBucketCount(MainApp.bucketCount);

                GPS gps = Utilities.getCurrentGPS();
                data.setLat(gps.getLat());
                data.setLon(gps.getLon());

                List<Object> eqs = new ArrayList<Object>();
                eqs.add(data);
                MainApp.operationLogDetailDB.postBulkData(eqs);

                // todo: send socket cycle
                SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
                Date date = new Date();
                String strDate = formatter.format(date);
                JSONObject json = new JSONObject();
                json.put("date", strDate);
                json.put("type", "loader");
                json.put("sender", MainApp.equipmentLocal.getId());
                json.put("sender_name", MainApp.config("config.unit_id"));
                json.put("destination", MainApp.haulerList.get(MainApp.activeHauler).getId_hauler().getId());
                json.put("message", ConstantValues.CYCLE_FULL_BUCKET);
                json.put("bucket_count", MainApp.bucketCount);

                MainApp.producer.push(IKafkaConstant.TOPIC_PRODUCE_CYCLE, json.toString());

                /**
                 * bucket Count jangan diset ke 0, karena menunggu kafka response untuk
                 * perhitungan tonase payload
                 */

                Utilities.setNotificationText(
                        MainApp.haulerList.get(MainApp.activeHauler).getId_hauler().getName() + ", full load");

                Utilities.handleFullBucketOnLoader();

            } else {

                System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                System.out.println("No active hauler");

            }

        } else {
            /**
             * offline mode
             */

            OperationLogDetailLocal data = new OperationLogDetailLocal();
            // data.setIdActual(MainApp.haulerList.get(MainApp.activeHauler).getId());
            data.setCdTime(ConstantValues.TIME_CYCLE_FULL_BUCKET);
            data.setIdLoader(MainApp.equipmentLocal.getId());
            data.setIdOperator(MainApp.user.getId());
            data.setCdEquipmentType(MainApp.config("config.unit_type"));
            data.setBucketCount(MainApp.bucketCount);
            data.setDate(Utilities.getCurrentUnix().toString());
            // data.setIdLocation(MainApp.haulerList.get(MainApp.activeHauler).getId_location().getId());
            // data.setIdEquipment(MainApp.haulerList.get(MainApp.activeHauler).getId_hauler().getId());

            GPS gps = Utilities.getCurrentGPS();
            data.setLat(gps.getLat());
            data.setLon(gps.getLon());

            MainApp.operationLogDetailDB.insertIgnore(data);

            Utilities.setNotificationText("Full load");

        }

    }

}
