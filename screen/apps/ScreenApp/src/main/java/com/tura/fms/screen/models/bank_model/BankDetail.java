package com.tura.fms.screen.models.bank_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BankDetail {

        @SerializedName("cd")
        @Expose
        private String cd;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("name_alias")
        @Expose
        private String nameAlias;
        @SerializedName("code_3")
        @Expose
        private String code3;
        @SerializedName("code_7")
        @Expose
        private String code7;
        @SerializedName("code_se")
        @Expose
        private String codeSe;
        @SerializedName("file_logo")
        @Expose
        private String fileLogo;
        @SerializedName("url_website")
        @Expose
        private String urlWebsite;
        @SerializedName("enum_bank")
        @Expose
        private Integer enumBank;

        @SerializedName("is_deleted")
        @Expose
        private Integer is_deleted;


        @SerializedName("json")
        @Expose
        private String json;


    public Integer getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(Integer is_deleted) {
        this.is_deleted = is_deleted;
    }

        public String getCd() {
            return cd;
        }

        public void setCd(String cd) {
            this.cd = cd;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNameAlias() {
            return nameAlias;
        }

        public void setNameAlias(String nameAlias) {
            this.nameAlias = nameAlias;
        }

        public String getCode3() {
            return code3;
        }

        public void setCode3(String code3) {
            this.code3 = code3;
        }

        public String getCode7() {
            return code7;
        }

        public void setCode7(String code7) {
            this.code7 = code7;
        }

        public String getCodeSe() {
            return codeSe;
        }

        public void setCodeSe(String codeSe) {
            this.codeSe = codeSe;
        }

        public String getFileLogo() {
            return fileLogo;
        }

        public void setFileLogo(String fileLogo) {
            this.fileLogo = fileLogo;
        }

        public String getUrlWebsite() {
            return urlWebsite;
        }

        public void setUrlWebsite(String urlWebsite) {
            this.urlWebsite = urlWebsite;
        }

        public Integer getEnumBank() {
            return enumBank;
        }

        public void setEnumBank(Integer enumBank) {
            this.enumBank = enumBank;
        }

        public String getJson() {
            return json;
        }

        public void setJson(String json) {
            this.json = json;
        }



}
