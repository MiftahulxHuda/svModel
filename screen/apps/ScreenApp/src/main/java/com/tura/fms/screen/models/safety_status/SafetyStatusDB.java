package com.tura.fms.screen.models.safety_status;

import java.io.IOException;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.database.Operation;
import com.tura.fms.screen.models.ResponseModel;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;

import retrofit2.Call;
import retrofit2.Response;

public class SafetyStatusDB extends Operation {

    public static final String TABLE_NAME = "SafetyStatus_Record";

    private static final String id = "id";
    private static final String date = "date";
    private static final String id_actual = "id_actual";
    private static final String id_operation_support = "id_operation_support";
    private static final String id_operation_loader = "id_operation_loader";
    private static final String cd_safety = "cd_safety";
    private static final String latitude = "latitude";
    private static final String longitude = "longitude";
    private static final String id_location = "id_location";
    private static final String id_operator = "id_operator";
    private static final String id_equipment = "id_equipment";
    private static final String sync_status = "sync_status";

    public SafetyStatusDB() {
        super(SafetyStatusDB.TABLE_NAME, ConstantValues.DATA_SAFETY_STATUS);
    }

    public void createTableSafetyStatus() {
        try {
            DatabaseMetaData dbm = conn().getMetaData();
            ResultSet rs = dbm.getTables(null, null, TABLE_NAME, null);
            Statement stmt = null;
            if (rs.next()) {
                System.out.println("Table safety status exists");
            } else {
                stmt = conn().createStatement();
                stmt.setQueryTimeout(10);
                stmt.executeUpdate(createTable());
                stmt.close();
                System.out.println("Table safety status created");
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String createTable() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + id + " TEXT PRIMARY KEY," + date + " NUMBER NULL,"
                + id_actual + " TEXT NULL," + id_operation_support + " TEXT NULL," + id_operation_loader + " TEXT NULL,"
                + cd_safety + " TEXT NULL," + latitude + " TEXT NULL," + longitude + " TEXT NULL," + id_location
                + " TEXT NULL," + id_operator + " TEXT NULL," + id_equipment + " TEXT NULL," + sync_status
                + " NUMBER NULL" + ")";
    }

    @Override
    public Object insertIgnore(Object eqa) {
        SafetyStatusLocal eq = (SafetyStatusLocal) eqa;
        PreparedStatement st = null;
        Boolean rs = null;
        try {
            String sql = "INSERT INTO " + TABLE_NAME + " " + "(" + id + "," + date + "," + id_actual + ","
                    + id_operation_support + "," + id_operation_loader + "," + cd_safety + "," + latitude + ","
                    + longitude + "," + id_location + "," + id_operator + "," + id_equipment + "," + sync_status + ")"
                    + " VALUES(?,?,?,?,?,?,?,?,?,?,?,0) ON CONFLICT(" + id + ") DO UPDATE SET " + id + "=?," + date
                    + "=?," + id_actual + "=?," + id_operation_support + "=?," + id_operation_loader + "=?," + cd_safety
                    + "=?," + latitude + "=?," + longitude + "=?," + id_location + "=?," + id_operator + "=?,"
                    + id_equipment + "=?;";

            st = conn().prepareStatement(sql);
            final String uuid = UUID.randomUUID().toString().replace("-", "");
            st.setString(1, uuid);
            st.setString(12, uuid);
            st.setLong(2, eq.getDate().longValue());
            st.setLong(13, eq.getDate().longValue());
            st.setString(3, eq.getIdActual());
            st.setString(14, eq.getIdActual());
            st.setString(4, eq.getIdOperationSupport());
            st.setString(15, eq.getIdOperationSupport());
            st.setString(5, eq.getIdOperationLoader());
            st.setString(16, eq.getIdOperationLoader());
            st.setString(6, eq.getCdSafety());
            st.setString(17, eq.getCdSafety());
            st.setDouble(7, eq.getLatitude().doubleValue());
            st.setDouble(18, eq.getLatitude().doubleValue());
            st.setDouble(8, eq.getLongitude().doubleValue());
            st.setDouble(19, eq.getLongitude().doubleValue());
            st.setString(9, eq.getIdLocation());
            st.setString(20, eq.getIdLocation());
            st.setString(10, eq.getIdOperator());
            st.setString(21, eq.getIdOperator());
            st.setString(11, eq.getIdEquipment());
            st.setString(22, eq.getIdEquipment());
            rs = st.execute();
            st.close();

            return this.getLocalOneById(id, uuid);

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (rs && st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
        return null;
    }

    @Override
    public void deleteData(String id) {
        String sql = "delete from " + TABLE_NAME + " where id=?";
        try {
            PreparedStatement ps = conn().prepareStatement(sql);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object getFromServer(Integer page, Integer size) {
        if (!NetworkConnection.getServiceStatus()) {
            return null;
        }
        // no action
        return null;
    }

    @Override
    public Integer getRemoteCount() {
        if (!NetworkConnection.getServiceStatus()) {
            return 0;
        }
        // no action
        return 0;
    }

    @Override
    public List<Object> getFlaggedData() {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE sync_status=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, "0");
            ResultSet cursor = preparedStatement.executeQuery();
            List<Object> result = new ArrayList<Object>();
            while (cursor.next()) {
                SafetyStatusLocal eq = new SafetyStatusLocal();
                eq.setId(cursor.getString(id));
                eq.setDate(cursor.getLong(date));
                eq.setIdActual(cursor.getString(id_actual));
                eq.setIdOperationSupport(cursor.getString(id_operation_support));
                eq.setIdOperationLoader(cursor.getString(id_operation_loader));
                eq.setCdSafety(cursor.getString(cd_safety));
                eq.setLatitude(cursor.getDouble(latitude));
                eq.setLongitude(cursor.getDouble(longitude));
                eq.setIdLocation(cursor.getString(id_location));
                eq.setIdOperator(cursor.getString(id_operator));
                eq.setIdEquipment(cursor.getString(id_equipment));
                result.add(eq);
            }
            cursor.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Object getFlaggedDataOne() {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE sync_status=? AND " + id_equipment + "=? LIMIT 1";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, "0");
            String eqId = MainApp.equipmentLocal != null ? MainApp.equipmentLocal.getId() : "";
            preparedStatement.setString(2, eqId);
            ResultSet cursor = preparedStatement.executeQuery();

            if (cursor.next()) {
                SafetyStatusLocal eq = new SafetyStatusLocal();
                eq.setId(cursor.getString(id));
                eq.setDate(cursor.getLong(date));
                eq.setIdActual(cursor.getString(id_actual));
                eq.setIdOperationSupport(cursor.getString(id_operation_support));
                eq.setIdOperationLoader(cursor.getString(id_operation_loader));
                eq.setCdSafety(cursor.getString(cd_safety));
                eq.setLatitude(cursor.getDouble(latitude));
                eq.setLongitude(cursor.getDouble(longitude));
                eq.setIdLocation(cursor.getString(id_location));
                eq.setIdOperator(cursor.getString(id_operator));
                eq.setIdEquipment(cursor.getString(id_equipment));

                cursor.close();
                return eq;

            } else {

                cursor.close();
                return null;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    @Override
    public void postBulkData(List<Object> eq) {
        if (!NetworkConnection.getServiceStatus()) {
            return;
        }
        System.out.println("--- post synchronization ---");
        Call<ResponseModel> call = APIClient.getInstance().postOperationStatus(MainApp.requestHeader, eq);
        try {
            Response<ResponseModel> response = call.execute();
            System.out.println(response);
            System.out.println(response.body().getResponse());
            System.out.println(response.body().getMessage());
            if (response.body().getResponse().equals("success")) {
                System.out.println("post success");
                /**
                 * change flag (sync_status to 1)
                 */
                for (Object obj : eq) {
                    SafetyStatusLocal oqe = (SafetyStatusLocal) obj;
                    setSynchronized(oqe.getId());
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void postData(Object eq, Runnable cb) {
        if (!NetworkConnection.getServiceStatus()) {
            return;
        }

        Call<ResponseModel> call = APIClient.getInstance().postOfflineSafetyStatus(MainApp.requestHeader, eq);
        try {
            Response<ResponseModel> response = call.execute();
            if (response.body() != null) {
                if (response.body().getResponse() != null) {
                    if (response.body().getResponse().equals("success")) {
                        System.out.println("post success");

                        if (cb != null) {
                            cb.run();
                        }
                    }
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public Object getLocalOneById(String fieldId, String searchId) {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + fieldId + "=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, searchId);
            ResultSet cursor = preparedStatement.executeQuery();

            if (cursor.next()) {
                SafetyStatusLocal eq = new SafetyStatusLocal();
                eq.setId(cursor.getString(id));
                eq.setDate(cursor.getLong(date));
                eq.setIdActual(cursor.getString(id_actual));
                eq.setIdOperationSupport(cursor.getString(id_operation_support));
                eq.setIdOperationLoader(cursor.getString(id_operation_loader));
                eq.setCdSafety(cursor.getString(cd_safety));
                eq.setLatitude(cursor.getDouble(latitude));
                eq.setLongitude(cursor.getDouble(longitude));
                eq.setIdLocation(cursor.getString(id_location));
                eq.setIdOperator(cursor.getString(id_operator));
                eq.setIdEquipment(cursor.getString(id_equipment));
                cursor.close();

                return eq;
            }

        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }

    @Override
    public void setSynchronized(String id) {
        String sql = "update " + TABLE_NAME + " set sync_status=1 where id=?";
        // deleteData(id);
        try {
            PreparedStatement ps = conn().prepareStatement(sql);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
