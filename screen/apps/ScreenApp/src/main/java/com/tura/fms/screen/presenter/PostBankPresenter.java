package com.tura.fms.screen.presenter;

import com.tura.fms.screen.models.ResponseModel;
import com.tura.fms.screen.models.bank_model.BankDetail;
import com.tura.fms.screen.models.bank_model.BankModel;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;
import retrofit2.Call;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PostBankPresenter {

    private static List<BankDetail> newmode = null;

    public static final NetworkConnection networkConnection=new NetworkConnection();

    //untuk post bank
    public ResponseModel postDataRestApi(String token, BankDetail bankDetail){

        Map<String, String> aturheader = new HashMap<>();

        aturheader.put("Authorization", "Bearer "+token);

        String response= "";
        String message = "";


        Call<BankModel> call = APIClient.getInstance().PostBank(aturheader, bankDetail);

        try {

            if(networkConnection.checkStatus()){

                newmode = call.execute().body().getOutput();

            }else{

                //ngambil di local jika koneksi lost
                newmode = null;

            }

        }catch (IOException e) {

            e.printStackTrace();
            //ngambil di local jika request gagal
            newmode = null;

        }

        ResponseModel responseModel=new ResponseModel();
        responseModel.setMessage("ok");
        responseModel.setResponse("error");


        return responseModel;

    }

    //untuk get bank
    public BankModel getDataRestApi(String token, int page, int limit) {

        Map<String, String> aturheader = new HashMap<>();

        aturheader.put("Authorization", "Bearer "+token);

        Call<BankModel> call = APIClient.getInstance().GetBank(aturheader, page, limit);

        try {

            if(networkConnection.check_connection_bersih()){
                newmode = call.execute().body().getOutput();
            }else{

                //ngambil di local jika koneksi lost
                newmode = null;

            }



        } catch (IOException e) {

            e.printStackTrace();
            //ngambil di local jika request gagal
            newmode = null;

        }

        BankModel hasil = new BankModel();
        hasil.setOutput(newmode);

        return hasil;

    }

}
