package com.tura.fms.screen.models.maintenance_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MaintenanceUserMakanik {

    @SerializedName("id")
    @Expose
    private String id;


    public MaintenanceUserMakanik(String id, String name) {
        this.id = id;
        this.name = name;

    }
    @SerializedName("name")
    @Expose
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return "MaintenanceUserMakanik [id=" + id + ", name=" + name + "]";
    }

}
