package com.tura.fms.screen.models.location;

public class LocationLocal {
    private String id;
    private String id_self;
    private String cd_location;
    private String id_area;
    private String name;
    private Double longitude;
    private Double latitude;
    private int is_deleted;
    private Integer date_submitted;
    private Integer date_modified;
    private String json;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_self() {
        return id_self;
    }

    public void setId_self(String id_self) {
        this.id_self = id_self;
    }

    public String getCd_location() {
        return cd_location;
    }

    public void setCd_location(String cd_location) {
        this.cd_location = cd_location;
    }

    public String getId_area() {
        return id_area;
    }

    public void setId_area(String id_area) {
        this.id_area = id_area;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }

    public Integer getDate_submitted() {
        return date_submitted;
    }

    public void setDate_submitted(Integer date_submitted) {
        this.date_submitted = date_submitted;
    }

    public Integer getDate_modified() {
        return date_modified;
    }

    public void setDate_modified(Integer date_modified) {
        this.date_modified = date_modified;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }
}
