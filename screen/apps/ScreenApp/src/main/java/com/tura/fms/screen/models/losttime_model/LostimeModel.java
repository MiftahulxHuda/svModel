package com.tura.fms.screen.models.losttime_model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class LostimeModel {

    private final StringProperty id;
    private final StringProperty name;

    public LostimeModel(String id, String name) {
        this.id = new SimpleStringProperty(id);
        this.name = new SimpleStringProperty(name);
    }

    public String getId() {
        return id.get();
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public StringProperty idProperty() {
        return id;
    }

    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public StringProperty nameProperty() {
        return name;
    }

}
