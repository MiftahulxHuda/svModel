package com.tura.fms.screen.controllers.support;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConfigApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.helpers.FatigueUI;
import com.tura.fms.screen.helpers.NavigationUI;
import com.tura.fms.screen.helpers.SessionFMS;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.dashboard_model.DashboardModel;
import com.tura.fms.screen.network.NetworkConnection;
import com.tura.fms.screen.presenter.Support.DashboardSupportPresenter;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

import javafx.scene.control.Label;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.scene.control.TextField;
import static javafx.geometry.Pos.CENTER_LEFT;
import javafx.scene.control.ComboBox;
import com.jfoenix.controls.JFXButton;
import java.util.Map;
import java.util.HashMap;
import java.util.Map.Entry;
import javafx.event.EventHandler;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ScheduledExecutorService;

import static javafx.geometry.NodeOrientation.LEFT_TO_RIGHT;
import static javafx.scene.shape.StrokeType.OUTSIDE;
import static org.javalite.app_config.AppConfig.p;

public class DashboardSupport implements Initializable {

    private NavigationUI navigationUI = new NavigationUI();

    ConfigApp configApp = new ConfigApp();

    @FXML
    private Text TitleBar;

    @FXML
    private Text textActivity;

    @FXML
    private GridPane gridata;

    private ObservableList<DashboardModel> recordsData = FXCollections.observableArrayList();

    // SessionFMS sessionFMS;

    @FXML
    private VBox vboxRoot;

    @FXML
    private VBox drawer;

    DashboardSupportPresenter dashboardPresenter;

    @FXML
    private Text opname;

    @FXML
    private Label location_label;

    @FXML
    private Label destination_label;

    @FXML
    private Label exavator_label;

    @FXML
    private StackPane stackPane;

    @FXML
    private Label mode_connect;

    @FXML
    private VBox paneLog;

    @FXML
    private Label labelMaterial;

    @FXML
    private Text textHour;

    @FXML
    private Button buttonActivity;

    @FXML
    private Button buttonBreakdown;

    int no = 0;
    int no_offline = 0;

    FatigueUI fatigueUI;

    Integer fatigueTimeoutConfig;
    Integer fatigueTimeout = 0;
    Boolean showFatigue = false;

    final Map<String, String> refUnitType = new HashMap<String, String>() {
        {
            put("HAULER", "015001");
            put("LOADER", "015002");
            put("GRADDER", "015003");
            put("DOZZER", "015004");
        }
    };

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        NavigationUI.publicStackPane = stackPane;

        // TODO
        TitleBar.setText(MainApp.config("config.unit_id"));

        // sessionFMS = new SessionFMS();
        fatigueUI = new FatigueUI();
        dashboardPresenter = new DashboardSupportPresenter();
        // sessionFMS.touchFms();

        // set indikator connection
        mode_connect.setText(NetworkConnection.getStringStatus());

        opname.setText(MainApp.user.getName());

        Utilities.setupFatigueTime();

        Utilities.setSatuan();

        paneLog.getChildren().add(MainApp.textLog);

        MainApp.textLog.setPrefHeight(450.0);

        gridata.setGridLinesVisible(true);

        labelMaterial.setPrefWidth(200.0);

        if (!MainApp.isOperator) {
            drawer.getChildren().remove(buttonActivity);
            drawer.getChildren().remove(buttonBreakdown);
        }

        String fTC = MainApp.remoteConfig.get(ConstantValues.SCREEN_CONFIG_FATIGUE_TIMEOUT);
        if (fTC != null) {
            fatigueTimeoutConfig = Integer.parseInt(fTC);
        } else {
            fatigueTimeoutConfig = ConstantValues.SCREEN_CONFIG_FATIGUE_DEFAULT_TIMEOUT;
        }

        MainApp.clockDashboard = new Timeline(new KeyFrame(Duration.ZERO, e -> {
            clearModel();

            recordsData = dashboardPresenter.runConsumer();

            tampilkanData(recordsData);

            String netS = NetworkConnection.getStringStatus();
            mode_connect.setText(netS);
            mode_connect.getStyleClass().clear();
            if (netS.equals("Online")) {
                mode_connect.getStyleClass().add("font-online");
            } else {
                mode_connect.getStyleClass().add("font-offline");
            }

            if (MainApp.isOperator) {

                fatigueUI.showFatigePopup(stackPane, Utilities.fatigueData, new Runnable() {

                    @Override
                    public void run() {
                        if (!showFatigue) {
                            showFatigue = true;
                            fatigueTimeout = 0;
                        }
                    }
                }, new Runnable() {

                    @Override
                    public void run() {
                        if (showFatigue) {
                            fatigueUI.closeAlertDialog();
                        }
                        showFatigue = false;
                        fatigueTimeout = 0;
                        Utilities.postFatigueStatus();
                    }
                });

                fatigueTimeout += MainApp.dashboardInterval;
                if (fatigueTimeout > fatigueTimeoutConfig && showFatigue) {
                    MainApp.fatigueLevel++;
                    fatigueTimeout = 0;
                    fatigueUI.closeAlertDialog();
                    fatigueUI.showMessage("Fatigue test timeout, LEVEL " + MainApp.fatigueLevel.toString(), stackPane);
                }
                if (MainApp.fatigueLevel >= ConstantValues.SCREEN_CONFIG_FATIGUE_MAX_LEVEL) {
                    showFatigue = false;
                    fatigueTimeout = 0;
                    fatigueUI.closeAlertDialog();
                    Utilities.postFatigueStatus();
                }

            }

            /**
             * check activity tum
             */
            if (MainApp.latestActivity != null) {
                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_DOWN)) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            Utilities.showMessageDialog("Unit Breakdown", "",
                                    "Unit breakdown, anda akan logout dari aplikasi");
                            Utilities.doLogOut(null, (Stage) location_label.getScene().getWindow());
                        }
                    });
                }
            }

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    String tm = Utilities.getCurrentTimeString();
                    textHour.setText(tm);
                    if (MainApp.isOperator) {
                        labelMaterial.setText(
                                MainApp.actualSupport == null ? "-" : MainApp.actualSupport.getCdMaterial().getType());
                    } else {
                        labelMaterial.setText("-");
                    }
                    String date = "";
                    if (MainApp.isOperator) {
                        date = Utilities.timeStampToDate(MainApp.actualSupport == null ? Utilities.getCurrentUnix()
                                : MainApp.actualSupport.getDate().longValue(), "tanggal");
                    } else {
                        date = Utilities.getTanggal();
                    }
                    location_label.setText("DATE : " + date);
                    if (MainApp.isOperator) {
                        if (MainApp.actualSupport != null) {
                            destination_label.setText("SHIFT : " + MainApp.actualSupport.getCdShift().getName());
                        } else {
                            destination_label.setText("SHIFT : "
                                    + (Utilities.getCurrentShift().equals(ConstantValues.SHIFT_DAY) ? "Day" : "Night"));
                        }
                    } else {
                        destination_label.setText("");
                    }
                    // exavator_label.setText("DISPOSAL : " +
                    // MainApp.actualSupport.getId_destination().getName());
                }
            });

        }), new KeyFrame(Duration.seconds(MainApp.dashboardInterval)));

        MainApp.clockDashboard.setCycleCount(Animation.INDEFINITE);
        MainApp.clockDashboard.play();
    }

    public void clearModel() {
        recordsData.clear();
        gridata.getChildren().clear();
    }

    private void tampilkanData(ObservableList<DashboardModel> recordsDataParam) {

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                if (MainApp.latestActivity != null) {
                    textActivity.setText(MainApp.latestActivity.getCdTum().getName().replaceAll(" Time", "") + ": "
                            + MainApp.latestActivity.getCdActivity().getName());
                    if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_WORKING)) {
                        textActivity.setFill(Color.web(ConstantValues.COLOR_WORKING, 1));
                    } else if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_DELAY)) {
                        textActivity.setFill(Color.web(ConstantValues.COLOR_DELAY, 1));
                    } else if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_IDLE)) {
                        textActivity.setFill(Color.web(ConstantValues.COLOR_IDLE, 1));
                    } else if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_DOWN)) {
                        textActivity.setFill(Color.web(ConstantValues.COLOR_DOWN, 1));
                    } else if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_RFU)) {
                        textActivity.setFill(Color.web(ConstantValues.COLOR_RFU, 1));
                    } else if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_STANDBY)) {
                        textActivity.setFill(Color.web(ConstantValues.COLOR_STANDBY, 1));
                    } else if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_ASSIGNED)) {
                        textActivity.setFill(Color.web(ConstantValues.COLOR_ASSIGNED, 1));
                    } else if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_MAINTENANCE)) {
                        textActivity.setFill(Color.web(ConstantValues.COLOR_MAINTENANCE, 1));
                    }
                }
            }
        });

        int Col = 0;
        int Row = 0;

        for (int i = 0; i < recordsDataParam.size(); i++) {

            Text texthbox = new Text();
            VBox vboxForButtons = new VBox();

            vboxForButtons.setPrefWidth(280);
            vboxForButtons.setPrefHeight(90);

            HBox hboxForButtons = new HBox();
            // ImageView gambar = new ImageView(new Image("/images/icondash.png", 120, 100,
            // false, true));
            Text textdashboard = new Text();

            vboxForButtons.setAlignment(Pos.TOP_CENTER);
            vboxForButtons.setNodeOrientation(LEFT_TO_RIGHT);
            // vboxForButtons.setPrefHeight(99.0);
            // vboxForButtons.setPrefWidth(50.0);
            vboxForButtons.getStyleClass().add("button-dashboard");

            // set untuk button

            hboxForButtons.setPrefHeight(30.0);
            hboxForButtons.setPrefWidth(63.0);
            hboxForButtons.setPadding(new Insets(10));

            texthbox.setStrokeType(OUTSIDE);
            texthbox.setText(recordsDataParam.get(i).getName());
            texthbox.setStrokeWidth(0.0);
            texthbox.setWrappingWidth(180);
            texthbox.setFont(Font.font("Verdana", 18));
            // texthbox.getStyleClass().add("text-dashboard");
            // texthbox.getStyleClass().add("font-white");
            texthbox.setFill(Color.WHITE);

            // gambar.setFitHeight(40.0);
            // gambar.setFitWidth(33.0);
            // gambar.setPickOnBounds(true);
            // gambar.setPreserveRatio(true);
            // gambar.setImage(new Image("@../images/icondash.png",160,160,false,true));

            hboxForButtons.getChildren().add(texthbox);
            // hboxForButtons.getChildren().add(gambar);

            // set text dashboard
            textdashboard.setText(recordsDataParam.get(i).getDescription());
            textdashboard.getStyleClass().add("text-dashboard");
            textdashboard.getStyleClass().add("font-white");
            // extdashboard.setFont(Font.font("Verdana", 20));
            textdashboard.setFill(Color.GREENYELLOW);

            VBox.setMargin(textdashboard, new Insets(0, 0, 40, 0));

            vboxForButtons.getChildren().add(hboxForButtons);
            vboxForButtons.getChildren().add(textdashboard);

            gridata.add(vboxForButtons, Col, Row);

            Col++;

            if (Col > 2) {
                // Reset Column
                Col = 0;
                // Next Row
                Row++;
            }
        }
    }

    @FXML
    public void breakdownAction(ActionEvent event) throws Exception {
        MainApp.clockDashboard.stop();
        // scheduledExecutorService.shutdownNow();
        navigationUI.windowsTab(p("app.linkBreakdown"), p("app.titleBreakdown"), event);
    }

    @FXML
    public void logoutAction(ActionEvent event) throws Exception {
        Utilities.showHourMeterDialog(event);
    }

    @FXML
    public void lostimeAction(ActionEvent event) throws Exception {
        MainApp.clockDashboard.stop();
        // scheduledExecutorService.shutdownNow();
        navigationUI.windowsTab(p("app.linkLosttime"), p("app.titleLosttime"), event);

    }

    @FXML
    public void idleAction(ActionEvent event) throws Exception {
        MainApp.clockDashboard.stop();
        // scheduledExecutorService.shutdownNow();
        navigationUI.windowsTab(p("app.linkIdle"), p("app.titleIdle"), event);
    }

    @FXML
    public void safetyAction(ActionEvent event) throws Exception {
        MainApp.clockDashboard.stop();
        // scheduledExecutorService.shutdownNow();
        navigationUI.windowsTab(p("app.linkSafetyIssue"), p("app.titleSafety"), event);
    }

    @FXML
    public void activityAction(ActionEvent event) throws Exception {
        MainApp.clockDashboard.stop();
        // scheduledExecutorService.shutdownNow();
        navigationUI.windowsTab(p("app.linkActivity"), p("app.titleActivity"), event);
    }

    @FXML
    public void onDutyAction(ActionEvent event) throws Exception {
        MainApp.clockDashboard.stop();
        navigationUI.windowsTab(p("app.linkOnDuty"), p("app.titleOnDuty"), event);
    }

    @FXML
    public void onSettingsAction(ActionEvent event) throws Exception {
        // MainApp.clockDashboard.stop();
        // navigationUI.windowsTab(p("app.linkOnDuty"), p("app.titleOnDuty"), event);

        JFXDialogLayout content = new JFXDialogLayout();
        // content.setHeading(new Text("SETTINGS"));
        content.setPrefSize(700, 500);
        content.getStyleClass().add("bg-dark-light");

        VBox vbox = new VBox();
        vbox.prefWidth(700);
        vbox.prefHeight(700);

        // vbox.getStyleClass().add("bg-dark");

        Label lblunit_id = new Label("UNIT ID");
        lblunit_id.setPrefHeight(27);
        lblunit_id.setPrefWidth(250);
        lblunit_id.setStyle(" -fx-text-fill: BLACK;-fx-font-size: 20px;");
        lblunit_id.setFont(Font.font("Verdana", 12));
        lblunit_id.setTextFill(Color.BLACK);

        TextField txtunit_id = new TextField();
        txtunit_id.setText(MainApp.config("config.unit_id").trim());
        txtunit_id.setPrefHeight(27);
        txtunit_id.setPrefWidth(300);
        // txtunit_id.setPromptText("0-9");
        // txtunit_id.getProperties().put("vkType", "numeri10150121c");

        HBox hBox = new HBox();
        hBox.setAlignment(CENTER_LEFT);
        hBox.prefHeight(100);
        hBox.setPrefWidth(700);
        hBox.setSpacing(60);
        // hBox.getStyleClass().add("bg-dark");

        hBox.getChildren().add(lblunit_id);
        hBox.getChildren().add(txtunit_id);

        Label lblunitype_id = new Label("UNIT TYPE");
        lblunitype_id.setPrefHeight(27);
        lblunitype_id.setPrefWidth(250);
        lblunitype_id.setStyle(" -fx-text-fill: BLACK;-fx-font-size: 20px;");
        lblunitype_id.setFont(Font.font("Verdana", 12));
        lblunitype_id.setTextFill(Color.BLACK);

        String types[] = { "LOADER", "HAULER", "GRADDER", "DOZER" };

        final ComboBox unitType = new ComboBox(FXCollections.observableArrayList(types));

        // unitType.getItems().addAll("LOADER", "HAULER", "GRADDER", "DOZER");

        unitType.setStyle(
                "-fx-background-color: #3d453f;-fx-text-fill: #d9c750;-fx-font-size: 23px;-fx-font-weight: bold;");
        for (Entry<String, String> entry : refUnitType.entrySet()) {
            if (entry.getValue().equals(MainApp.config("config.unit_type").trim())) {
                unitType.getSelectionModel().select(entry.getKey());
            }
        }

        // TextField txtunittype_id = new TextField();
        // txtunittype_id.setText(MainApp.config("config.unit_type").trim());
        // txtunittype_id.setPrefHeight(27);
        // txtunittype_id.setPrefWidth(300);
        // txtunittype_id.setPromptText("0-9");
        // txtunittype_id.getProperties().put("vkType", "numeric");

        HBox hBox2 = new HBox();
        hBox2.setAlignment(CENTER_LEFT);
        hBox2.prefHeight(100);
        hBox2.setPrefWidth(700);
        hBox2.setSpacing(60);

        hBox2.getChildren().add(lblunitype_id);
        // hBox2.getChildren().add(txtunittype_id);
        hBox2.getChildren().add(unitType);
        // hBox2.getStyleClass().add("bg-dark");

        // Label lblcom_gps = new Label("COM GPS");
        // lblcom_gps.setPrefHeight(27);
        // lblcom_gps.setPrefWidth(250);
        // lblcom_gps.setFont(Font.font("Verdana", 12));
        // lblcom_gps.setStyle(" -fx-text-fill: BLACK;-fx-font-size: 20px;");

        // lblcom_gps.setTextFill(Color.BLACK);

        // TextField txtcom_gps = new TextField();
        // txtcom_gps.setText(MainApp.config("config.com_gps").trim());
        // txtcom_gps.setPrefHeight(27);
        // txtcom_gps.setPrefWidth(300);

        // HBox hBox3 = new HBox();
        // hBox3.setAlignment(CENTER_LEFT);
        // hBox3.prefHeight(100);
        // hBox3.setPrefWidth(700);
        // hBox3.setSpacing(60);
        // hBox3.getStyleClass().add("bg-dark");

        // hBox3.getChildren().add(lblcom_gps);
        // hBox3.getChildren().add(txtcom_gps);

        Label lblcom_sensor = new Label("COM SENSOR");
        lblcom_sensor.setPrefHeight(27);
        lblcom_sensor.setPrefWidth(250);
        lblcom_sensor.setFont(Font.font("Verdana", 12));
        lblcom_sensor.setStyle(" -fx-text-fill: BLACK;-fx-font-size: 20px;");
        lblcom_sensor.setTextFill(Color.BLACK);

        TextField txtcom_sensor = new TextField();
        txtcom_sensor.setText(MainApp.config("config.com_sensor").trim());
        txtcom_sensor.setPrefHeight(27);
        txtcom_sensor.setPrefWidth(300);

        HBox hBox4 = new HBox();
        hBox4.setAlignment(CENTER_LEFT);
        hBox4.prefHeight(100);
        hBox4.setPrefWidth(700);
        hBox4.setSpacing(60);
        // hBox4.getStyleClass().add("bg-dark");
        hBox4.getChildren().add(lblcom_sensor);
        hBox4.getChildren().add(txtcom_sensor);

        Label lblcom_fuel = new Label("PARAMETER FUEL");
        lblcom_fuel.setPrefHeight(27);
        lblcom_fuel.setPrefWidth(250);
        lblcom_fuel.setFont(Font.font("Verdana", 12));
        lblcom_fuel.setStyle(" -fx-text-fill: BLACK;-fx-font-size: 20px;");
        lblcom_fuel.setTextFill(Color.BLACK);

        String listParamFuel[] = { "komHD785", "komHD465", "cat777E", "Quester", "Scania", "pc1250", "pc2000", "pc400",
                "cat374" };
        final ComboBox paramFuel = new ComboBox(FXCollections.observableArrayList(listParamFuel));
        paramFuel.setStyle(
                "-fx-background-color: #3d453f;-fx-text-fill: #d9c750;-fx-font-size: 23px;-fx-font-weight: bold;");
        paramFuel.getSelectionModel().select(MainApp.config("config.setfuelparameter").trim());

        HBox hBox5 = new HBox();
        hBox5.setAlignment(CENTER_LEFT);
        hBox5.prefHeight(100);
        hBox5.setPrefWidth(700);
        hBox5.setSpacing(60);
        hBox5.getChildren().add(lblcom_fuel);
        hBox5.getChildren().add(paramFuel);

        Label lblEcuParamIP = new Label("ECU IP ADDRESS");
        lblEcuParamIP.setPrefHeight(27);
        lblEcuParamIP.setPrefWidth(250);
        lblEcuParamIP.setFont(Font.font("Verdana", 12));
        lblEcuParamIP.setStyle(" -fx-text-fill: BLACK;-fx-font-size: 20px;");
        lblEcuParamIP.setTextFill(Color.BLACK);
        TextField txtEcuParamIP = new TextField();
        txtEcuParamIP.setText(MainApp.config("config.ecu_param_ip_address").trim());
        txtEcuParamIP.setPrefHeight(27);
        txtEcuParamIP.setPrefWidth(300);
        HBox hBox6 = new HBox();
        hBox6.setAlignment(CENTER_LEFT);
        hBox6.prefHeight(100);
        hBox6.setPrefWidth(700);
        hBox6.setSpacing(60);
        hBox6.getChildren().add(lblEcuParamIP);
        hBox6.getChildren().add(txtEcuParamIP);

        Label lblEcuType = new Label("JENIS UNIT");
        lblEcuType.setPrefHeight(27);
        lblEcuType.setPrefWidth(250);
        lblEcuType.setFont(Font.font("Verdana", 12));
        lblEcuType.setStyle(" -fx-text-fill: BLACK;-fx-font-size: 20px;");
        lblEcuType.setTextFill(Color.BLACK);
        String listEcuType[] = { "HD785", "PC1250", "PC2000" };
        final ComboBox paramEcuType = new ComboBox(FXCollections.observableArrayList(listEcuType));
        paramEcuType.setStyle(
                "-fx-background-color: #3d453f;-fx-text-fill: #d9c750;-fx-font-size: 23px;-fx-font-weight: bold;");
        paramEcuType.getSelectionModel().select(MainApp.config("config.ecu_param_type").trim());
        HBox hBox7 = new HBox();
        hBox7.setAlignment(CENTER_LEFT);
        hBox7.prefHeight(100);
        hBox7.setPrefWidth(700);
        hBox7.setSpacing(60);
        hBox7.getChildren().add(lblEcuType);
        hBox7.getChildren().add(paramEcuType);

        vbox.getChildren().add(hBox);
        vbox.getChildren().add(hBox2);
        // vbox.getChildren().add(hBox3);
        vbox.getChildren().add(hBox4);
        vbox.getChildren().add(hBox5);
        vbox.getChildren().add(hBox6);
        vbox.getChildren().add(hBox7);

        vbox.setSpacing(30);
        vbox.getStyleClass().add("bg-dark-light");

        content.setBody(vbox);
        JFXDialog dialog = new JFXDialog(stackPane, content, JFXDialog.DialogTransition.CENTER);
        dialog.prefWidth(700);
        dialog.getStyleClass().add("bg-dark");

        JFXButton simpan = new JFXButton("      Save      ");
        simpan.getStyleClass().add("button-raised");
        simpan.prefWidth(200);

        simpan.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                String timezone = MainApp.config("config.timezone");
                String main_service_address = MainApp.config("config.main_service_address");
                String domain_name = MainApp.config("config.domain_name");
                String unit_service_address = MainApp.config("config.unit_service_address");
                String unit_service_domain = MainApp.config("config.unit_service_domain");
                String kafka_address = MainApp.config("config.kafka_address");
                String kafka_port = MainApp.config("config.kafka_port");
                String request_timeout = MainApp.config("config.request_timeout");
                String service_check_interval = MainApp.config("config.service_check_interval");
                String send_gps_ecu_interval = MainApp.config("config.send_gps_ecu_interval");
                String synchronizer_interval = MainApp.config("config.synchronizer_interval");
                String synchronizer_delay = MainApp.config("config.synchronizer_delay");
                String sqlite_uri = MainApp.config("config.sqlite_uri");
                String sqlite_auth = MainApp.config("config.sqlite_auth");
                String sqlite_clean_interval = MainApp.config("config.sqlite_clean_interval");
                String main_service_address_backup = MainApp.config("config.main_service_address_backup");
                String kafka_addresss_backup = MainApp.config("config.kafka_addresss_backup");
                String sync_page_size = MainApp.config("config.sync_page_size");
                String activate_ecu_reader = MainApp.config("config.activate_ecu_reader");
                String activate_synchronizer = MainApp.config("config.activate_synchronizer");
                String dashboard_interval = MainApp.config("config.dashboard_interval");
                String self_signed = MainApp.config("config.self_signed");
                String kafka_poll_interval = MainApp.config("config.kafka_poll_interval");
                String write_log = MainApp.config("config.write_log");
                String com_gps = MainApp.config("config.com_gps");

                String ecu_param_ip_address = txtEcuParamIP.getText().toString();
                String ecu_param_type = paramEcuType.getSelectionModel().getSelectedItem().toString();
                String setfuelparameter = paramFuel.getSelectionModel().getSelectedItem().toString();

                String unitTypeText = unitType.getSelectionModel().getSelectedItem().toString();
                String unitTypeValue = refUnitType.get(unitTypeText);

                configApp.settingConfig(txtunit_id.getText(), unitTypeValue, com_gps,
                        txtcom_sensor.getText().toString(), timezone, main_service_address, domain_name,
                        unit_service_address, unit_service_domain, kafka_address, kafka_port, request_timeout,
                        service_check_interval, send_gps_ecu_interval, synchronizer_interval, synchronizer_delay,
                        sqlite_uri, sqlite_auth, sqlite_clean_interval, main_service_address_backup,
                        kafka_addresss_backup, sync_page_size, activate_ecu_reader, activate_synchronizer,
                        dashboard_interval, setfuelparameter, self_signed, ecu_param_ip_address, ecu_param_type,
                        kafka_poll_interval, write_log);
                // Alert alert = new Alert(Alert.AlertType.INFORMATION);
                // alert.setTitle("Information");
                // alert.setHeaderText("Sucessfully");
                // alert.setContentText("DATA SAVED");
                // alert.showAndWait();

                MainApp.resetSettings();

                TitleBar.setText(MainApp.config("config.unit_id"));

                Utilities.showMessageDialog("Success", "", "Konfigurasi sudah disimpan");

                dialog.close();
            }
        });

        content.setActions(simpan);

        dialog.show();
    }

    @FXML
    private void onExitAction(ActionEvent event) throws Exception {
        Utilities.confirm(stackPane, "Yakin akan keluar dari ScreenApp ?", new Runnable() {

            @Override
            public void run() {
                ((Node) (event.getSource())).getScene().getWindow().hide();
                System.exit(0);
            }
        }, new Runnable() {

            @Override
            public void run() {
                // no action
            }
        });
    }

}
