package com.tura.fms.screen.models.quesioner_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.List;

public class QuesionerDetail {

    private final StringProperty pertanyaan;

    private final StringProperty quesioner;

    public QuesionerDetail(String pertanyaan, String quesioner) {
        this.pertanyaan = new SimpleStringProperty(pertanyaan);
        this.quesioner = new SimpleStringProperty(quesioner);
    }

    public String getPertanyaan() {
        return pertanyaan.get();
    }

    public void setPertanyaan(String pertanyaan) {
        this.pertanyaan.set(pertanyaan);
    }

    public String getQuesioner() {
        return quesioner.get();
    }

    public void setQuesioner(String quesioner) {
        this.quesioner.set(quesioner);
    }

    public StringProperty pertanyaanProperty() {
        return pertanyaan;
    }


    public StringProperty quersionerProperty() {
        return quesioner;
    }


}
