package com.tura.fms.screen.models.maintenance_model.task_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MaintenanceDetailsPost {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("maintenance_id")
    @Expose
    private String maintenance_id;

    @SerializedName("mechanic_id")
    @Expose
    private String mechanic_id;

    @SerializedName("problem_id")
    @Expose
    private String problem_id;

    @SerializedName("status_id")
    @Expose
    private String status_id;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("cd_shift")
    @Expose
    private String cd_shift;

    @SerializedName("start_time")
    @Expose
    private String start_time;

    @SerializedName("end_time")
    @Expose
    private String end_time;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String dt) {
        this.date = dt;
    }

    public String getStartTime() {
        return start_time;
    }

    public void setStartTime(String desc) {
        this.start_time = desc;
    }

    public String getEndTime() {
        return end_time;
    }

    public void setEndTime(String end_time) {
        this.end_time = end_time;
    }

    public String getMaintenanceId() {
        return maintenance_id;
    }

    public void setMaintenanceId(String mainte) {
        this.maintenance_id = mainte;
    }

    public String getCdShift() {
        return cd_shift;
    }

    public void setCdShift(String shift) {
        this.cd_shift = shift;
    }

    public String getStatusId() {
        return status_id;
    }

    public void setStatusId(String created) {
        this.status_id = created;
    }

    public String getMechanicId() {
        return mechanic_id;
    }

    public void setMechanicId(String sup) {
        this.mechanic_id = sup;
    }

    public String getProblemId() {
        return problem_id;
    }

    public void setProblemId(String fore) {
        this.problem_id = fore;
    }

}