package com.tura.fms.screen.models.maintenance_model.task_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tura.fms.screen.helpers.Utilities;

public class Taskmodel {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("created_at")
    @Expose
    private Long created_at;

    @SerializedName("scheduled_at")
    @Expose
    private Long scheduled_at;

    @SerializedName("hm")
    @Expose
    private int hm;

    @SerializedName("model_unit")
    @Expose
    private String model_unit;

    @SerializedName("wo_number")
    @Expose
    private String wo_number;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("completed_at")
    @Expose
    private Long completed_at;

    @SerializedName("maintenance_group_id")
    @Expose
    private Object maintenance_group_id;

    @SerializedName("maintenance_request_id")
    @Expose
    private Object maintenance_request_id;

    @SerializedName("location_id")
    @Expose
    private Object location_id;

    @SerializedName("foreman_id")
    @Expose
    private Object foreman_id;

    @SerializedName("equipment_id")
    @Expose
    private Equipment equipment;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("problem_id")
    @Expose
    private ProblemItem problem;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Long created_at) {
        this.created_at = created_at;
    }

    public Long getScheduled_at() {
        return scheduled_at;
    }

    public void setScheduled_at(Long scheduled_at) {
        this.scheduled_at = scheduled_at;
    }

    public int getHm() {
        return hm;
    }

    public void setHm(int hm) {
        this.hm = hm;
    }

    public String getModel_unit() {
        return model_unit;
    }

    public void setModel_unit(String model_unit) {
        this.model_unit = model_unit;
    }

    public String getWo_number() {
        return wo_number;
    }

    public void setWo_number(String wo_number) {
        this.wo_number = wo_number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCompleted_at() {
        return completed_at;
    }

    public void setCompleted_at(Long completed_at) {
        this.completed_at = completed_at;
    }

    public Object getMaintenance_group_id() {
        return maintenance_group_id;
    }

    public void setMaintenance_group_id(Object maintenance_group_id) {
        this.maintenance_group_id = maintenance_group_id;
    }

    public Object getMaintenance_request_id() {
        return maintenance_request_id;
    }

    public void setMaintenance_request_id(Object maintenance_request_id) {
        this.maintenance_request_id = maintenance_request_id;
    }

    public Object getLocation_id() {
        return location_id;
    }

    public void setLocation_id(Object location_id) {
        this.location_id = location_id;
    }

    public Object getForeman_id() {
        return foreman_id;
    }

    public void setForeman_id(Object foreman_id) {
        this.foreman_id = foreman_id;
    }

    public Equipment getEquipment() {
        return equipment;
    }

    public void setEquipment(Equipment equipment) {
        this.equipment = equipment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ProblemItem getProblem() {
        return problem;
    }

    public void setProblem(ProblemItem problem) {
        this.problem = problem;
    }

}