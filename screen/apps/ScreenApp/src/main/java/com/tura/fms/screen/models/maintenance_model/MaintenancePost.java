package com.tura.fms.screen.models.maintenance_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MaintenancePost {

    @SerializedName("operation_actual_id")
    @Expose
    private String operation_actual_id;

    @SerializedName("actual_support_id")
    @Expose
    private String actual_support_id;

    @SerializedName("problem_id")
    @Expose
    private String problem_id;

    @SerializedName("equipment_id")
    @Expose
    private String equipment_id;

    @SerializedName("mechanic_id")
    @Expose
    private String mechanic_id;

    @SerializedName("location_id")
    @Expose
    private String location_id;

    @SerializedName("operator_id")
    @Expose
    private String operator_id;

    @SerializedName("latitude")
    @Expose
    private Double latitude;

    @SerializedName("longitude")
    @Expose
    private Double longitude;

    @SerializedName("is_deleted")
    @Expose
    private int is_deleted;

    @SerializedName("response")
    @Expose
    private String response;

    @SerializedName("info")
    @Expose
    private String info;

    @SerializedName("response_date")
    @Expose
    private Long response_date;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Long getResponse_date() {
        return response_date;
    }

    public void setResponse_date(Long response_date) {
        this.response_date = response_date;
    }

    public String getOperation_actual_id() {
        return operation_actual_id;
    }

    public void setOperation_actual_id(String operation_actual_id) {
        this.operation_actual_id = operation_actual_id;
    }

    public String getActual_support_id() {
        return actual_support_id;
    }

    public void setActual_support_id(String actual_support_id) {
        this.actual_support_id = actual_support_id;
    }

    public String getProblem_id() {
        return problem_id;
    }

    public void setProblem_id(String problem_id) {
        this.problem_id = problem_id;
    }

    public String getEquipment_id() {
        return equipment_id;
    }

    public void setEquipment_id(String equipment_id) {
        this.equipment_id = equipment_id;
    }

    public String getMechanic_id() {
        return mechanic_id;
    }

    public void setMechanic_id(String mechanic_id) {
        this.mechanic_id = mechanic_id;
    }

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public String getOperator_id() {
        return operator_id;
    }

    public void setOperator_id(String operator_id) {
        this.operator_id = operator_id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }

}
