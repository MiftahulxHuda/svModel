package com.tura.fms.screen.helpers;

import static com.tura.fms.screen.constanst.ConstantValues.EQUIPMENT_HAULER;
import static com.tura.fms.screen.constanst.ConstantValues.EQUIPMENT_LOADER;
import static org.javalite.app_config.AppConfig.p;

import java.net.ConnectException;
import java.text.ParseException;
import java.util.Locale;
import java.util.concurrent.TimeoutException;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.models.ResponseModel;
import com.tura.fms.screen.models.log_history.LogHistory;
import com.tura.fms.screen.models.log_history.LogHistoryLocal;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;

import org.comtel2000.keyboard.control.DefaultLayer;
import org.comtel2000.keyboard.control.KeyBoardPopup;
import org.comtel2000.keyboard.control.KeyboardPane;
import org.comtel2000.swing.robot.NativeAsciiRobotHandler;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NavigationUI {

    public static Parent root;

    public static Stage stage = null;

    public static Stage dashboardStage = null;

    public static StackPane publicStackPane;

    private double xOffset = 0;
    private double yOffset = 0;
    private int posX, posY;
    // SessionFMS sessionFMS;

    public static void windowsRefreshDashboard(String path, String title, Runnable callback) {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                try {
                    NavigationUI.root = FXMLLoader.load(NavigationUI.class.getResource(path));

                    Scene scene = new Scene(root, MainApp.screenBounds.getWidth(), MainApp.screenBounds.getHeight());
                    NavigationUI.dashboardStage.setScene(scene);
                    NavigationUI.dashboardStage.setTitle(title);
                    scene.setFill(null);
                    NavigationUI.dashboardStage.setResizable(false);
                    NavigationUI.dashboardStage.setX(0.0);
                    NavigationUI.dashboardStage.setY(0.0);
                    if (callback != null) {
                        callback.run();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    public void windowsTab(String path, String title, ActionEvent event, Runnable callback) throws Exception {
        // KeyBoardPopup popup =
        // KeyBoardPopupBuilder.create().initLocale(Locale.ENGLISH).build();
        MainApp.latestRefEvent = event;

        // sessionFMS = new SessionFMS();
        // double width = ((Node) event.getSource()).getScene().getWidth();
        // double height = ((Node) event.getSource()).getScene().getHeight();
        // !sessionFMS.cekSession() &&
        // if (!sessionFMS.cekSession() && sessionFMS.getSessionFms("token").isEmpty())
        // {

        // sessionFMS.logoutFms();
        // path = p("app.linkLogin");
        // title = p("app.titleLogin");

        // width = 600.0;
        // height = 400.0;

        // } else {
        // width = ((Node) event.getSource()).getScene().getWidth();
        // height = ((Node) event.getSource()).getScene().getHeight();
        // }
        // path = p("app.linkLogin");
        // title = p("app.titleLogin");

        Parent root = FXMLLoader.load(getClass().getResource(path));

        // Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();

        Scene scene = new Scene(root);// , screenBounds.getWidth(), screenBounds.getHeight());
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        // stage.getIcons().add(new Image("file:resources/images/logo.png"));
        stage.setTitle(title);
        stage.setScene(scene);
        scene.setFill(null);
        stage.setMaximized(true);
        stage.setFullScreen(true);
        stage.setFullScreenExitHint("");
        stage.setOnShown((WindowEvent ev) -> {
            if (callback != null) {
                callback.run();
            }
        });
        // popup.registerScene(scene);
        // popup.addGlobalFocusListener();

        stage.show();

        if (callback != null) {
            callback.run();
        }
    }

    public void windowsTab(String path, String title, ActionEvent event) throws Exception {
        windowsTab(path, title, event, null);
    }

    public void back_dashboard(ActionEvent evt, Runnable callback) {
        try {
            if (MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_HAULER) && MainApp.isOperator) {
                windowsTab(p("app.linkDashboard"), p("app.titleDashboard"), evt, callback);
            } else if (MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_LOADER)
                    && MainApp.isOperator) {
                windowsTab(p("app.linkDashboardLoader"), p("app.titleDashboardLoader"), evt, callback);
            } else {
                windowsTab(p("app.linkDashboardSupport"), p("app.titleDashboardSupport"), evt, callback);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void back_dashboard(ActionEvent evt) {
        back_dashboard(evt, null);
    }

    public static void to_dashboard(String level, Event event) {
        try {
            // System.out.println(">>>>>>>>>>>>>>>>>> to dashboard " + level);

            if (level.equals(EQUIPMENT_HAULER) && MainApp.isOperator) {

                NavigationUI.windowsNewScene(p("app.linkDashboard"), p("app.titleDashboard"), event);

            } else if (level.equals(EQUIPMENT_LOADER) && MainApp.isOperator) {

                NavigationUI.windowsNewScene(p("app.linkDashboardLoader"), p("app.titleDashboardLoader"), event);

            } else {

                NavigationUI.windowsNewScene(p("app.linkDashboardSupport"), p("app.titleDashboardSupport"), event);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void windowsMap(String path, String title, ActionEvent event) throws Exception {
        double width = ((Node) event.getSource()).getScene().getWidth();
        double height = ((Node) event.getSource()).getScene().getHeight();
        NavigationUI.root = FXMLLoader.load(getClass().getResource(path));
        Scene scene = new Scene(root, width, height);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setTitle(title);
        stage.setScene(scene);
        stage.show();
    }

    public void windowsNew(String path, String title, Runnable callback) throws Exception {
        // KeyBoardPopup popup =
        // KeyBoardPopupBuilder.create().initLocale(Locale.ENGLISH).build();

        NavigationUI.root = FXMLLoader.load(getClass().getResource(path));
        /*
         * root.setOnMousePressed((MouseEvent event) -> { xOffset = event.getSceneX();
         * yOffset = event.getSceneY(); });
         *
         *
         * root.setOnMouseDragged((MouseEvent event) -> { stage.setX(event.getScreenX()
         * - xOffset); stage.setY(event.getScreenY() - yOffset); });
         */
        Stage stage = new Stage();
        Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();

        Scene scene = new Scene(root, screenBounds.getWidth(), screenBounds.getHeight());
        stage.setTitle(title);
        stage.initStyle(StageStyle.UNDECORATED);
        scene.setFill(null);
        // stage.getIcons().add(new Image("/images/logo.png"));
        stage.setMaximized(true);
        // stage.setResizable(false);
        stage.setFullScreen(true);
        stage.setFullScreenExitHint("");

        // popup.registerScene(scene);
        // popup.addGlobalFocusListener();

        stage.setScene(scene);
        stage.show();

        if (callback != null) {
            callback.run();
        }
    }

    public void windowsNew(String path, String title) throws Exception {
        windowsNew(path, title, null);
    }

    public static void windowsNewScene(String path, String title, Event event, Boolean setStyle) throws Exception {
        // KeyBoardPopup popup =
        // KeyBoardPopupBuilder.create().initLocale(Locale.ENGLISH).build();

        NavigationUI.root = FXMLLoader.load(NavigationUI.class.getResource(path));

        /*
         * root.setOnMousePressed((MouseEvent event) -> { xOffset = event.getSceneX();
         * yOffset = event.getSceneY(); });
         *
         *
         * root.setOnMouseDragged((MouseEvent event) -> { stage.setX(event.getScreenX()
         * - xOffset); stage.setY(event.getScreenY() - yOffset); });
         */
        // Stage stage = new Stage();
        // Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
        MainApp.latestRefEvent = event;

        Scene scene = new Scene(root);// , MainApp.screenBounds.getWidth(), MainApp.screenBounds.getHeight());
        NavigationUI.stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        // NavigationUI.stage.getIcons().add(new
        // Image("file:resources/images/logo.png"));
        if (setStyle) {
            try {
                NavigationUI.stage.initStyle(StageStyle.UNDECORATED);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        // NavigationUI.stage = new Stage();
        NavigationUI.stage.setScene(scene);
        NavigationUI.stage.setTitle(title);
        scene.setFill(null);
        // NavigationUI.stage.getIcons().add(new Image("/images/logo.png"));
        NavigationUI.stage.setResizable(false);
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                NavigationUI.stage.setMaximized(true);
                NavigationUI.stage.setFullScreen(true);
            }
        });

        // NavigationUI.stage.setFullScreenExitHint("");
        // NavigationUI.stage.setX(0.0);
        // NavigationUI.stage.setY(0.0);
        // popup.registerScene(scene);
        // popup.addGlobalFocusListener();
        NavigationUI.dashboardStage = NavigationUI.stage;
    }

    public static void windowsNewScene(String path, String title, Event event) throws Exception {
        windowsNewScene(path, title, event, false);
    }

    public static void windowsNewScene(String path, String title, Stage stage) throws Exception {
        NavigationUI.root = FXMLLoader.load(NavigationUI.class.getResource(path));

        Scene scene = new Scene(root, MainApp.screenBounds.getWidth(), MainApp.screenBounds.getHeight());
        NavigationUI.stage = stage;
        NavigationUI.stage.setScene(scene);
        NavigationUI.stage.setTitle(title);
        scene.setFill(null);
        NavigationUI.stage.setResizable(false);
        NavigationUI.stage.setX(0.0);
        NavigationUI.stage.setY(0.0);
        NavigationUI.dashboardStage = NavigationUI.stage;
    }

    public void windowsNewKeyboard() throws Exception {

        Stage stage = new Stage();

        stage.setResizable(false);
        stage.initStyle(StageStyle.UNDECORATED);

        KeyboardPane kb = new KeyboardPane();
        kb.setMinSize(200.0, 200.0);
        kb.setMaxSize(200.0, 200.0);
        kb.setLayer(DefaultLayer.NUMBLOCK);
        kb.addRobotHandler(new NativeAsciiRobotHandler());
        kb.setOnKeyboardCloseButton(e -> stage.hide());
        try {
            /*
             * Map<String, String> params = getParameters().getNamed(); if (params.isEmpty()
             * && !getParameters().getRaw().isEmpty()) { showHelp(); }
             *
             * if (params.containsKey("help")) { showHelp(); } if
             * (params.containsKey("scale")) {
             * kb.setScale(Double.valueOf(params.get("scale"))); } if
             * (params.containsKey("locale")) {
             * kb.setLocale(parseLocale(params.get("locale"))); } if
             * (params.containsKey("pos")) { parsePosition(params.get("pos")); } if
             * (params.containsKey("layout")) {
             * kb.setLayerPath((Paths.get(this.getClass().getResource(params.get("layout")).
             * toURI()))); } if (params.containsKey("type")) {
             * kb.setKeyboardType(KeyboardType.valueOf(params.get("type").toUpperCase())); }
             */
            // kb.setKeyboardType(KeyboardType.valueOf(params.get("type").toUpperCase()));

            kb.load();

        } catch (Exception e) {
            System.out.println(e.getMessage());
            showHelp();
        }

        KeyBoardPopup popup = new KeyBoardPopup(kb);
        popup.setWidth(200.0);
        popup.setHeight(200.0);
        popup.setX(posX);
        popup.setY(posY);

        Scene scene = new Scene(new Group(), 0.1, 0.1);
        stage.setScene(scene);
        stage.show();

        popup.registerScene(scene);
        popup.setVisible(true);
    }

    private void showHelp() {
        System.out.println();
        System.out.println("\t--scale=<double>\tset the intial scale");
        System.out.println("\t--lang=<locale>\t\tsetting keyboard language (en,de,ru,..)");
        System.out.println("\t--layout=<path>\t\tpath to custom layout xml");
        System.out.println("\t--pos=<x,y>\t\tinitial keyboard position");
        System.out.println("\t--type=<type>\t\tvkType like numeric, email, url, text(default)");
        System.out.println("\t--help\t\t\tthis help screen");
        System.exit(0);
    }

    private Locale parseLocale(String l) throws Exception {
        if (l == null || l.isEmpty()) {
            throw new ParseException("invalid locale", 0);
        }
        String[] lang = l.split("_");
        if (lang.length == 2) {
            return new Locale(lang[0], lang[1]);
        }
        return Locale.forLanguageTag(l);
    }

    private void parsePosition(String p) throws Exception {
        if (p == null || p.isEmpty()) {
            throw new Exception("invalid position: " + String.valueOf(p));
        }

        String[] pos = p.split(",");
        if (pos.length == 2) {
            posX = Integer.valueOf(pos[0]);
            posY = Integer.valueOf(pos[1]);
        }
    }

    public static void setLabelText(String text) {
        // if (NavigationUI.root != null) {
        // Text lblData = (Text) NavigationUI.root.lookup("#lblinfo");
        // if (lblData != null) {
        // lblData.setText(text);
        // System.out.println("----- set label text");
        // } else {
        // System.out.println("----- can't set label text");
        // }
        // }
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                NavigationUI.addLogActivity("", text);
            }
        });
    }

    public static void setExcaLabel(String text) {
        if (NavigationUI.root != null) {
            Label lblData = (Label) NavigationUI.root.lookup("#exavator_label");
            if (lblData != null) {
                lblData.setText(text);
                System.out.println("----- set exca text");
            } else {
                System.out.println("----- can't set exca text");
            }
        }
    }

    public static void addLogActivity(String tum, String message) {
        // if (NavigationUI.root != null) {
        // InlineCssTextArea textLog = (InlineCssTextArea)
        // NavigationUI.root.lookup("#textLog");
        if (MainApp.textLog != null) {

            String color = "WHITE";
            if (tum.equals(ConstantValues.TUM_ASSIGNED)) {
                color = ConstantValues.COLOR_ASSIGNED;
            }
            if (tum.equals(ConstantValues.TUM_DELAY)) {
                color = ConstantValues.COLOR_DELAY;
            }
            if (tum.equals(ConstantValues.TUM_DOWN)) {
                color = ConstantValues.COLOR_DOWN;
            }
            if (tum.equals(ConstantValues.TUM_IDLE)) {
                color = ConstantValues.COLOR_IDLE;
            }
            if (tum.equals(ConstantValues.TUM_MAINTENANCE)) {
                color = ConstantValues.COLOR_MAINTENANCE;
            }
            if (tum.equals(ConstantValues.TUM_RFU)) {
                color = ConstantValues.COLOR_RFU;
            }
            if (tum.equals(ConstantValues.TUM_STANDBY)) {
                color = ConstantValues.COLOR_STANDBY;
            }
            if (tum.equals(ConstantValues.TUM_WORKING)) {
                color = ConstantValues.COLOR_WORKING;
            }
            String naw = Utilities.getDateTime();
            MainApp.textLog.append(naw + ": " + message + "\n", "-fx-fill:" + color + "; -fx-font-size:18px;");
            MainApp.textLog.moveTo(MainApp.textLog.getLength());
            MainApp.textLog.requestFollowCaret();
            // System.out.println("----- append log");
            // } else {
            // System.out.println("----- can't append log");
            // }

            LogHistory log = new LogHistory();
            if (Utilities.isLoaderHauler()) {
                if (MainApp.actualInfo != null) {
                    log.setActualId(MainApp.actualInfo.getId());
                }
            } else {
                if (MainApp.actualSupport != null) {
                    log.setActualSupportId(MainApp.actualSupport.getId());
                }
            }
            log.setLocalDate(Utilities.getCurrentUnix());
            log.setEquipmentId(MainApp.equipmentLocal.getId());
            if (MainApp.user != null) {
                log.setUserId(MainApp.user.getId());
            }
            log.setMessage(message);
            log.setIsDeleted(0);

            if (MainApp.isCanPost()) {

                Call<ResponseModel> call = APIClient.getInstance().postLogHistory(MainApp.requestHeader, log);
                call.enqueue(new Callback<ResponseModel>() {

                    @Override
                    public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                    }

                    @Override
                    public void onFailure(Call<ResponseModel> call, Throwable t) {
                        // t.printStackTrace();

                        if (t instanceof ConnectException) {
                            Utilities.messageSocketException(t, "Add LogActivity");
                        } else if (t instanceof TimeoutException) {
                            Utilities.messageTimeoutException(t, "Add LogActivity");
                        } else {
                            Utilities.messageOtherException(t, "Add LogActivity");
                        }

                        /**
                         * offline data
                         */
                        LogHistoryLocal lh = new LogHistoryLocal();
                        lh.setIdActual(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId());
                        lh.setIdOperationSupport(MainApp.actualSupport == null ? null : MainApp.actualSupport.getId());
                        lh.setIdOperationLoader(MainApp.actualLoader == null ? null : MainApp.actualLoader.getId());
                        lh.setTime(Utilities.getCurrentUnix());
                        lh.setIdEquipment(MainApp.equipmentLocal == null ? null : MainApp.equipmentLocal.getId());
                        lh.setIdOperator(MainApp.user == null ? null : MainApp.user.getId());
                        lh.setMessage(message);
                        MainApp.logHistoryDB.insertIgnore(lh);
                    }
                });

            } else {

                System.out.println("-------- offline NavigationUI.java");

                LogHistoryLocal lh = new LogHistoryLocal();
                lh.setIdActual(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId());
                lh.setIdOperationSupport(MainApp.actualSupport == null ? null : MainApp.actualSupport.getId());
                lh.setIdOperationLoader(MainApp.actualLoader == null ? null : MainApp.actualLoader.getId());
                lh.setTime(Utilities.getCurrentUnix());
                lh.setIdEquipment(MainApp.equipmentLocal == null ? null : MainApp.equipmentLocal.getId());
                lh.setIdOperator(MainApp.user == null ? null : MainApp.user.getId());
                lh.setMessage(message);
                MainApp.logHistoryDB.insertIgnore(lh);

            }
        }
    }

    public static void setOperatorName(String text) {
        if (NavigationUI.root != null) {
            Text opname = (Text) NavigationUI.root.lookup("#opname");
            if (opname != null) {
                opname.setText(text);
            } else {
                System.out.println("----- can't set operator text");
            }
        }
    }

    public static void setLocationName(String text) {
        if (NavigationUI.root != null) {
            Label location = (Label) NavigationUI.root.lookup("#location_label");
            if (location != null) {
                location.setText(text);
            } else {
                System.out.println("----- can't set location");
            }
        }
    }

    public static void setDestinationName(String text) {
        if (NavigationUI.root != null) {
            Label dest = (Label) NavigationUI.root.lookup("#destination_label");
            if (dest != null) {
                dest.setText(text);
            } else {
                System.out.println("----- can't set destination");
            }
        }
    }

}
