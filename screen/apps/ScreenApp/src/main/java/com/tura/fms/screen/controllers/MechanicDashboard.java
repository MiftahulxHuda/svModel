package com.tura.fms.screen.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXSpinner;
import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.ReloadApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.helpers.NavigationUI;
import com.tura.fms.screen.helpers.SessionFMS;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.equipment.EquipmentLogStartRequest;
import com.tura.fms.screen.models.maintenance_model.MainConstModel;
import com.tura.fms.screen.presenter.MaintenancePresenter;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.scene.control.ScrollPane;
import com.tura.fms.screen.network.NetworkConnection;

import java.net.URL;
import java.util.ResourceBundle;

import static org.javalite.app_config.AppConfig.p;

public class MechanicDashboard implements Initializable {

    private NavigationUI navigationUI = new NavigationUI();

    @FXML
    private Button TitleBar;

    @FXML
    private Text textHour;

    @FXML
    private Text unitName;

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXButton task;

    @FXML
    private JFXButton schedule;

    @FXML
    private JFXButton backlog;

    @FXML
    private JFXButton rfu;

    @FXML
    private GridPane gridata;

    @FXML
    private ScrollPane scrollPane;

    @FXML
    private Text usermekanik;

    @FXML
    private Label statusText;

    @FXML
    JFXSpinner loading;

    private ObservableList<MainConstModel> recordsData = FXCollections.observableArrayList();

    Timeline clock;

    MaintenancePresenter maintenancePresenter;

    public static String activeButton = ConstantValues.MECHANIC_MENU_TASK;

    public static Integer page = 1;

    public static Integer acceptedData = 0;

    public static final Integer limit = 5;

    @FXML
    private Label mode_connect;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        NavigationUI.publicStackPane = stackPane;

        // TODO
        TitleBar.setText(p("app.titleMechanicTash"));

        gridata.setVgap(10);

        maintenancePresenter = new MaintenancePresenter();

        usermekanik.setText(MainApp.user.getName());

        textHour.setWrappingWidth(150);

        unitName.setText(MainApp.config("config.unit_id"));

        clock = new Timeline(new KeyFrame(Duration.ZERO, e -> {
            mode_connect.setText(NetworkConnection.getStringStatus());
            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    if (MainApp.latestActivity != null) {
                        if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_DOWN)) {
                            statusText.setStyle("-fx-text-fill: #d32f2f;-fx-font-size: 20px;");
                        }
                        if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_MAINTENANCE)) {
                            statusText.setStyle("-fx-text-fill: #fbc02d;-fx-font-size: 20px;");
                        }
                        if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_RFU)) {
                            statusText.setStyle("-fx-text-fill: #a5d6a7;-fx-font-size: 20px;");
                        }
                        if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_STANDBY)) {
                            statusText.setStyle("-fx-text-fill: #388e3c;-fx-font-size: 20px;");
                        }
                        if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_ASSIGNED)) {
                            statusText.setStyle("-fx-text-fill: #90caf9;-fx-font-size: 20px;");
                        }
                        if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_WORKING)) {
                            statusText.setStyle("-fx-text-fill: #1976d2;-fx-font-size: 20px;");
                        }
                        statusText.setText(MainApp.latestActivity.getCdTum().getName());
                    }
                }
            });

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    String tm = Utilities.getCurrentTimeString();
                    textHour.setText(tm);
                }
            });

        }), new KeyFrame(Duration.seconds(MainApp.dashboardInterval)));

        clock.setCycleCount(Animation.INDEFINITE);
        clock.play();

        scrollPane.vvalueProperty()
                .addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
                    if (newValue.doubleValue() == scrollPane.getVmax()) {
                        if (MechanicDashboard.acceptedData >= MechanicDashboard.limit) {
                            MechanicDashboard.acceptedData = 0;
                            MechanicDashboard.page++;

                            if (MechanicDashboard.activeButton.equals(ConstantValues.MECHANIC_MENU_TASK)) {
                                task.fire();
                            }
                            if (MechanicDashboard.activeButton.equals(ConstantValues.MECHANIC_MENU_SCHEDULE)) {
                                schedule.fire();
                            }
                            if (MechanicDashboard.activeButton.equals(ConstantValues.MECHANIC_MENU_BACKLOG)) {
                                backlog.fire();
                            }
                        }
                    }
                });

        if (MechanicDashboard.activeButton.equals(ConstantValues.MECHANIC_MENU_TASK)) {
            task.fire();
        } else if (MechanicDashboard.activeButton.equals(ConstantValues.MECHANIC_MENU_SCHEDULE)) {
            schedule.fire();
        } else if (MechanicDashboard.activeButton.equals(ConstantValues.MECHANIC_MENU_BACKLOG)) {
            backlog.fire();
        } else {
            task.fire();
        }

        MechanicMaintenance.getReferenceData(new Runnable() {

            @Override
            public void run() {
                // do nothing
            }
        });
    }

    public void tampilanDataAsync(Runnable cbReload) {

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                loading.setVisible(true);
                task.getStyleClass().clear();
                task.getStyleClass().add("button-primary-disable");
                schedule.getStyleClass().clear();
                schedule.getStyleClass().add("button-primary-disable");
                backlog.getStyleClass().clear();
                backlog.getStyleClass().add("button-primary-disable");
                if (MechanicDashboard.activeButton == ConstantValues.MECHANIC_MENU_TASK) {
                    task.getStyleClass().clear();
                    task.getStyleClass().add("button-primary");
                }
                if (MechanicDashboard.activeButton == ConstantValues.MECHANIC_MENU_SCHEDULE) {
                    schedule.getStyleClass().clear();
                    schedule.getStyleClass().add("button-primary");
                }
                if (MechanicDashboard.activeButton == ConstantValues.MECHANIC_MENU_BACKLOG) {
                    backlog.getStyleClass().clear();
                    backlog.getStyleClass().add("button-primary");
                }
            }
        });

        maintenancePresenter.getDataRestApiAsync(MainApp.requestHeader, MechanicDashboard.page, MechanicDashboard.limit,
                gridata, loading, stackPane, cbReload);
    }

    public void clearGrid() {
        gridata.getChildren().clear();
    }

    @FXML
    public void listRequest(ActionEvent event) throws Exception {
        if (!MechanicDashboard.activeButton.equals(ConstantValues.MECHANIC_MENU_TASK)) {
            MechanicDashboard.acceptedData = 0;
            MechanicDashboard.page = 1;
        }
        MechanicDashboard.activeButton = ConstantValues.MECHANIC_MENU_TASK;
        if (MechanicDashboard.page == 1) {
            clearGrid();
        }
        tampilanDataAsync(new Runnable() {

            @Override
            public void run() {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        task.fire();
                    }
                });
            }
        });
    }

    @FXML
    public void listSchedule(ActionEvent event) throws Exception {
        if (!MechanicDashboard.activeButton.equals(ConstantValues.MECHANIC_MENU_SCHEDULE)) {
            MechanicDashboard.acceptedData = 0;
            MechanicDashboard.page = 1;
        }
        MechanicDashboard.activeButton = ConstantValues.MECHANIC_MENU_SCHEDULE;
        if (MechanicDashboard.page == 1) {
            clearGrid();
        }
        tampilanDataAsync(new Runnable() {

            @Override
            public void run() {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        schedule.fire();
                    }
                });
            }
        });
    }

    @FXML
    public void listBacklog(ActionEvent event) throws Exception {
        if (!MechanicDashboard.activeButton.equals(ConstantValues.MECHANIC_MENU_BACKLOG)) {
            MechanicDashboard.acceptedData = 0;
            MechanicDashboard.page = 1;
        }
        MechanicDashboard.activeButton = ConstantValues.MECHANIC_MENU_BACKLOG;
        if (MechanicDashboard.page == 1) {
            clearGrid();
        }
        tampilanDataAsync(new Runnable() {

            @Override
            public void run() {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        backlog.fire();
                    }
                });
            }
        });
    }

    @FXML
    public void doRFU(ActionEvent event) throws Exception {
        if (MainApp.latestActivity != null) {
            if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_RFU)) {
                Utilities.showMessageDialog("Information", "", "Unit sudah RFU");
                return;
            }
            if (!MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_DOWN)
                    && !MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_MAINTENANCE)) {
                Utilities.showMessageDialog("Information", "", "Unit tidak breakdown / maintenance");
                return;
            }
        }

        Utilities.confirm(stackPane, "Yakin akan RFU untuk unit ini ?", new Runnable() {

            @Override
            public void run() {
                loading.setVisible(true);
                rfu.setDisable(true);

                EquipmentLogStartRequest activity = new EquipmentLogStartRequest();
                // if (Utilities.isLoaderHauler()) {
                // activity.setIdActual(MainApp.actualInfo.getId());
                // } else {
                // activity.setIdOperationSupport(MainApp.actualSupport.getId());
                // }
                activity.setIdEquipment(MainApp.equipmentLocal.getId());
                // activity.setIdOperator(MainApp.user.getId());
                activity.setTime("current");
                // activity.setCdActivity(ConstantValues.ACTIVITY_CHANGE_SHIFT);
                activity.setCdTum(ConstantValues.TUM_RFU);
                Utilities.setActivity(activity, new Runnable() {

                    @Override
                    public void run() {
                        rfu.setDisable(false);
                        loading.setVisible(false);
                    }
                });
            }
        }, new Runnable() {

            @Override
            public void run() {
                // no action
            }
        });
    }

    @FXML
    public void logoutAction(ActionEvent event) throws Exception {
        // clock.stop();
        // scheduledExecutorService.shutdownNow();
        // sessionFMS.logoutFms();
        // ((Node) (event.getSource())).getScene().getWindow().hide();
        // navigationUI.windowsNew(p("app.linkLogin"), p("app.titleLogin"));

        // Platform.exit();
        // System.exit(0);
        // Process p = Runtime.getRuntime().exec("./run.sh");
        // p.waitFor();

        Utilities.showHourMeterDialog(event);

        // Utilities.confirm(stackPane, "Yakin akan keluar ?", new Runnable() {

        // @Override
        // public void run() {
        // Utilities.doLogOut(event, null);
        // }
        // }, new Runnable() {

        // @Override
        // public void run() {

        // }
        // });
    }

}
