package com.tura.fms.screen.network;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.helpers.Utilities;

import org.apache.kafka.common.errors.TimeoutException;

import javafx.application.Platform;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.Interceptor.Chain;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.net.ssl.HostnameVerifier;

public class APIClient {

    // Base URL for API Requests
    private static final String BASE_URL = MainApp.config("config.main_service_address");

    public static Boolean exceptionHappened = false;

    private static APIRequests apiRequests = null;

    private static APIRequests apiRequestsHandle = null;

    private static APIRequests unitServiceRequests = null;

    public static ObjectMapper mapper = new ObjectMapper();

    private static final Integer SELF_SIGNED = Integer.parseInt(MainApp.config("config.self_signed"));

    public static Response onOnIntercept(Chain chain) {
        try {
            Response response = chain.proceed(chain.request());
            APIClient.exceptionHappened = false;
            MainApp.apiStatus = ConstantValues.DONE;
            return response;
        } catch (TimeoutException exception) {
            exception.printStackTrace();
            if (!APIClient.exceptionHappened) {
                Platform.runLater(() -> {
                    Utilities.showMessageDialog("Exception", "Socket timeout ", exception.getMessage());
                });
            }
            APIClient.exceptionHappened = true;
            MainApp.apiStatus = ConstantValues.DONE;
            return null;
        } catch (IOException exception) {
            exception.printStackTrace();
            if (!APIClient.exceptionHappened) {
                Platform.runLater(() -> {
                    Utilities.showMessageDialog("Exception", "Terjadi kesalahan", exception.getMessage());
                });
            }
            APIClient.exceptionHappened = true;
            MainApp.apiStatus = ConstantValues.DONE;
            return null;
        } catch (Exception exception) {
            exception.printStackTrace();
            if (!APIClient.exceptionHappened) {
                Platform.runLater(() -> {
                    Utilities.showMessageDialog("Exception", "Terjadi kesalahan", exception.getMessage());
                });
            }
            APIClient.exceptionHappened = true;
            MainApp.apiStatus = ConstantValues.DONE;
            return null;
        }
    }

    public static APIRequests getInstance(Boolean handleException) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        Integer timeout = Integer.parseInt(MainApp.config("config.request_timeout"));

        MainApp.apiStatus = ConstantValues.PROCESSING;

        OkHttpClient okHttpClient;

        if (handleException == false) {
            if (apiRequests == null) {
                if (APIClient.SELF_SIGNED != 1) {
                    // System.out.println("------------------------------- get safe ");
                    okHttpClient = new OkHttpClient().newBuilder().connectTimeout(timeout, TimeUnit.SECONDS)
                            .readTimeout(timeout, TimeUnit.SECONDS).writeTimeout(timeout, TimeUnit.SECONDS).build();
                } else {
                    // System.out.println("------------------------------- get unsafe ");
                    okHttpClient = APIClient.getUnsafeOkHttpClient().connectTimeout(timeout, TimeUnit.SECONDS)
                            .readTimeout(timeout, TimeUnit.SECONDS).writeTimeout(timeout, TimeUnit.SECONDS).build();
                }

                Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL).client(okHttpClient)
                        .addConverterFactory(GsonConverterFactory.create()).build();
                apiRequests = retrofit.create(APIRequests.class);
                return apiRequests;
            } else {
                return apiRequests;
            }
        } else {
            if (apiRequestsHandle == null) {
                if (APIClient.SELF_SIGNED != 1) {
                    // System.out.println("------------------------------- get safe ");
                    okHttpClient = new OkHttpClient().newBuilder().connectTimeout(timeout, TimeUnit.SECONDS)
                            .readTimeout(timeout, TimeUnit.SECONDS).writeTimeout(timeout, TimeUnit.SECONDS)
                            .addInterceptor(logging)
                            // .addInterceptor(new Interceptor() {
                            // @Override
                            // public Response intercept(Chain chain) throws IOException {
                            // return onOnIntercept(chain);
                            // }
                            // })
                            .build();
                } else {
                    // System.out.println("------------------------------- get unsafe ");
                    okHttpClient = APIClient.getUnsafeOkHttpClient().connectTimeout(timeout, TimeUnit.SECONDS)
                            .readTimeout(timeout, TimeUnit.SECONDS).writeTimeout(timeout, TimeUnit.SECONDS)
                            .addInterceptor(logging)
                            // .addInterceptor(new Interceptor() {
                            // @Override
                            // public Response intercept(Chain chain) throws IOException {
                            // return onOnIntercept(chain);
                            // }
                            // })
                            .build();
                }

                Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL).client(okHttpClient)
                        .addConverterFactory(GsonConverterFactory.create()).build();
                apiRequestsHandle = retrofit.create(APIRequests.class);
                return apiRequestsHandle;
            } else {
                return apiRequestsHandle;
            }
        }
    }

    public static APIRequests getInstance() {
        // System.out.println("------------------------------- self signed config ");
        // System.out.println(APIClient.SELF_SIGNED);
        return getInstance(true);
    }

    public static APIRequests getUnitServiceInstance() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        if (unitServiceRequests == null) {
            Integer timeout = Integer.parseInt(MainApp.config("config.request_timeout"));
            OkHttpClient okHttpClient;
            if (APIClient.SELF_SIGNED == 1) {
                okHttpClient = new OkHttpClient().newBuilder().connectTimeout(timeout, TimeUnit.SECONDS)
                        .readTimeout(timeout, TimeUnit.SECONDS).writeTimeout(timeout, TimeUnit.SECONDS)
                        .addInterceptor(logging).build();
            } else {
                okHttpClient = APIClient.getUnsafeOkHttpClient().connectTimeout(timeout, TimeUnit.SECONDS)
                        .readTimeout(timeout, TimeUnit.SECONDS).writeTimeout(timeout, TimeUnit.SECONDS)
                        .addInterceptor(logging).build();
            }

            Retrofit retrofit = new Retrofit.Builder().baseUrl(MainApp.config("config.unit_service_address"))
                    .client(okHttpClient).addConverterFactory(GsonConverterFactory.create()).build();
            unitServiceRequests = retrofit.create(APIRequests.class);
            return unitServiceRequests;
        } else {
            return unitServiceRequests;
        }
    }

    public static OkHttpClient.Builder getUnsafeOkHttpClient() {
        // System.out.println("------------------------ get unsafe https client");
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
                @Override
                public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType)
                        throws CertificateException {
                }

                @Override
                public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType)
                        throws CertificateException {
                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[] {};
                }
            } };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            // OkHttpClient okHttpClient = builder.build();
            return builder;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
