package com.tura.fms.screen.models.prestart_model.submit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PrestartResponseDetail {

    @SerializedName("id")
    @Expose
    String id;

    @SerializedName("__id_equipment__")
    @Expose
    String id_equipment;

    @SerializedName("__has_id_equipment__")
    @Expose
    Boolean has_id_equipment;

    @SerializedName("__id_user__")
    @Expose
    String id_user;

    @SerializedName("__has_id_user__")
    @Expose
    Boolean has_id_user;

    @SerializedName("date")
    @Expose
    Long date;

    @SerializedName("__id_prestart__")
    @Expose
    Integer id_prestart;

    @SerializedName("__has_id_prestart__")
    @Expose
    Boolean has_id_prestart;

    @SerializedName("__id_actual__")
    @Expose
    String id_actual;

    @SerializedName("__has_id_actual__")
    @Expose
    Boolean has_id_actual;

    @SerializedName("qty")
    @Expose
    Integer qty;

    @SerializedName("value")
    @Expose
    String value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_equipment() {
        return id_equipment;
    }

    public void setId_equipment(String id_equipment) {
        this.id_equipment = id_equipment;
    }

    public Boolean getHas_id_equipment() {
        return has_id_equipment;
    }

    public void setHas_id_equipment(Boolean has_id_equipment) {
        this.has_id_equipment = has_id_equipment;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public Boolean getHas_id_user() {
        return has_id_user;
    }

    public void setHas_id_user(Boolean has_id_user) {
        this.has_id_user = has_id_user;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Integer getId_prestart() {
        return id_prestart;
    }

    public void setId_prestart(Integer id_prestart) {
        this.id_prestart = id_prestart;
    }

    public Boolean getHas_id_prestart() {
        return has_id_prestart;
    }

    public void setHas_id_prestart(Boolean has_id_prestart) {
        this.has_id_prestart = has_id_prestart;
    }

    public String getId_actual() {
        return id_actual;
    }

    public void setId_actual(String id_actual) {
        this.id_actual = id_actual;
    }

    public Boolean getHas_id_actual() {
        return has_id_actual;
    }

    public void setHas_id_actual(Boolean has_id_actual) {
        this.has_id_actual = has_id_actual;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

}
