package com.tura.fms.screen.database;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.models.user_model.*;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;

import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class User_Info_DB extends Operation {
    // Table Name
    public static final String TABLE_NAME = "User_Record";
    // Table Columns
    private static final String id = "id";
    private static final String id_crew = "id_crew";
    private static final String id_org = "id_org";
    private static final String cd_role = "cd_role";
    private static final String cd_department = "cd_department";
    private static final String name = "name";
    private static final String email = "email";
    private static final String pwd = "pwd";
    private static final String staff_id = "staff_id";
    private static final String fingerprint_id = "fingerprint_id";
    private static final String socket_id = "socket_id";
    private static final String file_picture = "file_picture";
    private static final String ip_address = "ip_address";
    private static final String token = "token";
    private static final String enum_availability = "enum_availability";
    private static final String is_authenticated = "is_authenticated";
    private static final String json = "json";
    // private static final int sync_status = 0;

    public User_Info_DB() {
        super(User_Info_DB.TABLE_NAME, ConstantValues.DATA_USER);
    }

    public void createTableUser() {
        try {
            DatabaseMetaData dbm = conn().getMetaData();
            ResultSet rs = dbm.getTables(null, null, TABLE_NAME, null);
            Statement stmt = null;
            if (rs.next()) {
                System.out.println("Table exists User");
                // stmt.close();
                // conn().close();
            } else {
                stmt = conn().createStatement();
                stmt.setQueryTimeout(10);
                stmt.executeUpdate(createTable());
                stmt.close();
                // conn().close();
                System.out.println("Table created User ");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String createTable() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + id + " TEXT PRIMARY KEY," + id_crew + " TEXT,"
                + id_org + " TEXT," + cd_role + " TEXT," + cd_department + " TEXT," + name + " TEXT," + email + " TEXT,"
                + pwd + " TEXT," + staff_id + " TEXT," + fingerprint_id + " TEXT," + socket_id + " TEXT," + file_picture
                + " TEXT," + ip_address + " TEXT," + token + " TEXT," + enum_availability + " TEXT," + is_authenticated
                + " TEXT," + json + " TEXT," + "sync_status NUMBER NULL )";
    }

    @Override
    public Object insertIgnore(Object us) {
        UserAPI user = Synchronizer.mapper.convertValue(us, UserAPI.class);
        PreparedStatement st = null;
        Boolean rs = null;
        try {
            String sql = "INSERT INTO " + TABLE_NAME + " " + "(" + id + "," + id_crew + "," + id_org + "," + cd_role
                    + "," + cd_department + "," + name + "," + email + "," + pwd + "," + staff_id + "," + fingerprint_id
                    + "," + socket_id + "," + file_picture + "," + ip_address + "," + token + "," + enum_availability
                    + "," + is_authenticated + "," + json + ", sync_status)"
                    + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,1) ON CONFLICT(id) DO UPDATE SET " + id + "=?,"
                    + id_crew + "=?," + id_org + "=?," + cd_role + "=?," + cd_department + "=?," + name + "=?," + email
                    + "=?," + pwd + "=?," + staff_id + "=?," + fingerprint_id + "=?," + socket_id + "=?," + file_picture
                    + "=?," + ip_address + "=?," + token + "=?," + enum_availability + "=?," + is_authenticated + "=?;";

            st = conn().prepareStatement(sql);
            st.setString(1, user.getId());
            st.setString(18, user.getId());
            if (user.getIdCrew() != null) {
                st.setString(2, user.getIdCrew().toString());
                st.setString(19, user.getIdCrew().toString());
            } else {
                st.setString(2, "");
                st.setString(19, "");
            }
            if (user.getIdOrg() != null) {
                st.setString(3, user.getIdOrg().getId());
                st.setString(20, user.getIdOrg().getId());
            } else {
                st.setString(3, "");
                st.setString(20, "");
            }
            if (user.getCdRole() != null) {
                st.setString(4, user.getCdRole().getCd());
                st.setString(21, user.getCdRole().getCd());
            } else {
                st.setString(4, "");
                st.setString(21, "");
            }
            if (user.getCdDepartment() != null) {
                st.setString(5, user.getCdDepartment().getCd());
                st.setString(22, user.getCdDepartment().getCd());
            } else {
                st.setString(5, "");
                st.setString(22, "");
            }
            st.setString(6, user.getName());
            st.setString(23, user.getName());
            st.setString(7, user.getEmail());
            st.setString(24, user.getEmail());
            st.setString(8, user.getPwd());
            st.setString(25, user.getPwd());
            st.setString(9, user.getStaffId());
            st.setString(26, user.getStaffId());
            st.setString(10, user.getFingerprintId());
            st.setString(27, user.getFingerprintId());
            st.setString(11, user.getSocketId());
            st.setString(28, user.getSocketId());
            st.setString(12, user.getFilePicture());
            st.setString(29, user.getFilePicture());
            st.setString(13, user.getIpAddress());
            st.setString(30, user.getIpAddress());
            st.setString(14, user.getToken());
            st.setString(31, user.getToken());
            st.setInt(15, user.getEnumAvailability());
            st.setInt(32, user.getEnumAvailability());
            st.setInt(16, user.getIsAuthenticated());
            st.setInt(33, user.getIsAuthenticated());
            if (user.getJson() != null) {
                st.setString(17, user.getJson().toString());
            } else {
                st.setString(17, "");
            }
            rs = st.execute();
            st.close();

            return user;

        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            if (rs) {
                try {
                    st.close();
                    // conn().close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }

    public UserAPILocal userLoginLocal(String nrp, String pwd_param) {
        try {
            ResultSet cursor = null;

            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE " + staff_id + " =? and " + pwd + "=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, nrp);
            preparedStatement.setString(2, pwd_param);

            cursor = preparedStatement.executeQuery();
            UserAPILocal userlocal = new UserAPILocal();

            boolean found = false;

            while (cursor.next()) {
                found = true;
                System.out.println("ID " + cursor.getString(1));
                userlocal.setId(cursor.getString(1));
                userlocal.setIdCrew(cursor.getString(2));
                userlocal.setIdOrg(cursor.getString(3));
                userlocal.setCdRole(cursor.getString(4));
                userlocal.setCdDepartment(cursor.getString(5));
                userlocal.setName(cursor.getString(6));
                userlocal.setEmail(cursor.getString(7));
                userlocal.setPwd(cursor.getString(8));
                userlocal.setStaffId(cursor.getString(9));
                userlocal.setFingerprintId(cursor.getString(10));
                userlocal.setSocketId(cursor.getString(11));
                userlocal.setFilePicture(cursor.getString(12));
                userlocal.setIpAddress(cursor.getString(13));
                userlocal.setToken(cursor.getString(14));
                userlocal.setEnumAvailability(cursor.getInt(15));
                userlocal.setIsAuthenticated(cursor.getInt(16));
                userlocal.setJson(cursor.getString(17));
            }

            // close the Database
            // conn().close();
            cursor.close();

            if (found) {
                return userlocal;
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public int getExisting(String id_param) {
        try {
            String sql = "SELECT count(*) as total FROM " + TABLE_NAME + " WHERE  " + id + "=?";

            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, id_param);
            ResultSet cursor = preparedStatement.executeQuery();

            int jumlah;

            if (cursor.next()) {
                jumlah = cursor.getInt("total");
            } else {
                jumlah = 0;
            }
            cursor.close();
            // conn().close();
            return jumlah;
        } catch (Exception e) {
            return 0;
        }
    }

    public int getExistingLogin(String nrp, String pwd_param) {
        try {
            String sql = "SELECT count(*) as total FROM " + TABLE_NAME + " WHERE " + staff_id + " =? and " + pwd + "=?";

            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, nrp);
            preparedStatement.setString(2, pwd_param);
            ResultSet cursor = preparedStatement.executeQuery();

            int jumlah;

            if (cursor.next()) {
                jumlah = cursor.getInt("total");
            } else {
                jumlah = 0;
            }
            cursor.close();
            // conn().close();
            return jumlah;
        } catch (Exception e) {
            return 0;
        }
    }

    public void updateUserData(UserAPI user) {
        Boolean rs = null;
        String sql = "UPDATE " + TABLE_NAME + " SET " + "" + id + " = ?, " + "" + id_crew + " = ?, " + "" + id_org
                + " = ?, " + "" + cd_role + " = ?, " + "" + cd_department + " = ?, " + "" + name + " = ?, " + "" + email
                + " = ?, " + "" + pwd + " = ?, " + "" + staff_id + " = ?, " + "" + fingerprint_id + " = ?, " + ""
                + socket_id + " = ?, " + "" + file_picture + " = ?, " + "" + ip_address + " = ?, " + "" + token
                + " = ?, " + "" + enum_availability + " = ?, " + "" + is_authenticated + " = ?, " + "" + json + " = ?,"
                + "" + "sync_status = 1" + " WHERE " + id + " = ?";

        try {
            PreparedStatement st = null;
            st = conn().prepareStatement(sql);
            st.setString(1, user.getId());
            if (user.getIdCrew() != null) {
                st.setString(2, user.getIdCrew().toString());
            } else {
                st.setString(2, "");
            }
            if (user.getIdOrg() != null) {
                st.setString(3, user.getIdOrg().getId());
            } else {
                st.setString(3, "");
            }
            if (user.getCdRole() != null) {
                st.setString(4, user.getCdRole().getCd());
            } else {
                st.setString(4, "");
            }
            if (user.getCdDepartment() != null) {
                st.setString(5, user.getCdDepartment().getCd());
            } else {
                st.setString(5, "");
            }
            st.setString(6, user.getName());
            st.setString(7, user.getEmail());
            st.setString(8, user.getPwd());
            st.setString(9, user.getStaffId());
            st.setString(10, user.getFingerprintId());
            st.setString(11, user.getSocketId());
            st.setString(12, user.getFilePicture());
            st.setString(13, user.getIpAddress());
            st.setString(14, user.getToken());
            st.setInt(15, user.getEnumAvailability());
            st.setInt(16, user.getIsAuthenticated());
            if (user.getJson() != null) {
                st.setString(17, user.getJson().toString());
            } else {
                st.setString(17, "");
            }
            rs = st.execute();
            st.close();
            // conn().close();
            // SQLiteJDBC.connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {

            if (rs) {
                try {
                    // conn().close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void deleteUserData(String email3) {
        String sql = "delete from " + TABLE_NAME + " where " + email + "=?";
        try {
            Statement stmt = conn().createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteData(String ids) {
        String sql = "delete from " + TABLE_NAME + " where " + id + "='" + ids + "'";
        try {
            Statement stmt = conn().createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object getFromServer(Integer page, Integer size) {
        if (!NetworkConnection.getServiceStatus()) {
            return null;
        }
        Call<Object> call = APIClient.getInstance().getUser(MainApp.requestHeader, page, size);
        try {
            Response<Object> response = call.execute();
            return response.body();
        } catch (IOException ex) {
            // ex.printStackTrace();
            System.out.println("[User getFromServer] " + ex.getMessage());
        }
        return null;
    }

    @Override
    public Object getFromServerById(String id) {
        if (!NetworkConnection.getServiceStatus()) {
            return null;
        }
        Call<Object> call = APIClient.getInstance().getUserById(MainApp.requestHeader, id);
        try {
            Response<Object> response = call.execute();
            return response.body();
        } catch (IOException ex) {
            // ex.printStackTrace();
            System.out.println("[User getFromServerById] " + ex.getMessage());
        }
        return null;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Integer getRemoteCount() {
        if (!NetworkConnection.getServiceStatus()) {
            return 0;
        }
        Call<Object> call = APIClient.getInstance().getUserCount(MainApp.requestHeader);
        try {
            Response<Object> response = call.execute();
            Map<String, Number> mapped = APIClient.mapper.convertValue(response.body(), Map.class);
            if (mapped.get("output") != null) {
                return mapped.get("output").intValue();
            } else {
                return 0;
            }
        } catch (IOException ex) {
            // ex.printStackTrace();
            System.out.println("[User getRemoteCount] " + ex.getMessage());
        }
        return 0;
    }

    @Override
    public List<Object> getFlaggedData() {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE sync_status=?";

            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, "0");
            ResultSet cursor = preparedStatement.executeQuery();

            List<Object> result = new ArrayList<Object>();

            while (cursor.next()) {
                UserAPILocal userlocal = new UserAPILocal();
                userlocal.setId(cursor.getString(1));
                userlocal.setIdCrew(cursor.getString(2));
                userlocal.setIdOrg(cursor.getString(3));
                userlocal.setCdRole(cursor.getString(4));
                userlocal.setCdDepartment(cursor.getString(5));
                userlocal.setName(cursor.getString(6));
                userlocal.setEmail(cursor.getString(7));
                userlocal.setPwd(cursor.getString(8));
                userlocal.setStaffId(cursor.getString(9));
                userlocal.setFingerprintId(cursor.getString(10));
                userlocal.setSocketId(cursor.getString(11));
                userlocal.setFilePicture(cursor.getString(12));
                userlocal.setIpAddress(cursor.getString(13));
                userlocal.setToken(cursor.getString(14));
                userlocal.setEnumAvailability(cursor.getInt(15));
                userlocal.setIsAuthenticated(cursor.getInt(16));
                userlocal.setJson(cursor.getString(17));
                result.add(userlocal);
            }
            cursor.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void postBulkData(List<Object> eq) {
        if (!NetworkConnection.getServiceStatus()) {
            return;
        }
        // do nothing
    }

    @Override
    public void setSynchronized(String ids) {
        String sql = "update " + TABLE_NAME + " set sync_status=1 where " + id + "='" + ids + "'";
        try {
            Statement stmt = conn().createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
