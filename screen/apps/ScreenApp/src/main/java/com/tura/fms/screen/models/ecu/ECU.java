package com.tura.fms.screen.models.ecu;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class ECU implements ECUInterface{

    public ECU() { }

    public ResponseData getHourMeter() {
        return new ResponseData(LocalDateTime.now(), "hour", 100.0);
    }

    public ResponseData getFuelRate() {
        return new ResponseData(LocalDateTime.now(), "litre/hour", 100.0);
    }

    public ResponseData getDistance() {
        return new ResponseData(LocalDateTime.now(), "km", 100.0);
    }

    public ResponseData getEngineTemperature() {
        return new ResponseData(LocalDateTime.now(), "^C", 100.0);
    }

    public ResponseData getPayload() {
        return new ResponseData(LocalDateTime.now(), "BCM", 100.0);
    }

    public ResponseData getVehicleSpeed() {
        return new ResponseData(LocalDateTime.now(), "km/hour", 100.0);
    }

    public Map<String, ResponseData> getAllECU() {
        Map<String, ResponseData> response = new HashMap<>();
        response.put(ECUData.HOUR_METER, new ResponseData(LocalDateTime.now(), "hour", 100.0));
        return response;
    }

    public ResponseGPS getGPS() {
        return new ResponseGPS(LocalDateTime.now(), 100.0, 100.0);
    }

    public void setTimezone(String tz) { }

    public void setECUPrefetchRate(Integer rate) { }

    public void setGPSPrefetchRate(Integer rate) { }

    public void setSoundVolume(Integer volume) { }

    public void setUnitID(String id) { }

    public String getTimezone() {
        return "";
    }

    public Integer getECUPrefetchRate() {
        return 0;
    }

    public Integer getGPSPrefetchRate() {
        return 0;
    }

    public Integer getSoundVolume() {
        return 0;
    }

    public String getUnitID() {
        return "";
    }

}
