package com.tura.fms.screen.presenter;

public class CustomRunnableX<T> implements Runnable {
    private T data;

    public void setData(T data) {
        this.data = data;
    }

    public T getData() {
        return this.data;
    }

    @Override
    public void run() {
    }
}