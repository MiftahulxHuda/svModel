package com.tura.fms.screen.models.equipment;

import com.google.gson.annotations.SerializedName;

public class EquipmentLogStartRequest {

    @SerializedName("id")
    private String id;

    @SerializedName("id_actual")
    private String id_actual;

    @SerializedName("id_operation_support")
    private String id_operation_support;

    @SerializedName("id_equipment")
    private String id_equipment;

    @SerializedName("id_operator")
    private String id_operator;

    @SerializedName("cd_activity")
    private String cd_activity;

    @SerializedName("cd_tum")
    private String cd_tum;

    @SerializedName("cd_type")
    private String cd_type;

    @SerializedName("time")
    private String time;

    @SerializedName("latitude")
    private String latitude;

    @SerializedName("longitude")
    private String longitude;

    public void setLat(String lat) {
        latitude = lat;
    }

    public String getLat() {
        return latitude;
    }

    public void setLon(String lon) {
        longitude = lon;
    }

    public String getLon() {
        return longitude;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdActual() {
        return id_actual;
    }

    public void setIdActual(String id_actual) {
        this.id_actual = id_actual;
    }

    public String getIdOperationSupport() {
        return id_operation_support;
    }

    public void setIdOperationSupport(String id_operation_support) {
        this.id_operation_support = id_operation_support;
    }

    public String getIdOperator() {
        return id_operator;
    }

    public void setIdOperator(String id_operator) {
        this.id_operator = id_operator;
    }

    public String getCdTum() {
        return cd_tum;
    }

    public void setCdTum(String cd_tum) {
        this.cd_tum = cd_tum;
    }

    public String getCdType() {
        return cd_type;
    }

    public void setCdType(String cdtype) {
        this.cd_type = cdtype;
    }

    public String getIdEquipment() {
        return id_equipment;
    }

    public void setIdEquipment(String id_equipment) {
        this.id_equipment = id_equipment;
    }

    public String getCdActivity() {
        return cd_activity;
    }

    public void setCdActivity(String cd_activity) {
        this.cd_activity = cd_activity;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}