package com.tura.fms.screen.kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;

import java.util.Properties;

import static com.tura.fms.screen.constanst.IKafkaConstant.*;

public class ProducerCreator {

    public static Producer<String, String> createProducer() {
        String jaasTemplate = "org.apache.kafka.common.security.scram.ScramLoginModule required username=\"%s\" password=\"%s\";";
        String jaasCfg = String.format(jaasTemplate, username_kafka, password_kafka);

        String serializer = "org.apache.kafka.common.serialization.StringSerializer";
        String deserializer = "org.apache.kafka.common.serialization.StringDeserializer";

        final Properties props = new Properties();
        props.put("bootstrap.servers", KAFKA_BROKERS);
        props.put("group.id", GROUP_ID_CONFIG);
        props.put("enable.auto.commit", "true");
        props.put("auto.commit.interval.ms", AUTO_COMMIT_INTERVAL);
        props.put("auto.offset.reset", OFFSET_RESET_EARLIER);
        props.put("session.timeout.ms", SESSION_TIMEOUT);
        props.put("key.deserializer", deserializer);
        props.put("value.deserializer", deserializer);
        props.put("key.serializer", serializer);
        props.put("value.serializer", serializer);
        props.put("security.protocol", SECURITY_PROTOCOL);
        props.put("sasl.mechanism", mechanism_kafka);
        props.put("sasl.jaas.config", jaasCfg);
        // props.put("request.timeout.ms", FETCH_TIMEOUT);
        // props.put("delivery.timeout.ms", FETCH_TIMEOUT);
        // props.put("metadata.fetch.timeout.ms", FETCH_TIMEOUT);
        // props.put(ProducerConfig.REQUEST_TIMEOUT_MS_CONFIG, FETCH_TIMEOUT);

        final Producer<String, String> producer = new KafkaProducer<>(props);
        return producer;
    }
}
