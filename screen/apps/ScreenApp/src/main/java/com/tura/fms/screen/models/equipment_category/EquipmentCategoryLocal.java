package com.tura.fms.screen.models.equipment_category;

import java.util.HashMap;
import java.util.Map;

public class EquipmentCategoryLocal {
    private String id;
    private String cd_manufactur;
    private String cd_product;
    private String cd_type;
    private String cd_brand;
    private String cd_model;
    private String cd_class;
    private Double bucket_capacity;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCdManufactur() {
        return cd_manufactur;
    }

    public void setCdManufactur(String manufactur) {
        this.cd_manufactur = manufactur;
    }

    public String getCdProduct() {
        return cd_product;
    }

    public void setCdProduct(String product) {
        this.cd_product = product;
    }

    public String getCdType() {
        return cd_type;
    }

    public void setCdType(String type) {
        this.cd_type = type;
    }

    public String getCdBrand() {
        return cd_brand;
    }

    public void setCdBrand(String brand) {
        this.cd_brand = brand;
    }

    public String getCdModel() {
        return cd_model;
    }

    public void setCdModel(String model) {
        this.cd_model = model;
    }

    public String getCdClass() {
        return cd_class;
    }

    public void setCdClass(String cls) {
        this.cd_class = cls;
    }

    public Double getBucketCapacity() {
        return bucket_capacity;
    }

    public void setBucketCapacity(Double capacity) {
        this.bucket_capacity = capacity;
    }

}
