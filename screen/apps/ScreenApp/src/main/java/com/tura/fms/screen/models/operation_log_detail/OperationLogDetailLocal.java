package com.tura.fms.screen.models.operation_log_detail;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "date", "id_actual", "cd_time", "id_equipment", "id_operator", "cd_equipment_type",
        "id_location", "id_loader", "latitude", "longitude", "bucket_count" })

public class OperationLogDetailLocal {

    @JsonProperty("id")
    private String id;
    @JsonProperty("date")
    private String date;
    @JsonProperty("id_actual")
    private String id_actual;
    @JsonProperty("cd_time")
    private String cd_time;
    @JsonProperty("id_equipment")
    private String id_equipment;
    @JsonProperty("id_operator")
    private String id_operator;
    @JsonProperty("cd_equipment_type")
    private String cd_equipment_type;
    @JsonProperty("id_location")
    private String id_location;
    @JsonProperty("id_loader")
    private String id_loader;
    @JsonProperty("latitude")
    private Double latitude;
    @JsonProperty("longitude")
    private Double longitude;
    @JsonProperty("bucket_count")
    private Integer bucket_count;

    public void setLat(Double lat) {
        latitude = lat;
    }

    public Double getLat() {
        return latitude;
    }

    public void setLon(Double lon) {
        longitude = lon;
    }

    public Double getLon() {
        return longitude;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdActual() {
        return id_actual;
    }

    public void setIdActual(String id_actual) {
        this.id_actual = id_actual;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCdTime() {
        return cd_time;
    }

    public void setCdTime(String cd_time) {
        this.cd_time = cd_time;
    }

    public String getIdEquipment() {
        return id_equipment;
    }

    public void setIdEquipment(String id_equipment) {
        this.id_equipment = id_equipment;
    }

    public String getIdLoader() {
        return id_loader;
    }

    public void setIdLoader(String id_loader) {
        this.id_loader = id_loader;
    }

    public String getIdOperator() {
        return id_operator;
    }

    public void setIdOperator(String id_operator) {
        this.id_operator = id_operator;
    }

    public String getCdEquipmentType() {
        return cd_equipment_type;
    }

    public void setCdEquipmentType(String cd_equipment_type) {
        this.cd_equipment_type = cd_equipment_type;
    }

    public String getIdLocation() {
        return id_location;
    }

    public void setIdLocation(String id_location) {
        this.id_location = id_location;
    }

    public Integer getBucketCount() {
        return bucket_count;
    }

    public void setBucketCount(Integer bucket_count) {
        this.bucket_count = bucket_count;
    }

}
