package com.tura.fms.screen.models.prestart_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PrestartDataRest {

    @SerializedName("respon")
    @Expose
    String respon;

    @SerializedName("message")
    @Expose
    String message;

    @SerializedName("pages")
    @Expose
    int pages;

    @SerializedName("totalItems")
    @Expose
    int totalItems;

    @SerializedName("currentPage")
    @Expose
    String currentPage;

    @SerializedName("output")
    @Expose
    List<PrestartDataDetail> output = null;

    public String getRespon() {
        return respon;
    }

    public void setRespon(String respon) {
        this.respon = respon;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public List<PrestartDataDetail> getOutput() {
        return output;
    }

    public void setOutput(List<PrestartDataDetail> output) {
        this.output = output;
    }
}
