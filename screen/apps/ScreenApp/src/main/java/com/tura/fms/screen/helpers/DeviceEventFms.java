package com.tura.fms.screen.helpers;

import org.json.JSONObject;

public interface DeviceEventFms {
    void onData(JSONObject var1);
}
