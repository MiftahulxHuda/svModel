package com.tura.fms.screen.models.actual_support;

import com.google.gson.annotations.SerializedName;

public class SupportData {

    @SerializedName("output")
    private OperationSupportLocal output;

    @SerializedName("response")
    private String response;

    @SerializedName("message")
    private String message;

    public OperationSupportLocal getOutput() {
        return output;
    }

    public void setOutput(OperationSupportLocal output) {
        this.output = output;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String res) {
        this.response = res;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String mes) {
        this.message = mes;
    }

}
