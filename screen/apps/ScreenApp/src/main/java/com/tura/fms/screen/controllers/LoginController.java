package com.tura.fms.screen.controllers;

import static javafx.geometry.Pos.CENTER_LEFT;
import static org.javalite.app_config.AppConfig.p;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXSpinner;
import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConfigApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.helpers.GPS;
import com.tura.fms.screen.helpers.NavigationUI;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.constant.Constant;
import com.tura.fms.screen.models.equipment.EquipmentLogStartLocal;
import com.tura.fms.screen.models.operation_status.OperationStatusLocal;
import com.tura.fms.screen.models.user_model.UserAPILocal;
import com.tura.fms.screen.models.user_model.UserCdRole;
import com.tura.fms.screen.models.user_model.UserDetails;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;
import com.tura.fms.screen.presenter.UserPresenterAsync;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Duration;

public class LoginController implements Initializable {

    ConfigApp configApp = new ConfigApp();

    @FXML
    private TextField usernameField, passwordField;

    @FXML
    private Label errorLabel;

    @FXML
    private Label unitLabel;

    @FXML
    private Label statusLabel;

    @FXML
    private Label gpsLabel;

    @FXML
    private Label versionLabel;

    private NavigationUI navigationUI = new NavigationUI();

    @FXML
    StackPane rootPane;

    @FXML
    private HBox kotakError;

    // @FXML
    // private ToggleGroup toggleGroup;

    @FXML
    private RadioButton radio_1;

    @FXML
    private RadioButton radio_2;

    @FXML
    JFXSpinner loading;

    @FXML
    Button buttonDoLogin;

    // SessionFMS sessionFMS;

    String sesi_level;

    final Map<String, String> refUnitType = new HashMap<String, String>() {
        {
            put("HAULER", "015001");
            put("LOADER", "015002");
            put("GRADDER", "015003");
            put("DOZZER", "015004");
        }
    };

    Timeline clockDashboard = null;

    boolean flipFlop = false;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                buttonDoLogin.requestFocus();
            }
        });

        NavigationUI.publicStackPane = rootPane;

        errorLabel.setWrapText(true);

        String nik = "";

        String unitInfo = "";

        if (MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_HAULER)) {
            if (Utilities.getCurrentShift().equals(ConstantValues.SHIFT_NIGHT)) {
                // nik = "10150058";
            } else {
                // nik = "10150124";
            }

            unitInfo = "Hauler - ";
        } else if (MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_LOADER)) {
            // nik = "10180077";

            unitInfo = "Loader - ";
        } else if (MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_GRADDER)) {
            // nik = "10170257";

            unitInfo = "Grader - ";
        } else if (MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_DOZZER)) {
            // nik = "10170257";

            unitInfo = "Dozer - ";
        }

        unitInfo += MainApp.config("config.unit_id").trim();
        unitLabel.setText(unitInfo);

        // usernameField.setText(nik);
        // usernameField.setText("10150162"); // mekanik online

        usernameField.setPromptText("masukan nik");
        usernameField.getProperties().put("vkType", "numeric");
        // usernameField.setStyle("-fx-text-fill: #d9c750;");

        // passwordField.setText("demo");
        passwordField.setPromptText("masukan password");
        passwordField.getProperties().put("vkType", "text");
        // passwordField.setStyle("-fx-text-fill: #d9c750;");

        loading.setVisible(false);

        versionLabel.setText("ScreenApp-v1.2.4");

        // if (sessionFMS.cekSession()) {
        // try {
        // navigationUI.windowsNew(p("app.linkDashboard"), p("app.titleDashboard"));
        // } catch (Exception e) {
        // e.printStackTrace();
        // }
        // }

        // Publisher publisher = new Publisher();
        // publisher.start();

        // Subscriber subscriber = new Subscriber();
        // subscriber.start();

        Integer ecuReader = Integer.parseInt(MainApp.config("config.activate_ecu_reader"));

        clockDashboard = new Timeline(new KeyFrame(Duration.ZERO, e -> {
            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    if ((MainApp.apiStatus == ConstantValues.DONE) && APIClient.exceptionHappened) {
                        loading.setVisible(false);
                        buttonDoLogin.setDisable(false);
                        errorLabel.setText("Silakan periksa jaringan dan service FMS");
                    }

                    String netS = NetworkConnection.getStringStatus();
                    statusLabel.setText(netS);
                    statusLabel.getStyleClass().clear();
                    if (netS.equals("Online")) {
                        statusLabel.getStyleClass().add("font-online");
                    } else {
                        statusLabel.getStyleClass().add("font-offline");
                    }

                    if (ecuReader == 1) {

                        String latStr = String.format("%.4f", MainApp.latestLat);
                        String lonStr = String.format("%.4f", MainApp.latestLon);

                        gpsLabel.setText("GPS: " + latStr + " - " + lonStr);
                        gpsLabel.getStyleClass().clear();

                        if (netS.equals("Online")) {
                            if (flipFlop) {
                                gpsLabel.getStyleClass().add("font-online");
                                flipFlop = false;
                            } else {
                                gpsLabel.getStyleClass().add("font-white");
                                flipFlop = true;
                            }

                        } else {
                            if (flipFlop) {
                                gpsLabel.getStyleClass().add("font-offline");
                                flipFlop = false;
                            } else {
                                gpsLabel.getStyleClass().add("font-white");
                                flipFlop = true;
                            }
                        }

                    }

                }
            });
        }), new KeyFrame(Duration.seconds(MainApp.dashboardInterval)));
        clockDashboard.setCycleCount(Animation.INDEFINITE);
        clockDashboard.play();

        enterPressed();
    }

    private void enterPressed() {

        // usernameField.setOnKeyPressed((KeyEvent ke) -> {
        // if (ke.getCode().equals(KeyCode.ENTER)) {
        // try {
        // authenticate(ke);
        // } catch (Exception ex) {
        // System.out.println(ex.getMessage());
        // }
        // }
        // });

        /** membuat exception */
        // usernameField.setOnAction(new EventHandler<ActionEvent>() {
        // @Override
        // public void handle(ActionEvent e) {
        // // javafx.application.Application.launch(KeyboardV.class);
        // }
        // });

        // passwordField.setOnKeyPressed((KeyEvent ke) -> {
        // if (ke.getCode().equals(KeyCode.ENTER)) {
        // try {
        // authenticate(ke);
        // } catch (Exception ex) {
        // System.out.println(ex.getMessage());
        // }
        // }
        // });
    }

    @FXML
    private void prosesLogin(ActionEvent event) throws Exception {
        /**
         * pastikan server UP
         */
        NetworkConnection.getAPIUpStatus();
        NetworkConnection.getKafkaUpStatus();

        if (NetworkConnection.getServiceStatus()) {
            authenticate(event);

        } else {
            Utilities.confirm(rootPane, "Tidak dapat terkoneksi ke server, aktifkan offline mode ?", new Runnable() {

                @Override
                public void run() {
                    try {
                        offlineAuthenticate(event);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

            }, new Runnable() {

                @Override
                public void run() {
                    // no action
                }

            });

        }

    }

    private void authenticate(Event event) throws Exception {
        MainApp.offlineMode = false;

        buttonDoLogin.setDisable(true);

        loading.setVisible(true);

        MainApp.latestRefEvent = event;

        if (validateInput()) {
            MainApp.pauseEmit = true;

            String username = usernameField.getText().trim();
            String password = passwordField.getText().trim();

            sesi_level = MainApp.config("config.unit_type").trim();

            UserPresenterAsync loginTestUnit = new UserPresenterAsync();

            loginTestUnit.login_async(username, password, sesi_level, true, errorLabel, loading, kotakError, event,
                    new Runnable() {

                        @Override
                        public void run() {
                            buttonDoLogin.setDisable(false);
                            loading.setVisible(false);

                            clockDashboard.stop();
                        }
                    }, new Runnable() {

                        @Override
                        public void run() {

                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {

                                    Utilities.confirm(rootPane,
                                            "Tidak dapat terkoneksi ke server, aktifkan offline mode ?",
                                            new Runnable() {

                                                @Override
                                                public void run() {
                                                    try {
                                                        offlineAuthenticate(event);
                                                    } catch (Exception ex) {
                                                        ex.printStackTrace();
                                                    }
                                                }

                                            }, new Runnable() {

                                                @Override
                                                public void run() {
                                                    // no action
                                                }

                                            });
                                }
                            });

                        }
                    });

        } else {

            resetFields();
            errorLabel.setText("User field all required");
            kotakError.setVisible(true);
            loading.setVisible(false);
            buttonDoLogin.setDisable(false);

        }

    }

    public void offlineAuthenticate(Event event) throws Exception {
        MainApp.offlineMode = true;

        buttonDoLogin.setDisable(true);

        loading.setVisible(true);

        MainApp.latestRefEvent = event;

        if (validateInput()) {
            MainApp.pauseEmit = true;

            String username = usernameField.getText().trim();
            String password = passwordField.getText().trim();

            UserAPILocal user = MainApp.userDB.userLoginLocal(username, Utilities.getMd5(password));

            if (user != null) {
                GPS gps = Utilities.getCurrentGPS();

                UserDetails userDetail = new UserDetails();
                userDetail.setId(user.getId());
                userDetail.setName(user.getName());
                UserCdRole cd_role = new UserCdRole();
                cd_role.setCd(user.getCdRole());
                userDetail.setCd_role(cd_role);
                userDetail.setToken(user.getToken());

                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        buttonDoLogin.setDisable(false);
                        loading.setVisible(false);
                        clockDashboard.stop();
                    }
                });

                MainApp.loginCallback(true, userDetail);

                if (userDetail.getCd_role().getCd().equals(ConstantValues.ROLE_MECHANIC)) {
                    MainApp.isMechanic = true;
                }
                if (userDetail.getCd_role().getCd().equals(ConstantValues.ROLE_OPERATOR)) {
                    MainApp.isOperator = true;
                }
                if (userDetail.getCd_role().getCd().equals(ConstantValues.ROLE_SUPERVISOR)) {
                    MainApp.isSupervisor = true;
                }
                if (userDetail.getCd_role().getCd().equals(ConstantValues.ROLE_GROUP_LEADER)) {
                    MainApp.isGroupLeader = true;
                }

                if (MainApp.isMechanic) {
                    UserPresenterAsync.jika_berhasil_mekanik(errorLabel, loading, kotakError, event, userDetail, user,
                            false, "Offline");

                } else if (MainApp.isSupervisor || MainApp.isGroupLeader) {
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            try {
                                NavigationUI.windowsNewScene(p("app.linkPrestart"), p("app.titlePrestart"), event,
                                        true);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    });

                } else {
                    List<OperationStatusLocal> latestActivity = MainApp.operationStatusDB
                            .getLatestActivity(MainApp.equipmentLocal.getId());

                    if (latestActivity.isEmpty()) {

                        OperationStatusLocal changeShift = new OperationStatusLocal();
                        changeShift.setIdEquipment(MainApp.equipmentLocal.getId());
                        changeShift.setIdOperator(MainApp.user.getId());
                        changeShift.setCdActivity(ConstantValues.ACTIVITY_CHANGE_SHIFT);
                        changeShift.setCdTum(ConstantValues.TUM_STANDBY);
                        changeShift.setCdType(MainApp.config("config.unit_type"));
                        changeShift.setTime(Utilities.getStartOfShift());
                        changeShift.setLatitude(gps.getLat());
                        changeShift.setLongitude(gps.getLon());
                        MainApp.operationStatusDB.insertIgnore(changeShift);

                        OperationStatusLocal prestart = new OperationStatusLocal();
                        prestart.setIdEquipment(MainApp.equipmentLocal.getId());
                        prestart.setIdOperator(MainApp.user.getId());
                        prestart.setCdActivity(ConstantValues.ACTIVITY_PRESTART);
                        prestart.setCdTum(ConstantValues.TUM_STANDBY);
                        prestart.setCdType(MainApp.config("config.unit_type"));
                        prestart.setTime(Utilities.getCurrentUnix());
                        prestart.setLatitude(gps.getLat());
                        prestart.setLongitude(gps.getLon());
                        MainApp.operationStatusDB.insertIgnore(prestart);

                    } else {

                        OperationStatusLocal prestart = new OperationStatusLocal();
                        prestart.setIdEquipment(MainApp.equipmentLocal.getId());
                        prestart.setIdOperator(MainApp.user.getId());
                        prestart.setCdActivity(ConstantValues.ACTIVITY_PRESTART);
                        prestart.setCdTum(ConstantValues.TUM_STANDBY);
                        prestart.setCdType(MainApp.config("config.unit_type"));
                        prestart.setTime(Utilities.getCurrentUnix());
                        prestart.setLatitude(gps.getLat());
                        prestart.setLongitude(gps.getLon());

                        OperationStatusLocal saved = (OperationStatusLocal) MainApp.operationStatusDB
                                .insertIgnore(prestart);

                        Utilities.setLatestActivity(saved);

                    }

                    UserPresenterAsync.jika_berhasil(errorLabel, loading, kotakError, event, userDetail, user, false,
                            "Offline");

                }
            } else {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        MainApp.pauseEmit = false;

                        errorLabel.setText("Login gagal, periksa kembali NRP dan password anda");
                        loading.setVisible(false);
                        kotakError.setVisible(true);
                        buttonDoLogin.setDisable(false);
                    }
                });

            }

        } else {

            resetFields();
            errorLabel.setText("User field all required");
            kotakError.setVisible(true);
            loading.setVisible(false);
            buttonDoLogin.setDisable(false);

        }

    }

    @FXML
    private void closeAplikasi(ActionEvent event) throws Exception {
        // navigationUI.windowsNewKeyboard();
        ((Node) (event.getSource())).getScene().getWindow().hide();
        System.exit(0);
    }

    private void resetFields() {
        usernameField.setText("");
        passwordField.setText("");
    }

    private boolean validateInput() {

        String errorMessage = "";

        if (usernameField.getText() == null || passwordField.getText().length() == 0) {
            errorMessage += "Please enter credentials!\n";
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            errorLabel.setText(errorMessage);
            return false;
        }
    }

}
