package com.tura.fms.screen.models.operation_status;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "id_actual", "id_operation_support", "id_operation_loader", "id_equipment", "id_operator",
        "cd_activity", "cd_tum", "cd_type", "time", "latitude", "longitude" })
public class OperationStatusLocal {

    @JsonProperty("id")
    private String id;
    @JsonProperty("id_actual")
    private Object idActual;
    @JsonProperty("id_operation_support")
    private Object idOperationSupport;
    @JsonProperty("id_operation_loader")
    private Object idOperationLoader;
    @JsonProperty("id_equipment")
    private Object idEquipment;
    @JsonProperty("id_operator")
    private Object idOperator;
    @JsonProperty("cd_activity")
    private Object cdActivity;
    @JsonProperty("cd_tum")
    private Object cdTum;
    @JsonProperty("cd_type")
    private Object cdType;
    @JsonProperty("time")
    private Long time;
    @JsonProperty("latitude")
    private Double latitude;
    @JsonProperty("longitude")
    private Double longitude;
    @JsonIgnore
    private Map<Object, Object> additionalProperties = new HashMap<Object, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("id_actual")
    public Object getIdActual() {
        return idActual;
    }

    @JsonProperty("id_actual")
    public void setIdActual(Object idActual) {
        this.idActual = idActual;
    }

    @JsonProperty("id_operation_support")
    public Object getIdOperationSupport() {
        return idOperationSupport;
    }

    @JsonProperty("id_operation_support")
    public void setIdOperationSupport(Object idOperationSupport) {
        this.idOperationSupport = idOperationSupport;
    }

    @JsonProperty("id_operation_loader")
    public Object getIdOperationLoader() {
        return idOperationLoader;
    }

    @JsonProperty("id_operation_loader")
    public void setIdOperationLoader(Object idOperationLoader) {
        this.idOperationLoader = idOperationLoader;
    }

    @JsonProperty("id_equipment")
    public Object getIdEquipment() {
        return idEquipment;
    }

    @JsonProperty("id_equipment")
    public void setIdEquipment(Object idEquipment) {
        this.idEquipment = idEquipment;
    }

    @JsonProperty("id_operator")
    public Object getIdOperator() {
        return idOperator;
    }

    @JsonProperty("id_operator")
    public void setIdOperator(Object idOperator) {
        this.idOperator = idOperator;
    }

    @JsonProperty("cd_activity")
    public Object getCdActivity() {
        return cdActivity;
    }

    @JsonProperty("cd_activity")
    public void setCdActivity(Object cdActivity) {
        this.cdActivity = cdActivity;
    }

    @JsonProperty("cd_tum")
    public Object getCdTum() {
        return cdTum;
    }

    @JsonProperty("cd_tum")
    public void setCdTum(Object cdTum) {
        this.cdTum = cdTum;
    }

    @JsonProperty("cd_type")
    public Object getCdType() {
        return cdType;
    }

    @JsonProperty("cd_type")
    public void setCdType(Object cdType) {
        this.cdType = cdType;
    }

    @JsonProperty("time")
    public Long getTime() {
        return time;
    }

    @JsonProperty("time")
    public void setTime(Long time) {
        this.time = time;
    }

    @JsonProperty("latitude")
    public Double getLatitude() {
        return latitude;
    }

    @JsonProperty("latitude")
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @JsonProperty("longitude")
    public Double getLongitude() {
        return longitude;
    }

    @JsonProperty("longitude")
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @JsonAnyGetter
    public Map<Object, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(Object name, Object value) {
        this.additionalProperties.put(name, value);
    }

}