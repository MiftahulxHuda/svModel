package com.tura.fms.screen.models.actual_support;

import java.util.HashMap;
import java.util.Map;

public class CdActivitySupportLocal {

    private String cd;
    private String cdSelf;
    private String name;
    private Object json;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getCd() {
        return cd;
    }

    public void setCd(String cd) {
        this.cd = cd;
    }

    public String getCd_self() {
        return cdSelf;
    }

    public void setCd_self(String cdSelf) {
        this.cdSelf = cdSelf;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getJson() {
        return json;
    }

    public void setJson(Object json) {
        this.json = json;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}