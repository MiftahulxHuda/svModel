package com.tura.fms.screen.models.material;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.database.Operation;
import com.tura.fms.screen.database.SQLiteJDBC;
import com.tura.fms.screen.database.Synchronizer;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;

import retrofit2.Call;
import retrofit2.Response;

public class MaterialDB extends Operation {

    public static final String TABLE_NAME = "Material_Record";

    private static final String cd = "cd";
    private static final String cd_self = "cd_self";
    private static final String name = "name";
    private static final String density = "density";
    private static final String price = "price";
    private static final String is_salable = "is_salable";
    private static final String type = "type";
    private static final String sync_status = "sync_status";

    public MaterialDB() {
        super(MaterialDB.TABLE_NAME, ConstantValues.DATA_MATERIAL);
    }

    public void createTableMaterial() {
        try {
            DatabaseMetaData dbm = conn().getMetaData();
            ResultSet rs = dbm.getTables(null, null, TABLE_NAME, null);
            Statement stmt = null;
            if (rs.next()) {
                System.out.println("Table Material exists");
            } else {
                stmt = conn().createStatement();
                stmt.setQueryTimeout(10);
                stmt.executeUpdate(createTable());
                stmt.close();
                System.out.println("Table Material created");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String createTable() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + cd + " TEXT PRIMARY KEY," + cd_self + " TEXT NULL,"
                + name + " TEXT NULL," + density + " DOUBLE NULL," + price + " DOUBLE NULL," + is_salable
                + " TINYINT NULL," + type + " TEXT NULL," + sync_status + " NUMBER NULL" + ")";
    }

    @Override
    public Object insertIgnore(Object eqa) {
        Material eq = Synchronizer.mapper.convertValue(eqa, Material.class);
        PreparedStatement st = null;
        Boolean rs = null;
        try {
            String sql = "INSERT INTO " + TABLE_NAME + " " + "(" + cd + "," + cd_self + "," + name + "," + density + ","
                    + price + "," + is_salable + "," + type + "," + sync_status + ")"
                    + " VALUES(?,?,?,?,?,?,?,1) ON CONFLICT(cd) DO UPDATE SET " + cd + "=?, " + cd_self + "=?, " + name
                    + "=?, " + density + "=?, " + price + "=?, " + type + "=?;";
            st = conn().prepareStatement(sql);
            st.setString(1, eq.getCd());
            st.setString(2, "");
            st.setString(3, eq.getName());
            st.setDouble(4, eq.getDensity());
            st.setDouble(5, eq.getPrice());
            st.setInt(6, 0);
            st.setString(7, eq.getType());
            st.setString(8, eq.getCd());
            st.setString(9, "");
            st.setString(10, eq.getName());
            st.setDouble(11, eq.getDensity());
            st.setDouble(12, eq.getPrice());
            st.setString(13, eq.getType());
            rs = st.execute();
            st.close();

            return eq;

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (rs && st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
        return null;
    }

    @Override
    public void deleteData(String id) {
        String sql = "delete from " + TABLE_NAME + " where id=" + id;
        try {
            Statement stmt = conn().createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object getFromServer(Integer page, Integer size) {
        if (!NetworkConnection.getServiceStatus()) {
            return null;
        }
        Call<Object> call = APIClient.getInstance().getMaterial(MainApp.requestHeader, page, size);
        try {
            Response<Object> response = call.execute();
            return response.body();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Object getFromServerById(String id) {
        if (!NetworkConnection.getServiceStatus()) {
            return null;
        }
        Call<Object> call = APIClient.getInstance().getMaterialById(MainApp.requestHeader, id);
        try {
            Response<Object> response = call.execute();
            return response.body();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Integer getRemoteCount() {
        if (!NetworkConnection.getServiceStatus()) {
            return 0;
        }
        Call<Object> call = APIClient.getInstance().getMaterialCount(MainApp.requestHeader);
        try {
            Response<Object> response = call.execute();
            Map<String, Number> mapped = APIClient.mapper.convertValue(response.body(), Map.class);
            if (mapped.get("output") != null) {
                return mapped.get("output").intValue();
            } else {
                return 0;
            }
        } catch (IOException ex) {
            // ex.printStackTrace();
            System.out.println("[Material getRemoteCount] " + ex.getMessage());
        }
        return 0;
    }

    @Override
    public List<Object> getFlaggedData() {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE sync_status=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, "0");
            ResultSet cursor = preparedStatement.executeQuery();
            List<Object> result = new ArrayList<Object>();
            while (cursor.next()) {
                MaterialLocal eq = new MaterialLocal();
                eq.setCd(cursor.getString("cd"));
                eq.setName(cursor.getString("name"));
                eq.setType(cursor.getString("type"));
                eq.setDensity(cursor.getDouble("density"));
                eq.setPrice(cursor.getDouble("price"));
                result.add(eq);
            }
            cursor.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void postBulkData(List<Object> eq) {
        if (!NetworkConnection.getServiceStatus()) {
            return;
        }
        // no post action
    }

    @Override
    public void setSynchronized(String id) {
        String sql = "update " + TABLE_NAME + " set sync_status=1 where id='" + id + "'";
        try {
            Statement stmt = conn().createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
