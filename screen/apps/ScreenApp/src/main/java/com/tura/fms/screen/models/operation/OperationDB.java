package com.tura.fms.screen.models.operation;

import java.io.IOException;
import java.net.ConnectException;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.database.Operation;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.ResponseModel;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OperationDB extends Operation {

    public static final String TABLE_NAME = "Operation_Record";

    private static final String id = "id";
    private static final String id_equipment = "id_equipment";
    private static final String id_operator = "id_operator";
    private static final String id_actual = "id_actual";
    private static final String material_type = "material_type";
    private static final String shift = "shift";
    private static final String date = "date";
    private static final String id_hauler_operator = "id_hauler_operator";
    private static final String id_material = "id_material";
    private static final String id_hauler = "id_hauler";
    private static final String id_loader_operator = "id_loader_operator";
    private static final String id_loader = "id_loader";
    private static final String id_location = "id_location";
    private static final String id_destination = "id_destination";
    private static final String date_operated = "date_operated";
    private static final String load_weight = "load_weight";
    private static final String empty_weight = "empty_weight";
    private static final String bucket_count = "bucket_count";
    private static final String tonnage = "tonnage";
    private static final String tonnage_ujipetik = "tonnage_ujipetik";
    private static final String tonnage_timbangan = "tonnage_timbangan";
    private static final String tonnage_bucket = "tonnage_bucket";
    private static final String distance = "distance";
    private static final String latitude = "latitude";
    private static final String longitude = "longitude";
    private static final String is_volume = "is_volume";
    private static final String is_supervised = "is_supervised";
    private static final String sync_status = "sync_status";

    public OperationDB() {
        super(OperationDB.TABLE_NAME, ConstantValues.DATA_OPERATION);
    }

    public void createTableOperation() {
        try {
            DatabaseMetaData dbm = conn().getMetaData();
            ResultSet rs = dbm.getTables(null, null, TABLE_NAME, null);
            Statement stmt = null;
            if (rs.next()) {
                System.out.println("Table operation exists");
            } else {
                stmt = conn().createStatement();
                stmt.setQueryTimeout(10);
                stmt.executeUpdate(createTable());
                stmt.close();
                System.out.println("Table operation created");
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String createTable() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + id + " TEXT PRIMARY KEY," + id_equipment
                + " TEXT NULL," + id_operator + " TEXT NULL," + id_actual + " TEXT NULL," + material_type
                + " TEXT NULL," + shift + " TEXT NULL," + date + " NUMBER NULL," + id_hauler_operator + " TEXT NULL,"
                + id_material + " TEXT NULL," + id_hauler + " TEXT NULL," + id_loader_operator + " TEXT NULL,"
                + id_loader + " TEXT NULL," + id_location + " TEXT NULL," + id_destination + " TEXT NULL,"
                + date_operated + " NUMBER NULL," + load_weight + " DOUBLE NULL," + empty_weight + " DOUBLE NULL,"
                + bucket_count + " DOUBLE NULL," + tonnage + " DOUBLE NULL," + tonnage_ujipetik + " DOUBLE NULL,"
                + tonnage_timbangan + " DOUBLE NULL," + tonnage_bucket + " DOUBLE NULL," + distance + " DOUBLE NULL,"
                + latitude + " DOUBLE NULL," + longitude + " DOUBLE NULL," + is_volume + " NUMBER NULL," + is_supervised
                + " NUMBER NULL," + sync_status + " NUMBER NULL" + ")";
    }

    @Override
    public Object insertIgnore(Object eqa) {
        OperationLocal eq = (OperationLocal) eqa;
        PreparedStatement st = null;
        Boolean rs = null;
        try {
            String sql = "INSERT INTO " + TABLE_NAME + " " + "(" + id + "," + id_equipment + "," + id_operator + ","
                    + id_actual + "," + material_type + "," + shift + "," + date + "," + id_hauler_operator + ","
                    + id_material + "," + id_hauler + "," + id_loader_operator + "," + id_loader + "," + id_location
                    + "," + id_destination + "," + date_operated + "," + load_weight + "," + empty_weight + ","
                    + bucket_count + "," + tonnage + "," + tonnage_ujipetik + "," + tonnage_timbangan + ","
                    + tonnage_bucket + "," + distance + "," + latitude + "," + longitude + "," + is_volume + ","
                    + is_supervised + "," + sync_status + ")"
                    + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,0) ON CONFLICT(" + id
                    + ") DO UPDATE SET " + id + "=?," + id_equipment + "=?," + id_operator + "=?," + id_actual + "=?,"
                    + material_type + "=?," + shift + "=?," + date + "=?," + id_hauler_operator + "=?," + id_material
                    + "=?," + id_hauler + "=?," + id_loader_operator + "=?," + id_loader + "=?," + id_location + "=?,"
                    + id_destination + "=?," + date_operated + "=?," + load_weight + "=?," + empty_weight + "=?,"
                    + bucket_count + "=?," + tonnage + "=?," + tonnage_ujipetik + "=?," + tonnage_timbangan + "=?,"
                    + tonnage_bucket + "=?," + distance + "=?," + latitude + "=?," + longitude + "=?," + is_volume
                    + "=?," + is_supervised + "=?;";

            st = conn().prepareStatement(sql);
            final String uuid = UUID.randomUUID().toString().replace("-", "");
            // st.setString(1, eq.getId());
            st.setString(1, uuid);
            st.setString(28, uuid);

            st.setString(2, eq.getIdEquipment());
            st.setString(29, eq.getIdEquipment());

            st.setString(3, eq.getIdOperator());
            st.setString(30, eq.getIdOperator());

            st.setString(4, eq.getIdActual());
            st.setString(31, eq.getIdActual());

            st.setString(5, eq.getMaterialType());
            st.setString(32, eq.getMaterialType());

            st.setString(6, eq.getShift());
            st.setString(33, eq.getShift());

            st.setString(7, eq.getDate());
            st.setString(34, eq.getDate());

            st.setString(8, eq.getIdHaulerOperator());
            st.setString(35, eq.getIdHaulerOperator());

            st.setString(9, eq.getIdMaterial());
            st.setString(36, eq.getIdMaterial());

            st.setString(10, eq.getIdHauler());
            st.setString(37, eq.getIdHauler());

            st.setString(11, eq.getIdLoaderOperator());
            st.setString(38, eq.getIdLoaderOperator());

            st.setString(12, eq.getIdLoader());
            st.setString(39, eq.getIdLoader());

            st.setString(13, eq.getIdLocation());
            st.setString(40, eq.getIdLocation());

            st.setString(14, eq.getIdDestination());
            st.setString(41, eq.getIdDestination());

            st.setString(15, eq.getDateOperated());
            st.setString(42, eq.getDateOperated());

            st.setDouble(16, eq.getLoadWeight());
            st.setDouble(43, eq.getLoadWeight());

            st.setDouble(17, eq.getEmptyWeight());
            st.setDouble(44, eq.getEmptyWeight());

            st.setDouble(18, eq.getBucketCount());
            st.setDouble(45, eq.getBucketCount());

            st.setDouble(19, eq.getTonnage());
            st.setDouble(46, eq.getTonnage());

            st.setDouble(20, eq.getTonnageUjipetik());
            st.setDouble(47, eq.getTonnageUjipetik());

            st.setDouble(21, eq.getTonnageTimbangan());
            st.setDouble(48, eq.getTonnageTimbangan());

            st.setDouble(22, eq.getTonnageBucket());
            st.setDouble(49, eq.getTonnageBucket());

            st.setDouble(23, eq.getDistance());
            st.setDouble(50, eq.getDistance());

            st.setDouble(24, eq.getLat());
            st.setDouble(51, eq.getLat());

            st.setDouble(25, eq.getLon());
            st.setDouble(52, eq.getLon());

            st.setDouble(26, eq.getIsVolume());
            st.setDouble(53, eq.getIsVolume());

            st.setInt(27, eq.getIsSupervised());
            st.setInt(54, eq.getIsSupervised());

            rs = st.execute();
            st.close();

            return this.getLocalById(id, uuid);

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (rs && st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    public void deleteData(String id) {
        String sql = "delete from " + TABLE_NAME + " where id=?";
        try {
            PreparedStatement ps = conn().prepareStatement(sql);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object getFromServer(Integer page, Integer size) {
        if (!NetworkConnection.getServiceStatus()) {
            return null;
        }
        // no action
        return null;
    }

    @Override
    public Integer getRemoteCount() {
        if (!NetworkConnection.getServiceStatus()) {
            return 0;
        }
        // no action
        return 0;
    }

    @Override
    public List<Object> getFlaggedData() {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE sync_status=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, "0");
            ResultSet cursor = preparedStatement.executeQuery();
            List<Object> result = new ArrayList<Object>();
            while (cursor.next()) {
                OperationLocal eq = new OperationLocal();
                eq.setId(cursor.getString(id));
                eq.setIdEquipment(cursor.getString(id_equipment));
                eq.setIdOperator(cursor.getString(id_operator));
                eq.setIdActual(cursor.getString(id_actual));
                eq.setMaterialType(cursor.getString(material_type));
                eq.setShift(cursor.getString(shift));
                eq.setDate(cursor.getString(date));
                eq.setIdHaulerOperator(cursor.getString(id_hauler_operator));
                eq.setIdMaterial(cursor.getString(id_material));
                eq.setIdHauler(cursor.getString(id_hauler));
                eq.setIdLoaderOperator(cursor.getString(id_loader_operator));
                eq.setIdLoader(cursor.getString(id_loader));
                eq.setIdLocation(cursor.getString(id_location));
                eq.setIdDestination(cursor.getString(id_destination));
                eq.setDateOperated(cursor.getString(date_operated));
                eq.setLoadWeight(cursor.getDouble(load_weight));
                eq.setEmptyWeight(cursor.getDouble(empty_weight));
                eq.setBucketCount(cursor.getInt(bucket_count));
                eq.setTonnage(cursor.getDouble(tonnage));
                eq.setTonnageUjipetik(cursor.getDouble(tonnage_ujipetik));
                eq.setTonnageTimbangan(cursor.getDouble(tonnage_timbangan));
                eq.setTonnageBucket(cursor.getDouble(tonnage_bucket));
                eq.setDistance(cursor.getDouble(distance));
                eq.setLat(cursor.getDouble(latitude));
                eq.setLon(cursor.getDouble(longitude));
                eq.setIsVolume(cursor.getInt(is_volume));
                eq.setIsSupervised(cursor.getInt(is_supervised));
                result.add(eq);
            }
            cursor.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Object getFlaggedDataOne() {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE sync_status=? AND " + id_equipment + "=? LIMIT 1";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, "0");
            String eqId = MainApp.equipmentLocal != null ? MainApp.equipmentLocal.getId() : "";
            preparedStatement.setString(2, eqId);
            ResultSet cursor = preparedStatement.executeQuery();

            if (cursor.next()) {
                OperationLocal eq = new OperationLocal();
                eq.setId(cursor.getString(id));
                eq.setIdEquipment(cursor.getString(id_equipment));
                eq.setIdOperator(cursor.getString(id_operator));
                eq.setIdActual(cursor.getString(id_actual));
                eq.setMaterialType(cursor.getString(material_type));
                eq.setShift(cursor.getString(shift));
                eq.setDate(cursor.getString(date));
                eq.setIdHaulerOperator(cursor.getString(id_hauler_operator));
                eq.setIdMaterial(cursor.getString(id_material));
                eq.setIdHauler(cursor.getString(id_hauler));
                eq.setIdLoaderOperator(cursor.getString(id_loader_operator));
                eq.setIdLoader(cursor.getString(id_loader));
                eq.setIdLocation(cursor.getString(id_location));
                eq.setIdDestination(cursor.getString(id_destination));
                eq.setDateOperated(cursor.getString(date_operated));
                eq.setLoadWeight(cursor.getDouble(load_weight));
                eq.setEmptyWeight(cursor.getDouble(empty_weight));
                eq.setBucketCount(cursor.getInt(bucket_count));
                eq.setTonnage(cursor.getDouble(tonnage));
                eq.setTonnageUjipetik(cursor.getDouble(tonnage_ujipetik));
                eq.setTonnageTimbangan(cursor.getDouble(tonnage_timbangan));
                eq.setTonnageBucket(cursor.getDouble(tonnage_bucket));
                eq.setDistance(cursor.getDouble(distance));
                eq.setLat(cursor.getDouble(latitude));
                eq.setLon(cursor.getDouble(longitude));
                eq.setIsVolume(cursor.getInt(is_volume));
                eq.setIsSupervised(cursor.getInt(is_supervised));

                cursor.close();
                return eq;

            } else {

                cursor.close();
                return null;
            }

        } catch (Exception e) {

            System.out.println(e.getMessage());
            return null;

        }
    }

    @Override
    public void postBulkData(List<Object> eq) {
        if (!NetworkConnection.getServiceStatus()) {
            return;
        }
        System.out.println("--- post synchronization ---");
        Call<ResponseModel> call = APIClient.getInstance().postOperation(MainApp.requestHeader, eq);
        try {
            Response<ResponseModel> response = call.execute();
            System.out.println(response);
            System.out.println(response.body().getResponse());
            System.out.println(response.body().getMessage());
            if (response.body().getResponse().equals("success")) {
                System.out.println("post success");
                /**
                 * change flag (sync_status to 1)
                 */
                for (Object obj : eq) {
                    OperationLocal oqe = (OperationLocal) obj;
                    setSynchronized(oqe.getId());
                }
            }
        } catch (IOException ex) {

            ex.printStackTrace();
        }
    }

    @Override
    public void postData(Object eq, Runnable cb) {
        if (!NetworkConnection.getServiceStatus()) {
            return;
        }

        Call<ResponseModel> call = APIClient.getInstance().postOfflineOperation(MainApp.requestHeader, eq);
        try {
            Response<ResponseModel> response = call.execute();
            if (response.body() != null) {
                if (response.body().getResponse() != null) {
                    if (response.body().getResponse().equals("success")) {
                        System.out.println("post success");

                        if (cb != null) {
                            cb.run();
                        }
                    }
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void setSynchronized(String id) {
        String sql = "update " + TABLE_NAME + " set sync_status=1 where id=?";
        // deleteData(id);
        try {
            PreparedStatement ps = conn().prepareStatement(sql);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void postSingleData(Object eq, Runnable cbSuccess, Runnable cbFail) {
        if (!NetworkConnection.getServiceStatus()) {
            return;
        }
        System.out.println("--- post synchronization ---");
        Call<ResponseModel> call = APIClient.getInstance().postOperation(MainApp.requestHeader, eq);
        call.enqueue(new Callback<ResponseModel>() {

            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                System.out.println("--- ritase log");
                System.out.println(response.body().getOutput());
                if (response.isSuccessful()) {
                    MainApp.latestCycle = response.body().getOutput();
                    // Map<String, Object> map = Utilities.mapObject(MainApp.latestCycle);
                    // if (map != null) {
                    // if (map.get("tonnage") != null) {
                    // Double tonnage = Double.parseDouble(map.get("tonnage").toString());
                    // MainApp.dashboardInfo.setTonnagePayload(tonnage);
                    // }
                    // if (map.get("tonnage_ujipetik") != null) {
                    // Double tonnage = Double.parseDouble(map.get("tonnage_ujipetik").toString());
                    // MainApp.dashboardInfo.setTonnageUjiPetik(tonnage);
                    // }
                    // if (map.get("tonnage_timbangan") != null) {
                    // Double tonnage = Double.parseDouble(map.get("tonnage_timbangan").toString());
                    // MainApp.dashboardInfo.setTonnageTimbangan(tonnage);
                    // }
                    // if (map.get("tonnage_bucket") != null) {
                    // Double tonnage = Double.parseDouble(map.get("tonnage_bucket").toString());
                    // MainApp.dashboardInfo.setTonnageBucket(tonnage);
                    // }
                    // }

                    if (cbSuccess != null) {
                        cbSuccess.run();
                    }
                }

                MainApp.tonnage = 0.0;
                MainApp.tonnageTimbangan = 0.0;
                // MainApp.tonnageUjiPetik = 0.0;
                MainApp.tonnageBucket = 0.0;

                MainApp.dashboardInfo.setTonnagePayload(0.0);
                // MainApp.dashboardInfo.setTonnageUjiPetik(0.0);
                MainApp.dashboardInfo.setTonnageTimbangan(0.0);
                MainApp.dashboardInfo.setTonnageBucket(0.0);
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                // t.printStackTrace();

                OperationLocal opLocal = (OperationLocal) eq;
                opLocal.setDate(Utilities.getCurrentUnix().toString());
                opLocal.setDateOperated(Utilities.getCurrentUnix().toString());

                MainApp.operationDB.insertIgnore(eq);

                MainApp.tonnage = 0.0;
                MainApp.tonnageTimbangan = 0.0;
                // MainApp.tonnageUjiPetik = 0.0;
                MainApp.tonnageBucket = 0.0;

                MainApp.dashboardInfo.setTonnagePayload(0.0);
                // MainApp.dashboardInfo.setTonnageUjiPetik(0.0);
                MainApp.dashboardInfo.setTonnageTimbangan(0.0);
                MainApp.dashboardInfo.setTonnageBucket(0.0);

                if (t instanceof ConnectException) {
                    Utilities.messageSocketException(t, "postRitase");
                } else if (t instanceof TimeoutException) {
                    Utilities.messageTimeoutException(t, "postRitase");
                } else {
                    Utilities.messageOtherException(t, "postRitase");
                }

                if (cbFail != null) {
                    cbFail.run();
                }
            }
        });
    }

    public void postSingleData(Object eq) {
        postSingleData(eq, null, null);
    }

}
