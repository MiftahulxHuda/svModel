package com.tura.fms.screen.models.maintenance_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LostStartPost {

    @SerializedName("id_actual")
    @Expose
    String id_actual;

    @SerializedName("id_operation_support")
    @Expose
    String id_operation_support;

    @SerializedName("id_equipment")
    @Expose
    String id_equipment;

    @SerializedName("id_operator")
    @Expose
    String id_operator;

    @SerializedName("cd_activity")
    @Expose
    String cd_activity;

    @SerializedName("time")
    @Expose
    String time;

    public String getId_actual() {
        return id_actual;
    }

    public void setId_actual(String id_actual) {
        this.id_actual = id_actual;
    }

    public String getId_operation_support() {
        return id_operation_support;
    }

    public void setId_operation_support(String id_operation_support) {
        this.id_operation_support = id_operation_support;
    }

    public String getId_equipment() {
        return id_equipment;
    }

    public void setId_equipment(String id_equipment) {
        this.id_equipment = id_equipment;
    }

    public String getId_operator() {
        return id_operator;
    }

    public void setId_operator(String id_operator) {
        this.id_operator = id_operator;
    }

    public String getCd_activity() {
        return cd_activity;
    }

    public void setCd_activity(String cd_activity) {
        this.cd_activity = cd_activity;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
