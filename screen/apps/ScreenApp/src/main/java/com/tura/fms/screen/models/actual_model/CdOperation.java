package com.tura.fms.screen.models.actual_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CdOperation {

    @SerializedName("cd")
    @Expose
    String cd;

    @SerializedName("name")
    @Expose
    String name;

    public String getCd() {
        return cd;
    }

    public void setCd(String cd) {
        this.cd = cd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
