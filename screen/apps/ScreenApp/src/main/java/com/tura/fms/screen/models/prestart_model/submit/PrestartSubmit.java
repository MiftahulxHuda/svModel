package com.tura.fms.screen.models.prestart_model.submit;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "id_actual", "id_operation_support", "id_operation_loader", "time", "id_equipment",
        "id_operator", "id_prestart", "value", "qty" })
public class PrestartSubmit {

    @JsonProperty("id")
    private String id;
    @JsonProperty("id_actual")
    private Object idActual;
    @JsonProperty("id_operation_support")
    private Object idOperationSupport;
    @JsonProperty("id_operation_loader")
    private Object idOperationLoader;
    @JsonProperty("time")
    private Long time;
    @JsonProperty("id_equipment")
    private String idEquipment;
    @JsonProperty("id_operator")
    private String idOperator;
    @JsonProperty("id_prestart")
    private Object idPrestart;
    @JsonProperty("value")
    private Object value;
    @JsonProperty("qty")
    private Integer qty;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("id_actual")
    public Object getIdActual() {
        return idActual;
    }

    @JsonProperty("id_actual")
    public void setIdActual(Object idActual) {
        this.idActual = idActual;
    }

    @JsonProperty("id_operation_support")
    public Object getIdOperationSupport() {
        return idOperationSupport;
    }

    @JsonProperty("id_operation_support")
    public void setIdOperationSupport(Object idOperationSupport) {
        this.idOperationSupport = idOperationSupport;
    }

    @JsonProperty("id_operation_loader")
    public Object getIdOperationLoader() {
        return idOperationLoader;
    }

    @JsonProperty("id_operation_loader")
    public void setIdOperationLoader(Object idOperationLoader) {
        this.idOperationLoader = idOperationLoader;
    }

    @JsonProperty("time")
    public Long getTime() {
        return time;
    }

    @JsonProperty("time")
    public void setTime(Long time) {
        this.time = time;
    }

    @JsonProperty("id_equipment")
    public String getIdEquipment() {
        return idEquipment;
    }

    @JsonProperty("id_equipment")
    public void setIdEquipment(String idEquipment) {
        this.idEquipment = idEquipment;
    }

    @JsonProperty("id_operator")
    public String getIdOperator() {
        return idOperator;
    }

    @JsonProperty("id_operator")
    public void setIdOperator(String idOperator) {
        this.idOperator = idOperator;
    }

    @JsonProperty("id_prestart")
    public Object getIdPrestart() {
        return idPrestart;
    }

    @JsonProperty("id_prestart")
    public void setIdPrestart(Object idPrestart) {
        this.idPrestart = idPrestart;
    }

    @JsonProperty("value")
    public Object getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(Object value) {
        this.value = value;
    }

    @JsonProperty("qty")
    public Integer getQty() {
        return qty;
    }

    @JsonProperty("qty")
    public void setQty(Integer qty) {
        this.qty = qty;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}