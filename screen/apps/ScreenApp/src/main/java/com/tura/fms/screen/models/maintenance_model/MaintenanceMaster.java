package com.tura.fms.screen.models.maintenance_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MaintenanceMaster {

    @SerializedName("id")
    @Expose
    String id;

    @SerializedName("type")
    @Expose
    String type;

    @SerializedName("maintenance_group_id")
    @Expose
    String maintenance_group_id;

    @SerializedName("equipment_id")
    @Expose
    String equipment_id;

    @SerializedName("problem_id")
    @Expose
    String problem_id;

    @SerializedName("maintenance_request_id")
    @Expose
    String maintenance_request_id;

    @SerializedName("created_at")
    @Expose
    String created_at;

    @SerializedName("scheduled_at")
    @Expose
    String scheduled_at;

    @SerializedName("hm")
    @Expose
    String hm;

    @SerializedName("status")
    @Expose
    String status;

    @SerializedName("model_unit")
    @Expose
    String model_unit;

    @SerializedName("wo_number")
    @Expose
    String wo_number;

    @SerializedName("description")
    @Expose
    String description;

    @SerializedName("foreman_id")
    @Expose
    String foreman_id;

    @SerializedName("completed_at")
    @Expose
    String completed_at;

    @SerializedName("is_deleted")
    @Expose
    String is_deleted;

    @SerializedName("location_id")
    @Expose
    String location_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMaintenance_group_id() {
        return maintenance_group_id;
    }

    public void setMaintenance_group_id(String maintenance_group_id) {
        this.maintenance_group_id = maintenance_group_id;
    }

    public String getEquipment_id() {
        return equipment_id;
    }

    public void setEquipment_id(String equipment_id) {
        this.equipment_id = equipment_id;
    }

    public String getProblem_id() {
        return problem_id;
    }

    public void setProblem_id(String problem_id) {
        this.problem_id = problem_id;
    }

    public String getMaintenance_request_id() {
        return maintenance_request_id;
    }

    public void setMaintenance_request_id(String maintenance_request_id) {
        this.maintenance_request_id = maintenance_request_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getScheduled_at() {
        return scheduled_at;
    }

    public void setScheduled_at(String scheduled_at) {
        this.scheduled_at = scheduled_at;
    }

    public String getHM() {
        return hm;
    }

    public void setHM(String hm) {
        this.hm = hm;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getModel_unit() {
        return model_unit;
    }

    public void setModel_unit(String model_unit) {
        this.model_unit = model_unit;
    }

    public String getWO_number() {
        return wo_number;
    }

    public void setWO_number(String wo_number) {
        this.wo_number = wo_number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getForeman_id() {
        return foreman_id;
    }

    public void setForeman_id(String foreman_id) {
        this.foreman_id = foreman_id;
    }

    public String getCompleted_at() {
        return completed_at;
    }

    public void setCompleted_at(String completed_at) {
        this.completed_at = completed_at;
    }

    public String getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(String is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }
}
