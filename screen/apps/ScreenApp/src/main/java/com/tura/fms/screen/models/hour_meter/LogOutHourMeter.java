package com.tura.fms.screen.models.hour_meter;

import java.util.HashMap;
import java.util.Map;

public class LogOutHourMeter {

    private String id_actual;
    private Integer hm;

    public Integer getHm() {
        return hm;
    }

    public void setHm(Integer hm) {
        this.hm = hm;
    }

    public String getIdActual() {
        return id_actual;
    }

    public void setIdActual(String ida) {
        this.id_actual = ida;
    }
}
