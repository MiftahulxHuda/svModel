package com.tura.fms.screen.models.dashboard_model;

public class DashboardDetail {

    String Status;
    String StatusText;
    String Date;
    String Shift;
    Double Cycle;
    Double Distance;
    String Operation;
    Double Engine;
    Double Fuel;
    Double Temperatur;
    String LoadingPoint;
    String DumpingPoint;
    String UnitId;
    String UnitType;
    Double HourMeter;
    Double Payload;
    Double tonnagePayload;
    Double tonnageUjiPetik;
    Double tonnageTimbangan;
    Double tonnageBucket;
    Double Speed;

    public DashboardDetail() {
        Status = "";
        Date = "";
        Shift = "";
        Cycle = 0.0;
        Distance = 0.0;
        Operation = "";
        Engine = 0.0;
        Fuel = 0.0;
        Temperatur = 0.0;
        LoadingPoint = "";
        DumpingPoint = "";
        UnitId = "";
        UnitType = "";
        HourMeter = 0.0;
        Payload = 0.0;
        Speed = 0.0;
        tonnagePayload = 0.0;
        tonnageUjiPetik = 0.0;
        tonnageTimbangan = 0.0;
        tonnageBucket = 0.0;
    }

    public Double getTonnagePayload() {
        return tonnagePayload;
    }

    public void setTonnagePayload(Double num) {
        tonnagePayload = num;
    }

    public Double getTonnageUjiPetik() {
        return tonnageUjiPetik;
    }

    public void setTonnageUjiPetik(Double num) {
        tonnageUjiPetik = num;
    }

    public Double getTonnageTimbangan() {
        return tonnageTimbangan;
    }

    public void setTonnageTimbangan(Double num) {
        tonnageTimbangan = num;
    }

    public Double getTonnageBucket() {
        return tonnageBucket;
    }

    public void setTonnageBucket(Double num) {
        tonnageBucket = num;
    }

    public Double getSpeed() {
        return Speed;
    }

    public void setSpeed(Double speed) {
        Speed = speed;
    }

    public void addSpeed(Double speed) {
        Speed += speed;
    }

    public Double getHourMeter() {
        return HourMeter;
    }

    public void setHourMeter(Double hourMeter) {
        HourMeter = hourMeter;
    }

    public void addHourMeter(Double hourMeter) {
        HourMeter += hourMeter;
    }

    public Double getPayload() {
        return Payload;
    }

    public void setPayload(Double payload) {
        Payload = payload;
    }

    public void addPayload(Double payload) {
        Payload += payload;
    }

    public String getLoadingPoint() {
        return LoadingPoint;
    }

    public void setLoadingPoint(String loadingPoint) {
        LoadingPoint = loadingPoint;
    }

    public String getDumpingPoint() {
        return DumpingPoint;
    }

    public void setDumpingPoint(String dumpingPoint) {
        DumpingPoint = dumpingPoint;
    }

    public String getUnitId() {
        return UnitId;
    }

    public void setUnitId(String unitId) {
        UnitId = unitId;
    }

    public String getUnitType() {
        return UnitType;
    }

    public void setUnitType(String unitType) {
        UnitType = unitType;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getStatusText() {
        return StatusText;
    }

    public void setStatusText(String status) {
        StatusText = status;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getShift() {
        return Shift;
    }

    public void setShift(String shift) {
        Shift = shift;
    }

    public Double getCycle() {
        return Cycle;
    }

    public void setCycle(Double cycle) {
        Cycle = cycle;
    }

    public void addCycle(Double cycle) {
        Cycle += cycle;
    }

    public Double getDistance() {
        return Distance;
    }

    public void setDistance(Double distance) {
        Distance = distance;
    }

    public void addDistance(Double distance) {
        Distance += distance;
    }

    public String getOperation() {
        return Operation;
    }

    public void setOperation(String operation) {
        Operation = operation;
    }

    public Double getEngine() {
        return Engine;
    }

    public void setEngine(Double engine) {
        Engine = engine;
    }

    public void addEngine(Double engine) {
        Engine += engine;
    }

    public Double getFuel() {
        return Fuel;
    }

    public void setFuel(Double fuel) {
        Fuel = fuel;
    }

    public void addFuel(Double fuel) {
        Fuel += fuel;
    }

    public Double getTemperatur() {
        return Temperatur;
    }

    public void setTemperatur(Double temperatur) {
        Temperatur = temperatur;
    }

    public void addTemperatur(Double temperatur) {
        Temperatur += temperatur;
    }
}
