package com.tura.fms.screen.models.actual_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.tura.fms.screen.models.equipment_category.EquipmentCategory;

public class Loader {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("__id_org__")
    @Expose
    private String id_org;

    @SerializedName("__has_id_org__")
    @Expose
    private Boolean has_id_org;

    @SerializedName("__id_operator__")
    @Expose
    private String id_operator;

    @SerializedName("__has_id_operator__")
    @Expose
    private Boolean has_id_operator;

    @SerializedName("__cd_operation__")
    @Expose
    private String cd_operation;

    @SerializedName("__has_cd_operation__")
    @Expose
    private Boolean has_cd_operation;

    @SerializedName("__cd_equipment__")
    @Expose
    private String cd_equipment;

    @SerializedName("__has_cd_equipment__")
    @Expose
    private Boolean has_cd_equipment;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("capacity_real")
    @Expose
    private int capacity_real;

    @SerializedName("capacity_labs")
    @Expose
    private int capacity_labs;

    @SerializedName("enum_availability")
    @Expose
    private int enum_availability;

    @SerializedName("is_deleted")
    @Expose
    private int is_deleted;

    @SerializedName("date_submitted")
    @Expose
    private long date_submitted;

    @SerializedName("date_modified")
    @Expose
    private long date_modified;

    @SerializedName("load_weight")
    @Expose
    private int load_weight;

    @SerializedName("empty_weight")
    @Expose
    private long empty_weight;

    @SerializedName("capacity_labs_2")
    @Expose
    private long capacity_labs_2;

    @SerializedName("capacity_labs_3")
    @Expose
    private long capacity_labs_3;

    @SerializedName("hour_meter")
    @Expose
    private long hour_meter;

    @SerializedName("class")
    @Expose
    private String kelas;

    @SerializedName("id_equipment_category")
    @Expose
    private EquipmentCategory id_equipment_category;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_org() {
        return id_org;
    }

    public void setId_org(String id_org) {
        this.id_org = id_org;
    }

    public Boolean getHas_id_org() {
        return has_id_org;
    }

    public void setHas_id_org(Boolean has_id_org) {
        this.has_id_org = has_id_org;
    }

    public String getId_operator() {
        return id_operator;
    }

    public void setId_operator(String id_operator) {
        this.id_operator = id_operator;
    }

    public Boolean getHas_id_operator() {
        return has_id_operator;
    }

    public void setHas_id_operator(Boolean has_id_operator) {
        this.has_id_operator = has_id_operator;
    }

    public String getCd_operation() {
        return cd_operation;
    }

    public void setCd_operation(String cd_operation) {
        this.cd_operation = cd_operation;
    }

    public Boolean getHas_cd_operation() {
        return has_cd_operation;
    }

    public void setHas_cd_operation(Boolean has_cd_operation) {
        this.has_cd_operation = has_cd_operation;
    }

    public String getCd_equipment() {
        return cd_equipment;
    }

    public void setCd_equipment(String cd_equipment) {
        this.cd_equipment = cd_equipment;
    }

    public Boolean getHas_cd_equipment() {
        return has_cd_equipment;
    }

    public void setHas_cd_equipment(Boolean has_cd_equipment) {
        this.has_cd_equipment = has_cd_equipment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCapacity_real() {
        return capacity_real;
    }

    public void setCapacity_real(int capacity_real) {
        this.capacity_real = capacity_real;
    }

    public int getCapacity_labs() {
        return capacity_labs;
    }

    public void setCapacity_labs(int capacity_labs) {
        this.capacity_labs = capacity_labs;
    }

    public int getEnum_availability() {
        return enum_availability;
    }

    public void setEnum_availability(int enum_availability) {
        this.enum_availability = enum_availability;
    }

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }

    public long getDate_submitted() {
        return date_submitted;
    }

    public void setDate_submitted(long date_submitted) {
        this.date_submitted = date_submitted;
    }

    public long getDate_modified() {
        return date_modified;
    }

    public void setDate_modified(long date_modified) {
        this.date_modified = date_modified;
    }

    public int getLoad_weight() {
        return load_weight;
    }

    public void setLoad_weight(int load_weight) {
        this.load_weight = load_weight;
    }

    public long getEmpty_weight() {
        return empty_weight;
    }

    public void setEmpty_weight(long empty_weight) {
        this.empty_weight = empty_weight;
    }

    public long getCapacity_labs_2() {
        return capacity_labs_2;
    }

    public void setCapacity_labs_2(long capacity_labs_2) {
        this.capacity_labs_2 = capacity_labs_2;
    }

    public long getCapacity_labs_3() {
        return capacity_labs_3;
    }

    public void setCapacity_labs_3(long capacity_labs_3) {
        this.capacity_labs_3 = capacity_labs_3;
    }

    public long getHour_meter() {
        return hour_meter;
    }

    public void setHour_meter(long hour_meter) {
        this.hour_meter = hour_meter;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public EquipmentCategory getEquipmentCategory() {
        return id_equipment_category;
    }

    public void setEquipmentCategory(EquipmentCategory eq) {
        this.id_equipment_category = eq;
    }

}
