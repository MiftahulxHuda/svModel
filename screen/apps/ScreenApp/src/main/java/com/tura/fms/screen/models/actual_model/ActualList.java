package com.tura.fms.screen.models.actual_model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ActualList {

    @SerializedName("output")
    private List<ActualDetail> output;

    public List<ActualDetail> getOutput() {
        return output;
    }

    public void setOutput(List<ActualDetail> output) {
        this.output = output;
    }

}
