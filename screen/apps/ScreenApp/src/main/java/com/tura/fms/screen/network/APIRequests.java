package com.tura.fms.screen.network;

import com.tura.fms.screen.models.DefaultResponse;
import com.tura.fms.screen.models.PagingResponse;
import com.tura.fms.screen.models.ResponseModel;
import com.tura.fms.screen.models.ResponseModelCustom;
import com.tura.fms.screen.models.activity_model.ActivityData;
import com.tura.fms.screen.models.actual_model.ActualData;
import com.tura.fms.screen.models.actual_model.ActualDetail;
import com.tura.fms.screen.models.actual_model.ActualList;
import com.tura.fms.screen.models.actual_model.ActualLoaderData;
import com.tura.fms.screen.models.actual_support.SupportData;
import com.tura.fms.screen.models.bank_model.BankDetail;
import com.tura.fms.screen.models.bank_model.BankModel;
import com.tura.fms.screen.models.config.ScreenConfigResponse;
import com.tura.fms.screen.models.constant.ConstantData;
import com.tura.fms.screen.models.equipment.EquipmentLogStartRequest;
import com.tura.fms.screen.models.fatigue_model.FatigueLogLocal;
import com.tura.fms.screen.models.maintenance_model.LostStartPost;
import com.tura.fms.screen.models.maintenance_model.MaintenanceMasterPost;
import com.tura.fms.screen.models.maintenance_model.MaintenancePost;
import com.tura.fms.screen.models.maintenance_model.MaintenanceRequestPost;
import com.tura.fms.screen.models.maintenance_model.MaintenanceSetStatusPost;
import com.tura.fms.screen.models.maintenance_model.MechanicArrivePost;
import com.tura.fms.screen.models.maintenance_model.task_model.MaintenanceDescription;
import com.tura.fms.screen.models.maintenance_model.task_model.MaintenanceDetails;
import com.tura.fms.screen.models.maintenance_model.task_model.ResponseMaintenanceDescriptionOutput;
import com.tura.fms.screen.models.maintenance_model.task_model.ResponseMaintenanceDetailsOutput;
import com.tura.fms.screen.models.maintenance_model.task_model.ResponseTaskOutput;
import com.tura.fms.screen.models.maintenance_model.task_model.Taskmodel;
import com.tura.fms.screen.models.plan.CurrentPlanResponse;
import com.tura.fms.screen.models.prestart_model.PrestartDataRest;
import com.tura.fms.screen.models.prestart_model.submit.HourMeterSubmit;
import com.tura.fms.screen.models.prestart_model.submit.PrestartResponse;
import com.tura.fms.screen.models.prestart_model.submit.PrestartResponseBulk;
import com.tura.fms.screen.models.prestart_model.submit.PrestartSubmit;
import com.tura.fms.screen.models.safety_status.SafetyStatusRequest;
import com.tura.fms.screen.models.user_model.UserData;
import com.tura.fms.screen.models.user_model.UserDetails;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;
import java.util.Map;

public interface APIRequests {

    @POST()
    @Headers("Content-Type: application/json")
    Call<ResponseModel> postOfflineGPS(@Url String url, @HeaderMap Map<String, String> headers, @Body Object eq);

    @FormUrlEncoded
    @PUT("v1/user/auth")
    Call<UserData> ProsesLogin(@Field("staff_id") String staff_id, @Field("pwd") String pwd,
            @Field("socket_id") String socket_id, @Field("state") String state,
            @Field("equipment_id") String equipment_id, @Field("unit_type") String unit_type,
            @Field("latitude") Double latitude, @Field("longitude") Double longitude);

    @FormUrlEncoded
    @PUT("v1/user/auth-screen-background")
    Call<UserData> backgroundLogin(@Field("staff_id") String staff_id, @Field("pwd") String pwd,
            @Field("socket_id") String socket_id, @Field("state") String state,
            @Field("equipment_id") String equipment_id, @Field("unit_type") String unit_type,
            @Field("latitude") Double latitude, @Field("longitude") Double longitude);

    @FormUrlEncoded
    @PUT("v1/user/auth/mechanic")
    Call<UserData> ProsesLoginMekanik(@Field("staff_id") String staff_id, @Field("pwd") String pwd,
            @Field("state") String state);

    @GET("v1/bank")
    Call<BankModel> GetBank(@HeaderMap Map<String, String> headers, @Query("page") int page, @Query("limit") int limit);

    @POST("v1/bank")
    Call<BankModel> PostBank(@HeaderMap Map<String, String> headers, @Body BankDetail bankDetail);

    @GET("v1/constant/search-cd-by-cd-self")
    Call<ActivityData> GetActivity(@HeaderMap Map<String, String> headers, @Query("cd_self") String cd);

    @GET("v1/prestart")
    Call<PrestartDataRest> GetPrestart(@HeaderMap Map<String, String> headers,
            @Query("cd_type_equipment") String cd_type_equipment, @Query("page") int page, @Query("limit") int limit,
            @Query("type") String type);

    // untuk prestart submit
    @POST("v1/prestart-log/screen")
    @Headers("Content-Type: application/json")
    Call<PrestartResponse> PrestartSubmit(@HeaderMap Map<String, String> headers, @Body PrestartSubmit prestartSubmit);

    @POST("v1/prestart-log/screen/bulk")
    @Headers("Content-Type: application/json")
    Call<PrestartResponseBulk> prestartSubmitBulk(@HeaderMap Map<String, String> headers,
            @Body List<Object> prestartSubmit);

    @POST("v1/prestart-log/screen/offline-data")
    @Headers("Content-Type: application/json")
    Call<ResponseModel> postOfflinePrestart(@HeaderMap Map<String, String> headers, @Body Object prestartSubmit);

    // untuk prestart submit
    @POST("v1/daily-hour-meter")
    @Headers("Content-Type: application/json")
    Call<ResponseModel> PrestartSubmitHour(@HeaderMap Map<String, String> headers,
            @Body HourMeterSubmit hourMeterSubmit);

    // untuk prestart submit
    @DELETE("v1//prestart-log/{id}")
    Call<PrestartResponse> PrestartDelete(@HeaderMap Map<String, String> headers, @Path("id") long id);

    @GET("v1/service/status")
    Call<ResponseModel> getHealthCheck();

    @GET("v-us/kafka/status")
    Call<ResponseModel> getKafkaStatus();

    @GET("v1/screen-config/public")
    Call<ScreenConfigResponse> getScreenConfig();

    @GET("v1/user")
    Call<Object> getUser(@HeaderMap Map<String, String> headers, @Query("page") int page, @Query("limit") int limit);

    @GET("v1/user/{id}")
    Call<Object> getUserById(@HeaderMap Map<String, String> headers, @Path("id") String id);

    @GET("v1/user/count")
    Call<Object> getUserCount(@HeaderMap Map<String, String> headers);

    @GET("v1/operation/loader/by-equipment-name")
    Call<ActualLoaderData> getActualLoader(@HeaderMap Map<String, String> headers, @Query("date") String date,
            @Query("name") String name, @Query("shift") String shift);

    @GET("v1/operation/plan/by-equipment-name")
    Call<CurrentPlanResponse> getPlanConfig(@HeaderMap Map<String, String> headers, @Query("date") String date,
            @Query("name") String name);

    @GET("v1/operation/actual/by-equipment-name")
    Call<ActualData> getActual(@HeaderMap Map<String, String> headers, @Query("date") String date,
            @Query("name") String name, @Query("type") String type, @Query("shift") String shift);

    @GET("v1/operation-support/by-equipment-name")
    Call<SupportData> getOperationSupport(@HeaderMap Map<String, String> headers, @Query("date") String date,
            @Query("name") String name, @Query("type") String type, @Query("shift") String shift);

    @GET("v1/operation/actual")
    Call<PagingResponse> getActual(@HeaderMap Map<String, String> headers, @Query("page") Number page,
            @Query("limit") Number limit, @Query("id_loader") String id_loader, @Query("id_hauler") String id_hauler,
            @Query("date") String dt);

    @GET("v1/operation-support")
    Call<PagingResponse> getActualSupport(@HeaderMap Map<String, String> headers, @Query("page") Number page,
            @Query("limit") Number limit, @Query("equipment_id") String equipment_id, @Query("date") String dt);

    @GET("v1/operation/actual/today-count")
    Call<DefaultResponse> getCountActualToday(@HeaderMap Map<String, String> headers, @Query("unit_id") String unitId,
            @Query("unit_type") String unitType);

    @GET("v1/operation-support/today-count")
    Call<DefaultResponse> getCountActualSupportToday(@HeaderMap Map<String, String> headers,
            @Query("unit_id") String unitId, @Query("unit_type") String unitType);

    @POST("v1/operation/actual")
    Call<DefaultResponse> postOperationActual(@HeaderMap Map<String, String> headers, @Body List<Object> detail);

    @POST("v1/operation-support")
    Call<ResponseModel> postOperationActualSupport(@HeaderMap Map<String, String> headers, @Body List<Object> detail);

    @GET("v1/operation/actual/get-current-hauler")
    Call<ActualList> getCurrentHauler(@HeaderMap Map<String, String> headers, @Query("loader") String loader);

    @GET("v1/equipment")
    Call<Object> getEquipment(@HeaderMap Map<String, String> headers, @Query("name") String name,
            @Query("types") String types, @Query("page") int page, @Query("limit") int limit);

    @GET("v1/equipment-category")
    Call<Object> getEquipmentCategory(@HeaderMap Map<String, String> headers, @Query("page") int page,
            @Query("limit") int limit);

    @GET("v1/equipment-category/count")
    Call<Object> getEquipmentCategoryCount(@HeaderMap Map<String, String> headers);

    @GET("v1/equipment-category/{id}")
    Call<Object> getEquipmentCategoryById(@HeaderMap Map<String, String> headers, @Path("id") String id);

    @GET("v1/equipment/count")
    Call<Object> getEquipmentCount(@HeaderMap Map<String, String> headers);

    @POST("v1/equipment")
    @Headers("Content-Type: application/json")
    Call<ResponseModel> postEquipment(@HeaderMap Map<String, String> headers, @Body List<Object> eq);

    @GET("v1/equipment/{id}")
    Call<Object> getEquipmentById(@HeaderMap Map<String, String> headers, @Path("id") String id);

    @POST("v1/equipment-log-start/screen/activity")
    @Headers("Content-Type: application/json")
    Call<DefaultResponse> setActivity(@HeaderMap Map<String, String> headers, @Body EquipmentLogStartRequest status);

    @POST("v1/safety-status")
    @Headers("Content-Type: application/json")
    Call<ResponseModel> setSafetyStatus(@HeaderMap Map<String, String> headers, @Body SafetyStatusRequest status);

    @POST("v1/safety-status/offline-data")
    @Headers("Content-Type: application/json")
    Call<ResponseModel> postOfflineSafetyStatus(@HeaderMap Map<String, String> headers, @Body Object eq);

    @POST("v1/equipment-log-start")
    @Headers("Content-Type: application/json")
    Call<DefaultResponse> postEquipmentLogStart(@HeaderMap Map<String, String> headers, @Body List<Object> status);

    @GET("v1/location")
    Call<Object> getLocation(@HeaderMap Map<String, String> headers, @Query("name") String name,
            @Query("page") int page, @Query("limit") int limit);

    @GET("v1/equipment/by-name")
    Call<DefaultResponse> getEquipmentByName(@HeaderMap Map<String, String> headers, @Query("name") String name);

    @GET("v1/location/count")
    Call<Object> getLocationCount(@HeaderMap Map<String, String> headers);

    @POST("v1/location")
    @Headers("Content-Type: application/json")
    Call<ResponseModel> postLocation(@HeaderMap Map<String, String> headers, @Body List<Object> eq);

    @GET("v1/location/{id}")
    Call<Object> getLocationById(@HeaderMap Map<String, String> headers, @Path("id") String id);

    @GET("v1/constant")
    Call<Object> getConstant(@HeaderMap Map<String, String> headers, @Query("page") int page,
            @Query("limit") int limit);

    @GET("v1/constant/{cd}")
    Call<Object> getConstantById(@HeaderMap Map<String, String> headers, @Path("cd") String cd);

    @GET("v1/constant/count")
    Call<Object> getConstantCount(@HeaderMap Map<String, String> headers);

    @POST("v1/constant")
    @Headers("Content-Type: application/json")
    Call<ResponseModel> postConstant(@HeaderMap Map<String, String> headers, @Body List<Object> eq);

    @GET("v1/material")
    Call<Object> getMaterial(@HeaderMap Map<String, String> headers, @Query("page") int page,
            @Query("limit") int limit);

    @GET("v1/material/count")
    Call<Object> getMaterialCount(@HeaderMap Map<String, String> headers);

    @GET("v1/material/{cd}")
    Call<Object> getMaterialById(@HeaderMap Map<String, String> headers, @Path("cd") String cd);

    @POST("v1/material")
    @Headers("Content-Type: application/json")
    Call<ResponseModel> postMaterial(@HeaderMap Map<String, String> headers, @Body List<Object> eq);

    @GET("v1/prestart")
    Call<Object> getPrestart(@HeaderMap Map<String, String> headers, @Query("page") int page,
            @Query("limit") int limit);

    @GET("v1/prestart/count")
    Call<Object> getPrestartCount(@HeaderMap Map<String, String> headers);

    @POST("v1/prestart")
    @Headers("Content-Type: application/json")
    Call<ResponseModel> postPrestart(@HeaderMap Map<String, String> headers, @Body List<Object> eq);

    @GET("v1/prestart/{id}")
    Call<Object> getPrestartById(@HeaderMap Map<String, String> headers, @Path("id") String id);

    @POST("v1/equipment-log-start")
    @Headers("Content-Type: application/json")
    Call<ResponseModel> postOperationStatus(@HeaderMap Map<String, String> headers, @Body List<Object> eq);

    @POST("v1/equipment-log-start/offline-data")
    @Headers("Content-Type: application/json")
    Call<ResponseModel> postOfflineOperationStatus(@HeaderMap Map<String, String> headers, @Body Object eq);

    @POST("v1/operation/actual/raw")
    @Headers("Content-Type: application/json")
    Call<ResponseModel> postOperation(@HeaderMap Map<String, String> headers, @Body Object eq);

    @POST("v1/operation/actual/raw/offline-data")
    @Headers("Content-Type: application/json")
    Call<ResponseModel> postOfflineOperation(@HeaderMap Map<String, String> headers, @Body Object eq);

    @POST("v1/operation/actual/hm-logout")
    @Headers("Content-Type: application/json")
    Call<ResponseModel> postHmLogOut(@HeaderMap Map<String, String> headers, @Body Object eq);

    @POST("v1/fatigue-operation/screen/fatigue")
    @Headers("Content-Type: application/json")
    Call<ResponseModel> postFatigueOperation(@HeaderMap Map<String, String> headers, @Body List<FatigueLogLocal> eq);

    @POST("v1/fatigue-logs/operation")
    @Headers("Content-Type: application/json")
    Call<ResponseModel> postFatigueLog(@HeaderMap Map<String, String> headers, @Body Object eq);

    @POST("v1/fatigue-operation/offline-data")
    @Headers("Content-Type: application/json")
    Call<ResponseModel> postOfflineFatigueLog(@HeaderMap Map<String, String> headers, @Body Object eq);

    @POST("v1/operation/log-detail")
    @Headers("Content-Type: application/json")
    Call<ResponseModel> postOperationLogDetail(@HeaderMap Map<String, String> headers, @Body Object eq);

    @POST("v1/operation/log-detail")
    @Headers("Content-Type: application/json")
    Call<ResponseModel> postOperationLogDetailBulk(@HeaderMap Map<String, String> headers, @Body List<Object> eq);

    @POST("v1/operation/log-detail/offline-data")
    @Headers("Content-Type: application/json")
    Call<ResponseModel> postOfflineOperationLogDetail(@HeaderMap Map<String, String> headers, @Body Object eq);

    @POST("v1/daily-hour-meter")
    @Headers("Content-Type: application/json")
    Call<ResponseModel> postDailyHourMeter(@HeaderMap Map<String, String> headers, @Body Object eq);

    @POST("v1/daily-hour-meter/screen/start")
    @Headers("Content-Type: application/json")
    Call<ResponseModel> postDailyStart(@HeaderMap Map<String, String> headers, @Body Object eq);

    @POST("v1/daily-hour-meter/screen/stop")
    @Headers("Content-Type: application/json")
    Call<ResponseModel> postDailyStop(@HeaderMap Map<String, String> headers, @Body Object eq);

    @GET("v1/daily-hour-meter/screen/latest")
    Call<ResponseModel> getLatestHM(@HeaderMap Map<String, String> headers, @Query("equipment_id") String equipment_id);

    @POST("v1/daily-hour-meter/offline-data")
    @Headers("Content-Type: application/json")
    Call<ResponseModel> postOfflineHM(@HeaderMap Map<String, String> headers, @Body Object eq);

    @POST("v1/maintenance-request")
    Call<MaintenanceRequestPost> PostMaintenance(@HeaderMap Map<String, String> headers,
            @Body MaintenancePost maintenancePost);

    @GET("v1/maintenance")
    Call<ResponseTaskOutput> getMaintenance(@HeaderMap Map<String, String> headers, @Query("page") int page,
            @Query("limit") int limit, @Query("type") String type, @Query("equipment_id") String eq);

    @GET("v1/maintenance/{id}")
    Call<ResponseModel> getMaintenanceById(@HeaderMap Map<String, String> headers, @Path("id") String id_maintenance);

    @POST("v1/maintenance/start/{id}")
    Call<ResponseModelCustom<Taskmodel>> postMaintenanceStart(@HeaderMap Map<String, String> headers,
            @Path("id") String id);

    @POST("v1/maintenance/done/{id}")
    Call<ResponseModelCustom<Taskmodel>> postMaintenanceStop(@HeaderMap Map<String, String> headers,
            @Path("id") String id);

    @GET("v1/maintenance-details")
    Call<ResponseMaintenanceDetailsOutput> getMaintenanceDetails(@HeaderMap Map<String, String> headers,
            @Query("page") int page, @Query("limit") int limit, @Query("maintenance_id") String mid);

    @POST("v1/maintenance-details")
    @Headers("Content-Type: application/json")
    Call<ResponseModelCustom<MaintenanceDetails>> postMaintenanceDetails(@HeaderMap Map<String, String> headers,
            @Body Object eq);

    @POST("v1/maintenance-details/stop")
    @Headers("Content-Type: application/json")
    Call<ResponseModelCustom<MaintenanceDetails>> stopMaintenanceDetails(@HeaderMap Map<String, String> headers,
            @Body Object eq);

    @PUT("v1/maintenance-details/{id}")
    @Headers("Content-Type: application/json")
    Call<ResponseModelCustom<MaintenanceDetails>> putMaintenanceDetails(@HeaderMap Map<String, String> headers,
            @Body Object eq, @Path("id") String id);

    @DELETE("v1/maintenance-details/{id}")
    @Headers("Content-Type: application/json")
    Call<ResponseModelCustom<MaintenanceDetails>> deleteMaintenanceDetails(@HeaderMap Map<String, String> headers,
            @Path("id") String id);

    @GET("v1/maintenance-description")
    Call<ResponseMaintenanceDescriptionOutput> getMaintenanceDescription(@HeaderMap Map<String, String> headers,
            @Query("page") int page, @Query("limit") int limit, @Query("maintenance_id") String mid);

    @POST("v1/maintenance-description")
    @Headers("Content-Type: application/json")
    Call<ResponseModelCustom<MaintenanceDescription>> postMaintenanceDescription(@HeaderMap Map<String, String> headers,
            @Body Object eq);

    @PUT("v1/maintenance-description/{id}")
    @Headers("Content-Type: application/json")
    Call<ResponseModelCustom<MaintenanceDescription>> putMaintenanceDescription(@HeaderMap Map<String, String> headers,
            @Body Object eq, @Path("id") String id);

    @DELETE("v1/maintenance-description/{id}")
    @Headers("Content-Type: application/json")
    Call<ResponseModelCustom<MaintenanceDescription>> deleteMaintenanceDescription(
            @HeaderMap Map<String, String> headers, @Path("id") String id);

    @POST("v1/equipment-log-start/screen/activity")
    Call<LostStartPost> postStopMaintenance(@HeaderMap Map<String, String> headers, @Body LostStartPost lostStartPost);

    @POST("v1/maintenance")
    Call<MaintenanceRequestPost> postMaintenanceMaster(@HeaderMap Map<String, String> headers,
            @Body MaintenanceMasterPost maintenancePost);

    @GET("v1/maintenance/move-to-backlog")
    Call<ResponseModel> getMoveToBacklog(@HeaderMap Map<String, String> headers, @Query("id") String idm);

    @POST("v1/maintenance/set-status")
    Call<ResponseModel> postSetStatus(@HeaderMap Map<String, String> headers, @Body MaintenanceSetStatusPost idm);

    @POST("v1/equipment-log-start/screen/mechanic-arrive")
    Call<DefaultResponse> postMechanicArrive(@HeaderMap Map<String, String> headers, @Body MechanicArrivePost data);

    @GET("v1/equipment-log-start/screen/latest-activity")
    Call<ResponseModel> getLatestActivity(@HeaderMap Map<String, String> headers,
            @Query("equipment_id") String equipmentId, @Query("actual_id") String actualId,
            @Query("actual_support_id") String actualSupportId);

    @GET("v1/constant/search-cd-by-cd-self")
    Call<ConstantData> GetConstant(@HeaderMap Map<String, String> headers, @Query("cd_self") String cd);

    @GET("v1/user/user")
    Call<List<UserDetails>> searchMekanik(@HeaderMap Map<String, String> headers, @Query("page") int page,
            @Query("limit") int limit, @Query("cd_role") String cd_role);

    @GET("v1/operation/actual/raw/cycle-count/{id}")
    Call<ResponseModel> getActualCycle(@HeaderMap Map<String, String> headers, @Path("id") String id_actual);

    @GET("v1/operation/actual/raw/cycle-loader-total/{id}")
    Call<ResponseModel> getActualCycleLoader(@HeaderMap Map<String, String> headers, @Path("id") String id_loader);

    @GET("v1/crew/current")
    Call<ResponseModel> getCurrentCrew(@HeaderMap Map<String, String> headers, @Query("role") String role);

    @GET("v1/crew/check")
    Call<ResponseModel> checkCurrentCrew(@HeaderMap Map<String, String> headers, @Query("user_id") String user);

    @GET("v1/on-duty")
    Call<ResponseModel> getOnDuty(@HeaderMap Map<String, String> headers, @Query("actual") String actual,
            @Query("acsupport") String support);

    @POST("v1/log-history")
    Call<ResponseModel> postLogHistory(@HeaderMap Map<String, String> headers, @Body Object data);

    @GET("v1/operation/actual/screen-actual/{id}")
    Call<ResponseModelCustom<ActualDetail>> getHaulerActualById(@HeaderMap Map<String, String> headers,
            @Path("id") String id_actual);

    @GET("v1/operation/actual/actual-area/{id}")
    Call<ResponseModel> getHaulerArea(@HeaderMap Map<String, String> headers, @Path("id") String id_actual);

    @GET("v1/operation/loader/loader-area/{id}")
    Call<ResponseModel> getLoaderArea(@HeaderMap Map<String, String> headers, @Path("id") String id_actual);

    @GET("v1/operation-support/support-area/{id}")
    Call<ResponseModel> getSupportArea(@HeaderMap Map<String, String> headers, @Path("id") String id_actual);
}
