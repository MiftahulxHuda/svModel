package com.tura.fms.screen.models.device_status;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.constanst.IKafkaConstant;
import com.tura.fms.screen.database.Operation;
import com.tura.fms.screen.database.SQLiteJDBC;
import com.tura.fms.screen.network.NetworkConnection;

import org.apache.kafka.clients.producer.RecordMetadata;

public class DeviceStatusDB extends Operation {

    public static final String TABLE_NAME = "DeviceStatus_Record";

    private static final String date = "date";
    private static final String cpu_usage = "cpu_usage";
    private static final String ram_usage = "ram_usage";
    private static final String disk_usage = "disk_usage";
    private static final String db_size = "db_size";
    private static final String sync_status = "sync_status";

    public DeviceStatusDB() {
        super(DeviceStatusDB.TABLE_NAME, ConstantValues.DATA_DEVICE);
    }

    public void createTableDeviceStatus() {
        try {
            conn().setAutoCommit(true);
            DatabaseMetaData dbm = conn().getMetaData();
            ResultSet rs = dbm.getTables(null, null, TABLE_NAME, null);
            Statement stmt = null;
            if (rs.next()) {
                System.out.println("Table device exists");
            } else {
                stmt = conn().createStatement();
                stmt.setQueryTimeout(10);
                stmt.executeUpdate(createTable());
                stmt.close();
                System.out.println("Table device created");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String createTable() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" + date + " TEXT PRIMARY KEY," + cpu_usage
                + " TEXT NULL," + ram_usage + " TEXT NULL," + disk_usage + " TEXT NULL," + db_size + " TEXT NULL,"
                + sync_status + " NUMBER NULL" + ")";
    }

    @Override
    public Object insertIgnore(Object eqa) {
        DeviceStatusLocal eq = (DeviceStatusLocal) eqa;
        PreparedStatement st = null;
        Boolean rs = null;
        try {
            String sql = "INSERT INTO " + TABLE_NAME + " " + "(" + date + "," + cpu_usage + "," + ram_usage + ","
                    + disk_usage + "," + db_size + "," + sync_status + ")"
                    + " VALUES(?,?,?,?,?,0) ON CONFLICT(date) DO UPDATE SET sync_status=sync_status;";
            st = conn().prepareStatement(sql);
            st.setString(1, eq.getDate());
            st.setString(2, eq.getCpuUsage());
            st.setString(3, eq.getRamUsage());
            st.setString(4, eq.getDiskUsage());
            st.setString(5, eq.getDbSize());
            rs = st.execute();
            st.close();

            return eq;

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (rs && st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;

        }
    }

    @Override
    public void deleteData(String id) {
        String sql = "delete from " + TABLE_NAME + " where date=?";
        try {
            PreparedStatement ps = conn().prepareStatement(sql);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object getFromServer(Integer page, Integer size) {
        if (!NetworkConnection.getServiceStatus()) {
            return null;
        }
        // no action
        return null;
    }

    @Override
    public Integer getRemoteCount() {
        if (!NetworkConnection.getServiceStatus()) {
            return 0;
        }
        // no action
        return 0;
    }

    @Override
    public List<Object> getFlaggedData() {
        try {
            String sql = "SELECT * FROM " + TABLE_NAME + " WHERE sync_status=?";
            PreparedStatement preparedStatement = conn().prepareStatement(sql);
            preparedStatement.setString(1, "0");
            ResultSet cursor = preparedStatement.executeQuery();
            List<Object> result = new ArrayList<Object>();
            while (cursor.next()) {
                DeviceStatusLocal eq = new DeviceStatusLocal();
                eq.setDate(cursor.getString(date));
                eq.setCpuUsage(cursor.getString(cpu_usage));
                eq.setRamUsage(cursor.getString(ram_usage));
                eq.setDiskUsage(cursor.getString(disk_usage));
                eq.setDbSize(cursor.getString(db_size));
                result.add(eq);
            }
            cursor.close();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void postBulkData(List<Object> eq) {
        if (!NetworkConnection.getKafkaStatus()) {
            return;
        }
        System.out.println("--- post synchronization ---");
        for (Object obj : eq) {
            DeviceStatusLocal oqe = (DeviceStatusLocal) obj;
            oqe.setOffline(true);
            RecordMetadata rm = MainApp.producer.push(IKafkaConstant.TOPIC_PRODUCE_DEVICE, obj);
            if (rm.hasOffset()) {
                setSynchronized(oqe.getDate());
            }
        }
    }

    @Override
    public void setSynchronized(String id) {
        String sql = "update " + TABLE_NAME + " set sync_status=1 where date=?";
        // deleteData(id);
        try {
            PreparedStatement ps = conn().prepareStatement(sql);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
