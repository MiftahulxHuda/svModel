package com.tura.fms.screen.presenter;

import java.io.IOException;
import java.util.List;

import com.jfoenix.controls.JFXSnackbar;
import com.jfoenix.controls.JFXSnackbarLayout;
import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.database.Activity_DB;
import com.tura.fms.screen.database.Synchronizer;
import com.tura.fms.screen.models.DefaultResponse;
import com.tura.fms.screen.models.activity_model.ActivityData;
import com.tura.fms.screen.models.activity_model.ActivityDetail;
import com.tura.fms.screen.models.equipment.EquipmentLogStartLocal;
import com.tura.fms.screen.models.equipment.EquipmentLogStartRequest;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import retrofit2.Call;
import retrofit2.Callback;

public class ActivityPresenter {

    private static List<ActivityDetail> activityDetails = null;
    public static final NetworkConnection networkConnection = new NetworkConnection();
    Activity_DB activity_db;
    private static final Logger log = LoggerFactory.getLogger(ActivityPresenter.class);

    public void sendActivity(EquipmentLogStartRequest activity, JFXSnackbar snackbar) {
        Call<DefaultResponse> call = APIClient.getInstance().setActivity(MainApp.requestHeader, activity);

        call.enqueue(new Callback<DefaultResponse>() {

            @Override
            public void onResponse(Call<DefaultResponse> call, retrofit2.Response<DefaultResponse> response) {

                if (response.body().getRespon().equals("success")) {
                    MainApp.latestActivity = Synchronizer.mapper.convertValue(response.body().getOutput(),
                            EquipmentLogStartLocal.class);
                    System.out.println(response.body().getOutput());
                }

                snackbar.fireEvent(new JFXSnackbar.SnackbarEvent(new JFXSnackbarLayout(response.body().getMessage())));
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {
                System.out.print(t.getMessage());

            }

        });
    }

    // untuk get activity
    public ActivityData getDataRestApi(String token, String cd) {

        // Map<String, String> aturheader = new HashMap<>();
        // aturheader.put("Authorization", "Bearer " + token);

        activity_db = new Activity_DB();
        // System.out.println("coba "+activityDetails.get(0).getName());

        try {

            if (NetworkConnection.getServiceStatus()) {
                Call<ActivityData> call = APIClient.getInstance().GetActivity(MainApp.requestHeader, cd);

                activityDetails = call.execute().body().getOutput();

            } else {
                // log.info("G Koneks");
                // ngambil di local jika koneksi lost
                activityDetails = null;
                activityDetails = activity_db.getData(cd);

            }

        } catch (IOException e) {

            e.printStackTrace();
            // log.info("Gagal");
            // ngambil di local jika request gagal
            activityDetails = null;
            activityDetails = activity_db.getData(cd);

        }

        ActivityData hasil = new ActivityData();
        hasil.setOutput(activityDetails);

        return hasil;

    }

}
