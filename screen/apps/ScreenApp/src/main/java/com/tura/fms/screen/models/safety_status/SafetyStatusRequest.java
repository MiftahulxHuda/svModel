package com.tura.fms.screen.models.safety_status;

import com.google.gson.annotations.SerializedName;

public class SafetyStatusRequest {

    @SerializedName("id")
    private String id;

    @SerializedName("date")
    private Number date;

    @SerializedName("actual_id")
    private String actual_id;

    @SerializedName("operation_support_id")
    private String operation_support_id;

    @SerializedName("operation_loader_id")
    private String operation_loader_id;

    @SerializedName("cd_safety")
    private String cd_safety;

    @SerializedName("latitude")
    private Number latitude;

    @SerializedName("longitude")
    private Number longitude;

    @SerializedName("location_id")
    private String location_id;

    @SerializedName("operator_id")
    private String operator_id;

    @SerializedName("equipment_id")
    private String equipment_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Number getDate() {
        return date;
    }

    public void setDate(Number date) {
        this.date = date;
    }

    public String getIdActual() {
        return actual_id;
    }

    public void setIdActual(String id_actual) {
        this.actual_id = id_actual;
    }

    public String getIdOperationSupport() {
        return operation_support_id;
    }

    public void setIdOperationSupport(String id_operation_support) {
        this.operation_support_id = id_operation_support;
    }

    public String getIdOperationLoader() {
        return operation_loader_id;
    }

    public void setIdOperationLoader(String id_operation_loader) {
        this.operation_loader_id = id_operation_loader;
    }

    public String getCdSafety() {
        return cd_safety;
    }

    public void setCdSafety(String cd_safety) {
        this.cd_safety = cd_safety;
    }

    public Number getLatitude() {
        return latitude;
    }

    public void setLatitude(Number latitude) {
        this.latitude = latitude;
    }

    public Number getLongitude() {
        return longitude;
    }

    public void setLongitude(Number longitude) {
        this.longitude = longitude;
    }

    public String getIdLocation() {
        return location_id;
    }

    public void setIdLocation(String id_location) {
        this.location_id = id_location;
    }

    public String getIdOperator() {
        return operator_id;
    }

    public void setIdOperator(String id_operator) {
        this.operator_id = id_operator;
    }

    public String getIdEquipment() {
        return equipment_id;
    }

    public void setIdEquipment(String id_equipment) {
        this.equipment_id = id_equipment;
    }

}