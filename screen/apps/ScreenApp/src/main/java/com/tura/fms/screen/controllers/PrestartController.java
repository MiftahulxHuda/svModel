package com.tura.fms.screen.controllers;

import java.net.ConnectException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.TimeoutException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.database.Actual_DB;
import com.tura.fms.screen.helpers.GPS;
import com.tura.fms.screen.helpers.NavigationUI;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.actual_model.ActualDetail;
import com.tura.fms.screen.models.equipment.EquipmentLogStartRequest;
import com.tura.fms.screen.models.operation_status.OperationStatusLocal;
import com.tura.fms.screen.models.prestart_model.PrestartData;
import com.tura.fms.screen.models.prestart_model.PrestartDataDetail;
import com.tura.fms.screen.models.prestart_model.PrestartDataRest;
import com.tura.fms.screen.models.prestart_model.submit.PrestartResponseBulk;
import com.tura.fms.screen.models.prestart_model.submit.PrestartSubmit;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;
import com.tura.fms.screen.presenter.CustomRunnableX;
import com.tura.fms.screen.presenter.PrestartPresenter;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.util.Duration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrestartController implements Initializable {

    // private NavigationUI navigationUI = new NavigationUI();

    // SessionFMS sessionFMS;

    // @FXML
    // private ScrollPane scrollPane;

    @FXML
    private AnchorPane stackPane;

    // @FXML
    // private GridPane gridata;

    @FXML
    private Button buttonSatu;

    @FXML
    private Button buttonDua;

    @FXML
    private Button buttonTiga;

    @FXML
    private Button buttonEmpat;

    @FXML
    private Button buttonLima;

    @FXML
    private StackPane rootPane;

    @FXML
    private Label mode_connect;

    @FXML
    private Label labelHM;

    @FXML
    private TextField txthour;

    @FXML
    private Text textTitle;

    @FXML
    Button buttonSubmit;

    String texthour;

    PrestartPresenter presenter = new PrestartPresenter();

    private static PrestartDataRest prestartDataBahanBakar = null;

    private static PrestartDataRest prestartDataKomponen = null;

    long unixTime;

    ActualDetail actualDetail;

    Actual_DB actual_db = new Actual_DB();

    Map<String, String> bahanBakarValue = new HashMap<String, String>();
    Map<String, Integer> bahanBakarQty = new HashMap<String, Integer>();
    Map<String, String> komponenValue = new HashMap<String, String>();

    GridPane gridPane1;
    GridPane gridPane2;
    GridPane gridPane3;
    GridPane gridPane4;
    GridPane gridPane5;

    Timeline clockDashboard = null;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        NavigationUI.publicStackPane = rootPane;

        // TODO
        // sessionFMS = new SessionFMS();

        // set indikator connection
        mode_connect.setText(NetworkConnection.getStringStatus());

        // sessionFMS.touchFms();
        // token = sessionFMS.getSessionFms("token");

        txthour.setText("");

        // txthour.setPromptText("");
        txthour.getProperties().put("vkType", "numeric");

        // txthour.setOnKeyPressed((KeyEvent ke) -> {
        // if (ke.getCode().equals(KeyCode.ENTER)) {
        // try {
        // // buttonSubmit.fire();
        // MainApp.popup.hide();
        // } catch (Exception ex) {
        // ex.printStackTrace();
        // }
        // }
        // });

        unixTime = System.currentTimeMillis() / 1000L;
        // System.out.println("ID :"+sessionFMS.getSessionFms("id"));

        gridPane1 = new GridPane();
        gridPane1.getStyleClass().add("bg-dark-sub");
        gridPane1.setHgap(7);
        gridPane1.setVgap(30);
        gridPane1.setPadding(new Insets(40, 0, 0, 0));

        gridPane2 = new GridPane();
        gridPane2.getStyleClass().add("bg-dark-sub");
        gridPane2.setHgap(7);
        gridPane2.setVgap(30);
        gridPane2.setPadding(new Insets(40, 0, 0, 0));

        gridPane3 = new GridPane();
        gridPane3.getStyleClass().add("bg-dark-sub");
        gridPane3.setHgap(7);
        gridPane3.setVgap(30);
        gridPane3.setPadding(new Insets(40, 0, 0, 0));

        gridPane4 = new GridPane();
        gridPane4.getStyleClass().add("bg-dark-sub");
        gridPane4.setHgap(7);
        gridPane4.setVgap(30);
        gridPane4.setPadding(new Insets(40, 0, 0, 0));

        gridPane5 = new GridPane();
        gridPane5.getStyleClass().add("bg-dark-sub");
        gridPane5.setHgap(7);
        gridPane5.setVgap(30);
        gridPane5.setPadding(new Insets(40, 0, 0, 0));

        // isiModel();
        // scrollPane.setFitToWidth(true);
        // scrollPane.setFitToHeight(true);

        buttonSatu.setDisable(true);

        /**
         * online mode
         */
        // presenter.getDataRestApi(MainApp.config("config.unit_type"), "bahanbakar",
        // new CustomRunnable() {
        // @Override
        // public void run() {
        // System.out.println("===========================>> prestart data");
        // System.out.println(this.getData());
        // Platform.runLater(() -> {
        // showPrestartBahanBakar((List<PrestartDataDetail>) this.getData(), 0);
        // stackPane.getChildren().add(gridPane1);
        // });
        // super.run();
        // }
        // });
        // presenter.getDataRestApi(MainApp.config("config.unit_type"), "komponen", new
        // CustomRunnable() {
        // @Override
        // public void run() {
        // System.out.println("===========================>> prestart data 2");
        // System.out.println(this.getData());
        // Platform.runLater(() -> {
        // showPrestartKomponen((List<PrestartDataDetail>) this.getData(), 0);
        // });
        // super.run();
        // }
        // });

        /**
         * offline mode
         */
        List<PrestartDataDetail> prestartLocalBahanBakar = MainApp.prestartDB
                .getData(MainApp.config("config.unit_type"), "bahanbakar");

        List<PrestartDataDetail> prestartLocalKomponen = MainApp.prestartDB.getData(MainApp.config("config.unit_type"),
                "komponen");

        Platform.runLater(() -> {
            showPrestartBahanBakar(prestartLocalBahanBakar, 0);
            stackPane.getChildren().add(gridPane1);
        });

        Platform.runLater(() -> {
            showPrestartKomponen(prestartLocalKomponen, 0);
        });

        clockDashboard = new Timeline(new KeyFrame(Duration.ZERO, e -> {
            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    String netS = NetworkConnection.getStringStatus();
                    mode_connect.setText(netS);
                    mode_connect.getStyleClass().clear();
                    if (netS.equals("Online")) {
                        mode_connect.getStyleClass().add("font-online");
                    } else {
                        mode_connect.getStyleClass().add("font-offline");
                    }

                    if ((MainApp.apiStatus == ConstantValues.DONE) && APIClient.exceptionHappened) {
                        buttonSubmit.setDisable(false);
                        textTitle.setText("Pre-start Check");
                    }
                }
            });
        }), new KeyFrame(Duration.seconds(MainApp.dashboardInterval)));
        clockDashboard.setCycleCount(Animation.INDEFINITE);
        clockDashboard.play();

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                if (MainApp.latestHourMeter != null) {
                    labelHM.setText("HM : " + String.valueOf(MainApp.latestHourMeter.intValue()));
                } else {
                    labelHM.setText("HM : 0");
                }
            }
        });

    }

    private void showPrestartBahanBakar(List<PrestartDataDetail> data, int Row) {
        int Col = 0;
        int Part = 0;
        int index = 0;
        for (PrestartDataDetail pd : data) {
            if (index == 13) {
                Row = 0;
                Col = 0;
                Part = 0;
            }

            index++;

            Label label = new Label(pd.getName());
            // label.setFont(Font.font("Arial", FontWeight.BOLD, 12));
            label.setStyle("-fx-font-size: 20px;");
            label.getStyleClass().add("checkbox");
            label.setMinWidth(200.0);
            label.setMaxWidth(200.0);
            label.setWrapText(true);

            ComboBox comboBox = new ComboBox();
            comboBox.setId(pd.getId());
            comboBox.getItems().add("Cukup");
            comboBox.getItems().add("Kurang");
            bahanBakarValue.put(pd.getId(), "");
            comboBox.valueProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    bahanBakarValue.put(comboBox.getId(), newValue.toLowerCase());
                }
            });
            comboBox.setPrefWidth(180);
            comboBox.setStyle(
                    "-fx-background-color: #3d453f;-fx-text-fill: #d9c750;-fx-font-size: 23px;-fx-font-weight: bold;");

            TextField textField = new TextField();
            textField.setId(pd.getId());
            textField.setPrefWidth(100.0);
            textField.setStyle("-fx-font-size: 23px;");
            // textField.setFont(Font.font("Arial", FontWeight.BOLD, 12));
            textField.getProperties().put("vkType", "numeric");
            bahanBakarQty.put(pd.getId(), null);
            textField.textProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    if (!newValue.matches("\\d*")) {
                        textField.setText(newValue.replaceAll("[^\\d]", ""));
                    }
                    bahanBakarQty.put(textField.getId(), Integer.parseInt(newValue));
                }
            });

            // textField.setOnKeyPressed((KeyEvent ke) -> {
            // if (ke.getCode().equals(KeyCode.ENTER)) {
            // try {
            // // buttonSubmit.fire();
            // MainApp.popup.hide();
            // } catch (Exception ex) {
            // ex.printStackTrace();
            // }
            // }
            // });

            if (index < 13) {
                gridPane1.add(label, Col, Row);
                Col++;
                gridPane1.add(comboBox, Col, Row);
                Col++;
                gridPane1.add(textField, Col, Row);
                gridPane1.setHalignment(label, HPos.LEFT);
                gridPane1.setValignment(label, VPos.CENTER);
            } else {
                gridPane2.add(label, Col, Row);
                Col++;
                gridPane2.add(comboBox, Col, Row);
                Col++;
                gridPane2.add(textField, Col, Row);
                gridPane2.setHalignment(label, HPos.LEFT);
                gridPane2.setValignment(label, VPos.CENTER);
            }

            Col = 0;
            Row++;

            if (Part == 0) {
                if (Row == 6) {
                    Part = 1;
                    Row = 0;
                    Col += 3;
                }
            } else if (Part == 1) {
                if (Row == 6) {
                    Part = 2;
                    Row = 0;
                    Col = 0;
                }
                Col += 3;
            } else {
                Row = 7;
            }
        }
    }

    private void showPrestartKomponen(List<PrestartDataDetail> data, int Row) {
        int Col = 0;
        int Part = 0;
        int index = 0;

        for (PrestartDataDetail pdk : data) {

            if (index == 12) {
                Row = 0;
                Col = 0;
                Part = 0;
            }
            if (index == 24) {
                Row = 0;
                Col = 0;
                Part = 0;
            }

            Label label = new Label(pdk.getName());
            // label.setFont(Font.font("Arial", FontWeight.BOLD, 12));
            label.setStyle("-fx-font-size: 20px;");
            label.getStyleClass().add("checkbox");
            label.setMinWidth(200.0);

            ComboBox comboBox = new ComboBox();
            comboBox.setId(pdk.getId());
            comboBox.getItems().add("Baik");
            comboBox.getItems().add("Rusak");
            comboBox.getItems().add("Hilang");
            komponenValue.put(pdk.getId(), "");
            comboBox.valueProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                    komponenValue.put(comboBox.getId(), newValue.toLowerCase());
                }
            });
            comboBox.setPrefWidth(180);
            comboBox.setStyle(
                    "-fx-background-color: #3d453f;-fx-text-fill: #d9c750;-fx-font-size: 23px;-fx-font-weight: bold;");

            // System.out.println(Col + "," + Row + "--" + Part + "--" + index + "--" +
            // pdk.getName());

            if (index < 12) {
                gridPane3.add(label, Col, Row);
                Col++;
                gridPane3.add(comboBox, Col, Row);

                gridPane3.setHalignment(label, HPos.LEFT);
                gridPane3.setValignment(label, VPos.CENTER);
            } else if (index < 24) {
                gridPane4.add(label, Col, Row);
                Col++;
                gridPane4.add(comboBox, Col, Row);

                gridPane4.setHalignment(label, HPos.LEFT);
                gridPane4.setValignment(label, VPos.CENTER);
            } else {
                gridPane5.add(label, Col, Row);
                Col++;
                gridPane5.add(comboBox, Col, Row);

                gridPane5.setHalignment(label, HPos.LEFT);
                gridPane5.setValignment(label, VPos.CENTER);
            }

            Col = 0;
            Row++;

            if (Part == 0) {
                if (Row == 6) {
                    Part = 1;
                    Row = 0;
                    Col += 2;
                }
            } else if (Part == 1) {
                // if (Row == 6) {
                // // Part = 0;
                // Row = 0;
                // // Col = 0;
                // }
                Col += 2;
            }

            index++;
        }
    }

    @FXML
    private void toDashboard(ActionEvent event) throws Exception {
        Utilities.confirm(rootPane, "Yakin akan di-Submit ?", new Runnable() {

            @Override
            public void run() {
                try {
                    doSubmit(event);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }, null);
    }

    @FXML
    private void btnSatuClick(ActionEvent event) throws Exception {
        Platform.runLater(() -> {
            stackPane.getChildren().clear();
            stackPane.getChildren().add(gridPane1);
            buttonSatu.setDisable(true);
            buttonDua.setDisable(false);
            buttonTiga.setDisable(false);
            buttonEmpat.setDisable(false);
            buttonLima.setDisable(false);
        });
    }

    @FXML
    private void btnDuaClick(ActionEvent event) throws Exception {
        Platform.runLater(() -> {
            stackPane.getChildren().clear();
            stackPane.getChildren().add(gridPane2);
            buttonSatu.setDisable(false);
            buttonDua.setDisable(true);
            buttonTiga.setDisable(false);
            buttonEmpat.setDisable(false);
            buttonLima.setDisable(false);
        });
    }

    @FXML
    private void btnTigaClick(ActionEvent event) throws Exception {
        Platform.runLater(() -> {
            stackPane.getChildren().clear();
            stackPane.getChildren().add(gridPane3);
            buttonSatu.setDisable(false);
            buttonDua.setDisable(false);
            buttonTiga.setDisable(true);
            buttonEmpat.setDisable(false);
            buttonLima.setDisable(false);
        });
    }

    @FXML
    private void btnEmpatClick(ActionEvent event) throws Exception {
        Platform.runLater(() -> {
            stackPane.getChildren().clear();
            stackPane.getChildren().add(gridPane4);
            buttonSatu.setDisable(false);
            buttonDua.setDisable(false);
            buttonTiga.setDisable(false);
            buttonEmpat.setDisable(true);
            buttonLima.setDisable(false);
        });
    }

    @FXML
    private void btnLimaClick(ActionEvent event) throws Exception {
        Platform.runLater(() -> {
            stackPane.getChildren().clear();
            stackPane.getChildren().add(gridPane5);
            buttonSatu.setDisable(false);
            buttonDua.setDisable(false);
            buttonTiga.setDisable(false);
            buttonEmpat.setDisable(false);
            buttonLima.setDisable(true);
        });
    }

    public static void showMessageDialog(String title, String header, String message, CustomRunnableX<String> cbOption,
            Runnable cbNo) {

        JFXDialogLayout content = new JFXDialogLayout();
        content.getStyleClass().add("bg-dark-light");
        Text txtTitle = new Text(title);
        txtTitle.setStyle("-fx-font-size: 20px;-fx-text-fill: #ff3333;");
        txtTitle.setFill(Color.RED);

        content.setHeading(txtTitle);

        VBox vbox = new VBox();
        vbox.setSpacing(20);

        Text txt = new Text(header);
        txt.setStyle("-fx-font-size: 20px;");
        vbox.getChildren().add(txt);

        Text msg = new Text(message);
        msg.setStyle("-fx-font-size: 20px;-fx-font-weight:BOLD;");
        vbox.getChildren().add(msg);

        content.setBody(vbox);

        JFXDialog dialog = new JFXDialog(NavigationUI.publicStackPane, content, JFXDialog.DialogTransition.CENTER,
                false);

        JFXButton gen = new JFXButton("   General   ");
        gen.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
        gen.setPrefHeight(30.0);
        // gen.setPrefWidth(100.0);
        gen.getStyleClass().add("button-cancel");

        gen.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (cbOption != null) {
                    cbOption.setData("General");
                    cbOption.run();
                }
                dialog.close();
            }
        });

        JFXButton mai = new JFXButton("   Maintenance   ");
        mai.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
        mai.setPrefHeight(30.0);
        // mai.setPrefWidth(100.0);
        mai.getStyleClass().add("button-cancel");

        mai.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (cbOption != null) {
                    cbOption.setData("Maintenance");
                    cbOption.run();
                }
                dialog.close();
            }
        });

        HBox hbox = new HBox();
        hbox.setSpacing(20);
        hbox.getChildren().add(gen);
        hbox.getChildren().add(mai);

        vbox.getChildren().add(hbox);

        JFXButton keluar = new JFXButton("Batal");
        keluar.setButtonType(com.jfoenix.controls.JFXButton.ButtonType.RAISED);
        keluar.setPrefHeight(30.0);
        keluar.setPrefWidth(100.0);
        keluar.getStyleClass().add("button-raised");

        keluar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (cbNo != null) {
                    cbNo.run();
                }
                dialog.close();
            }
        });

        content.setActions(keluar);
        dialog.show();

        keluar.requestFocus();
    }

    private void doSubmit(ActionEvent event) throws Exception {
        texthour = txthour.getText();
        if (texthour.length() == 0) {
            texthour = "0";
        }
        Double hmStart = Double.parseDouble(texthour);

        if (MainApp.latestHourMeter != null) {
            if (hmStart < MainApp.latestHourMeter) {
                Utilities.showMessageDialog("Hour Meter Tidak Valid", "",
                        "HM lebih kecil dari HM terakhir (" + MainApp.latestHourMeter.toString() + ")");
                return;
            }
        }

        if (hmStart > MainApp.latestHourMeter) {
            Double lebih = hmStart - MainApp.latestHourMeter;
            showMessageDialog("Hour Meter Lebih Besar", "HM lebih besar dari HM terakhir (" + lebih.toString() + " H)",
                    "Silakan pilih keterangan", new CustomRunnableX<String>() {
                        @Override
                        public void run() {
                            super.run();

                            textTitle.setText("Processing ...");
                            buttonSubmit.setDisable(true);
                            stackPane.requestFocus();

                            Utilities.postHMStart(hmStart, getData());

                            postP2H(event);
                        }
                    }, new Runnable() {

                        @Override
                        public void run() {
                            // no action
                        }
                    });
        } else {

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    textTitle.setText("Processing ...");
                    buttonSubmit.setDisable(true);
                    stackPane.requestFocus();
                }
            });

            Utilities.postHMStart(hmStart, "");

            postP2H(event);

        }
    }

    private void postP2H(ActionEvent event) {
        if (MainApp.equipmentLocal == null) {
            /**
             * trap disini ketika equipment tidak dikenal
             */
            clockDashboard.stop();

            buttonSubmit.setDisable(false);

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    textTitle.setText("Pre-start Check");
                    // hauler
                    NavigationUI.to_dashboard(MainApp.config("config.unit_type"), event);
                }
            });
            return;
        }

        try {
            List<Object> data = new ArrayList<Object>();
            bahanBakarValue.forEach((k, v) -> {
                // try {
                PrestartData pd = new PrestartData();
                pd.setIdEquipment(MainApp.equipmentLocal.getId());
                pd.setIdUser(MainApp.user.getId());
                pd.setIdPrestart(k);
                if (Utilities.isLoaderHauler()) {
                    if (MainApp.isOperator) {
                        pd.setIdActual(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId());
                    }
                } else {
                    if (MainApp.isOperator) {
                        pd.setIdActualSupport(MainApp.actualSupport == null ? null : MainApp.actualSupport.getId());
                    }
                }
                pd.setValue(v);
                pd.setQty(bahanBakarQty.get(k));
                data.add(pd);
                // System.out.println("--- bahanbakar --- " + data.size());
                // } catch (Exception e) {
                // }
            });
            komponenValue.forEach((k, v) -> {
                // try {
                PrestartData pd = new PrestartData();
                pd.setIdEquipment(MainApp.equipmentLocal.getId());
                pd.setIdUser(MainApp.user.getId());
                // pd.setIdUser(sessionFMS.getSessionFms("id"));
                pd.setIdPrestart(k);
                if (Utilities.isLoaderHauler()) {
                    if (MainApp.isOperator) {
                        pd.setIdActual(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId());
                    }
                } else {
                    if (MainApp.isOperator) {
                        pd.setIdActualSupport(MainApp.actualSupport == null ? null : MainApp.actualSupport.getId());
                    }
                }
                pd.setValue(v);
                data.add(pd);
                // System.out.println("--- komponen --- " + data.size());
                // } catch (Exception e) {
                // }
            });

            System.out.println("--- prestart --- " + data.size());

            if (NetworkConnection.getServiceStatus()) {
                /**
                 * online mode
                 */

                Call<PrestartResponseBulk> call = APIClient.getInstance(false).prestartSubmitBulk(MainApp.requestHeader,
                        data);
                // Response<PrestartResponseBulk> response = call.execute();
                // System.out.println(response);

                call.enqueue(new Callback<PrestartResponseBulk>() {

                    @Override
                    public void onResponse(Call<PrestartResponseBulk> call, Response<PrestartResponseBulk> response) {
                        // NavigationUI.to_dashboard(sessionFMS.getSessionFms("sesi_level"), event);
                        // System.out.println(
                        // "============================================================ prestart
                        // response");
                        // Utilities.mapObject(response);

                        /** set activity to main activity */
                        EquipmentLogStartRequest activity = new EquipmentLogStartRequest();
                        if (Utilities.isLoaderHauler()) {
                            if (MainApp.isOperator) {
                                activity.setIdActual(MainApp.actualInfo.getId());
                                activity.setCdActivity(MainApp.actualInfo.getCd_operation().getCd());
                            }
                        } else {
                            if (MainApp.isOperator) {
                                activity.setIdOperationSupport(MainApp.actualSupport.getId());
                                activity.setCdActivity(MainApp.actualSupport.getCdActivity().getCd());
                            }
                        }
                        activity.setIdEquipment(MainApp.equipmentLocal.getId());
                        activity.setIdOperator(MainApp.user.getId());
                        activity.setTime("current");

                        Utilities.setActivity(activity, new Runnable() {

                            @Override
                            public void run() {

                                buttonSubmit.setDisable(false);

                                Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        textTitle.setText("Pre-start Check");
                                        // hauler
                                        NavigationUI.to_dashboard(MainApp.config("config.unit_type"), event);
                                    }
                                });

                            }
                        });

                        /** push to kafka */
                        if (Utilities.isLoaderHauler()) {
                            if (MainApp.isOperator) {
                                Utilities.producerActivity(MainApp.actualInfo.getCd_operation().getCd(),
                                        MainApp.actualInfo.getCd_operation().getName());
                            }
                        } else {
                            if (MainApp.isOperator) {
                                Utilities.producerActivity(MainApp.actualSupport.getCdActivity().getCd(),
                                        MainApp.actualSupport.getCdActivity().getName());
                            }
                        }

                        clockDashboard.stop();
                    }

                    @Override
                    public void onFailure(Call<PrestartResponseBulk> call, Throwable t) {

                        if (t instanceof ConnectException) {
                            Utilities.messageSocketException(t, "Login");
                        } else if (t instanceof TimeoutException) {
                            Utilities.messageTimeoutException(t, "Login");
                        } else {
                            Utilities.messageOtherException(t, "Login");
                        }

                        /**
                         * offline mode
                         */
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                // buttonSubmit.setDisable(false);
                                // textTitle.setText("Pre-start Check");

                                Utilities.showMessageDialog("Save Offline", "Terjadi kesalahan",
                                        "Terjadi kesalahan, data P2H akan disimpan secara lokal", new Runnable() {

                                            @Override
                                            public void run() {
                                                prestartOffline(data, event);
                                            }
                                        });
                            }
                        });
                    }
                });

            } else {
                /**
                 * offline mode
                 */
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        // buttonSubmit.setDisable(false);
                        // textTitle.setText("Pre-start Check");

                        Utilities.showMessageDialog("Save Offline", "Terjadi kesalahan",
                                "Offline mode, data P2H akan disimpan secara lokal", new Runnable() {

                                    @Override
                                    public void run() {
                                        prestartOffline(data, event);
                                    }
                                });
                    }
                });
            }

        } catch (Exception ex) {
            buttonSubmit.setDisable(false);

            ex.printStackTrace();
        }

    }

    public void prestartOffline(List<Object> data, ActionEvent event) {

        Platform.runLater(new Runnable() {
            @Override
            public void run() {

                // save prestart log
                for (Object item : data) {
                    PrestartData pd = (PrestartData) item;
                    PrestartSubmit pl = new PrestartSubmit();
                    pl.setIdActual(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId());
                    pl.setIdOperationSupport(MainApp.actualSupport == null ? null : MainApp.actualSupport.getId());
                    pl.setIdOperationLoader(MainApp.actualLoader == null ? null : MainApp.actualLoader.getId());
                    pl.setTime(Utilities.getCurrentUnix());
                    pl.setIdEquipment(MainApp.equipmentLocal == null ? null : MainApp.equipmentLocal.getId());
                    pl.setIdOperator(MainApp.user == null ? null : MainApp.user.getId());
                    pl.setIdPrestart(pd.getIdPrestart());
                    pl.setValue(pd.getValue());
                    pl.setQty(pd.getQty());
                    MainApp.prestartLogDB.insertIgnore(pl);
                }

                // save main activity
                GPS gps = Utilities.getCurrentGPS();

                OperationStatusLocal mainAct = new OperationStatusLocal();
                mainAct.setIdEquipment(MainApp.equipmentLocal.getId());
                mainAct.setIdOperator(MainApp.user.getId());

                if (MainApp.isOperator) {
                    if (MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_HAULER)) {
                        mainAct.setCdActivity(ConstantValues.ACTIVITY_HAULING);
                    } else if (MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_LOADER)) {
                        mainAct.setCdActivity(ConstantValues.ACTIVITY_LOADING);
                    } else {
                        mainAct.setCdActivity(ConstantValues.ACTIVITY_GENERAL);
                    }

                } else {
                    /**
                     * bukan operator default activity general
                     */

                    mainAct.setCdActivity(ConstantValues.ACTIVITY_GENERAL);

                }

                mainAct.setCdTum(ConstantValues.TUM_WORKING);
                mainAct.setCdType(MainApp.config("config.unit_type"));
                mainAct.setTime(Utilities.getCurrentUnix());
                mainAct.setLatitude(gps.getLat());
                mainAct.setLongitude(gps.getLon());

                OperationStatusLocal saved = (OperationStatusLocal) MainApp.operationStatusDB.insertIgnore(mainAct);

                Utilities.setLatestActivity(saved);

                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        String actName = MainApp.latestActivity.getCdActivity() == null ? ""
                                : ", " + MainApp.latestActivity.getCdActivity().getName();
                        NavigationUI.addLogActivity(MainApp.latestActivity.getCdTum().getCd(),
                                MainApp.latestActivity.getCdTum().getName() + actName);
                    }
                });

                clockDashboard.stop();

                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        buttonSubmit.setDisable(false);
                        textTitle.setText("Pre-start Check");
                        // hauler
                        NavigationUI.to_dashboard(MainApp.config("config.unit_type"), event);
                    }
                });

            }
        });
    }

}
