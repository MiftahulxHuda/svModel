package com.tura.fms.screen.controllers;

import static org.javalite.app_config.AppConfig.p;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXSnackbar;
import com.jfoenix.controls.JFXSnackbarLayout;
import com.tura.fms.screen.MainApp;
import com.tura.fms.screen.constanst.ConstantValues;
import com.tura.fms.screen.database.Activity_DB;
import com.tura.fms.screen.database.Actual_DB;
import com.tura.fms.screen.helpers.GPS;
import com.tura.fms.screen.helpers.NavigationUI;
import com.tura.fms.screen.helpers.Utilities;
import com.tura.fms.screen.models.activity_model.ActivityData;
import com.tura.fms.screen.models.activity_model.ActivityModel;
import com.tura.fms.screen.models.actual_model.ActualDetail;
import com.tura.fms.screen.models.equipment.EquipmentLogStartRequest;
import com.tura.fms.screen.models.operation_status.OperationStatusLocal;
import com.tura.fms.screen.network.APIClient;
import com.tura.fms.screen.network.NetworkConnection;
import com.tura.fms.screen.presenter.ActivityPresenter;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityController implements Initializable {

    private NavigationUI navigationUI = new NavigationUI();
    @FXML
    private Button TitleBar;

    @FXML
    private GridPane gridata;

    private ObservableList<ActivityModel> recordsData = FXCollections.observableArrayList();

    // SessionFMS sessionFMS;

    // String token;

    ActivityData activityData;

    ActivityPresenter activityPresenter = new ActivityPresenter();

    Activity_DB activity_db;

    Actual_DB actual_db;

    ActualDetail actualDetail;

    @FXML
    VBox rootpane;

    JFXSnackbar snackbar;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        MainApp.clockDashboard.stop();

        // sessionFMS = new SessionFMS();
        // sessionFMS.touchFms();
        // token = sessionFMS.getSessionFms("token");

        activity_db = new Activity_DB();
        actual_db = new Actual_DB();

        snackbar = new JFXSnackbar(rootpane);
        snackbar.setPrefWidth(300);
        snackbar.getStyleClass().add("-fx-background-color: #ccc;");

        drawerAction();
        TitleBar.setText(p("app.textLoading"));
        getData();
    }

    private void getData() {
        List<Object> localActivityData;
        List<Object> localGeneralData;

        localActivityData = MainApp.constantDB.getLocalById("cd_self", "ACTIVITY");
        localGeneralData = MainApp.constantDB.getLocalById("cd_self", "GENERAL");

        if (localActivityData != null && localGeneralData != null) {

            TitleBar.setText(p("app.titleActivity"));
            ActivityData act = new ActivityData();
            act.setOutputObject(localActivityData);
            ActivityData actGeneral = new ActivityData();
            actGeneral.setOutputObject(localGeneralData);
            tampilkanData(act, actGeneral);

        } else {

            if (NetworkConnection.getServiceStatus()) {

                Call<ActivityData> call = APIClient.getInstance().GetActivity(MainApp.requestHeader, "ACTIVITY");
                call.enqueue(new Callback<ActivityData>() {

                    @Override
                    public void onResponse(Call<ActivityData> call, Response<ActivityData> response) {
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                TitleBar.setText(p("app.titleActivity"));
                                tampilkanData(response.body(), new ActivityData());
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<ActivityData> call, Throwable t) {
                        t.printStackTrace();
                    }
                });

            } else {
                System.out.println("Service not available and local data is empty");
            }

        }
    }

    private void tampilkanData(ActivityData activityData, ActivityData activityGeneral) {
        int Col = 0;
        int Row = 2;

        String status;

        try {

            if (activityData.getOutput() != null) {

                GPS gps = Utilities.getCurrentGPS();

                Pane spring = new Pane();
                spring.setPrefHeight(20);
                gridata.add(spring, 0, 1);

                for (int i = 0; i < activityData.getOutput().size(); i++) {

                    /// System.out.println(activityData.getOutput().get(i).getCd());
                    // ActivityDetail activityDetail = new ActivityDetail();
                    // activityDetail.setCd(activityData.getOutput().get(i).getCd());
                    // activityDetail.setName(activityData.getOutput().get(i).getName());
                    // activityDetail.setCd_self(activityData.getOutput().get(i).getCd_self());

                    // activity_db.insertData(activityDetail);

                    JFXButton rowbutton = new JFXButton(activityData.getOutput().get(i).getName());
                    VBox vboxForButtons = new VBox();

                    rowbutton.setButtonType(JFXButton.ButtonType.RAISED);
                    rowbutton.getStyleClass().add("button-activity");
                    rowbutton.setFont(Font.font("Arial", FontWeight.BOLD, 90));

                    rowbutton.setPrefHeight(60);
                    rowbutton.setPrefWidth(250);
                    rowbutton.setWrapText(true);
                    rowbutton.setId(String.valueOf(i));

                    // rowbutton.(new Insets(0,0,0,0));
                    vboxForButtons.setPadding(new Insets(10, 5, 10, 5));

                    vboxForButtons.getChildren().add(rowbutton);

                    gridata.setHalignment(rowbutton, HPos.CENTER);
                    gridata.setValignment(rowbutton, VPos.CENTER);

                    rowbutton.setOnAction((ActionEvent) -> {
                        rowbutton.setDisable(true);

                        int index = Integer.valueOf(rowbutton.getId());

                        // snackbar.fireEvent(new JFXSnackbar.SnackbarEvent(
                        // new JFXSnackbarLayout("Sucessfully Data Retrieve")));
                        // System.out.println(rowbutton.getId());

                        if (MainApp.isCanPost()) {

                            EquipmentLogStartRequest activity = new EquipmentLogStartRequest();
                            if (Utilities.isLoaderHauler()) {
                                activity.setIdActual(MainApp.actualInfo.getId());
                            } else {
                                activity.setIdOperationSupport(MainApp.actualSupport.getId());
                            }
                            activity.setIdEquipment(MainApp.equipmentLocal.getId());
                            activity.setIdOperator(MainApp.user.getId());
                            activity.setTime("current");
                            activity.setCdActivity(activityData.getOutput().get(index).getCd());
                            activity.setCdTum(ConstantValues.TUM_WORKING);
                            Utilities.setActivity(activity, new Runnable() {

                                @Override
                                public void run() {
                                    rowbutton.setDisable(false);
                                }
                            });

                            /** push to kafka */
                            Utilities.producerActivity(activityData.getOutput().get(index).getCd(),
                                    activityData.getOutput().get(index).getName());

                        } else {
                            /**
                             * offline mode
                             */

                            OperationStatusLocal act = new OperationStatusLocal();
                            act.setIdActual(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId());
                            act.setIdOperationSupport(
                                    MainApp.actualSupport == null ? null : MainApp.actualSupport.getId());
                            act.setIdOperationLoader(
                                    MainApp.actualLoader == null ? null : MainApp.actualLoader.getId());
                            act.setIdEquipment(MainApp.equipmentLocal.getId());
                            act.setIdOperator(MainApp.user.getId());
                            act.setCdActivity(activityData.getOutput().get(index).getCd());
                            act.setCdTum(ConstantValues.TUM_WORKING);
                            act.setCdType(MainApp.config("config.unit_type"));
                            act.setTime(Utilities.getCurrentUnix());
                            act.setLatitude(gps.getLat());
                            act.setLongitude(gps.getLon());
                            OperationStatusLocal saved = (OperationStatusLocal) MainApp.operationStatusDB
                                    .insertIgnore(act);

                            Utilities.playSound("");

                            Utilities.setLatestActivity(saved);

                            if (MainApp.latestActivity != null) {

                                if (MainApp.latestActivity.getCdActivity() != null) {
                                    MainApp.dashboardInfo.setStatus(MainApp.latestActivity.getCdActivity().getName());
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_WORKING)) {
                                    MainApp.dashboardInfo.setStatusText("Activity");
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_DELAY)) {
                                    MainApp.dashboardInfo.setStatusText("Delay");
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_IDLE)) {
                                    MainApp.dashboardInfo.setStatusText("Idle");
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_DOWN)) {
                                    MainApp.dashboardInfo.setStatusText("Down");
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_STANDBY)) {
                                    MainApp.dashboardInfo.setStatusText("Standby");
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_ASSIGNED)) {
                                    MainApp.dashboardInfo.setStatusText("Assigned");
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_MAINTENANCE)) {
                                    MainApp.dashboardInfo.setStatusText("Maintenance");
                                }

                                Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        String actName = MainApp.latestActivity.getCdActivity() == null ? ""
                                                : ", " + MainApp.latestActivity.getCdActivity().getName();

                                        NavigationUI.addLogActivity(MainApp.latestActivity.getCdTum().getCd(),
                                                MainApp.latestActivity.getCdTum().getName() + actName);
                                    }
                                });
                            }

                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    rowbutton.setDisable(false);
                                }
                            });

                        }

                        snackbar.fireEvent(new JFXSnackbar.SnackbarEvent(new JFXSnackbarLayout("Success")));

                        TitleBar.fire();
                    });

                    String name = activityData.getOutput().get(i).getName().toLowerCase();

                    if (name.equals("hauling")
                            && MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_HAULER)) {

                        gridata.add(vboxForButtons, 0, 0);

                    } else if (name.equals("loading")
                            && MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_LOADER)) {

                        gridata.add(vboxForButtons, 0, 0);

                    } else if (name.equals("grading")
                            && MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_GRADDER)) {

                        gridata.add(vboxForButtons, 0, 0);

                    } else if (name.contains("dozzing")
                            && MainApp.config("config.unit_type").equals(ConstantValues.EQUIPMENT_DOZZER)) {

                        gridata.add(vboxForButtons, 0, 0);

                    } else {

                        gridata.add(vboxForButtons, Col, Row);
                        Col++;

                    }

                    if (Col > 3) {
                        // Reset Column
                        Col = 0;
                        // Next Row
                        Row++;
                    }
                }

                Col = 0;
                Row++;
                Pane spring1 = new Pane();
                spring1.setPrefHeight(20);
                gridata.add(spring1, 0, Row);
                Row++;

                for (int i = 0; i < activityGeneral.getOutput().size(); i++) {

                    /// System.out.println(activityData.getOutput().get(i).getCd());
                    // ActivityDetail activityDetail = new ActivityDetail();
                    // activityDetail.setCd(activityData.getOutput().get(i).getCd());
                    // activityDetail.setName(activityData.getOutput().get(i).getName());
                    // activityDetail.setCd_self(activityData.getOutput().get(i).getCd_self());

                    // activity_db.insertData(activityDetail);

                    JFXButton rowbutton = new JFXButton(activityGeneral.getOutput().get(i).getName());
                    VBox vboxForButtons = new VBox();

                    rowbutton.setButtonType(JFXButton.ButtonType.RAISED);
                    rowbutton.getStyleClass().add("button-activity");
                    rowbutton.setFont(Font.font("Arial", FontWeight.BOLD, 90));

                    rowbutton.setPrefHeight(60);
                    rowbutton.setPrefWidth(250);
                    rowbutton.setWrapText(true);
                    rowbutton.setId(String.valueOf(i));
                    rowbutton.setPadding(new Insets(20));

                    // rowbutton.(new Insets(0,0,0,0));
                    vboxForButtons.setPadding(new Insets(10, 5, 10, 5));

                    vboxForButtons.getChildren().add(rowbutton);

                    gridata.setHalignment(rowbutton, HPos.CENTER);
                    gridata.setValignment(rowbutton, VPos.CENTER);

                    rowbutton.setOnAction((ActionEvent) -> {
                        rowbutton.setDisable(true);

                        int index = Integer.valueOf(rowbutton.getId());

                        // snackbar.fireEvent(new JFXSnackbar.SnackbarEvent(
                        // new JFXSnackbarLayout("Sucessfully Data Retrieve")));
                        // System.out.println(rowbutton.getId());

                        if (MainApp.equipmentLocal == null) {

                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    Utilities.showMessageDialog("Equipment not registered", "",
                                            MainApp.config("config.unit_id") + " tidak belum terdaftar dalam database");

                                    rowbutton.setDisable(false);
                                }
                            });

                        } else if (MainApp.isCanPost()) {

                            EquipmentLogStartRequest activity = new EquipmentLogStartRequest();
                            if (Utilities.isLoaderHauler()) {
                                activity.setIdActual(MainApp.actualInfo.getId());
                            } else {
                                activity.setIdOperationSupport(MainApp.actualSupport.getId());
                            }
                            activity.setIdEquipment(MainApp.equipmentLocal.getId());
                            activity.setIdOperator(MainApp.user.getId());
                            activity.setTime("current");
                            activity.setCdActivity(activityGeneral.getOutput().get(index).getCd());
                            activity.setLat(gps.getLat().toString());
                            activity.setLon(gps.getLon().toString());

                            Utilities.setActivity(activity, new Runnable() {

                                @Override
                                public void run() {
                                    rowbutton.setDisable(false);
                                }
                            });

                            /** push to kafka */
                            Utilities.producerActivity(activityGeneral.getOutput().get(index).getCd(),
                                    activityGeneral.getOutput().get(index).getName());

                        } else {
                            /**
                             * offline mode
                             */

                            OperationStatusLocal act = new OperationStatusLocal();
                            act.setIdActual(MainApp.actualInfo == null ? null : MainApp.actualInfo.getId());
                            act.setIdOperationSupport(
                                    MainApp.actualSupport == null ? null : MainApp.actualSupport.getId());
                            act.setIdOperationLoader(
                                    MainApp.actualLoader == null ? null : MainApp.actualLoader.getId());
                            act.setIdEquipment(MainApp.equipmentLocal.getId());
                            act.setIdOperator(MainApp.user.getId());
                            act.setCdActivity(activityGeneral.getOutput().get(index).getCd());
                            act.setCdTum(ConstantValues.TUM_WORKING);
                            act.setCdType(MainApp.config("config.unit_type"));
                            act.setTime(Utilities.getCurrentUnix());
                            act.setLatitude(gps.getLat());
                            act.setLongitude(gps.getLon());
                            OperationStatusLocal saved = (OperationStatusLocal) MainApp.operationStatusDB
                                    .insertIgnore(act);

                            Utilities.playSound("");

                            Utilities.setLatestActivity(saved);

                            if (MainApp.latestActivity != null) {

                                if (MainApp.latestActivity.getCdActivity() != null) {
                                    MainApp.dashboardInfo.setStatus(MainApp.latestActivity.getCdActivity().getName());
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_WORKING)) {
                                    MainApp.dashboardInfo.setStatusText("Activity");
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_DELAY)) {
                                    MainApp.dashboardInfo.setStatusText("Delay");
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_IDLE)) {
                                    MainApp.dashboardInfo.setStatusText("Idle");
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_DOWN)) {
                                    MainApp.dashboardInfo.setStatusText("Down");
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_STANDBY)) {
                                    MainApp.dashboardInfo.setStatusText("Standby");
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_ASSIGNED)) {
                                    MainApp.dashboardInfo.setStatusText("Assigned");
                                }
                                if (MainApp.latestActivity.getCdTum().getCd().equals(ConstantValues.TUM_MAINTENANCE)) {
                                    MainApp.dashboardInfo.setStatusText("Maintenance");
                                }

                                Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        String actName = MainApp.latestActivity.getCdActivity() == null ? ""
                                                : ", " + MainApp.latestActivity.getCdActivity().getName();
                                        NavigationUI.addLogActivity(MainApp.latestActivity.getCdTum().getCd(),
                                                MainApp.latestActivity.getCdTum().getName() + actName);
                                    }
                                });
                            }

                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    rowbutton.setDisable(false);
                                }
                            });

                        }

                        snackbar.fireEvent(new JFXSnackbar.SnackbarEvent(new JFXSnackbarLayout("Success")));

                        TitleBar.fire();
                    });

                    gridata.add(vboxForButtons, Col, Row);

                    Col++;

                    if (Col > 3) {
                        // Reset Column
                        Col = 0;
                        // Next Row
                        Row++;
                    }
                }

                status = "success";

            } else {

                status = "success";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void drawerAction() {

        TitleBar.setOnAction((ActionEvent evt) -> {
            TitleBar.setText(p("app.textLoading"));

            TitleBar.setDisable(true);

            navigationUI.back_dashboard(evt, new Runnable() {

                @Override
                public void run() {
                    TitleBar.setDisable(false);
                }
            });

            MainApp.clockDashboard.play();

        });

    }

}
