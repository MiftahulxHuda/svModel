package com.tura.fms.test;

import com.tura.fms.screen.models.activity_model.ActivityData;
import com.tura.fms.screen.models.user_model.UserData;
import com.tura.fms.screen.presenter.ActivityPresenter;
import com.tura.fms.screen.presenter.UserPresenter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class TestAcivity {

    private String token;
    ActivityData activityData;
    ActivityPresenter activityPresenter =new ActivityPresenter();


    private final PrintStream originalOut = System.out;

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    @After
    public void restoreStreams() {

        System.setOut(originalOut);

    }

    @Before
    public void initialize() {

        UserPresenter loginTestUnit = new UserPresenter();

        try{
            UserData userData = loginTestUnit.requestLogin("10000000", "demo","Loader",false);

            if (userData.getKeluaran() != null) {

                token = userData.getKeluaran().getToken();


                activityData = activityPresenter.getDataRestApi(token,"0050");

            }
        }catch (Exception e){

        }

        System.setOut(new PrintStream(outContent));


    }

   /* @Test
    public void cek_token() {

        System.out.print(token);

        assertEquals(outContent.toString(),"token");


    }*/



    @Test
    public void postdata() {

        String status;

        try {

            if(activityData.getOutput()!=null) {

                for (int i = 0; i < activityData.getOutput().size(); i++) {
                    System.out.print(activityData.getOutput().get(i).getName() + " , ");
                }

                status = "success";
                System.out.print(status);

            }else{
                status = "error";

                System.out.print(status);

            }

            assertEquals(outContent.toString(),status);

        }catch (Exception e){

            System.out.print("gagal mengambil database");
            assertEquals(outContent.toString(),e.getMessage());

        }




    }



}


