package com.tura.fms.test;

import com.tura.fms.screen.models.bank_model.BankModel;
import com.tura.fms.screen.models.user_model.UserData;
import com.tura.fms.screen.presenter.PostBankPresenter;
import com.tura.fms.screen.presenter.UserPresenter;
import org.junit.After;
import org.junit.Test; // for @Test
import org.junit.Before; // for @Before

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import static org.junit.Assert.*;

public class TestPosBank {

    private String token;
    BankModel bankModel;
    PostBankPresenter postBank =new PostBankPresenter();


    private final PrintStream originalOut = System.out;

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    @After
    public void restoreStreams() {

        System.setOut(originalOut);

    }

    @Before  public void initialize() {

        UserPresenter loginTestUnit = new UserPresenter();

        try{
        UserData userData = loginTestUnit.requestLogin("10000000", "demo","Loader",false);

        if (userData.getKeluaran() != null) {
            token = userData.getKeluaran().getToken();


            bankModel = postBank.getDataRestApi(token, 1, 100);
        }
        }catch (Exception e){

        }

       System.setOut(new PrintStream(outContent));


    }

    @Test
    public void cek_token() {

        System.out.print(token);

        assertEquals(outContent.toString(),"token");


    }



    @Test
    public void postdata() {

        int jumlah;

        try {

            if(bankModel.getOutput()!=null) {

                for (int i = 0; i < bankModel.getOutput().size(); i++) {
                    System.out.print(bankModel.getOutput().get(i).getName() + " , ");
                }

                jumlah = bankModel.getTotalItems();

            }else{
                jumlah = 0;
                System.out.print("gagal mengambil database");
            }

            assertEquals(outContent.toString(),"jumlah +"+jumlah);

        }catch (Exception e){
            System.out.print("gagal mengambil database");
            assertEquals(outContent.toString(),e.getMessage());
        }




    }



}
