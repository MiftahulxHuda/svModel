const mysql = require('mysql');
const moment = require('moment');
const fs = require('fs');
const async = require('async');
const unit = require('./unit.js');
const constant = require('./constant.js');
const util = require('./util');
const axios = require('axios');

const cfg = fs.readFileSync(__dirname + '/config.json').toString();
const config = JSON.parse(cfg);

const log_file = fs.createWriteStream(config.global.logdir + 'autotest' + moment().format('YYYYMMDD') + '.log', { flags: 'w' });
const log_stdout = process.stdout;

let connMysql = null;
let units = [];

const tmpToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImNiOWU5YzA4LTQyYmYtMTFlYS04MDZhLTAyNDJhYzEyMDAwMiIsImVtYWlsIjoiYWRtaW5pc3RyYXRvckBtYWlsLmNvbSIsIm5hbWUiOiJBZG1pbmlzdHJhdG9yIiwiY2Rfcm9sZSI6eyJjZCI6IjAwMzAwMCIsImNkX3NlbGYiOiIwMDMiLCJuYW1lIjoiQWRtaW5pc3RyYXRvciIsImpzb24iOnsiY2QiOiIwMDMwMDAiLCJzZXEiOm51bGwsIm5hbWUiOiJOT05FIiwic2x1ZyI6bnVsbCwidGFnXzEiOm51bGwsInRhZ18yIjpudWxsLCJ0YWdfMyI6bnVsbCwiY2Rfc2VsZiI6IjAwMyIsImlzX2ZpeGVkIjoxLCJpc19kZWxldGVkIjowfX0sImNkX2RlcGFydG1lbnQiOnsiY2QiOiIwMDQwMDEiLCJjZF9zZWxmIjoiMDA0IiwibmFtZSI6IlN5c3RlbSIsImpzb24iOnsiY2QiOiIwMDQwMDEiLCJzZXEiOm51bGwsIm5hbWUiOiJTeXN0ZW0iLCJzbHVnIjpudWxsLCJ0YWdfMSI6bnVsbCwidGFnXzIiOm51bGwsInRhZ18zIjpudWxsLCJjZF9zZWxmIjoiMDA0IiwiaXNfZml4ZWQiOjEsImlzX2RlbGV0ZWQiOjB9fSwic3RhZmZfaWQiOiIwMDAwMDAiLCJmaW5nZXJwcmludF9pZCI6MCwic29ja2V0X2lkIjpudWxsLCJmaWxlX3BpY3R1cmUiOm51bGwsImVudW1fYXZhaWxhYmlsaXR5Ijo0LCJpc19hdXRoZW50aWNhdGVkIjoxLCJfb3JnIjp7ImlkIjoiMSIsIm5hbWUiOiJQVCBTYXRyaWEgQmFoYW5hIFNhcmFuYSIsImFiYnIiOiJTQlMiLCJqc29uIjpudWxsfSwiX3JvbGUiOnsiY2QiOiIwMDMwMDAiLCJjZF9zZWxmIjoiMDAzIiwibmFtZSI6IkFkbWluaXN0cmF0b3IiLCJqc29uIjp7ImNkIjoiMDAzMDAwIiwic2VxIjpudWxsLCJuYW1lIjoiTk9ORSIsInNsdWciOm51bGwsInRhZ18xIjpudWxsLCJ0YWdfMiI6bnVsbCwidGFnXzMiOm51bGwsImNkX3NlbGYiOiIwMDMiLCJpc19maXhlZCI6MSwiaXNfZGVsZXRlZCI6MH19LCJfZGVwYXJ0bWVudCI6eyJjZCI6IjAwNDAwMSIsImNkX3NlbGYiOiIwMDQiLCJuYW1lIjoiU3lzdGVtIiwianNvbiI6eyJjZCI6IjAwNDAwMSIsInNlcSI6bnVsbCwibmFtZSI6IlN5c3RlbSIsInNsdWciOm51bGwsInRhZ18xIjpudWxsLCJ0YWdfMiI6bnVsbCwidGFnXzMiOm51bGwsImNkX3NlbGYiOiIwMDQiLCJpc19maXhlZCI6MSwiaXNfZGVsZXRlZCI6MH19LCJpYXQiOjE1ODk4NDc2NjUsImV4cCI6MTkwNTIwNzY2NX0.g3RTSvgqyE1RYBkQv7elsh1cabE6zNnTIPUXey4XiLw';

console.log = function (d, e, f) {
    const t = moment().toLocaleString();
    log_file.write(t + '\t' + d + '\t' + e + '\n');
    // log_stdout.write(t + '\t' + d + '\t' + e + '\n');
};

async.waterfall([

    (cb) => {
        /**
         * create dummy if not exist
         */
        if (util.getCurrentShift() === constant.SHIFT_DAY) {
            axios.get(config.APIs.baseUrl + '/operation/actual', {
                params: {
                    date: moment().startOf('day').unix()
                },
                headers: {
                    'Authorization': `Bearer ${tmpToken}`
                }
            }).then((result) => {
                if (result.data) {
                    if (result.data.response === 'success') {
                        const co = result.data.output.length;
                        if (co === 0) {

                            // dummy actual
                            axios.post(config.APIs.baseUrl + '/dummy/plan-actual-new', {
                                tanggal: moment().format('YYYY-MM-DD')
                            }, {
                                headers: {
                                    'Authorization': `Bearer ${tmpToken}`
                                }
                            }).then((result) => { });

                            // dummy support
                            axios.post(config.APIs.baseUrl + '/dummy/operation-support', {
                                tanggal: moment().format('YYYY-MM-DD')
                            }, {
                                headers: {
                                    'Authorization': `Bearer ${tmpToken}`
                                }
                            }).then((result) => { });

                            // dummy crew
                            axios.post(config.APIs.baseUrl + '/dummy/crew', {
                                tanggal: moment().format('YYYY-MM-DD')
                            }, {
                                headers: {
                                    'Authorization': `Bearer ${tmpToken}`
                                }
                            }).then((result) => { });

                            cb();
                        } else {
                            cb();
                        }
                    } else {
                        cb();
                    }
                } else {
                    cb();
                }
            });
        } else {
            cb();
        }

    },

    (cb) => {
        /**
         * connect to MySQL
         */
        console.log('-- connect to mysql');
        connMysql = mysql.createConnection(config.mysqlConfig);
        connMysql.connect(function (err) {
            if (err) {
                console.error('error connecting: ' + err.stack);
                throw err;
            }
            cb();
        });
    },

    (cb) => {
        /**
         * unit loader sebanyak 3 unit
         * setiap loader memiliki 3 unit hauler
         */
        const defaultRoute = Math.floor(Math.random() * 3) + 1;
        const routeList = [defaultRoute];
        for (let l of [1, 2, 3]) {
            if (l !== defaultRoute) {
                routeList.push(l);
            }
        }

        const routeInfo = {};
        let index = 0;
        for (let un of Object.values(config.units.loaders)) {
            const route = fs.readFileSync(__dirname + '/route/' + routeList[index] + '.json').toString();

            const loaderUnit = require('./unit.js')(connMysql, config.kafkaServer, un, constant.UNIT_LOADER, JSON.parse(route), config.APIs.baseUrl);
            loaderUnit.run();

            units.push(loaderUnit);
            routeInfo[un.name] = routeList[index];
            index++;
        }

        const curShift = util.getCurrentShift();
        for (let un of Object.values(config.units.haulers)) {
            if (un.shift !== curShift) {
                continue;
            }
            const routeA = fs.readFileSync(__dirname + '/route/' + routeInfo[un.loader] + 'a.json').toString();
            const routeB = fs.readFileSync(__dirname + '/route/' + routeInfo[un.loader] + 'b.json').toString();

            const haulerUnit = require('./unit.js')(connMysql, config.kafkaServer, un, constant.UNIT_HAULER, {
                a: JSON.parse(routeA),
                b: JSON.parse(routeB)
            }, config.APIs.baseUrl);
            haulerUnit.run();

            units.push(haulerUnit);
        }

        cb();
    },

    (cb) => {

        let increment = 0;

        const interval = setInterval(function () {

            const offline = units.filter(un => un.isSignIn === false && un.unitType === constant.UNIT_HAULER);

            console.log('OFFLINE UNIT', offline.length);

            if (offline.length === 9 && increment > 10) {
                // exit auto test mining process
                console.log('EXIT AUTO TEST MINING');
                return process.exit(22);
            }

            increment++;

        }, 5000);

        cb();
    }


], (res) => {

    console.log('Realtime mining dummy test is running');

});