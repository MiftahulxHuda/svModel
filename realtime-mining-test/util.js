const constant = require('./constant');
const moment = require('moment');

module.exports.util = {

    /**
     * Berkaitan dengan tabel equipment_log_start
     * untuk menentukan cd_tum
     * @param cdSelf equipment activity (CD_SELF)
     * @param cd equipment activity (CD)
     */
    getCdTUMFromCdSelf(cdSelf, cd) {
        if (cdSelf.startsWith('ACTIVITY') || cdSelf.startsWith('GENERAL')) {
            return constant.TUM_WORKING_TIME;
        } else if (cdSelf.startsWith('DELAY')) {
            return constant.TUM_DELAY_TIME;
        } else if (cdSelf.startsWith('IDLE')) {
            return constant.TUM_IDLE_TIME;
        } else if (cdSelf.startsWith('STANDBY')) {
            return constant.TUM_STANDBY_TIME;
        } else if (cdSelf.startsWith('M') || cd === 'B3') { // wait mechanic
            return constant.TUM_DOWN_TIME;
        } else if (cdSelf.startsWith('B')) {
            return constant.TUM_MAINTENANCE_TIME;
        } else {
            return null;
        }
    },

    generateWONumber() {
        return 'WO-' + moment().unix();
    },

    getYesterdayTime() {
        const start = moment().subtract(1, 'days').startOf('day').unix();
        const end = moment().subtract(1, 'days').endOf('day').unix();
        return {
            start: start,
            end: end
        };
    },

    isBefore6() {
        const naw = moment().unix();
        const tm6 = moment().set({ hour: 6, minute: 0 }).unix();
        if (naw < tm6) {
            return true;
        } else {
            return false;
        }
    },

    getCurrentShift() {
        const naw = moment().unix();
        const tm0 = moment().set({ hour: 0, minute: 0 }).unix();
        const tm6 = moment().set({ hour: 6, minute: 0 }).unix();
        const tm18 = moment().set({ hour: 18, minute: 0 }).unix();

        if (naw > tm0 && naw < tm6) {
            return constant.SHIFT_NIGHT;
        }
        if (naw > tm6 && naw < tm18) {
            return constant.SHIFT_DAY;
        }
        if (naw > tm18) {
            return constant.SHIFT_NIGHT;
        }
        return null;
    },

    getCurrentRoster() {
        const shift = this.getCurrentShift();
        if (shift === constant.SHIFT_NIGHT) {
            return constant.ROSTER_NIGHT;
        } else if (shift === constant.SHIFT_DAY) {
            return constant.ROSTER_DAY;
        } else {
            return '';
        }
    },

    getShiftByTime(naw) {
        const tm0 = moment.unix(naw).set({ hour: 0, minute: 0 }).unix();
        const tm6 = moment.unix(naw).set({ hour: 6, minute: 0 }).unix();
        const tm18 = moment.unix(naw).set({ hour: 18, minute: 0 }).unix();

        if (naw > tm0 && naw < tm6) {
            return constant.SHIFT_NIGHT;
        }
        if (naw > tm6 && naw < tm18) {
            return constant.SHIFT_DAY;
        }
        if (naw > tm18) {
            return constant.SHIFT_NIGHT;
        }
        return null;
    },

    isToday(tm) {
        const tm0 = moment().set({ hour: 0, minute: 0 }).unix();
        const tm23 = moment().set({ hour: 23, minute: 59, second: 59 }).unix();
        if (tm >= tm0 && tm < tm23) {
            return true;
        } else {
            return false;
        }
    },

    getStartEndTime() {
        const start = moment().startOf('day').unix();
        const end = moment().endOf('day').unix();
        return {
            start: start,
            end: end
        };
    },

    getShiftDayStartTime() {
        let tm;
        if (this.isBefore6()) {
            tm = moment().subtract(1, 'days').startOf('day').set({ hour: 6 }).unix();
        } else {
            tm = moment().startOf('day').set({ hour: 6 }).unix();
        }
        return tm;
    },

    getShiftNightStartTime() {
        let tm;
        if (this.isBefore6()) {
            tm = moment().subtract(1, 'days').startOf('day').set({ hour: 18 }).unix();
        } else {
            tm = moment().startOf('day').set({ hour: 18 }).unix();
        }
        return tm;
    },

    getShiftNightEndTime() {
        let tm;
        if (this.isBefore6()) {
            tm = moment().startOf('day').set({ hour: 6 }).unix();
        } else {
            tm = moment().add(1, 'days').startOf('day').set({ hour: 6 }).unix();
        }
        return tm;
    },

    getDateOfShift() {
        let tm;
        if (this.isBefore6()) {
            tm = moment().subtract(1, 'days').startOf('day').unix();
        } else {
            tm = moment().startOf('day').unix();
        }
        return tm;
    },

    getRad(x, y) {
        const deg = Math.atan(x / y);
        return deg;
    },

    translateTimestamp(tmt) {
        if (tmt === 'current') {
            /** mengambil waktu saat ini */
            return moment().unix();

        } else if (tmt === 'startShiftDay') {
            /** start shift day pada saat ini */
            return this.getShiftDayStartTime();

        } else if (tmt === 'endShiftDay') {
            /** end shift day pada saat ini */
            return this.getShiftNightStartTime();

        } else if (tmt === 'startShiftNight') {
            /** start shift night pada saat ini, call sebelum jam 24 */
            return this.getShiftNightStartTime();

        } else if (tmt === 'endShiftNight') {
            /** end shift night kemarin, call antara jam 0 sampe 6 */
            return this.getShiftNightEndTime();

        } else if (tmt === 'startDay') {
            return moment().startOf('day').unix();

        } else if (tmt === 'endDay') {
            return moment().endOf('day').unix();

        } else if (tmt === 'startOfShift') {
            /** get start of shift (day/night), call bebas */
            const cur = this.getCurrentShift();
            if (cur === constant.SHIFT_DAY) {
                return this.getShiftDayStartTime();
            } else {
                return this.getShiftNightStartTime();
            }

        } else if (tmt === 'endOfShift') {
            /** get end of shift (day/night), call bebas */
            const cur = this.getCurrentShift();
            if (cur === constant.SHIFT_DAY) {
                return this.getShiftNightStartTime();
            } else {
                return this.getShiftNightEndTime();
            }

        } else if (tmt === 'dateOfShift') {
            /** mengambil tanggal dari shift saat ini */
            return this.getDateOfShift();

        } else {
            return null;

        }
    },

    getBCMFromTonnage(tonnage, density) {
        return tonnage / density;
    },

    getTonnageFromBCM(bcm, density) {
        return bcm * density;
    },

    /**
     * @param payload payload from loadcell device
     * @param capacityReal depend on type of material
     */
    getTonnage(payload, capacityReal) {
        return payload - capacityReal;
    },

    /**
     * Distance from lat lon in meter
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     */
    getDistance(lat1, lon1, lat2, lon2) {
        const R = 6371; // Radius of the earth in km
        const dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
        const dLon = this.deg2rad(lon2 - lon1);
        const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        const d = R * c; // Distance in km
        const dM = d * 1000; // Distance in meter
        return dM;
    },

    deg2rad(deg) {
        return deg * (Math.PI / 180)
    }

};


/**
 * Distance from lat lon in meter
 * @param lat1
 * @param lon1
 * @param lat2
 * @param lon2
 */

module.exports.getDistance = function (lat1, lon1, lat2, lon2) {
    const R = 6371; // Radius of the earth in km
    const dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
    const dLon = this.deg2rad(lon2 - lon1);
    const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const d = R * c; // Distance in km
    const dM = d * 1000; // Distance in meter
    return dM;
}

module.exports.mysqlQuery = (my, sql) => {
    return new Promise((resolve, reject) => {
        my.query(sql, function (err, result, fields) {
            if (err) reject(err);
            resolve(result);
        });
    });
};

module.exports.mongoAggregate = (mo, opt) => {
    return new Promise((resolve, reject) => {
        const mongo = mo.db(opt.database).collection(opt.collection).aggregate(opt.option);
        mongo.toArray(function (err, result) {
            if (err) reject(err);
            resolve(result);
        });
    });
};

module.exports.getCurrentShift = () => {
    const naw = moment().unix();
    const tm0 = moment().set({ hour: 0, minute: 0 }).unix();
    const tm6 = moment().set({ hour: 6, minute: 0 }).unix();
    const tm18 = moment().set({ hour: 18, minute: 0 }).unix();

    if (naw >= tm0 && naw < tm6) {
        return constant.SHIFT_NIGHT;
    }
    if (naw >= tm6 && naw < tm18) {
        return constant.SHIFT_DAY;
    }
    if (naw >= tm18) {
        return constant.SHIFT_NIGHT;
    }
    return null;
};

module.exports.isBefore6 = () => {
    const naw = moment().unix();
    const tm6 = moment().set({ hour: 6, minute: 0 }).unix();
    if (naw < tm6) {
        return true;
    } else {
        return false;
    }
};

module.exports.getShiftDayStartTime = () => {
    let tm;
    if (this.isBefore6()) {
        tm = moment().subtract(1, 'days').startOf('day').set({ hour: 6 }).unix();
    } else {
        tm = moment().startOf('day').set({ hour: 6 }).unix();
    }
    return tm;
};

module.exports.getShiftNightStartTime = () => {
    let tm;
    if (this.isBefore6()) {
        tm = moment().subtract(1, 'days').startOf('day').set({ hour: 18 }).unix();
    } else {
        tm = moment().startOf('day').set({ hour: 18 }).unix();
    }
    return tm;
};

module.exports.getYesterdayTime = () => {
    const start = moment().subtract(1, 'days').startOf('day').unix();
    const end = moment().subtract(1, 'days').endOf('day').unix();
    return {
        start: start,
        end: end
    };
};