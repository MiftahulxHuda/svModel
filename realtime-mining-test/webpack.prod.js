const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const nodeExternals = require('webpack-node-externals');

module.exports = merge(common, {
    mode: 'production',
    target: 'node',
    externals: [nodeExternals()],
    optimization: {
        minimize: true
    },
    node: {
        __dirname: false,
        fs: 'empty',
        global: true,
        crypto: 'empty',
        tls: 'empty',
        net: 'empty',
        dns: 'empty',
        module: false,
        process: true,
        clearImmediate: false,
        setImmediate: false
    }
});