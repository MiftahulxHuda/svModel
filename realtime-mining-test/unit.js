const util = require('./util');
const constant = require('./constant');
const kafka = require('node-rdkafka');
const os = require('os');
const moment = require('moment');
const axios = require('axios');
const datatype = require('./datatype');
const md5 = require('md5');
const async = require('async');

module.exports = (mysql, kafkaSrv, unitInfo, unitType, route, baseAPI) => {
    const module = {};

    module.equipment = null;

    module.producer = null;

    module.consumer = null;

    module.pusher = null;

    module.currentActivity = null;

    module.currentTum = null;

    module.haulerState = null;

    module.isSignIn = false;

    module.token = null;

    module.latestGps = { lat: 0, lon: 0 };

    module.actual_id = '';

    module.actual_loader_id = '';

    module.actual_support_id = '';

    module.actual = null;

    module.actual_loader = null;

    module.user = null;

    module.hmStart = 0;

    module.distance = 0;

    module.area = null;

    module.routeIndex = null;

    module.stopUnit = false;

    module.tonnage = 0;

    module.tonnageTimbangan = 0;

    module.tonnageUjiPetik = 0;

    module.tonnageBucket = 0;

    module.loadWeight = 0;

    module.emptyWeight = 0;

    module.sholatDzuhur = false;

    module.sholatAsar = false;

    module.sholatIsya = false;

    module.unitInfo = unitInfo;

    module.unitType = unitType;

    module.getEquipment = async () => {
        const equ = await util.mysqlQuery(mysql, "SELECT * FROM equipment WHERE name='" + unitInfo.name + "'");
        if (equ.length > 0) {
            module.equipment = equ[0];
        } else {
            await mysql.close();
            throw Error('equipment not found');
        }
    }

    module.initKafkaPusher = async () => {
        module.producer = new kafka.Producer({
            'client.id': module.equipment.id + '-producer',
            'metadata.broker.list': kafkaSrv,

            'compression.codec': 'lz4',

            'group.id': module.equipment.id + '-producer',
            'enable.auto.commit': false,
            'security.protocol': 'SASL_PLAINTEXT',
            'sasl.mechanisms': 'PLAIN',
            'sasl.username': 'coba',
            'sasl.password': 'cobacoba',
            'dr_cb': true
        });

        module.producer.on('event.log', function (log) {
            // console.log('event-log', log);
        });

        module.producer.on('event.error', function (err) {
            console.error('Error from producer');
            console.error(err);
        });

        module.producer.on('ready', function (arg) {
            console.log('producer ready', unitInfo.name);
        });

        module.producer.on('disconnected', function (arg) {
            console.log('producer disconnected. ' + JSON.stringify(arg));
        });

        module.producer.connect();

        module.pusher = require('./pusher')(module.producer);
    }

    module.initKafkaConsumer = async () => {
        module.consumer = new kafka.KafkaConsumer({
            'client.id': module.equipment.id + '-consumer',
            // 'debug': 'all',
            'metadata.broker.list': kafkaSrv,
            'group.id': module.equipment.id + '-consumer',
            'enable.auto.commit': true,
            'rebalance_cb': true,
            'security.protocol': 'SASL_PLAINTEXT',
            'sasl.mechanisms': 'PLAIN',
            'sasl.username': 'coba',
            'sasl.password': 'cobacoba',
        });

        module.consumer.on('event.log', function (log) {
            console.log('event-log', log);
        });

        module.consumer.on('event.error', function (err) {
            console.error('Error from consumer', err);
        });

        module.consumer.on('ready', function (arg) {

            console.log('consumer ready ', unitInfo.name);

            if (unitType === constant.UNIT_HAULER) {
                module.consumer.subscribe(['cycle', 'data', 'notification', 'command']);
            } else if (unitType === constant.UNIT_LOADER) {
                module.consumer.subscribe(['cycle', 'data', 'notification', 'command', 'distance']);
            } else {
                module.consumer.subscribe(['data', 'notification', 'command']);
            }

            /**
             * tidak dilakukan disini, karena blocking
             */
            // module.consumer.consume(5, (err, message) => {
            //     console.log(err, message);
            // });
        });

        module.consumer.on('data', function (m) {
            const data = JSON.parse(m.value.toString());

            console.log('consumer accept data', unitInfo.name, m.topic);

            module.consumerHandler(m.topic, data);

            module.consumer.commit(m);
        });

        module.consumer.on('disconnected', function (arg) {
            console.log('consumer disconnected. ');
        });

        module.consumer.connect();
    }

    module.consumerHandler = (topic, data) => {
        if (topic === constant.TOPIC_CYCLE) {
            module.handleCycle(data);
        }
    }

    module.handleCycle = (data) => {
        const date = data.date;
        const type = data.type;
        const sender = data.sender;
        const sender_name = data.sender_nameid
        const destination = data.destination;
        const message = data.message;
        const bucket_count = data.bucket_count;

        if (destination === module.equipment.id || destination === constant.KAFKA_DESTINATION_ALL) {
            if (unitType === constant.UNIT_HAULER) {

                if (message === constant.CYCLE_FIRST_BUCKET) {

                    if (module.actual) {

                        console.log('handle cycle', 'first bucket', unitInfo.name);

                        module.haulerState = constant.HAULER_STATE_LOADING;
                        module.bucketCount = 1;

                        const materialType = module.actual.__cd_material__.type;

                        const capatityReal = module.equipment.capacity_real;
                        const payload = Math.random() * 10;
                        const density = module.actual.__cd_material__.density;

                        if (materialType === constant.MATERIAL_COAL) {
                            module.tonnage = payload - capatityReal;
                            module.tonnageUjiPetik = module.equipment.capacity_labs_2 ? module.equipment.capacity_labs_2 : Math.floor(Math.random() * 25);
                            const bcm = module.actual.__id_loader__.id_equipment_category ? module.actual.__id_loader__.id_equipment_category.bucket_capacity : 0;
                            module.tonnageBucket = util.util.getTonnageFromBCM(bcm, density);

                        } else if (materialType === constant.MATERIAL_OB) {
                            module.tonnage = util.util.getBCMFromTonnage(payload - capatityReal, density);
                            module.tonnageUjiPetik = module.equipment.capacity_labs ? module.equipment.capacity_labs : Math.floor(Math.random() * 25);
                            const bcm = module.actual.__id_loader__.id_equipment_category ? module.actual.__id_loader__.id_equipment_category.bucket_capacity : 0;
                            module.tonnageBucket = bcm;

                        } else if (materialType === constant.MATERIAL_MUD) {
                            module.tonnage = util.util.getBCMFromTonnage(payload - capatityReal, density);
                            module.tonnageUjiPetik = module.equipment.capacity_labs_3 ? module.equipment.capacity_labs_3 : Math.floor(Math.random() * 25);
                            const bcm = module.actual.__id_loader__.id_equipment_category ? module.actual.__id_loader__.id_equipment_category.bucket_capacity : 0;
                            module.tonnageBucket = bcm;

                        }
                    }

                }

                if (message === constant.CYCLE_FULL_BUCKET) {

                    if (module.actual) {
                        console.log('handle cycle', 'full bucket', unitInfo.name);

                        module.haulerState = constant.HAULER_STATE_FULL_TRAVEL;
                        module.bucketCount = bucket_count;

                        const materialType = module.actual.__cd_material__.type;

                        const capatityReal = module.equipment.capacity_real;
                        const payload = Math.random() * 10;
                        const density = module.actual.__cd_material__.density;

                        // random saja
                        module.tonnageTimbangan = Math.floor(Math.random() * 11) + 2;

                        if (materialType === constant.MATERIAL_COAL) {
                            module.tonnage = payload - capatityReal;
                            module.tonnageUjiPetik = module.equipment.capacity_labs_2 ? module.equipment.capacity_labs_2 : Math.floor(Math.random() * 25);
                            const bcm = module.actual.__id_loader__.id_equipment_category ? module.actual.__id_loader__.id_equipment_category.bucket_capacity : 0;
                            module.tonnageBucket = util.util.getTonnageFromBCM(bcm, density);

                        } else if (materialType === constant.MATERIAL_OB) {
                            module.tonnage = util.util.getBCMFromTonnage(payload - capatityReal, density);
                            module.tonnageUjiPetik = module.equipment.capacity_labs ? module.equipment.capacity_labs : Math.floor(Math.random() * 25);
                            const bcm = module.actual.__id_loader__.id_equipment_category ? module.actual.__id_loader__.id_equipment_category.bucket_capacity : 0;
                            module.tonnageBucket = bcm;

                        } else if (materialType === constant.MATERIAL_MUD) {
                            module.tonnage = util.util.getBCMFromTonnage(payload - capatityReal, density);
                            module.tonnageUjiPetik = module.equipment.capacity_labs_3 ? module.equipment.capacity_labs_3 : Math.floor(Math.random() * 25);
                            const bcm = module.actual.__id_loader__.id_equipment_category ? module.actual.__id_loader__.id_equipment_category.bucket_capacity : 0;
                            module.tonnageBucket = bcm;

                        }
                    }

                    /**
                     * kembalikan posisi GPS
                     */
                    module.routeIndex = 0;

                    // set full weight, random saja
                    module.loadWeight = Math.floor(Math.random() * 11) + module.emptyWeight;
                }

            } else if (unitType === constant.UNIT_LOADER) {

                if (message === constant.CYCLE_ARRIVE_LOADING) {
                    console.log('handle cycle', 'arrive loading', unitInfo.name);

                    /**
                     * first bucket
                     */
                    setTimeout(function () {

                        if (module.user) {
                            // belum login tapi sudah dapat socket

                            let ld = Object.assign({}, datatype.logDetail);
                            ld.date = 'current';
                            ld.id_actual = module.actual_id;
                            ld.cd_time = constant.TIME_CYCLE_FIRST_BUCKET;
                            ld.id_loader = module.equipment.id;
                            ld.id_equipment = sender;
                            ld.id_operator = module.user.id;
                            ld.cd_equipment_type = unitType;
                            ld.id_location = module.actual_loader.id_location.id;
                            ld.bucket_count = 1;
                            ld.latitude = module.latestGps.lat;
                            ld.longitude = module.latestGps.lon;

                            module.postLogDetail(ld, (result) => {

                                let topicCycle = Object.assign({}, datatype.dataTopicCycle);
                                topicCycle.date = moment().unix();
                                topicCycle.type = unitType === constant.UNIT_HAULER ? 'hauler' : 'loader';
                                topicCycle.sender = module.equipment.id;
                                topicCycle.sender_name = unitInfo.name;
                                topicCycle.destination = sender;
                                topicCycle.message = constant.CYCLE_FIRST_BUCKET;

                                module.pusher.send(constant.TOPIC_PRODUCE_CYCLE, module.token, topicCycle);

                            });
                        }
                    }, Math.floor(Math.random() * 3) * 1000);

                    /**
                     * full bucket
                     */
                    setTimeout(function () {

                        if (module.user) {
                            // belum login tapi sudah dapat socket

                            let ld = Object.assign({}, datatype.logDetail);
                            ld.date = 'current';
                            ld.id_actual = module.actual_id;
                            ld.cd_time = constant.TIME_CYCLE_FULL_BUCKET;
                            ld.id_loader = module.equipment.id;
                            ld.id_equipment = sender;
                            ld.id_operator = module.user.id;
                            ld.cd_equipment_type = unitType;
                            ld.id_location = module.actual_loader.id_location.id;
                            ld.bucket_count = Math.floor(Math.random() * 5) + 5;
                            ld.latitude = module.latestGps.lat;
                            ld.longitude = module.latestGps.lon;

                            module.postLogDetail(ld, (result) => {

                                let topicCycle = Object.assign({}, datatype.dataTopicCycle);
                                topicCycle.date = moment().unix();
                                topicCycle.type = unitType === constant.UNIT_HAULER ? 'hauler' : 'loader';
                                topicCycle.sender = module.equipment.id;
                                topicCycle.sender_name = unitInfo.name;
                                topicCycle.destination = sender;
                                topicCycle.message = constant.CYCLE_FULL_BUCKET;
                                topicCycle.bucket_count = ld.bucket_count;

                                module.pusher.send(constant.TOPIC_PRODUCE_CYCLE, module.token, topicCycle);

                            });

                        }

                    }, (Math.floor(Math.random() * 3) * 1000) + 3000);

                }

                if (message === constant.CYCLE_FINISH_DUMP) {

                }

            }

        }

    }

    module.computeCPU = () => {
        const cpus = os.cpus();

        let cpu = cpus[0], total = 0;

        for (let type in cpu.times) {
            total += cpu.times[type];
        }

        return {
            user: Math.round(100 * cpu.times['user'] / total),
            sys: Math.round(100 * cpu.times['sys'] / total)
        }
    }

    module.simulatePray = (jam, sholat) => {
        const currentHour = moment().hour();

        let status = false;

        if (sholat === 'dzuhur') {
            status = module.sholatDzuhur;
        } else if (sholat === 'asar') {
            status = module.sholatAsar;
        } else if (sholat === 'isya') {
            status = module.sholatIsya;
        }

        if (status === false && currentHour > jam && module.isSignIn && (module.haulerState === constant.HAULER_STATE_EMPTY_TRAVEL || module.haulerState === constant.HAULER_STATE_FULL_TRAVEL)) {
            module.stopUnit = true;

            console.log(unitInfo.name, ' take standby pray dzuhur');

            let pray = Object.assign({}, datatype.activity);
            if (unitType === constant.UNIT_HAULER) {
                pray.id_actual = module.actual_id;
            } else if (unitType === constant.UNIT_LOADER) {
                pray.id_actual = module.actual_id;
                pray.id_operation_loader = module.actual_loader_id;
            } else {
                pray.id_operation_support = module.actual_support_id;
            }
            pray.id_equipment = module.equipment.id;
            pray.id_operator = module.user.id;
            pray.time = 'current';
            pray.cd_activity = constant.CD_ACTIVITY_PRAY;
            module.setActivity(pray, (res) => {
                module.currentActivity = res.cd_activity;
                module.currentTum = res.cd_tum;

                if (sholat === 'dzuhur') {
                    module.sholatDzuhur = true;
                } else if (sholat === 'asar') {
                    module.sholatAsar = true;
                } else if (sholat === 'isya') {
                    module.sholatIsya = true;
                }

                setTimeout(function () {

                    let work = Object.assign({}, datatype.activity);
                    if (unitType === constant.UNIT_HAULER) {
                        work.id_actual = module.actual_id;
                    } else if (unitType === constant.UNIT_LOADER) {
                        work.id_actual = module.actual_id;
                        work.id_operation_loader = module.actual_loader_id;
                    } else {
                        work.id_operation_support = module.actual_support_id;
                    }
                    work.id_equipment = module.equipment.id;
                    work.id_operator = module.user.id;
                    work.time = 'current';
                    if (unitType === constant.UNIT_HAULER) {
                        work.cd_activity = module.actual.__cd_operation__.cd;
                    } else if (unitType === constant.UNIT_LOADER) {
                        work.cd_activity = module.actual_loader.cd_operation.cd;
                    }
                    module.setActivity(work, (res) => {
                        module.currentActivity = res.cd_activity;
                        module.currentTum = res.cd_tum;

                        module.stopUnit = false;

                        console.log(unitInfo.name, ' back to work');
                    });

                }, 10000 + (Math.floor(Math.random() * 10) * 1000));
            });

        }
    }

    module.initGpsPusher = () => {
        const interval = setInterval(function () {
            console.log(module.equipment.name, module.currentActivity, module.currentTum, module.haulerState, 'send gps');

            /**
             * simulate standby time
             */
            module.simulatePray(12, 'dzuhur');
            module.simulatePray(15, 'asar');
            module.simulatePray(19, 'isya');

            /**
             * 1 jam sebelum shift berakhir logout & exit
             */
            const currentHour = moment().hour();
            if (util.getCurrentShift() === constant.SHIFT_DAY && module.haulerState === constant.HAULER_STATE_EMPTY_TRAVEL && module.isSignIn) {
                if (currentHour === 17) {
                    // logour & exit
                    axios.put(baseAPI + '/user/auth', {
                        staff_id: unitInfo.operator,
                        pwd: md5('sbsjaya'),
                        state: 'logout',
                        equipment_id: module.equipment.id,
                        unit_type: unitType,
                        latitude: module.latestGps.lat,
                        longitude: module.latestGps.lon
                    }).then((result) => {
                        module.isSignIn = false;
                    });
                }
            } else if (util.getCurrentShift() === constant.SHIFT_NIGHT && module.haulerState === constant.HAULER_STATE_EMPTY_TRAVEL && module.isSignIn) {
                if (currentHour === 5) {
                    // logour & exit
                    axios.put(baseAPI + '/user/auth', {
                        staff_id: unitInfo.operator,
                        pwd: md5('sbsjaya'),
                        state: 'logout',
                        equipment_id: module.equipment.id,
                        unit_type: unitType,
                        latitude: module.latestGps.lat,
                        longitude: module.latestGps.lon
                    }).then((result) => {
                        module.isSignIn = false;
                    });
                }
            }


            const date = moment().unix();
            const cpu = module.computeCPU();

            /**
             * HM dan distance
             * bertambah ketika user sudah login
             */
            if (module.isSignIn) {
                module.hmStart++;
                module.distance++;
                if (module.routeIndex === null) {
                    module.routeIndex = 0;
                }
            }

            /**
             * GPS berjalan ketika tum working
             */
            if (module.currentTum) {
                if (module.currentTum.cd === constant.TUM_WORKING_TIME && module.stopUnit === false) {
                    module.routeIndex++;
                }
            }

            let latitude = '0.0';
            let longitude = '0.0';

            /**
             * increment tum
             */
            if (module.routeIndex !== null) {

                if (unitType === constant.UNIT_LOADER) {
                    if (route.coordinates[module.routeIndex]) {
                        latitude = route.coordinates[module.routeIndex][1];
                        longitude = route.coordinates[module.routeIndex][0];

                    } else {
                        /**
                         * jalur loader hanya berputar2, kembalikan lagi ke posisi awal
                         */
                        module.routeIndex = -1;
                    }

                } else if (unitType === constant.UNIT_HAULER) {

                    if (module.haulerState === constant.HAULER_STATE_FULL_TRAVEL) {

                        if (route.b.coordinates[module.routeIndex]) {
                            latitude = route.b.coordinates[module.routeIndex][1];
                            longitude = route.b.coordinates[module.routeIndex][0];

                        } else {
                            latitude = module.latestGps.lat;
                            longitude = module.latestGps.lon;

                        }

                        if (module.routeIndex >= route.b.coordinates.length - 6) {
                            // arrive dumping

                            console.log('post arrive dumping', unitInfo.name);

                            module.haulerState = constant.HAULER_STATE_SPOTING_AT_DUMP;

                            let ld = Object.assign({}, datatype.logDetail);
                            ld.date = 'current';
                            ld.id_actual = module.actual_id;
                            ld.cd_time = constant.TIME_CYCLE_ARRIVE_DUMPING;
                            ld.id_equipment = module.equipment.id;
                            ld.id_operator = module.user.id;
                            ld.cd_equipment_type = unitType;
                            ld.id_location = module.actual.__id_location__.id;
                            ld.id_loader = null;
                            ld.latitude = latitude;
                            ld.longitude = longitude;

                            module.postLogDetail(ld, (result) => {

                                let topicCycle = Object.assign({}, datatype.dataTopicCycle);
                                topicCycle.date = moment().unix();
                                topicCycle.type = unitType === constant.UNIT_HAULER ? 'hauler' : 'loader';
                                topicCycle.sender = module.equipment.id;
                                topicCycle.sender_name = unitInfo.name;
                                topicCycle.destination = module.actual.__id_loader__.id;
                                topicCycle.message = constant.CYCLE_ARRIVE_DUMPING;

                                module.pusher.send(constant.TOPIC_PRODUCE_CYCLE, module.token, topicCycle);

                            });
                        }

                    } else if (module.haulerState === constant.HAULER_STATE_EMPTY_TRAVEL) {

                        latitude = route.a.coordinates[module.routeIndex][1];
                        longitude = route.a.coordinates[module.routeIndex][0];

                        if (module.routeIndex >= route.a.coordinates.length - 6) {
                            // arrive front / loading

                            console.log('post arrive loading', unitInfo.name);

                            module.haulerState = constant.HAULER_STATE_QUEUE_AT_LOADING;

                            let ld = Object.assign({}, datatype.logDetail);
                            ld.date = 'current';
                            ld.id_actual = module.actual_id;
                            ld.cd_time = constant.TIME_CYCLE_ARRIVE_LOADING;
                            ld.id_equipment = module.equipment.id;
                            ld.id_operator = module.user.id;
                            ld.cd_equipment_type = unitType;
                            ld.id_location = module.actual.__id_location__.id;
                            ld.id_loader = null;
                            ld.latitude = latitude;
                            ld.longitude = longitude;

                            module.postLogDetail(ld, (result) => {

                                let topicCycle = Object.assign({}, datatype.dataTopicCycle);
                                topicCycle.date = moment().unix();
                                topicCycle.type = unitType === constant.UNIT_HAULER ? 'hauler' : 'loader';
                                topicCycle.sender = module.equipment.id;
                                topicCycle.sender_name = unitInfo.name;
                                topicCycle.destination = module.actual.__id_loader__.id;
                                topicCycle.message = constant.CYCLE_ARRIVE_LOADING;

                                module.pusher.send(constant.TOPIC_PRODUCE_CYCLE, module.token, topicCycle);

                            });

                            // set empty weight, random saja
                            module.emptyWeight = Math.floor(Math.random() * 5);
                        }

                    } else if (module.haulerState === constant.HAULER_STATE_QUEUE_AT_LOADING) {
                        if (route.a.coordinates[module.routeIndex]) {
                            latitude = route.a.coordinates[module.routeIndex][1];
                            longitude = route.a.coordinates[module.routeIndex][0];
                        } else {
                            latitude = route.a.coordinates[route.a.coordinates.length - 1][1];
                            longitude = route.a.coordinates[route.a.coordinates.length - 1][0];
                        }

                    } else if (module.haulerState === constant.HAULER_STATE_SPOTING_AT_DUMP) {
                        if (route.b.coordinates[module.routeIndex]) {
                            latitude = route.b.coordinates[module.routeIndex][1];
                            longitude = route.b.coordinates[module.routeIndex][0];

                        } else {
                            latitude = module.latestGps.lat;
                            longitude = module.latestGps.lon;

                        }

                        if (module.routeIndex >= (route.b.coordinates.length - 1)) {
                            // start dump

                            console.log('post start dumping', unitInfo.name);

                            module.haulerState = constant.HAULER_STATE_START_DUMP;

                            let ld = Object.assign({}, datatype.logDetail);
                            ld.date = 'current';
                            ld.id_actual = module.actual_id;
                            ld.cd_time = constant.TIME_CYCLE_START_DUMP;
                            ld.id_equipment = module.equipment.id;
                            ld.id_operator = module.user.id;
                            ld.cd_equipment_type = unitType;
                            ld.id_location = module.actual.__id_location__.id;
                            ld.id_loader = null;
                            ld.latitude = latitude;
                            ld.longitude = longitude;

                            module.postLogDetail(ld, (result) => {

                                let topicCycle = Object.assign({}, datatype.dataTopicCycle);
                                topicCycle.date = moment().unix();
                                topicCycle.type = unitType === constant.UNIT_HAULER ? 'hauler' : 'loader';
                                topicCycle.sender = module.equipment.id;
                                topicCycle.sender_name = unitInfo.name;
                                topicCycle.destination = module.actual.__id_loader__.id;
                                topicCycle.message = constant.CYCLE_START_DUMP;

                                module.pusher.send(constant.TOPIC_PRODUCE_CYCLE, module.token, topicCycle);

                            });


                            // finish dump
                            setTimeout(function () {

                                console.log('post finish dumping', unitInfo.name);

                                module.haulerState = constant.HAULER_STATE_EMPTY_TRAVEL;

                                let ld = Object.assign({}, datatype.logDetail);
                                ld.date = 'current';
                                ld.id_actual = module.actual_id;
                                ld.cd_time = constant.TIME_CYCLE_FINISH_DUMP;
                                ld.id_equipment = module.equipment.id;
                                ld.id_operator = module.user.id;
                                ld.cd_equipment_type = unitType;
                                ld.id_location = module.actual.__id_location__.id;
                                ld.id_loader = null;
                                ld.latitude = latitude;
                                ld.longitude = longitude;

                                module.postLogDetail(ld, (result) => {

                                    // post ritase
                                    let ritase = Object.assign({}, datatype.dataRitase);
                                    ritase.date = 'current';
                                    ritase.date_operated = 'current';
                                    ritase.id_equipment = module.equipment ? module.equipment.id : '';
                                    ritase.id_actual = module.actual_id;
                                    ritase.material_type = module.actual ? module.actual.__cd_material__.type : '';
                                    ritase.shift = module.actual ? module.actual.__cd_shift__.cd : '';
                                    ritase.id_hauler_operator = module.actual ? module.actual.__id_hauler_operator__ : '';
                                    ritase.id_material = module.actual ? module.actual.__cd_material__.cd : '';
                                    ritase.id_hauler = module.actual ? module.actual.__id_hauler__.id : '';
                                    ritase.id_loader_operator = module.actual ? module.actual.__id_loader_operator__ : '';
                                    ritase.id_loader = module.actual ? module.actual.__id_loader__.id : '';
                                    ritase.id_location = module.actual ? module.actual.__id_location__.id : '';
                                    ritase.id_destination = module.actual ? module.actual.__id_destination__.id : '';
                                    ritase.load_weight = module.loadWeight;
                                    ritase.empty_weight = module.emptyWeight;
                                    ritase.tonnage = module.tonnage;
                                    ritase.tonnage_timbangan = module.tonnageTimbangan;
                                    ritase.tonnage_ujipetik = module.tonnageUjiPetik;
                                    ritase.tonnage_bucket = module.tonnageBucket;
                                    ritase.distance = module.distance;
                                    ritase.bucket_count = module.bucketCount;
                                    ritase.is_supervised = 1;
                                    ritase.is_volume = 0;
                                    ritase.latitude = latitude;
                                    ritase.longitude = longitude;

                                    module.postRitase(ritase, (result) => {
                                        console.log('RITASE', 'saved');

                                        module.tonnage = 0;
                                        module.tonnageTimbangan = 0;
                                        module.tonnageUjiPetik = 0;
                                        module.tonnageBucket = 0;

                                    });


                                    let topicCycle = Object.assign({}, datatype.dataTopicCycle);
                                    topicCycle.date = moment().unix();
                                    topicCycle.type = unitType === constant.UNIT_HAULER ? 'hauler' : 'loader';
                                    topicCycle.sender = module.equipment.id;
                                    topicCycle.sender_name = unitInfo.name;
                                    topicCycle.destination = module.actual.__id_loader__.id;
                                    topicCycle.message = constant.CYCLE_FINISH_DUMP;
                                    topicCycle.detail = {
                                        loadWeight: 0,
                                        emptyWeight: 0,
                                        tonnage: module.tonnage,
                                        distance: module.distance,
                                        bucketCount: module.bucketCount,
                                        latitude: latitude,
                                        longitude: longitude
                                    };

                                    module.pusher.send(constant.TOPIC_PRODUCE_CYCLE, module.token, topicCycle);

                                });

                                // kembalikan index gps ke posisi awal
                                module.routeIndex = -1;

                            }, Math.floor(Math.random() * 7) * 1000);
                        }

                    }

                }
            }

            let gps = {
                "date": date,
                "area_name": module.area ? module.area.name : '',
                "user_status": module.currentActivity ? module.currentActivity.cd : '',  // latest activity
                "device_status": {  // MB
                    "memory_free": os.freemem() / (1024 * 1024),
                    "cpu_system_load": 3.08,
                    "storage_free": 15 * 1024,
                    "storage_size": 30 * 1024,
                    "cpu_system": cpu.sys,
                    "cpu_process": cpu.user,
                    "memory_size": os.totalmem() / (1024 * 1024)
                },
                "actual_id": module.actual_id,
                "unit_type": module.equipment ? module.equipment.cd_equipment : '',
                "operation_loader_id": module.actual_loader_id,
                "material_type": module.actual ? module.actual.__cd_material__.type : '',
                "actual_support_id": module.actual_support_id,
                "ecus": {
                    "date": moment.unix(date).toString(),
                    "ecuHD785": {
                        "Eng_Speed": Math.random() * 10,
                        "TM_Out_Speed": Math.random() * 10,
                        "Vehicle_Speed_S": Math.random() * 10,
                        "Shift_Indicator": Math.random() * 10,
                        "Live_Weight": Math.random() * 10,
                        "Fuel_Injection": Math.random() * 10,
                        "Accel_Pos": Math.random() * 10,
                        "Retarder_F_Oit_temp": Math.random() * 10,
                        "Retarder_R_Oil_temp": Math.random() * 10,
                        "Fuel_Rate_01L": Math.random() * 10,
                        "Retarder_Pos": Math.random() * 10,
                    }
                },
                "user_id": module.user ? module.user.id : '',
                "engine": module.isSignIn ? 'on' : 'off',
                "ecu": {
                    "hour_meter": {
                        "unit": "hour",
                        "value": module.hmStart / 3600  // in hour
                    },
                    "distance": {
                        "unit": "km",
                        "value": module.distance / 1000 // in KM
                    },
                    "payload": {
                        "unit": "ton",
                        "value": Math.random() * 10,
                    },
                    "engine_temperature": {
                        "unit": "C",
                        "value": Math.random() * 10,
                    },
                    "fuel_rate": {
                        "unit": "liter",
                        "value": Math.random() * 10,
                    },
                    "vehicle_speed": {
                        "unit": "km/hour",
                        "value": Math.random() * 10,
                    }
                },
                "user_login": module.isSignIn,
                "user_tum": module.currentTum ? module.currentTum.cd : '',
                "unit_id": module.equipment.id,
                "altitude": "0",
                "latitude": latitude,
                "longitude": longitude,
                "status": module.haulerState
            };

            module.latestGps.lat = latitude;
            module.latestGps.lon = longitude;

            // console.log(gps);

            module.pusher.send(constant.TOPIC_GPS, module.token ? module.token : constant.DEFAULT_KEY, gps);

        }, constant.GPS_INTERVAL);

        // clearInterval(interval);
    }

    module.doLogin = (callback) => {
        console.log('login process');
        axios.put(baseAPI + '/user/auth', {
            staff_id: unitInfo.operator,
            pwd: md5('sbsjaya'),
            state: 'login',
            equipment_id: module.equipment.id,
            unit_type: unitType,
            latitude: module.latestGps.lat,
            longitude: module.latestGps.lon
        }).then((result) => {
            let _user = null;
            let _token = null;
            if (result.data) {
                if (result.data.response === 'success') {
                    _user = result.data.output;
                    _token = _user.token;
                }
            }

            if (_user === null) {
                throw Error('Sign in failed');
            }

            /**
             * get data actual after login
             * ketika data aktual ada maka proses login sukses
             * jika tidak proses login dianggap gagal
             */
            if (_token !== null) {
                console.log(unitInfo.name, ' login success, get actual ');

                if (unitType === constant.UNIT_HAULER || unitType === constant.UNIT_LOADER) {
                    module.getActual(_token, (status, actual) => {
                        if (status) {
                            module.isSignIn = true;
                            module.user = _user;
                            module.token = _token;
                            module.actual_id = actual.id;
                            module.actual = actual;

                            module.getArea(baseAPI + '/operation/actual/actual-area/' + module.actual_id);

                            if (unitType === constant.UNIT_LOADER) {
                                module.getActualLoader(_token, (status, actual) => {
                                    if (status) {
                                        module.actual_loader_id = actual.id;
                                        module.actual_loader = actual;

                                        console.log('actual loader found');
                                    } else {
                                        console.log('actual loader not found');
                                    }
                                });
                            }

                            callback();

                            console.log('actual hauler found');
                        } else {
                            console.log('actual hauler not found');
                        }
                    });
                }
            }
        });
    }

    module.setActivity = (act, cb) => {
        axios.post(baseAPI + '/equipment-log-start/screen/activity', act, {
            headers: {
                'Authorization': `Bearer ${module.token}`
            }
        }).then((result) => {
            if (result.data) {
                if (result.data.response === 'success') {
                    cb(result.data.output);
                } else {
                    console.error(act, result.data);
                }
            } else {
                console.error(result);
            }
        }, (error) => {
            console.error(error);
        });
    }

    module.postLogDetail = (ld, cb) => {
        axios.post(baseAPI + '/operation/log-detail', [ld], {
            headers: {
                'Authorization': `Bearer ${module.token}`
            }
        }).then((result) => {
            if (result.data) {
                if (result.data.response === 'success') {
                    cb(result.data.output);
                } else {
                    console.error(ld, result.data);
                }
            } else {
                console.error(ld, result);
            }
        }, (error) => {
            console.error(error);
        });
    }

    module.postRitase = (ld, cb) => {
        axios.post(baseAPI + '/operation/actual/raw', ld, {
            headers: {
                'Authorization': `Bearer ${module.token}`
            }
        }).then((result) => {
            if (result.data) {
                if (result.data.response === 'success') {
                    cb(result.data.output);
                } else {
                    console.error(ld, result.data);
                }
            } else {
                console.error(ld, result);
            }
        }, (error) => {
            console.error(error);
        });
    }

    module.getArea = (url) => {
        axios.get(url, {
            params: {},
            headers: {
                'Authorization': `Bearer ${module.token}`
            }
        }).then((result) => {
            if (result.data) {
                if (result.data.response === 'success') {
                    module.area = result.data.output.__id_location__.id_area;
                }
            }
        });
    }

    module.getActual = (_token, callback) => {
        axios.get(baseAPI + '/operation/actual/by-equipment-name', {
            params: {
                date: moment().unix(),
                name: unitInfo.name,
                type: unitType,
                shift: util.getCurrentShift()
            },
            headers: {
                'Authorization': `Bearer ${_token}`
            }
        }).then((result) => {
            if (result.data) {
                if (result.data.output) {
                    callback(true, result.data.output);
                } else {
                    console.error(result.data.message, unitInfo);
                    callback(false, null);
                }
            } else {
                callback(false, null);
            }
        });
    }

    module.getActualLoader = (_token, callback) => {
        axios.get(baseAPI + '/operation/loader/by-equipment-name', {
            params: {
                date: moment().unix(),
                name: unitInfo.name,
                shift: util.getCurrentShift()
            },
            headers: {
                'Authorization': `Bearer ${_token}`
            }
        }).then((result) => {
            if (result.data) {
                if (result.data.output) {
                    callback(true, result.data.output);
                } else {
                    console.error(result.data.message, unitInfo);
                    callback(false, null);
                }
            } else {
                callback(false, null);
            }
        });
    }

    module.postPrestart = async (pdata, cb) => {
        axios.post(baseAPI + '/prestart-log/screen/bulk', pdata, {
            headers: {
                'Authorization': `Bearer ${module.token}`
            }
        }).then((result) => {
            if (result.data) {
                if (result.data.respon === 'success') {
                    cb(result.data.output);
                } else {
                    console.error(result.data);
                    throw Error('failed post p2h');
                }
            } else {
                console.error(result);
                throw Error('failed post p2h');
            }
        });
    }

    module.run = async () => {
        let nextDelay = 0;

        const currentHour = moment().hour();
        if (currentHour < 12) {
            module.sholatDzuhur = false;
            module.sholatAsar = false;
            module.sholatIsya = false;
        } else if (currentHour < 15) {
            module.sholatDzuhur = false;
            module.sholatAsar = false;
            module.sholatIsya = false;
        } else if (currentHour < 19) {
            module.sholatDzuhur = true;
            module.sholatAsar = true;
            module.sholatIsya = false;
        } else {
            module.sholatDzuhur = true;
            module.sholatAsar = true;
            module.sholatIsya = true;
        }

        await module.getEquipment();
        module.initKafkaConsumer();
        module.initKafkaPusher();

        /**
         * delay agar tiap truk tidak mulai pada waktu yang sama
         */
        setTimeout(function () {
            module.initGpsPusher();
        }, Math.floor(Math.random() * 10) * 1000);

        //
        nextDelay += 10;

        /**
         * user login
         */
        setTimeout(function () {
            console.log(unitInfo.name, ' do login ');

            module.doLogin(() => {
                let p2h = Object.assign({}, datatype.activity);
                if (unitType === constant.UNIT_HAULER) {
                    p2h.id_actual = module.actual_id;
                } else if (unitType === constant.UNIT_LOADER) {
                    p2h.id_actual = module.actual_id;
                    p2h.id_operation_loader = module.actual_loader_id;
                } else {
                    p2h.id_operation_support = module.actual_support_id;
                }
                p2h.id_equipment = module.equipment.id;
                p2h.id_operator = module.user.id;
                p2h.time = 'current';
                p2h.cd_activity = constant.CD_ACTIVITY_P2H;
                module.setActivity(p2h, (res) => {
                    module.currentActivity = res.cd_activity;
                    module.currentTum = res.cd_tum;
                });
            });
        }, (Math.floor(Math.random() * 10) + nextDelay) * 1000);

        //
        nextDelay += 10;

        /**
         * get prestart data
         */
        const prestartList = await util.mysqlQuery(mysql, "SELECT * FROM prestart WHERE cd_type_equipment='" + unitType + "'");

        /**
         * post P2H
         */
        setTimeout(function () {

            console.log(unitInfo.name, ' post prestart ');

            let pdata = [];
            for (let plist of prestartList) {
                let _pdata = Object.assign({}, datatype.prestart);
                _pdata.id_equipment = module.equipment.id;
                _pdata.id_user = module.user.id;
                _pdata.id_prestart = plist.id;
                if (unitType === constant.UNIT_HAULER || unitType === constant.UNIT_LOADER) {
                    _pdata.id_actual = module.actual_id;
                } else {
                    _pdata.id_actual_support = module.actual_support_id;
                }
                _pdata.value = 'dummy';
                _pdata.qty = Math.floor(Math.random() * 20);
                pdata.push(_pdata);
            }

            module.postPrestart(pdata, (result) => {

                console.log(unitInfo.name, ' start working activity ');

                let act = Object.assign({}, datatype.activity);
                if (unitType === constant.UNIT_HAULER) {
                    act.id_actual = module.actual_id;
                    act.cd_activity = module.actual.__cd_operation__.cd;
                } else if (unitType === constant.UNIT_LOADER) {
                    act.id_actual = module.actual_id;
                    act.id_operation_loader = module.actual_loader_id;
                    act.cd_activity = module.actual_loader.cd_operation.cd;
                } else {
                    act.id_operation_support = module.actual_support_id;
                }
                act.id_equipment = module.equipment.id;
                act.id_operator = module.user.id;
                act.time = 'current';
                module.setActivity(act, (res) => {
                    module.currentActivity = res.cd_activity;
                    module.currentTum = res.cd_tum;
                });

            });

        }, (Math.floor(Math.random() * 10) + nextDelay) * 1000);

        //
        nextDelay += 10;

        /**
         * working process
         */
        setTimeout(function () {

            if (unitType === constant.UNIT_HAULER) {

                module.haulerState = constant.HAULER_STATE_EMPTY_TRAVEL;

                let position = Math.floor(Math.random() * route.a.coordinates.length);
                /**
                 * kurangi 5, untuk spare arrive loading
                 */
                if (position > 5) {
                    position -= 5;
                }

                module.routeIndex = position;

            }

        }, (Math.floor(Math.random() * 10) + nextDelay) * 1000);

        //
        nextDelay += 10;

        /**
         * start consume
         */
        setInterval(function () {

            /**
             * interval consumer
             */
            module.consumer.consume(5, (err, message) => {
                console.log('CONSUMER', message.length);
            });

        }, 5000);

    }

    return module;
};